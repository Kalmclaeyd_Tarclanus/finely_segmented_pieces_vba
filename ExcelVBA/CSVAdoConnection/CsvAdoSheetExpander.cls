VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CsvAdoSheetExpander"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   output SQL result into Excel.Worksheet after connecting CSV file by ADO-DB interface
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 10/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IOutputBookPath

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjConnector As CsvAdoConnector: Private mitfConnector As IADOConnector

Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjConnector = New CsvAdoConnector
    
    Set mitfConnector = mobjConnector
    
    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
    
    With mobjAdoRecordsetSheetExpander
    
        Set .ADOConnectorInterface = mobjConnector
    
        Set .OutputBookPathInterface = Me
        
        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfCsvAdoSQL
    End With
End Sub

Private Sub Class_Terminate()

    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess

    Me.CloseConnection

    Set mobjAdoRecordsetSheetExpander = Nothing
    
    Set mobjConnector = Nothing
End Sub

'///////////////////////////////////////////////
'/// Properties - connoted ADO connector
'///////////////////////////////////////////////
Public Property Get SQLExecutionResult() As SQLResult

    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
End Property

Public Property Get IsSqlExecutionFailed() As Boolean

    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
End Property

Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)

    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
End Property
Public Property Get AllowToRecordSQLLog() As Boolean

    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
End Property


Public Property Get CurrentSheet() As Excel.Worksheet

    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
End Property


Public Property Get CurrentBook() As Excel.Workbook

    If Not Me.CurrentSheet Is Nothing Then
    
        Set CurrentBook = Me.CurrentSheet.Parent
    Else
        Set CurrentBook = Nothing
    End If
End Property

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get ADOConnectorInterface() As IADOConnector

    Set ADOConnectorInterface = mitfConnector
End Property

'''
'''
'''
Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter

    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
End Property


'**---------------------------------------------
'** connoted DataTableSheetFormatter
'**---------------------------------------------
Public Property Get TopLeftRowIndex() As Long

    TopLeftRowIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex
End Property
Public Property Let TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)

    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex = vintTopLeftRowIndex
End Property
Public Property Get TopLeftColumnIndex() As Long

    TopLeftColumnIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex
End Property
Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)

    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex = vintTopLeftColumnIndex
End Property


Public Property Let AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)

    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle = vblnAllowToShowFieldTitle
End Property
Public Property Get AllowToShowFieldTitle() As Boolean

    AllowToShowFieldTitle = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle
End Property



Public Property Let AllowToSetAutoFilter(ByVal vblnAllowToSetAutoFilter As Boolean)

    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter = vblnAllowToSetAutoFilter
End Property
Public Property Get AllowToSetAutoFilter() As Boolean

    AllowToSetAutoFilter = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter
End Property

'''
''' Dictionary(Of String(column title), Double(column-width))
'''
Public Property Set FieldTitleToColumnWidthDic(ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary)

    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic = vobjFieldTitleToColumnWidthDic
End Property
Public Property Get FieldTitleToColumnWidthDic() As Scripting.Dictionary

    Set FieldTitleToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic
End Property

'''
''' Dictionary(Of String(column title order, such as 'Col1', 'Col07'), Double(column-width))
'''
Public Property Set FieldTitleOrderToColumnWidthDic(ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary)

    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic = vobjFieldTitleOrderToColumnWidthDic
End Property
Public Property Get FieldTitleOrderToColumnWidthDic() As Scripting.Dictionary

    Set FieldTitleOrderToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic
End Property


'''
''' When it is true, then ConvertDataForColumns procedure is used.
'''
Public Property Get AllowToConvertDataForSpecifiedColumns() As Boolean

    AllowToConvertDataForSpecifiedColumns = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns
End Property
Public Property Let AllowToConvertDataForSpecifiedColumns(ByVal vblnAllowToConvertDataForSpecifiedColumns As Boolean)

    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns = vblnAllowToConvertDataForSpecifiedColumns
End Property
'''
''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
'''
Public Property Get FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary

    Set FieldTitlesToColumnDataConvertTypeDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic
End Property
Public Property Set FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)

    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
End Property

'''
''' ColumnsNumberFormatLocalParam
'''
Public Property Set ColumnsNumberFormatLocal(ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)

    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal = vobjColumnsNumberFormatLocalParam
End Property
Public Property Get ColumnsNumberFormatLocal() As ColumnsNumberFormatLocalParam

    Set ColumnsNumberFormatLocal = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal
End Property

'''
''' set format-condition for each column
'''
Public Property Set ColumnsFormatCondition(ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)

    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition = vobjColumnsFormatConditionParam
End Property
Public Property Get ColumnsFormatCondition() As ColumnsFormatConditionParam

    Set ColumnsFormatCondition = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition
End Property

'''
''' merge cells when the same values continue
'''
Public Property Let AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)

    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
End Property
Public Property Get AllowToMergeCellsByContinuousSameValues() As Boolean

    AllowToMergeCellsByContinuousSameValues = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues
End Property
Public Property Let AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)

    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
End Property
Public Property Get AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean

    AllowToSetVerticalTopAlignmentTopOfMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells
End Property

Public Property Set FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)

    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
End Property
Public Property Get FieldTitlesToMergeCells()

    Set FieldTitlesToMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells
End Property

Public Property Let FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)

    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType = venmFieldTitleInteriorType
End Property
Public Property Get FieldTitleInteriorType() As FieldTitleInterior

    FieldTitleInteriorType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType
End Property

Public Property Let RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)

    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType = venmRecordCellsBordersType
End Property
Public Property Get RecordBordersType() As RecordCellsBorders

    RecordBordersType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType
End Property

'///////////////////////////////////////////////
'/// Properties - connoted ADOConnectionSetting
'///////////////////////////////////////////////
Public Property Get ConnectSetting() As ADOConnectionSetting

    Set ConnectSetting = mobjConnector.ConnectSetting
End Property

'''
''' unit is second. [s]
'''
Public Property Let CommandTimeout(ByVal vintTimeout As Long)

    mobjConnector.CommandTimeout = vintTimeout
End Property
Public Property Get CommandTimeout() As Long

    CommandTimeout = mobjConnector.CommandTimeout
End Property

'///////////////////////////////////////////////
'/// Properties - CSV files connection by ACCESS OLE-DB provider
'///////////////////////////////////////////////
'''
''' connecting CSV file path
'''
Public Property Get ConnectingFilePath() As String

    ConnectingFilePath = mobjConnector.ConnectingFilePath
End Property

'''
''' connecting directory path, which includes CSV files
'''
Public Property Get ConnectingDirectoryPath() As String

    ConnectingDirectoryPath = mobjConnector.ConnectingDirectoryPath
End Property

'''
''' when the load Excel sheet doesn't have the field header title text, this is False
'''
Public Property Get HeaderFieldTitleExists() As Boolean

    HeaderFieldTitleExists = mobjConnector.HeaderFieldTitleExists
End Property


'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
Private Function IOutputBookPath_GetPreservedOutputBookPath() As String

    IOutputBookPath_GetPreservedOutputBookPath = GetCsvAdoConnectingGeneralLogBookPath()
End Function


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** preparation before SQL execution
'**---------------------------------------------
'''
''' set input CSV file path
'''
Public Sub SetInputCsvFileConnection(ByVal vstrCSVFilePath As String, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)

    mobjConnector.SetInputCsvFileConnection vstrCSVFilePath, vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
End Sub

'''
''' set input directory path, which includes CSV files
'''
Public Sub SetInputCsvDirectoryConnection(ByVal vstrCsvDirectoryPath As String, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)

    mobjConnector.SetInputCsvDirectoryConnection vstrCsvDirectoryPath, vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
End Sub

'''
''' Close ADO connection
'''
Public Sub CloseConnection()

    mobjConnector.CloseConnection
End Sub

'''
'''
'''
Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()

    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
End Sub

'''
'''
'''
Public Sub CloseAll()

    CloseConnection

    CloseCacheOutputBookOfOpenByPresetModeProcess
End Sub

'''
''' get recordset object by the Select SQL
'''
Public Function GetRecordset(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset

    Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
End Function


'**---------------------------------------------
'** Output Recordset and logs to cells on sheet
'**---------------------------------------------
'''
''' query SQL and output result to Excel sheet
'''
Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
        ByVal vobjOutputSheet As Excel.Worksheet, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
     
    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, _
            vobjOutputSheet, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            mfobjGetConnectedCSVPathInsertText(), _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the Existed Excel sheet
'''
Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, _
            vstrNewSheetName, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            mfobjGetConnectedCSVPathInsertText(), _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)


    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
'''
'''
Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)

    
    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)

    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the already specified Excel sheet
'''
Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, _
            vstrNewSheetName, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            mfobjGetConnectedCSVPathInsertText(), _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
'''
'''
Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)


    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, _
            venmOutputRSetExcelBookOpenModeProcessFlag
End Sub

'**---------------------------------------------
'** Excecute SQL command (UPDATE, INSERT, DELETE)
'**---------------------------------------------
'''
''' command SQL and output result-log to Excel sheet
'''
Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the prepared Excel sheet
'''
Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
Private Function mfobjGetConnectedCSVPathInsertText() As Collection
    
    Dim objInsertTexts As Collection
    
    Set objInsertTexts = New Collection
    
    With Me
        If FileExistsByVbaDir(.ConnectingFilePath) Then
    
            objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <CSV Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
        Else
            objInsertTexts.Add "<CSV Directory> " & .ConnectingDirectoryPath
        End If
    End With
    
    Set mfobjGetConnectedCSVPathInsertText = objInsertTexts
End Function


