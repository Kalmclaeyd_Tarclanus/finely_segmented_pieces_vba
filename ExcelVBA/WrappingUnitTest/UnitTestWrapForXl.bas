Attribute VB_Name = "UnitTestWrapForXl"
'
'   Output results to Excel sheet for the formal wrapped unit tests
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Visualize the results of Unit-tests
'
'   Dependency Abstract:
'       Dependent on Excel for sheet-output
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 17/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Unit test wrapper tools
'**---------------------------------------------
'''
'''
'''
Public Function UnitTestBasedOnWrappedFormation(ByRef robjUnitTestWrappedParameters As UnitTestWrappedParameters) As Excel.Worksheet

    Dim objDTCol As Collection, blnAreAllValid As Boolean, objSheet As Excel.Worksheet
    Dim objDoubleStopWatch As DoubleStopWatch
    Dim varFunctionName As Variant, strFunctionName As String, strRightIOGettingFunctionName As String
    Dim strOriginalFunctionNameBeforeWrapping As String
    
    
    Set objDoubleStopWatch = New DoubleStopWatch
    
    Set objDTCol = New Collection
    
    objDoubleStopWatch.MeasureStart
    
    blnAreAllValid = True
    
    With robjUnitTestWrappedParameters
    
        For Each varFunctionName In .TestFunctionNameToRightIOGettingFunctionNameDic.Keys
    
            strFunctionName = varFunctionName
        
            strRightIOGettingFunctionName = .TestFunctionNameToRightIOGettingFunctionNameDic.Item(varFunctionName)
        
            With .TestFunctionNameToOriginalFunctionNameBeforeWrappingDic
            
                If .Exists(varFunctionName) Then
                
                    strOriginalFunctionNameBeforeWrapping = .Item(varFunctionName)
                Else
                    strOriginalFunctionNameBeforeWrapping = ""
                End If
            End With
            
 
            If IsPassedCompletelyByInterfaceOfOneInputAndPluralOutputs(objDTCol, strFunctionName, strRightIOGettingFunctionName, .CountOfOutputs, False, .ModuleNameOfTestingFunction, strOriginalFunctionNameBeforeWrapping) Then
            
                'Debug.Print "The all unit-tests are passed about "
                
            Else
                blnAreAllValid = False
            End If
        Next
    End With
    
    objDoubleStopWatch.MeasureInterval
    
    DebugCol objDTCol

    With robjUnitTestWrappedParameters

        Set objSheet = OutputUnitTestResultsToSheetWithOpeningBook(objDTCol, .UnitTestResultBookBaseName, .SheetFormatSetting)

        AddAutoShapeGeneralMessageForWrappedUnitTest objSheet, .AutoShapeUnitTestTheme, blnAreAllValid, objDTCol, .SheetFormatSetting, objDoubleStopWatch
    End With

    Set UnitTestBasedOnWrappedFormation = objSheet
End Function

'**---------------------------------------------
'** Output unit-test results to Sheet
'**---------------------------------------------
'''
'''
'''
Public Sub OutputUnitTestResultsToSheet(ByRef robjSheet As Excel.Worksheet, ByVal vobjDataTableCol As Collection, ByRef robjUnitTestSheetFormatSetting As UnitTestSheetFormatSetting)

    Dim objFieldTitlesCol As Collection, objDataTableSheetFormatter As DataTableSheetFormatter

    With robjUnitTestSheetFormatSetting
    
        Set objFieldTitlesCol = mfobjGetFieldTitlesColForWrappedUnitTest(.InputFieldTitlesCol, .OutputFieldTitlesCol)
    
        Set objDataTableSheetFormatter = mfobjGetDataTableSheetFormatterOfUnitTest(.InputFieldTitleToColumnWidthDic, .OutputFieldTitleToColumnWidthDic)
    End With

    OutputColToSheet robjSheet, vobjDataTableCol, objFieldTitlesCol, objDataTableSheetFormatter
End Sub

'''
'''
'''
''' <Argument>robjSheet: Input-Output</Argument>
''' <Argument>vobjDataTableCol: Input</Argument>
''' <Argument>rudtUnitTestSheetFormatSetting: Input</Argument>
Public Sub OutputUnitTestResultsToSheetByStruct(ByRef robjSheet As Excel.Worksheet, _
        ByVal vobjDataTableCol As Collection, _
        ByRef rudtUnitTestSheetFormatSetting As UnitTestSheetFormatSettingStruct)


    Dim objFieldTitlesCol As Collection, objDataTableSheetFormatter As DataTableSheetFormatter

    With rudtUnitTestSheetFormatSetting
    
        Set objFieldTitlesCol = mfobjGetFieldTitlesColForWrappedUnitTest(.InputFieldTitlesCol, .OutputFieldTitlesCol)
    
        Set objDataTableSheetFormatter = mfobjGetDataTableSheetFormatterOfUnitTest(.InputFieldTitleToColumnWidthDic, _
                .OutputFieldTitleToColumnWidthDic)
    End With

    OutputColToSheet robjSheet, _
            vobjDataTableCol, _
            objFieldTitlesCol, _
            objDataTableSheetFormatter
End Sub

'''
'''
'''
Public Sub OutputUnitTestResultsToSheetByParams(ByRef robjSheet As Excel.Worksheet, ByVal vobjDataTableCol As Collection, ByVal objInputFieldTitlesCol As Collection, ByVal objInputFieldTitleToColumnWidthDic As Scripting.Dictionary, ByVal objOutputFieldTitlesCol As Collection, ByVal objOutputFieldTitleToColumnWidthDic As Scripting.Dictionary)

    Dim objFieldTitlesCol As Collection, objDataTableSheetFormatter As DataTableSheetFormatter

    Set objFieldTitlesCol = mfobjGetFieldTitlesColForWrappedUnitTest(objInputFieldTitlesCol, objOutputFieldTitlesCol)

    Set objDataTableSheetFormatter = mfobjGetDataTableSheetFormatterOfUnitTest(objInputFieldTitleToColumnWidthDic, objOutputFieldTitleToColumnWidthDic)

    OutputColToSheet robjSheet, vobjDataTableCol, objFieldTitlesCol, objDataTableSheetFormatter
End Sub

'''
'''
'''
Public Function OutputUnitTestResultsToSheetWithOpeningBook(ByVal vobjDataTableCol As Collection, _
        ByVal vstrUnitTestResultsBookBaseName As String, _
        ByRef robjUnitTestSheetFormatSetting As UnitTestSheetFormatSetting) As Excel.Worksheet
    
    
    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    Set objBook = GetTemporaryUnitTestResultsBook(vstrUnitTestResultsBookBaseName)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    objSheet.Name = "UnitTests"
    
    OutputUnitTestResultsToSheet objSheet, vobjDataTableCol, robjUnitTestSheetFormatSetting
    
    DeleteDummySheetWhenItExists objBook
    
    Set OutputUnitTestResultsToSheetWithOpeningBook = objSheet
End Function


'''
'''
'''
Public Function OutputUnitTestResultsToSheetWithOpeningBookByStruct(ByVal vobjDataTableCol As Collection, _
        ByVal vstrUnitTestResultsBookBaseName As String, _
        ByRef rudtUnitTestSheetFormatSetting As UnitTestSheetFormatSettingStruct) As Excel.Worksheet
    
    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    Set objBook = GetTemporaryUnitTestResultsBook(vstrUnitTestResultsBookBaseName)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    objSheet.Name = "UnitTests"
    
    OutputUnitTestResultsToSheetByStruct objSheet, _
            vobjDataTableCol, _
            rudtUnitTestSheetFormatSetting
    
    DeleteDummySheetWhenItExists objBook
    
    Set OutputUnitTestResultsToSheetWithOpeningBookByStruct = objSheet
End Function

'''
'''
'''
''' <Argument>vstrUnitTestResultsBookBaseName: Input</Argument>
''' <Return>Excel.Workbook</Return>
Public Function GetTemporaryUnitTestResultsBook(ByVal vstrUnitTestResultsBookBaseName As String) As Excel.Workbook

    Dim strBookPath As String, objBook As Excel.Workbook
    
    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\" & vstrUnitTestResultsBookBaseName & ".xlsx"

    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)

    Set GetTemporaryUnitTestResultsBook = objBook
End Function


'**---------------------------------------------
'** AutoShape general message modification for the unit test wrapper tools
'**---------------------------------------------
'''
'''
'''
''' <Argument>vobjSheet: Input-Output</Argument>
''' <Argument>vstrTheme: Input</Argument>
''' <Argument>vblnAreAllValid: Input</Argument>
''' <Argument>robjUnitTestResultsDataTable: Input</Argument>
''' <Argument>rudtUnitTestSheetFormatSetting: Input</Argument>
''' <Argument>vobjDoubleStopWatch: Input</Argument>
Public Sub AddAutoShapeGeneralMessageForWrappedUnitTest(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vstrTheme As String, _
        ByVal vblnAreAllValid As Boolean, _
        ByRef robjUnitTestResultsDataTable As Collection, _
        ByRef robjUnitTestSheetFormatSetting As UnitTestSheetFormatSetting, _
        ByVal vobjDoubleStopWatch As DoubleStopWatch)


    Dim enmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType
    
    Dim objDecorationTargetTextSettings As Collection, objDecorationTagValueSettings As Collection, objDecorationTagValueSettingParts As Collection
    
    Dim strMessage As String, strLog As String
    
    
    Set objDecorationTargetTextSettings = New Collection
    
    With objDecorationTargetTextSettings
    
        .Add vstrTheme & ";15;trRGBWhiteFontColor"
    End With
    
    
    If vblnAreAllValid Then
    
        enmAutoShapeMessageFillPatternType = XlShapeTextLogInteriorFillDefault
    Else
        enmAutoShapeMessageFillPatternType = XlShapeTextLogInteriorFillGradientYellow
    End If

    Set objDecorationTagValueSettings = New Collection
    
    strLog = mfstrGetLogTextFromUnitTestSheetFormatSetting(robjUnitTestSheetFormatSetting, objDecorationTagValueSettings)
    
    strLog = strLog & vbNewLine & "Count of test cases: " & CStr(robjUnitTestResultsDataTable.Count) & vbNewLine
    
    strLog = strLog & "Elapsed time: " & Format(vobjDoubleStopWatch.ElapsedSecondTime, "0.000") & " [s]" & vbNewLine
    
    strLog = strLog & vbNewLine & "Logging date: " & Format(Now, "ddd, yyyy/m/d hh:mm:ss")
    
    With objDecorationTagValueSettings
    
        .Add "Count of test cases;" & vbNewLine & ";11;trRGBPaleYellowFontColor; "
        
        .Add "Elapsed time;[s];11;trRGBPaleYellowFontColor; "
        
        .Add "Logging date;" & vbNewLine & ";10.5;trRGBPaleGreenFontColor; "
    End With

    strMessage = vstrTheme & vbNewLine & strLog

    AddAutoShapeGeneralMessage vobjSheet, _
            strMessage, _
            enmAutoShapeMessageFillPatternType, _
            Nothing, _
            objDecorationTagValueSettings, _
            objDecorationTargetTextSettings, _
            9
End Sub

'''
'''
'''
''' <Argument>vobjSheet: Input-Output</Argument>
''' <Argument>vstrTheme: Input</Argument>
''' <Argument>vblnAreAllValid: Input</Argument>
''' <Argument>robjUnitTestResultsDataTable: Input</Argument>
''' <Argument>rudtUnitTestSheetFormatSetting: Input</Argument>
''' <Argument>vobjDoubleStopWatch: Input</Argument>
Public Sub AddAutoShapeGeneralMessageForWrappedUnitTestByStruct(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vstrTheme As String, _
        ByVal vblnAreAllValid As Boolean, _
        ByRef robjUnitTestResultsDataTable As Collection, _
        ByRef rudtUnitTestSheetFormatSetting As UnitTestSheetFormatSettingStruct, _
        ByVal vobjDoubleStopWatch As DoubleStopWatch)


    Dim enmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType
    
    Dim objDecorationTargetTextSettings As Collection, objDecorationTagValueSettings As Collection, objDecorationTagValueSettingParts As Collection
    
    Dim strMessage As String, strLog As String
    
    
    Set objDecorationTargetTextSettings = New Collection
    
    With objDecorationTargetTextSettings
    
        .Add vstrTheme & ";15;trRGBWhiteFontColor"
    End With
    
    
    If vblnAreAllValid Then
    
        enmAutoShapeMessageFillPatternType = XlShapeTextLogInteriorFillDefault
    Else
        enmAutoShapeMessageFillPatternType = XlShapeTextLogInteriorFillGradientYellow
    End If

    Set objDecorationTagValueSettings = New Collection
    
    strLog = mfstrGetLogTextFromUnitTestSheetFormatSettingByStruct(rudtUnitTestSheetFormatSetting, objDecorationTagValueSettings)
    
    strLog = strLog & vbNewLine & "Count of test cases: " & CStr(robjUnitTestResultsDataTable.Count) & vbNewLine
    
    strLog = strLog & "Elapsed time: " & Format(vobjDoubleStopWatch.ElapsedSecondTime, "0.000") & " [s]" & vbNewLine
    
    strLog = strLog & vbNewLine & "Logging date: " & Format(Now, "ddd, yyyy/m/d hh:mm:ss")
    
    With objDecorationTagValueSettings
    
        .Add "Count of test cases;" & vbNewLine & ";11;trRGBPaleYellowFontColor; "
        
        .Add "Elapsed time;[s];11;trRGBPaleYellowFontColor; "
        
        .Add "Logging date;" & vbNewLine & ";10.5;trRGBPaleGreenFontColor; "
    End With

    strMessage = vstrTheme & vbNewLine & strLog

    AddAutoShapeGeneralMessage vobjSheet, _
            strMessage, _
            enmAutoShapeMessageFillPatternType, _
            Nothing, objDecorationTagValueSettings, _
            objDecorationTargetTextSettings, _
            9
End Sub

'''
'''
'''
Private Function mfstrGetLogTextFromUnitTestSheetFormatSetting(ByRef robjUnitTestSheetFormatSetting As UnitTestSheetFormatSetting, _
        ByRef robjDecorationTagValueSettingParts As Collection) As String

    Dim strLog As String, i As Long, varTag As Variant

    strLog = ""

    With robjUnitTestSheetFormatSetting
    
        If .InputFieldTitlesCol.Count = 1 Then
        
            strLog = strLog & "Input: "
            
            robjDecorationTagValueSettingParts.Add "Input;" & vbNewLine & ";11;trRGBPaleYellowFontColor; "
        Else
            strLog = strLog & "Inputs: "
            
            robjDecorationTagValueSettingParts.Add "Inputs;" & vbNewLine & ";11;trRGBPaleYellowFontColor; "
        End If
        
        i = 1
        For Each varTag In .InputFieldTitlesCol
        
            strLog = strLog & varTag
            
            If i < .InputFieldTitlesCol.Count Then
            
                strLog = strLog & ", "
            End If
            
            i = i + 1
        Next
    
        strLog = strLog & vbNewLine
    
        If .OutputFieldTitlesCol.Count = 1 Then
        
            strLog = strLog & "Output: "
            
            robjDecorationTagValueSettingParts.Add "Output;" & vbNewLine & ";11;trRGBPaleYellowFontColor; "
        Else
            strLog = strLog & "Outputs: "
            
            robjDecorationTagValueSettingParts.Add "Outputs;" & vbNewLine & ";11;trRGBPaleYellowFontColor; "
        End If
    
        i = 1
        For Each varTag In .OutputFieldTitlesCol
        
            strLog = strLog & varTag
            
            If i < .OutputFieldTitlesCol.Count Then
            
                strLog = strLog & ", "
            End If
            
            i = i + 1
        Next
    End With

    mfstrGetLogTextFromUnitTestSheetFormatSetting = strLog
End Function


'''
'''
'''
''' <Argument>rudtUnitTestSheetFormatSetting: Input</Argument>
''' <Argument>robjDecorationTagValueSettingParts: Input</Argument>
''' <Return>String</Return>
Private Function mfstrGetLogTextFromUnitTestSheetFormatSettingByStruct(ByRef rudtUnitTestSheetFormatSetting As UnitTestSheetFormatSettingStruct, _
        ByRef robjDecorationTagValueSettingParts As Collection) As String

    Dim strLog As String, i As Long, varTag As Variant

    strLog = ""

    With rudtUnitTestSheetFormatSetting
    
        If .InputFieldTitlesCol.Count = 1 Then
        
            strLog = strLog & "Input: "
            
            robjDecorationTagValueSettingParts.Add "Input;" & vbNewLine & ";11;trRGBPaleYellowFontColor; "
        Else
            strLog = strLog & "Inputs: "
            
            robjDecorationTagValueSettingParts.Add "Inputs;" & vbNewLine & ";11;trRGBPaleYellowFontColor; "
        End If
        
        i = 1
        For Each varTag In .InputFieldTitlesCol
        
            strLog = strLog & varTag
            
            If i < .InputFieldTitlesCol.Count Then
            
                strLog = strLog & ", "
            End If
            
            i = i + 1
        Next
    
        strLog = strLog & vbNewLine
    
        If .OutputFieldTitlesCol.Count = 1 Then
        
            strLog = strLog & "Output: "
            
            robjDecorationTagValueSettingParts.Add "Output;" & vbNewLine & ";11;trRGBPaleYellowFontColor; "
        Else
            strLog = strLog & "Outputs: "
            
            robjDecorationTagValueSettingParts.Add "Outputs;" & vbNewLine & ";11;trRGBPaleYellowFontColor; "
        End If
    
        i = 1
        For Each varTag In .OutputFieldTitlesCol
        
            strLog = strLog & varTag
            
            If i < .OutputFieldTitlesCol.Count Then
            
                strLog = strLog & ", "
            End If
            
            i = i + 1
        Next
    End With

    mfstrGetLogTextFromUnitTestSheetFormatSettingByStruct = strLog
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterOfUnitTest(ByVal vobjInputFieldTitleToColumnWidthDic As Scripting.Dictionary, _
        ByVal vobjOutputFieldTitleToColumnWidthDic As Scripting.Dictionary) As DataTableSheetFormatter

    
    Dim objDataTableSheetFormatter As DataTableSheetFormatter
    
    Dim objFormatConditionExpander As FormatConditionExpander, varOutputTitle As Variant, strValidityOutputTitle As String
    
    
    Set objDataTableSheetFormatter = New DataTableSheetFormatter
    
    With objDataTableSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        
        .RecordBordersType = RecordCellsGrayAll
        
        .RecordsFontSize = 9
        
        Set .FieldTitleToColumnWidthDic = mfobjGetFieldTitleToColumnWidthDicForWrappedUnitTest(vobjInputFieldTitleToColumnWidthDic, vobjOutputFieldTitleToColumnWidthDic)
    
        Set .ColumnsFormatCondition = New ColumnsFormatConditionParam
        
        With .ColumnsFormatCondition
        
            Set objFormatConditionExpander = New FormatConditionExpander
                
            With objFormatConditionExpander
            
                .AddTextContainCondition "TRUE", FontDeepGreenBgLightGreen
                
                .AddTextContainCondition "FALSE", FontDeepRedBgLightRed
            End With
        
            For Each varOutputTitle In vobjOutputFieldTitleToColumnWidthDic.Keys
            
                strValidityOutputTitle = varOutputTitle & "Validity"
            
                .FieldTitleToCellFormatCondition.Add strValidityOutputTitle, objFormatConditionExpander
            Next
        End With
    
        .AllowToMergeCellsByContinuousSameValues = True
        
        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("ModuleName,FunctionName")
    End With
    
    Set mfobjGetDataTableSheetFormatterOfUnitTest = objDataTableSheetFormatter
End Function

'''
'''
'''
Private Function mfobjGetFieldTitlesColForWrappedUnitTest(ByVal objInputFieldTitlesCol As Collection, _
        ByVal objOutputFieldTitlesCol As Collection) As Collection

    Dim objCol As Collection, varOutputTitle As Variant, strValidityOutputTitle As String
    
    Set objCol = GetColFromLineDelimitedChar("ModuleName,FunctionName")

    UnionDoubleCollectionsToSingle objCol, objInputFieldTitlesCol
    
    UnionDoubleCollectionsToSingle objCol, objOutputFieldTitlesCol

    For Each varOutputTitle In objOutputFieldTitlesCol
    
        strValidityOutputTitle = varOutputTitle & "Validity"
        
        objCol.Add strValidityOutputTitle
    Next

    Set mfobjGetFieldTitlesColForWrappedUnitTest = objCol
End Function



Private Function mfobjGetFieldTitleToColumnWidthDicForWrappedUnitTest(ByVal vobjInputFieldTitleToColumnWidthDic As Scripting.Dictionary, _
        ByVal vobjOutputFieldTitleToColumnWidthDic As Scripting.Dictionary) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = GetTextToRealNumberDicFromLineDelimitedChar("ModuleName,15,FunctionName,29")
    
    ' GetTextToRealNumberDicFromLineDelimitedChar("ModuleName,15,FunctionName,29,Input,20,RightOutput01,24,Validity01,11")
    
    UnionDoubleDictionariesToSingle objDic, vobjInputFieldTitleToColumnWidthDic

    UnionDoubleDictionariesToSingle objDic, vobjOutputFieldTitleToColumnWidthDic
    
    UnionDoubleDictionariesToSingle objDic, mfobjGetValidityFieldTitleToWidthDic(vobjOutputFieldTitleToColumnWidthDic)
    
    Set mfobjGetFieldTitleToColumnWidthDicForWrappedUnitTest = objDic
End Function

'''
'''
'''
Private Function mfobjGetValidityFieldTitleToWidthDic(ByVal vobjOutputFieldTitleToColumnWidthDic As Scripting.Dictionary) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, varKey As Variant, strFieldTitle As String
    
    Set objDic = New Scripting.Dictionary
    
    With vobjOutputFieldTitleToColumnWidthDic
    
        For Each varKey In .Keys
        
            strFieldTitle = varKey & "Validity"
        
            objDic.Add strFieldTitle, CDbl(11)
        Next
    End With

    Set mfobjGetValidityFieldTitleToWidthDic = objDic
End Function


