Attribute VB_Name = "UTfCurrentLocalize"
'
'   Design simply to localize text with dependent on ADO
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on Excel for viewing the original key-value sheet
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat,  5/Aug/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToUTfCurrentLocalize()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", _
            "CurrentLocalize,ADOParameters,ADOConStrToolsForExcelBookBase,LocalizationADOConnector"
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' Open related Key-Value table Excel book for viewing
'''
Private Sub msubSanityTestToOpenStringKeyValuesTableBook()

    Dim strModuleName As String
    
    strModuleName = "UErrorADOSQLForm"

    GetStringKeyValuesTableVbaLocalizationBook strModuleName

    With GetCurrentOfficeFileObject()
    
        On Error Resume Next
    
        Application.Run "ShowIDECodePaneFromModuleNameAndVBProjectObject", _
                strModuleName, _
                .VBProject, _
                .Name
        
        On Error GoTo 0
    End With
End Sub

'''
''' SQL tests
'''
Private Sub msubSanityTestToOutputKeyValue()

    OutputKeyValueTableAbout "UErrorADOSQLForm", GetCurrentUICaptionLanguageType()
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
''' Output test-book by writing SQL results
'''
Private Sub OutputKeyValueTableAbout(ByVal vstrModuleName As String, _
        ByVal venmCaptionLanguageType As CaptionLanguageType)


    Dim strInputPath As String, strSQL As String, objDic As Scripting.Dictionary
    
    Dim objDataTableSheetFormatter As DataTableSheetFormatter, objBook As Excel.Workbook, strBookPath As String, objSheet As Excel.Worksheet
    
    
    strBookPath = GetCurrentBookOutputDir() & "\LocalizationADOConnectorTest.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    Select Case venmCaptionLanguageType
    
        Case CaptionLanguageType.UIDefaultEnglishCaptions
        
            objSheet.Name = "KeyAndEnglishValue"
        
        Case CaptionLanguageType.UIJapaneseCaptions
        
            objSheet.Name = "KeyAndJapaneseValue"
    
        Case Else
        
            objSheet.Name = "KeyValue"
    End Select
    
    
    strInputPath = GetKeyValuesVbaLocalizationBookPathForEachModule(vstrModuleName)

    ' This is a sanity test of [StoreLocalizeKeyToTextIntoDictionaryByLoadingFromDataTable]
    StoreLocalizeKeyToTextIntoDictionaryByLoadingFromDataTable objDic, _
            vstrModuleName, _
            venmCaptionLanguageType


    Set objDataTableSheetFormatter = New DataTableSheetFormatter
    
    With objDataTableSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        
        .RecordBordersType = RecordCellsBlacksAndGrayInsideHorizontal
        
        .RecordsFontSize = 9
        
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("StringKey,74,JapaneseText,40,EnglishText,40")
    End With


    OutputDicToSheet objSheet, _
            objDic, _
            GetColFromLineDelimitedChar("StringKey," & GetLocalizingColumnNameByCaptionLanguageType(venmCaptionLanguageType)), _
            objDataTableSheetFormatter
    
    DeleteDummySheetWhenItExists objBook
End Sub

'''
''' Open the Excel book which include the base Key-Value table
'''
Private Function GetStringKeyValuesTableVbaLocalizationBook(ByVal vstrModuleName As String) As Excel.Workbook

    Dim objBook As Excel.Workbook, strPath As String
    
    Set objBook = Nothing
    
    strPath = GetKeyValuesVbaLocalizationBookPathForEachModule(vstrModuleName)
    
    If strPath <> "" Then
    
        Set objBook = GetWorkbook(strPath)
    End If

    Set GetStringKeyValuesTableVbaLocalizationBook = objBook
End Function

