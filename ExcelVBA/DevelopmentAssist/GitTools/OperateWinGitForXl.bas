Attribute VB_Name = "OperateWinGitForXl"
'
'   Utilities for the installed 'Git for Windows'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and 'Git for Windows'
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  1/Jun/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About git repository individual files
'**---------------------------------------------
'''
'''
'''
Public Sub OutputGitRegisteredFilesToSheet(ByVal vstrGitRepositoryDirectoryPath As String, ByVal vstrLogOutputBookPath As String)

    Dim objFiles As Collection, objLogDic As Scripting.Dictionary, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    Dim objDTFormatter As DataTableSheetFormatter
    

    With New DoubleStopWatch
    
        .MeasureStart
    
        Set objFiles = GetGitRegisteredFiles(vstrGitRepositoryDirectoryPath)
    
        If objFiles Is Nothing Then
        
            ' There is no git traced files
        
            Set objFiles = New Collection
        End If
    
        .MeasureInterval
    
        Set objLogDic = New Scripting.Dictionary
        
        With objLogDic
        
            .Add "Git repository root directory path", vstrGitRepositoryDirectoryPath
        
            .Add "Count of the Git managed files", objFiles.Count
        End With
    
        objLogDic.Add "Collecting info elapsed time", .ElapsedTimeByString
    End With

    Set objBook = GetWorkbookAndPrepareForUnitTest(vstrLogOutputBookPath)

    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet.Name = "GitRepositoryFiles"

    Set objDTFormatter = New DataTableSheetFormatter

    With objDTFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTableByOrangeTone
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FileName,27")
    
        .RecordBordersType = RecordCellsGrayAll
    End With

    OutputColToSheet objSheet, objFiles, GetColFromLineDelimitedChar("FileName"), objDTFormatter
     
    OutputDicToAutoShapeLogTextOnSheet objSheet, objLogDic, "A Git local repository files"
    
    DeleteDummySheetWhenItExists objBook
End Sub

'**---------------------------------------------
'** About git configure informations
'**---------------------------------------------
'''
'''
'''
''' <Argument>objInfoTypeToGitCommandDic: Input</Argument>
''' <Argument>vstrGitRepositoryDirectoryPath: Input</Argument>
''' <Argument>vstrLogOutputBookPath: Input</Argument>
Public Sub OutputGitConfigParametersToSheet(ByVal objInfoTypeToGitCommandDic As Scripting.Dictionary, _
        ByVal vstrGitRepositoryDirectoryPath As String, _
        ByVal vstrLogOutputBookPath As String)

    Dim objBook As Excel.Workbook, varInfoType As Variant, strCommand As String, objSheet As Excel.Worksheet
    
    Dim objParameterDic As Scripting.Dictionary, objLogDic As Scripting.Dictionary
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(vstrLogOutputBookPath)

    With objInfoTypeToGitCommandDic

        For Each varInfoType In .Keys
        
            strCommand = .Item(varInfoType)
        
            ' use cme.exe by using WshShell
            Set objParameterDic = GetParameterDicFromGitConfig(strCommand, vstrGitRepositoryDirectoryPath)
        
            Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
        
            objSheet.Name = varInfoType
        
            OutputDicToSheet objSheet, objParameterDic, GetColFromLineDelimitedChar("ParameterName,ParameterValue"), mfobjGetDTFormatterOfGitParameters()
        
        
            Set objLogDic = New Scripting.Dictionary
            
            With objLogDic
            
                .Add "Repository directory path", vstrGitRepositoryDirectoryPath
            
                .Add "Config information", varInfoType
            
                .Add "Git command", strCommand
            End With
        
            OutputDicToAutoShapeLogTextOnSheet objSheet, objLogDic, "Git config command log", XlShapeTextLogInteriorFillDefault
            
            OutputThisComputerGeneralInfoToAutoShapeLogTextOnSheet objSheet
        Next
    End With

    DeleteDummySheetWhenItExists objBook
End Sub

'''
'''
'''
Public Function GetGitConfigLogBookRecommendedPath(ByVal vstrGitLogBookName As String) As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\GitLogs"

    ForceToCreateDirectory strDir

    GetGitConfigLogBookRecommendedPath = strDir & "\" & vstrGitLogBookName
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetDTFormatterOfGitParameters() As DataTableSheetFormatter

    Dim objDTFormatter As DataTableSheetFormatter
    
    Set objDTFormatter = New DataTableSheetFormatter
    
    With objDTFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQL
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("ParameterName,23,ParameterValue,30")
    
        .RecordBordersType = RecordCellsBlacksAndGrayInsideHorizontal
    End With

    Set mfobjGetDTFormatterOfGitParameters = objDTFormatter
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' only both git --global and --system
'''
Public Sub OutputGitConfigParametersForOnlyGlobalAndSystemToSheet()

    Dim strOutputLogBookPath As String, objInfoTypeToGitCommandDic As Scripting.Dictionary
    
    strOutputLogBookPath = GetGitConfigLogBookRecommendedPath("TestGitRepositoryConfigs.xlsx")

    Set objInfoTypeToGitCommandDic = New Scripting.Dictionary

    With objInfoTypeToGitCommandDic
    
        .Add "GlobalList", "git config --global --list"
    
        .Add "SystemList", "git config --system --list"
    End With

    OutputGitConfigParametersToSheet objInfoTypeToGitCommandDic, GetTmpTestingVirtualGitRepositoryRootPathOriginal(), strOutputLogBookPath
End Sub

'''
'''
'''
Public Sub OutputGitConfigAllParametersToSheet(ByVal vstrGitRepositoryPath As String, _
        Optional ByVal vstrBookFileName As String = "GitRepositoryConfig.xlsx")

    Dim strOutputLogBookPath As String, objInfoTypeToGitCommandDic As Scripting.Dictionary
    
    strOutputLogBookPath = GetGitConfigLogBookRecommendedPath(vstrBookFileName)

    Set objInfoTypeToGitCommandDic = New Scripting.Dictionary

    With objInfoTypeToGitCommandDic
    
        .Add "LocalList", "git config --local --list"
        
        .Add "GlobalList", "git config --global --list"
    
        .Add "SystemList", "git config --system --list"
    End With

    OutputGitConfigParametersToSheet objInfoTypeToGitCommandDic, vstrGitRepositoryPath, strOutputLogBookPath
End Sub


'''
'''
'''
Public Sub msubSanityTestToOutputGitConfigParametersToSheet()

    OutputGitConfigAllParametersToSheet GetTmpTestingVirtualGitRepositoryRootPathOriginal(), _
            "TestGitRepositoryConfigs.xlsx"
End Sub

