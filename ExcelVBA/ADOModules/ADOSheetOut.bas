Attribute VB_Name = "ADOSheetOut"
'
'   create a SQL result Excel sheet by using ADODB.Recordset
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 10/May/2022    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "ADOSheetOut"

'**---------------------------------------------
'** Key-Value cache preparation for ADOSheetOut
'**---------------------------------------------
Private mobjStrKeyValueADOSheetOutDic As Scripting.Dictionary

Private mblnIsStrKeyValueADOSheetOutDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToADOOut()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "ADOLogTextSheetOut,UTfDataTableSheetOut,UTfDataTableTextOut,DataTableSheetFormatter"
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Output Recordset data-table to Cells on sheet
'**---------------------------------------------
'''
''' output RecordSet object to worksheet (new-type), <CQRS: Query method>
'''
Public Function OutputRecordsetToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        ByRef robjSQLRes As SQLResult, _
        ByVal vobjADOSheetFormatter As ADOSheetFormatter, _
        ByRef renmRdbConnectionInformationFlag As RdbConnectionInformationFlag, _
        Optional ByVal vblnAllowToGetDataTableSheetRangeAddresses As Boolean = True) As DataTableSheetRangeAddresses

    Dim intColumnsCountOfRecordset As Long, intRowsCountOfRecordset As Long, objDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, objFieldTitlesCol As Collection
    
    
    With vobjADOSheetFormatter
    
        .SetSheetFormatBeforeTableOut (Not robjSQLRes Is Nothing)
        
        .PreparationForRecordSet vobjSheet, vobjRSet, robjSQLRes
    
        intColumnsCountOfRecordset = GetFieldTitlesCountAfterOutputRecordsetFieldTitleToCellsOnSheet(vobjSheet, vobjRSet, vobjADOSheetFormatter)
    
        intRowsCountOfRecordset = OutputOnlyRecordsetDataTableToCellsOnSheet(vobjSheet, vobjRSet, robjSQLRes, vobjADOSheetFormatter, mfblnIsAvoidingToUseCopyFromRecordsetNeeded(renmRdbConnectionInformationFlag))

        If vblnAllowToGetDataTableSheetRangeAddresses Then
        
            Set objDataTableSheetRangeAddresses = GetDataTableSheetRangeAddressesFromDTIndexesAndFieldsCountAndDTFormatter(vobjSheet, intRowsCountOfRecordset, intColumnsCountOfRecordset, intColumnsCountOfRecordset, vobjADOSheetFormatter.DataTableSheetFormattingInterface)
        End If

        .SetSheetFormatAfterTableOut vobjSheet
        
        .ResultOutForAdoSqlExecution vobjSheet, robjSQLRes, RdbSqlQueryType, objDataTableSheetRangeAddresses
    End With
    
    Set OutputRecordsetToCellsOnSheet = objDataTableSheetRangeAddresses
End Function

'''
'''
'''
Private Function mfblnIsAvoidingToUseCopyFromRecordsetNeeded(ByRef renmRdbConnectionInformationFlag As RdbConnectionInformationFlag) As Boolean

    Dim blnAllowToAvoidToUseCopyFromRecordsetMethod As Boolean
    
    
    blnAllowToAvoidToUseCopyFromRecordsetMethod = False
    
    If (renmRdbConnectionInformationFlag And RdbConnectionInformationFlag.AdoOdbcToSqLiteRdbFlag) = RdbConnectionInformationFlag.AdoOdbcToSqLiteRdbFlag Then
    
        ' For SQLite database, SQL "SELECT name FROM sqlite_master WHERE type = 'table'" doesn't work when the Range.CopyFromRecordset is used.
    
        blnAllowToAvoidToUseCopyFromRecordsetMethod = True
    End If
    
    mfblnIsAvoidingToUseCopyFromRecordsetNeeded = blnAllowToAvoidToUseCopyFromRecordsetMethod
End Function

'''
'''
'''
Public Function GetFieldTitlesCountAfterOutputRecordsetFieldTitleToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, ByVal vobjRSet As ADODB.Recordset, ByVal vobjADOSheetFormatter As ADOSheetFormatter) As Long

    Dim intColumnsCountOfRecordset As Long

    intColumnsCountOfRecordset = vobjRSet.Fields.Count
    
    With vobjADOSheetFormatter
    
        If .AllowToShowFieldTitle Then
                
            OutputRecordsetFieldTitleToCellsOnSheet vobjSheet, vobjRSet, .FieldTitlesRowIndex, .TopLeftColumnIndex
        End If
    End With
    
    GetFieldTitlesCountAfterOutputRecordsetFieldTitleToCellsOnSheet = intColumnsCountOfRecordset
End Function

'''
''' data-table sheet out with Recordset object
'''
Public Function OutputOnlyRecordsetDataTableToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        ByRef robjSQLRes As SQLResult, _
        ByVal vobjADOSheetFormatter As ADOSheetFormatter, _
        Optional ByVal vblnAllowToAvoidToUseCopyFromRecordsetMethod As Boolean = False) As Long


    Dim intColumnMax As Long, objRange As Excel.Range, objTopLeftDataTableRange As Excel.Range, intRowsCountOfRecords As Long

    Dim objDummyDTCol As Collection


    intRowsCountOfRecords = 0
    
    With vobjSheet
    
        With vobjADOSheetFormatter
        
            Set objTopLeftDataTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex + 1))
        End With
        
        If vobjRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
        
            If vblnAllowToAvoidToUseCopyFromRecordsetMethod Then
            
                If Not vobjRSet.BOF Then
                
                    vobjRSet.MoveFirst
                End If
                
                intRowsCountOfRecords = MoveNextNormalCopyFromRecordSet(objDummyDTCol, objTopLeftDataTableRange, vobjRSet)
            Else
                intColumnMax = vobjRSet.Fields.Count
                
                ' ordinary-code, which use the Excel.Range.CopyFromRecordset method

                With vobjADOSheetFormatter

                    objTopLeftDataTableRange.CopyFromRecordset vobjRSet

                    With vobjSheet.Range(ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex + 1)).CurrentRegion

                        intRowsCountOfRecords = .Rows.Count + .Row - 1 - vobjADOSheetFormatter.FieldTitlesRowIndex
                    End With
                End With
            End If

            If Not robjSQLRes Is Nothing Then
            
                If Not vobjADOSheetFormatter.SupportedPropertyRecordCount Then
                
                    robjSQLRes.TableRecordCount = intRowsCountOfRecords
                End If
            Else
                Debug.Print "[ADOSheetOut.bas] OutputOnlyRecordsetDataTableToCellsOnSheet, the robjSQLRes is Nothing"
            End If
        End If
    End With
    
    OutputOnlyRecordsetDataTableToCellsOnSheet = intRowsCountOfRecords
End Function

'''
'''
'''
Private Function mfintGetEstimatedRowsCountOfRecordsetByCurrentRegion(ByRef robjSQLRes As SQLResult, _
        ByRef robjSheet As Excel.Worksheet, _
        ByRef robjADOSheetFormatter As ADOSheetFormatter, _
        ByRef robjRSet As ADODB.Recordset)


    Dim intRowsCountOfRecordset As Long, objTopLeftDataTableRange As Excel.Range
    
    With robjADOSheetFormatter
    
        If Not .SupportedPropertyRecordCount Then
            
            If .AllowToShowFieldTitle Then
            
                Set objTopLeftDataTableRange = robjSheet.Range(ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex)).CurrentRegion
            Else
                Set objTopLeftDataTableRange = robjSheet.Range(ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex + 1)).CurrentRegion
            End If
            
            intRowsCountOfRecordset = objTopLeftDataTableRange.Row + objTopLeftDataTableRange.Rows.Count - 1 - robjADOSheetFormatter.FieldTitlesRowIndex
        Else
            intRowsCountOfRecordset = robjRSet.RecordCount
        End If
    End With
    
    If Not robjSQLRes Is Nothing Then
    
        robjSQLRes.TableRecordCount = intRowsCountOfRecordset
    End If

    mfintGetEstimatedRowsCountOfRecordsetByCurrentRegion = intRowsCountOfRecordset
End Function



'''
'''
'''
Public Function OutputRecordsetFieldTitleToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1) As Collection
    
    
    Dim objField As ADOR.Field, objFieldTitlesCol As Collection
    
    Set objFieldTitlesCol = New Collection
    
    For Each objField In vobjRSet.Fields
    
        objFieldTitlesCol.Add objField.Name
    Next

    OutputFieldTitlesToCellsOnSheet vobjSheet, objFieldTitlesCol, vintFieldTitlesRowIndex, vintTopLeftColumnIndex
    
    Set OutputRecordsetFieldTitleToCellsOnSheet = objFieldTitlesCol
End Function



'''
''' Substitute this for Range.CopyFromRecordSet
'''
''' because the CopyFromRecordSet causes often some errors which is hard to understand
'''
''' <Argument>robjOutputDTCol: Input</Argument>
''' <Argument>vobjRecordTopLeftRange: Input-Output</Argument>
''' <Argument>vobjRSet: Input</Argument>
''' <Argument>vstrSuppressingColumnsDelimitedComma: Input</Argument>
Public Function MoveNextNormalCopyFromRecordSet(ByRef robjOutputDTCol As Collection, _
        ByVal vobjRecordTopLeftRange As Excel.Range, _
        ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long


    Dim objSheet As Excel.Worksheet, intRowIdx As Long, intColumnIdx As Long
    
    Dim intRecordTopLeftRowIndex As Long, intRecordTopLeftColumnIndex As Long, intColumnsCountOfDataTable As Long
    
    Dim i As Long
    
    Dim objDTCol As Collection
    
    Dim objUnionRange As Excel.Range, intRowsCountOfRecordset As Long
    
    
    Set objSheet = vobjRecordTopLeftRange.Worksheet
    
    
    intRowIdx = intRecordTopLeftRowIndex
    
    With vobjRecordTopLeftRange
    
        intRecordTopLeftRowIndex = .Row
        
        intRecordTopLeftColumnIndex = .Column
    End With
    
    
    intColumnsCountOfDataTable = vobjRSet.Fields.Count
    
    NormalConvertRSetToCollectionAndGetStringRanges objDTCol, _
            objUnionRange, _
            vobjRSet, _
            objSheet, _
            intRecordTopLeftRowIndex, _
            intRecordTopLeftColumnIndex, _
            vstrSuppressingColumnsDelimitedComma
    
    Set robjOutputDTCol = objDTCol
    
    On Error Resume Next

    If Not objUnionRange Is Nothing Then
    
        objUnionRange.NumberFormatLocal = "@"
    End If
    
    msubNormalOutputDTCol objDTCol, intColumnsCountOfDataTable, objSheet, intRecordTopLeftRowIndex, intRecordTopLeftColumnIndex
    
    intRowsCountOfRecordset = objDTCol.Count
    
    On Error GoTo 0
    
    MoveNextNormalCopyFromRecordSet = intRowsCountOfRecordset
End Function

'''
''' because the CopyFromRecordSet causes often some errors which is hard to understand
'''
Public Function MoveNextNormalCopyFromDTCol(ByVal vobjRecordTopLeftRange As Excel.Range, _
        ByVal vobjInputDTCol As Collection, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long


    Dim objSheet As Excel.Worksheet, intRowIdx As Long, intColumnIdx As Long
    Dim intRecordTopLeftRowIndex As Long, intRecordTopLeftColumnIndex As Long, intColumnsCountOfDataTable As Long
    Dim i As Long
    
    Dim objOutputDTCol As Collection
    Dim objUnionRange As Excel.Range
    Dim intRowsCountOfRecordset As Long
    
    Set objSheet = vobjRecordTopLeftRange.Worksheet
    
    
    intRowIdx = intRecordTopLeftRowIndex
    
    With vobjRecordTopLeftRange
    
        intRecordTopLeftRowIndex = .Row
        
        intRecordTopLeftColumnIndex = .Column
    End With
    
    
    
    
    NormalConvertDTColToCollectionAndGetStringRanges objOutputDTCol, objUnionRange, vobjInputDTCol, objSheet, intRecordTopLeftRowIndex, intRecordTopLeftColumnIndex, vstrSuppressingColumnsDelimitedComma
    
    On Error Resume Next

    If Not objUnionRange Is Nothing Then
    
        objUnionRange.NumberFormatLocal = "@"
    End If
    
    intColumnsCountOfDataTable = objOutputDTCol.Item(1).Count
    
    msubNormalOutputDTCol objOutputDTCol, intColumnsCountOfDataTable, objSheet, intRecordTopLeftRowIndex, intRecordTopLeftColumnIndex
    
    intRowsCountOfRecordset = objOutputDTCol.Count
    
    On Error GoTo 0
    
    MoveNextNormalCopyFromDTCol = intRowsCountOfRecordset
End Function


'''
'''
'''
Private Sub msubNormalOutputDTCol(ByVal vobjDTCol As Collection, _
        ByVal vintColumnsCountOfDataTable As Long, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintRecordTopLeftRowIndex As Long = 1, _
        Optional ByVal vintRecordTopLeftColumnIndex As Long = 1)

    Dim objRange As Excel.Range, varValues As Variant

    Set objRange = vobjSheet.Cells(vintRecordTopLeftRowIndex, vintRecordTopLeftColumnIndex)
    
    GetRCValuesFromRCCollection varValues, vobjDTCol
    
    objRange.Resize(vobjDTCol.Count, vintColumnsCountOfDataTable).Value = varValues
End Sub


'''
'''
'''
''' <Argument>robjOutputDTCol: Output</Argument>
''' <Argument>robjStringUnionRange: Output</Argument>
''' <Argument>robjInputDTCol: Input</Argument>
''' <Argument>robjSheet: Input</Argument>
''' <Argument>rintRecordTopLeftRowIndex: Input</Argument>
''' <Argument>rintRecordTopLeftColumnIndex: Input</Argument>
''' <Argument>vstrSuppressingColumnsDelimitedComma: Input</Argument>
Public Sub NormalConvertDTColToCollectionAndGetStringRanges(ByRef robjOutputDTCol As Collection, _
        ByRef robjStringUnionRange As Excel.Range, _
        ByRef robjInputDTCol As Collection, _
        ByRef robjSheet As Excel.Worksheet, _
        ByRef rintRecordTopLeftRowIndex As Long, _
        ByRef rintRecordTopLeftColumnIndex As Long, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")
        
    On Error Resume Next
    
    Dim i As Long, objRowCol As Collection, varInputRowCol As Variant, objInputRowCol As Collection, varInputItem As Variant
    Dim intColumnIdx As Long, intRowIdx As Long, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
    
    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
    
    
    intRowIdx = rintRecordTopLeftRowIndex
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    
    Set robjOutputDTCol = New Collection
    
    Set robjStringUnionRange = Nothing
    
    For Each varInputRowCol In robjInputDTCol
    
        Set objRowCol = New Collection
    
        intColumnDecrement = 0
    
        i = 0
    
        Set objInputRowCol = varInputRowCol
    
        For Each varInputItem In objInputRowCol
        
            intColumnIdx = rintRecordTopLeftColumnIndex + i
        
            blnContinueToAddValue = True
            
            If vstrSuppressingColumnsDelimitedComma <> "" Then
            
                If objSuppressingColumnsDic.Exists(i + 1) Then
                
                    blnContinueToAddValue = False
                
                    intColumnDecrement = intColumnDecrement + 1
                End If
            End If
        
            If blnContinueToAddValue Then
            
                SetNormalRowColValueAndStringUnionRange robjSheet, _
                        objRowCol, _
                        varInputItem, _
                        robjStringUnionRange, _
                        intRowIdx, _
                        intColumnIdx, _
                        intColumnDecrement
            End If
            
            i = i + 1
        Next
        
        robjOutputDTCol.Add objRowCol
        
        intRowIdx = intRowIdx + 1
    Next

            
    On Error GoTo 0
End Sub

'''
'''
'''
Public Sub NormalConvertRSetToCollectionAndGetStringRanges(ByRef robjDTCol As Collection, _
        ByRef robjStringUnionRange As Excel.Range, _
        ByRef robjRSet As ADODB.Recordset, _
        ByRef robjSheet As Excel.Worksheet, _
        ByRef rintRecordTopLeftRowIndex As Long, _
        ByRef rintRecordTopLeftColumnIndex As Long, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")
        
    
    On Error Resume Next
    
    
    Dim objField As ADOR.Field, i As Long, objRowCol As Collection
    
    Dim intColumnIdx As Long, intRowIdx As Long, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
    
    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
    
    
    intRowIdx = rintRecordTopLeftRowIndex
    
    intColumnDecrement = 0
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    
    Set robjDTCol = New Collection
    
    Set robjStringUnionRange = Nothing
    
    With robjRSet
    
        While Not .EOF
        
            Set objRowCol = New Collection
        
            i = 0
        
            For Each objField In .Fields
            
                intColumnIdx = rintRecordTopLeftColumnIndex + i
            
                blnContinueToAddValue = True
                
                If vstrSuppressingColumnsDelimitedComma <> "" Then
                
                    If objSuppressingColumnsDic.Exists(intColumnIdx) Then
                    
                        blnContinueToAddValue = False
                    
                        intColumnDecrement = intColumnDecrement + 1
                    End If
                End If
            
                If blnContinueToAddValue Then
                
                    With objField
                    
                        SetNormalRowColValueAndStringUnionRange robjSheet, _
                                objRowCol, _
                                .Value, _
                                robjStringUnionRange, _
                                intRowIdx, _
                                intColumnIdx, _
                                intColumnDecrement
                    End With
                End If
                
                i = i + 1
            Next
            
            robjDTCol.Add objRowCol
            
            intRowIdx = intRowIdx + 1
            
            .MoveNext
        Wend
    End With
            
    On Error GoTo 0
End Sub

'''
'''
'''
''' <Argument>robjSheet: Input</Argument>
''' <Argument>robjRowCol: Input-Output</Argument>
''' <Argument>rvarValue: Input</Argument>
''' <Argument>robjStringUnionRange: Output</Argument>
''' <Argument>rintRowIdx: Input</Argument>
''' <Argument>rintColumnIdx: Input</Argument>
''' <Argument>rintColumnDecrement: Input</Argument>
Public Sub SetNormalRowColValueAndStringUnionRange(ByRef robjSheet As Excel.Worksheet, _
        ByRef robjRowCol As Collection, _
        ByRef rvarValue As Variant, _
        ByRef robjStringUnionRange As Excel.Range, _
        ByRef rintRowIdx As Long, _
        ByRef rintColumnIdx As Long, _
        ByRef rintColumnDecrement As Long)


    robjRowCol.Add rvarValue
                    
    If IsNumeric(rvarValue) Then
        
        If TypeName(rvarValue) = "String" Then
            
            If StrComp(Left(rvarValue, 1), "0") = 0 Then
            
                If robjStringUnionRange Is Nothing Then
                    
                    Set robjStringUnionRange = robjSheet.Cells(rintRowIdx, rintColumnIdx - rintColumnDecrement)
                Else
                    Set robjStringUnionRange = Union(robjStringUnionRange, robjSheet.Cells(rintRowIdx, rintColumnIdx - rintColumnDecrement))
                End If
            End If
        End If
    End If
End Sub


'''
'''
'''
''' <Argument>robjSuppressingColumnsDic: Output</Argument>
''' <Argument>vstrSuppressingColumnsDelimitedComma: Input</Argument>
Public Sub SetUpSuppressingColumnsDic(ByRef robjSuppressingColumnsDic As Scripting.Dictionary, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")

    Dim varColumn As Variant

    If vstrSuppressingColumnsDelimitedComma <> "" Then
    
        Set robjSuppressingColumnsDic = New Scripting.Dictionary
    
        For Each varColumn In Split(vstrSuppressingColumnsDelimitedComma, ",")
        
            If IsNumeric(varColumn) Then
            
                robjSuppressingColumnsDic.Add CLng(varColumn), Null
            End If
        Next
    End If
End Sub


'**---------------------------------------------
'** Key-Value cache preparation for ADOSheetOut
'**---------------------------------------------
'''
''' get string Value from STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutSqlExecutionElapsedTime() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutSqlExecutionElapsedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_EXCEL_SHEET_PROCESSING_ELAPSED_TIME
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutExcelSheetProcessingElapsedTime() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutExcelSheetProcessingElapsedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_EXCEL_SHEET_PROCESSING_ELAPSED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_TABLE_RECORD_COUNT
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutTableRecordCount() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutTableRecordCount = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_TABLE_RECORD_COUNT")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_TABLE_FIELD_COUNT
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutTableFieldCount() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutTableFieldCount = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_TABLE_FIELD_COUNT")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_SQL_EXECUTED_TIME
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutSqlExecutedTime() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutSqlExecutedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_SQL_EXECUTED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_UNIT_SECOND
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutUnitSecond() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutUnitSecond = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_UNIT_SECOND")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_RECORDS_AFFECTED
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutRecordsAffected() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutRecordsAffected = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_RECORDS_AFFECTED")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_DELETE
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutCountOfSqlDelete() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutCountOfSqlDelete = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_DELETE")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_UPDATE
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutCountOfSqlUpdate() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutCountOfSqlUpdate = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_UPDATE")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_INSERT
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutCountOfSqlInsert() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutCountOfSqlInsert = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_INSERT")
End Function


'''
''' get string Value from STR_KEY_ADO_SHEETOUT_CLIENT_USER_NAME
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutClientUserName() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutClientUserName = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_CLIENT_USER_NAME")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_CLIENT_HOST_NAME
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutClientHostName() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutClientHostName = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_CLIENT_HOST_NAME")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_CLIENT_IPV4_ADDRESS
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutClientIpv4Address() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutClientIpv4Address = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_CLIENT_IPV4_ADDRESS")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_CLIENT_MAC_ADDRESS
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutClientMacAddress() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutClientMacAddress = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_CLIENT_MAC_ADDRESS")
End Function


'''
''' get string Value from STR_KEY_ADO_SHEETOUT_DELETING_ELAPSED_TIME
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutDeletingElapsedTime() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutDeletingElapsedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_DELETING_ELAPSED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_UPDATING_ELAPSED_TIME
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutUpdatingElapsedTime() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutUpdatingElapsedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_UPDATING_ELAPSED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_INSERTING_ELAPSED_TIME
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutInsertingElapsedTime() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutInsertingElapsedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_INSERTING_ELAPSED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_SQL_SYNCHRONOUS_EXECUTION_ERROR
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutSqlSynchronousExecutionError() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutSqlSynchronousExecutionError = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_SQL_SYNCHRONOUS_EXECUTION_ERROR")
End Function

'''
''' get string Value from STR_KEY_ADO_SHEETOUT_SQL_ASYNCHRONOUS_EXECUTION_ERROR
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoSheetoutSqlAsynchronousExecutionError() As String

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        msubInitializeTextForADOSheetOut
    End If

    GetTextOfStrKeyAdoSheetoutSqlAsynchronousExecutionError = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_SQL_ASYNCHRONOUS_EXECUTION_ERROR")
End Function


'''
''' Initialize Key-Values which values
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Private Sub msubInitializeTextForADOSheetOut()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForADOSheetOutByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForADOSheetOutByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueADOSheetOutDicInitialized = True
    End If

    Set mobjStrKeyValueADOSheetOutDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for ADOSheetOut key-values cache
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Private Sub AddStringKeyValueForADOSheetOutByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME", "SQL execution elapsed time"
        .Add "STR_KEY_ADO_SHEETOUT_EXCEL_SHEET_PROCESSING_ELAPSED_TIME", "Excel sheet processing elapsed time"
        .Add "STR_KEY_ADO_SHEETOUT_TABLE_RECORD_COUNT", "record count"
        .Add "STR_KEY_ADO_SHEETOUT_TABLE_FIELD_COUNT", "field count"
        .Add "STR_KEY_ADO_SHEETOUT_SQL_EXECUTED_TIME", "SQL executed time"
        .Add "STR_KEY_ADO_SHEETOUT_UNIT_SECOND", "[s]"
        .Add "STR_KEY_ADO_SHEETOUT_RECORDS_AFFECTED", "Records affected"
        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_DELETE", "Count of deleted records"
        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_UPDATE", "Count of updated records"
        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_INSERT", "Count of inserted records"
        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_USER_NAME", "Client-UserName"
        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_HOST_NAME", "Client-HostName"
        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_IPV4_ADDRESS", "Client-IPv4Address"
        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_MAC_ADDRESS", "Client-MACAddress"
        .Add "STR_KEY_ADO_SHEETOUT_DELETING_ELAPSED_TIME", "Deleting elapsed time"
        .Add "STR_KEY_ADO_SHEETOUT_UPDATING_ELAPSED_TIME", "Updating elapsed time"
        .Add "STR_KEY_ADO_SHEETOUT_INSERTING_ELAPSED_TIME", "Inserting elapsed time"
        .Add "STR_KEY_ADO_SHEETOUT_SQL_SYNCHRONOUS_EXECUTION_ERROR", "SQL execution error : Synchronous"
        .Add "STR_KEY_ADO_SHEETOUT_SQL_ASYNCHRONOUS_EXECUTION_ERROR", "SQL execution error : Asynchronous"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for ADOSheetOut key-values cache
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Private Sub AddStringKeyValueForADOSheetOutByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME", "SQL実行経過時間"
        .Add "STR_KEY_ADO_SHEETOUT_EXCEL_SHEET_PROCESSING_ELAPSED_TIME", "Excelシート描画経過時間"
        .Add "STR_KEY_ADO_SHEETOUT_TABLE_RECORD_COUNT", "レコード数"
        .Add "STR_KEY_ADO_SHEETOUT_TABLE_FIELD_COUNT", "列数"
        .Add "STR_KEY_ADO_SHEETOUT_SQL_EXECUTED_TIME", "SQL実行の日付時刻"
        .Add "STR_KEY_ADO_SHEETOUT_UNIT_SECOND", "[秒]"
        .Add "STR_KEY_ADO_SHEETOUT_RECORDS_AFFECTED", "変更のあったレコード数"
        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_DELETE", "データ削除数(DELETE: Records affected)"
        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_UPDATE", "データ更新数(UPDATE: Records affected)"
        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_INSERT", "データ追加数(INSERT: Records affected)"
        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_USER_NAME", "クライアントのユーザ名"
        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_HOST_NAME", "クライアントのホスト名"
        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_IPV4_ADDRESS", "クライアントのIPv4アドレス"
        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_MAC_ADDRESS", "クライアントのMACアドレス"
        .Add "STR_KEY_ADO_SHEETOUT_DELETING_ELAPSED_TIME", "削除経過時間"
        .Add "STR_KEY_ADO_SHEETOUT_UPDATING_ELAPSED_TIME", "更新経過時間"
        .Add "STR_KEY_ADO_SHEETOUT_INSERTING_ELAPSED_TIME", "挿入経過時間"
        .Add "STR_KEY_ADO_SHEETOUT_SQL_SYNCHRONOUS_EXECUTION_ERROR", "SQL実行時エラー : 同期実行"
        .Add "STR_KEY_ADO_SHEETOUT_SQL_ASYNCHRONOUS_EXECUTION_ERROR", "SQL実行時エラー : 非同期実行"
    End With
End Sub

'''
''' Remove Keys for ADOSheetOut key-values cache
'''
''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueADOSheetOut(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME"
            .Remove "STR_KEY_ADO_SHEETOUT_EXCEL_SHEET_PROCESSING_ELAPSED_TIME"
            .Remove "STR_KEY_ADO_SHEETOUT_TABLE_RECORD_COUNT"
            .Remove "STR_KEY_ADO_SHEETOUT_TABLE_FIELD_COUNT"
            .Remove "STR_KEY_ADO_SHEETOUT_SQL_EXECUTED_TIME"
            .Remove "STR_KEY_ADO_SHEETOUT_UNIT_SECOND"
            .Remove "STR_KEY_ADO_SHEETOUT_RECORDS_AFFECTED"
            .Remove "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_DELETE"
            .Remove "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_UPDATE"
            .Remove "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_INSERT"
            .Remove "STR_KEY_ADO_SHEETOUT_CLIENT_USER_NAME"
            .Remove "STR_KEY_ADO_SHEETOUT_CLIENT_HOST_NAME"
            .Remove "STR_KEY_ADO_SHEETOUT_CLIENT_IPV4_ADDRESS"
            .Remove "STR_KEY_ADO_SHEETOUT_CLIENT_MAC_ADDRESS"
            .Remove "STR_KEY_ADO_SHEETOUT_DELETING_ELAPSED_TIME"
            .Remove "STR_KEY_ADO_SHEETOUT_UPDATING_ELAPSED_TIME"
            .Remove "STR_KEY_ADO_SHEETOUT_INSERTING_ELAPSED_TIME"
            .Remove "STR_KEY_ADO_SHEETOUT_SQL_SYNCHRONOUS_EXECUTION_ERROR"
            .Remove "STR_KEY_ADO_SHEETOUT_SQL_ASYNCHRONOUS_EXECUTION_ERROR"
        End If
    End With
End Sub


