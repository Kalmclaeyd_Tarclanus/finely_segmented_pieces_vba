VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AdoRSetSheetExpander"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Outputting Excel sheet common operations for SQL result Recordset object
'   Collect the actual method implementations for serveing Excel books with creating its
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IExpandAdoRecordsetOnSheet

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mitfAdoConnector As IADOConnector

Private mitfOutputBookPath As IOutputBookPath

Private mobjADOSheetFormatter As ADOSheetFormatter

'**---------------------------------------------
'** About cache for adding ListObject with naming
'**---------------------------------------------
Private mobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses

'**---------------------------------------------
'** About cache for Unit-test mode Workbook preparetion
'**---------------------------------------------
Private mobjCurrentSheet As Excel.Worksheet

Private mstrCacheOutputExcelBookPath As String

Private menmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag

Private mblnIsCacheMemoriedOfOutputtedExcelBook As Boolean

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjADOSheetFormatter = New ADOSheetFormatter
    

    Set mobjCurrentSheet = Nothing
    
    mstrCacheOutputExcelBookPath = ""
    
    menmOutputRSetExcelBookOpenModeProcessFlag = NoProcessAboutOutputExcelBook
    
    mblnIsCacheMemoriedOfOutputtedExcelBook = False
End Sub

Private Sub Class_Terminate()

    IExpandAdoRecordsetOnSheet_CloseCacheOutputBookOfOpenByPresetModeProcess
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get ExpandAdoRecordsetOnSheetInterface() As IExpandAdoRecordsetOnSheet

    Set ExpandAdoRecordsetOnSheetInterface = Me
End Property

'///////////////////////////////////////////////
'/// Implemented properties
'///////////////////////////////////////////////
Public Property Set IExpandAdoRecordsetOnSheet_CurrentSheet(ByVal vobjSheet As Excel.Worksheet)

    Set mobjCurrentSheet = vobjSheet
End Property
Public Property Get IExpandAdoRecordsetOnSheet_CurrentSheet() As Excel.Worksheet

    Set IExpandAdoRecordsetOnSheet_CurrentSheet = mobjCurrentSheet
End Property

'''
'''
'''
Public Property Let IExpandAdoRecordsetOnSheet_CacheOutputExcelBookPath(ByVal vstrCacheOutputExcelBookPath As String)

    mstrCacheOutputExcelBookPath = vstrCacheOutputExcelBookPath
End Property
Public Property Get IExpandAdoRecordsetOnSheet_CacheOutputExcelBookPath() As String

    IExpandAdoRecordsetOnSheet_CacheOutputExcelBookPath = mstrCacheOutputExcelBookPath
End Property

Public Property Let IExpandAdoRecordsetOnSheet_OutputRSetExcelBookOpenModeProcess(ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag)

    menmOutputRSetExcelBookOpenModeProcessFlag = venmOutputRSetExcelBookOpenModeProcessFlag
End Property
Public Property Get IExpandAdoRecordsetOnSheet_OutputRSetExcelBookOpenModeProcess() As OutputRSetExcelBookOpenModeProcessFlag

    IExpandAdoRecordsetOnSheet_OutputRSetExcelBookOpenModeProcess = menmOutputRSetExcelBookOpenModeProcessFlag
End Property

Public Property Let IExpandAdoRecordsetOnSheet_IsCacheMemoriedOfOutputtedExcelBook(ByVal vblnIsCacheMemoriedOfOutputtedExcelBook As Boolean)

    mblnIsCacheMemoriedOfOutputtedExcelBook = vblnIsCacheMemoriedOfOutputtedExcelBook
End Property
Public Property Get IExpandAdoRecordsetOnSheet_IsCacheMemoriedOfOutputtedExcelBook() As Boolean

    IExpandAdoRecordsetOnSheet_IsCacheMemoriedOfOutputtedExcelBook = mblnIsCacheMemoriedOfOutputtedExcelBook
End Property


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
''' Reference of an interface
'''
Public Property Get ADOConnectorInterface() As IADOConnector

    Set ADOConnectorInterface = mitfAdoConnector
End Property
Public Property Set ADOConnectorInterface(ByVal vitfAdoConnector As IADOConnector)

    Set mitfAdoConnector = vitfAdoConnector
End Property

'''
''' Reference of an interface
'''
Public Property Get OutputBookPathInterface() As IOutputBookPath

    Set OutputBookPathInterface = mitfOutputBookPath
End Property
Public Property Set OutputBookPathInterface(ByVal vitfOutputBookPath As IOutputBookPath)

    Set mitfOutputBookPath = vitfOutputBookPath
End Property

'''
''' old: ADOSheetFormatting
'''
Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter

    Set DataTableSheetFormattingInterface = mobjADOSheetFormatter.DataTableSheetFormattingInterface
End Property

'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
'''
'''
'''
Private Sub IExpandAdoRecordsetOnSheet_CloseCacheOutputBookOfOpenByPresetModeProcess()

    Dim objOutputBook As Excel.Workbook

    If menmOutputRSetExcelBookOpenModeProcessFlag <> OutputRSetExcelBookOpenModeProcessFlag.NoProcessAboutOutputExcelBook Then
        
        If Not mobjCurrentSheet Is Nothing Then
        
            Set objOutputBook = mobjCurrentSheet.Parent
            
            If Not objOutputBook Is Nothing Then
            
                DoAfterProcessingToOutputBook objOutputBook, menmOutputRSetExcelBookOpenModeProcessFlag
            End If
            
            Set mobjCurrentSheet = Nothing
        End If
    End If
    
    mblnIsCacheMemoriedOfOutputtedExcelBook = False
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()

    IExpandAdoRecordsetOnSheet_CloseCacheOutputBookOfOpenByPresetModeProcess
End Sub


'**---------------------------------------------
'** about query SQL and expand results into Excel.Worksheet
'**---------------------------------------------
'''
''' query SQL and output result to Excel sheet
'''
Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
        ByVal vobjOutputSheet As Excel.Worksheet, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
        
        
    Dim objRSetAfterExpanded As ADODB.Recordset


    OutputSqlQueryResultsToSheetFromByIAdoConnector objRSetAfterExpanded, _
            mobjDataTableSheetRangeAddresses, _
            mitfAdoConnector, _
            mobjADOSheetFormatter, _
            vstrSQL, _
            vobjOutputSheet, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            vobjHeaderInsertTexts, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible

    Set mobjDataTableSheetRangeAddresses = mobjDataTableSheetRangeAddresses

    Set mobjCurrentSheet = vobjOutputSheet
    
    ' Close ADODB.Recordset exactly
    
    If Not objRSetAfterExpanded Is Nothing Then
    
        With objRSetAfterExpanded
        
            If .State <> ADODB.ObjectStateEnum.adStateClosed Then
        
                ' For PostgreSQL ODBC driver, Oracle ODBC driver, Access OLE DB provider, the following is not needed. The ADODB.Recordset is closed automatically when the ADODB.Connection has been closed
        
                ' But the SQLite3 ODBC driver needs the following at the 2023 Aug.
                
                .Close
            End If
        End With
    End If
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    Dim objNewSheet As Excel.Worksheet
    
    Set objNewSheet = GetNewWorksheetAfterAllExistedSheetsAndFindNewSheetName(vstrOutputBookPath, vstrNewSheetName)
    
    ' Debug code
'    With New Scripting.FileSystemObject

'        CurrentProcessInfoFromThisApplication objNewSheet.Parent, True, vstrNewSheetName & "_", .GetParentFolderName(GetCurrentOfficeFileObject().FullName)
'    End With
    
    Me.OutputToSheetFrom vstrSQL, objNewSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the Existed Excel sheet
'''
Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    Me.OutputToSpecifiedBook vstrSQL, mitfOutputBookPath.GetPreservedOutputBookPath(), vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the already specified Excel sheet
'''
Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjInsertTexts As Collection = Nothing, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    SetupFirstCacheOfIAdoRecordsetSheetExpander Me
     
    OutputToSpecifiedBook vstrSQL, Me.ExpandAdoRecordsetOnSheetInterface.CacheOutputExcelBookPath(), vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
'''
'''
Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjInsertTexts As Collection = Nothing, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)


    With Me.ExpandAdoRecordsetOnSheetInterface
    
        .CacheOutputExcelBookPath = mitfOutputBookPath.GetPreservedOutputBookPath()
    End With

    OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
'''
'''
Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjInsertTexts As Collection = Nothing, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)


    With Me.ExpandAdoRecordsetOnSheetInterface
    
        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
    
        .CacheOutputExcelBookPath = vstrOutputBookPath
    End With
    
    Me.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
'''
'''
Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjInsertTexts As Collection = Nothing, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)


    With Me.ExpandAdoRecordsetOnSheetInterface
    
        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
    End With
    
    Me.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'**---------------------------------------------
'** about command SQL
'**---------------------------------------------
'''
''' execute SQL command (UPDATE, INSERT, DELTE) and output result log to Excel sheet
'''
Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
        ByVal vobjOutputSheet As Excel.Worksheet, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    OutputSqlCommandLogToCellsOnSheetFromByIADOConnector mitfAdoConnector, mobjADOSheetFormatter, vstrSQL, vobjOutputSheet, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    With mobjADOSheetFormatter
    
        If vstrNewSqlCommandLogSheetName <> "" Then
        
            .ExecutedAdoSqlCommandLogPosition = AppendingSqlCommandLogsInSheetCellsOnSpecifiedSheet
        Else
            .ExecutedAdoSqlCommandLogPosition = AppendingSqlCommandLogsInSheetCellsOnFixedIndependentSheet
        End If
    End With
    
    Me.OutputCommandLogToSheetFrom vstrSQL, _
            GetRdbSqlCommandLogWorksheet(vstrOutputBookPath, vstrNewSqlCommandLogSheetName, mobjADOSheetFormatter), _
            venmShowUpAdoErrorOptionFlag, _
            vobjHeaderInsertTexts, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    SetupFirstCacheOfIAdoRecordsetSheetExpander Me
    
    OutputCommandLogToSpecifiedBook vstrSQL, _
            Me.ExpandAdoRecordsetOnSheetInterface.CacheOutputExcelBookPath(), _
            venmShowUpAdoErrorOptionFlag, _
            vobjHeaderInsertTexts, _
            vstrNewSqlCommandLogSheetName, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    With Me.ExpandAdoRecordsetOnSheetInterface
    
        .CacheOutputExcelBookPath = mitfOutputBookPath.GetPreservedOutputBookPath()
    End With
    
    OutputCommandLogToAlreadySpecifiedBook vstrSQL, _
            venmShowUpAdoErrorOptionFlag, _
            vobjHeaderInsertTexts, _
            vstrNewSqlCommandLogSheetName, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    With Me.ExpandAdoRecordsetOnSheetInterface
    
        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
    
        .CacheOutputExcelBookPath = vstrOutputBookPath
    End With
    
    
    OutputCommandLogToAlreadySpecifiedBook vstrSQL, _
            venmShowUpAdoErrorOptionFlag, _
            vobjHeaderInsertTexts, _
            vstrNewSqlCommandLogSheetName, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    With Me.ExpandAdoRecordsetOnSheetInterface
    
        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
    End With
    
    
    OutputCommandLogInAlreadyExistedBook vstrSQL, _
            venmShowUpAdoErrorOptionFlag, _
            vobjHeaderInsertTexts, _
            vstrNewSqlCommandLogSheetName, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub



'**---------------------------------------------
'** About outputting Excel book control
'**---------------------------------------------
'''
'''
'''
''' <Argument>vstrOutputBookPath: Input</Argument>
''' <Argument>venmOutputRSetExcelBookOpenModeProcessFlag: Input</Argument>
Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)

    mstrCacheOutputExcelBookPath = vstrOutputBookPath
    
    menmOutputRSetExcelBookOpenModeProcessFlag = venmOutputRSetExcelBookOpenModeProcessFlag
End Sub

