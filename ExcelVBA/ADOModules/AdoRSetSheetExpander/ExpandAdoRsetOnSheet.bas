Attribute VB_Name = "ExpandAdoRsetOnSheet"
'
'   Utilities for expanding ADODB.Recordset into a region on a Excel.Worksheet with creating new sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'       Dependent on IADOConnector.bas, ADOSheetFormatter.cls, DataTableSheetRangeAddresses.cls
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  9/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' Output created Excel-book processing flag
'''
Public Enum OutputRSetExcelBookOpenModeProcessFlag
    
    NoProcessAboutOutputExcelBook = 0
    
    OpenOutputBookOnlyWhenItIsExist = &H1
    
    DeleteAllSheetsBeforeSQLExecution = &H2
    
    DeleteDefaultSheetsAfterSQLExecution = &H4
    
    DeleteDummySheetWhenItExistsAfterSQLExecution = &H8

    
    OpenOutputBookByUnitTestMode = DeleteAllSheetsBeforeSQLExecution Or DeleteDefaultSheetsAfterSQLExecution Or DeleteDummySheetWhenItExistsAfterSQLExecution

    CloseOutputExcelBookAfterSQLExecution = &H10
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Adding name of either ListObject or Range
'**---------------------------------------------
'''
''' This is a complementary function.
'''
''' If your main purpose is that the creating a table-object of SQL queried,
''' you should simply use the Excel.Worksheet.QueryTables.Add(ConnectionString, LeftTopRange, SQL) method
'''
''' <Argument>vstrTableObjectName: Input</Argument>
''' <Argument>vobjSheet: Input-Output</Argument>
''' <Argument>vobjDataTableSheetRangeAddresses: Input</Argument>
Public Sub AddTableObjectWithNameOfExpandedRecordsetArea(ByVal vstrTableObjectName As String, _
        ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses)

    Dim objTableRange As Excel.Range

    With vobjDataTableSheetRangeAddresses
    
        If .FieldTitlesRangeAddress = "" Then
        
            ' This means that AllowToShowFieldTitle = False
        
            With vobjSheet.ListObjects.Add(xlSrcRange, vobjSheet.Range(.RecordAreaRangeAddress), XlListObjectHasHeaders:=xlNo, TableStyleName:="")
            
                .Name = vstrTableObjectName
            End With
        Else
            Set objTableRange = Union(vobjSheet.Range(.FieldTitlesRangeAddress), vobjSheet.Range(.RecordAreaRangeAddress))
    
            With vobjSheet.ListObjects.Add(xlSrcRange, objTableRange, XlListObjectHasHeaders:=xlYes, TableStyleName:="")
            
                .Name = vstrTableObjectName
            End With
        End If
    End With
End Sub

'''
''' Use CurrentRegion
'''
''' <Argument>vstrTableObjectName: Input</Argument>
''' <Argument>vobjTopLeftRange: Input-Output</Argument>
Public Sub AddTableObjectWithoutTableStyle(ByVal vstrTableObjectName As String, _
        ByVal vobjTopLeftRange As Excel.Range)


    Dim objTableRange As Excel.Range, objSheet As Excel.Worksheet
    
    Set objTableRange = vobjTopLeftRange.CurrentRegion

    Set objSheet = objTableRange.Worksheet
    
    With objSheet.ListObjects.Add(xlSrcRange, objTableRange, XlListObjectHasHeaders:=xlYes, TableStyleName:="")

        .Name = vstrTableObjectName
    End With
End Sub


'''
''' Add a named range to Sheet
'''
''' <Argument>vstrName: Input</Argument>
''' <Argument>vobjSheet: Input-Output</Argument>
''' <Argument>vobjDataTableSheetRangeAddresses: Input</Argument>
Public Sub AddNamedRangeOfExpandedRecordsetArea(ByVal vstrName As String, _
        ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses)

    Dim objTableRange As Excel.Range

    With vobjDataTableSheetRangeAddresses
    
        If .FieldTitlesRangeAddress = "" Then
        
            ' This means that AllowToShowFieldTitle = False
        
            With vobjSheet.Range(.RecordAreaRangeAddress)
            
                .Name = vstrName
            End With
        Else
            Set objTableRange = Union(vobjSheet.Range(.FieldTitlesRangeAddress), vobjSheet.Range(.RecordAreaRangeAddress))
    
            With objTableRange
            
                .Name = vstrName
            End With
        End If
    End With
End Sub

'''
''' get SQL Command log Excel.Worksheet using an ADOSheetFormatter object
'''
''' <Argument>rstrOutputSqlCommandLogBookPath: Input</Argument>
''' <Argument>rstrSheetName: Input</Argument>
''' <Argument>robjADOSheetFormatter: Input</Argument>
Public Function GetRdbSqlCommandLogWorksheet(ByRef rstrOutputSqlCommandLogBookPath As String, _
        ByRef rstrSheetName As String, _
        ByRef robjADOSheetFormatter As ADOSheetFormatter) As Excel.Worksheet


    Dim objSheet As Excel.Worksheet, objBook As Excel.Workbook, strSqlSheetName As String
    
    
    Set objBook = GetWorkbook(rstrOutputSqlCommandLogBookPath)

    With robjADOSheetFormatter
    
        Select Case .ExecutedAdoSqlCommandLogPosition
        
            Case ExecutedAdoSqlCommandLogPositionType.AppendingSqlCommandLogsInSheetCellsOnSpecifiedSheet
        
                strSqlSheetName = rstrSheetName
        
            Case ExecutedAdoSqlCommandLogPositionType.AppendingSqlCommandLogsInSheetCellsOnFixedIndependentSheet
                
                strSqlSheetName = .ExclusiveSQLLogSheetName
        End Select
    End With
    
    ' The data-table sheet should be placed after the SQL log sheet
     
    Set objSheet = GetSheetWithFindingNewNameWhenUsedRangeRowsCountExceedsLimit(strSqlSheetName, objBook)

    Set GetRdbSqlCommandLogWorksheet = objSheet
End Function


'**---------------------------------------------
'** Basic expanding
'**---------------------------------------------
'''
''' output recordset object to sheet
'''
Public Function OutputRecordsetToCellsOnSheetForSheetExpander(ByVal vobjHeaderInsertTexts As Collection, _
        ByVal vobjADOSheetFormatter As ADOSheetFormatter, _
        ByVal vobjOutputSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        ByVal vobjSQLResult As SQLResult, _
        ByRef renmRdbConnectionInformationFlag As RdbConnectionInformationFlag, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As DataTableSheetRangeAddresses
    
    
    SetSqlQueryLogOptionAndHeaderTextOptionForSheetExpander (Not vobjSQLResult Is Nothing), venmExecutedAdoSqlQueryLogPositionType, vobjHeaderInsertTexts, vobjADOSheetFormatter
    
    Set OutputRecordsetToCellsOnSheetForSheetExpander = OutputRecordsetToCellsOnSheet(vobjOutputSheet, vobjRSet, vobjSQLResult, vobjADOSheetFormatter, renmRdbConnectionInformationFlag)
End Function


'''
''' query SQL and output result to Excel sheet, by interface IADOConnector object
'''
''' <Argument>robjRSetAfterExpanded: Output</Argument>
''' <Argument>robjDataTableSheetRangeAddresses: Output</Argument>
''' <Argument>ritfIAdoConnector: Input</Argument>
''' <Argument>vobjADOSheetFormatter: Input</Argument>
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>vobjOutputSheet: Input</Argument>
''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vobjHeaderInsertTexts: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input, this is used only when the this code is executed from either Word or PowerPoint</Argument>
Public Sub OutputSqlQueryResultsToSheetFromByIAdoConnector(ByRef robjRSetAfterExpanded As ADODB.Recordset, _
        ByRef robjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
        ByRef ritfIAdoConnector As IADOConnector, _
        ByVal vobjADOSheetFormatter As ADOSheetFormatter, _
        ByVal vstrSQL As String, _
        ByVal vobjOutputSheet As Excel.Worksheet, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    Dim objRSet As ADODB.Recordset

    Set objRSet = ritfIAdoConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
    
    If ritfIAdoConnector.IsConnected Then
    
        Set robjDataTableSheetRangeAddresses = OutputRecordsetToCellsOnSheetForSheetExpander(vobjHeaderInsertTexts, vobjADOSheetFormatter, vobjOutputSheet, objRSet, ritfIAdoConnector.SQLExecutionResult, ritfIAdoConnector.RdbConnectionInformation, venmExecutedAdoSqlQueryLogPositionType)
        
        If vblnChangeVisibleExcelApplicationIfItIsImvisible Then
            
            If Not vobjOutputSheet.Application.Visible Then
        
                ' The following is executed only when the ADO sheet-expander is used from either Word application or PowerPoint application
        
                vobjOutputSheet.Application.Visible = True
            End If
        End If
        
        If Not objRSet Is Nothing Then
        
            If objRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
        
                If Not objRSet.BOF Then
                    
                    objRSet.MoveFirst
                End If
                
                Set robjRSetAfterExpanded = objRSet
            End If
        End If
    Else
        Debug.Print "Failed to open ADODB.Connection -  the current ADODB.Connection.State is adStateClosed (is not connected.)."
    End If
End Sub

'''
'''
'''
Public Sub SetSqlQueryLogOptionAndHeaderTextOptionForSheetExpander(ByVal vblnAllowToRecordSQLLog As Boolean, _
        ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType, _
        ByVal vobjHeaderInsertTexts As Collection, _
        ByRef robjADOSheetFormatter As ADOSheetFormatter)

    With robjADOSheetFormatter
    
        If .ExecutedAdoSqlQueryLogPosition <> venmExecutedAdoSqlQueryLogPositionType Then
        
            .ExecutedAdoSqlQueryLogPosition = venmExecutedAdoSqlQueryLogPositionType
        End If
    End With
    
    SetSqlQueryAndCommandLogOptionAndHeaderTextOptionForSheetExpander vblnAllowToRecordSQLLog, vobjHeaderInsertTexts, robjADOSheetFormatter
End Sub


'''
'''
'''
Public Sub SetSqlQueryAndCommandLogOptionAndHeaderTextOptionForSheetExpander(ByVal vblnAllowToRecordSQLLog As Boolean, _
        ByVal vobjHeaderInsertTexts As Collection, _
        ByRef robjADOSheetFormatter As ADOSheetFormatter)

    With robjADOSheetFormatter
    
        If vblnAllowToRecordSQLLog Then
        
            .AllowToWriteInsertTextInSheetHeader = True
            
            If Not vobjHeaderInsertTexts Is Nothing Then
            
                If .HeaderInsertTexts Is Nothing Then
                
                    If Not .HeaderInsertTexts Is vobjHeaderInsertTexts Then
                    
                        Set .HeaderInsertTexts = vobjHeaderInsertTexts
                    End If
                End If
            End If
        Else
            .AllowToWriteInsertTextInSheetHeader = False
        End If
    End With
End Sub

'**---------------------------------------------
'** About SQL query Recordset sheet expander operations
'**---------------------------------------------
'''
'''
'''
Public Sub SetupFirstCacheOfIAdoRecordsetSheetExpander(ByRef ritfAdoRecordsetSheetExpander As IExpandAdoRecordsetOnSheet)

    Dim objLoadedOrCreatedOutputBook As Excel.Workbook

    With ritfAdoRecordsetSheetExpander
    
        If Not .IsCacheMemoriedOfOutputtedExcelBook Then
        
            Set objLoadedOrCreatedOutputBook = GetOutputBookByPresetOpenModeOfSomeTypes(.CacheOutputExcelBookPath, .OutputRSetExcelBookOpenModeProcess)
    
            .IsCacheMemoriedOfOutputtedExcelBook = True
        End If
    End With
End Sub


'**---------------------------------------------
'** About SQL command sheet expander operations
'**---------------------------------------------
'''
''' execute SQL command (UPDATE, INSERT, DELTE) and output result log to Excel sheet
'''
Public Sub OutputSqlCommandLogToCellsOnSheetFromByIADOConnector(ByRef ritfADOConnector As IADOConnector, _
        ByRef robjADOSheetFormatter As ADOSheetFormatter, _
        ByVal vstrSQL As String, _
        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    Dim objRSet As ADODB.Recordset
    
    With ritfADOConnector
    
        If .IsConnected Then
        
            Set objRSet = .ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
            
            SetSqlQueryAndCommandLogOptionAndHeaderTextOptionForSheetExpander (Not .SQLExecutionResult Is Nothing), vobjHeaderInsertTexts, robjADOSheetFormatter
            
            ' SQL command log
            
            OutputSQLBodyTextAndDetailAllLogToCellsOnSheet vobjOutputSqlCommandLogSheet, .SQLExecutionResult, robjADOSheetFormatter
            
            If vblnChangeVisibleExcelApplicationIfItIsImvisible Then
                
                If Not vobjOutputSqlCommandLogSheet.Application.Visible Then
            
                    ' The following is executed only when the ADO sheet-expander is used from either Word application or PowerPoint application
            
                    vobjOutputSqlCommandLogSheet.Application.Visible = True
                End If
            End If
            
            If Not objRSet Is Nothing Then
            
                If objRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
                
                    objRSet.Close
                End If
            End If
        Else
            Debug.Print "[ADOConnecto...] ADO.Connection object isn't connected."
        End If
    End With
End Sub


'**---------------------------------------------
'** Preprocess or Postprocess operations for AdoSheetExpander
'**---------------------------------------------
'''
''' Pre-processing based on the OutputRSetExcelBookOpenModeProcessFlag value
'''
Public Function GetOutputBookByPresetOpenModeOfSomeTypes(ByVal vstrExcelOutputBookPath As String, _
        ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag) As Excel.Workbook
    
    Dim objOutputBook As Excel.Workbook
    
    Set objOutputBook = Nothing
    
    If vstrExcelOutputBookPath <> "" Then
        
        If (venmOutputRSetExcelBookOpenModeProcessFlag And OpenOutputBookByUnitTestMode) = OpenOutputBookByUnitTestMode Then
        
            Set objOutputBook = GetWorkbookAndPrepareForUnitTest(vstrExcelOutputBookPath)
        
        ElseIf (venmOutputRSetExcelBookOpenModeProcessFlag And OpenOutputBookOnlyWhenItIsExist) > 0 Then
        
            With New Scripting.FileSystemObject
            
                If .FileExists(vstrExcelOutputBookPath) Then
                
                    Set objOutputBook = GetWorkbookIfItExists(vstrExcelOutputBookPath)
                Else
                    ' get New Workbook
                    
                    Set objOutputBook = GetNewWorksheetAfterAllExistedSheetsFromBook(ThisWorkbook)
                End If
            End With
        Else
            Set objOutputBook = GetWorkbook(vstrExcelOutputBookPath)
        End If
    
        If (venmOutputRSetExcelBookOpenModeProcessFlag And DeleteAllSheetsBeforeSQLExecution) > 0 Then
        
            DeleteAllSheetsWithoutDummySheetButWhenTheDummySheetDoesntExistThenItIsAdded objOutputBook
        End If
    End If
    
    Set GetOutputBookByPresetOpenModeOfSomeTypes = objOutputBook
End Function

'''
''' Post-processing based on the OutputRSetExcelBookOpenModeProcessFlag value
'''
Public Sub DoAfterProcessingToOutputBook(ByVal vobjOutputBook As Excel.Workbook, _
        ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag)

    If (venmOutputRSetExcelBookOpenModeProcessFlag And DeleteDummySheetWhenItExistsAfterSQLExecution) > 0 Then
    
        DeleteDummySheetWhenItExists vobjOutputBook
    End If

    If (venmOutputRSetExcelBookOpenModeProcessFlag And DeleteAllSheetsBeforeSQLExecution) = 0 Then
    
        If (venmOutputRSetExcelBookOpenModeProcessFlag And DeleteDefaultSheetsAfterSQLExecution) > 0 Then
            
            DeleteDefaultSheet vobjOutputBook
        End If
    End If

    If (venmOutputRSetExcelBookOpenModeProcessFlag And CloseOutputExcelBookAfterSQLExecution) > 0 Then
    
        CloseBookIfItIsOpened vobjOutputBook
    End If
End Sub


