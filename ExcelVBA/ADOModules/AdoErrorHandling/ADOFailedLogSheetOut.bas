Attribute VB_Name = "ADOFailedLogSheetOut"
'
'   Generate ADO error logs on cells of the specified Excel sheet,
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both the ADO and Excel.Application
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private Const mstrModuleName As String = "ADOFailedLogSheetOut"

'**---------------------------------------------
'** Key-Value cache preparation for ADOFailedLogSheetOut
'**---------------------------------------------
Private mobjStrKeyValueADOFailedLogSheetOutDic As Scripting.Dictionary

Private mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** clear default log-Excel book
'**---------------------------------------------
Public Sub DeleteErrorLogForDefaultErrorLogBookPath()

    With New ErrorLogSheetLocator
        
        .DeleteErrorLogsForDefaultPathBook
    End With
End Sub

'**---------------------------------------------
'** call-back interface for outputting ADO failed executed SQL detail error-logs
'**---------------------------------------------
'''
'''
'''
Public Sub GenerateADOSQLExecutionErrorLogSheet(ByRef robjErrorADOSQL As ErrorADOSQL)

    Dim objLogSheet As Excel.Worksheet, objErrorLogSheetLocator As ErrorLogSheetLocator

    Set objErrorLogSheetLocator = New ErrorLogSheetLocator

    Set objLogSheet = objErrorLogSheetLocator.GetToLogSheetByDefaultSetting()

    OutputAdoFailedExecutedSqlErrorObjectToSheetWithAdjustingLogginPosition objLogSheet, objErrorLogSheetLocator.DefaultErrorLogSheetName, robjErrorADOSQL
End Sub


'**---------------------------------------------
'** Output error log to Excel.Worksheet
'**---------------------------------------------
'''
''' Output ADO-SQL error summary to sheet
'''
Public Sub OutputAdoFailedExecutedSqlErrorObjectToSheetWithAdjustingLogginPosition(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vstrBaseSheetName As String, _
        ByRef robjErrorADOSQL As ErrorADOSQL)
        
    Dim objBook As Excel.Workbook, intBottomRowIndex As Long, objFoundSqlExecutionErrorLogSheet As Excel.Worksheet
    
    
    Set objBook = vobjSheet.Parent
    
    ' The data-table sheet should be placed after the SQL log sheet
    
    FindBottomRowIndexAfterGetSheetWithFindingNewNameWhenUsedRangeRowsCountExceedsLimit objFoundSqlExecutionErrorLogSheet, intBottomRowIndex, vstrBaseSheetName, objBook, 500
        
    OutputAdoFailedExecutedSqlErrorObjectToSheet objFoundSqlExecutionErrorLogSheet, robjErrorADOSQL, intBottomRowIndex + 1, 1
    
    WriteTopAndBottomBoundaryBorderLineOfSpecifiedRow vobjSheet, intBottomRowIndex + 1
    
    ShowBottomUsedRangeOfWorksheet vobjSheet
End Sub

'''
''' Output ADO-SQL error summary to sheet
'''
Public Sub OutputAdoFailedExecutedSqlErrorObjectToSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjErrorADOSQL As ErrorADOSQL, _
        Optional ByVal vintTopLeftRowIndex As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long)


    ' About SQL
    OutputUsedActualSQLToSpecifiedRangeOnExcelSheet vobjErrorADOSQL.ADOSQLResult.SQL, vobjSheet.Cells(vintTopLeftRowIndex, vintTopLeftColumnIndex), CellsInteriorOfSQLExecutionErrorOccured
    
    ' Logging
    With vobjSheet
    
        OutputSqlCommandDetailLogToCellsOnSheet vobjSheet, vobjErrorADOSQL.ADOSQLResult, vintTopLeftRowIndex + 1, vintTopLeftColumnIndex, True, CellsInteriorOfSQLExecutionErrorOccured
    
        OutputADOConnectionParametersLogToSheet vobjSheet, vobjErrorADOSQL.LoadedConnectionSetting, .Cells(vintTopLeftRowIndex + 3, vintTopLeftColumnIndex), False
        
        OutputCurrentUserDomainLogToSheet vobjSheet, vobjErrorADOSQL.ADOSQLResult.CurrentUserInfo, .Cells(vintTopLeftRowIndex + 3, vintTopLeftColumnIndex + 3), SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec
        
        OutputAdoSqlExecutedDetailErrorContentsToCellsOnSheet vobjSheet, vobjErrorADOSQL, vobjSheet.Cells(vintTopLeftRowIndex + 8, vintTopLeftColumnIndex), CellsInteriorOfSQLExecutionErrorOccured
    
    
        msubAdjustColumnWidthForAdoSqlFailedExecutedSqlSheet vobjSheet, vintTopLeftColumnIndex
    End With
End Sub

'''
'''
'''
Private Sub msubAdjustColumnWidthForAdoSqlFailedExecutedSqlSheet(ByRef robjSheet As Excel.Worksheet, Optional ByVal vintTopLeftColumnIndex As Long = 1)

    Dim strColumnLetter As String, dblColumnWidth As Double

    With robjSheet
    
        strColumnLetter = ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex): dblColumnWidth = 20.5
    
        With .Range(strColumnLetter & ":" & strColumnLetter).EntireColumn
        
            If .ColumnWidth <> dblColumnWidth Then
                
                .ColumnWidth = dblColumnWidth
            End If
        End With
        
        strColumnLetter = ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex + 1): dblColumnWidth = 25
        
        With .Range(strColumnLetter & ":" & strColumnLetter).EntireColumn
            
            If .ColumnWidth <> dblColumnWidth Then
                
                .ColumnWidth = dblColumnWidth
            End If
        End With
    End With
End Sub


'''
'''
'''
Public Sub OutputAdoSqlExecutedDetailErrorContentsToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjErrorADOSQL As ErrorADOSQL, _
        ByVal vobjStartingRange As Excel.Range, _
        Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)


    Dim intTopRowIndex As Long, intLeftColumnIndex As Long, varValues() As Variant, objRange As Excel.Range
    
    With vobjStartingRange
    
        intTopRowIndex = .Row
        
        intLeftColumnIndex = .Column
    End With

    ReDim varValues(1 To 3, 1 To 2)

    varValues(1, 1) = GetTextOfStrKeyAdoFailedLogSheetoutErrorDescription()
    
    varValues(2, 1) = GetTextOfStrKeyAdoFailedLogSheetoutErrorSource()
    
    varValues(3, 1) = GetTextOfStrKeyAdoFailedLogSheetoutErrorNumber()
    
    With vobjErrorADOSQL
    
        varValues(1, 2) = .ErrorDescription
        
        varValues(2, 2) = .ErrorSource
        
        varValues(3, 2) = .ErrorNumber
    End With

    vobjSheet.Cells(intTopRowIndex, intLeftColumnIndex).Resize(3, 2).Value = varValues

    With vobjSheet
    
        Select Case venmLogTitleInterior
            
            Case SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured
                
                Set objRange = .Range(ConvertXlColumnIndexToLetter(intLeftColumnIndex) & CStr(intTopRowIndex) & ":" & ConvertXlColumnIndexToLetter(intLeftColumnIndex) & CStr(intTopRowIndex + 2))
                
                DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSQLExecutionErrorOccured
                
                Set objRange = .Range(ConvertXlColumnIndexToLetter(intLeftColumnIndex + 1) & CStr(intTopRowIndex) & ":" & ConvertXlColumnIndexToLetter(intLeftColumnIndex + 1) & CStr(intTopRowIndex + 2))
                
                DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSynchronizedSQLExec
            
            Case Else
                
                Set objRange = .Range(ConvertXlColumnIndexToLetter(intLeftColumnIndex) & CStr(intTopRowIndex) & ":" & ConvertXlColumnIndexToLetter(intLeftColumnIndex + 1) & CStr(intTopRowIndex + 2))
                
                DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, venmLogTitleInterior
        End Select
        
        DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog objRange
    
        Set objRange = .Range(ConvertXlColumnIndexToLetter(intLeftColumnIndex) & CStr(intTopRowIndex) & ":" & ConvertXlColumnIndexToLetter(intLeftColumnIndex) & CStr(intTopRowIndex + 2))
        
        DecorateSheetCellsInteriorForSQLQueryAndCommandLog objRange, venmLogTitleInterior
    End With
End Sub


'**---------------------------------------------
'** Key-Value cache preparation for ADOFailedLogSheetOut
'**---------------------------------------------
'''
''' get string Value from STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION
'''
''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedLogSheetoutErrorDescription() As String

    If Not mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized Then

        msubInitializeTextForADOFailedLogSheetOut
    End If

    GetTextOfStrKeyAdoFailedLogSheetoutErrorDescription = mobjStrKeyValueADOFailedLogSheetOutDic.Item("STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_SOURCE
'''
''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedLogSheetoutErrorSource() As String

    If Not mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized Then

        msubInitializeTextForADOFailedLogSheetOut
    End If

    GetTextOfStrKeyAdoFailedLogSheetoutErrorSource = mobjStrKeyValueADOFailedLogSheetOutDic.Item("STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_SOURCE")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_NUMBER
'''
''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedLogSheetoutErrorNumber() As String

    If Not mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized Then

        msubInitializeTextForADOFailedLogSheetOut
    End If

    GetTextOfStrKeyAdoFailedLogSheetoutErrorNumber = mobjStrKeyValueADOFailedLogSheetOutDic.Item("STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_NUMBER")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
Private Sub msubInitializeTextForADOFailedLogSheetOut()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForADOFailedLogSheetOutByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForADOFailedLogSheetOutByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized = True
    End If

    Set mobjStrKeyValueADOFailedLogSheetOutDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for ADOFailedLogSheetOut key-values cache
'''
''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
Private Sub AddStringKeyValueForADOFailedLogSheetOutByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION", "Error Description"
        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_SOURCE", "Error Source"
        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_NUMBER", "Error Number"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for ADOFailedLogSheetOut key-values cache
'''
''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
Private Sub AddStringKeyValueForADOFailedLogSheetOutByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION", "エラーの内容"
        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_SOURCE", "エラー元"
        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_NUMBER", "エラー番号"
    End With
End Sub

'''
''' Remove Keys for ADOFailedLogSheetOut key-values cache
'''
''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueADOFailedLogSheetOut(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION"
            .Remove "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_SOURCE"
            .Remove "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_NUMBER"
        End If
    End With
End Sub

