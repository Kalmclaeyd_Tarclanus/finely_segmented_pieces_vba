VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ErrorLogSheetLocator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Independent on ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mstrErrorBookPath As String

Private mobjToLogSheet As Excel.Worksheet

Private mblnAllowToAddLogToOutputingSheet As Boolean

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mstrErrorBookPath = ""

    mblnAllowToAddLogToOutputingSheet = True
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Get AllowToAddLogToOutputingSheet() As Boolean

    AllowToAddLogToOutputingSheet = mblnAllowToAddLogToOutputingSheet
End Property
Public Property Let AllowToAddLogToOutputingSheet(ByVal vblnAllowToAddLogToOutputingSheet As Boolean)

    mblnAllowToAddLogToOutputingSheet = vblnAllowToAddLogToOutputingSheet
End Property

'''
''' read-only error log output sheet in order to write log
'''
Public Property Get ToLogSheet() As Excel.Worksheet

    Set ToLogSheet = mobjToLogSheet
End Property

'''
''' read-only error-log default Excel sheet name
'''
Public Property Get DefaultErrorLogSheetName() As String

    DefaultErrorLogSheetName = mfstrGetDefaultSheetName()
End Property

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetToLogSheetByDefaultSetting() As Excel.Worksheet
    
    Dim objBook As Excel.Workbook
    
    With New Scripting.FileSystemObject
    
        If .FileExists(mfstrGetDefaultFilePath()) Then
        
            Set objBook = GetWorkbook(mfstrGetDefaultFilePath(), False)
            
            With objBook.Worksheets
            
                Set mobjToLogSheet = .Item(.Count)
            End With
        Else
            SetDefaultErrorLog
        End If
    End With

    Set GetToLogSheetByDefaultSetting = mobjToLogSheet
End Function


'''
'''
'''
Public Sub SetDefaultErrorLog()

    Dim objLogSheet As Excel.Worksheet

    Set objLogSheet = GetNewWorksheetAfterAllExistedSheets(mfstrGetDefaultFilePath())

    With objLogSheet
    
        .Name = FindNewSheetNameWhenAlreadyExist(mfstrGetDefaultSheetName(), GetWorkbookIfItExists(mfstrGetDefaultFilePath()))
    
        DeleteDefaultSheet .Parent
        
        DeleteDummySheetWhenItExists .Parent
    End With

    Set mobjToLogSheet = objLogSheet
End Sub

'''
'''
'''
Public Sub DeleteErrorLogsForDefaultPathBook()

    Dim objBook As Excel.Workbook

    Set objBook = GetWorkbook(mfstrGetDefaultFilePath())

    DeleteSheetsWithoutDummySheet objBook
    
    SetDefaultErrorLog
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrGetDefaultDirectoryPath() As String
    
    Dim strMyDocumentsDir As String
    
    'With New IWshRuntimeLibrary.WshShell
    
    With CreateObject("WScript.Shell")
    
        strMyDocumentsDir = .SpecialFolders("MyDocuments")
    End With
    
    mfstrGetDefaultDirectoryPath = strMyDocumentsDir
End Function

'''
''' Excel book file name
'''
Private Function mfstrGetDefaultFileName() As String

    mfstrGetDefaultFileName = "ErrLog.xlsx"
End Function

'''
''' error log file path
'''
Private Function mfstrGetDefaultFilePath() As String

    mfstrGetDefaultFilePath = mfstrGetDefaultDirectoryPath() & "\" & mfstrGetDefaultFileName
End Function


'''
''' sheet name
'''
Private Function mfstrGetDefaultSheetName() As String

    mfstrGetDefaultSheetName = "ErrLog"
End Function
