VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOConStrOfAceOleDbXl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO connection string generator for Access OLEDB connection with Excel.Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       ADOConnector focus on the visualization of both the ADO error-trapping and SQL data-base error-trapping
'       LozalizationADOConnector doesn't support the error trapping visualization , logging of SQL results, and localizing captions of itself
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
'       which should have been the installed when the Microsoft Office Excel software has been installed.
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  9/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IADOConnectStrGenerator

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjCoreGenerator As ADOConStrOfAceOleDb


Private menmAccessOLEDBProviderType As AccessOLEDBProviderType

Private menmExcelBookSpecType As ExcelBookSpecType

Private menmIMEXMode As IMEXMode

Private mblnHeaderFieldTitleExists As Boolean

Private mblnFileReadonly As Boolean

Private mblnForceToUseAceOLEDBMacro As Boolean


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjCoreGenerator = New ADOConStrOfAceOleDb
    
    mblnForceToUseAceOLEDBMacro = False
End Sub


'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
Private Function IADOConnectStrGenerator_GetConnectionString() As String

    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForXlBook()
End Function

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** OLE DB connection
'**---------------------------------------------
'''
''' read-only Provider Name for OLE DB
'''
Public Property Get ProviderName() As String

    ProviderName = mobjCoreGenerator.OleDbProviderName
End Property

'**---------------------------------------------
'** Excel-book connection
'**---------------------------------------------
'''
''' read-only Excel book path or ACCESS accdb path
'''
Public Property Get ConnectingFilePath() As String

    ConnectingFilePath = mobjCoreGenerator.ConnectingFilePath
End Property

Public Property Get BookReadOnly() As Boolean

    BookReadOnly = mblnFileReadonly
End Property

Public Property Get HeaderFieldTitleExists() As Boolean

    HeaderFieldTitleExists = mblnHeaderFieldTitleExists
End Property

Public Property Get IMEX() As IMEXMode

    IMEX = menmIMEXMode
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetConnectionString() As String
 
    GetConnectionString = IADOConnectStrGenerator_GetConnectionString
End Function

'''
'''
'''
Public Function GetConnectionStringForXlBook() As String

    GetConnectionStringForXlBook = GetADODBConnectionOleDbStringToXlBook(mobjCoreGenerator.ConnectingFilePath, menmAccessOLEDBProviderType, menmExcelBookSpecType, mblnHeaderFieldTitleExists, mblnFileReadonly, menmIMEXMode)
End Function


'**---------------------------------------------
'** Set ADODB Access OLE DB parameters directly
'**---------------------------------------------
'''
'''
'''
Public Sub SetExcelBookConnection(ByVal vstrBookPath As String, _
        ByVal venmIMEXMode As IMEXMode, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal vblnBookReadOnly As Boolean = False, _
        Optional ByVal vblnIsConnectedToOffice95 As Boolean = False)


    mobjCoreGenerator.ConnectingFilePath = vstrBookPath
    
    If mblnForceToUseAceOLEDBMacro Then
    
        menmAccessOLEDBProviderType = AceOLEDB_12P0 ' for Excel2007 or lator
        
        menmExcelBookSpecType = XlBookFileType12P0Macro
    Else
        Select Case LCase(GetExtensionNameByVbaDir(vstrBookPath))
        
            Case "xls"
            
                If vblnIsConnectedToOffice95 Then
                
                    menmAccessOLEDBProviderType = JetOLEDB_4P0 ' for Excel97
                    
                    menmExcelBookSpecType = XlBookFileType5P0   ' for Excel95
                Else
                    menmAccessOLEDBProviderType = JetOLEDB_4P0
                    
                    menmExcelBookSpecType = XlBookFileType8P0   ' for Excel97, Excel2000, Excel2002
                End If
                
            Case "xlsx" ' book xml
            
                menmAccessOLEDBProviderType = AceOLEDB_12P0 ' for Excel2007 or lator
                
                menmExcelBookSpecType = XlBookFileType12P0Xml
                
            Case "xlsb" ' book binary
            
                menmAccessOLEDBProviderType = AceOLEDB_12P0 ' for Excel2007 or lator
                
                menmExcelBookSpecType = XlBookFileType12P0B
                
            Case "xlsm" ' book macro
            
                menmAccessOLEDBProviderType = AceOLEDB_12P0 ' for Excel2007 or lator
                
                menmExcelBookSpecType = XlBookFileType12P0Macro
            Case Else
            
                Debug.Print "!!! Atention - The Excel book might not be saved as a new file yet"
            
                menmAccessOLEDBProviderType = AceOLEDB_12P0 ' for Excel2007 or lator
                
                menmExcelBookSpecType = XlBookFileType12P0Xml
        End Select
    End If
    
    mobjCoreGenerator.OleDbProviderName = GetAccessOLEDBProviderDescriptionFromEnm(menmAccessOLEDBProviderType)
    
    menmIMEXMode = venmIMEXMode
    
    mblnHeaderFieldTitleExists = vblnHeaderFieldTitleExists
    
    mblnFileReadonly = vblnBookReadOnly
End Sub


'''
'''
'''
Public Sub SetExcelBookConnectionInDetails(ByVal vstrBookPath As String, _
        ByVal venmIMEXMode As IMEXMode, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal vblnBookReadOnly As Boolean = False, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vblnIsConnectedToOffice95 As Boolean = False)

    With mobjCoreGenerator
    
        .ConnectingFilePath = vstrBookPath
    
        menmAccessOLEDBProviderType = venmAccessOLEDBProviderType
    
        .OleDbProviderName = GetAccessOLEDBProviderDescriptionFromEnm(menmAccessOLEDBProviderType)
    End With

    Select Case LCase(GetExtensionNameByVbaDir(vstrBookPath))
    
        Case "xls"
        
            If vblnIsConnectedToOffice95 Then
            
                menmExcelBookSpecType = XlBookFileType5P0   ' for Excel95
            Else
                menmExcelBookSpecType = XlBookFileType8P0   ' for Excel97, Excel2000, Excel2002
            End If
            
        Case "xlsx" ' book xml
        
            menmExcelBookSpecType = XlBookFileType12P0Xml
            
        Case "xlsb" ' book binary
        
            menmExcelBookSpecType = XlBookFileType12P0B
            
        Case "xlsm" ' book macro
        
            menmExcelBookSpecType = XlBookFileType12P0Macro
        Case Else
        
        
    End Select

    menmIMEXMode = venmIMEXMode
    
    mblnHeaderFieldTitleExists = vblnHeaderFieldTitleExists
    
    mblnFileReadonly = vblnBookReadOnly
End Sub


'**---------------------------------------------
'** By a User-form, set ADODB ODBC parameters
'**---------------------------------------------
'''
'''
'''
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrBookPath: Input</Argument>
''' <Argument>venmIMEXMode: Input</Argument>
''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
''' <Argument>vblnBookReadOnly: Input</Argument>
''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
''' <Argument>vstrBookLockPassword: Input</Argument>
''' <Return>Boolean</Return>
Public Function IsAdoConnectionContinuedAfterAccessOleDbToExcelSheetConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrBookPath As String = "", _
        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal vblnBookReadOnly As Boolean = False, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vstrBookLockPassword As String = "") As Boolean

    Dim blnIsRequestedToCancelAdoConnection As Boolean
    
    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmAccessOLEDBProviderType, vstrBookLockPassword, True
    
    IsAdoConnectionContinuedAfterAccessOleDbToExcelSheetConnectionParametersByForm = Not blnIsRequestedToCancelAdoConnection
End Function

