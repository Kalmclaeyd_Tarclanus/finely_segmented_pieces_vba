Attribute VB_Name = "ADOSheetIn"
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 10/May/2022    Kalmclaeyd Tarclanus    Improved
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' get table-collection from Excel sheet using ADO-OLEDB connection
'''
Public Function GetDataTableColFromSheetByBookPathAndSheetName(ByVal vstrInputBookPath As String, _
        ByVal vstrSheetName As String, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjColumnsExceptionIndexList As Scripting.Dictionary = Nothing, _
        Optional ByVal vobjColumnsFieldTitleNameExceptionList As Scripting.Dictionary = Nothing) As Collection
    
    
    Dim objSheet As Excel.Worksheet, strSQLWherePart As String
    Dim strSQL As String, objDataTableCol As Collection, strTableName As String
    
    strSQLWherePart = GetSQLWhereFromNotNullConditionFromWorkbookPathAndSheetName(vstrInputBookPath, vstrSheetName, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjColumnsExceptionIndexList, vobjColumnsFieldTitleNameExceptionList)

    With New XlAdoConnector
    
        .SetInputExcelBookPath vstrInputBookPath, vblnBookReadOnly:=True
        
        strTableName = "[" & vstrSheetName & "$]"
        
        strSQL = "SELECT * FROM " & strTableName & "WHERE " & strSQLWherePart
        
        Set objDataTableCol = GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
    
    Set GetDataTableColFromSheetByBookPathAndSheetName = objDataTableCol
End Function
