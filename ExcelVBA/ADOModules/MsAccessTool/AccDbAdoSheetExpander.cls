VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AccDbAdoSheetExpander"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   output SQL result into Excel.Worksheet after connecting Access database by ADO OLE DB interface
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IOutputBookPath

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjConnector As AccDbAdoConnector

Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjConnector = New AccDbAdoConnector
    
    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
    
    With mobjAdoRecordsetSheetExpander
    
        Set .ADOConnectorInterface = mobjConnector
    
        Set .OutputBookPathInterface = Me
        
        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOrMSAccessRDBSQL
    End With
End Sub

'''
'''
'''
Private Sub Class_Terminate()

    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess

    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing

    Me.CloseConnection

    Set mobjAdoRecordsetSheetExpander = Nothing
    
    Set mobjConnector = Nothing
End Sub


'///////////////////////////////////////////////
'/// Properties - connoted ADO connector
'///////////////////////////////////////////////
Public Property Get SQLExecutionResult() As SQLResult

    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
End Property

Public Property Get IsSqlExecutionFailed() As Boolean

    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
End Property

Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)

    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
End Property
Public Property Get AllowToRecordSQLLog() As Boolean

    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
End Property


Public Property Get CurrentSheet() As Excel.Worksheet

    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
End Property


Public Property Get CurrentBook() As Excel.Workbook

    If Not Me.CurrentSheet Is Nothing Then
    
        Set CurrentBook = Me.CurrentSheet.Parent
    Else
        Set CurrentBook = Nothing
    End If
End Property


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get ADOConnectorInterface() As IADOConnector

    Set ADOConnectorInterface = mobjConnector.ADOConnectorInterface
End Property

'''
'''
'''
Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter

    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
End Property


'///////////////////////////////////////////////
'/// Properties - connoted ADOConnectionSetting
'///////////////////////////////////////////////
Public Property Get ConnectSetting() As ADOConnectionSetting

    Set ConnectSetting = mobjConnector.ConnectSetting
End Property

'''
''' unit is second. [s]
'''
Public Property Let CommandTimeout(ByVal vintTimeout As Long)

    mobjConnector.CommandTimeout = vintTimeout
End Property
Public Property Get CommandTimeout() As Long

    CommandTimeout = mobjConnector.CommandTimeout
End Property


'///////////////////////////////////////////////
'/// Properties - Access database connection by ACCESS OLE-DB provider
'///////////////////////////////////////////////
Public Property Get ConnectingFilePath() As String

    ConnectingFilePath = mobjConnector.ConnectingFilePath
End Property


'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
'''
'''
'''
Private Function IOutputBookPath_GetPreservedOutputBookPath() As String

    IOutputBookPath_GetPreservedOutputBookPath = GetAccDbConnectingPrearrangedLogBookPath()
End Function


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** set ADO connection string all parameters directly
'**---------------------------------------------
'''
'''
'''
Public Sub SetMsAccessConnection(ByVal vstrDbPath As String, _
        Optional ByVal vstrOldTypeDbPassword As String = "", _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)


    mobjConnector.SetMsAccessConnection vstrDbPath, vstrOldTypeDbPassword, venmAccessOLEDBProviderType
End Sub

'**---------------------------------------------
'** set ADO connection string parameters by a User-form interface
'**---------------------------------------------
'''
'''
'''
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrDbPath: Input</Argument>
''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
''' <Argument>vstrOldTypeDbPassword: Input</Argument>
''' <Return>Boolean</Return>
Public Function IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDbPath As String = "", _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vstrOldTypeDbPassword As String = "")
        
        
    IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(vstrSettingKeyName, _
            vstrDbPath, _
            venmAccessOLEDBProviderType, _
            vstrOldTypeDbPassword)
End Function

'''
''' Close ADO connection
'''
Public Sub CloseConnection()

    mobjConnector.CloseConnection
End Sub

'''
'''
'''
Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()

    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
End Sub

'''
'''
'''
Public Sub CloseAll()

    CloseConnection

    CloseCacheOutputBookOfOpenByPresetModeProcess
End Sub

'**---------------------------------------------
'** Output Recordset and logs to cells on sheet
'**---------------------------------------------
'''
''' query SQL and output result to Excel sheet
'''
Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
        ByVal vobjOutputSheet As Excel.Worksheet, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
     
    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, _
            vobjOutputSheet, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            mfobjGetConnectedAccDbPathInsertText(), _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the Existed Excel sheet
'''
Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)


    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
'''
'''
Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)

    
    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)

    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the already specified Excel sheet
'''
Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
'''
'''
Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)

    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
End Sub

'**---------------------------------------------
'** Excecute SQL command (UPDATE, INSERT, DELETE)
'**---------------------------------------------
'''
''' command SQL and output result-log to Excel sheet
'''
Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the prepared Excel sheet
'''
Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetConnectedAccDbPathInsertText() As Collection
    
    Dim objInsertTexts As Collection
    
    Set objInsertTexts = New Collection
    
    With mobjConnector
    
        objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
    End With
    
    Set mfobjGetConnectedAccDbPathInsertText = objInsertTexts
End Function



