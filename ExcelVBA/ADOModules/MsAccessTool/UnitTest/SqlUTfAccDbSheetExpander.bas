Attribute VB_Name = "SqlUTfAccDbSheetExpander"
'
'   Sanity test to output SQL result into a Excel sheet for Access database using ADO OLE DB
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
'       which should have been the installed when the Microsoft Office Excel software has been installed.
'       Dependent on SqlAdoConnectingUTfAccDb.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 15/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToSqlUTfAccDbSheetExpander()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UTfAdoConnectAccDb,UTfDaoConnectAccDb"
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** AccDbAdoSheetExpander
'**---------------------------------------------
'''
''' Create an in-line table from the only SELECT sql inside
'''
Private Sub msubSanityTestToConnectToAccDbBySqlInlineVirtualTableByAccDbAdoSheetExpander()
    
    Dim strSQL As String

    With New AccDbAdoSheetExpander

        .SetMsAccessConnection PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist("TestDbOfAccDbAdoSheetExpander.accdb")

        .AllowToRecordSQLLog = True

        'strSQL = "SELECT 'ABC', 'DEF' UNION SELECT 'GHI', 'JKL'" ' Error occurs

        'strSQL = "SELECT 1"

        'strSQL = "SELECT 'ABC', 'DEF'"

        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"

        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
        
        .CloseCacheOutputBookOfOpenByPresetModeProcess
    End With
End Sub


'''
'''
'''
Private Sub msubSanityTestToConnectToAccDbBySqlInsert()

    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
    
    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()

    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
    

    With New AccDbAdoSheetExpander
    
        .SetMsAccessConnection strDbPath
        
        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
        
        strSQL = "INSERT INTO Test_Table VALUES (13, 14, 15, 'Type3');"
    
        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
        
        strSQL = "INSERT INTO Test_Table VALUES (16, 17, 18, 'Type3');"
        
        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
        
        
        strSQL = "SELECT * FROM Test_Table"

        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
        
        .CloseConnection
    End With
    
    DeleteDummySheetWhenItExists objOutputBook
End Sub

'''
''' Test for SQL DELETE
'''
Private Sub msubSanityTestToConnectToAccDbBySqlDelete()

    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
    
    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()

    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
    

    With New AccDbAdoSheetExpander
    
        .SetMsAccessConnection strDbPath
        
        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
        
        strSQL = "DELETE FROM Test_Table WHERE ColText = 'Type2';"
    
        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
        
        strSQL = "SELECT * FROM Test_Table"
        
        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
        
        .CloseAll
    End With
    
    DeleteDummySheetWhenItExists objOutputBook
End Sub

'''
'''
'''
Private Sub msubSanityTestToConnectToAccDbBySqlUpdate()

    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
    
    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()

    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
    

    With New AccDbAdoSheetExpander
    
        .SetMsAccessConnection strDbPath
        
        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
        
        strSQL = "UPDATE Test_Table SET Col1 = 41, Col2 = 42 WHERE Col3 = 12 AND ColText = 'Type2';"
    
        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
        
        strSQL = "SELECT * FROM Test_Table"
        
        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
        
        .CloseAll
    End With
    
    DeleteDummySheetWhenItExists objOutputBook
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrGetTempAccDbSqlTestOutputBookPath() As String

    mfstrGetTempAccDbSqlTestOutputBookPath = mfstrGetTempAccDbSqlTestOutputBookDir() & "\SqlTestResultBookOfAccDbAdoSheetExpander.xlsx"
End Function


'''
'''
'''
Private Function mfstrGetTempAccDbSqlTestOutputBookDir() As String

    Dim strDir As String
    
    strDir = GetCurrentBookOutputDir() & "\SQLTests\FromAccDbConnect"

    ForceToCreateDirectory strDir
    
    mfstrGetTempAccDbSqlTestOutputBookDir = strDir
End Function


