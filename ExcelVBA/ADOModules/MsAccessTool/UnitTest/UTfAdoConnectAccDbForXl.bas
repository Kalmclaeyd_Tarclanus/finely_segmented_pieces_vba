Attribute VB_Name = "UTfAdoConnectAccDbForXl"
'
'   Serving Microsoft Access db serving tests by ADO without Access application
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToLoadAccDbTableToExcelSheetWithUsingQueryTable()

    Dim strDbPath As String, strDbName As String, strSQL As String, strConnectionString As String
    Dim strBookName As String
    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    Dim objQueryTable As Excel.QueryTable
    
    
    strDbName = "TestFromVba01.accdb"
    
    strDbPath = GetPathAfterCreateSampleAccDb(strDbName)



    strBookName = Replace(strDbName, ".", "_") & ".xlsx"
    
    ' prepare a AccDb
    strOutputBookPath = mfstrGetTemporaryBookDir() & "\" & strBookName
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "FromAccDb")


    'strSQL = "select * from TestTable;"
    
    strSQL = "select * from TestTable"

    strConnectionString = "ODBC;DSN=MS Access Database;DBQ=" & strDbPath
    
    
    Set objQueryTable = objSheet.QueryTables.Add(strConnectionString, objSheet.Range("A1"), strSQL)

    With objQueryTable

        .Connection = strConnectionString

        .Name = "A_Query"

        '.BackgroundQuery = False  ' prevent the back-ground processing
        
        .Refresh
        
        '.Delete  ' delete query-table
    End With
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrGetTemporaryBookDir() As String

    Dim strDir As String

    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\LoadedFromAccDb"

    ForceToCreateDirectory strDir

    mfstrGetTemporaryBookDir = strDir
End Function

