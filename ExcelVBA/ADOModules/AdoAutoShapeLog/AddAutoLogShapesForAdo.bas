Attribute VB_Name = "AddAutoLogShapesForAdo"
'
'   Add auto-shape log onto the specified Excel sheet for ADO SQL results
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on SQLResult.cls which dependent on LoadedADOConnectionSetting.cls
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Separated from AddAutoLogShapes.bas
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjFileTagOrDirectoryTagRegExp As VBScript_RegExp_55.RegExp


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' the following is dependent on SQLResult object
'''
''' <Argument>vobjSheet: Input-Output</Argument>
''' <Argument>vobjSQLRes: Input</Argument>
''' <Argument>vobjDataTableSheetRangeAddresses: Input</Argument>
''' <Argument>vobjHeaderInsertTexts: Input</Argument>
''' <Argument>vsngFontSize: Input</Argument>
Public Sub AddAutoShapeRdbSqlQueryLog(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjSQLRes As SQLResult, _
        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
        ByVal vobjHeaderInsertTexts As Collection, _
        Optional ByVal vsngFontSize As Single = 9.5)

    Dim strDisplayText As String, objAutoShapeAdjuster As ASWindowPositionAdjuster
    Dim objShape As Excel.Shape, varText As Variant
    
    
    Set objAutoShapeAdjuster = New ASWindowPositionAdjuster

    If Not vobjSQLRes Is Nothing Then

        Set objShape = GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics(vobjSheet, _
                objAutoShapeAdjuster, _
                XlShapeTextLogOfUsedActualSQL, _
                vobjSQLRes.SQL, _
                XlShapeTextLogInteriorFillDefault, _
                vsngFontSize + 1#)
        
        
        strDisplayText = mfstrGetSQLSelectedTableLogOfSQLResult(vobjSQLRes, vobjDataTableSheetRangeAddresses)
        
        
        Set objShape = GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics(vobjSheet, _
                objAutoShapeAdjuster, _
                XlShapeTextLogOfElapsedTimeAndRowCountAppearance, _
                strDisplayText, _
                XlShapeTextLogInteriorFillDefault, _
                vsngFontSize)
    End If
    
    If Not vobjHeaderInsertTexts Is Nothing Then
    
        If vobjHeaderInsertTexts.Count > 0 Then
        
            With mfobjGetFileTagOrDirectoryTagRegExp()
            
                For Each varText In vobjHeaderInsertTexts
                
                    If .Test(varText) Then
                        
                        ' This is either an Excel source book path or a source CSV directory path
                        
                        strDisplayText = varText
                        
                        Set objShape = GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics(vobjSheet, _
                                objAutoShapeAdjuster, _
                                XlShapeTextLogOfInputSourceTableFilePath, _
                                strDisplayText, _
                                XlShapeTextLogInteriorFillDefault, _
                                vsngFontSize)
                        
                        msubChangeAutoShapeTargetTextFontsAboutInputSourceTableFilePath objShape
                    Else
                        ' Shape-general message
                        
                        AddAutoShapeGeneralMessage vobjSheet, _
                                varText, _
                                AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault, _
                                objAutoShapeAdjuster
                    End If
                Next
            End With
        End If
    End If
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>robjSQLResult: Input</Argument>
''' <Argument>vobjDataTableSheetRangeAddresses: Input</Argument>
''' <Return>String</Return>
Private Function mfstrGetSQLSelectedTableLogOfSQLResult(ByRef robjSQLResult As SQLResult, _
        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses) As String

    Dim strDisplayText As String

    With robjSQLResult
    
        strDisplayText = GetTextOfStrKeyAddAutoLogShapesSqlExecutionElapsedTime() & ": " & Format(CDbl(.SQLExeElapsedTime) / 1000#, "0.000") & " " & GetTextOfStrKeyAddAutoLogShapesUnitSecond() & vbCrLf
        
        strDisplayText = strDisplayText & GetTextOfStrKeyAddAutoLogShapesExcelSheetProcessingElapsedTime() & ": " & Format(CDbl(.ExcelSheetOutputElapsedTime) / 1000#, "0.000") & " " & GetTextOfStrKeyAddAutoLogShapesUnitSecond() & vbCrLf
        
        strDisplayText = strDisplayText & GetTextOfStrKeyAddAutoLogShapesTableRecordCount() & ": " & CStr(.TableRecordCount) & vbCrLf
        
        strDisplayText = strDisplayText & GetTextOfStrKeyAddAutoLogShapesTableFieldCount() & ": " & CStr(.TableFieldCount) & vbCrLf
        
        If Not vobjDataTableSheetRangeAddresses Is Nothing Then
        
            With vobjDataTableSheetRangeAddresses
            
                strDisplayText = strDisplayText & "Records address" & ": " & CStr(.RecordAreaRangeAddress) & vbCrLf
        
                strDisplayText = strDisplayText & "Field-titles address" & ": " & CStr(.FieldTitlesRangeAddress) & vbCrLf
            End With
        End If
        
        strDisplayText = strDisplayText & GetTextOfStrKeyAddAutoLogShapesSqlExecutedTime() & ": " & Format(.SQLExeTime, "ddd, yyyy/m/d hh:mm:dd")
    End With

    mfstrGetSQLSelectedTableLogOfSQLResult = strDisplayText
End Function


'''
'''
'''
''' <Argument>robjShape: Input-Output</Argument>
Private Sub msubChangeAutoShapeTargetTextFontsAboutInputSourceTableFilePath(ByRef robjShape As Excel.Shape)

    Dim objDecorationTargetTextSettings As Collection
        
    Set objDecorationTargetTextSettings = New Collection
    
    With objDecorationTargetTextSettings

        .Add "<File Name>;9.5;trRGBBeigeWhiteFontColor"
        
        .Add "<Directory>;9.5;trRGBBeigeWhiteFontColor"
        
        .Add "<CSV Directory>;9.5;trRGBBeigeWhiteFontColor"
    End With
    
    ChangeAutoShapeTargetTextFonts robjShape, objDecorationTargetTextSettings
End Sub


'**---------------------------------------------
'** Prepare RegExp objects
'**---------------------------------------------
'''
'''
'''
Private Function mfobjGetFileTagOrDirectoryTagRegExp() As VBScript_RegExp_55.RegExp

    If mobjFileTagOrDirectoryTagRegExp Is Nothing Then
    
        Set mobjFileTagOrDirectoryTagRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjFileTagOrDirectoryTagRegExp
        
            .Pattern = "<File Name>.*xls|<CSV Directory>"
        End With
    End If

    Set mfobjGetFileTagOrDirectoryTagRegExp = mobjFileTagOrDirectoryTagRegExp
End Function
