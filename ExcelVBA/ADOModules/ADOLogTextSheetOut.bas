Attribute VB_Name = "ADOLogTextSheetOut"
'
'   create a ADO 'SELECT' SQL result Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  4/May/2023    Kalmclaeyd Tarclanus    Separated from ADOSheetOut.bas
'       Thu, 11/May/2023    Kalmclaeyd Tarclanus    Added ExecutedAdoSqlQueryLogPositionType enumeration
'

Option Explicit

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' SQL query execution log outputting position
'''
Public Enum ExecutedAdoSqlQueryLogPositionType

    SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable = &H0 ' auto-shapes log
    
    SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable = 1    ' sheet cells log on the same sheet of the Recordset data-table
    
    AppendingSqlQueryLogsInSheetCellsOnIndependentSheet = 2  ' sheet cells log on the other specified independent sheet
End Enum

'''
''' SQL command execution log outputting position
'''
Public Enum ExecutedAdoSqlCommandLogPositionType

    AppendingSqlCommandLogsInSheetCellsOnSpecifiedSheet = &H0

    AppendingSqlCommandLogsInSheetCellsOnFixedIndependentSheet = 2
End Enum

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About enumerations
'**---------------------------------------------
'''
'''
'''
Public Function GetAllExecutedAdoSqlQueryLogPositionTypesCol() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
        
        .Add ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
    
        .Add ExecutedAdoSqlQueryLogPositionType.AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
    End With

    Set GetAllExecutedAdoSqlQueryLogPositionTypesCol = objCol
End Function

'''
'''
'''
Public Function GetExecutedAdoSqlQueryLogPositionTypeTextFromEnm(ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType) As String

    Dim strText As String

    Select Case venmExecutedAdoSqlQueryLogPositionType
    
        Case ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
        
            strText = "SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable"
        
        Case ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
        
            strText = "SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable"
            
        Case ExecutedAdoSqlQueryLogPositionType.AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
        
            strText = "AppendingSqlQueryLogsInSheetCellsOnIndependentSheet"
    End Select

    GetExecutedAdoSqlQueryLogPositionTypeTextFromEnm = strText
End Function


'**---------------------------------------------
'** Output logs into cells on sheet
'**---------------------------------------------
'''
'''
'''
Public Sub OutputAdoSqlQueryResultLogsToSheet(ByVal vobjDataTableSheet As Excel.Worksheet, _
        ByRef robjSQLRes As SQLResult, _
        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
        ByVal vobjHeaderInsertTexts As Collection, _
        ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType, _
        ByVal vintTopLeftRowIndex As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmCQRSClassification As SqlRdbCQRSClassification = SqlRdbCQRSClassification.RdbSqlQueryType, _
        Optional ByVal vstrExclusiveSQLLogSheetName As String = "ExclusiveSQLLogs")
        

    Select Case venmExecutedAdoSqlQueryLogPositionType
    
        Case ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
    
            InsertHeaderTextsIntoCellsOnSheet vobjDataTableSheet, vobjHeaderInsertTexts, vintTopLeftRowIndex, vintTopLeftColumnIndex, True, True
    
            OutputAdoSqlQueryAndCommandResultLogsToCellsOnSpecifiedSheet vobjDataTableSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, vobjHeaderInsertTexts, vintTopLeftRowIndex, vintTopLeftColumnIndex, venmCQRSClassification
    
        Case ExecutedAdoSqlQueryLogPositionType.AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
        
            OutputAdoSqlQueryAndCommandResultLogsToCellsOnExclusiveSQLLogSheetWithFindingExclusiveLogSheet vobjDataTableSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, vobjHeaderInsertTexts, vintTopLeftRowIndex, vintTopLeftColumnIndex, venmCQRSClassification, vstrExclusiveSQLLogSheetName
         
        Case ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
    
            AddAutoShapeRdbSqlQueryLog vobjDataTableSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, vobjHeaderInsertTexts
    End Select
End Sub

'''
'''
'''
Public Sub OutputAdoSqlCommandResultLogsToCellsOnSpecifiedSQLLogSheet(ByVal vobjSqlCommandLogSheet As Excel.Worksheet, _
        ByRef robjSQLRes As SQLResult, _
        ByVal vobjHeaderInsertTexts As Collection, _
        ByVal vintTopLeftRowIndex As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)
        

    Dim intBottomRowIndex As Long
    
    intBottomRowIndex = GetBottomRowIndexFromWorksheetUsedRange(vobjSqlCommandLogSheet)
    
    OutputAdoSqlQueryAndCommandResultLogsToCellsOnExclusiveSQLLogSheet vobjSqlCommandLogSheet, robjSQLRes, Nothing, vobjHeaderInsertTexts, intBottomRowIndex + 1, 1, RdbSqlCommandType
End Sub



'''
''' if the count of rows exceeds the vintLoggingLimitRowMax, then fine new SQL log sheet.
'''
Public Sub OutputAdoSqlQueryAndCommandResultLogsToCellsOnExclusiveSQLLogSheetWithFindingExclusiveLogSheet(ByVal vobjDataTableSheet As Excel.Worksheet, _
        ByRef robjSQLRes As SQLResult, _
        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
        ByVal vobjHeaderInsertTexts As Collection, _
        ByVal vintTopLeftRowIndex As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmCQRSClassification As SqlRdbCQRSClassification = SqlRdbCQRSClassification.RdbSqlQueryType, _
        Optional ByVal vstrExclusiveSQLLogSheetName As String = "ExclusiveSQLLogs", _
        Optional ByVal vintLoggingLimitRowMax As Long = 300)
        

    Dim objSQLLogSheet As Excel.Worksheet, objBook As Excel.Workbook, intBottomRowIndex As Long
    
    Set objBook = vobjDataTableSheet.Parent
    
    FindBottomRowIndexAfterGetSheetWithFindingNewNameWhenUsedRangeRowsCountExceedsLimit objSQLLogSheet, intBottomRowIndex, vstrExclusiveSQLLogSheetName, objBook, vintLoggingLimitRowMax
    
    OutputAdoSqlQueryAndCommandResultLogsToCellsOnExclusiveSQLLogSheet objSQLLogSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, vobjHeaderInsertTexts, intBottomRowIndex + 1, 1, venmCQRSClassification
End Sub

'''
'''
'''
Public Sub OutputAdoSqlQueryAndCommandResultLogsToCellsOnExclusiveSQLLogSheet(ByVal vobjSQLLogSheet As Excel.Worksheet, _
        ByRef robjSQLRes As SQLResult, _
        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
        ByVal vobjHeaderInsertTexts As Collection, _
        ByVal vintTopLeftRowIndex As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmCQRSClassification As SqlRdbCQRSClassification = SqlRdbCQRSClassification.RdbSqlQueryType)
    
    OutputAdoSqlQueryAndCommandResultLogsToCellsOnSpecifiedSheet vobjSQLLogSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, vobjHeaderInsertTexts, vintTopLeftRowIndex, vintTopLeftColumnIndex, venmCQRSClassification
    
    InsertHeaderTextsIntoCellsOnSheet vobjSQLLogSheet, vobjHeaderInsertTexts, vintTopLeftRowIndex, vintTopLeftColumnIndex, True, True
    
    WriteTopAndBottomBoundaryBorderLineOfSpecifiedRow vobjSQLLogSheet, vintTopLeftRowIndex
    
    ShowBottomUsedRangeOfWorksheet vobjSQLLogSheet
End Sub


'''
''' vintTopLeftColumnIndex
'''
Public Sub OutputAdoSqlQueryAndCommandResultLogsToCellsOnSpecifiedSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByRef robjSQLRes As SQLResult, _
        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
        ByVal vobjHeaderInsertTexts As Collection, _
        ByVal vintTopLeftRowIndex As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional venmCQRSClassification As SqlRdbCQRSClassification = SqlRdbCQRSClassification.RdbSqlQueryType)


    Dim intSQLLogStartingRowIndex As Long, enmSQLSheetLogTitleInterior As SQLSheetLogTitleInterior
    
    enmSQLSheetLogTitleInterior = CellsInteriorOfSynchronizedSQLExec

    ' Write the SQL body text
    OutputUsedActualSQLToSpecifiedRangeOnExcelSheet robjSQLRes.SQL, vobjSheet.Cells(vintTopLeftRowIndex, vintTopLeftColumnIndex)
    
    intSQLLogStartingRowIndex = vintTopLeftRowIndex + 1
    
    If Not vobjHeaderInsertTexts Is Nothing Then
    
        intSQLLogStartingRowIndex = intSQLLogStartingRowIndex + vobjHeaderInsertTexts.Count
    End If
    
    Select Case venmCQRSClassification
    
        Case SqlRdbCQRSClassification.RdbSqlQueryType
        
            OutputSQLQueryDetailLogToCellsOnSheet vobjSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, intSQLLogStartingRowIndex, vintTopLeftColumnIndex, enmSQLSheetLogTitleInterior
            
        Case SqlRdbCQRSClassification.RdbSqlCommandType
            
            OutputSqlCommandDetailLogToCellsOnSheet vobjSheet, robjSQLRes, intSQLLogStartingRowIndex, vintTopLeftColumnIndex, False, enmSQLSheetLogTitleInterior
    End Select
    
    If (robjSQLRes.SQLOptionalLog And SqlOptionalLogFlagOfWorksheet.LogOfAdoConnectSettingOnSheet) > 0 Then
    
        OutputADOConnectionParametersLogToSheet vobjSheet, robjSQLRes.LoadedADOConnectSetting, vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex + 7) & CStr(intSQLLogStartingRowIndex)), False, enmSQLSheetLogTitleInterior
    End If
    
    If (robjSQLRes.SQLOptionalLog And SqlOptionalLogFlagOfWorksheet.LogOfCurrentUserComputerDomainOnSheet) > 0 Then
    
        OutputCurrentUserDomainLogToSheet vobjSheet, robjSQLRes.CurrentUserInfo, vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex + 10) & CStr(intSQLLogStartingRowIndex)), enmSQLSheetLogTitleInterior
     End If
End Sub




'''
'''
'''
Public Sub OutputUsedActualSQLToSpecifiedRangeOnExcelSheet(ByVal vstrSQL As String, ByVal vobjUsedActualRange As Excel.Range, Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)
    
    With vobjUsedActualRange
    
        .Value = vstrSQL
        
        With .Font
        
            .Name = "Meiryo": .FontStyle = "Regular"
            
            If (venmLogTitleInterior And SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured) > 0 Then
                
                .Color = RGB(255, 0, 0)
            Else
                .TintAndShade = 0
            End If
            
            .ThemeFont = xlThemeFontNone
        End With
    End With
End Sub


'**---------------------------------------------
'** output SQL result log to sheet
'**---------------------------------------------
'''
''' output executed SQL body text to worksheet, <CQRS: Command method>
'''
Public Sub OutputSQLBodyTextAndDetailAllLogToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByRef robjSQLRes As SQLResult, _
        ByVal vobjADOSheetFormatter As ADOSheetFormatter)
    
    With vobjADOSheetFormatter
        
        '.InsertInputtedHeaderTexts vobjSheet
    
        .ResultOutForAdoSqlExecution vobjSheet, robjSQLRes, SqlRdbCQRSClassification.RdbSqlCommandType
    End With
End Sub


'**---------------------------------------------
'** common logging
'**---------------------------------------------
'''
''' Add the SQL query(SELECT sentence) result log into the Excel sheet.
'''
Public Sub OutputSQLQueryDetailLogToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjSQLResult As SQLResult, _
        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
        ByVal vintLogStartTopLeftRowIndex As Long, _
        Optional ByVal vintLogStartTopLeftColumnIndex As Long = 1, _
        Optional venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)
    
    
    Dim objRange As Excel.Range, varValues() As Variant, varRangeAddresses() As Variant
    
    With vobjSheet
    
        ReDim varValues(1 To 5, 1 To 3)
    
        varValues(1, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutionElapsedTime(): varValues(1, 3) = GetTextOfStrKeyAdoSheetoutUnitSecond()
        
        varValues(2, 1) = GetTextOfStrKeyAdoSheetoutExcelSheetProcessingElapsedTime(): varValues(2, 3) = GetTextOfStrKeyAdoSheetoutUnitSecond()
        
        
        varValues(3, 1) = GetTextOfStrKeyAdoSheetoutTableRecordCount()
        
        varValues(4, 1) = GetTextOfStrKeyAdoSheetoutTableFieldCount()
        
        varValues(5, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutedTime()
        
        
        varValues(1, 2) = CSng(vobjSQLResult.SQLExeElapsedTime) / 1000#
        
        varValues(2, 2) = CSng(vobjSQLResult.ExcelSheetOutputElapsedTime) / 1000#
        
        varValues(3, 2) = vobjSQLResult.TableRecordCount
        
        varValues(4, 2) = vobjSQLResult.TableFieldCount
        
        varValues(5, 2) = Format(vobjSQLResult.SQLExeTime, "yyyy/mm/dd hh:mm:ss")
        
        
        SetNumberFormatLocalForLocazliedDateTime .Cells(vintLogStartTopLeftRowIndex + 4, vintLogStartTopLeftColumnIndex + 1)
        
        Set objRange = .Cells(vintLogStartTopLeftRowIndex, vintLogStartTopLeftColumnIndex)
        
        objRange.Resize(5, 3).Value = varValues
    
        .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex + 1)).NumberFormatLocal = "0.000"

        
        
        ' change appearence
        DecorateSheetCellsFontsForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 2) & CStr(vintLogStartTopLeftRowIndex + 4))
        
        DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex + 4))

        DecorateSheetCellsInteriorForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 4)), venmLogTitleInterior
        
        If Not vobjDataTableSheetRangeAddresses Is Nothing Then
        
            ReDim varRangeAddresses(1 To 2, 1 To 2)
        
            varRangeAddresses(1, 1) = "Records address": varRangeAddresses(2, 1) = "Field titles address"
            
            With vobjDataTableSheetRangeAddresses
            
                varRangeAddresses(1, 2) = .RecordAreaRangeAddress: varRangeAddresses(2, 2) = .FieldTitlesRangeAddress
            End With
        
            Set objRange = .Cells(vintLogStartTopLeftRowIndex + 2, vintLogStartTopLeftColumnIndex + 3)
        
            objRange.Resize(2, 2).Value = varRangeAddresses
        
            DecorateSheetCellsInteriorForSQLQueryAndCommandLog objRange.Resize(2, 1)
        
            DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog objRange.Resize(2, 2)
            
            DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange.Resize(2, 2)
        End If
    End With
End Sub



'''
''' Add the SQL command(UPDATE, DELETE, INSERT sentences) result log into the Excel sheet.
'''
Public Sub OutputSqlCommandDetailLogToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjSQLResult As SQLResult, _
        ByVal vintLogStartTopLeftRowIndex As Long, _
        Optional ByVal vintLogStartTopLeftColumnIndex As Long = 1, _
        Optional ByVal vblnOmitRecordsAffected As Boolean = False, _
        Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)


    Dim objRange As Excel.Range, varValues() As Variant
    
    With vobjSheet
    
        If Not vblnOmitRecordsAffected Then
        
            ' After RDB Command execution
            
            ReDim varValues(1 To 3, 1 To 3)
        
            varValues(1, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutionElapsedTime()
            
            varValues(1, 3) = GetTextOfStrKeyAdoSheetoutUnitSecond()
            
            Select Case GetSQLSyntaxType(vobjSQLResult.SQL)
            
                Case RdbSqlClassification.RdbSqlDeleteType
                
                    varValues(2, 1) = GetTextOfStrKeyAdoSheetoutCountOfSqlDelete()
                    
                Case RdbSqlClassification.RdbSqlUpdateType
                    
                    varValues(2, 1) = GetTextOfStrKeyAdoSheetoutCountOfSqlUpdate()
                    
                Case RdbSqlClassification.RdbSqlInsertType
                    
                    varValues(2, 1) = GetTextOfStrKeyAdoSheetoutCountOfSqlInsert()
                    
                Case Else
                
                    varValues(2, 1) = GetTextOfStrKeyAdoSheetoutRecordsAffected()
            End Select
            
            varValues(3, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutedTime()
            
            varValues(1, 2) = CSng(vobjSQLResult.SQLExeElapsedTime) / 1000#
            
            varValues(2, 2) = vobjSQLResult.RecordsAffected
           
            varValues(3, 2) = Format(vobjSQLResult.SQLExeTime, "yyyy/mm/dd hh:mm:ss")
            
            SetNumberFormatLocalForLocazliedDateTime .Cells(vintLogStartTopLeftRowIndex + 2, 2)
            
            Set objRange = .Cells(vintLogStartTopLeftRowIndex, vintLogStartTopLeftColumnIndex)
            
            objRange.Resize(3, 3).Value = varValues
        
            .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex)).NumberFormatLocal = "0.000"
            
            
            ' change appearence
            DecorateSheetCellsFontsForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 2) & CStr(vintLogStartTopLeftRowIndex + 2))
            
            DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex + 2))
    
            DecorateSheetCellsInteriorForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 2)), venmLogTitleInterior
            
            .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex)).ColumnWidth = 22
            
            .Range(CStr(vintLogStartTopLeftRowIndex + 1) & ":" & CStr(vintLogStartTopLeftRowIndex + 1)).RowHeight = 27
            
            With .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex + 1))
                
                .Font.Size = 12
            End With
            
            .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 3) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 3)).ColumnWidth = 17
        Else
            ' After error occured
            
            ReDim varValues(1 To 2, 1 To 3)
        
            varValues(1, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutionElapsedTime(): varValues(1, 3) = GetTextOfStrKeyAdoSheetoutUnitSecond()
            
            varValues(2, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutedTime()
            
            varValues(1, 2) = CSng(vobjSQLResult.SQLExeElapsedTime) / 1000#
            
            'varValues(2, 2) = FormatDateTime(vobjSQLResult.SQLExeTime, vbLongDate) & "," & FormatDateTime(vobjSQLResult.SQLExeTime, vbLongTime)
            varValues(2, 2) = Format(vobjSQLResult.SQLExeTime, "yyyy/mm/dd hh:mm:ss")
            
            SetNumberFormatLocalForLocazliedDateTime .Cells(vintLogStartTopLeftRowIndex + 1, vintLogStartTopLeftColumnIndex + 1)
            
            Set objRange = .Cells(vintLogStartTopLeftRowIndex, vintLogStartTopLeftColumnIndex)
            
            objRange.Resize(2, 3).Value = varValues
        
        
            .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex)).NumberFormatLocal = "0.000"
        
            DecorateSheetCellsFontsForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 2) & CStr(vintLogStartTopLeftRowIndex + 1)), CellsInteriorOfSynchronizedSQLExec
            
            DecorateSheetCellsFontsForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex)), CellsInteriorOfSynchronizedSQLExec
            
            DecorateSheetCellsFontsForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 1)), venmLogTitleInterior
            
            
            DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex + 1))
    
    
            DecorateSheetCellsInteriorForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex)), CellsInteriorOfSynchronizedSQLExec
            
            DecorateSheetCellsInteriorForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 1)), venmLogTitleInterior
        End If
    End With
End Sub


'''
''' logging ADO connection parameters: CommandTimeout, CursorLocation, ExecuteOptions, CursorType, and LockType
'''
Public Sub OutputADOConnectionParametersLogToSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjLoadedConnectSetting As LoadedADOConnectionSetting, _
        ByVal vobjStartingRange As Excel.Range, _
        Optional ByVal vblnSetTwoColumns As Boolean = False, _
        Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)


    Dim intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    Dim varValues() As Variant, objRange As Excel.Range
    
    With vobjStartingRange
    
        intTopLeftRowIndex = .Row
        
        intTopLeftColumnIndex = .Column
    End With

    With vobjLoadedConnectSetting
    
        If vblnSetTwoColumns Then
            
            ReDim varValues(1 To 3, 1 To 5)
            
            If .IsRecordsetExists Then
            
                varValues(1, 4) = "CursorType"
                
                varValues(2, 4) = "LockType"
                
                
                varValues(1, 5) = GetADOEnumLiteralFromRecordsetCursorType(.CursorType)
                
                varValues(2, 5) = GetADOEnumLiteralFromRecordsetLockType(.LockType)
            End If
        Else
            ReDim varValues(1 To 5, 1 To 3)
            
            If .IsRecordsetExists Then
            
                varValues(4, 1) = "CursorType"
                
                varValues(5, 1) = "LockType"
                
                
                varValues(4, 2) = GetADOEnumLiteralFromRecordsetCursorType(.CursorType)
                
                varValues(5, 2) = GetADOEnumLiteralFromRecordsetLockType(.LockType)
            End If
        End If
        
        ' common area
        varValues(1, 1) = "CursorLocation"
        
        varValues(2, 1) = "ExecutionOption"
        
        varValues(3, 1) = "CommandTimeout"
        
        varValues(3, 3) = GetTextOfStrKeyAdoSheetoutUnitSecond()
        
        
        varValues(1, 2) = GetADOEnumLiteralFromCursorLocation(.CursorLocation)
        
        varValues(2, 2) = GetADOEnumAllLiteralsFromExecuteOption(.ExecuteOption)
        
        varValues(3, 2) = .CommandTimeout
        
        If vblnSetTwoColumns Then
        
            vobjSheet.Cells(intTopLeftRowIndex, intTopLeftColumnIndex).Resize(3, 5).Value = varValues
            
            With vobjSheet
            
                Select Case venmLogTitleInterior
                    
                    Case SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured
                        
                        Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 2))
                        
                        If vobjLoadedConnectSetting.IsRecordsetExists Then
                            
                            Set objRange = Union(objRange, .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex + 2)))
                        End If
                        
                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSQLExecutionErrorOccured
                        
                        Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 2) & CStr(intTopLeftRowIndex + 2))
                        
                        If vobjLoadedConnectSetting.IsRecordsetExists Then
                            
                            Set objRange = Union(objRange, .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 4) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 4) & CStr(intTopLeftRowIndex + 2)))
                        End If
                        
                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSynchronizedSQLExec
                    
                    Case Else
                    
                        If vobjLoadedConnectSetting.IsRecordsetExists Then
                        
                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 4) & CStr(intTopLeftRowIndex + 2))
                        Else
                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 2) & CStr(intTopLeftRowIndex + 2))
                        End If
                        
                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange
                End Select
            
            
                Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex + 2))
                
                If vobjLoadedConnectSetting.IsRecordsetExists Then
                    
                    Set objRange = Union(objRange, .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 4) & CStr(intTopLeftRowIndex + 1)))
                End If
                
                DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog objRange
        
                Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 2))
                
                If vobjLoadedConnectSetting.IsRecordsetExists Then
                    
                    Set objRange = Union(objRange, .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex + 1)))
                End If
                
                DecorateSheetCellsInteriorForSQLQueryAndCommandLog objRange, venmLogTitleInterior
            End With
            
        Else
            vobjSheet.Cells(intTopLeftRowIndex, intTopLeftColumnIndex).Resize(5, 3).Value = varValues
            
            With vobjSheet
            
                Select Case venmLogTitleInterior
                    
                    Case SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured
                        
                        If vobjLoadedConnectSetting.IsRecordsetExists Then
                            
                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 4))
                        Else
                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 2))
                        End If
                        
                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSQLExecutionErrorOccured
                        
                        If vobjLoadedConnectSetting.IsRecordsetExists Then
                            
                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex + 4))
                        Else
                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex + 2))
                        End If
                        
                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSynchronizedSQLExec
                    
                    Case Else
                        
                        If vobjLoadedConnectSetting.IsRecordsetExists Then
                            
                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 2) & CStr(intTopLeftRowIndex + 4))
                        Else
                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 2) & CStr(intTopLeftRowIndex + 2))
                        End If
                        
                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSynchronizedSQLExec
                End Select
            
                If vobjLoadedConnectSetting.IsRecordsetExists Then
                    
                    Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex + 4))
                Else
                    Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex + 2))
                End If
                
                DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog objRange
        
                If vobjLoadedConnectSetting.IsRecordsetExists Then
                    
                    Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 4))
                Else
                    Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 2))
                End If
                
                DecorateSheetCellsInteriorForSQLQueryAndCommandLog objRange, venmLogTitleInterior
            End With
        End If
    End With
End Sub

'''
''' logging client(this PC) current user domain informations
'''
Public Sub OutputCurrentUserDomainLogToSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjCurrentUserDomain As CurrentUserDomain, _
        ByVal vobjStartingRange As Excel.Range, _
        Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)

    Dim intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    Dim varValues() As Variant, objRange As Excel.Range, strStartingColumn As String
    
    With vobjStartingRange
    
        intTopLeftRowIndex = .Row
        
        intTopLeftColumnIndex = .Column
    End With

    ReDim varValues(1 To 4, 1 To 2)

    With vobjCurrentUserDomain
    
        varValues(1, 1) = GetTextOfStrKeyAdoSheetoutClientUserName()
        
        varValues(2, 1) = GetTextOfStrKeyAdoSheetoutClientHostName()
        
        varValues(3, 1) = GetTextOfStrKeyAdoSheetoutClientIpv4Address()
        
        varValues(4, 1) = GetTextOfStrKeyAdoSheetoutClientMacAddress()
        
        
        varValues(1, 2) = .CurrentUserName
        
        varValues(2, 2) = .CurrentNodePCName
        
        varValues(3, 2) = .IPv4Address
        
        varValues(4, 2) = .MACAddress
    End With

    vobjSheet.Cells(intTopLeftRowIndex, intTopLeftColumnIndex).Resize(4, 2).Value = varValues

    strStartingColumn = ConvertXlColumnIndexToLetter(vobjStartingRange.Column) ' = 21#
    
    With vobjSheet
    
        .Columns(strStartingColumn & ":" & strStartingColumn).EntireColumn.ColumnWidth = 21
    
        Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex + 3))
        
        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSynchronizedSQLExec
    
        DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog objRange
        
        Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 3))
        
        DecorateSheetCellsInteriorForSQLQueryAndCommandLog objRange, venmLogTitleInterior
    End With
End Sub


'''
'''
'''
Public Sub DecorateSheetCellsFontsForSQLQueryAndCommandLog(ByVal vobjRange As Excel.Range, _
        Optional venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)
   
    With vobjRange
    
        With .Font
        
            .Name = "Meiryo": .FontStyle = "Regular"
            
            .Size = 9
            
            Select Case venmLogTitleInterior
            
                Case SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec, SQLSheetLogTitleInterior.CellsInteriorOfAsynchronousSQLExec
                
                    .ThemeColor = xlThemeColorLight1
                    
                    .TintAndShade = 0
                    
                Case SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured
                
                    .Color = RGB(255, 255, 255)
            End Select
            
            .ThemeFont = xlThemeFontNone
        End With
    End With
End Sub


'''
''' about borderlines
'''
Public Sub DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog(ByVal vobjRange As Excel.Range)
    
    Dim objBorders As Collection
    Dim varBorder As Variant, enmBorder As Excel.XlBordersIndex
    
    Set objBorders = GetXlBorderIndexColFromOptions()
    
    With vobjRange
    
        .Borders(xlDiagonalDown).LineStyle = xlNone
        
        .Borders(xlDiagonalUp).LineStyle = xlNone
            
        For Each varBorder In objBorders
        
            enmBorder = varBorder
            
            If .Borders(enmBorder).LineStyle = xlNone Then
                
                With .Borders(enmBorder)
                
                    .LineStyle = xlContinuous
                    
                    .ThemeColor = 1
                    
                    .TintAndShade = -0.249946592608417
                    
                    .Weight = xlThin
                End With
            End If
        Next
    End With
End Sub

'''
''' about interiors
'''
Public Sub DecorateSheetCellsInteriorForSQLQueryAndCommandLog(ByVal vobjRange As Excel.Range, _
        Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)


    Dim intFirstColor As Long, intSecondColor As Long

    Select Case venmLogTitleInterior
    
        Case SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec
        
            intFirstColor = RGB(255, 255, 255)
            
            intSecondColor = RGB(235, 241, 222)
            
        Case SQLSheetLogTitleInterior.CellsInteriorOfAsynchronousSQLExec
        
            intFirstColor = RGB(255, 255, 255)
            
            intSecondColor = RGB(241, 223, 227)
            
            
        Case SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured
        
            intFirstColor = RGB(249, 169, 177)
            
            intSecondColor = RGB(255, 0, 0)
    End Select

    With vobjRange.Interior
    
        .Pattern = xlPatternLinearGradient
        
        With .Gradient
        
            .Degree = 90
            
            With .ColorStops
                    
                .Clear
                
                With .Add(0): .Color = intFirstColor: End With
                
                With .Add(0.5): .Color = intSecondColor: End With
                
                With .Add(1): .Color = intFirstColor: End With
            End With
        End With
    End With
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub SetNumberFormatLocalForLocazliedDateTime(ByVal vobjRange As Excel.Range)

    With vobjRange
            
        Select Case GetCurrentUICaptionLanguageType()
        
            Case CaptionLanguageType.UIJapaneseCaptions
            
                '.NumberFormatLocal = "yyyy""年""m""月""d""日""""(""aaa"")"" hh:mm:ss"
                .NumberFormatLocal = "m""月""d""日""""(""aaa"")"" hh:mm:ss"
            Case Else
                ' English default
                
                '.NumberFormatLocal = "ddd"","" d/mmm/yyyy h:mm:ss AM/PM"
                .NumberFormatLocal = "ddd"","" m/d h:mm:ss AM/PM"
        End Select
    End With
End Sub


