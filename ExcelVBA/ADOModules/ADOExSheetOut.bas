Attribute VB_Name = "ADOExSheetOut"
'
'   create a SQL result Excel sheet with additional information by using ADODB.Recordset
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun, 14/Jan/2024    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "ADOExSheetOut"


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vobjInfoTypeToTitleToValueDic: Dictionary(Of String, Dictionary(Of String, Variant))</Argument>
Public Function NormalOutputRecordsetWithAdditionalInfoToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        ByVal vobjInfoTypeToTitleToValueDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False, _
        Optional ByVal vblnAllowToOutputMetaColumnInfoNameToSheet As Boolean = True, _
        Optional ByVal vblnAllowToAddAutoShapeInfoAboutBasicRecordSet As Boolean = True) As Long


    Dim intCountRowsOfAdditionalInfo As Long, intFieldTitlesRowIndex As Long, strNullColumnsDelimitedComma As String
    
    intCountRowsOfAdditionalInfo = vobjInfoTypeToTitleToValueDic.Count

    If vobjRSet.EOF Then

        NormalOutputOnlyAdditionalInfoToCellsOnSheetWithoutCheckingFieldTitles vobjSheet, _
                vobjInfoTypeToTitleToValueDic, _
                vintTopLeftRowIndex, _
                vintTopLeftColumnIndex, _
                vstrSuppressingColumnsDelimitedComma, _
                vblnAllowToOutputMetaColumnInfoNameToSheet
    Else
        NormalOutputOnlyAdditionalInfoToCellsOnSheet vobjSheet, _
                vobjRSet, _
                vobjInfoTypeToTitleToValueDic, _
                vintTopLeftRowIndex, _
                vintTopLeftColumnIndex, _
                vstrSuppressingColumnsDelimitedComma, _
                vblnAllowToOutputMetaColumnInfoNameToSheet
    End If

    intFieldTitlesRowIndex = vintTopLeftRowIndex + intCountRowsOfAdditionalInfo
    
    If Not vobjRSet.EOF Then
    
        strNullColumnsDelimitedComma = NormalOutputRecordsetToCellsOnSheet(vobjSheet, _
                vobjRSet, _
                intFieldTitlesRowIndex, _
                vintTopLeftColumnIndex, _
                vobjFieldTitleToColumnWidthDic, _
                vstrSuppressingColumnsDelimitedComma, _
                vblnAllowToCreateNullColumnsCompressedTable, _
                vblnAllowToAddAutoShapeInfoAboutBasicRecordSet)
    
        If vblnAllowToOutputMetaColumnInfoNameToSheet Then
    
            If vintTopLeftColumnIndex > 1 Then
    
                msubDecorateRangeOfOutputMetaColumnInfoNameAndText "FieldTitles", _
                        vobjSheet, _
                        intFieldTitlesRowIndex, _
                        vintTopLeftColumnIndex
                
                vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex - 1) & CStr(intFieldTitlesRowIndex)).EntireColumn.ColumnWidth = "11"
            End If
        End If
    
        If vblnAllowToCreateNullColumnsCompressedTable And strNullColumnsDelimitedComma <> "" Then
        
            ' output compressed additional info
        
        End If
    End If

End Function

'''
'''
'''
''' <Argument>vobjInfoTypeToTitleToValueDic: Dictionary(Of String, Dictionary(Of String, Variant))</Argument>
Public Function TransposeOutputRecordsetWithAdditionalInfoToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        ByVal vobjInfoTypeToTitleToValueDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjInfoTypeToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False, _
        Optional ByVal vblnAllowToOutputMetaColumnInfoNameToSheet As Boolean = True, _
        Optional ByVal vblnAllowToAddAutoShapeInfoAboutBasicRecordSet As Boolean = True) As Long


    Dim intCountColumnsOfAdditionalInfo As Long, intFieldTitlesColumnIndex As Long, strNullColumnsDelimitedComma As String
    
    intCountColumnsOfAdditionalInfo = vobjInfoTypeToTitleToValueDic.Count

    If vobjRSet.EOF Then
    
        TransposeOutputOnlyAdditionalInfoToCellsOnSheetWithoutCheckingFieldTitles vobjSheet, _
                vobjInfoTypeToTitleToValueDic, _
                vintTopLeftRowIndex, _
                vintTopLeftColumnIndex, _
                vobjInfoTypeToColumnWidthDic, _
                vstrSuppressingColumnsDelimitedComma, _
                vblnAllowToOutputMetaColumnInfoNameToSheet
    Else
        TransposeOutputOnlyAdditionalInfoToCellsOnSheet vobjSheet, _
                vobjRSet, _
                vobjInfoTypeToTitleToValueDic, _
                vintTopLeftRowIndex, _
                vintTopLeftColumnIndex, _
                vobjInfoTypeToColumnWidthDic, _
                vstrSuppressingColumnsDelimitedComma, _
                vblnAllowToOutputMetaColumnInfoNameToSheet
    End If

    intFieldTitlesColumnIndex = vintTopLeftColumnIndex + intCountColumnsOfAdditionalInfo
    
    If Not vobjRSet.EOF Then
    
        ' vobjFieldTitleToColumnWidthDic
    
        strNullColumnsDelimitedComma = TransposeOutputRecordsetToCellsOnSheet(vobjSheet, _
                vobjRSet, _
                vintTopLeftRowIndex, _
                intFieldTitlesColumnIndex, _
                vobjInfoTypeToColumnWidthDic, _
                vstrSuppressingColumnsDelimitedComma, _
                vblnAllowToCreateNullColumnsCompressedTable, _
                vblnAllowToAddAutoShapeInfoAboutBasicRecordSet)
    
    
        If vblnAllowToOutputMetaColumnInfoNameToSheet Then
    
            If vintTopLeftColumnIndex > 1 Then

                msubDecorateTransposeRangeOfOutputMetaColumnInfoNameAndText "FieldTitles", _
                        vobjSheet, _
                        vintTopLeftRowIndex, _
                        intFieldTitlesColumnIndex, _
                        vobjInfoTypeToColumnWidthDic
            End If
        End If
    
        If vblnAllowToCreateNullColumnsCompressedTable And strNullColumnsDelimitedComma <> "" Then
        
            ' output compressed additional info
        
        End If
    End If

End Function



'''
'''
'''
Public Sub NormalOutputOnlyAdditionalInfoToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        ByVal vobjInfoTypeToTitleToValueDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
        Optional ByVal vblnAllowToOutputMetaColumnInfoNameToSheet As Boolean = True)

    Dim objTitleToValueDic As Scripting.Dictionary, varInfoType As Variant
    Dim intRowIndex As Long, objSuppressingColumnsDic As Scripting.Dictionary, sngFontSize As Single, enmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern
    Dim strInfosArray() As String, strMetaTitle As String, blnSetHorizontalAlignmentCenter As Boolean
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    With vobjInfoTypeToTitleToValueDic
    
        intRowIndex = vintTopLeftRowIndex
    
        For Each varInfoType In .Keys
    
            Set objTitleToValueDic = .Item(varInfoType)
            
            GetFieldAdditionalInfosParams enmManualDecoratingFieldTitlesRangePattern, sngFontSize, blnSetHorizontalAlignmentCenter, varInfoType
            
            NormalOutputInfoTitleToValueDicToCellsOnSheet vobjSheet, vobjRSet, objTitleToValueDic, objSuppressingColumnsDic, intRowIndex, vintTopLeftColumnIndex, enmManualDecoratingFieldTitlesRangePattern, sngFontSize, blnSetHorizontalAlignmentCenter
            
            If vblnAllowToOutputMetaColumnInfoNameToSheet Then

                msubDecorateRangeOfOutputMetaColumnInfoNameAndTextForEachInfoType varInfoType, vobjSheet, intRowIndex, vintTopLeftColumnIndex
            End If
            
            intRowIndex = intRowIndex + 1
        Next
    End With
End Sub

'''
'''
'''
Public Sub NormalOutputOnlyAdditionalInfoToCellsOnSheetWithoutCheckingFieldTitles(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjInfoTypeToTitleToValueDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
        Optional ByVal vblnAllowToOutputMetaColumnInfoNameToSheet As Boolean = True)

    Dim objTitleToValueDic As Scripting.Dictionary, varInfoType As Variant
    Dim intRowIndex As Long, objSuppressingColumnsDic As Scripting.Dictionary, sngFontSize As Single, enmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern
    Dim strInfosArray() As String, strMetaTitle As String, blnSetHorizontalAlignmentCenter As Boolean
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    With vobjInfoTypeToTitleToValueDic
    
        intRowIndex = vintTopLeftRowIndex
    
        For Each varInfoType In .Keys
    
            Set objTitleToValueDic = .Item(varInfoType)
            
            GetFieldAdditionalInfosParams enmManualDecoratingFieldTitlesRangePattern, sngFontSize, blnSetHorizontalAlignmentCenter, varInfoType
            
            NormalOutputInfoTitleToValueDicToCellsOnSheetWithoutUsingRSetFieldTitles vobjSheet, objTitleToValueDic, objSuppressingColumnsDic, intRowIndex, vintTopLeftColumnIndex, enmManualDecoratingFieldTitlesRangePattern, sngFontSize, blnSetHorizontalAlignmentCenter
            
            If vblnAllowToOutputMetaColumnInfoNameToSheet Then

                msubDecorateRangeOfOutputMetaColumnInfoNameAndTextForEachInfoType varInfoType, vobjSheet, intRowIndex, vintTopLeftColumnIndex
            End If
            
            intRowIndex = intRowIndex + 1
        Next
    End With
End Sub

'''
'''
'''
Private Sub msubDecorateRangeOfOutputMetaColumnInfoNameAndTextForEachInfoType(ByRef rvarInfoType As Variant, _
        ByRef robjSheet As Excel.Worksheet, _
        ByVal vintRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long)

    Dim strInfosArray() As String, strMetaTitle As String

    If vintTopLeftColumnIndex > 1 Then

        strInfosArray = Split(rvarInfoType, "_")

        strMetaTitle = Replace(strInfosArray(1), "Column", "")

        msubDecorateRangeOfOutputMetaColumnInfoNameAndText strMetaTitle, robjSheet, vintRowIndex, vintTopLeftColumnIndex
    End If
End Sub


'''
'''
'''
Public Sub TransposeOutputOnlyAdditionalInfoToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        ByVal vobjInfoTypeToTitleToValueDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjInfoTypeToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
        Optional ByVal vblnAllowToOutputMetaColumnInfoNameToSheet As Boolean = True)

    Dim objTitleToValueDic As Scripting.Dictionary, varInfoType As Variant
    
    Dim intColumnIndex As Long, objSuppressingColumnsDic As Scripting.Dictionary, sngFontSize As Single
    
    Dim enmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern
    
    Dim blnSetHorizontalAlignmentCenter As Boolean
    
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    With vobjInfoTypeToTitleToValueDic
    
        intColumnIndex = vintTopLeftColumnIndex
    
        For Each varInfoType In .Keys
    
            Set objTitleToValueDic = .Item(varInfoType)
            
            GetFieldAdditionalInfosParams enmManualDecoratingFieldTitlesRangePattern, _
                    sngFontSize, _
                    blnSetHorizontalAlignmentCenter, _
                    varInfoType
            
            
            TransposeOutputInfoTitleToValueDicToCellsOnSheet vobjSheet, _
                    vobjRSet, _
                    objTitleToValueDic, _
                    objSuppressingColumnsDic, _
                    vintTopLeftRowIndex, _
                    intColumnIndex, _
                    enmManualDecoratingFieldTitlesRangePattern, _
                    sngFontSize, _
                    blnSetHorizontalAlignmentCenter
            
            
            If vblnAllowToOutputMetaColumnInfoNameToSheet Then

                msubDecorateTransposeRangeOfOutputMetaColumnInfoNameAndTextForEachInfoType varInfoType, _
                        vobjSheet, _
                        vintTopLeftRowIndex, _
                        intColumnIndex, _
                        vobjInfoTypeToColumnWidthDic
            End If
            
            intColumnIndex = intColumnIndex + 1
        Next
    End With
End Sub

'''
'''
'''
Public Sub TransposeOutputOnlyAdditionalInfoToCellsOnSheetWithoutCheckingFieldTitles(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjInfoTypeToTitleToValueDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjInfoTypeToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
        Optional ByVal vblnAllowToOutputMetaColumnInfoNameToSheet As Boolean = True)


    Dim objFieldTitleToValueDic As Scripting.Dictionary, varInfoType As Variant
    
    Dim intColumnIndex As Long, objSuppressingColumnsDic As Scripting.Dictionary, sngFontSize As Single
    
    Dim enmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern
    
    Dim blnSetHorizontalAlignmentCenter As Boolean
    
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    With vobjInfoTypeToTitleToValueDic
    
        intColumnIndex = vintTopLeftColumnIndex
    
        For Each varInfoType In .Keys
    
            Set objFieldTitleToValueDic = .Item(varInfoType)
            
            
            GetFieldAdditionalInfosParams enmManualDecoratingFieldTitlesRangePattern, _
                    sngFontSize, _
                    blnSetHorizontalAlignmentCenter, _
                    varInfoType
            
            
            TransposeOutputInfoTitleToValueDicToCellsOnSheetWithoutRSetFieldTitles vobjSheet, _
                    objFieldTitleToValueDic, _
                    objSuppressingColumnsDic, _
                    vintTopLeftRowIndex, _
                    intColumnIndex, _
                    enmManualDecoratingFieldTitlesRangePattern, _
                    sngFontSize, _
                    blnSetHorizontalAlignmentCenter
            
            
            If vblnAllowToOutputMetaColumnInfoNameToSheet Then

                msubDecorateTransposeRangeOfOutputMetaColumnInfoNameAndTextForEachInfoType varInfoType, _
                        vobjSheet, _
                        vintTopLeftRowIndex, _
                        intColumnIndex, _
                        vobjInfoTypeToColumnWidthDic
            End If
            
            intColumnIndex = intColumnIndex + 1
        Next
    End With
End Sub

'''
'''
'''
Private Sub msubDecorateTransposeRangeOfOutputMetaColumnInfoNameAndTextForEachInfoType(ByRef rvarInfoType As Variant, _
        ByRef robjSheet As Excel.Worksheet, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintCurrentColumnIndex As Long, _
        Optional ByVal vobjInfoTypeToColumnWidthDic As Scripting.Dictionary = Nothing)

    Dim strInfosArray() As String, strMetaTitle As String

    If vintTopLeftRowIndex > 1 Then

        strInfosArray = Split(rvarInfoType, "_")

        strMetaTitle = Replace(strInfosArray(1), "Column", "")

        msubDecorateTransposeRangeOfOutputMetaColumnInfoNameAndText strMetaTitle, robjSheet, vintTopLeftRowIndex, vintCurrentColumnIndex, vobjInfoTypeToColumnWidthDic
    End If
End Sub



'''
'''
'''
''' <Argument>vstrText: Input</Argument>
''' <Argument>robjSheet: Input-Output</Argument>
''' <Argument>vintRowIndex: Input</Argument>
''' <Argument>vintTopLeftColumnIndex: Input</Argument>
Private Sub msubDecorateRangeOfOutputMetaColumnInfoNameAndText(ByVal vstrText As String, _
        ByRef robjSheet As Excel.Worksheet, _
        ByVal vintRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long)

    Dim objRange As Excel.Range
    
    Set objRange = robjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex - 1) & CStr(vintRowIndex))
    
    objRange.Value = vstrText
    
    ' Blue back-ground
    DecorateFieldTitleFontAndInteriorForGatheredColTable objRange
    
    objRange.Font.Size = 9.5
End Sub

'''
'''
'''
''' <Argument>vstrText: Input</Argument>
''' <Argument>robjSheet: Input-Output</Argument>
''' <Argument>vintTopLeftRowIndex: Input</Argument>
''' <Argument>vintColumnIndex: Input</Argument>
''' <Argument>vobjInfoTypeToColumnWidthDic: Input</Argument>
Private Sub msubDecorateTransposeRangeOfOutputMetaColumnInfoNameAndText(ByVal vstrText As String, _
        ByRef robjSheet As Excel.Worksheet, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintColumnIndex As Long, _
        Optional ByVal vobjInfoTypeToColumnWidthDic As Scripting.Dictionary = Nothing)


    Dim objRange As Excel.Range
    
    Set objRange = robjSheet.Range(ConvertXlColumnIndexToLetter(vintColumnIndex) & CStr(vintTopLeftRowIndex - 1))
    
    objRange.Value = vstrText
    
    ' Blue back-ground
    DecorateFieldTitleFontAndInteriorForGatheredColTable objRange
    
    objRange.Font.Size = 9.5
    
    If Not vobjInfoTypeToColumnWidthDic Is Nothing Then
    
        With vobjInfoTypeToColumnWidthDic
        
            If .Exists(vstrText) Then
            
                objRange.EntireColumn.ColumnWidth = .Item(vstrText)
            End If
        End With
    End If
End Sub

'''
'''
'''
''' <Argument>renmManualDecoratingFieldTitlesRangePattern: Output</Argument>
''' <Argument>rsngFontSize: Output</Argument>
''' <Argument>rblnSetHorizontalAlignmentCenter: Output</Argument>
''' <Argument>vstrInfoType: Input</Argument>
Public Sub GetFieldAdditionalInfosParams(ByRef renmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern, _
        ByRef rsngFontSize As Single, _
        ByRef rblnSetHorizontalAlignmentCenter As Boolean, _
        ByVal vstrInfoType As String)

    Select Case True
    
        Case InStr(1, vstrInfoType, "No") > 0
        
            renmManualDecoratingFieldTitlesRangePattern = SepiaFontLightToDarkBrownGraduationBgMeiryo
        
            rsngFontSize = 9.5
        
            rblnSetHorizontalAlignmentCenter = True
        
        Case InStr(1, vstrInfoType, "TypeName") > 0
    
            renmManualDecoratingFieldTitlesRangePattern = DarkBrownFontPaleGradientGreenBgMeiryo
            
            rsngFontSize = 9
            
            rblnSetHorizontalAlignmentCenter = False
            
        Case InStr(1, vstrInfoType, "LengthOfType") > 0
        
            renmManualDecoratingFieldTitlesRangePattern = DarkBrownFontMorePaleGradientGreenBgMeiryo
        
            rsngFontSize = 10
            
            rblnSetHorizontalAlignmentCenter = False
            
        Case InStr(1, vstrInfoType, "ColumnDescription") > 0
    
            renmManualDecoratingFieldTitlesRangePattern = DarkBrownFontLightSilverBgMeiryo
            
            rsngFontSize = 9.5
        
            rblnSetHorizontalAlignmentCenter = False
        
        Case Else
        
            renmManualDecoratingFieldTitlesRangePattern = DarkBrownFontLightSilverBgMeiryo
            
            rsngFontSize = 9.5
            
            rblnSetHorizontalAlignmentCenter = False
    End Select
End Sub


'''
'''
'''
Public Sub NormalOutputInfoTitleToValueDicToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        ByVal vobjTitleToValueDic As Scripting.Dictionary, _
        ByVal vobjSuppressingColumnsDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, _
        Optional ByVal vsngFont As Single = 9.5, _
        Optional ByVal vblnSetHorizontalAlignmentCenter As Boolean = False)
        
        
    Dim varValues As Variant, i As Long, varKey As Variant, varItem As Variant, objInfosRange As Excel.Range
    Dim intIndex As Long, objField As ADOR.Field
    
    
    If vobjTitleToValueDic.Count = 1 Then
    
        varValues = vobjTitleToValueDic.Items(0)
    
        vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Value = varValues
    Else
        If Not vobjSuppressingColumnsDic Is Nothing Then
        
            ReDim varValues(1 To 1, 1 To vobjTitleToValueDic.Count - vobjSuppressingColumnsDic.Count)
        Else
            ReDim varValues(1 To 1, 1 To vobjTitleToValueDic.Count)
        End If
    
    
        i = 1
        
        intIndex = 1
        
        With vobjTitleToValueDic
        
            For Each objField In vobjRSet.Fields
            
                If .Exists(objField.Name) Then
            
                    Select Case True
    
                        Case vobjSuppressingColumnsDic Is Nothing, Not vobjSuppressingColumnsDic.Exists(i)
    
                            varItem = .Item(objField.Name)
    
                            varValues(1, intIndex) = varItem
    
                            intIndex = intIndex + 1
                    End Select
                End If
            Next
        End With
        
        Set objInfosRange = mfobjGetInfosRangeAndSetRowValues(varValues, vobjSheet, vintTopLeftRowIndex, vintTopLeftColumnIndex, vobjRSet.Fields.Count, vobjSuppressingColumnsDic)
        
        ' decorate Range
        msubDecorateInfosRangeOfFieldTitleInfos objInfosRange, venmManualDecoratingFieldTitlesRangePattern, vsngFont, vblnSetHorizontalAlignmentCenter
    End If
End Sub

'''
'''
'''
Public Sub NormalOutputInfoTitleToValueDicToCellsOnSheetWithoutUsingRSetFieldTitles(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjTitleToValueDic As Scripting.Dictionary, _
        ByVal vobjSuppressingColumnsDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, _
        Optional ByVal vsngFont As Single = 9.5, _
        Optional ByVal vblnSetHorizontalAlignmentCenter As Boolean = False)
        
        
    Dim varValues As Variant, i As Long, varKey As Variant, varItem As Variant, objInfosRange As Excel.Range
    Dim intIndex As Long
    
    
    If vobjTitleToValueDic.Count = 1 Then
    
        varValues = vobjTitleToValueDic.Items(0)
    
        vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Value = varValues
    Else
        If Not vobjSuppressingColumnsDic Is Nothing Then
        
            ReDim varValues(1 To 1, 1 To vobjTitleToValueDic.Count - vobjSuppressingColumnsDic.Count)
        Else
            ReDim varValues(1 To 1, 1 To vobjTitleToValueDic.Count)
        End If
    
    
        i = 1
        
        intIndex = 1
        
        With vobjTitleToValueDic
        
            For Each varKey In .Keys
            
                Select Case True
                
                    Case vobjSuppressingColumnsDic Is Nothing, Not vobjSuppressingColumnsDic.Exists(i)
            
                        varItem = .Item(varKey)
            
                        varValues(1, intIndex) = varItem
            
                        intIndex = intIndex + 1
                End Select
            Next
        End With
        
        Set objInfosRange = mfobjGetInfosRangeAndSetRowValues(varValues, _
                vobjSheet, _
                vintTopLeftRowIndex, _
                vintTopLeftColumnIndex, _
                vobjTitleToValueDic.Count, _
                vobjSuppressingColumnsDic)
        
        ' decorate Range
        msubDecorateInfosRangeOfFieldTitleInfos objInfosRange, _
                venmManualDecoratingFieldTitlesRangePattern, _
                vsngFont, _
                vblnSetHorizontalAlignmentCenter
    End If
End Sub


'''
'''
'''
Private Function mfobjGetInfosRangeAndSetRowValues(ByRef rvarRowValues As Variant, _
        ByRef robjSheet As Excel.Worksheet, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long, _
        ByVal vintCountOfFieldInfos As Long, _
        Optional ByVal vobjSuppressingColumnsDic As Scripting.Dictionary = Nothing) As Excel.Range


    Dim objInfosRange As Excel.Range
    
    If Not vobjSuppressingColumnsDic Is Nothing Then
    
        Set objInfosRange = robjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Resize(1, vintCountOfFieldInfos - vobjSuppressingColumnsDic.Count)
    
        objInfosRange.Value = rvarRowValues
    Else
    
        Set objInfosRange = robjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Resize(1, vintCountOfFieldInfos)
    
        objInfosRange.Value = rvarRowValues
    End If

    Set mfobjGetInfosRangeAndSetRowValues = objInfosRange
End Function



'''
'''
'''
Public Sub TransposeOutputInfoTitleToValueDicToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        ByVal vobjTitleToValueDic As Scripting.Dictionary, _
        ByVal vobjSuppressingColumnsDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, _
        Optional ByVal vsngFont As Single = 9.5, _
        Optional ByVal vblnSetHorizontalAlignmentCenter As Boolean = False)
        
        
    Dim varValues As Variant, i As Long, varKey As Variant, varItem As Variant, objInfosRange As Excel.Range
    Dim intIndex As Long, objField As ADOR.Field
    
    
    If vobjTitleToValueDic.Count = 1 Then
    
        varValues = vobjTitleToValueDic.Items(0)
    
        vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Value = varValues
    Else
        If Not vobjSuppressingColumnsDic Is Nothing Then
        
            ReDim varValues(1 To (vobjTitleToValueDic.Count - vobjSuppressingColumnsDic.Count), 1 To 1)
        Else
            ReDim varValues(1 To vobjTitleToValueDic.Count, 1 To 1)
        End If
    
    
        i = 1
        
        intIndex = 1
        
        With vobjTitleToValueDic
        
            For Each objField In vobjRSet.Fields
            
                If .Exists(objField.Name) Then
            
                    Select Case True
    
                        Case vobjSuppressingColumnsDic Is Nothing, Not vobjSuppressingColumnsDic.Exists(i)
    
                            varItem = .Item(objField.Name)
    
                            varValues(intIndex, 1) = varItem
    
                            intIndex = intIndex + 1
                    End Select
                End If
            Next
        End With
                
        Set objInfosRange = mfobjGetInfosRangeAndSetColumnValues(varValues, vobjSheet, vintTopLeftRowIndex, vintTopLeftColumnIndex, vobjRSet.Fields.Count, vobjSuppressingColumnsDic)
        
        ' decorate Range
        msubDecorateInfosRangeOfFieldTitleInfos objInfosRange, venmManualDecoratingFieldTitlesRangePattern, vsngFont, vblnSetHorizontalAlignmentCenter
    End If
End Sub

'''
'''
'''
Public Sub TransposeOutputInfoTitleToValueDicToCellsOnSheetWithoutRSetFieldTitles(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitleToValueDic As Scripting.Dictionary, _
        ByVal vobjSuppressingColumnsDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, _
        Optional ByVal vsngFont As Single = 9.5, _
        Optional ByVal vblnSetHorizontalAlignmentCenter As Boolean = False)
        
        
    Dim varValues As Variant, i As Long, varFieldTitle As Variant, varItem As Variant, objInfosRange As Excel.Range
    
    Dim intIndex As Long
    
    
    If vobjFieldTitleToValueDic.Count = 1 Then
    
        varValues = vobjFieldTitleToValueDic.Items(0)
    
        vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Value = varValues
    Else
        If Not vobjSuppressingColumnsDic Is Nothing Then
        
            ReDim varValues(1 To (vobjFieldTitleToValueDic.Count - vobjSuppressingColumnsDic.Count), 1 To 1)
        Else
            ReDim varValues(1 To vobjFieldTitleToValueDic.Count, 1 To 1)
        End If
    
    
        i = 1
        
        intIndex = 1
        
        With vobjFieldTitleToValueDic
        
            For Each varFieldTitle In .Keys
            
                Select Case True
                
                    Case vobjSuppressingColumnsDic Is Nothing, Not vobjSuppressingColumnsDic.Exists(i)
            
                        varItem = .Item(varFieldTitle)
            
                        varValues(intIndex, 1) = varItem
            
                        intIndex = intIndex + 1
                End Select
            Next
        End With
                
        Set objInfosRange = mfobjGetInfosRangeAndSetColumnValues(varValues, _
                vobjSheet, _
                vintTopLeftRowIndex, _
                vintTopLeftColumnIndex, _
                vobjFieldTitleToValueDic.Count, _
                vobjSuppressingColumnsDic)
        
        ' decorate Range
        
        msubDecorateInfosRangeOfFieldTitleInfos objInfosRange, _
                venmManualDecoratingFieldTitlesRangePattern, _
                vsngFont, _
                vblnSetHorizontalAlignmentCenter
    End If
End Sub

'''
'''
'''
''' <Argument>rvarColumnValues: Input</Argument>
''' <Argument>robjSheet: Input-Output</Argument>
''' <Argument>vintTopLeftRowIndex: Input</Argument>
''' <Argument>vintTopLeftColumnIndex: Input</Argument>
''' <Argument>vobjSuppressingColumnsDic: Input</Argument>
''' <Return>Excel.Range</Return>
Private Function mfobjGetInfosRangeAndSetColumnValues(ByRef rvarColumnValues As Variant, _
        ByRef robjSheet As Excel.Worksheet, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long, _
        ByVal vintCountOfFieldInfos As Long, _
        Optional ByVal vobjSuppressingColumnsDic As Scripting.Dictionary = Nothing) As Excel.Range


    Dim objInfosRange As Excel.Range
    
    If Not vobjSuppressingColumnsDic Is Nothing Then
    
        Set objInfosRange = robjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Resize(vintCountOfFieldInfos - vobjSuppressingColumnsDic.Count, 1)
    
        objInfosRange.Value = rvarColumnValues
    Else
    
        Set objInfosRange = robjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Resize(vintCountOfFieldInfos, 1)
    
        objInfosRange.Value = rvarColumnValues
    End If

    Set mfobjGetInfosRangeAndSetColumnValues = objInfosRange
End Function

'''
'''
'''
''' <Argument>robjInfosRange: Input-Output<Argument>
''' <Argument>venmManualDecoratingFieldTitlesRangePattern: Input<Argument>
''' <Argument>vsngFont: Input<Argument>
''' <Argument>vblnSetHorizontalAlignmentCenter: Input<Argument>
Private Sub msubDecorateInfosRangeOfFieldTitleInfos(ByRef robjInfosRange As Excel.Range, _
        Optional ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, _
        Optional ByVal vsngFont As Single = 9.5, _
        Optional ByVal vblnSetHorizontalAlignmentCenter As Boolean = False)


    DecorateRangeOfFieldTitleInformationsForManualGrids robjInfosRange, venmManualDecoratingFieldTitlesRangePattern
        
    With robjInfosRange
    
        .Font.Size = vsngFont
        
        If vblnSetHorizontalAlignmentCenter Then
        
            .HorizontalAlignment = xlCenter
        End If
    End With
End Sub

'''
'''
'''
Public Function NormalOutputRecordsetToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False, _
        Optional ByVal vblnAllowToAddAutoShapeInfoAboutBasicRecordSet As Boolean = True) As String
        
    
    Dim intRowsCountOfRecords As Long, objOutputDTCol As Collection, strNullColumnsDelimitedComma As String
    
    Dim objCompressedSheet As Excel.Worksheet, objFieldTitlesCol As Collection
    
    Dim objDoubleStopWatch As DoubleStopWatch
    
    
    If vblnAllowToAddAutoShapeInfoAboutBasicRecordSet Then
    
        Set objDoubleStopWatch = New DoubleStopWatch
        
        objDoubleStopWatch.MeasureStart
    End If
    
    Set objFieldTitlesCol = NormalOutputFieldTitlesFromRSetOnSheet(vobjSheet, _
            vobjRSet, _
            vintFieldTitlesRowIndex, _
            vintTopLeftColumnIndex, _
            vstrSuppressingColumnsDelimitedComma)
        
    intRowsCountOfRecords = NormalOutputOnlyRecordsetDataTableToCellsOnSheet(objOutputDTCol, _
            vobjSheet, _
            vobjRSet, _
            vintFieldTitlesRowIndex, _
            vintTopLeftColumnIndex, _
            vstrSuppressingColumnsDelimitedComma)
    
    If Not vobjFieldTitleToColumnWidthDic Is Nothing Then
    
        AdjustColumnWidthByTitleTextWithFindingRightDirection vobjSheet, _
                vobjFieldTitleToColumnWidthDic, _
                vintFieldTitlesRowIndex, _
                vintTopLeftColumnIndex
    End If
    
    strNullColumnsDelimitedComma = ""
    
    
    If vblnAllowToCreateNullColumnsCompressedTable And objOutputDTCol.Count > 0 Then
    
        strNullColumnsDelimitedComma = GetNullColumnsDelimitedCommaFromSqlResultDataTableCol(objOutputDTCol)
    
        If strNullColumnsDelimitedComma <> "" Then
        
            Set objCompressedSheet = GetNullColumnsCompressedSheet(vobjSheet)
        
            NormalOutputFieldTitlesFromColOnSheet objCompressedSheet, _
                    objFieldTitlesCol, _
                    vintFieldTitlesRowIndex, _
                    vintTopLeftColumnIndex, _
                    strNullColumnsDelimitedComma
            
            NormalOutputOnlyDTColToCellsOnSheet objCompressedSheet, _
                    objOutputDTCol, _
                    vintFieldTitlesRowIndex, _
                    vintTopLeftColumnIndex, _
                    strNullColumnsDelimitedComma
        End If
    End If
    
    If vblnAllowToAddAutoShapeInfoAboutBasicRecordSet Then
    
        objDoubleStopWatch.MeasureInterval
        
        AddAutoShapeGeneralMessage vobjSheet, "(Normal) Recordset output" & vbNewLine & mfstrGetBasicRecordSetLog(objFieldTitlesCol.Count, intRowsCountOfRecords, objDoubleStopWatch)
    End If
    
    NormalOutputRecordsetToCellsOnSheet = strNullColumnsDelimitedComma
End Function


'''
'''
'''
''' <Argument>vobjSheet: Input-Output</Argument>
''' <Argument>vobjRSet: Input</Argument>
''' <Argument>vintTopLeftRowIndex: Input</Argument>
''' <Argument>vintFieldTitlesColumnIndex: Input</Argument>
''' <Argument>vobjInfoTypeToColumnWidthDic: Input</Argument>
''' <Argument>vstrSuppressingColumnsDelimitedComma: Input</Argument>
''' <Argument>vblnAllowToCreateNullColumnsCompressedTable: Input</Argument>
''' <Argument>vblnAllowToAddAutoShapeInfoAboutBasicRecordSet: Input</Argument>
Public Function TransposeOutputRecordsetToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintFieldTitlesColumnIndex As Long = 1, _
        Optional ByVal vobjInfoTypeToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False, _
        Optional ByVal vblnAllowToAddAutoShapeInfoAboutBasicRecordSet As Boolean = True) As Long
        
    Dim intRowsCountOfRecords As Long, objOutputDTCol As Collection, objFieldTitlesCol As Collection
    Dim objDoubleStopWatch As DoubleStopWatch
    
    If vblnAllowToAddAutoShapeInfoAboutBasicRecordSet Then
    
        Set objDoubleStopWatch = New DoubleStopWatch
        
        objDoubleStopWatch.MeasureStart
    End If
    
    
    Set objFieldTitlesCol = TransposeOutputFieldTitlesFromRSetOnSheet(vobjSheet, _
            vobjRSet, _
            vintTopLeftRowIndex, _
            vintFieldTitlesColumnIndex, _
            vstrSuppressingColumnsDelimitedComma)
    
    
    intRowsCountOfRecords = TransposeOutputOnlyRecordsetDataTableToCellsOnSheet(objOutputDTCol, _
            vobjSheet, _
            vobjRSet, _
            vintTopLeftRowIndex, _
            vintFieldTitlesColumnIndex, _
            vstrSuppressingColumnsDelimitedComma)
    
    
    If Not vobjInfoTypeToColumnWidthDic Is Nothing Then
    
        AdjustColumnWidthByInfoTypeWithFindingRightDirectionForTransposeOutput vobjSheet, _
                vobjInfoTypeToColumnWidthDic, _
                vintTopLeftRowIndex, _
                vintFieldTitlesColumnIndex
    End If
    
    
    If vblnAllowToCreateNullColumnsCompressedTable And objOutputDTCol.Count > 0 Then
    
    
    
    End If
    
    If vblnAllowToAddAutoShapeInfoAboutBasicRecordSet Then
    
        objDoubleStopWatch.MeasureInterval
        
        AddAutoShapeGeneralMessage vobjSheet, "Transpose Recordset output" & vbNewLine & mfstrGetBasicRecordSetLog(objFieldTitlesCol.Count, intRowsCountOfRecords, objDoubleStopWatch)
    End If
    
    TransposeOutputRecordsetToCellsOnSheet = intRowsCountOfRecords
End Function

'''
'''
'''
''' <Argument>rintCountOfFieldTitles: Input</Argument>
''' <Argument>rintCountOfRecords: Input</Argument>
''' <Argument>robjDoubleStopWatch: Input</Argument>
''' <Return>String: log text</Return>
Private Function mfstrGetBasicRecordSetLog(ByRef rintCountOfFieldTitles As Long, _
        ByRef rintCountOfRecords As Long, _
        ByRef robjDoubleStopWatch As DoubleStopWatch) As String


    Dim strLog As String: strLog = ""
    
    strLog = "Count of fields: " & CStr(rintCountOfFieldTitles) & vbNewLine
    
    strLog = strLog & "Count of records: " & CStr(rintCountOfRecords) & vbNewLine
    
    strLog = strLog & "Elapsed time of the basic table output on cells in sheet: " & robjDoubleStopWatch.ElapsedTimeByString

    mfstrGetBasicRecordSetLog = strLog
End Function




'''
'''
'''
Public Function NormalOutputFieldTitlesFromRSetOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Collection

    Dim varTitles As Variant, i As Long, objField As ADOR.Field, objRange As Excel.Range, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
    
    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean, objFieldTitlesCol As Collection
    
    
    Set objFieldTitlesCol = New Collection
    
    intColumnDecrement = 0
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    If vobjRSet.Fields.Count > 1 Then
    
        If vstrSuppressingColumnsDelimitedComma <> "" Then
        
            ReDim varTitles(1 To 1, 1 To vobjRSet.Fields.Count - objSuppressingColumnsDic.Count)
        Else
            ReDim varTitles(1 To 1, 1 To vobjRSet.Fields.Count)
        End If
        
        i = 1
        
        For Each objField In vobjRSet.Fields
        
            Select Case True
            
                Case vstrSuppressingColumnsDelimitedComma = "", Not objSuppressingColumnsDic.Exists(i)
                
                    varTitles(1, i - intColumnDecrement) = objField.Name
                    
                    objFieldTitlesCol.Add objField.Name
                Case Else
                    
                    intColumnDecrement = intColumnDecrement + 1
            End Select
            
            i = i + 1
        Next
        
        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).Resize(1, vobjRSet.Fields.Count - intColumnDecrement)
    
        objRange.Value = varTitles
    Else
        varTitles = vobjRSet.Fields.Item(0).Name
        
        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
    
        objRange.Value = varTitles
        
        objFieldTitlesCol.Add varTitles
    End If
    
    ' Temporary decoration
    
    DecorateRangeOfVariousMetaInfo objRange, WhiteFontGrayBgMeiryo
    
    
    Set NormalOutputFieldTitlesFromRSetOnSheet = objFieldTitlesCol
End Function

'''
'''
'''
Public Sub NormalOutputFieldTitlesFromColOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjInputCol As Collection, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")

    Dim varTitles As Variant, i As Long, objRange As Excel.Range, objSuppressingColumnsDic As Scripting.Dictionary
    
    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
    Dim varItem As Variant
    
    
    intColumnDecrement = 0
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    If vobjInputCol.Count > 1 Then
    
        If vstrSuppressingColumnsDelimitedComma <> "" Then
        
            ReDim varTitles(1 To 1, 1 To vobjInputCol.Count - objSuppressingColumnsDic.Count)
        Else
            ReDim varTitles(1 To 1, 1 To vobjInputCol.Count)
        End If
        
        i = 1
        
        For Each varItem In vobjInputCol
        
            Select Case True
            
                Case vstrSuppressingColumnsDelimitedComma = "", Not objSuppressingColumnsDic.Exists(i)
                
                    varTitles(1, i - intColumnDecrement) = varItem
                Case Else
                    
                    intColumnDecrement = intColumnDecrement + 1
            End Select
            
            i = i + 1
        Next
        
        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).Resize(1, vobjInputCol.Count - intColumnDecrement)
    
        objRange.Value = varTitles
    Else
        varTitles = vobjInputCol.Item(0)
        
        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
    
        objRange.Value = varTitles
    End If
    
    ' Temporary decoration
    
    DecorateRangeOfVariousMetaInfo objRange, WhiteFontGrayBgMeiryo
End Sub


'''
'''
'''
Public Function TransposeOutputFieldTitlesFromRSetOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Collection

    Dim varTitles As Variant, i As Long, objField As ADOR.Field, objRange As Excel.Range, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
    
    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean, objFieldTitlesCol As Collection
    
    
    
    Set objFieldTitlesCol = New Collection
    
    intColumnDecrement = 0
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    If vobjRSet.Fields.Count > 1 Then
    
        If vstrSuppressingColumnsDelimitedComma <> "" Then
    
            ReDim varTitles(1 To vobjRSet.Fields.Count - objSuppressingColumnsDic.Count, 1 To 1)
        Else
            ReDim varTitles(1 To vobjRSet.Fields.Count, 1 To 1)
        End If
        
        i = 1
        
        For Each objField In vobjRSet.Fields
        
            Select Case True
            
                Case vstrSuppressingColumnsDelimitedComma = "", Not objSuppressingColumnsDic.Exists(i)
                
                    varTitles(i - intColumnDecrement, 1) = objField.Name
                    
                    objFieldTitlesCol.Add objField.Name
                Case Else
                    
                    intColumnDecrement = intColumnDecrement + 1
            End Select
        
            i = i + 1
        Next
        
        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).Resize(vobjRSet.Fields.Count - intColumnDecrement, 1)
        
        objRange.Value = varTitles
    Else
        varTitles = vobjRSet.Fields.Item(0).Name
    
        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
        
        objRange.Value = varTitles
        
        objFieldTitlesCol.Add varTitles
    End If
    
    ' Temporary decoration
    
    DecorateRangeOfVariousMetaInfo objRange, WhiteFontGrayBgMeiryo
    
    
    Set TransposeOutputFieldTitlesFromRSetOnSheet = objFieldTitlesCol
End Function




'''
'''
'''
Public Sub TransposeOutputFieldTitlesFromColOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjInputCol As Collection, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")

    Dim varTitles As Variant, i As Long, objRange As Excel.Range, objSuppressingColumnsDic As Scripting.Dictionary
    
    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
    Dim varItem As Variant
    
    
    
    intColumnDecrement = 0
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    If vobjInputCol.Count > 1 Then
    
        If vstrSuppressingColumnsDelimitedComma <> "" Then
    
            ReDim varTitles(1 To vobjInputCol.Count - objSuppressingColumnsDic.Count, 1 To 1)
        Else
            ReDim varTitles(1 To vobjInputCol.Count, 1 To 1)
        End If
        
        i = 1
        
        For Each varItem In vobjInputCol
        
            Select Case True
            
                Case vstrSuppressingColumnsDelimitedComma = "", Not objSuppressingColumnsDic.Exists(i)
                
                    varTitles(i - intColumnDecrement, 1) = varItem
                Case Else
                    
                    intColumnDecrement = intColumnDecrement + 1
            End Select
        
            i = i + 1
        Next
        
        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).Resize(vobjInputCol.Count - intColumnDecrement, 1)
        
        objRange.Value = varTitles
    Else
        varTitles = vobjInputCol.Item(0)
    
        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
        
        objRange.Value = varTitles
    End If
    
    ' Temporary decoration
    
    DecorateRangeOfVariousMetaInfo objRange, WhiteFontGrayBgMeiryo
End Sub


'''
''' data-table sheet out with Recordset object
'''
Public Function NormalOutputOnlyRecordsetDataTableToCellsOnSheet(ByRef robjOutputDTCol As Collection, ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long


    Dim objRange As Excel.Range, objTopLeftDataTableRange As Excel.Range, intRowsCountOfRecords As Long, intColumnsCount As Long
    
    Dim strSuppressingColumns() As String
    
    
    
    intRowsCountOfRecords = 0
    
    With vobjSheet
    
        Set objTopLeftDataTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex + 1))
        
        If vobjRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
        
            If Not vobjRSet.BOF Then
            
                vobjRSet.MoveFirst
            End If
            
            If vstrSuppressingColumnsDelimitedComma <> "" Then
        
                strSuppressingColumns = Split(vstrSuppressingColumnsDelimitedComma, ",")
                
                intColumnsCount = vobjRSet.Fields.Count - UBound(strSuppressingColumns) - 1
            Else
                intColumnsCount = vobjRSet.Fields.Count
            End If
            
            
            intRowsCountOfRecords = MoveNextNormalCopyFromRecordSet(robjOutputDTCol, objTopLeftDataTableRange, vobjRSet, vstrSuppressingColumnsDelimitedComma)
            
            ' temporary decoration
            
            Set objRange = objTopLeftDataTableRange.Resize(intRowsCountOfRecords, intColumnsCount)
            
            DecorateRangeOfRecords objRange, False, RecordCellsBlacksAndGrayInsideHorizontal, 9.5
        End If
    End With
    
    NormalOutputOnlyRecordsetDataTableToCellsOnSheet = intRowsCountOfRecords
End Function


'''
''' data-table sheet out
'''
Public Function NormalOutputOnlyDTColToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjDTCol As Collection, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long


    Dim objRange As Excel.Range, objTopLeftDataTableRange As Excel.Range, intRowsCountOfRecords As Long, intColumnsCount As Long
    
    Dim strSuppressingColumns() As String
    
    intRowsCountOfRecords = 0
    
    With vobjSheet
    
        Set objTopLeftDataTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex + 1))
        
        
        If vstrSuppressingColumnsDelimitedComma <> "" Then
        
            strSuppressingColumns = Split(vstrSuppressingColumnsDelimitedComma, ",")
            
            intColumnsCount = vobjDTCol.Item(1).Count - UBound(strSuppressingColumns) - 1
        Else
            intColumnsCount = vobjDTCol.Item(1).Count
        End If
        
        intRowsCountOfRecords = MoveNextNormalCopyFromDTCol(objTopLeftDataTableRange, vobjDTCol, vstrSuppressingColumnsDelimitedComma)
        
        ' temporary decoration
        
        Set objRange = objTopLeftDataTableRange.Resize(intRowsCountOfRecords, intColumnsCount)
        
        DecorateRangeOfRecords objRange, False, RecordCellsBlacksAndGrayInsideHorizontal
    End With
    
    NormalOutputOnlyDTColToCellsOnSheet = intRowsCountOfRecords
End Function


'''
''' transposed data-table sheet out with Recordset object
'''
Public Function TransposeOutputOnlyRecordsetDataTableToCellsOnSheet(ByRef robjOutputDTCol As Collection, ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintFieldTitlesColumnIndex As Long = 1, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long


    Dim objRange As Excel.Range, objTopLeftDataTableRange As Excel.Range, intRowsCountOfRecords As Long, intColumnsCount As Long

    Dim strSuppressingColumns() As String
    
    
    intRowsCountOfRecords = 0
    
    With vobjSheet
    
        Set objTopLeftDataTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintFieldTitlesColumnIndex + 1) & CStr(vintTopLeftRowIndex))
        
        If vobjRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
        
            If Not vobjRSet.BOF Then
            
                vobjRSet.MoveFirst
            End If
            
            If vstrSuppressingColumnsDelimitedComma <> "" Then
        
                strSuppressingColumns = Split(vstrSuppressingColumnsDelimitedComma, ",")
                
                intColumnsCount = vobjRSet.Fields.Count - UBound(strSuppressingColumns) - 1
            Else
                intColumnsCount = vobjRSet.Fields.Count
            End If
            
            
            intRowsCountOfRecords = MoveNextTransposeCopyFromRecordSet(robjOutputDTCol, objTopLeftDataTableRange, vobjRSet, vstrSuppressingColumnsDelimitedComma)
            
            ' temporary decoration
            
            Set objRange = objTopLeftDataTableRange.Resize(intColumnsCount, intRowsCountOfRecords)
            
            DecorateRangeOfRecords objRange, False, RecordCellsBlacksAndGrayInsideVertical, 9.5, True, False
        End If
    End With
    
    TransposeOutputOnlyRecordsetDataTableToCellsOnSheet = intRowsCountOfRecords
End Function


'''
''' Substitute this for Range.CopyFromRecordSet
'''
''' because the CopyFromRecordSet causes often some errors which is hard to understand
'''
''' <Argument>robjOutputDTCol: Output<Argument>
''' <Argument>vobjRecordTopLeftRange: Output - it is also the output Excel.Worksheet<Argument>
''' <Argument>vobjRSet: Input<Argument>
''' <Argument>vstrSuppressingColumnsDelimitedComma: Input<Argument>
Public Function MoveNextTransposeCopyFromRecordSet(ByRef robjOutputDTCol As Collection, _
        ByVal vobjRecordTopLeftRange As Excel.Range, _
        ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long


    Dim objSheet As Excel.Worksheet, intRowIdx As Long, intColumnIdx As Long
    Dim intRecordTopLeftRowIndex As Long, intRecordTopLeftColumnIndex As Long, intColumnsCountOfDataTable As Long
    Dim i As Long
    
    Dim objDTCol As Collection, objRowValues As Collection, varTransposeValues As Variant
    Dim objUnionRange As Excel.Range, objRange As Excel.Range, intCountOfDataTableRows As Long
    Dim intRowsCountOfRecordset As Long
    
    Set objSheet = vobjRecordTopLeftRange.Worksheet
    
    
    intRowIdx = intRecordTopLeftRowIndex
    
    With vobjRecordTopLeftRange
    
        intRecordTopLeftRowIndex = .Row
        
        intRecordTopLeftColumnIndex = .Column
    End With
    
    intColumnsCountOfDataTable = vobjRSet.Fields.Count
    
    TransposeConvertRSetToCollectionAndGetStringRanges objDTCol, objUnionRange, vobjRSet, objSheet, intRecordTopLeftRowIndex, intRecordTopLeftColumnIndex, vstrSuppressingColumnsDelimitedComma
    
    Set robjOutputDTCol = objDTCol
    
    On Error Resume Next

    If Not objUnionRange Is Nothing Then
    
        objUnionRange.NumberFormatLocal = "@"
    End If
    
    TransposeOutputOnlyDTCol objDTCol, intColumnsCountOfDataTable, objSheet, intRecordTopLeftRowIndex, intRecordTopLeftColumnIndex
    
    intRowsCountOfRecordset = objDTCol.Count
    
    On Error GoTo 0
    
    MoveNextTransposeCopyFromRecordSet = intRowsCountOfRecordset
End Function




'''
'''
'''
Public Sub TransposeConvertDTColToCollectionAndGetStringRanges(ByRef robjOutputCol As Collection, _
        ByRef robjStringUnionRange As Excel.Range, _
        ByRef robjInputDTCol As VBA.Collection, _
        ByRef robjSheet As Excel.Worksheet, _
        ByRef rintRecordTopLeftRowIndex As Long, _
        ByRef rintRecordTopLeftColumnIndex As Long, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")
        
    On Error Resume Next
    
    Dim objField As ADOR.Field, i As Long, objRowCol As Collection, varInputRowCol As Variant, objInputRowCol As Collection, varInputItem As Variant
    Dim intColumnIdx As Long, intRowIdx As Long, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
    
    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
    
    
    intRowIdx = rintRecordTopLeftRowIndex
    
    intColumnDecrement = 0
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    
    
    Set robjOutputCol = New Collection
    
    Set robjStringUnionRange = Nothing
    
    
    For Each varInputRowCol In robjInputDTCol
    
        Set objRowCol = New Collection
    
        i = 0
    
        Set objInputRowCol = varInputRowCol
    
        For Each varInputItem In objInputRowCol
        
            intColumnIdx = rintRecordTopLeftColumnIndex + i
        
            blnContinueToAddValue = True
            
            If vstrSuppressingColumnsDelimitedComma <> "" Then
            
                If objSuppressingColumnsDic.Exists(intColumnIdx) Then
                
                    blnContinueToAddValue = False
                
                    intColumnDecrement = intColumnDecrement + 1
                End If
            End If
            
            If blnContinueToAddValue Then
        
                SetTransposeRowColValueAndStringUnionRange objRowCol, robjStringUnionRange, varInputItem, robjSheet, intRowIdx, intColumnIdx, intColumnDecrement
            End If
            
            i = i + 1
        Next
        
        robjOutputCol.Add objRowCol
        
        intRowIdx = intRowIdx + 1
    Next

            
    On Error GoTo 0
End Sub



'''
'''
'''
''' <Argument>robjDTCol: Output</Argument>
''' <Argument>robjStringUnionRange: Output</Argument>
''' <Argument>robjRSet: Input - For example, after SQL executed</Argument>
''' <Argument>robjSheet: Input</Argument>
''' <Argument>rintRecordTopLeftRowIndex: Input</Argument>
''' <Argument>rintRecordTopLeftColumnIndex: Input</Argument>
''' <Argument>vstrSuppressingColumnsDelimitedComma: Input</Argument>
Public Sub TransposeConvertRSetToCollectionAndGetStringRanges(ByRef robjDTCol As Collection, _
        ByRef robjStringUnionRange As Excel.Range, _
        ByRef robjRSet As ADODB.Recordset, _
        ByRef robjSheet As Excel.Worksheet, _
        ByRef rintRecordTopLeftRowIndex As Long, _
        ByRef rintRecordTopLeftColumnIndex As Long, _
        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")
        
    On Error Resume Next
    
    Dim objField As ADOR.Field, i As Long, objRowCol As Collection
    Dim intColumnIdx As Long, intRowIdx As Long, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
    
    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
    
    
    intRowIdx = rintRecordTopLeftRowIndex
    
    intColumnDecrement = 0
    
    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
    
    
    
    Set robjDTCol = New Collection
    
    Set robjStringUnionRange = Nothing
    
    With robjRSet
    
        While Not .EOF
        
            Set objRowCol = New Collection
        
            i = 0
        
            For Each objField In .Fields
            
                intColumnIdx = rintRecordTopLeftColumnIndex + i
            
                blnContinueToAddValue = True
                
                If vstrSuppressingColumnsDelimitedComma <> "" Then
                
                    If objSuppressingColumnsDic.Exists(intColumnIdx) Then
                    
                        blnContinueToAddValue = False
                    
                        intColumnDecrement = intColumnDecrement + 1
                    End If
                End If
                
                If blnContinueToAddValue Then
            
                    With objField
                    
                        SetTransposeRowColValueAndStringUnionRange objRowCol, robjStringUnionRange, .Value, robjSheet, intRowIdx, intColumnIdx, intColumnDecrement
                    End With
                End If
                
                i = i + 1
            Next
            
            robjDTCol.Add objRowCol
            
            intRowIdx = intRowIdx + 1
            
            .MoveNext
        Wend
    End With
            
    On Error GoTo 0
End Sub



'''
'''
'''
''' <Argument>robjRowCol: Output</Argument>
''' <Argument>robjStringUnionRange: Output-Input</Argument>
''' <Argument>rvarValue: Input</Argument>
''' <Argument>robjSheet: Input</Argument>
''' <Argument>rintRowIdx: Input</Argument>
''' <Argument>rintColumnIdx: Input</Argument>
''' <Argument>rintColumnDecrement: Input</Argument>
Public Sub SetTransposeRowColValueAndStringUnionRange(ByRef robjRowCol As Collection, _
        ByRef robjStringUnionRange As Excel.Range, _
        ByRef rvarValue As Variant, _
        ByRef robjSheet As Excel.Worksheet, _
        ByRef rintRowIdx As Long, _
        ByRef rintColumnIdx As Long, _
        ByRef rintColumnDecrement As Long)


    robjRowCol.Add rvarValue
                    
    If IsNumeric(rvarValue) Then
        
        If TypeName(rvarValue) = "String" Then
            
            If StrComp(Left(rvarValue, 1), "0") = 0 Then
            
                If robjStringUnionRange Is Nothing Then
                                        
                    ' transposed
                    
                    Set robjStringUnionRange = robjSheet.Cells(rintColumnIdx - rintColumnDecrement, rintRowIdx)
                Else
                    ' transposed
                
                    Set robjStringUnionRange = Union(robjStringUnionRange, robjSheet.Cells(rintColumnIdx - rintColumnDecrement, rintRowIdx))
                End If
            End If
        End If
    End If
End Sub


