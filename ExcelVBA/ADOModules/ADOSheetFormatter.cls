VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOSheetFormatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Excel.Worksheet decoration format parameters for outputting ADO Recordset data-table on cells in the Worksheet
'   SQL query logging parameters data object
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both the ADO and the EXCEL
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  7/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'       Wed,  9/Aug/2023    Kalmclaeyd Tarclanus    Added
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Variable declarations
'**---------------------------------------------
Private mobjSheetFormatter As DataTableSheetFormatter

Private mstrSQL As String


Private menmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType    ' for SQL query , which is the SELECT sentenve

Private menmExecutedAdoSqlCommandLogPositionType As ExecutedAdoSqlCommandLogPositionType ' for SQL command, such as UPDATE, INSERT, DELETE, etc all

Private mstrExclusiveSQLLogSheetName As String

'**---------------------------------------------
'** Temporary state
'**---------------------------------------------
Private mblnSupportedPropertyRecordCount As Boolean

Private mintExcelSheetOutputTime1 As Long

Private mintExcelSheetOutputTime2 As Long


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjSheetFormatter = New DataTableSheetFormatter

    mstrSQL = ""

    mblnSupportedPropertyRecordCount = False
    
    menmExecutedAdoSqlQueryLogPositionType = SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
    
    mstrExclusiveSQLLogSheetName = "ExclusiveSQLLogs"
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter

    Set DataTableSheetFormattingInterface = mobjSheetFormatter
End Property

'**---------------------------------------------
'** For general
'**---------------------------------------------
Public Property Get FieldTitlesRowIndex() As Long

    FieldTitlesRowIndex = mobjSheetFormatter.FieldTitlesRowIndex
End Property
Public Property Let FieldTitlesRowIndex(ByVal vintFieldTitlesRowIndex As Long)

    mobjSheetFormatter.FieldTitlesRowIndex = vintFieldTitlesRowIndex
End Property


Public Property Get TopLeftRowIndex() As Long

    TopLeftRowIndex = mobjSheetFormatter.TopLeftRowIndex
End Property
Public Property Let TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)

    mobjSheetFormatter.TopLeftRowIndex = vintTopLeftRowIndex
End Property
Public Property Get TopLeftColumnIndex() As Long

    TopLeftColumnIndex = mobjSheetFormatter.TopLeftColumnIndex
End Property
Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)

    mobjSheetFormatter.TopLeftColumnIndex = vintTopLeftColumnIndex
End Property


Public Property Let AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)

    mobjSheetFormatter.AllowToShowFieldTitle = vblnAllowToShowFieldTitle
End Property
Public Property Get AllowToShowFieldTitle() As Boolean

    AllowToShowFieldTitle = mobjSheetFormatter.AllowToShowFieldTitle
End Property

Public Property Let FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)

    mobjSheetFormatter.FieldTitleInteriorType = venmFieldTitleInteriorType
End Property
Public Property Get FieldTitleInteriorType() As FieldTitleInterior

    FieldTitleInteriorType = mobjSheetFormatter.FieldTitleInteriorType
End Property

Public Property Let RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)

    mobjSheetFormatter.RecordBordersType = venmRecordCellsBordersType
End Property
Public Property Get RecordBordersType() As RecordCellsBorders

    RecordBordersType = mobjSheetFormatter.RecordBordersType
End Property

Public Property Let RecordsFontSize(ByVal vsngRecordsFontSize As Single)

    mobjSheetFormatter.RecordsFontSize = vsngRecordsFontSize
End Property
Public Property Get RecordsFontSize() As Single

    RecordsFontSize = mobjSheetFormatter.RecordsFontSize
End Property

'**---------------------------------------------
'** ADO dependent properties
'**---------------------------------------------
Public Property Get SupportedPropertyRecordCount() As Boolean

    SupportedPropertyRecordCount = mblnSupportedPropertyRecordCount
End Property

'''
''' output SQL SELECT result ADODB.Recordset datatable to whether outputted data-table Excel sheet or the other SQL log sheet, or output it on some AutoShapes with the outputted data-table
'''
Public Property Get ExecutedAdoSqlQueryLogPosition() As ExecutedAdoSqlQueryLogPositionType

    ExecutedAdoSqlQueryLogPosition = menmExecutedAdoSqlQueryLogPositionType
End Property
Public Property Let ExecutedAdoSqlQueryLogPosition(ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType)

    menmExecutedAdoSqlQueryLogPositionType = venmExecutedAdoSqlQueryLogPositionType
End Property

'''
''' output SQL command result to cells on logging Excel sheet
'''
Public Property Get ExecutedAdoSqlCommandLogPosition() As ExecutedAdoSqlCommandLogPositionType

    ExecutedAdoSqlCommandLogPosition = menmExecutedAdoSqlCommandLogPositionType
End Property
Public Property Let ExecutedAdoSqlCommandLogPosition(ByVal venmExecutedAdoSqlCommandLogPositionType As ExecutedAdoSqlCommandLogPositionType)

    menmExecutedAdoSqlCommandLogPositionType = venmExecutedAdoSqlCommandLogPositionType
End Property

'''
'''
'''
Public Property Get ExclusiveSQLLogSheetName() As String

    ExclusiveSQLLogSheetName = mstrExclusiveSQLLogSheetName
End Property

Public Property Let ExclusiveSQLLogSheetName(ByVal vstrExclusiveSQLLogSheetName As String)

    mstrExclusiveSQLLogSheetName = vstrExclusiveSQLLogSheetName
End Property

'**---------------------------------------------
'** Properties - connoted sheet-formatting objects or paramters
'**---------------------------------------------

Public Property Let AllowToWriteInsertTextInSheetHeader(ByVal vblnAllowToWriteInsertTextInSheetHeader As Boolean)

    mobjSheetFormatter.AllowToWriteInsertTextInSheetHeader = vblnAllowToWriteInsertTextInSheetHeader
End Property
Public Property Get AllowToWriteInsertTextInSheetHeader() As Boolean

    AllowToWriteInsertTextInSheetHeader = mobjSheetFormatter.AllowToWriteInsertTextInSheetHeader
End Property

Public Property Set HeaderInsertTexts(ByVal vobjHeaderInsertTexts As Collection)

    Set mobjSheetFormatter.HeaderInsertTexts = vobjHeaderInsertTexts
End Property
Public Property Get HeaderInsertTexts() As Collection

    Set HeaderInsertTexts = mobjSheetFormatter.HeaderInsertTexts
End Property


Public Property Let AllowToSetAutoFilter(ByVal vblnAllowToSetAutoFilter As Boolean)

    mobjSheetFormatter.AllowToSetAutoFilter = vblnAllowToSetAutoFilter
End Property
Public Property Get AllowToSetAutoFilter() As Boolean

    AllowToSetAutoFilter = mobjSheetFormatter.AllowToSetAutoFilter
End Property

'''
''' Dictionary(Of String(column title), Double(column-width))
'''
Public Property Set FieldTitleToColumnWidthDic(ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary)

    Set mobjSheetFormatter.FieldTitleToColumnWidthDic = vobjFieldTitleToColumnWidthDic
End Property
Public Property Get FieldTitleToColumnWidthDic() As Scripting.Dictionary

    Set FieldTitleToColumnWidthDic = mobjSheetFormatter.FieldTitleToColumnWidthDic
End Property

'''
''' Dictionary(Of String(column title order, such as 'Col1', 'Col07'), Double(column-width))
'''
Public Property Set FieldTitleOrderToColumnWidthDic(ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary)

    Set mobjSheetFormatter.FieldTitleOrderToColumnWidthDic = vobjFieldTitleOrderToColumnWidthDic
End Property
Public Property Get FieldTitleOrderToColumnWidthDic() As Scripting.Dictionary

    Set FieldTitleOrderToColumnWidthDic = mobjSheetFormatter.FieldTitleOrderToColumnWidthDic
End Property


'''
''' When it is true, then ConvertDataForColumns procedure is used.
'''
Public Property Get AllowToConvertDataForSpecifiedColumns() As Boolean

    AllowToConvertDataForSpecifiedColumns = mobjSheetFormatter.AllowToConvertDataForSpecifiedColumns
End Property
Public Property Let AllowToConvertDataForSpecifiedColumns(ByVal vblnAllowToConvertDataForSpecifiedColumns As Boolean)

    mobjSheetFormatter.AllowToConvertDataForSpecifiedColumns = vblnAllowToConvertDataForSpecifiedColumns
End Property
'''
''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
'''
Public Property Get FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary

    Set FieldTitlesToColumnDataConvertTypeDic = mobjSheetFormatter.FieldTitlesToColumnDataConvertTypeDic
End Property
Public Property Set FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)

    Set mobjSheetFormatter.FieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
End Property


'''
''' ColumnsNumberFormatLocalParam
'''
Public Property Set ColumnsNumberFormatLocal(ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)

    Set mobjSheetFormatter.ColumnsNumberFormatLocal = vobjColumnsNumberFormatLocalParam
End Property
Public Property Get ColumnsNumberFormatLocal() As ColumnsNumberFormatLocalParam

    Set ColumnsNumberFormatLocal = mobjSheetFormatter.ColumnsNumberFormatLocal
End Property

'''
''' set format-condition for each column
'''
Public Property Set ColumnsFormatCondition(ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)

    Set mobjSheetFormatter.ColumnsFormatCondition = vobjColumnsFormatConditionParam
End Property
Public Property Get ColumnsFormatCondition() As ColumnsFormatConditionParam

    Set ColumnsFormatCondition = mobjSheetFormatter.ColumnsFormatCondition
End Property

'''
''' merge cells when the same values continue
'''
Public Property Let AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)

    mobjSheetFormatter.AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
End Property
Public Property Get AllowToMergeCellsByContinuousSameValues() As Boolean

    AllowToMergeCellsByContinuousSameValues = mobjSheetFormatter.AllowToMergeCellsByContinuousSameValues
End Property
Public Property Let AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)

    mobjSheetFormatter.AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
End Property
Public Property Get AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean

    AllowToSetVerticalTopAlignmentTopOfMergeCells = mobjSheetFormatter.AllowToSetVerticalTopAlignmentTopOfMergeCells
End Property

Public Property Set FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)

    Set mobjSheetFormatter.FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
End Property
Public Property Get FieldTitlesToMergeCells()

    Set FieldTitlesToMergeCells = mobjSheetFormatter.FieldTitlesToMergeCells
End Property


'''
''' Enabling WrapText for each cells
'''
Public Property Let AllowToSetEnablingWrapText(ByVal vblnAllowToSetEnablingWrapText As Boolean)

    mobjSheetFormatter.AllowToSetEnablingWrapText = vblnAllowToSetEnablingWrapText
End Property
Public Property Get AllowToSetEnablingWrapText() As Boolean

    AllowToSetEnablingWrapText = mobjSheetFormatter.AllowToSetEnablingWrapText
End Property

Public Property Set FieldTitlesToSetEnablingWrapTextCells(ByVal vobjFieldTitlesToSetEnablingWrapTextCells As Collection)

    Set mobjSheetFormatter.FieldTitlesToSetEnablingWrapTextCells = vobjFieldTitlesToSetEnablingWrapTextCells
End Property
Public Property Get FieldTitlesToSetEnablingWrapTextCells() As Collection

    Set FieldTitlesToSetEnablingWrapTextCells = mobjSheetFormatter.FieldTitlesToSetEnablingWrapTextCells
End Property

'''
''' If mblnAllowToAutoFitRowsHeight is true, Excel.Worksheet.Rows.AutoFit method is to be executed
'''
Public Property Let AllowToAutoFitRowsHeight(ByVal vblnAllowToAutoFitRowsHeight As Boolean)

    mobjSheetFormatter.AllowToAutoFitRowsHeight = vblnAllowToAutoFitRowsHeight
End Property
Public Property Get AllowToAutoFitRowsHeight() As Boolean

    AllowToAutoFitRowsHeight = mobjSheetFormatter.AllowToAutoFitRowsHeight
End Property

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** For general
'**---------------------------------------------
'''
'''
'''
Public Sub SetSheetFormatBeforeTableOut(Optional ByVal vblnExecutedSQLLogExists As Boolean = True)

    Dim intRowIndex As Long
    
    Const intCountOfSQLQueryLogCellsRows As Long = 6


    intRowIndex = Me.TopLeftRowIndex

    If menmExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable Then

        If Not Me.HeaderInsertTexts Is Nothing And Me.AllowToWriteInsertTextInSheetHeader Then
        
            intRowIndex = intRowIndex + Me.HeaderInsertTexts.Count
        End If
    
        If vblnExecutedSQLLogExists Then
        
            intRowIndex = intRowIndex + intCountOfSQLQueryLogCellsRows
        End If
    End If
    
    If Not Me.AllowToShowFieldTitle Then
    
        intRowIndex = intRowIndex - 1
    End If
    
    Me.FieldTitlesRowIndex = intRowIndex
End Sub

'''
'''
'''
Public Sub SetSheetFormatAfterTableOut(ByVal vobjSheet As Excel.Worksheet)

    mobjSheetFormatter.SetDataTableSheetFormatAfterTableOut vobjSheet
End Sub


'**---------------------------------------------
'** ADO dependent operations
'**---------------------------------------------
'''
'''
'''
Public Sub InsertInputtedHeaderTexts(ByVal vobjSheet As Excel.Worksheet)

    ' The SQL string is written at the first row of the sheet

    mobjSheetFormatter.InsertInputtedHeaderTexts vobjSheet, True
End Sub
'''
'''
'''
Public Sub PreparationForRecordSet(ByVal vobjSheet As Excel.Worksheet, ByVal vobjRSet As ADODB.Recordset, ByRef robjSQLRes As SQLResult)

    If Not robjSQLRes Is Nothing Then
    
        DetectRecordsetRecordsCountState mblnSupportedPropertyRecordCount, robjSQLRes, vobjRSet
    
        GetFieldCountsAndStartToMeasureTimeOfExpandingToSheet robjSQLRes, vobjRSet, mintExcelSheetOutputTime1
    End If
End Sub


'''
'''
'''
Public Sub ResultOutForAdoSqlExecution(ByVal vobjOutputtedDataTableOrLogSheet As Excel.Worksheet, _
        ByRef robjSQLRes As SQLResult, _
        Optional ByVal venmCQRSClassification As SqlRdbCQRSClassification = SqlRdbCQRSClassification.RdbSqlQueryType, _
        Optional ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses = Nothing)
    
    Dim intSQLLogStartingRowIndex As Long
    
    If Not robjSQLRes Is Nothing Then
    
        GetMeasureTimeOfExpandedRecordsetToSheet robjSQLRes, mintExcelSheetOutputTime1, mintExcelSheetOutputTime2

        'Debug.Print "Excel sheet processing elapsed time : " & CStr(CSng(robjSQLRes.ExcelSheetOutputElapsedTime) / 1000#) & " [s]"
        
        With Me
        
            Select Case venmCQRSClassification
            
                Case SqlRdbCQRSClassification.RdbSqlQueryType
            
                    OutputAdoSqlQueryResultLogsToSheet vobjOutputtedDataTableOrLogSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, .HeaderInsertTexts, .ExecutedAdoSqlQueryLogPosition, .TopLeftRowIndex, .TopLeftColumnIndex, RdbSqlQueryType, mstrExclusiveSQLLogSheetName
                
                Case SqlRdbCQRSClassification.RdbSqlCommandType
                
                    ' In the SQL command log case, the sheet-name has been already decided.
                
                    OutputAdoSqlCommandResultLogsToCellsOnSpecifiedSQLLogSheet vobjOutputtedDataTableOrLogSheet, robjSQLRes, .HeaderInsertTexts, .TopLeftRowIndex, .TopLeftColumnIndex
            End Select
        End With
    End If
End Sub

