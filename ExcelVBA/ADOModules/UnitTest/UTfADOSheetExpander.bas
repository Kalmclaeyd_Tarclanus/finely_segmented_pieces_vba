Attribute VB_Name = "UTfADOSheetExpander"
'
'   Testing for ADOSheetExpander using both XlAdoSheetExpander and CsvAdoSheetExpander
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on UTfDecorationSetterToXlSheet.bas, UTfCreateDataTable.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 11/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToUTfADOSheetExpander()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
    
    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", mfstrGetModuleNamesToOpenInVBE()
End Sub

'''
'''
'''
Private Function mfstrGetModuleNamesToOpenInVBE() As String

    Dim strNames As String
    
    strNames = "DataTableSheetRangeAddresses,ADOConnectingInClasses,AdoRSetSheetExpander,ADOSheetFormatter,ColumnsNumberFormatLocalParam,"

    strNames = strNames & "AdoRSetSheetExpanding,AppendAdoRsetOnSheet,ADOSheetOut,ADOLogTextSheetOut,"

    strNames = strNames & "XlAdoSheetExpander,CsvAdoSheetExpander,ADOAsyncConnector,SqlUTfXlSheetExpander,SqlUTfCsvAdoSheetExpander,"

    strNames = strNames & "ADOConnector,IADOConnector,IADOConnectStrGenerator,IADOConnectStrGenerator,XlAdoConnector,CsvAdoConnector,"

    strNames = strNames & "IOutputBookPath,IExpandAdoRecordsetOnSheet"
    

    mfstrGetModuleNamesToOpenInVBE = strNames
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToInterfaceObject()

    Dim itfADOConnector As IADOConnector
    
    Dim objAdoRecordsetSheetExpander As AdoRSetSheetExpander
    
    Debug.Print objAdoRecordsetSheetExpander Is Nothing
    
    Debug.Print itfADOConnector Is Nothing
End Sub

'**---------------------------------------------
'** About comprehensive parameters
'**---------------------------------------------
'''
''' About XlAdoSheetExpander
'''
Private Sub msubComprehensiveTestToXlAdoSheetExpander()


    Dim varRowCol As Variant, objRowCol As Collection, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    
    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet, strSheetName As String, i As Long
    
    Dim strSQL As String
    
    Dim varAllowToShowFieldTitle As Variant, varAllowToRecordSQLLog As Variant, blnAfterAllowToRecordSQLLog As Boolean
    
    Dim objAdditionalLogDic As Scripting.Dictionary
    
    Dim varExecutedAdoSqlQueryLogPositionType As Variant, enmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType
    
    
    strOutputBookPath = mfstrGetSheetDecorationTestBookDir() & "\XlAdoSheetExpanderOutputComprensiveTest.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
    
    i = 1
    
    With New XlAdoSheetExpander

        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()

        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
 
        For Each varAllowToRecordSQLLog In GetBooleanColFromLineDelimitedChar("True,False")

            For Each varExecutedAdoSqlQueryLogPositionType In GetAllExecutedAdoSqlQueryLogPositionTypesCol()
    
                enmExecutedAdoSqlQueryLogPositionType = varExecutedAdoSqlQueryLogPositionType
    
                For Each varAllowToShowFieldTitle In GetBooleanColFromLineDelimitedChar("True,False")
        
                    For Each varRowCol In GetDataTableSheetCreateTestConditionOnlyTopLeftDTCol(5, 7)
                    
                        Set objRowCol = varRowCol
                    
                        GetDataTableTopLeftWhenCreateTableOnSheet intTopLeftRowIndex, intTopLeftColumnIndex, objRowCol
                    
                        
                        .TopLeftRowIndex = intTopLeftRowIndex
                        
                        .TopLeftColumnIndex = intTopLeftColumnIndex
                        
                        .AllowToShowFieldTitle = varAllowToShowFieldTitle
                        
                        .AllowToRecordSQLLog = varAllowToRecordSQLLog
                        
                        .RecordBordersType = RecordCellsGrayAll
                        
                        
                        If .AllowToShowFieldTitle Then
                        
                            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("A_Column,12,B_Column,14")
                        Else
                            Set .FieldTitleOrderToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Col1,12,Col2,14")
                        End If
                        
                        
                        Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
                        
                        strSheetName = "XlSQLTest_" & Format(i, "00")
                        
                        objSheet.Name = strSheetName
                        
                        .OutputToSheetFrom strSQL, objSheet, enmExecutedAdoSqlQueryLogPositionType
                        
                        blnAfterAllowToRecordSQLLog = .AllowToRecordSQLLog
                        
                        
                        Set objAdditionalLogDic = New Scripting.Dictionary
                        
                        With objAdditionalLogDic
                        
                            .Add "Input AllowToRecordSQLLog", varAllowToRecordSQLLog
                            
                            .Add "After-SQL-executed AllowToRecordSQLLog", blnAfterAllowToRecordSQLLog
                        
                            .Add "ExecutedAdoSqlQueryLogPositionType", GetExecutedAdoSqlQueryLogPositionTypeTextFromEnm(enmExecutedAdoSqlQueryLogPositionType)
                            
                            .Add "AllowToShowFieldTitle", varAllowToShowFieldTitle
                            
                            .Add "TopLeftRowIndex", intTopLeftRowIndex
                            
                            .Add "TopLeftColumnIndex", intTopLeftColumnIndex
                        End With
                        
                        OutputDicToAutoShapeLogTextOnSheet objSheet, objAdditionalLogDic, "Excel sheet connecting ADOSheetFormatter some parameters test"
                        
                        i = i + 1
                    Next
                Next
            Next
        Next
    End With

    DeleteDummySheetWhenItExists objBook
End Sub


'**---------------------------------------------
'** About a specialized parameters combination
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToXlAdoSheetExpanderWithSomeParameters()

    Dim varRowCol As Variant, objRowCol As Collection, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet, strSheetName As String, i As Long
    Dim strSQL As String
    
    Dim varAllowToShowFieldTitle As Variant
    Dim varAllowToRecordSQLLog As Variant, blnAfterAllowToRecordSQLLog As Boolean
    
    Dim varExecutedAdoSqlQueryLogPositionType As Variant, enmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType
    
    strOutputBookPath = mfstrGetSheetDecorationTestBookDir() & "\SomeParametersXlAdoSheetExpanderOutputSanityTest.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
    
    
    i = 1
    
    With New XlAdoSheetExpander

        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()

        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
 
        '---
        
        intTopLeftRowIndex = 5: intTopLeftColumnIndex = 7
        
        varAllowToRecordSQLLog = True
 
        enmExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
        
        varAllowToShowFieldTitle = True
        'varAllowToShowFieldTitle = False
        
                        
        
        .TopLeftRowIndex = intTopLeftRowIndex
        
        .TopLeftColumnIndex = intTopLeftColumnIndex
        
        .AllowToShowFieldTitle = varAllowToShowFieldTitle
        
        .AllowToRecordSQLLog = varAllowToRecordSQLLog
        
        .RecordBordersType = RecordCellsGrayAll
        
        
        Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
        
        strSheetName = "XlSQLTest_" & Format(i, "00")
        
        objSheet.Name = strSheetName
        
        .OutputToSheetFrom strSQL, objSheet, enmExecutedAdoSqlQueryLogPositionType
    End With

    DeleteDummySheetWhenItExists objBook
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** About file-path
'**---------------------------------------------
'''
'''
'''
Private Function mfstrGetSheetDecorationTestBookDir() As String

    Dim strDir As String

    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\ADOSheetFormatterTest"

    ForceToCreateDirectory strDir
    
    mfstrGetSheetDecorationTestBookDir = strDir
End Function





