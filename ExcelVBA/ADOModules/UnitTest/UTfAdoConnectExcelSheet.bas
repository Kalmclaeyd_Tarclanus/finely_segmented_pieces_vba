Attribute VB_Name = "UTfAdoConnectExcelSheet"
'
'   Sanity tests for USetAdoOleDbConStrForXlBook.frm
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'       Dependent on UTfDataTableSecurity.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 25/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrSheetName As String = "TestSheet"


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** XlAdoSheetExpander tests, which need to open the password locked Excel book by Microsoft Excel specification
'**---------------------------------------------
'''
''' Use a User-form
'''
Private Sub msubSanityTestToXlAdoSheetExpanderWithForm()

    Dim strBookPath As String, strSQL As String

    strBookPath = mfstrPrepareTestBookWithLockPassword()
    
    With New XlAdoConnector
    
        ' input the password by a User-form
    
        If .IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm("TestAccOleDbToExcelWithPasswordKey", strBookPath) Then
    
            strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
    
            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
        End If
    End With
End Sub

Private Sub msubSanityTestToXlAdoSheetExpander()

    Dim strBookPath As String, strSQL As String

    strBookPath = mfstrPrepareTestBookWithLockPassword()
    
    With New XlAdoSheetExpander
    
        ' In the case, input the password directly
    
        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
        
        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"

        .OutputInExistedBookByUnitTestMode strSQL, "TestSQLResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
    End With
End Sub

'**---------------------------------------------
'** USetAdoOleDbConStrForXlBook form tests
'**---------------------------------------------
'''
''' delete testing SettingKeyName from the registory
'''
Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToAccessOleDbWithExcelSheet()

    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelKey"
    
    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelWithPasswordKey"
End Sub

'''
''' Excel book, which is not locked by any password.
'''
Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetForm()

    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
    Dim strSettingKeyName As String, strBookPath As String
    
    
    strSettingKeyName = "TestAccOleDbToExcelKey"
    
    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl

    strBookPath = mfstrPrepareTestBook()

    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
End Sub

'''
''' Connect a password locked Excel book
'''
Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLock()

    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
    Dim strSettingKeyName As String, strBookPath As String
    
    
    strSettingKeyName = "TestAccOleDbToExcelWithPasswordKey"
    
    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl

    strBookPath = mfstrPrepareTestBookWithLockPassword()

    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath, vstrBookLockPassword:="1234"
End Sub

'''
''' Connect a password locked Excel book
'''
Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLockAndOpen()

    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
    Dim strSettingKeyName As String, strBookPath As String
    
    
    strSettingKeyName = "TestAccOleDbToExcelWithPasswordKey"
    
    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl

    strBookPath = mfstrPrepareTestBookWithLockPassword()

    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
End Sub

'''
''' Connect a password locked Excel book using a User-form
'''
Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLockAndOpenFormIfItNeeded()

    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
    Dim strSettingKeyName As String, strBookPath As String
    
    
    strSettingKeyName = "TestAccOleDbToExcelKey"
    
    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl

    strBookPath = mfstrPrepareTestBookWithLockPassword()

    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
End Sub




'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrPrepareTestBookWithLockPassword() As String

    Dim strPath As String
    
    strPath = mfstrGetTemporaryAdoConnectingTestExcelBookDir() & "\AdoConectionUITestWithLockPassword.xlsx"

    PrepareTestBookWithLockingPasswordAndCreating strPath, mstrSheetName, "1234"

    mfstrPrepareTestBookWithLockPassword = strPath
End Function

'''
'''
'''
Private Function mfstrPrepareTestBook() As String

    Dim strPath As String
    
    strPath = mfstrGetTemporaryAdoConnectingTestExcelBookDir() & "\AdoConectionUITest.xlsx"

    PrepareTestBookWithLockingPasswordAndCreating strPath, mstrSheetName, ""

    mfstrPrepareTestBook = strPath
End Function


'''
'''
'''
Private Function mfstrGetTemporaryAdoConnectingTestExcelBookDir() As String

    Dim strDir As String

    strDir = GetParentDirectoryPathOfCurrentOfficeFile() & "\TemporaryBooks\AdoConnectionUITests"

    'strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoConnectionUITests"

    ForceToCreateDirectory strDir

    mfstrGetTemporaryAdoConnectingTestExcelBookDir = strDir
End Function

