Attribute VB_Name = "UTfCompareOnlySheetsByAdo"
'
'   Sanity tests to compare two Excel book about only Worksheet table data
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'       Dependent on XlAdoConnector.bas, VariantTypeConversion.bas
'       Dependent on UTfDecorationSetterToXlSheet.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 29/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** About double sheets
'**---------------------------------------------
Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsWithTwoSheetsUsingAdo01()

    Dim strBookPath01 As String, strBookPath02 As String, str1stSheetName As String, str2ndSheetName As String
    Dim strSheetNamesDelimitedByComma As String
    
    'msubGetTestingTwoExcelBooksWhichHasTwoSheet strBookPath01, strBookPath02, str1stSheetName, str2ndSheetName, "3, 4, 1, 1", "8, 12, 1, 1", "3, 4, 1, 1", "8, 12, 1, 1", "TwoSheetsTestBook01.xlsx"
    
    msubGetTestingTwoExcelBooksWhichHasTwoSheet strBookPath01, strBookPath02, str1stSheetName, str2ndSheetName, "3, 4, 1, 1", "8, 12, 5, 7", "3, 4, 1, 1", "8, 12, 1, 1", "TwoSheetsTestBook01.xlsx"
    

    strSheetNamesDelimitedByComma = str1stSheetName & "," & str2ndSheetName

    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetNamesDelimitedByComma) Then
    
        Debug.Assert False
    Else
        Debug.Print "The two books are same for each specified sheets" & " - The sanity test is passed"
    End If
End Sub

Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsWithTwoSheetsUsingAdo02()

    Dim strBookPath01 As String, strBookPath02 As String, str1stSheetName As String, str2ndSheetName As String
    Dim strSheetNamesDelimitedByComma As String
    
    msubGetTestingTwoExcelBooksWhichHasTwoSheet strBookPath01, strBookPath02, str1stSheetName, str2ndSheetName, "3, 4, 1, 1", "8, 11, 1, 1", "3, 4, 1, 1", "8, 12, 1, 1", "TwoSheetsTestDifferentBook02.xlsx"
    
    strSheetNamesDelimitedByComma = str1stSheetName & "," & str2ndSheetName

    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetNamesDelimitedByComma) Then
    
        Debug.Print "The two books are different" & " - The sanity test is passed"
    Else
        Debug.Assert False
    End If
End Sub

'**---------------------------------------------
'** About single sheet
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsUsingAdo01()

    Dim strBookPath01 As String, strBookPath02 As String, strSheetName As String
    

    'msubGetTestingTwoExcelBooks strBookPath01, strBookPath02, strSheetName, "3, 4, 1, 1", "3, 4, 1, 1", "TestDataBook01"
    
    msubGetTestingTwoExcelBooks strBookPath01, strBookPath02, strSheetName, "8, 9, 1, 1", "8, 9, 1, 1", "TestDataBook01.xlsx"

    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetName) Then
    
        Debug.Assert False
    Else
        Debug.Print "The two books are same" & " - The sanity test is passed"
    End If
End Sub

Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsUsingAdo02()

    Dim strBookPath01 As String, strBookPath02 As String, strSheetName As String
    

    msubGetTestingTwoExcelBooks strBookPath01, strBookPath02, strSheetName, "3, 4, 1, 1", "3, 5, 1, 1", "TestDifferentDataBook02.xlsx"

    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetName) Then
    
        Debug.Print "The two books are different" & " - The sanity test is passed"
    Else
        Debug.Assert False
    End If
End Sub


'''
'''
'''
Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsUsingAdo03()

    Dim strBookPath01 As String, strBookPath02 As String, strSheetName As String
    

    msubGetTestingTwoExcelBooks strBookPath01, strBookPath02, strSheetName, "3, 4, 1, 1", "3, 4, 3, 3", "TestDifferentDataBook03.xlsx"

    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetName) Then
    
        Debug.Assert False
    Else
        Debug.Print "The two books are same, but the top-left of table is different." & " - The sanity test is passed"
    End If
End Sub

'''
'''
'''
Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsUsingAdo04()

    Dim strBookPath01 As String, strBookPath02 As String, strSheetName As String

    msubGetTestingTwoExcelBooks strBookPath01, strBookPath02, strSheetName, "3, 4, 1, 1", "3, 4, 1, 1", "TestDifferentDataBook04", "", "2;2;105"
    
    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetName) Then
    
        Debug.Print "The two books are different" & " - The sanity test has been passed"
    Else
        Debug.Assert False
    End If
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubExploreTestDir()

    ExploreParentFolderWithSelectingWhenOpenedThatIsNothing mfstrGetTemporaryBookDir("SudDir01")
End Sub


'''
'''
'''
Private Sub msubGetTestingTwoExcelBooks(ByRef rstrBookPath01 As String, _
        ByRef rstrBookPath02 As String, _
        ByRef rstrSheetName As String, _
        ByVal vstrDataTableParamsDelimitedByComma01 As String, _
        ByVal vstrDataTableParamsDelimitedByComma02 As String, _
        ByVal vstrBookFileName As String, _
        Optional ByVal vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon01 As String = "", _
        Optional ByVal vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon02 As String = "")
        
    
    msubGetTestingExcelBookOfOneSheet rstrBookPath01, rstrSheetName, "SudDir01", vstrDataTableParamsDelimitedByComma01, vstrBookFileName, vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon01
    
    msubGetTestingExcelBookOfOneSheet rstrBookPath02, rstrSheetName, "SudDir02", vstrDataTableParamsDelimitedByComma02, vstrBookFileName, vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon02
End Sub

'''
'''
'''
Private Sub msubGetTestingTwoExcelBooksWhichHasTwoSheet(ByRef rstrBookPath01 As String, _
        ByRef rstrBookPath02 As String, _
        ByRef rstr1stSheetName As String, _
        ByRef rstr2ndSheetName As String, _
        ByVal vstr1stDataTableParamsDelimitedByComma01 As String, _
        ByVal vstr2ndDataTableParamsDelimitedByComma01 As String, _
        ByVal vstr1stDataTableParamsDelimitedByComma02 As String, _
        ByVal vstr2ndDataTableParamsDelimitedByComma02 As String, _
        ByVal vstrBookFileName As String)
        
    
    msubGetTestingExcelBookOfTwoSheets rstrBookPath01, rstr1stSheetName, rstr2ndSheetName, "SudDir01", vstr1stDataTableParamsDelimitedByComma01, vstr2ndDataTableParamsDelimitedByComma01, vstrBookFileName
    
    msubGetTestingExcelBookOfTwoSheets rstrBookPath02, rstr1stSheetName, rstr2ndSheetName, "SudDir02", vstr1stDataTableParamsDelimitedByComma02, vstr2ndDataTableParamsDelimitedByComma02, vstrBookFileName
End Sub

'''
'''
'''
Private Sub msubGetTestingExcelBookOfOneSheet(ByRef rstrBookPath As String, _
        ByRef rstrSheetName As String, _
        ByVal vstrSubDirectoryName As String, _
        ByVal vstrDataTableParamsDelimitedByComma As String, _
        ByVal vstrBookFileName As String, _
        Optional ByVal vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon As String = "")
    
    Dim objBook As Excel.Workbook, strSheetName As String
    
    strSheetName = "TestName"


    rstrBookPath = mfstrGetTestBookPath(vstrSubDirectoryName, vstrBookFileName)

    Set objBook = GetWorkbookAndPrepareForUnitTest(rstrBookPath)

    GenerateSimpleTableSheetForSpecifiedBookWithStringParams objBook, strSheetName, vstrDataTableParamsDelimitedByComma, vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon
    
    DeleteDummySheetWhenItExists objBook

    ForceToSaveAsBookWithoutDisplayAlert objBook, rstrBookPath
    
    objBook.Close

    rstrSheetName = strSheetName
End Sub

'''
'''
'''
Private Sub msubGetTestingExcelBookOfTwoSheets(ByRef rstrBookPath As String, _
        ByRef rstr1stSheetName As String, _
        ByRef rstr2ndSheetName As String, _
        ByVal vstrSubDirectoryName As String, _
        ByVal vstr1stDataTableParamsDelimitedByComma As String, _
        ByVal vstr2ndDataTableParamsDelimitedByComma As String, _
        ByVal vstrBookFileName As String)
    
    Dim objBook As Excel.Workbook
    
    rstr1stSheetName = "TestName01"
    
    rstr2ndSheetName = "TestName02"


    rstrBookPath = mfstrGetTestBookPath(vstrSubDirectoryName, vstrBookFileName)

    Set objBook = GetWorkbookAndPrepareForUnitTest(rstrBookPath)

    GenerateSimpleTableSheetForSpecifiedBookWithStringParams objBook, rstr1stSheetName, vstr1stDataTableParamsDelimitedByComma
    
    GenerateSimpleTableSheetForSpecifiedBookWithStringParams objBook, rstr2ndSheetName, vstr2ndDataTableParamsDelimitedByComma
    
    DeleteDummySheetWhenItExists objBook

    ForceToSaveAsBookWithoutDisplayAlert objBook, rstrBookPath
    
    objBook.Close
End Sub


'''
'''
'''
Private Function mfstrGetTestBookPath(ByVal vstrSubDirectoryName As String, ByVal vstrBookName As String) As String

    Dim strDir As String
    
    strDir = mfstrGetTemporaryBookDir(vstrSubDirectoryName)

    mfstrGetTestBookPath = strDir & "\" & vstrBookName
End Function

'''
'''
'''
Private Function mfstrGetTemporaryBookDir(ByVal vstrSubDirectoryName As String) As String

    Dim strDir As String

    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoBookCompare\" & vstrSubDirectoryName

    ForceToCreateDirectoryByVbaDir strDir

    mfstrGetTemporaryBookDir = strDir
End Function

