Attribute VB_Name = "UTfADOExSheetOut"
'
'   Using a local Excel ADO connection, sanity tests to create a SQL result Excel sheet with additional information by using ADODB.Recordset
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'       Dependent on UTfDecorationSetterToXlSheet.bas, LoadAndModifyBook.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun, 14/Jan/2024    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToPTfADOExSheetOut()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", mfstrGetVBEModuleNamesToOpen()
End Sub

Private Function mfstrGetVBEModuleNamesToOpen() As String

    Dim strNames As String: strNames = "ADOExSheetOut,ADOSheetOut,SolveSavePathGeneral"
    
    mfstrGetVBEModuleNamesToOpen = strNames
End Function


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////

'''
'''
'''
Private Sub msubSanityTestToRSetAdditionalInfosAndNormalOutputTest()

    'msubRSetAdditionalInfosAndNormalOutputTest 5, 14, , 2, 3
    
    'msubRSetAdditionalInfosAndNormalOutputTest 5, 14, , 2, 3, Nothing, "", False, True
    
    'msubRSetAdditionalInfosAndNormalOutputTest 5, 14, "SELECT Column2, Column5 FROM [TestTable$]", 2, 3, Nothing, "", False, True
    
    msubRSetAdditionalInfosAndNormalOutputTest 5, 14, , 2, 3, GetTextToRealNumberDicFromLineDelimitedChar("Column1,5,Column3,12,Column4,15"), "", False, True
End Sub


'''
'''
'''
Private Sub msubSanityTestToRSetAdditionalInfosAndTransposeOutputTest()


    'msubRSetAdditionalInfosAndTransposeOutputTest 5, 14, , 2, 3, GetTextToRealNumberDicFromLineDelimitedChar("No,13,TypeName,14,Description,16,FieldTitlesColumn,15,DataColumn,12")
    
    'msubRSetAdditionalInfosAndTransposeOutputTest 5, 14, , 2, 3, Nothing, "", False, True
    
    msubRSetAdditionalInfosAndTransposeOutputTest 7, 14, , 4, 6, GetTextToRealNumberDicFromLineDelimitedChar("No,13,TypeName,14,Description,16,FieldTitlesColumn,15,DataColumn,12"), "", False, True
    
    
    'msubRSetAdditionalInfosAndTransposeOutputTest 7, 14, "SELECT Column5, Column9, Column11 FROM [TestTable$]", 4, 6, GetTextToRealNumberDicFromLineDelimitedChar("No,13,TypeName,14,Description,16,FieldTitlesColumn,15,DataColumn,12"), "", False, True
    
End Sub


'''
'''
'''
Private Sub msubSanityTestToRSetNormalOutputTest()


    ' msubRSetNormalOutputTest 1, 1, 1, 1
    
    ' msubRSetNormalOutputTest 3, 2, 1, 1

    'msubRSetNormalOutputTest 6, 4, 2, 3

    'msubRSetNormalOutputTest 5, 14, 2, 3
    
    msubRSetNormalOutputTest 5, 14, 2, 3, Nothing, "5,6,9,11", True
End Sub

'''
'''
'''
Private Sub msubSanityTestToRSetTransposeOutputTest()

    'msubRSetTransposeOutputTest 3, 2, 1, 1, GetTextToRealNumberDicFromLineDelimitedChar("FieldTitlesColumn,15,DataColumn,12")

    msubRSetTransposeOutputTest 5, 3, 2, 4, GetTextToRealNumberDicFromLineDelimitedChar("FieldTitlesColumn,15,DataColumn,12")
    
    'msubRSetTransposeOutputTest 6, 4, 2, 3
    
'    msubRSetTransposeOutputTest 4, 11, 2, 3
'
    'msubRSetTransposeOutputTest 4, 15, 2, 3, Nothing, "5,7,10,11", True

End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubRSetAdditionalInfosAndTransposeOutputTest(ByVal vintInputTableRowMax As Long, _
        ByVal vintInputTableColumnMax As Long, _
        Optional ByVal vstrSQL As String = "SELECT * FROM [TestTable$]", _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjInfoTypeToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False, _
        Optional ByVal vblnAllowToAddColumnNo As Boolean = False)
        

    Dim strOutputBookPath As String, strOutputBookDir As String, objOutputBook As Excel.Workbook
    Dim objOutputSheet As Excel.Worksheet
    
    Dim objRSet As ADODB.Recordset, intRowsCountOfRecords As Long
    
    Dim objInfoTypeToTitleToValueDic As Scripting.Dictionary
    
    
    strOutputBookDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoExSheetOutTests"
    
    strOutputBookPath = strOutputBookDir & "\RSetAdditionalInfosPlusTransposeOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintTopLeftRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & ".xlsx"
    
    If vstrNullColumnIndexDelimitedComma <> "" Then
    
        strOutputBookPath = GetParentDirectoryPathByVbaDir(strOutputBookPath) & "\" & GetBaseNameByVbaDir(strOutputBookPath) & "_WithNullRows." & GetExtensionNameByVbaDir(strOutputBookPath)
    End If
    
    Set objOutputSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "RSetTransposeOut")
    
    Set objInfoTypeToTitleToValueDic = GetInfoTypeToTitleToValueDicOfALeastSamples(vintInputTableColumnMax, vblnAllowToAddColumnNo)
    
    With New ADODB.Connection
    
        .ConnectionString = mfstrGetAdoConnectionStringToTestingExcelBook(vintInputTableRowMax, vintInputTableColumnMax, vstrNullColumnIndexDelimitedComma)
    
        On Error Resume Next
        
        .Open
        
        If Err.Number = 0 Then
        
            Set objRSet = .Execute(vstrSQL)
        
            If Err.Number = 0 Then
            
                ' Transpose data-table output
                
                TransposeOutputRecordsetWithAdditionalInfoToCellsOnSheet objOutputSheet, _
                        objRSet, _
                        objInfoTypeToTitleToValueDic, _
                        vintTopLeftRowIndex, _
                        vintTopLeftColumnIndex, _
                        vobjInfoTypeToColumnWidthDic, _
                        "", _
                        vblnAllowToCreateNullColumnsCompressedTable
            End If
        
            .Close
        End If
        
        On Error GoTo 0
    End With
    
    AddAutoShapeGeneralMessage objOutputSheet, "Transpose-Test-log"
End Sub


'''
'''
'''
Private Sub msubRSetTransposeOutputTest(ByVal vintInputTableRowMax As Long, _
        ByVal vintInputTableColumnMax As Long, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjInfoTypeToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False)
        

    Dim strOutputBookPath As String, strOutputBookDir As String, objOutputBook As Excel.Workbook
    Dim objOutputSheet As Excel.Worksheet
    
    Dim strSQL As String, objRSet As ADODB.Recordset, intRowsCountOfRecords As Long
    
    
    strOutputBookDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoExSheetOutTests"
    
    If vstrNullColumnIndexDelimitedComma <> "" Then
    
        strOutputBookPath = strOutputBookDir & "\RSetTransposeOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintFieldTitlesRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & "_WithNullColumns.xlsx"
    Else
        strOutputBookPath = strOutputBookDir & "\RSetTransposeOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintFieldTitlesRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & ".xlsx"
    End If
    
    Set objOutputSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "RSetTransposeOut")
        
    With New ADODB.Connection
    
        .ConnectionString = mfstrGetAdoConnectionStringToTestingExcelBook(vintInputTableRowMax, vintInputTableColumnMax, vstrNullColumnIndexDelimitedComma)
    
        On Error Resume Next
        
        .Open
        
        If Err.Number = 0 Then
        
            strSQL = "SELECT * FROM [TestTable$]"
        
            Set objRSet = .Execute(strSQL)
        
            If Err.Number = 0 Then
            
                ' Transpose data-table output
                
                TransposeOutputRecordsetToCellsOnSheet objOutputSheet, objRSet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjInfoTypeToColumnWidthDic, "", vblnAllowToCreateNullColumnsCompressedTable
            End If
        
            .Close
        End If
        
        On Error GoTo 0
    End With
End Sub



'''
'''
'''
Private Sub msubRSetAdditionalInfosAndNormalOutputTest(ByVal vintInputTableRowMax As Long, _
        ByVal vintInputTableColumnMax As Long, _
        Optional ByVal vstrSQL As String = "SELECT * FROM [TestTable$]", _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False, _
        Optional ByVal vblnAllowToAddColumnNo As Boolean = False)
        

    Dim strOutputBookPath As String, strOutputBookDir As String, objOutputBook As Excel.Workbook
    Dim objOutputSheet As Excel.Worksheet
    
    Dim objRSet As ADODB.Recordset, intRowsCountOfRecords As Long
    
    Dim objInfoTypeToTitleToValueDic As Scripting.Dictionary
    
    
    strOutputBookDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoExSheetOutTests"
    
    strOutputBookPath = strOutputBookDir & "\RSetAdditionalInfosPlusNormalOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintFieldTitlesRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & ".xlsx"
    
    If vstrNullColumnIndexDelimitedComma <> "" Then
    
        strOutputBookPath = GetParentDirectoryPathByVbaDir(strOutputBookPath) & "\" & GetBaseNameByVbaDir(strOutputBookPath) & "_WithNullColumns." & GetExtensionNameByVbaDir(strOutputBookPath)
    End If
    
    
    Set objOutputSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "RSetNormalOut")
    
    Set objInfoTypeToTitleToValueDic = GetInfoTypeToTitleToValueDicOfALeastSamples(vintInputTableColumnMax, vblnAllowToAddColumnNo)
    
    With New ADODB.Connection
    
        .ConnectionString = mfstrGetAdoConnectionStringToTestingExcelBook(vintInputTableRowMax, vintInputTableColumnMax, vstrNullColumnIndexDelimitedComma)
    
        On Error Resume Next
        
        .Open
        
        If Err.Number = 0 Then
        
            'strSQL = "SELECT * FROM [TestTable$]"
        
            Set objRSet = .Execute(vstrSQL)
        
            If Err.Number = 0 Then
            
                ' Normal data-table output
                
                NormalOutputRecordsetWithAdditionalInfoToCellsOnSheet objOutputSheet, objRSet, objInfoTypeToTitleToValueDic, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjFieldTitleToColumnWidthDic, "", vblnAllowToCreateNullColumnsCompressedTable
            End If
        
            .Close
        End If
        
        On Error GoTo 0
    End With
    
    
    AddAutoShapeGeneralMessage objOutputSheet, "Test-log"
End Sub

'''
'''
'''
''' <Return>Dictionary(Of Variant[info-type], Dictionary(Variant[column-name], Variant[Info]))</Return>
Private Function GetInfoTypeToTitleToValueDicOfALeastSamples(ByVal vintColumnMax As Long, _
        Optional ByVal vblnAllowToAddColumnNo As Boolean = False) As Scripting.Dictionary


    Dim objInfoTypeToTitleToValueDic As Scripting.Dictionary, strInfoTypeKey As String, objTitleToValueDic As Scripting.Dictionary
    
    Dim objFieldTitleToColumnDescriptionDic As Scripting.Dictionary, objFieldTitleToColumnTypeNameDic As Scripting.Dictionary, objFieldTitleToColumnTypeLengthDic As Scripting.Dictionary
    
    Dim varKey As Variant, i As Long, objFieldTitleToNumberDic As Scripting.Dictionary
    
    
    Set objInfoTypeToTitleToValueDic = New Scripting.Dictionary
    
    SetupPadAdditionalInfoDicsAboutColumnNames objFieldTitleToColumnDescriptionDic, objFieldTitleToColumnTypeNameDic, objFieldTitleToColumnTypeLengthDic, vintColumnMax
    
    Set objInfoTypeToTitleToValueDic = New Scripting.Dictionary
    
    With objInfoTypeToTitleToValueDic
    
        If vblnAllowToAddColumnNo Then
        
            Set objFieldTitleToNumberDic = New Scripting.Dictionary
        
            With objFieldTitleToColumnDescriptionDic
        
                i = 1
        
                For Each varKey In .Keys
        
                    objFieldTitleToNumberDic.Add varKey, i
        
                    i = i + 1
                Next
            End With
            
            strInfoTypeKey = "2_ColumnNo"
            
            .Add strInfoTypeKey, objFieldTitleToNumberDic
        End If
    
    
        strInfoTypeKey = "3_ColumnTypeName"
    
        .Add strInfoTypeKey, objFieldTitleToColumnTypeNameDic
    
        strInfoTypeKey = "4_ColumnLengthOfType"
    
        .Add strInfoTypeKey, objFieldTitleToColumnTypeLengthDic
    
        strInfoTypeKey = "5_ColumnDescription"
    
        .Add strInfoTypeKey, objFieldTitleToColumnDescriptionDic
    End With
    
    Set GetInfoTypeToTitleToValueDicOfALeastSamples = objInfoTypeToTitleToValueDic
End Function



'''
'''
'''
Private Sub msubRSetNormalOutputTest(ByVal vintInputTableRowMax As Long, _
        ByVal vintInputTableColumnMax As Long, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False)
        

    Dim strOutputBookPath As String, strOutputBookDir As String, objOutputBook As Excel.Workbook
    Dim objOutputSheet As Excel.Worksheet
    
    Dim strSQL As String, objRSet As ADODB.Recordset, intRowsCountOfRecords As Long
    
    
    strOutputBookDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoExSheetOutTests"
    
    If vstrNullColumnIndexDelimitedComma <> "" Then
    
        strOutputBookPath = strOutputBookDir & "\RSetNormalOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintFieldTitlesRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & "_WithNullColumns.xlsx"
    Else
        strOutputBookPath = strOutputBookDir & "\RSetNormalOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintFieldTitlesRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & ".xlsx"
    End If
    
    Set objOutputSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "RSetNormalOut")
        
    With New ADODB.Connection
    
        .ConnectionString = mfstrGetAdoConnectionStringToTestingExcelBook(vintInputTableRowMax, vintInputTableColumnMax, vstrNullColumnIndexDelimitedComma)
    
        On Error Resume Next
        
        .Open
        
        If Err.Number = 0 Then
        
            strSQL = "SELECT * FROM [TestTable$]"
        
            Set objRSet = .Execute(strSQL)
        
            If Err.Number = 0 Then
            
                ' Normal data-table output
                
                NormalOutputRecordsetToCellsOnSheet objOutputSheet, objRSet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjFieldTitleToColumnWidthDic, "", vblnAllowToCreateNullColumnsCompressedTable
            End If
        
            .Close
        End If
        
        On Error GoTo 0
    End With
End Sub


'''
'''
'''
Private Function mfstrGetAdoConnectionStringToTestingExcelBook(ByVal vintRowMax As Long, ByVal vintColumnMax As Long, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "") As String

    Dim strBookPath As String
    
    strBookPath = mfstrGetBookPathWithCreatingTestBookIfItDoesntExist(vintRowMax, vintColumnMax, vstrNullColumnIndexDelimitedComma)

    mfstrGetAdoConnectionStringToTestingExcelBook = GetADODBConnectionOleDbStringToXlBook(strBookPath)
End Function

'''
'''
'''
Private Function mfstrGetBookPathWithCreatingTestBookIfItDoesntExist(ByVal vintRowMax As Long, ByVal vintColumnMax As Long, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "") As String

    Dim strBookPath As String
    
    strBookPath = mfstrGetTestBookPath(vintRowMax, vintColumnMax, vstrNullColumnIndexDelimitedComma)
    
    If Not FileExistsByVbaDir(strBookPath) Then
    
        strBookPath = mfstrGetBookPathAfterCreatingTestBook(vintRowMax, vintColumnMax, vstrNullColumnIndexDelimitedComma)
    End If
    
    mfstrGetBookPathWithCreatingTestBookIfItDoesntExist = strBookPath
End Function

'''
'''
'''
Private Function mfstrGetBookPathAfterCreatingTestBook(ByVal vintRowMax As Long, ByVal vintColumnMax As Long, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "") As String

    Dim strBookPath As String, objBook As Excel.Workbook
    
    strBookPath = mfstrGetTestBookPath(vintRowMax, vintColumnMax, vstrNullColumnIndexDelimitedComma)
    
    Set objBook = mfobjGetSampleSimpleIntegerAndStringTableBook(strBookPath, "TestTable", vintRowMax, vintColumnMax, vstrNullColumnIndexDelimitedComma)
    
    ForceToSaveAsBookWithoutDisplayAlert objBook, strBookPath

    mfstrGetBookPathAfterCreatingTestBook = strBookPath
End Function

'''
'''
'''
Private Function mfstrGetTestBookPath(ByVal vintRowMax As Long, ByVal vintColumnMax As Long, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "") As String

    Dim strTmpDir As String, strBookName As String, strBookPath As String

    strTmpDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoExSheetOutTests"
    
    If vstrNullColumnIndexDelimitedComma <> "" Then
    
        strBookName = "TmpBook_" & Format(vintRowMax, "00") & "_" & Format(vintColumnMax) & "_WithNullColumns" & ".xlsx"
    Else
        strBookName = "TmpBook_" & Format(vintRowMax, "00") & "_" & Format(vintColumnMax) & ".xlsx"
    End If
    
    strBookPath = strTmpDir & "\" & strBookName
    
    mfstrGetTestBookPath = strBookPath
End Function

'''
'''
'''
Private Function mfobjGetSampleSimpleIntegerAndStringTableBook(ByVal vstrBookPath As String, _
        ByVal vstrSheetName As String, _
        Optional ByVal vintRowMax As Long = 1, _
        Optional ByVal vintColumnMax As Long = 1, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "") As Excel.Workbook

    Dim objDTCol As Collection, objFieldTitlesCol As Collection, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    
    SetupPadSimpleIntegerAndStringWithNullColumnsForRCTableColAndFieldTitles objDTCol, objFieldTitlesCol, vintRowMax, vintColumnMax, 4, vstrNullColumnIndexDelimitedComma

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrBookPath, vstrSheetName)

    Set objDTSheetFormatter = New DataTableSheetFormatter

    objDTSheetFormatter.RecordBordersType = RecordCellsGrayAll

    OutputColToSheet objSheet, objDTCol, objFieldTitlesCol, objDTSheetFormatter

    Set objBook = objSheet.Parent

    Set mfobjGetSampleSimpleIntegerAndStringTableBook = objBook
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetSampleSimpleIntegerAndStringTableBook()

    Dim objBook As Excel.Workbook, strBookPath As String, strTmpBookDir As String


    strTmpBookDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoExSheetOutTests"

    strBookPath = strTmpBookDir & "\TestIntAndStringData.xlsx"

    Set objBook = mfobjGetSampleSimpleIntegerAndStringTableBook(strBookPath, "TestTable", 1, 6)
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetBookPathAfterCreatingTestBook()

    Debug.Print mfstrGetBookPathAfterCreatingTestBook(3, 4)
End Sub

