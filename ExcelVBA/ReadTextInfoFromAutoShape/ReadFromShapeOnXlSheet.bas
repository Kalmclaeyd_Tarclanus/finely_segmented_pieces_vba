Attribute VB_Name = "ReadFromShapeOnXlSheet"
'
'   Get key-value dictionary from auto-shape on Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 22/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToReadFromShapeOnXlSheet()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UTfReadFromShapeOnXlSheet,WinINIGeneral,WinINIGeneralForXl,UTfWinINIGeneral"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** get some type Dictionary object
'**---------------------------------------------
'''
'''
'''
''' <Argument>vobjSheet: Input</Argument>
''' <Argument>vstrKeyValueDelimiter</Argument>
''' <Argument>vstrFilterShapeName</Argument>
''' <Argument>vblnAllowToAvoidDuplicatedKey</Argument>
''' <Return>Dictionary(Of Key, Value)</Return>
Public Function GetKeyValueDicFromShapes(ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vstrKeyValueDelimiter As String = ": ", _
        Optional ByVal vstrFilterShapeName As String = "", _
        Optional ByVal vblnAllowToAvoidDuplicatedKey As Boolean = True) As Scripting.Dictionary


    Dim objDic As Scripting.Dictionary, objShape As Excel.Shape
    Dim blnContinue As Boolean, blnNeedToFilter As Boolean
    
    
    Set objDic = New Scripting.Dictionary
    
    blnNeedToFilter = False
    
    If vstrFilterShapeName <> "" Then
    
        blnNeedToFilter = True
    End If
    
    With vobjSheet
    
        For Each objShape In .Shapes
        
            blnContinue = True
            
            If blnNeedToFilter Then
            
                If InStr(1, objShape.Name, vstrFilterShapeName) = 0 Then
                
                    blnContinue = False
                End If
            End If
        
            If blnContinue Then
            
                msubAddKeyValueToDicInAShapeText objDic, objShape, vstrKeyValueDelimiter, vblnAllowToAvoidDuplicatedKey
            End If
        Next
    End With

    Set GetKeyValueDicFromShapes = objDic
End Function


'''
'''
'''
''' <Argument>vobjSheet: Input</Argument>
''' <Argument>vstrKeyValueDelimiter</Argument>
''' <Argument>vstrFilterShapeName</Argument>
''' <Argument>vblnAllowToAvoidDuplicatedKey</Argument>
''' <Return>Dictionary(Of ShapeNameKey, Dictionary(Of Key, Value))</Return>
Public Function GetShapeNameToKeyValueDicFromShapes(ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vstrKeyValueDelimiter As String = ": ", _
        Optional ByVal vstrFilterShapeName As String = "", _
        Optional ByVal vblnAllowToAvoidDuplicatedKey As Boolean = True) As Scripting.Dictionary


    Dim objShapeNameToKeyValueDic As Scripting.Dictionary, objDic As Scripting.Dictionary, objShape As Excel.Shape
    Dim blnContinue As Boolean, blnNeedToFilter As Boolean
    
    
    blnNeedToFilter = False
    
    If vstrFilterShapeName <> "" Then
    
        blnNeedToFilter = True
    End If
    
    
    Set objShapeNameToKeyValueDic = New Scripting.Dictionary
    
    With vobjSheet
    
        For Each objShape In .Shapes
        
            With objShape
        
                blnContinue = True
                
                If blnNeedToFilter Then
                
                    If InStr(1, .Name, vstrFilterShapeName) = 0 Then
                    
                        blnContinue = False
                    End If
                End If
            
                If blnContinue Then
                
                    Set objDic = New Scripting.Dictionary
                
                    msubAddKeyValueToDicInAShapeText objDic, objShape, vstrKeyValueDelimiter, vblnAllowToAvoidDuplicatedKey
                    
                    If objDic.Count > 0 Then
                    
                        If Not objShapeNameToKeyValueDic.Exists(.Name) Then
                        
                            objShapeNameToKeyValueDic.Add .Name, objDic
                        End If
                    End If
                End If
            End With
        Next
    End With


    Set GetShapeNameToKeyValueDicFromShapes = objShapeNameToKeyValueDic
End Function


'''
'''
'''
Public Sub AddKeyValueToDicInFromText(ByRef robjKeyValueDic As Scripting.Dictionary, _
        ByRef rstrFirstRowLine As String, _
        ByRef rstrInputText As String, _
        Optional ByVal vstrKeyValueDelimiter As String = ": ", _
        Optional ByVal vblnPriorToUseLineFeedChar As Boolean = False, _
        Optional ByVal vblnAllowToAvoidDuplicatedKey As Boolean = True)


    Dim strRowTexts() As String, i As Long, intP1 As Long, strKey As String, strValue As String, blnIsFirstLineStringKeyValue As Boolean, intFirstIndex As Long

    blnIsFirstLineStringKeyValue = False

    msubGetStringLinesArray strRowTexts, rstrInputText, vblnPriorToUseLineFeedChar
    
    intFirstIndex = LBound(strRowTexts)
    
    For i = intFirstIndex To UBound(strRowTexts)
    
        intP1 = InStr(1, strRowTexts(i), vstrKeyValueDelimiter)
    
        If intP1 > 0 Then
        
            strKey = Trim(Left(strRowTexts(i), intP1 - 1))
            
            strValue = Trim(Right(strRowTexts(i), Len(strRowTexts(i)) - intP1))
        
            If Not IsEmpty(strKey) Then
            
                If vblnAllowToAvoidDuplicatedKey Then
                
                    With robjKeyValueDic
                    
                        If Not .Exists(strKey) Then
                        
                            .Add strKey, strValue
                            
                            If i = intFirstIndex Then
                            
                                blnIsFirstLineStringKeyValue = True
                            End If
                        End If
                    End With
                Else
                    robjKeyValueDic.Add strKey, strValue
                    
                    If i = intFirstIndex Then
                    
                        blnIsFirstLineStringKeyValue = True
                    End If
                End If
            End If
        End If
    Next
    
    If Not blnIsFirstLineStringKeyValue Then
    
        rstrFirstRowLine = strRowTexts(LBound(strRowTexts))
    End If
End Sub

'**---------------------------------------------
'** Output a Dictionary object to a Sheet
'**---------------------------------------------
'''
''' output Dictionary(Of Key, Value) to Excel book
'''
Public Function OutputSheetShapesKeyValuesToBook(ByVal vobjKeyValueDic As Scripting.Dictionary, _
        ByVal vstrOutputBookPath As String) As Excel.Worksheet

    Dim objSheet As Excel.Worksheet

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "AutoShapeKeyValue")

    OutputDicToSheetRestrictedByDataTableSheetFormatter objSheet, vobjKeyValueDic, GetKeyValueDataTableSheetFormatter()

    Set OutputSheetShapesKeyValuesToBook = objSheet
End Function

'''
''' output Dictionary(Of FirstKey, Dictionary(Of Key, Value)) to Excel book
'''
Public Function OutputSheetShapeNameToKeyValuesDicToBook(ByVal vobjShapeNameToKeyValueDic As Scripting.Dictionary, _
        ByVal vstrOutputBookPath As String) As Excel.Worksheet

    Dim objSheet As Excel.Worksheet

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "AutoShapeNameAndKeyValue")

    OutputOneNestedDicToSheetRestrictedByDataTableSheetFormatter objSheet, vobjShapeNameToKeyValueDic, GetOneNestedKeyValueDicDataTableSheetFormatter("ShapeName,20,Key,15,Value,15") ' GetOneNestedKeyValueDicDataTableSheetFormatter("ShapeName", 20)

    Set OutputSheetShapeNameToKeyValuesDicToBook = objSheet
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>robjKeyValueDic: Output</Argument>
''' <Argument>robjShape: Input</Argument>
''' <Argument>vblnAllowToAvoidDuplicatedKey: Input</Argument>
Private Sub msubAddKeyValueToDicInAShapeText(ByRef robjKeyValueDic As Scripting.Dictionary, _
        ByRef robjShape As Excel.Shape, _
        Optional ByVal vstrKeyValueDelimiter As String = ": ", _
        Optional ByVal vblnAllowToAvoidDuplicatedKey As Boolean = True)


    Dim strRowTexts() As String, i As Long, intP1 As Long, strKey As String, strValue As String, strFirstRowLine As String

    With robjShape.TextFrame2.TextRange.Characters
                
        If Not IsEmpty(.Text) Then
        
            Debug.Print "Auto shape name: " & robjShape.Name
        
            If InStr(1, .Text, vstrKeyValueDelimiter) > 0 Then
            
                AddKeyValueToDicInFromText robjKeyValueDic, strFirstRowLine, .Text, vstrKeyValueDelimiter, True, vblnAllowToAvoidDuplicatedKey
            End If
        End If
    End With
End Sub



'''
'''
'''
Private Sub msubGetStringLinesArray(ByRef rstrLines() As String, _
        ByRef rstrInputString As String, _
        ByRef rblnPriorToUseLineFeedChar As Boolean)


    If rblnPriorToUseLineFeedChar Then
    
        rstrLines = Split(rstrInputString, vbLf)
    Else
        If InStr(1, rstrInputString, vbNewLine) > 0 Then
        
            rstrLines = Split(rstrInputString, vbNewLine)
        
        ElseIf InStr(1, rstrInputString, vbLf) > 0 Then
        
            rstrLines = Split(rstrInputString, vbLf)
        Else
            ReDim rstrLines(0)
            
            rstrLines(0) = rstrInputString
        End If
    End If
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToReadDicAfterCreateExcelSheet()

    Dim strLog As String, objSheet As Excel.Worksheet
    Dim objDic As Scripting.Dictionary
    
    strLog = "Test auto-shape with texts" & vbNewLine & "Time: 2.0 [s]" & vbNewLine & "Search keywords:AI,�@�B�w�K"
    
    Set objSheet = mfobjGetTestingNewSheet(strLog)
    

    AddAutoShapeGeneralMessage objSheet, strLog
    
    Set objDic = GetKeyValueDicFromShapes(objSheet)
    
    DebugDic objDic
End Sub

'''
'''
'''
Private Function mfobjGetTestingNewSheet(ByVal vstrLog As String) As Excel.Worksheet

    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    
    strBookPath = mfstrGetTmporaryBookDirPath() & "SanityTestToGetKeyValueDicFromShapes.xlsx"

    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet.Name = "ForAutoShapes"


    DeleteDummySheetWhenItExists objBook

    Set mfobjGetTestingNewSheet = objSheet
End Function


'''
'''
'''
Private Function mfstrGetTmporaryBookDirPath() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
    
    ForceToCreateDirectory strDir

    mfstrGetTmporaryBookDirPath = strDir
End Function






