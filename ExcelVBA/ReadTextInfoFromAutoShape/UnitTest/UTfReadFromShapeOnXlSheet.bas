Attribute VB_Name = "UTfReadFromShapeOnXlSheet"
'
'   Sanity tests for getting key-value dictionary from auto-shape on Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on SqlUTfCsvAdoSheetExpander.bas, SqlUTfXlSheetExpander.bas, WinRegViewForXl
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 22/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Sanity test to output results to Excel book
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToOutputToExcelBookAfterGetShapeNameToKeyValueDicByReadingFromLoadedWinRegSubKeysSheet()

    Dim objDic As Scripting.Dictionary, strBookPath As String, strSubRegKey As String
 
    strSubRegKey = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion"

    Set objDic = mfobjGetDicFromReadingFromLoadedWinRegSubKeysBook(strSubRegKey, True)
    
    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\SanityTestReadAllShapesKeyValuesOfSheet.xlsx"

    OutputSheetShapeNameToKeyValuesDicToBook objDic, strBookPath
End Sub


'**---------------------------------------------
'** get Dictionary test
'**---------------------------------------------
'''
''' get Dictionary(Of ShapeName, Dictionary(Of Key, Value)) about loaded INI file
'''
Private Sub msubSanityTestToGetShapeNameToKeyValueDicByReadingFromLoadedSampleIniDataBook()

    DebugDic mfobjGetShapeNameToKeyValueDicFromReadingFromLoadedSampleIniDataBook()
End Sub



'''
''' get Dictionary(Of Key, Value) about loaded INI file
'''
Private Sub msubSanityTestToGetKeyValueDicByReadingFromLoadedSampleIniDataBook()

    DebugDic mfobjGetKeyValueDicFromReadingFromLoadedSampleIniDataBook()
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** Tools for getting Dictinary objects
'**---------------------------------------------
'''
''' get Dictionary(Of ShapeName, Dictionary(Of Key, Value)) about sample .ini file
'''
Private Function mfobjGetShapeNameToKeyValueDicFromReadingFromLoadedSampleIniDataBook() As Scripting.Dictionary

    Set mfobjGetShapeNameToKeyValueDicFromReadingFromLoadedSampleIniDataBook = mfobjGetDicFromReadingFromLoadedSampleIniDataBook(True)
End Function


'''
''' get Dictionary(Of Key, Value) about sample .ini file
'''
Private Function mfobjGetKeyValueDicFromReadingFromLoadedSampleIniDataBook() As Scripting.Dictionary

    Set mfobjGetKeyValueDicFromReadingFromLoadedSampleIniDataBook = mfobjGetDicFromReadingFromLoadedSampleIniDataBook(False)
End Function


'''
''' About sample .ini file
'''
Private Function mfobjGetDicFromReadingFromLoadedSampleIniDataBook(Optional ByVal vblnGetShapeNameToKeyValueDic As Boolean = True) As Scripting.Dictionary

    Dim strBookPath As String, objSheet As Excel.Worksheet
    Dim objDic As Scripting.Dictionary

    strBookPath = GetOutputExcelBookPathAfterCreateBookOfLoadedIniAllData()

    With GetWorkbook(strBookPath)
    
        Set objSheet = .Worksheets.Item(1)
    
        If vblnGetShapeNameToKeyValueDic Then
        
            Set objDic = GetShapeNameToKeyValueDicFromShapes(objSheet)
        Else
            Set objDic = GetKeyValueDicFromShapes(objSheet)
        End If
    End With
    
    Set mfobjGetDicFromReadingFromLoadedSampleIniDataBook = objDic
End Function


'''
''' About loaded Windows registry outputted sheet
'''
Private Function mfobjGetDicFromReadingFromLoadedWinRegSubKeysBook(ByVal vstrFullSubKey As String, _
        Optional ByVal vblnGetShapeNameToKeyValueDic As Boolean = True) As Scripting.Dictionary

    Dim strBookPath As String, objSheet As Excel.Worksheet
    Dim objDic As Scripting.Dictionary

    strBookPath = GetWinRegistoriesInfomationBookPath()

    OutputWinRegSubKeysToSheet vstrFullSubKey
    
    With GetWorkbook(strBookPath)
    
        Set objSheet = .Worksheets.Item(1)
    
        If vblnGetShapeNameToKeyValueDic Then
        
            Set objDic = GetShapeNameToKeyValueDicFromShapes(objSheet)
        Else
            Set objDic = GetKeyValueDicFromShapes(objSheet)
        End If
    End With

    Set mfobjGetDicFromReadingFromLoadedWinRegSubKeysBook = objDic
End Function
