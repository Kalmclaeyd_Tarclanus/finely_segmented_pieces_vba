Attribute VB_Name = "UTfReadFromShapeByAdoCsv"
'
'   Sanity tests for getting key-value dictionary from auto-shape on Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on ReadFromShapeOnXlSheet.bas
'       Dependent on both UTfReadFromShapeOnXlSheet.bas and SqlUTfCsvAdoSheetExpander.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun, 18/Aug/2024    Kalmclaeyd Tarclanus    Separated from UTfReadFromShapeOnXlSheet.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** get Dictionary test
'**---------------------------------------------
'''
''' get Dictionary(Of String[ShapeName], Dictionary(Of String[Key], String[Value]))
'''
Private Sub msubSanityTestToGetShapeNameToKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()

    DebugDic mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
End Sub


'''
''' get Dictionary(Of String[Key], String[Value]) about CsvAdoSheetExpander
'''
Private Sub msubSanityTestToGetKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()

    Dim objDic As Scripting.Dictionary

    Set objDic = mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()

    DebugDic objDic
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetShapeNameToKeyValueDicByReadingFromExcelAdoExpanderAutoShapes()

    DebugDic mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
End Sub

'**---------------------------------------------
'** Sanity test to output results to Excel book
'**---------------------------------------------
'''
''' get Dictionary(Of String[ShapeName], Dictionary(Of String[Key], String[Value]))
'''
Private Sub msubSanityTestToOutputToExcelBookAfterGetShapeNameToKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()

    Dim objDic As Scripting.Dictionary, strBookPath As String

    Set objDic = mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()

    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\SanityTestReadAllShapesKeyValuesOfSheet.xlsx"

    OutputSheetShapeNameToKeyValuesDicToBook objDic, strBookPath
End Sub

'''
''' get Dictionary(Of String[Key], String[Value]) about CsvAdoSheetExpander
'''
Private Sub msubSanityTestToOutputToExcelBookAfterGetKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()

    Dim objDic As Scripting.Dictionary, strBookPath As String

    Set objDic = mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
    
    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\SanityTestReadAllShapesKeyValuesOfSheet.xlsx"
    
    OutputSheetShapesKeyValuesToBook objDic, strBookPath
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** Tools for getting Dictinary objects
'**---------------------------------------------
'''
'''
'''
Private Function mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes() As Scripting.Dictionary

    Set mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes = mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes(True)
End Function

'''
'''
'''
Private Function mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes() As Scripting.Dictionary

    Set mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes = mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes(False)
End Function

'''
'''
'''
Private Function mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes(Optional ByVal vblnGetShapeNameToKeyValueDic As Boolean = True) As Scripting.Dictionary

    Dim strBookPath As String, objSheet As Excel.Worksheet
    Dim objDic As Scripting.Dictionary
    
    strBookPath = GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate(BasicSampleDT, 100, False)
    
    With GetWorkbook(strBookPath)
    
        Set objSheet = .Worksheets.Item(1)
    
        If vblnGetShapeNameToKeyValueDic Then
        
            Set objDic = GetShapeNameToKeyValueDicFromShapes(objSheet)
        Else
            Set objDic = GetKeyValueDicFromShapes(objSheet)
        End If
    End With

    Set mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes = objDic
End Function

