Attribute VB_Name = "UTfRemoveAllVBACodesForXl"
'
'   Sanity-tests for RemoveAllVBA.bas, ExportAllVBA.bas, and ImportAllVBA.bas. This test is dependent on the many other Excel-VBA codes
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Excel and VBIDE
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 27/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////

#Const HAS_REF = False

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrCommonVBAGenerationSubDirectoryName As String = "GenerateVBACodes"

Private Const mstrCommonTemporarySourceExportedDirectoryName As String = "TemporaryExportedCodes"

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToExportAllVBACodes()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "ExportAllVBACodes,ImportAllVBACodes,RemoveAllVBACodes"
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Public Sub SanityTestToExportAndImportAndRemoveAllCodes()

    msubSanityTestToClearFilesBeforeExportCodes
    
    msubSanityTestToExportCodes

    SanityTestToImportCodesWithCreatingNewBook

    msubSanityTestToRemovingVBACodes
End Sub


'***--------------------------------------------
'*** Excel dependent Export all VBA codes test
'***--------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToExportCodes()

    Dim strTmpDir As String
    
    strTmpDir = mfstrGetTemporaryCodeDirectoryPath()
    
    SaveAllVBAProjectCodesWithoutObjectModule strTmpDir, mfobjGetCurrentOfficeFile().VBProject
End Sub


'''
''' Update all VBA codes
'''
Private Sub msubSanityTestToUpdateCodesOfSaveallVBAProjectCodesFromSampleExcelBook()

    msubSanityTestToExportOrUpdateCodesOfSaveallVBAProjectCodesFromSampleExcelBook True
End Sub

'''
''' Save (overwrite) all VBA codes
'''
Private Sub msubSanityTestToExportCodesOfSaveallVBAProjectCodesFromSampleExcelBook()

    msubSanityTestToExportOrUpdateCodesOfSaveallVBAProjectCodesFromSampleExcelBook False
End Sub



'''
'''
'''
Private Sub msubSanityTestToExportOrUpdateCodesOfSaveallVBAProjectCodesFromSampleExcelBook(ByVal vblnUpdateCodes As Boolean)

    Dim strBookDir As String, strBookName As String, strBookPath As String, objOfficeFile As Object
    
    Dim strTmpDir As String, blnIsAlreadyOpened As Boolean
    
    
#If HAS_REF Then

    Dim objExcelApplication As Excel.Application, objFS As Scripting.FileSystemObject
    
    Dim objBook As Excel.Workbook
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objExcelApplication As Object, objFS As Object
    
    Dim objBook As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If
    
    Set objOfficeFile = mfobjGetCurrentOfficeFile()
    
    strTmpDir = mfstrGetTemporaryCodeDirectoryPath()
    
    strBookDir = objOfficeFile.Path & "\GeneratedMacroBooks"

    strBookName = "SeparatedSwitchCodePanes.xlsm"

    strBookPath = strBookDir & "\" & strBookName

    With objFS

        If .FileExists(strBookPath) Then

            Set objExcelApplication = mfobjGetExcelApplication()

            Set objBook = mfobjGetWorkbook(strBookPath, objExcelApplication, blnIsAlreadyOpened)
            
            If vblnUpdateCodes Then
            
                UpdateAllVBAProjectCodes strTmpDir, objBook.VBProject, "", True, False
            Else
                ClearAllFilesBeforeSaveAllVBAProjectCodes strTmpDir
            
                SaveAllVBAProjectCodes strTmpDir, objBook.VBProject, True, False
            End If
            
            If Not blnIsAlreadyOpened Then
            
                objBook.Saved = True
            
                objBook.Close
            End If
        End If
    End With
End Sub

'''
'''
'''
#If HAS_REF Then

Private Function mfobjGetWorkbook(ByRef rstrBookPath As String, ByRef robjExcelApplication As Excel.Application, ByRef rblnIsAlreadyOpened As Boolean) As Excel.Workbook

    Dim objBook As Excel.Workbook, objTargetBook As Excel.Workbook, objFS As Scripting.FileSystemObject
    
    Set objFS = New Scripting.FileSystemObject
#Else
Private Function mfobjGetWorkbook(ByRef rstrBookPath As String, ByRef robjExcelApplication As Object, ByRef rblnIsAlreadyOpened As Boolean) As Object

    Dim objBook As Object, objTargetBook As Object, objFS As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If
    
    rblnIsAlreadyOpened = False
    
    With objFS
    
        For Each objBook In robjExcelApplication.Workbooks
        
            If StrComp(.GetBaseName(objBook.Name), .GetBaseName(rstrBookPath)) = 0 Then
            
                Set objTargetBook = objBook
                
                rblnIsAlreadyOpened = True
                
                Exit For
            End If
        Next
    End With
    
    If objTargetBook Is Nothing Then
    
        Set objTargetBook = robjExcelApplication.Workbooks.Open(rstrBookPath, ReadOnly:=True)
    End If
    
    Set mfobjGetWorkbook = objTargetBook
End Function


'''
''' clear VB code files and the specified log text file
'''
Private Sub msubSanityTestToClearFilesBeforeExportCodes()

    Dim strTmpDir As String
    
    strTmpDir = mfstrGetTemporaryCodeDirectoryPath()

    ClearAllFilesBeforeSaveAllVBAProjectCodes strTmpDir
End Sub



'***--------------------------------------------
'*** Excel dependent Remove all VBA codes test
'***--------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToRemovingVBACodes()

    Dim objBook As Excel.Workbook, strTmpDir As String, strTmpBookPath As String
    
    Const strTmpBookBaseName As String = "TemporaryMacroBook"
    
    strTmpDir = GetCurrentOfficeFileObject().Path & "\TemporaryBooks"
    
    strTmpBookPath = strTmpDir & "\" & strTmpBookBaseName & ".xlsm"
    
    Set objBook = GetWorkbookIfItExists(strTmpBookPath)

    RemoveAllVBACodesWhenThisIsNotActiveApplication objBook
End Sub


'***--------------------------------------------
'*** Other office application importing tests
'***--------------------------------------------
'''
''' for Word test
'''
Private Sub msubSanityTestToImportCodesIntoAWordDocument()

    Dim objDocument As Object, objApplication As Object, strVBACodeDirectoryPath As String
    
    If IsTheSpecifiedOfficeApplicationInstalledInThisComputer("Word") Then
    
        Set objApplication = CreateObject("Word.Application")
        
        Set objDocument = objApplication.Documents.Add()
    
        objDocument.VBProject.Name = "AutoImportedTestVBA"
    
        'strVBACodeDirectoryPath = GetRepositoryCodesRootDir() & "\CommonOfficeVBA"
        
        strVBACodeDirectoryPath = GetRepositoryCodesRootDir() & "\CommonOfficeVBA\Clipboard"
    
        LoadAllVBACodesFromDirectory strVBACodeDirectoryPath, objDocument
        
        objApplication.Visible = True
    Else
        MsgBox "The Microsoft Word is not installed in this computer.", vbCritical Or vbOKOnly
    End If
End Sub

'''
''' for PowerPoint test
'''
Private Sub msubSanityTestToImportCodesIntoAPowerPointPresentation()

    Dim objPresentation As Object, objApplication As Object, strVBACodeDirectoryPath As String
    
    Dim objApplicationObjects As Object
    
    If IsTheSpecifiedOfficeApplicationInstalledInThisComputer("PowerPoint") Then
    
        Set objApplication = CreateObject("PowerPoint.Application")
        
        Set objApplicationObjects = VBA.CallByName(objApplication, "Presentations", VbGet)
        
        Set objPresentation = objApplicationObjects.Add()
    
        objPresentation.VBProject.Name = "AutoImportedTestVBA"
    
        'strVBACodeDirectoryPath = GetRepositoryCodesRootDir() & "\CommonOfficeVBA"
        
        strVBACodeDirectoryPath = GetRepositoryCodesRootDir() & "\CommonOfficeVBA\Clipboard"
    
        LoadAllVBACodesFromDirectory strVBACodeDirectoryPath, objPresentation
        
        objApplication.Visible = True
    Else
        MsgBox "The Microsoft PowerPoint is not installed in this computer.", vbCritical Or vbOKOnly
    End If
End Sub

'''
'''
'''
Public Function GetTemporaryTestingCodeDirectoryPath() As String

    GetTemporaryTestingCodeDirectoryPath = mfstrGetTemporaryCodeDirectoryPath()
End Function

'''
'''
'''
Public Function GetCurrentOfficeFileOfTemporaryTesting() As Object

    Set GetCurrentOfficeFileOfTemporaryTesting = mfobjGetCurrentOfficeFile()
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrGetTemporaryCodeDirectoryPath() As String

    Dim objOfficeFile As Object, strTmpDir As String
    
    Set objOfficeFile = mfobjGetCurrentOfficeFile()
    
    strTmpDir = objOfficeFile.Path & "\" & mstrCommonVBAGenerationSubDirectoryName

#If HAS_REF Then

    With New Scripting.FileSystemObject
#Else
    With CreateObject("Scripting.FileSystemObject")
#End If
        If Not .FolderExists(strTmpDir) Then
        
            .CreateFolder strTmpDir
        End If
        
        strTmpDir = strTmpDir & "\" & mstrCommonTemporarySourceExportedDirectoryName
        
        If Not .FolderExists(strTmpDir) Then
        
            .CreateFolder strTmpDir
        End If
    End With

    mfstrGetTemporaryCodeDirectoryPath = strTmpDir
End Function

'''
''' Attention! use duplicated codes for keeping this module independency
'''
Private Function mfobjGetCurrentOfficeFile() As Object

    Dim objOfficeObject As Object
    
    Set objOfficeObject = Nothing

    Select Case Trim(Replace(LCase(Application.Name), "microsoft", ""))
    
        Case "excel"
        
            On Error Resume Next
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisWorkbook", VbGet) ' Application.ThisWorkbook
            
            On Error GoTo 0
            
            If objOfficeObject Is Nothing Then
            
                Set objOfficeObject = VBA.CallByName(Application, "ActiveWorkbook", VbGet)  ' Excel.Workbook
            End If
            
        Case "word"
        
            On Error Resume Next
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisDocument", VbGet)
            
            On Error GoTo 0
            
            If objOfficeObject Is Nothing Then
            
                Set objOfficeObject = VBA.CallByName(Application, "ActiveDocument", VbGet)  ' Word.Document
            End If
            
        Case "powerpoint"
    
            Set objOfficeObject = VBA.CallByName(Application, "ActivePresentation", VbGet)  ' PowerPoint.Presentation
    End Select

    Set mfobjGetCurrentOfficeFile = objOfficeObject
End Function

'''
'''
'''
Private Function mfobjGetExcelApplication() As Object

    Dim objExcelApplication As Object

    Select Case Trim(Replace(LCase(Application.Name), "microsoft", ""))
    
        Case "excel"

            Set objExcelApplication = Application

        Case Else

            On Error Resume Next
            
            Set objExcelApplication = GetObject(Class:="Excel.Application")
            
            On Error GoTo 0

            If Not objExcelApplication Then
            
                Set objExcelApplication = CreateObject("Excel.Application")
            End If
    End Select

    Set mfobjGetExcelApplication = objExcelApplication
End Function

