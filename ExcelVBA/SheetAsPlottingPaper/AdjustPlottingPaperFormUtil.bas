Attribute VB_Name = "AdjustPlottingPaperFormUtil"
'
'   Utilities to control for adjusting a plotting paper Excel sheet tool form
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on MSForms.UserForm
'       Dependent on AdjustPlottingPaperSheet.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 25/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "AdjustPlottingPaperFormUtil"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjUAdjustPlottingPaperSheet As UAdjustPlottingPaperSheet

'**---------------------------------------------
'** Key-Value cache preparation for AdjustPlottingPaperFormUtil
'**---------------------------------------------
Private mobjStrKeyValueAdjustPlottingPaperFormUtilDic As Scripting.Dictionary

Private mblnIsStrKeyValueAdjustPlottingPaperFormUtilDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open UAdjustPlottingPaperSheet form
'**---------------------------------------------
'''
'''
'''
Public Sub OpenAdjustPlottingPaperFormByCurrentSelectedRange()

    Set mobjUAdjustPlottingPaperSheet = New UAdjustPlottingPaperSheet
    
    With mobjUAdjustPlottingPaperSheet
    
        Set .ExcelApplication = ThisWorkbook.Application
        
        .RefreshAllControlsBySelectedRange CurrentSelectedXlRange()
        
        .Show vbModeless
    End With
End Sub


'**---------------------------------------------
'** Implement body of RefleshTextsFromCurrentPrintArea of ICtlTextBoxesOfCurrentPrintArea interface
'**---------------------------------------------
'''
'''
'''
Public Sub RefleshTextsFromCurrentPrintAreaForCtlTextBoxesOfCurrentPrintAreaInterface(ByRef ritfCtlTextBoxesOfCurrentPrintArea As ICtlTextBoxesOfCurrentPrintArea, ByVal vobjRange As Excel.Range)

    Dim strPrintAreaAddress As String, strPrintAreaRowsCount As String, strPrintAreaColumnsCount As String
    Dim objSheet As Excel.Worksheet
    
    Set objSheet = vobjRange.Worksheet
    
    With objSheet

        If .PageSetup.PrintArea <> "" Then
        
            strPrintAreaAddress = .PageSetup.PrintArea
            
            With .Range(.PageSetup.PrintArea)
            
                strPrintAreaRowsCount = CStr(.Rows.Count)
            
                strPrintAreaColumnsCount = CStr(.Columns.Count)
            End With
            
            ritfCtlTextBoxesOfCurrentPrintArea.IsPrintAreaSetUpOfSelectedSheet = True
        Else
            strPrintAreaAddress = "": strPrintAreaRowsCount = "": strPrintAreaColumnsCount = ""
            
            ritfCtlTextBoxesOfCurrentPrintArea.IsPrintAreaSetUpOfSelectedSheet = False
        End If
    End With

    With ritfCtlTextBoxesOfCurrentPrintArea
    
        UpdateUserControlTextBoxWithEnabledState .CurrentPrintAreaAddressTextBox, strPrintAreaAddress
    
        UpdateUserControlTextBoxWithEnabledState .CurrentPrintAreaRowsCountTextBox, strPrintAreaRowsCount
        
        UpdateUserControlTextBoxWithEnabledState .CurrentPrintAreaColumnsCountTextBox, strPrintAreaColumnsCount
    End With
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubGetUserControlsNames()

    Set mobjUAdjustPlottingPaperSheet = New UAdjustPlottingPaperSheet

    Dim objControl As Object

    Debug.Print TypeName(mobjUAdjustPlottingPaperSheet)

    For Each objControl In mobjUAdjustPlottingPaperSheet.Controls

        Debug.Print objControl.Name & ", " & TypeName(objControl)
    Next
End Sub


'///////////////////////////////////////////////
'/// Localizing operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Key-Value cache preparation for AdjustPlottingPaperFormUtil
'**---------------------------------------------
'''
''' get string Value from STR_KEY_OPEN_ADJUST_PLOTTING_PAPER_XL_SHEET_FORM
'''
''' automatically-added for AdjustPlottingPaperFormUtil string-key-value management for standard module and class module
Public Function GetTextOfStrKeyOpenAdjustPlottingPaperXlSheetForm() As String

    If Not mblnIsStrKeyValueAdjustPlottingPaperFormUtilDicInitialized Then

        msubInitializeTextForAdjustPlottingPaperFormUtil
    End If

    GetTextOfStrKeyOpenAdjustPlottingPaperXlSheetForm = mobjStrKeyValueAdjustPlottingPaperFormUtilDic.Item("STR_KEY_OPEN_ADJUST_PLOTTING_PAPER_XL_SHEET_FORM")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for AdjustPlottingPaperFormUtil string-key-value management for standard module and class module
Private Sub msubInitializeTextForAdjustPlottingPaperFormUtil()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueAdjustPlottingPaperFormUtilDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForAdjustPlottingPaperFormUtilByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForAdjustPlottingPaperFormUtilByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueAdjustPlottingPaperFormUtilDicInitialized = True
    End If

    Set mobjStrKeyValueAdjustPlottingPaperFormUtilDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for AdjustPlottingPaperFormUtil key-values cache
'''
''' automatically-added for AdjustPlottingPaperFormUtil string-key-value management for standard module and class module
Private Sub AddStringKeyValueForAdjustPlottingPaperFormUtilByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_OPEN_ADJUST_PLOTTING_PAPER_XL_SHEET_FORM", "Open an adjusting plotting-paper worksheet Form"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for AdjustPlottingPaperFormUtil key-values cache
'''
''' automatically-added for AdjustPlottingPaperFormUtil string-key-value management for standard module and class module
Private Sub AddStringKeyValueForAdjustPlottingPaperFormUtilByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_OPEN_ADJUST_PLOTTING_PAPER_XL_SHEET_FORM", "方眼紙シート調整用フォームを開く"
    End With
End Sub

'''
''' Remove Keys for AdjustPlottingPaperFormUtil key-values cache
'''
''' automatically-added for AdjustPlottingPaperFormUtil string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueAdjustPlottingPaperFormUtil(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_OPEN_ADJUST_PLOTTING_PAPER_XL_SHEET_FORM") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_OPEN_ADJUST_PLOTTING_PAPER_XL_SHEET_FORM"
        End If
    End With
End Sub






