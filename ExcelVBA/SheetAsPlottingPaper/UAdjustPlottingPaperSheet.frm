VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UAdjustPlottingPaperSheet 
   Caption         =   "UAdjustPlottingPaperSheet"
   ClientHeight    =   8160
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   4780
   OleObjectBlob   =   "UAdjustPlottingPaperSheet.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "UAdjustPlottingPaperSheet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   Adjust a plotting paper Excel sheet tool form
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on MSForms.UserForm
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 25/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements ICtlTextBoxesOfCurrentRange

Implements ICtlTextBoxesOfCurrentPrintArea

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "UAdjustPlottingPaperSheet"


Private Const mstrGridSizeParametersBookName As String = "PrintAreaGridSizes.xlsx"

Private Const mstrGridSizeParametersSheetName As String = "PrintGridDimensions"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private WithEvents mobjApplication As Excel.Application
Attribute mobjApplication.VB_VarHelpID = -1

Private mblnIsPrintAreaSetUpOfSelectedSheet As Boolean

'**---------------------------------------------
'** Semaphore parameter cache within user-control objects initializing
'**---------------------------------------------
Private mintInitializing As Long

Private mblnIsInitialized As Boolean

'**---------------------------------------------
'** Outside control cache
'**---------------------------------------------
Private mobjTextBoxes As Collection


'**---------------------------------------------
'** Form window state control by Windows-registry
'**---------------------------------------------
Private mobjFormTopMostEnabledCtlHdr As FormTopMostEnabledCtlHdr

Private mobjKeepStateFormControls As Collection

'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub UserForm_Initialize()

    InitializeFormTopMostEnabledControlHandler mobjFormTopMostEnabledCtlHdr, Me


    Set mobjTextBoxes = New Collection
    
    With mobjTextBoxes
    
        .Add txtSelectedBookPath
        
        .Add txtSelectedSheet
        
        .Add txtSelectedSizeInfo
        
        .Add txtCurrentSheetPrintAreaAddress
        
        .Add txtPrintAreaRowsCount
        
        .Add txtPrintAreaColumnsCount
    End With
    
    Set mobjKeepStateFormControls = New Collection
    
    With mobjKeepStateFormControls
    
        .Add txtGridSizeParameterBookPath
    End With
End Sub


Private Sub UserForm_QueryClose(ByRef rintCancel As Integer, ByRef rintCloseMode As Integer)

    SetCurrentFormPositionToUserReg mstrModuleName, Me
    
    SetCurrentFormControlValuesFromUserReg mstrModuleName, mobjKeepStateFormControls
End Sub

'''
'''
'''
Private Sub UserForm_Terminate()

    Set mobjApplication = Nothing
    
    Set mobjTextBoxes = Nothing
End Sub

'''
'''
'''
Private Sub UserForm_Layout()

    If Not mblnIsInitialized Then
    
        GetCurrentFormPositionFromUserReg mstrModuleName, Me
    
        GetCurrentFormControlValuesFromUserReg mstrModuleName, mobjKeepStateFormControls
    
        With New Scripting.FileSystemObject
        
            If Not .FolderExists(.GetParentFolderName(txtGridSizeParameterBookPath.Text)) Then
            
                txtGridSizeParameterBookPath.Text = GetAutomaticRecommendedGridSizeParameterBookPath(txtSelectedBookPath.Text)
            End If
        End With
    
        msubLocalize
        
        SetLockedOfControls mobjTextBoxes
    
        mblnIsInitialized = True
    End If
End Sub


'''
'''
'''
Private Sub mobjApplication_SheetSelectionChange(ByVal vobjSheet As Object, ByVal vobjTarget As Excel.Range)

    If Not vobjTarget Is Nothing Then
    
        RefreshAllControlsBySelectedRange vobjTarget
    End If
End Sub


'''
'''
'''
Private Sub cmdSetAutomaticRecommendedGridSizeParameterBookPath_Click()

    txtGridSizeParameterBookPath.Text = GetAutomaticRecommendedGridSizeParameterBookPath(txtSelectedBookPath.Text)
End Sub

'''
'''
'''
Private Sub cmdGetPlottingPaperGridSizeFromSelectedSheet_Click()

    OutputPlottingPaperLogAndPrintAreaColumnWidthsAndRowHeghtsToSheetByIOSheetParams txtSelectedBookPath.Text, txtSelectedSheet.Text, txtGridSizeParameterBookPath.Text
End Sub

'''
'''
'''
Private Sub cmdReflectGirdSizesOfParamSheetIntoPlottingPaperGrid_Click()

    ReflectLoadedBothColumnWidthsAndRowHeightsByIOSheetParams txtGridSizeParameterBookPath.Text
End Sub


'///////////////////////////////////////////////
'/// Implemented properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** Implemented ICtlTextBoxesOfCurrentRange properties
'**---------------------------------------------
Private Property Get ICtlTextBoxesOfCurrentRange_CurrentBookPathTextBox() As MSForms.IMdcText

    Set ICtlTextBoxesOfCurrentRange_CurrentBookPathTextBox = txtSelectedBookPath
End Property
Private Property Set ICtlTextBoxesOfCurrentRange_CurrentBookPathTextBox(ByVal vobjTextBox As MSForms.IMdcText)
    ' Nothing to do
End Property

Private Property Get ICtlTextBoxesOfCurrentRange_CurrentSheetNameTextBox() As MSForms.IMdcText

    Set ICtlTextBoxesOfCurrentRange_CurrentSheetNameTextBox = txtSelectedSheet
End Property
Private Property Set ICtlTextBoxesOfCurrentRange_CurrentSheetNameTextBox(ByVal vobjTextBox As MSForms.IMdcText)
    ' Nothing to do
End Property


'**---------------------------------------------
'** Implemented ICtlTextBoxesOfCurrentPrintArea properties
'**---------------------------------------------
Private Property Get ICtlTextBoxesOfCurrentPrintArea_CurrentPrintAreaAddressTextBox() As MSForms.IMdcText
    
    Set ICtlTextBoxesOfCurrentPrintArea_CurrentPrintAreaAddressTextBox = txtCurrentSheetPrintAreaAddress
End Property
Private Property Set ICtlTextBoxesOfCurrentPrintArea_CurrentPrintAreaAddressTextBox(ByVal vobjTextBox As MSForms.IMdcText)
    ' Nothing to do
End Property

Private Property Get ICtlTextBoxesOfCurrentPrintArea_CurrentPrintAreaRowsCountTextBox() As MSForms.IMdcText

    Set ICtlTextBoxesOfCurrentPrintArea_CurrentPrintAreaRowsCountTextBox = txtPrintAreaRowsCount
End Property
Private Property Set ICtlTextBoxesOfCurrentPrintArea_CurrentPrintAreaRowsCountTextBox(ByVal vobjTextBox As MSForms.IMdcText)
    ' Nothing to do
End Property

Private Property Get ICtlTextBoxesOfCurrentPrintArea_CurrentPrintAreaColumnsCountTextBox() As MSForms.IMdcText

    Set ICtlTextBoxesOfCurrentPrintArea_CurrentPrintAreaColumnsCountTextBox = txtPrintAreaColumnsCount
End Property
Private Property Set ICtlTextBoxesOfCurrentPrintArea_CurrentPrintAreaColumnsCountTextBox(ByVal vobjTextBox As MSForms.IMdcText)
    ' Nothing to do
End Property

'''
'''
'''
Private Property Get ICtlTextBoxesOfCurrentPrintArea_IsPrintAreaSetUpOfSelectedSheet() As Boolean

    ICtlTextBoxesOfCurrentPrintArea_IsPrintAreaSetUpOfSelectedSheet = mblnIsPrintAreaSetUpOfSelectedSheet
End Property
Private Property Let ICtlTextBoxesOfCurrentPrintArea_IsPrintAreaSetUpOfSelectedSheet(ByVal vblnIsPrintAreaSetUpOfSelectedSheet As Boolean)

    mblnIsPrintAreaSetUpOfSelectedSheet = vblnIsPrintAreaSetUpOfSelectedSheet
End Property

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
''' Monitoring event variable
'''
Property Get ExcelApplication() As Excel.Application

    Set ExcelApplication = mobjApplication
End Property
Property Set ExcelApplication(ByVal vobjApplication As Excel.Application)

    Set mobjApplication = vobjApplication
End Property


'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Implemented ICtlTextBoxesOfCurrentRange
'**---------------------------------------------
Private Sub ICtlTextBoxesOfCurrentRange_RefleshTextsFromCurrentRange(ByVal vobjRange As Excel.Range)

    RefleshTextsFromCurrentRangeByCtlTextBoxesOfCurrentRangeInterface Me, vobjRange
End Sub

'**---------------------------------------------
'** Implemented ICtlTextBoxesOfCurrentPrintArea
'**---------------------------------------------
Private Sub ICtlTextBoxesOfCurrentPrintArea_RefleshTextsFromCurrentPrintArea(ByVal vobjRange As Excel.Range)

    RefleshTextsFromCurrentPrintAreaForCtlTextBoxesOfCurrentPrintAreaInterface Me, vobjRange
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub RefreshAllControlsBySelectedRange(ByVal vobjSelectedRange As Excel.Range)


    ICtlTextBoxesOfCurrentRange_RefleshTextsFromCurrentRange vobjSelectedRange

    txtSelectedSizeInfo.Text = GetSelectedCellsSizeLogText(vobjSelectedRange)

    ICtlTextBoxesOfCurrentPrintArea_RefleshTextsFromCurrentPrintArea vobjSelectedRange
    
    If mblnIsPrintAreaSetUpOfSelectedSheet Then
    
        If txtGridSizeParameterBookPath.Text = "" Then
        
            txtGridSizeParameterBookPath.Text = GetAutomaticRecommendedGridSizeParameterBookPath(txtSelectedBookPath.Text)
        End If
    End If
    
    SetLockedOfControls mobjTextBoxes
    
    ControlButtonsEnabledForAdjustExecutions
End Sub

'''
'''
'''
Public Sub ControlButtonsEnabledForAdjustExecutions()

    If mblnIsPrintAreaSetUpOfSelectedSheet Then
    
        cmdGetPlottingPaperGridSizeFromSelectedSheet.Enabled = True
        
        cmdReflectGirdSizesOfParamSheetIntoPlottingPaperGrid.Enabled = True
        
        
        cmdSetAutomaticRecommendedGridSizeParameterBookPath.Enabled = True
    Else
        cmdGetPlottingPaperGridSizeFromSelectedSheet.Enabled = False
        
        cmdReflectGirdSizesOfParamSheetIntoPlottingPaperGrid.Enabled = False
        
        
        cmdSetAutomaticRecommendedGridSizeParameterBookPath.Enabled = False
    End If
End Sub



'**---------------------------------------------
'** Key-Value cache preparation for UAdjustPlottingPaperSheet
'**---------------------------------------------

'''
''' Query the localized strings from either the external Key-Value table database or the default values from codes
'''
''' automatically-added for UAdjustPlottingPaperSheet string-key-value management
Private Sub msubLocalize()

    Dim objKeyToTextDic As Scripting.Dictionary

    Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

    If objKeyToTextDic Is Nothing Then

        ' If the Key-Value table file is lost, then the default setting is used

        Set objKeyToTextDic = New Scripting.Dictionary

        Select Case GetCurrentUICaptionLanguageType()

            Case CaptionLanguageType.UIJapaneseCaptions

                AddStringKeyValueForUAdjustPlottingPaperSheetByJapaneseValues objKeyToTextDic
            Case Else

                AddStringKeyValueForUAdjustPlottingPaperSheetByEnglishValues objKeyToTextDic
        End Select
    End If

    AssigningStringValuesForUAdjustPlottingPaperSheet objKeyToTextDic
End Sub

'''
''' Assign each target string property from dictionary values
'''
''' automatically-added for UAdjustPlottingPaperSheet string-key-value management
Private Sub AssigningStringValuesForUAdjustPlottingPaperSheet(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        Me.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FORM_TITLE")

        lblSelectedSizeInfo.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_SIZE_INFO")

        fraAdjustPlottingPaperSheet.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_ADJUST_PLOTTING_PAPER_SHEET")
        cmdGetPlottingPaperGridSizeFromSelectedSheet.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_GET_PLOTTING_PAPER_GRID_SIZE_FROM_SELECTED_SHEET")
        cmdReflectGirdSizesOfParamSheetIntoPlottingPaperGrid.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_REFLECT_GIRD_SIZES_OF_PARAM_SHEET_INTO_PLOTTING_PAPER_GRID")
        lblGridSizeParameterBookPath.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_GRID_SIZE_PARAMETER_BOOK_PATH")

        fraSelectedWorksheet.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_SELECTED_WORKSHEET")
        lblSelectedBookPath.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_BOOK_PATH")
        lblSelectedSheet.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_SHEET")

        fraPrintAreaInformation.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_PRINT_AREA_INFORMATION")
        lblPrintAreaAddress.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_ADDRESS")
        lblPrintAreaRowsCount.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_ROWS_COUNT")
        lblPrintAreaColumnsCount.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_COLUMNS_COUNT")
        cmdSetAutomaticRecommendedGridSizeParameterBookPath.Caption = .Item("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_SET_AUTOMATIC_RECOMMENDED_GRID_SIZE_PARAMETER_BOOK_PATH")
    End With
End Sub

'''
''' Add Key-Values which values are in English for UAdjustPlottingPaperSheet key-values cache
'''
''' automatically-added for UAdjustPlottingPaperSheet string-key-value management
Private Sub AddStringKeyValueForUAdjustPlottingPaperSheetByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FORM_TITLE", "Adjust a plotting-paper sheet"

        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_SIZE_INFO", "Selected range size information"

        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_ADJUST_PLOTTING_PAPER_SHEET", "Adjust the current plotting-paper sheet"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_GET_PLOTTING_PAPER_GRID_SIZE_FROM_SELECTED_SHEET", "Get the plotting-paper grid sizes from selected sheet"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_REFLECT_GIRD_SIZES_OF_PARAM_SHEET_INTO_PLOTTING_PAPER_GRID", "Reflect grid modified sizes to the plotting-paper sheet"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_GRID_SIZE_PARAMETER_BOOK_PATH", "Grid size parameter modification book path"

        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_SELECTED_WORKSHEET", "Selected a worksheet"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_BOOK_PATH", "Selected book path"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_SHEET", "Selected sheet name"

        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_PRINT_AREA_INFORMATION", "Print area information Print_Area"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_ADDRESS", "Print area address"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_ROWS_COUNT", "Rows count"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_COLUMNS_COUNT", "Columns count"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_SET_AUTOMATIC_RECOMMENDED_GRID_SIZE_PARAMETER_BOOK_PATH", "Set defaul path"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for UAdjustPlottingPaperSheet key-values cache
'''
''' automatically-added for UAdjustPlottingPaperSheet string-key-value management
Private Sub AddStringKeyValueForUAdjustPlottingPaperSheetByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FORM_TITLE", "方眼紙シートを調整"

        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_SIZE_INFO", "選択中セルのサイズ情報"

        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_ADJUST_PLOTTING_PAPER_SHEET", "選択中の方眼紙シートを調整"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_GET_PLOTTING_PAPER_GRID_SIZE_FROM_SELECTED_SHEET", "選択中の方眼紙シートよりサイズ読み取り"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_REFLECT_GIRD_SIZES_OF_PARAM_SHEET_INTO_PLOTTING_PAPER_GRID", "編集後の数値を読み取り、元の方眼紙シートを修正"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_GRID_SIZE_PARAMETER_BOOK_PATH", "方眼紙サイズ出力・編集用ブックのパス"

        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_SELECTED_WORKSHEET", "選択中のシート"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_BOOK_PATH", "選択中のブックのパス"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_SHEET", "選択中のシート名"

        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_PRINT_AREA_INFORMATION", "印刷範囲情報 Print_Area"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_ADDRESS", "印刷範囲のアドレス"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_ROWS_COUNT", "行数"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_COLUMNS_COUNT", "列数"
        .Add "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_SET_AUTOMATIC_RECOMMENDED_GRID_SIZE_PARAMETER_BOOK_PATH", "既定のパスを設定"
    End With
End Sub

'''
''' Remove Keys for UAdjustPlottingPaperSheet key-values cache
'''
''' automatically-added for UAdjustPlottingPaperSheet string-key-value management
Private Sub RemoveKeysOfKeyValueUAdjustPlottingPaperSheet(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FORM_TITLE") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FORM_TITLE"

            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_SIZE_INFO"

            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_ADJUST_PLOTTING_PAPER_SHEET"
            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_GET_PLOTTING_PAPER_GRID_SIZE_FROM_SELECTED_SHEET"
            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_REFLECT_GIRD_SIZES_OF_PARAM_SHEET_INTO_PLOTTING_PAPER_GRID"
            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_GRID_SIZE_PARAMETER_BOOK_PATH"

            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_SELECTED_WORKSHEET"
            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_BOOK_PATH"
            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_SELECTED_SHEET"

            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_FRA_PRINT_AREA_INFORMATION"
            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_ADDRESS"
            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_ROWS_COUNT"
            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_LBL_PRINT_AREA_COLUMNS_COUNT"
            .Remove "STR_KEY_U_ADJUST_PLOTTING_PAPER_SHEET_CMD_SET_AUTOMATIC_RECOMMENDED_GRID_SIZE_PARAMETER_BOOK_PATH"
        End If
    End With
End Sub



