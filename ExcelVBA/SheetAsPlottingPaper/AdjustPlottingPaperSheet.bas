Attribute VB_Name = "AdjustPlottingPaperSheet"
'
'   adjust both column-widths and row-heights of a plotting-paper Worksheet for printing
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 13/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrGridSizeParametersBookName As String = "PrintAreaGridSizes.xlsx"

Private Const mstrGridSizeParametersSheetName As String = "PrintGridDimensions"


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToAdjustPlottingPaperSheet()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "AdjustPlottingPaperFormUtil,UAdjustPlottingPaperSheet,ICtlTextBoxesOfCurrentRange,CurrentSelectedXlCellsWithForm,CurrentSelectedXlCells"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** get user-defined paper constant value
'**---------------------------------------------
'''
'''
'''
Public Function GetActivePrinterUserDefinedPaperConstantValue() As Long
    
    Dim intCurrentConstant As Integer, objSheet As Excel.Worksheet
    
    ActiveWorkbook.Activate
    
    Set objSheet = ActiveSheet
    
    With objSheet
        
        .Select
        
        Debug.Print .Application.ActivePrinter
        
        intCurrentConstant = .PageSetup.PaperSize
        
        '.PageSetup.PaperSize = xlPaperUser
        
    End With
    
    MsgBox "User-defined paper size constant value: " & CStr(intCurrentConstant) & ", vbOKOnly"
    
    GetActivePrinterUserDefinedPaperConstantValue = intCurrentConstant
End Function

'**---------------------------------------------
'** Generate book to manage size parameters sheet
'**---------------------------------------------
'''
'''
'''
Public Sub ReflectLoadedBothColumnWidthsAndRowHeightsByIOSheetParams(ByVal vstrInputBookPath As String, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)


    Dim objPlottingPaperLogDic As Scripting.Dictionary, objColumnWidthsDTCol As Collection, objRowHeightsDTCol As Collection

    LoadPrintAreaParametersFromInputtedSheetByIOSheetParams objPlottingPaperLogDic, objColumnWidthsDTCol, objRowHeightsDTCol, vstrInputBookPath, vintTopLeftRowIndex, vintTopLeftColumnIndex

    ReflectLoadedBothColumnWidthsAndRowHeights objPlottingPaperLogDic, objColumnWidthsDTCol, objRowHeightsDTCol
End Sub

'''
''' get figures of both column-widths and row-heights from changed input Worksheet
'''
Public Sub LoadPrintAreaParametersFromInputtedSheetByIOSheetParams(ByRef robjPlottingPaperLogDic As Scripting.Dictionary, _
        ByRef robjColumnWidthsDTCol As Collection, _
        ByRef robjRowHeightsDTCol As Collection, _
        ByVal vstrInputBookPath As String, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    Dim objInputSheet As Excel.Worksheet
        
    Set objInputSheet = GetWorksheetFromBookPathAndSheetName(vstrInputBookPath, mstrGridSizeParametersSheetName)
        
    LoadPrintAreaParametersFromInputtedSheet robjPlottingPaperLogDic, robjColumnWidthsDTCol, robjRowHeightsDTCol, objInputSheet, vintTopLeftRowIndex, vintTopLeftColumnIndex
End Sub

'''
'''
'''
Public Sub OutputPlottingPaperLogAndPrintAreaColumnWidthsAndRowHeghtsToSheetByIOSheetParams(ByVal vstrInputPlottingPaperBookPath As String, _
        ByVal vstrInputPlottingPaperSheetName As String, _
        ByVal vstrOutputGridSizeParametersBookPath As String)

    Dim objParameterSheet As Excel.Worksheet, objPlottingPaperLogDic As Scripting.Dictionary
    Dim strPrintArea As String, objColumnWidthsDTCol As Collection, objRowHeightsDTCol As Collection


    Set objParameterSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputGridSizeParametersBookPath, mstrGridSizeParametersSheetName)
    
    LoadPrintAreaParameters strPrintArea, objColumnWidthsDTCol, objRowHeightsDTCol, vstrInputPlottingPaperBookPath, vstrInputPlottingPaperSheetName, False


    Set objPlottingPaperLogDic = GetPlottingPaperLogDicFromSettings(vstrInputPlottingPaperBookPath, vstrInputPlottingPaperSheetName, strPrintArea)

    OutputPlottingPaperLogAndPrintAreaColumnWidthsAndRowHeghtsToSheet objPlottingPaperLogDic, objColumnWidthsDTCol, objRowHeightsDTCol, objParameterSheet
End Sub

'''
'''
'''
Public Function GetAutomaticRecommendedGridSizeParameterBookPath(ByVal vstrPlottingPaperBookPath As String) As String

    Dim strDir As String, strPath As String
    
    strDir = GetTemporaryOutputBookDirWithConsideringRemovableDisk(vstrPlottingPaperBookPath)

    strPath = strDir & "\" & mstrGridSizeParametersBookName

    GetAutomaticRecommendedGridSizeParameterBookPath = strPath
End Function


'**---------------------------------------------
'** Reflect parameters from the prepared plotting-paper sheet
'**---------------------------------------------
'''
'''
'''
Public Sub ReflectLoadedBothColumnWidthsAndRowHeights(ByVal vobjPlottingPaperLogDic As Scripting.Dictionary, _
        ByVal vobjColumnWidthsDTCol As Collection, _
        ByVal vobjRowHeightsDTCol As Collection)

    Dim strReflectBookPath As String, strReflectSheetName As String, objReflectSheet As Excel.Worksheet
    
    With vobjPlottingPaperLogDic
    
        strReflectBookPath = .Item("InputBookPath")
        
        strReflectSheetName = .Item("PlottingPaperSheetName")
    End With

    Set objReflectSheet = GetWorksheetFromBookPathAndSheetName(strReflectBookPath, strReflectSheetName)

    ' About column-widths
    msubReflectLoadedColumnWidthsDTColToPlottingPaperSheet objReflectSheet, vobjColumnWidthsDTCol

    ' About row-heights
    msubReflectLoadedRowHeightsDTColToPlottingPaperSheet objReflectSheet, vobjRowHeightsDTCol
End Sub


'''
'''
'''
Public Function GetPlottingPaperLogDicFromSettings(ByVal vstrPlottingPaperBookPath As String, ByVal vstrPlottingPaperSheetName As String, ByVal vstrPrintAreaAddress As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add "InputBookPath", vstrPlottingPaperBookPath
        
        .Add "PlottingPaperSheetName", vstrPlottingPaperSheetName
    
        .Add "PrintAreaAddress", vstrPrintAreaAddress
    End With

    Set GetPlottingPaperLogDicFromSettings = objDic
End Function


'''
''' get figures of both column-widths and row-heights from changed input Worksheet
'''
Public Sub LoadPrintAreaParametersFromInputtedSheet(ByRef robjPlottingPaperLogDic As Scripting.Dictionary, _
        ByRef robjColumnWidthsDTCol As Collection, _
        ByRef robjRowHeightsDTCol As Collection, _
        ByVal vobjInputSheet As Excel.Worksheet, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    Dim objFieldTitlesCol As Collection
    
    Set robjPlottingPaperLogDic = GetDictionaryFromExpandedKeyValuesWithoutHeaderSimply(vobjInputSheet, vintTopLeftRowIndex, vintTopLeftColumnIndex)
    
    GetDTTableAndFieldTitlesFromSheet robjColumnWidthsDTCol, objFieldTitlesCol, vobjInputSheet, vintTopLeftRowIndex + robjPlottingPaperLogDic.Count + 1, vintTopLeftColumnIndex + 6
    
    GetDTTableAndFieldTitlesFromSheet robjRowHeightsDTCol, objFieldTitlesCol, vobjInputSheet, vintTopLeftRowIndex + robjPlottingPaperLogDic.Count + 1, vintTopLeftColumnIndex + 1
End Sub


'**---------------------------------------------
'** Output grid cell size parameters to a prepared sheet
'**---------------------------------------------
'''
'''
'''
Public Sub OutputPlottingPaperLogAndPrintAreaColumnWidthsAndRowHeghtsToSheet(ByVal vobjPlottingPaperLogDic As Scripting.Dictionary, _
        ByVal vobjColumnWidthsDTCol As Collection, _
        ByVal vobjRowHeightsDTCol As Collection, _
        ByVal vobjOutputSheet As Excel.Worksheet, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)

    Dim intTableExpandAreaTopLeftRowIndex As Long, intTableExpandAreaTopLeftColumnIndex As Long, objKeyColumnRange As Excel.Range

    OutputDicWithoutHeaderSimplyToCellsOnSheet vobjOutputSheet, vobjPlottingPaperLogDic, vintTopLeftRowIndex, vintTopLeftColumnIndex, DicExpandingKyesColumnsValuesColumnsAsTable, ColumnNameInteriorOfExtractedTableFromSheet

    Set objKeyColumnRange = vobjOutputSheet.Cells(1, vintTopLeftColumnIndex)

    objKeyColumnRange.EntireColumn.ColumnWidth = 24


    intTableExpandAreaTopLeftRowIndex = vintTopLeftRowIndex + vobjPlottingPaperLogDic.Count + 1

    intTableExpandAreaTopLeftColumnIndex = vintTopLeftColumnIndex + 1
    
    OutputPrintAreaColumnWidthsAndRowHeghtsToSheet vobjColumnWidthsDTCol, vobjRowHeightsDTCol, vobjOutputSheet, intTableExpandAreaTopLeftRowIndex, intTableExpandAreaTopLeftColumnIndex
    
    
    AddAutoShapeGeneralMessage vobjOutputSheet, "You can adjust both column-widths and row-heights by changing sheet figures", XlShapeTextLogInteriorFillGradientYellow
End Sub


'''
'''
'''
Public Sub OutputPrintAreaColumnWidthsAndRowHeghtsToSheet(ByVal vobjColumnWidthsDTCol As Collection, _
        ByVal vobjRowHeightsDTCol As Collection, _
        ByVal vobjOutputSheet As Excel.Worksheet, _
        Optional ByVal vintTableExpandAreaTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTableExpandAreaTopLeftColumnIndex As Long = 1)

    Dim objDTFormatterOfColumnWidthsDTCol As DataTableSheetFormatter, objDTFormatterOfRowHeightsDTCol As DataTableSheetFormatter

    
    msubGetOutputDTFormattersForBothColumnWidthsAndRowHeghts objDTFormatterOfColumnWidthsDTCol, objDTFormatterOfRowHeightsDTCol, vintTableExpandAreaTopLeftRowIndex, vintTableExpandAreaTopLeftColumnIndex


    msubAddConvertedColumnsMiliMeterFomulaInDTCol vobjColumnWidthsDTCol, objDTFormatterOfColumnWidthsDTCol

    OutputColToSheetRestrictedByDataTableSheetFormatter vobjOutputSheet, vobjColumnWidthsDTCol, objDTFormatterOfColumnWidthsDTCol
    
    
    msubAddConvertedRowsMiliMeterFomulaInDTCol vobjRowHeightsDTCol, objDTFormatterOfRowHeightsDTCol
    
    OutputColToSheetRestrictedByDataTableSheetFormatter vobjOutputSheet, vobjRowHeightsDTCol, objDTFormatterOfRowHeightsDTCol
End Sub


'''
'''
'''
Private Sub msubAddConvertedColumnsMiliMeterFomulaInDTCol(ByRef robjDTCol As Collection, _
        ByRef rintDTFormatter As IDataTableSheetFormatter)

    Dim objNewDTCol As Collection, varRowCol As Variant, objRowCol As Collection, i As Long, j As Long, intRowCounter As Long
    Dim strRangeAddress As String, dblRacioOfWidthToColumnWidthOfPlottingPapterSheet As Double, objSourcePlottingPapterRange As Excel.Range
    Dim strWidthByCharUnitAddress As String, strRacioOfCharUnitAddress As String
    
    
    Set objNewDTCol = New Collection
    
    With rintDTFormatter
    
        i = .FieldTitlesRowIndex + 1
    
        j = .TopLeftColumnIndex + robjDTCol.Item(1).Count - 1
    End With
    
    intRowCounter = 1
            
    For Each varRowCol In robjDTCol
    
        Set objRowCol = varRowCol
        
        strWidthByCharUnitAddress = ConvertXlColumnIndexToLetter(j - 1) & CStr(i)
        
        strRacioOfCharUnitAddress = ConvertXlColumnIndexToLetter(j) & CStr(i)
        
        objRowCol.Add "=" & strWidthByCharUnitAddress & "*" & strRacioOfCharUnitAddress & "*(25.4/72)"
       
        
        strRangeAddress = ConvertXlColumnIndexToLetter(j + 1) & CStr(i)
        
        If intRowCounter = 1 Then
        
            objRowCol.Add "=" & strRangeAddress
        Else
            objRowCol.Add "=" & strRangeAddress & "+" & ConvertXlColumnIndexToLetter(j + 2) & CStr(i - 1)
        End If
    
        objNewDTCol.Add objRowCol
    
        i = i + 1
        
        intRowCounter = intRowCounter + 1
    Next

    Set robjDTCol = objNewDTCol
End Sub

'''
'''
'''
Private Sub msubAddConvertedRowsMiliMeterFomulaInDTCol(ByRef robjDTCol As Collection, _
        ByRef rintDTFormatter As IDataTableSheetFormatter)

    Dim objNewDTCol As Collection, varRowCol As Variant, objRowCol As Collection, i As Long, j As Long, intRowCounter As Long
    Dim strRangeAddress As String, dblRacioOfWidthToColumnWidthOfPlottingPapterSheet As Double, objSourcePlottingPapterRange As Excel.Range
    
    
    Set objNewDTCol = New Collection
    
    With rintDTFormatter
    
        i = .FieldTitlesRowIndex + 1
    
        j = .TopLeftColumnIndex + robjDTCol.Item(1).Count - 1
    End With
    
    intRowCounter = 1
            
    For Each varRowCol In robjDTCol
    
        Set objRowCol = varRowCol
        
        strRangeAddress = ConvertXlColumnIndexToLetter(j) & CStr(i)
        
        objRowCol.Add "=" & strRangeAddress & "*(25.4/72)"
        
        
        strRangeAddress = ConvertXlColumnIndexToLetter(j + 1) & CStr(i)
        
        If intRowCounter = 1 Then
        
            objRowCol.Add "=" & strRangeAddress
        Else
            objRowCol.Add "=" & strRangeAddress & "+" & ConvertXlColumnIndexToLetter(j + 2) & CStr(i - 1)
        End If
    
        objNewDTCol.Add objRowCol
    
        i = i + 1
        
        intRowCounter = intRowCounter + 1
    Next

    Set robjDTCol = objNewDTCol
End Sub


'''
''' A4 paper format (297 mm x 210 mm), letter-size format (8.5 inches x 11.0 inches)(279 mm x 216 mm)
'''
''' <Argument></Argument>
''' <Argument></Argument>
''' <Argument></Argument>
''' <Argument></Argument>
''' <Argument></Argument>
Public Sub LoadPrintAreaParameters(ByRef rstrPrintAreaAddress As String, _
    ByRef robjColumnWidthsDTCol As Collection, _
    ByRef robjRowHeightsDTCol As Collection, _
    ByVal vstrInputPlottingPaperBookPath As String, _
    ByVal vstrPlottingPaperSheetName As String, _
    Optional ByVal vblnAllowToFindPrintAreaWhenNoSettingExist As Boolean = True)

    
    Dim objSheet As Excel.Worksheet, objRegion As Excel.Range, strRange As String

    Set objSheet = GetWorksheetFromBookPathAndSheetName(vstrInputPlottingPaperBookPath, vstrPlottingPaperSheetName)

    With objSheet.PageSetup
    
        If .PrintArea <> "" Then
    
            strRange = .PrintArea
        Else
            If vblnAllowToFindPrintAreaWhenNoSettingExist Then
        
                .PrintArea = objSheet.UsedRange.Address
                
                strRange = .PrintArea
            End If
        End If
    End With
    
    If strRange <> "" Then

        rstrPrintAreaAddress = strRange

        Set objRegion = objSheet.Range(strRange)
    
        msubLoadColumnWidthsDTColAndRowHeightsDTColFromRegion robjColumnWidthsDTCol, robjRowHeightsDTCol, objRegion
    End If
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'//////////////////////////////////////////////
'**---------------------------------------------
'** Reflect parameters to the plotting-paper sheet
'**---------------------------------------------
'''
''' About column-widths
'''
Private Sub msubReflectLoadedColumnWidthsDTColToPlottingPaperSheet(ByVal vobjPlottingPaperSheet As Excel.Worksheet, ByVal vobjColumnWidthsDTCol As Collection)

    Dim varRowCol As Variant, objRowCol As Collection, dblColumnWidth As Double
    Dim strColumnLetter As String, objRange As Excel.Range
    
    With vobjPlottingPaperSheet
    
        For Each varRowCol In vobjColumnWidthsDTCol
        
            Set objRowCol = varRowCol
            
            strColumnLetter = objRowCol.Item(2)
            
            dblColumnWidth = objRowCol.Item(3)
        
            Set objRange = .Range(strColumnLetter & ":" & strColumnLetter)
            
            With objRange.EntireColumn
            
                If .ColumnWidth <> dblColumnWidth Then
                
                    .ColumnWidth = dblColumnWidth
                End If
            End With
        Next
    End With
End Sub

'''
''' About row-heights
'''
Private Sub msubReflectLoadedRowHeightsDTColToPlottingPaperSheet(ByVal vobjPlottingPaperSheet As Excel.Worksheet, ByVal vobjRowHeightsDTCol As Collection)

    Dim varRowCol As Variant, objRowCol As Collection, dblRowHeight As Double, objRange As Excel.Range
    Dim intRowIndex As Long

    With vobjPlottingPaperSheet
    
        For Each varRowCol In vobjRowHeightsDTCol
        
            Set objRowCol = varRowCol
            
            intRowIndex = objRowCol.Item(1)
            
            dblRowHeight = objRowCol.Item(2)
        
            Set objRange = .Range(CStr(intRowIndex) & ":" & CStr(intRowIndex))
            
            With objRange.EntireRow
            
                If .RowHeight <> dblRowHeight Then
                
                    .RowHeight = dblRowHeight
                End If
            End With
        Next
    End With
End Sub


'**---------------------------------------------
'** Load both column-widths and row-heights from a print-area Range
'**---------------------------------------------
'''
'''
'''
Private Sub msubLoadColumnWidthsDTColAndRowHeightsDTColFromRegion(ByRef robjColumnWidthsDTCol As Collection, ByRef robjRowHeightsDTCol As Collection, ByVal vobjRegion As Excel.Range)

    Dim objRange As Excel.Range, objRowCol As Collection, objSheet As Excel.Worksheet
    
    With vobjRegion
    
        ' About columns
        
        Set objSheet = .Parent
        
        Set robjColumnWidthsDTCol = New Collection
        
        For Each objRange In objSheet.Range(.Cells(1, 1), .Cells(1, .Columns.Count))
    
            With objRange
            
                Set objRowCol = New Collection
            
                objRowCol.Add .Column
                
                objRowCol.Add ConvertXlColumnIndexToLetter(.Column)
                
                objRowCol.Add .EntireColumn.ColumnWidth ' character size unit
                
                If .ColumnWidth <> 0 Then
                
                    objRowCol.Add .Width / .ColumnWidth ' racio of .Width is point unit, to .ColumnWidth is character size unit
                Else
                    objRowCol.Add 0#
                End If
                
                robjColumnWidthsDTCol.Add objRowCol
            End With
        Next
        
        ' About rows
        
        Set robjRowHeightsDTCol = New Collection
        
        For Each objRange In objSheet.Range(.Cells(1, 1), .Cells(.Rows.Count, 1))
        
            With objRange
            
                'Debug.Print .Address
            
                Set objRowCol = New Collection
                
                objRowCol.Add .Row
                
                objRowCol.Add .EntireRow.RowHeight  ' point unit
                
                robjRowHeightsDTCol.Add objRowCol
            End With
        Next
    End With
End Sub

'**---------------------------------------------
'** About DataTableSheetFormatter objects for extracted parameters sheet
'**---------------------------------------------
'''
'''
'''
Private Sub msubGetOutputDTFormattersForBothColumnWidthsAndRowHeghts(ByRef robjDTFormatterOfColumnWidthsDTCol As DataTableSheetFormatter, _
        ByRef robjDTFormatterOfRowHeightsDTCol As DataTableSheetFormatter, _
        Optional ByVal vintTableExpandAreaTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTableExpandAreaTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmFieldTitleInterior As FieldTitleInterior = FieldTitleInterior.ColumnNameInteriorOfExtractedTableFromSheet)

    
    Set robjDTFormatterOfColumnWidthsDTCol = New DataTableSheetFormatter
    
    With robjDTFormatterOfColumnWidthsDTCol
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("ColumnIndex,9,ColumnLetter,5,ColumnWidth,13,RacioOfCharUnit,10,ColumnWidth_mm,15,SumOfColumns_mm,15")
    
        .FieldTitleInteriorType = venmFieldTitleInterior
    
        .RecordBordersType = RecordCellsGrayAll
    
        .TopLeftRowIndex = vintTableExpandAreaTopLeftRowIndex
    
        .TopLeftColumnIndex = vintTableExpandAreaTopLeftColumnIndex + 5 ' 3
        
        
        Set .FieldTitleToHorizontalAlignmentDic = GetTextToExcelConstantsDicFromLineDelimitedChar("ColumnLetter,xlCenter")
        
        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
        
        With .ColumnsNumberFormatLocal
        
            .SetNumberFormatLocalAndApplyFieldTitles "0.00", GetColFromLineDelimitedChar("ColumnWidth")
            
            .SetNumberFormatLocalAndApplyFieldTitles "0.0000 ""[mm]""", GetColFromLineDelimitedChar("ColumnWidth_mm,SumOfColumns_mm")
        End With
        
        .SetSheetFormatBeforeTableOut
    End With

    
    Set robjDTFormatterOfRowHeightsDTCol = New DataTableSheetFormatter
    
    With robjDTFormatterOfRowHeightsDTCol
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("RowIndex,9,RowHeight,13,RowHeight_mm,15,SumOfHeight_mm,15")
    
        .FieldTitleInteriorType = venmFieldTitleInterior
        
        .RecordBordersType = RecordCellsGrayAll
        
        .TopLeftRowIndex = vintTableExpandAreaTopLeftRowIndex
    
        .TopLeftColumnIndex = vintTableExpandAreaTopLeftColumnIndex
        
        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
        
        With .ColumnsNumberFormatLocal
        
            .SetNumberFormatLocalAndApplyFieldTitles "0.00", GetColFromLineDelimitedChar("RowHeight")
            
            .SetNumberFormatLocalAndApplyFieldTitles "0.0000 ""[mm]""", GetColFromLineDelimitedChar("RowHeight_mm,SumOfHeight_mm")
        End With
        
        .SetSheetFormatBeforeTableOut
    End With
End Sub



