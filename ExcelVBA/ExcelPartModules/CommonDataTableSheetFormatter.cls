VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CommonDataTableSheetFormatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   common data-object class of both DataTableSheetFormatter class and TrDataTableSheetFormatter class (transposed-DataTableSheetFormatter)
'
'   This has no relation with Excel.DataTable class of Chart object.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Independent on either the ADO or the SQL
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 10/Feb/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements ICommonDataTableSheetFormatter


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mintTopLeftRowIndex As Long: Private mintTopLeftColumnIndex As Long

Private mblnAllowToShowFieldTitle As Boolean

Private menmFieldTitleInteriorType As FieldTitleInterior

Private menmRecordCellsBordersType As RecordCellsBorders

Private msngRecordsFontSize As Single


Private mblnAllowToConvertDataForEachFieldTitles As Boolean

Private mobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary   ' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))

Private mblnAllowToMergeCellsByContinuousSameValues As Boolean
Private mblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean
Private mobjFieldTitlesToMergeCells As Collection

Private mblnAllowToSetEnablingWrapText As Boolean
Private mobjFieldTitlesToSetEnablingWrapTextCells As Collection

Private mobjFieldTitleToHorizontalAlignmentDic As Scripting.Dictionary

Private mblnAllowToAutoFitRowsHeight As Boolean

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mintTopLeftRowIndex = 1: mintTopLeftColumnIndex = 1
    
    
    msngRecordsFontSize = 9.5
    
    mblnAllowToShowFieldTitle = True
    
    menmFieldTitleInteriorType = FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQL   ' Default appearance
    
    menmRecordCellsBordersType = RecordCellsBorders.RecordCellsBordersNone
    
    
    mblnAllowToConvertDataForEachFieldTitles = False
    
    mblnAllowToMergeCellsByContinuousSameValues = False
    
    mblnAllowToSetVerticalTopAlignmentTopOfMergeCells = False
    
    mblnAllowToSetEnablingWrapText = False
    
    Set mobjFieldTitlesToSetEnablingWrapTextCells = Nothing
    
    Set mobjFieldTitleToHorizontalAlignmentDic = Nothing
    
    mblnAllowToAutoFitRowsHeight = False
End Sub

'''
'''
'''
Private Sub Class_Terminate()

    
End Sub


'///////////////////////////////////////////////
'/// Implemented properties
'///////////////////////////////////////////////
'''
'''
'''
Private Property Get ICommonDataTableSheetFormatter_TopLeftRowIndex() As Long

    ICommonDataTableSheetFormatter_TopLeftRowIndex = mintTopLeftRowIndex
End Property
Private Property Let ICommonDataTableSheetFormatter_TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)

    mintTopLeftRowIndex = vintTopLeftRowIndex
End Property
Private Property Get ICommonDataTableSheetFormatter_TopLeftColumnIndex() As Long

    ICommonDataTableSheetFormatter_TopLeftColumnIndex = mintTopLeftColumnIndex
End Property
Private Property Let ICommonDataTableSheetFormatter_TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)

    mintTopLeftColumnIndex = vintTopLeftColumnIndex
End Property



Private Property Get ICommonDataTableSheetFormatter_AllowToShowFieldTitle() As Boolean

    ICommonDataTableSheetFormatter_AllowToShowFieldTitle = mblnAllowToShowFieldTitle
End Property
Private Property Let ICommonDataTableSheetFormatter_AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)

    mblnAllowToShowFieldTitle = vblnAllowToShowFieldTitle
End Property

Private Property Get ICommonDataTableSheetFormatter_FieldTitleInteriorType() As FieldTitleInterior

    ICommonDataTableSheetFormatter_FieldTitleInteriorType = menmFieldTitleInteriorType
End Property
Private Property Let ICommonDataTableSheetFormatter_FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)

    menmFieldTitleInteriorType = venmFieldTitleInteriorType
End Property

Private Property Get ICommonDataTableSheetFormatter_RecordBordersType() As RecordCellsBorders

    ICommonDataTableSheetFormatter_RecordBordersType = menmRecordCellsBordersType
End Property
Private Property Let ICommonDataTableSheetFormatter_RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)

    menmRecordCellsBordersType = venmRecordCellsBordersType
End Property

Private Property Get ICommonDataTableSheetFormatter_RecordsFontSize() As Single

    ICommonDataTableSheetFormatter_RecordsFontSize = msngRecordsFontSize
End Property
Private Property Let ICommonDataTableSheetFormatter_RecordsFontSize(ByVal vsngRecordsFontSize As Single)

    msngRecordsFontSize = vsngRecordsFontSize
End Property


Private Property Get ICommonDataTableSheetFormatter_AllowToConvertDataForEachFieldTitles() As Boolean

    ICommonDataTableSheetFormatter_AllowToConvertDataForEachFieldTitles = mblnAllowToConvertDataForEachFieldTitles
End Property
Private Property Let ICommonDataTableSheetFormatter_AllowToConvertDataForEachFieldTitles(ByVal vblnAllowToConvertDataForEachFieldTitles As Boolean)

    mblnAllowToConvertDataForEachFieldTitles = vblnAllowToConvertDataForEachFieldTitles
End Property
'''
''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
'''
Private Property Set ICommonDataTableSheetFormatter_FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)

    Set mobjFieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
End Property
Private Property Get ICommonDataTableSheetFormatter_FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary

    Set ICommonDataTableSheetFormatter_FieldTitlesToColumnDataConvertTypeDic = mobjFieldTitlesToColumnDataConvertTypeDic
End Property


'''
''' merge cells when the same values continue
'''
Private Property Get ICommonDataTableSheetFormatter_AllowToMergeCellsByContinuousSameValues() As Boolean

    ICommonDataTableSheetFormatter_AllowToMergeCellsByContinuousSameValues = mblnAllowToMergeCellsByContinuousSameValues
End Property
Private Property Let ICommonDataTableSheetFormatter_AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)

    mblnAllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
End Property


Private Property Get ICommonDataTableSheetFormatter_AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean

   ICommonDataTableSheetFormatter_AllowToSetVerticalTopAlignmentTopOfMergeCells = mblnAllowToSetVerticalTopAlignmentTopOfMergeCells
End Property
Private Property Let ICommonDataTableSheetFormatter_AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)

    mblnAllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
End Property


Private Property Get ICommonDataTableSheetFormatter_FieldTitlesToMergeCells() As Collection

    Set ICommonDataTableSheetFormatter_FieldTitlesToMergeCells = mobjFieldTitlesToMergeCells
End Property
Private Property Set ICommonDataTableSheetFormatter_FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)

    Set mobjFieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
End Property



'''
''' Enabling WrapText for each cells
'''
Private Property Get ICommonDataTableSheetFormatter_AllowToSetEnablingWrapText() As Boolean

    ICommonDataTableSheetFormatter_AllowToSetEnablingWrapText = mblnAllowToSetEnablingWrapText
End Property
Private Property Let ICommonDataTableSheetFormatter_AllowToSetEnablingWrapText(ByVal vblnAllowToSetEnablingWrapText As Boolean)

    mblnAllowToSetEnablingWrapText = vblnAllowToSetEnablingWrapText
End Property


Private Property Get ICommonDataTableSheetFormatter_FieldTitlesToSetEnablingWrapTextCells() As Collection

    Set ICommonDataTableSheetFormatter_FieldTitlesToSetEnablingWrapTextCells = mobjFieldTitlesToSetEnablingWrapTextCells
End Property
Private Property Set ICommonDataTableSheetFormatter_FieldTitlesToSetEnablingWrapTextCells(ByVal vobjFieldTitlesToSetEnablingWrapTextCells As Collection)

    Set mobjFieldTitlesToSetEnablingWrapTextCells = vobjFieldTitlesToSetEnablingWrapTextCells
End Property

'''
''' Dictionary(Of String[Field-title], Excel.Constants)
'''
Private Property Get ICommonDataTableSheetFormatter_FieldTitleToHorizontalAlignmentDic() As Scripting.Dictionary

    Set ICommonDataTableSheetFormatter_FieldTitleToHorizontalAlignmentDic = mobjFieldTitleToHorizontalAlignmentDic
End Property
Private Property Set ICommonDataTableSheetFormatter_FieldTitleToHorizontalAlignmentDic(ByVal vobjDic As Scripting.Dictionary)

    Set mobjFieldTitleToHorizontalAlignmentDic = vobjDic
End Property

'''
''' If mblnAllowToAutoFitRowsHeight is true, Excel.Worksheet.Rows.AutoFit method is to be executed
'''
Private Property Get ICommonDataTableSheetFormatter_AllowToAutoFitRowsHeight() As Boolean

    ICommonDataTableSheetFormatter_AllowToAutoFitRowsHeight = mblnAllowToAutoFitRowsHeight
End Property
Private Property Let ICommonDataTableSheetFormatter_AllowToAutoFitRowsHeight(ByVal vblnAllowToAutoFitRowsHeight As Boolean)

    mblnAllowToAutoFitRowsHeight = vblnAllowToAutoFitRowsHeight
End Property

