Attribute VB_Name = "DataVectorSheetIn"
'
'   get a one-dimensioal vector from Excel Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  8/Feb/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////

'''
'''
'''
Public Sub GetRowVectorValuesOnSheet(ByRef rvarVectorValues As Variant, _
        ByVal vobjTopLeftRange As Excel.Range, _
        Optional ByVal vblnAllowToUseSheetUsedRange As Boolean = True)

    
    GetVectorValuesOnSheet rvarVectorValues, vobjTopLeftRange, True, vblnAllowToUseSheetUsedRange
End Sub

'''
'''
'''
Public Sub GetColumnVectorValuesOnSheet(ByRef rvarVectorValues As Variant, _
        ByVal vobjTopLeftRange As Excel.Range, _
        Optional ByVal vblnAllowToUseSheetUsedRange As Boolean = True)

    
    GetVectorValuesOnSheet rvarVectorValues, vobjTopLeftRange, False, vblnAllowToUseSheetUsedRange
End Sub

'''
'''
'''
Public Sub GetVectorValuesOnSheet(ByRef rvarVectorValues As Variant, _
        ByVal vobjTopLeftRange As Excel.Range, _
        Optional ByVal vblnIsRowVectorDirection As Boolean = True, _
        Optional ByVal vblnAllowToUseSheetUsedRange As Boolean = True)

    
    Dim objVectorRange As Excel.Range
    
    If vblnAllowToUseSheetUsedRange Then
    
        Set objVectorRange = GetVectorRangeFromUsedRange(vobjTopLeftRange, vblnIsRowVectorDirection)
    Else
        Set objVectorRange = GetVectorRangeFromCurrentRegion(vobjTopLeftRange, vblnIsRowVectorDirection)
    End If

    If vblnIsRowVectorDirection Then
    
        If objVectorRange.Columns.Count > 1 Then
        
            ReDim rvarVectorValues(1 To 1, 1 To objVectorRange.Columns.Count)
        End If
    Else
        If objVectorRange.Rows.Count > 1 Then
        
            ReDim rvarVectorValues(1 To objVectorRange.Rows.Count, 1 To 1)
        End If
    End If
    
    rvarVectorValues = objVectorRange.Value
End Sub


'''
'''
'''
Public Function GetRowVectorRangeFromUsedRange(ByVal vobjTopLeftRange As Excel.Range) As Excel.Range

    Set GetRowVectorRangeFromUsedRange = GetVectorRangeFromUsedRange(vobjTopLeftRange, True)
End Function

'''
'''
'''
Public Function GetColumnVectorRangeFromUsedRange(ByVal vobjTopLeftRange As Excel.Range) As Excel.Range

    Set GetColumnVectorRangeFromUsedRange = GetVectorRangeFromUsedRange(vobjTopLeftRange, False)
End Function

'''
'''
'''
Public Function GetVectorRangeFromUsedRange(ByVal vobjTopLeftRange As Excel.Range, Optional ByVal vblnIsRowVectorDirection As Boolean = True) As Excel.Range

    Dim objVectorRange As Excel.Range, objSheet As Excel.Worksheet, intEndIndex As Long, intStartIndex As Long
    
    
    Set objSheet = vobjTopLeftRange.Worksheet
    
    With objSheet
    
        If vblnIsRowVectorDirection Then
        
            intStartIndex = vobjTopLeftRange.Column
            
            With .UsedRange
            
                intEndIndex = .Column + .Columns.Count - 1
            End With
            
            Set objVectorRange = .Range(.Cells(vobjTopLeftRange.Row, intStartIndex), .Cells(vobjTopLeftRange.Row, intEndIndex))
        Else
            intStartIndex = vobjTopLeftRange.Row
        
            With .UsedRange
        
                intEndIndex = .Row + .Rows.Count - 1
            End With
        
            Set objVectorRange = .Range(.Cells(intStartIndex, vobjTopLeftRange.Column), .Cells(intEndIndex, vobjTopLeftRange.Column))
        End If
    End With
    
    Set GetVectorRangeFromUsedRange = objVectorRange
End Function



'''
'''
'''
Public Function GetVectorRangeFromCurrentRegion(ByVal vobjTopLeftRange As Excel.Range, Optional ByVal vblnIsRowVectorDirection As Boolean = True) As Excel.Range

    Dim objVectorRange As Excel.Range, objSheet As Excel.Worksheet, intEndIndex As Long, intStartIndex As Long
    
    
    Set objSheet = vobjTopLeftRange.Worksheet
    
    With objSheet
    
        If vblnIsRowVectorDirection Then
        
            intStartIndex = vobjTopLeftRange.Column
            
            With vobjTopLeftRange.CurrentRegion
            
                intEndIndex = .Column + .Columns.Count - 1
            End With
            
            Set objVectorRange = .Range(.Cells(vobjTopLeftRange.Row, intStartIndex), .Cells(vobjTopLeftRange.Row, intEndIndex))
        Else
            intStartIndex = vobjTopLeftRange.Row
        
            With vobjTopLeftRange.CurrentRegion
        
                intEndIndex = .Row + .Rows.Count - 1
            End With
        
            Set objVectorRange = .Range(.Cells(intStartIndex, vobjTopLeftRange.Column), .Cells(intEndIndex, vobjTopLeftRange.Column))
        End If
    End With
    
    Set GetVectorRangeFromCurrentRegion = objVectorRange
End Function

