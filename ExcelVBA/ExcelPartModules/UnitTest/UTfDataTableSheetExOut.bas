Attribute VB_Name = "UTfDataTableSheetExOut"
'
'   Sanity tests for transposed output data-table into Excel Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on UTfDecorationSetterToXlSheet.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 10/Feb/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** About transpose output Data-table collection
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToTransposeOutputColToSheet()

    Dim objBook As Excel.Workbook, strBookPath As String, objDTCol As VBA.Collection, objFieldTitlesCol As VBA.Collection
    Dim objSheet As Excel.Worksheet

    strBookPath = GetUnitTestingSheetRandomizedDataBookDir() & "\TestingTransposedSheetRandomizedValuesTests.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet.Name = "Sample000"

    SetupPadSampleRandomSomeValueWithNullColumnsOnRCTableAndFieldTitles objDTCol, objFieldTitlesCol, 5, 17, 4, "", False

    TransposeOutputColToSheet objSheet, objDTCol, objFieldTitlesCol, GetTransposeDataTableSheetFormatterOfSampleRandomSomeValueTable(3, 4)

    DeleteDummySheetWhenItExists objBook
End Sub

'''
'''
'''
Private Sub msubSanityTestToTrDataTableSheetFormatter()

    Dim objTrDataTableSheetFormatter As TrDataTableSheetFormatter
    
    Set objTrDataTableSheetFormatter = New TrDataTableSheetFormatter

    objTrDataTableSheetFormatter.TransposeDataTableSheetFormattingInterface.RecordBordersType = RecordCellsBlacksAndGrayInsideHorizontal
End Sub


'**---------------------------------------------
'** About one-nested Dictionary
'**---------------------------------------------

'''
'''
'''
Private Sub msubSanityTestToOutputOneNestedDicToSheet()

    Dim enmOneNestedDicType As OneNestedDicType
    Dim objNestedDic As Scripting.Dictionary, objFieldTitlesCol As Collection
    
    Dim strBookPath As String, objSheet As Excel.Worksheet
    
    Dim blnIsInterpretedOfSingleDimensionColAsRowData As Boolean
    
    
    strBookPath = mfstrGetTestingOutputBookTempDir() & "\TestOutputForNestedDicToDicWithOneDimentionalType01.xlsx"
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "OutputNestedDic")

    enmOneNestedDicType = NestedDicToDicWithOneDimentionalType

    SetupPadSimpleIntegerForOneNestedDicAndFieldTitles objNestedDic, objFieldTitlesCol, blnIsInterpretedOfSingleDimensionColAsRowData, 2, 4, 2, enmOneNestedDicType

    OutputOneNestedDicToSheet objSheet, objNestedDic, objFieldTitlesCol, mfobjGetDataTableSheetFormatter(), blnIsInterpretedOfSingleDimensionColAsRowData

End Sub


'''
'''
'''
Private Function mfstrGetTestingOutputBookTempDir() As String

    Dim strTmpDir As String
    
    strTmpDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\TestingOutputNestedDicOnCells"
    
    If Not DirectoryExistsByVbaDir(strTmpDir) Then
    
        ForceToCreateDirectoryByVbaDir strTmpDir
    End If

    mfstrGetTestingOutputBookTempDir = strTmpDir
End Function



'''
'''
'''
Private Function mfobjGetDataTableSheetFormatter() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter

        .RecordBordersType = RecordCellsGrayAll
        
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable

        .AllowToMergeCellsByContinuousSameValues = True
        
        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("Column1")
    End With
    
    Set mfobjGetDataTableSheetFormatter = objDTSheetFormatter
End Function


'''
'''
'''
Private Sub msubSanityTestToSetupPadSimpleIntegerForOneNestedDicAndFieldTitles()

    Dim objNestedDic As Scripting.Dictionary, objFieldTitlesCol As Collection
    
    Dim blnIsInterpretedOfSingleDimensionColAsRowData As Boolean
    
    SetupPadSimpleIntegerForOneNestedDicAndFieldTitles objNestedDic, objFieldTitlesCol, blnIsInterpretedOfSingleDimensionColAsRowData, 2, 4, 2, NestedDicToDicWithOneDimentionalType

    DebugTypeNameOfDic objNestedDic

    DebugDic objNestedDic, blnIsInterpretedOfSingleDimensionColAsRowData

'
    SetupPadSimpleIntegerForOneNestedDicAndFieldTitles objNestedDic, objFieldTitlesCol, blnIsInterpretedOfSingleDimensionColAsRowData, 2, 3, 2, NestedDicToDicWithOneDimentionalType

    DebugTypeNameOfDic objNestedDic

    DebugDic objNestedDic, blnIsInterpretedOfSingleDimensionColAsRowData



    SetupPadSimpleIntegerForOneNestedDicAndFieldTitles objNestedDic, objFieldTitlesCol, blnIsInterpretedOfSingleDimensionColAsRowData, 2, 3, 2, NestedDicToDTColType

    DebugTypeNameOfDic objNestedDic

    DebugDic objNestedDic, blnIsInterpretedOfSingleDimensionColAsRowData


    SetupPadSimpleIntegerForOneNestedDicAndFieldTitles objNestedDic, objFieldTitlesCol, blnIsInterpretedOfSingleDimensionColAsRowData, 2, 4, 1, NestedDicToDTColType

    DebugTypeNameOfDic objNestedDic

    DebugDic objNestedDic, blnIsInterpretedOfSingleDimensionColAsRowData
    
    
End Sub


