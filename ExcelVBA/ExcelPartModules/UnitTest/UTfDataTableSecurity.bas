Attribute VB_Name = "UTfDataTableSecurity"
'
'   Sanity test to lock and unlock Excel book by password
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on UTfDecorationSetterToXlSheet.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 29/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrSheetName As String = "TestSheet"


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** About Excel.Workbook security setting
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToGetPluralBooksSheetSecurityInfoDic()

    DebugDic GetPluralBooksSheetSecurityInfoDic(ThisWorkbook.Application)
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetBookSheetSecurityInfoDic()

    DebugDic GetBookSheetSecurityInfoDic(ThisWorkbook)
End Sub


'**---------------------------------------------
'** Open test to password locked sample Excel book
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToOpenPasswordLockedBook()

    Dim strBookPath As String, objBook As Excel.Workbook

    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"

    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"

    ' This causes no error and it will not be opened
    Set objBook = GetWorkbook(strBookPath, False, vstrPassword:="")
    
    If objBook Is Nothing Then
    
        Set objBook = GetWorkbook(strBookPath, False, vstrPassword:="1234")
    End If
End Sub

'''
''' test to ChangeLockPasswordOfBook
'''
Private Sub msubSanityTestToChangeLockPasswordOfBook()

    Dim strBookPath As String, objBook As Excel.Workbook

    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBookToChange.xlsx"

    Set objBook = mfobjGetTestBookWithPasswordLock(strBookPath, mstrSheetName, "1234")
        
    objBook.Close: Set objBook = Nothing

    ' change the password
    
    ChangeLockPasswordOfBook strBookPath, "1234", "5678"

    Set objBook = GetWorkbook(strBookPath, False, vstrPassword:="5678")
    
    ExploreParentFolderWithSelectingWhenOpenedThatIsNothing strBookPath
End Sub


'**---------------------------------------------
'** Get password locked sample Excel book
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToGetTestBookWithPasswordLock()

    Dim strBookPath As String, objBook As Excel.Workbook
    
    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"

    Set objBook = mfobjGetTestBookWithPasswordLock(strBookPath, mstrSheetName, "1234")

    objBook.Close
    
    ExploreParentFolderWithSelectingWhenOpenedThatIsNothing strBookPath    ' This can be opened by inputting the password
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetTestStructurePasswordLockedBook()

    Dim strBookPath As String, objBook As Excel.Workbook
    
    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SampleBookWithStructureLocked.xlsx"

    Set objBook = mfobjGetTestStructurePasswordLockedBook(strBookPath, mstrSheetName, "1234")

    objBook.Close
    
    ExploreParentFolderWithSelectingWhenOpenedThatIsNothing strBookPath    ' this can be opened without the opening password
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub PrepareTestBookWithLockingPasswordAndCreating(ByVal vstrBookPath As String, ByVal vstrSheetName As String, ByVal vstrBookLockPassword As String)

    Dim objBook As Excel.Workbook
    
    If Not FileExistsByVbaDir(vstrBookPath) Then
    
        Set objBook = mfobjGetTestBookWithPasswordLock(vstrBookPath, vstrSheetName, vstrBookLockPassword)
        
        objBook.Close
    End If
End Sub


'''
'''
'''
Public Function GetBookSecurityTemporaryGeneratingDirectoryPath() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\ExcelBookSecurities"

    ForceToCreateDirectory strDir
    
    GetBookSecurityTemporaryGeneratingDirectoryPath = strDir
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetTestBookWithPasswordLock(ByVal vstrBookPath As String, ByVal vstrSheetName As String, ByVal vstrBookLockPassword As String) As Excel.Workbook

    Dim objBook As Excel.Workbook

    DeleteExcelBookIfItInThisProcessOrOtherProcessesExistsWithClosing vstrBookPath
    
    Set objBook = mfobjGetSampleSimpleIntegerTableBook(vstrBookPath, vstrSheetName)
    
    If vstrBookLockPassword <> "" Then
    
        AddAutoShapeGeneralMessage objBook.Worksheets(vstrSheetName), "Auto-generated password locked Book" & vbNewLine & "Generated date time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss") & vbNewLine & "Password locked: " & String(Len(vstrBookLockPassword), "*")
    Else
        AddAutoShapeGeneralMessage objBook.Worksheets(vstrSheetName), "Auto-generated Book" & vbNewLine & "Generated date time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
    End If
    
    ForceToSaveAsBookWithoutDisplayAlert objBook, vstrBookPath, xlOpenXMLWorkbook, False, vstrBookLockPassword
    
    Set mfobjGetTestBookWithPasswordLock = objBook
End Function

'''
'''
'''
Private Function mfobjGetTestStructurePasswordLockedBook(ByVal vstrBookPath As String, ByVal vstrSheetName As String, ByVal vstrBookStructurePassword As String)

    Dim objBook As Excel.Workbook

    DeleteExcelBookIfItInThisProcessOrOtherProcessesExistsWithClosing vstrBookPath
    
    Set objBook = mfobjGetSampleSimpleIntegerTableBook(vstrBookPath, vstrSheetName)
    
    SetBookStructureProtection objBook, vstrBookStructurePassword
    
    If vstrBookStructurePassword <> "" Then
    
        AddAutoShapeGeneralMessage objBook.Worksheets(vstrSheetName), "Auto-generated Book with password protected structure" & vbNewLine & "Generated date time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss") & vbNewLine & "Password structure protected: " & String(Len(vstrBookStructurePassword), "*")
    Else
        AddAutoShapeGeneralMessage objBook.Worksheets(vstrSheetName), "Auto-generated Book" & vbNewLine & "Generated date time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
    End If
    
    ForceToSaveAsBookWithoutDisplayAlert objBook, vstrBookPath
    
    Set mfobjGetTestStructurePasswordLockedBook = objBook
End Function


'''
'''
'''
Private Function mfobjGetSampleSimpleIntegerTableBook(ByVal vstrBookPath As String, ByVal vstrSheetName As String) As Excel.Workbook

    Dim objDTCol As Collection, objFieldTitlesCol As Collection, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    
    SetupPadSimpleIntegerForRCTableColAndFieldTitles objDTCol, objFieldTitlesCol, 8, 5

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrBookPath, vstrSheetName)

    Set objDTSheetFormatter = New DataTableSheetFormatter

    objDTSheetFormatter.RecordBordersType = RecordCellsGrayAll

    OutputColToSheet objSheet, objDTCol, objFieldTitlesCol, objDTSheetFormatter

    Set objBook = objSheet.Parent

    Set mfobjGetSampleSimpleIntegerTableBook = objBook
End Function








