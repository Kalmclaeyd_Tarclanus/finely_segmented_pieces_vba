Attribute VB_Name = "UTfDataTableSheetIn"
'
'   sanity tests for getting data-table from Excel.Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on UTfDataTableSheetOut.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 15/Aug/2024    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** About testing GetDataTableCollectionForOnlySpecifiedFieldsFromWorksheet
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToGetDataTableCollectionForOnlySpecifiedFieldsFromWorksheet()

    Dim objSheet As Excel.Worksheet, objDTCol As Collection, objFieldTitlesCol As Collection
    Dim objDT02Col As Collection
    
    Set objSheet = mfobjGetSheetAfterCreatingTestTable(2, 2)
    
    GetDTTableAndFieldTitlesFromSheet objDTCol, objFieldTitlesCol, objSheet

    Set objDT02Col = GetDataTableCollectionForOnlySpecifiedFieldsFromWorksheet(objSheet, objFieldTitlesCol, 1, 1, True)

    DebugCol objDT02Col
End Sub

'**---------------------------------------------
'** About testing GetDTTableAndFieldTitlesFromSheet
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToGetDTTableAndFieldTitlesFromSheet_2_2()

    Dim objSheet As Excel.Worksheet, objDTCol As Collection, objFieldTitlesCol As Collection
    
    
    Set objSheet = mfobjGetSheetAfterCreatingTestTable(2, 2)
    
    GetDTTableAndFieldTitlesFromSheet objDTCol, objFieldTitlesCol, objSheet
    
    Debug.Print GetLineTextFromCol(objFieldTitlesCol)
    
    DebugCol objDTCol
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetDTTableAndFieldTitlesFromSheet_1_1()

    Dim objSheet As Excel.Worksheet, objDTCol As Collection, objFieldTitlesCol As Collection
    
    
    Set objSheet = mfobjGetSheetAfterCreatingTestTable(1, 1)
    
    GetDTTableAndFieldTitlesFromSheet objDTCol, objFieldTitlesCol, objSheet
    
    Debug.Print GetLineTextFromCol(objFieldTitlesCol)
    
    DebugCol objDTCol
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetDTTableAndFieldTitlesFromSheet_1_1_3_4()

    Dim objSheet As Excel.Worksheet, objDTCol As Collection, objFieldTitlesCol As Collection
    
    
    Set objSheet = mfobjGetSheetAfterCreatingTestTable(1, 1, 3, 4)
    
    GetDTTableAndFieldTitlesFromSheet objDTCol, objFieldTitlesCol, objSheet, 3, 4
    
    Debug.Print GetLineTextFromCol(objFieldTitlesCol)
    
    DebugCol objDTCol
End Sub

'**---------------------------------------------
'** About testing GetRCDictionaryFromSheet
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToGetRCDictionaryFromSheet01AboutTwoColumns()

    Dim objSheet As Excel.Worksheet, objDTDic As Scripting.Dictionary, objFieldTitlesCol As Collection
    
    
    Set objSheet = mfobjGetSheetAfterCreatingTestTable(5, 2)
    
    GetRCDictionaryFromSheet objDTDic, objFieldTitlesCol, objSheet
    
    Debug.Print GetLineTextFromCol(objFieldTitlesCol)
    
    DebugDic objDTDic
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetRCDictionaryFromSheet01AboutTwoColumnsAndOtherTopLeft()

    Dim objSheet As Excel.Worksheet, objDTDic As Scripting.Dictionary, objFieldTitlesCol As Collection
    
    
    Set objSheet = mfobjGetSheetAfterCreatingTestTable(5, 2, 3, 8)
    
    GetRCDictionaryFromSheet objDTDic, objFieldTitlesCol, objSheet, 3, 8
    
    Debug.Print GetLineTextFromCol(objFieldTitlesCol)
    
    DebugDic objDTDic
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetRCDictionaryFromSheet02AboutMoreThanTwoColumns()

    Dim objSheet As Excel.Worksheet, objDTDic As Scripting.Dictionary, objFieldTitlesCol As Collection
    
    
    Set objSheet = mfobjGetSheetAfterCreatingTestTable(5, 3)
    
    GetRCDictionaryFromSheet objDTDic, objFieldTitlesCol, objSheet
    
    Debug.Print GetLineTextFromCol(objFieldTitlesCol)
    
    DebugDic objDTDic
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetRCDictionaryFromSheet02AboutMoreThanTwoColumnsAndOtherTopLeft()

    Dim objSheet As Excel.Worksheet, objDTDic As Scripting.Dictionary, objFieldTitlesCol As Collection
    
    
    Set objSheet = mfobjGetSheetAfterCreatingTestTable(5, 3, 3, 8)
    
    GetRCDictionaryFromSheet objDTDic, objFieldTitlesCol, objSheet, 3, 8
    
    Debug.Print GetLineTextFromCol(objFieldTitlesCol)
    
    DebugDic objDTDic
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetSheetAfterCreatingTestTable(ByVal vintRowsCount As Long, _
        ByVal vintColumnsCount As Long, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1) As Excel.Worksheet

    Dim strBookPath As String, strBookName As String
    
    Dim strFileNameMiddlePart As String
    
    
    strFileNameMiddlePart = "Row" & Format(vintRowsCount, "00") & "_Column" & Format(vintColumnsCount, "00")
    
    If Not (vintTopLeftRowIndex = 1 And vintTopLeftColumnIndex = 1) Then
    
        strFileNameMiddlePart = strFileNameMiddlePart & "_TopLeftRowIdx" & Format(vintTopLeftRowIndex, "00") & "_TopLeftColumnIdx" & Format(vintTopLeftColumnIndex, "00")
    End If
    
    strBookName = "Reading_" & strFileNameMiddlePart & "_Book.xlsx"
    
    strBookPath = mfstrGetTestDirOfDataTableSheetIn() & "\" & strBookName
    
    Set mfobjGetSheetAfterCreatingTestTable = GetSamplePadDataSheetWithCreatingTestingWorkbook(strBookPath, _
            "TestReading", _
            vintRowsCount, _
            vintColumnsCount, _
            vintTopLeftRowIndex, _
            vintTopLeftColumnIndex)
End Function


'''
'''
'''
Private Function mfstrGetTestDirOfDataTableSheetIn() As String

    Dim strDir As String: strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\TestsForDataTableSheetIn"

    ForceToCreateDirectoryByVbaDir strDir

    mfstrGetTestDirOfDataTableSheetIn = strDir
End Function





