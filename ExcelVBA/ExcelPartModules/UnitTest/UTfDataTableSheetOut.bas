Attribute VB_Name = "UTfDataTableSheetOut"
'
'   Testing for DataTableSheetFormatter
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on UTfDecorationSetterToXlSheet.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  4/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToUTfDataTableSheetOut()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "DataTableSheetFormatter,UTfDecorationSetterToXlSheet,UTfDecorateXlShapeFontInterior"
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' create new test Worksheet
'''
Public Function GetSamplePadDataSheetWithCreatingTestingWorkbook(ByRef rstrOutputBookPath As String, _
        ByVal vstrSheetName As String, _
        ByVal vintTableRowsCount As Long, _
        ByVal vintTableColumnsCount As Long, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vblnAllowToShowFieldTitle As Boolean = True, _
        Optional ByVal vstrAdditionalInfoTitleOfAutoShape As String = "A condition OutputColToSheet sub-procedure test") As Excel.Worksheet
        

    Dim objFieldTitlesCol As Collection, objDataTableSheetFormatter As DataTableSheetFormatter, objCol As Collection
    
    Dim objSheet As Excel.Worksheet, objBook As Excel.Workbook
    
    Dim varRowCol As Variant, objRowCol As Collection
    
    Dim strSheetName As String
    
    
    Dim objDataTableSheetRangeAddresses As DataTableSheetRangeAddresses
    Dim objLogDic As Scripting.Dictionary
    
    
    Const dblFixedColumnWidth As Double = 11
    
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(rstrOutputBookPath)

    Set objCol = Nothing: Set objFieldTitlesCol = Nothing



    SetupPadSimpleIntegerForRCTableColAndFieldTitles objCol, objFieldTitlesCol, vintTableRowsCount, vintTableColumnsCount

    'Debug.Print objCol.Count

    Set objDataTableSheetFormatter = GetDataTableSheetFormatterFromFieldTitlesBySameColumnWidth(objFieldTitlesCol, _
            dblFixedColumnWidth, _
            ColumnNameInteriorOfGatheredColTable, _
            RecordCellsGrayAll)


    With objDataTableSheetFormatter
    
        .AllowToShowFieldTitle = vblnAllowToShowFieldTitle
        
        .TopLeftColumnIndex = vintTopLeftColumnIndex
        
        .TopLeftRowIndex = vintTopLeftRowIndex
        
        If Not vblnAllowToShowFieldTitle Then
        
            ' FieldTitleOrderToWidth
        
            Set .FieldTitleOrderToColumnWidthDic = GetFieldTitleOrderToColumnWidthDicForFixedWidthFromFieldTitleCol(objFieldTitlesCol, dblFixedColumnWidth)
        End If
    End With
    
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    If vstrSheetName = "" Then

        strSheetName = "Sample" & Format(objBook.Worksheets.Count - 1, "000")
    Else
        strSheetName = vstrSheetName
    End If
    
    objSheet.Name = strSheetName
    
    OutputColToSheet objSheet, objCol, objFieldTitlesCol, objDataTableSheetFormatter
    
    Set objDataTableSheetRangeAddresses = GetDataTableSheetRangeAddressesFromDTColAndDTFormatter(objSheet, objCol, objFieldTitlesCol, objDataTableSheetFormatter)
    
    Set objLogDic = mfobjGetLogDicForExpandTableParamsAndDataTableSheetRangeAddresses(vintTableRowsCount, _
            vintTableColumnsCount, _
            vintTopLeftRowIndex, _
            vintTopLeftColumnIndex, _
            vblnAllowToShowFieldTitle, _
            objDataTableSheetRangeAddresses)
    
    OutputDicToAutoShapeLogTextOnSheet objSheet, objLogDic, vstrAdditionalInfoTitleOfAutoShape, XlShapeTextLogInteriorFillDefault
    
    DeleteDummySheetWhenItExists objBook


    Set GetSamplePadDataSheetWithCreatingTestingWorkbook = objSheet
End Function




'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Test for OutputColToSheet of general data-table
'**---------------------------------------------
'''
''' test of both OutputColToSheet sub-procedure and DataTableSheetFormatter object
'''
Private Sub msubComprehensiveTestToOutputColToSheet()

    Dim strBookPath As String, objSheet As Excel.Worksheet, objBook As Excel.Workbook
    Dim objCol As Collection, objFieldTitlesCol As Collection, objDataTableSheetFormatter As DataTableSheetFormatter
    Dim varAllowToShowFieldTitle As Variant, varRowCol As Variant, objRowCol As Collection
    Dim intRowMax As Long, intColumnMax As Long, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long, strSheetName As String
    
    Dim objDataTableSheetRangeAddresses As DataTableSheetRangeAddresses
    Dim objLogDic As Scripting.Dictionary
    
    
    Const dblFixedColumnWidth As Double = 11
    
    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\DataTableSomethingOutputSanityTest.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    
    For Each varAllowToShowFieldTitle In GetBooleanColFromLineDelimitedChar("True,False")
    
        For Each varRowCol In GetDataTableSheetCreateTestConditionPatternsDTCol(2, 3, 3, 4)
    
            Set objRowCol = varRowCol
    
            GetDataTableRegionParamsOfCreateTableOnSheet intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex, objRowCol
    
            Set objCol = Nothing: Set objFieldTitlesCol = Nothing
    
            SetupPadSimpleIntegerForRCTableColAndFieldTitles objCol, objFieldTitlesCol, intRowMax, intColumnMax
    
            Set objDataTableSheetFormatter = GetDataTableSheetFormatterFromFieldTitlesBySameColumnWidth(objFieldTitlesCol, _
                    dblFixedColumnWidth, _
                    ColumnNameInteriorOfGatheredColTable, _
                    RecordCellsGrayAll)
    
    
            With objDataTableSheetFormatter
            
                .AllowToShowFieldTitle = varAllowToShowFieldTitle
                
                .TopLeftColumnIndex = intTopLeftColumnIndex
                
                .TopLeftRowIndex = intTopLeftRowIndex
                
                If Not varAllowToShowFieldTitle Then
                
                    ' FieldTitleOrderToWidth
                
                    Set .FieldTitleOrderToColumnWidthDic = GetFieldTitleOrderToColumnWidthDicForFixedWidthFromFieldTitleCol(objFieldTitlesCol, dblFixedColumnWidth)
                End If
            End With
            
            
            Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
            strSheetName = "Sample" & Format(objBook.Worksheets.Count - 1, "000")
            
            objSheet.Name = strSheetName
            
            OutputColToSheet objSheet, objCol, objFieldTitlesCol, objDataTableSheetFormatter
            
            Set objDataTableSheetRangeAddresses = GetDataTableSheetRangeAddressesFromDTColAndDTFormatter(objSheet, objCol, objFieldTitlesCol, objDataTableSheetFormatter)
            
            Set objLogDic = mfobjGetLogDicForExpandTableParamsAndDataTableSheetRangeAddresses(intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex, varAllowToShowFieldTitle, objDataTableSheetRangeAddresses)
            
            OutputDicToAutoShapeLogTextOnSheet objSheet, objLogDic, "Comprehensive OutputColToSheet sub-procedure test", XlShapeTextLogInteriorFillDefault
        Next
    Next
    
    DeleteDummySheetWhenItExists objBook
End Sub


'''
''' getting DataTableSheetRangeAddresses test of a condition
'''
Private Sub msubSanityTestToOneSampleOfOutputColToSheet()


    GetSamplePadDataSheetWithCreatingTestingWorkbook GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\OneCaseOfDataTableSomethingOutputSanityTest.xlsx", _
            "", _
            2, _
            3, _
            3, _
            4, _
            True

End Sub



'**---------------------------------------------
'** Test for dictionary sheet expanding
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToWriteAndReadAboutExpandingDictionary()

    Dim strBookPath As String, objBook As Excel.Workbook, objDic As Scripting.Dictionary, objReadDic As Scripting.Dictionary
    Dim objSheet01 As Excel.Worksheet, objSheet02 As Excel.Worksheet
    Dim intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long


    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\DictionarySimpleExpandingSanityTest.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)

    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add "KeyAB01", 0: .Add "KeyCD02", 1: .Add "KeyEF03", "TestValue": .Add "KeyGH04", Now()
    End With
    
    intTopLeftRowIndex = 2

    intTopLeftColumnIndex = 3
    
    
    Set objSheet01 = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet01.Name = "KeyValue_rows"
    
    OutputDicWithoutHeaderSimplyToCellsOnSheet objSheet01, objDic, intTopLeftRowIndex, intTopLeftColumnIndex, DicExpandingKyesColumnsValuesColumnsAsTable
    
    Set objReadDic = GetDictionaryFromExpandedKeyValuesWithoutHeaderSimply(objSheet01, intTopLeftRowIndex, intTopLeftColumnIndex, DicExpandingKyesColumnsValuesColumnsAsTable)
    
    DebugDic objReadDic
    
    
    Set objSheet02 = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet02.Name = "KeyValue_columns"
    
    OutputDicWithoutHeaderSimplyToCellsOnSheet objSheet02, objDic, intTopLeftRowIndex, intTopLeftColumnIndex, DicExpandingKeysRowsValuesRows
    
    Set objReadDic = GetDictionaryFromExpandedKeyValuesWithoutHeaderSimply(objSheet02, intTopLeftRowIndex, intTopLeftColumnIndex, DicExpandingKeysRowsValuesRows)
    
    DebugDic objReadDic
    
    
    DeleteDummySheetWhenItExists objBook

End Sub


'**---------------------------------------------
'** Test for DataTableSheetRangeAddresses
'**---------------------------------------------
'''
''' about OutputDicToSheet and GetDataTableSheetRangeAddressesFromDTDicAndDTFormatter
'''
Private Sub msubSanityTestToGetDataTableSheetRangeAddressesFromDTDicAndDTFormatter()

    Dim objFieldTitlesCol As Collection, intRowMax As Long, intColumnMax As Long, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    
    Dim objDic As Scripting.Dictionary

    Dim strBookPath As String, objSheet As Excel.Worksheet, objBook As Excel.Workbook, objDataTableSheetFormatter As DataTableSheetFormatter
    Dim varAllowToShowFieldTitle As Variant, varRowCol As Variant, objRowCol As Collection
    Dim strSheetName As String
    
    Dim objDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, objLogDic As Scripting.Dictionary
    
    Const dblFixedColumnWidth As Double = 11
    

    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\OneCaseOfDataTableSomethingOutputSanityTest.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    

    Set objDic = Nothing: Set objFieldTitlesCol = Nothing

    intTopLeftRowIndex = 3: intTopLeftColumnIndex = 4

    varAllowToShowFieldTitle = True
    
    intRowMax = 3: intColumnMax = 4

    SetupPadSimpleIntegerForRCTableDicAndFieldTitles objDic, objFieldTitlesCol, intRowMax, intColumnMax


    'Debug.Print objCol.Count

    Set objDataTableSheetFormatter = GetDataTableSheetFormatterFromFieldTitlesBySameColumnWidth(objFieldTitlesCol, dblFixedColumnWidth, ColumnNameInteriorOfGatheredColTable, RecordCellsGrayAll)

    With objDataTableSheetFormatter
    
        .AllowToShowFieldTitle = varAllowToShowFieldTitle
        
        .TopLeftColumnIndex = intTopLeftColumnIndex
        
        .TopLeftRowIndex = intTopLeftRowIndex
        
        If Not varAllowToShowFieldTitle Then
        
            ' FieldTitleOrderToWidth
        
            Set .FieldTitleOrderToColumnWidthDic = GetFieldTitleOrderToColumnWidthDicForFixedWidthFromFieldTitleCol(objFieldTitlesCol, dblFixedColumnWidth)
        End If
    End With
    
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    strSheetName = "Sample" & Format(objBook.Worksheets.Count - 1, "000")
    
    objSheet.Name = strSheetName
    
    OutputDicToSheet objSheet, objDic, objFieldTitlesCol, objDataTableSheetFormatter
    
    Set objDataTableSheetRangeAddresses = GetDataTableSheetRangeAddressesFromDTDicAndDTFormatter(objSheet, objDic, objFieldTitlesCol, objDataTableSheetFormatter)
    
    Set objLogDic = mfobjGetLogDicForExpandTableParamsAndDataTableSheetRangeAddresses(intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex, varAllowToShowFieldTitle, objDataTableSheetRangeAddresses)
    
    OutputDicToAutoShapeLogTextOnSheet objSheet, objLogDic, "A condition OutputDicToSheet sub-procedure test", XlShapeTextLogInteriorFillDefault
    
    DeleteDummySheetWhenItExists objBook
End Sub


'''
''' about OutputVarTableToSheet and GetDataTableSheetRangeAddressesFromDTVarAndDTFormatter
'''
Private Sub msubSanityTestToGetDataTableSheetRangeAddressesFromDTVarAndDTFormatter()

    Dim objFieldTitlesCol As Collection, intRowMax As Long, intColumnMax As Long, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    
    Dim varRCValues As Variant

    Dim strBookPath As String, objSheet As Excel.Worksheet, objBook As Excel.Workbook, objDataTableSheetFormatter As DataTableSheetFormatter
    Dim varAllowToShowFieldTitle As Variant, varRowCol As Variant, objRowCol As Collection
    Dim strSheetName As String
    
    Dim objDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, objLogDic As Scripting.Dictionary
    
    Const dblFixedColumnWidth As Double = 11
    

    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\OneCaseOfVariantDataTableSomethingOutputSanityTest.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    

    Set objFieldTitlesCol = Nothing

    intTopLeftRowIndex = 3: intTopLeftColumnIndex = 4

    varAllowToShowFieldTitle = True
    
    intRowMax = 3: intColumnMax = 4

    SetupPadSimpleIntegerForRCTableAndFieldTitles varRCValues, objFieldTitlesCol, intRowMax, intColumnMax


    'Debug.Print objCol.Count

    Set objDataTableSheetFormatter = GetDataTableSheetFormatterFromFieldTitlesBySameColumnWidth(objFieldTitlesCol, dblFixedColumnWidth, ColumnNameInteriorOfGatheredColTable, RecordCellsGrayAll)

    With objDataTableSheetFormatter
    
        .AllowToShowFieldTitle = varAllowToShowFieldTitle
        
        .TopLeftColumnIndex = intTopLeftColumnIndex
        
        .TopLeftRowIndex = intTopLeftRowIndex
        
        If Not varAllowToShowFieldTitle Then
        
            ' FieldTitleOrderToWidth
        
            Set .FieldTitleOrderToColumnWidthDic = GetFieldTitleOrderToColumnWidthDicForFixedWidthFromFieldTitleCol(objFieldTitlesCol, dblFixedColumnWidth)
        End If
    End With
    
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    strSheetName = "Sample" & Format(objBook.Worksheets.Count - 1, "000")
    
    objSheet.Name = strSheetName
    
    OutputVarTableToSheet objSheet, varRCValues, objFieldTitlesCol, objDataTableSheetFormatter
    
    Set objDataTableSheetRangeAddresses = GetDataTableSheetRangeAddressesFromDTVarAndDTFormatter(objSheet, varRCValues, objFieldTitlesCol, objDataTableSheetFormatter)
    
    Set objLogDic = mfobjGetLogDicForExpandTableParamsAndDataTableSheetRangeAddresses(intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex, varAllowToShowFieldTitle, objDataTableSheetRangeAddresses)
    
    OutputDicToAutoShapeLogTextOnSheet objSheet, objLogDic, "A condition OutputVarTableToSheet sub-procedure test", XlShapeTextLogInteriorFillDefault
    
    DeleteDummySheetWhenItExists objBook
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetLogDicForExpandTableParamsAndDataTableSheetRangeAddresses(ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long, _
        ByVal vblnAllowToShowFieldTitle As Boolean, _
        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add "Data-table row max", vintRowMax
        
        .Add "Data-table column max", vintColumnMax
        
        .Add "Row index of the top-left of the data-table", vintTopLeftRowIndex
        
        .Add "Column index of the top-left of the data-table", vintTopLeftColumnIndex
        
        .Add "AllowToShowFieldTitle", vblnAllowToShowFieldTitle
        
        With vobjDataTableSheetRangeAddresses
            
            objDic.Add "Field-titles range-address", .FieldTitlesRangeAddress
            
            objDic.Add "Records range-address", .RecordAreaRangeAddress
        End With
        
        .Add "Logging date-time", Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
    End With

    Set mfobjGetLogDicForExpandTableParamsAndDataTableSheetRangeAddresses = objDic
End Function

