Attribute VB_Name = "UTfDataVectorSheetIn"
'
'   sanity tests to get a one-dimensioal vector from Excel Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on UTfDecorationSetterToXlSheet.bas and DataVectorSheetIn.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 10/Feb/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToUTfDataVectorSheetIn()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UTfDataTableSheetExOut,DataVectorSheetIn,DataTableSheetExOut"
End Sub

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetBothColumnVectorValuesAndRowVectorValuesOnSheet()

    Dim objBook As Excel.Workbook, strBookPath As String
    Dim objSheet As Excel.Worksheet, varColumnVector As Variant, varRowVector As Variant

    Dim intRowMax As Long, intColumnMax As Long

    Dim intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long

    Dim intFieldTitlesRowIndex As Long, intFieldTitlesColumnIndex As Long


    intRowMax = 13

    intColumnMax = 8

    strBookPath = GetUnitTestingSheetRandomizedDataBookDir() & "\TestingSheetRandomizedValuesTests.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)


    intFieldTitlesRowIndex = 3: intTopLeftColumnIndex = 3



    Set objSheet = GetSampleRandomSomeValuesTableSheetForTest(objBook, "SampleTable", intRowMax, intColumnMax, intFieldTitlesRowIndex, intTopLeftColumnIndex, 4, "", False)

    DeleteDummySheetWhenItExists objBook

    Debug.Print "Testing Column title: " & objSheet.Cells(intFieldTitlesRowIndex, intTopLeftColumnIndex + 2).Value

    GetColumnVectorValuesOnSheet varColumnVector, objSheet.Cells(intFieldTitlesRowIndex + 1, intTopLeftColumnIndex + 2)

    DebugVar varColumnVector, True
    
    ' 2nd transposed
    
    intTopLeftRowIndex = 3: intFieldTitlesColumnIndex = 3
    
    Set objSheet = GetTransposeSampleRandomSomeValuesTableSheetForTest(objBook, "TranposeSampleTable", intRowMax, intColumnMax, intTopLeftRowIndex, intFieldTitlesColumnIndex, 4, "", False)
    
    GetRowVectorValuesOnSheet varRowVector, objSheet.Cells(intTopLeftRowIndex + 2, intFieldTitlesColumnIndex + 1)
    
    Debug.Print "Testing Row title: " & objSheet.Cells(intTopLeftRowIndex + 2, intFieldTitlesColumnIndex).Value
    
    DebugVar varRowVector
    
End Sub



