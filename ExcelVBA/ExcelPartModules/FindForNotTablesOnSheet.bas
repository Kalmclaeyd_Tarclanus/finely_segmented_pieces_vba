Attribute VB_Name = "FindForNotTablesOnSheet"
'
'   correct some values in an CurrentRegion and find CurrentRegions on UsedRange
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on VariantTypeConversion.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 17/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Enumeration
'///////////////////////////////////////////////
'''
''' Range line matching pattern
'''
Public Enum RangeValuesLineMatchPattern
    
    RangeLineAreAllEmpties = 0
    
    RangeLineIsNoMatchAndSomeValuesExist = 1
    
    RangeLineAreAnAllValueSetMatched = 2    ' find field-titles row indexes or ignoring row indexes, For each cell value, Not use StrComp, but use InStr (Using partial matiching)
End Enum

'''
''' some processing flag after matching
'''
Public Enum ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag

    NoMatchedOfLineValuesPatternPartialMatched = &H0

    OnlyIgnoreValuesAfterRangeLineValuesMatched = &H1
    
    RecordValuesToCacheAfterRangeLineValuesMatched = &H2

    ReconizeFieldTitlesAfterRangeLineValuesMatched = &H3

End Enum

'''
'''
'''
Public Enum NotTablesDistinctAndConvertDicType

    FindAndConvertFieldTitlesDicFromNotTableRegion
    
    DistinctTableByAdditionalInfoFromNotTableRegion

    ExtractTableByFilteringInfoDicFromNotTableRegion
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Dump plural CurrentRegion from extracted not-table sheet
'**---------------------------------------------
'''
'''
'''
Public Sub DumpCollectedRegionTablesExtractedFromNoneTablesSheetWithTopBottomDirection(ByVal vstrOutputBookPath As String, ByVal vobjFirstTopLeftRange As Excel.Range)

    msubDumpCollectedRegionTablesExtractedFromNoneTablesSheetWithOneDirection vstrOutputBookPath, vobjFirstTopLeftRange
End Sub

'''
'''
'''
Public Sub DumpCollectedRegionTablesExtractedFromNoneTablesSheetWithLeftRightDirection(ByVal vstrOutputBookPath As String, ByVal vobjFirstTopLeftRange As Excel.Range)

    msubDumpCollectedRegionTablesExtractedFromNoneTablesSheetWithOneDirection vstrOutputBookPath, vobjFirstTopLeftRange, False
End Sub

'''
'''
'''
Private Sub msubDumpCollectedRegionTablesExtractedFromNoneTablesSheetWithOneDirection(ByVal vstrOutputBookPath As String, _
        ByVal vobjFirstTopLeftRange As Excel.Range, _
        Optional ByVal vblnIsTopBottomDirectionSearched As Boolean = True)


    Dim objOneDirectionRegions As Collection, varTopLeftRange As Variant, objTopLeftRange As Excel.Range
    Dim varRCValues As Variant, objFieldTitlesCol As Collection, i As Long, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(vstrOutputBookPath)
    
    If vblnIsTopBottomDirectionSearched Then
    
        Set objOneDirectionRegions = FindCurrentRegionsByTopBottomDirection(vobjFirstTopLeftRange)
    Else
        Set objOneDirectionRegions = FindCurrentRegionsByLeftRightDirection(vobjFirstTopLeftRange)
    End If


    i = 1
    
    With New DoubleStopWatch
    
        .MeasureStart
    
        For Each varTopLeftRange In objOneDirectionRegions
        
            Set objTopLeftRange = varTopLeftRange
            
            GetVarTableAndTemporaryFieldTitlesFromTopLeftRange varRCValues, objFieldTitlesCol, objTopLeftRange
            
            Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
            
            If vblnIsTopBottomDirectionSearched Then
            
                objSheet.Name = "DumpByTopBottom" & Format(i, "00")
            Else
                objSheet.Name = "DumpByLeftRight" & Format(i, "00")
            End If
            
            OutputVarTableToSheet objSheet, varRCValues, objFieldTitlesCol, GetDataTableSheetFormatterFromFieldTitlesBySameColumnWidth(objFieldTitlesCol, 11, ColumnNameInteriorOfExtractedTableFromSheet)
            
            AddAutoShapeGeneralMessage objSheet, "This region address: " & objTopLeftRange.CurrentRegion.Address & vbNewLine & "Sheet name: " & objTopLeftRange.Worksheet.Name
            
            
            i = i + 1
        Next
        
        .MeasureInterval
        
        AddAutoShapeGeneralMessage objSheet, "Elapsed all-processes time: " & .ElapsedTimeByString & vbNewLine & "Logged time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss"), XlShapeTextLogInteriorFillRedCornerGradient
    End With

    DeleteDummySheetWhenItExists objBook
End Sub


'**---------------------------------------------
'** Temporary field-titles
'**---------------------------------------------
'''
'''
'''
Public Sub GetVarTableAndTemporaryFieldTitlesFromTopLeftRange(ByRef rvarRCValues As Variant, ByRef robjFieldTitles As Collection, ByVal vobjInputTopLeftRange As Excel.Range, Optional ByVal vstrColumnNamePrefix As String = "Col")

    Dim objRegion As Excel.Range
    
    Set objRegion = vobjInputTopLeftRange.CurrentRegion

    Set robjFieldTitles = GetTemporaryFieldTitlesColFromRegion(objRegion, vstrColumnNamePrefix)

    With objRegion
    
        ReDim rvarRCValues(1 To .Rows.Count, 1 To .Columns.Count)
        
        rvarRCValues = .Value
    End With
End Sub


'''
'''
'''
Public Function GetTemporaryFieldTitlesColFromRegion(ByRef robjRegion As Excel.Range, Optional ByVal vstrColumnNamePrefix As String = "Col") As Collection

    Dim objCol As Collection, i As Long
    
    Set objCol = New Collection
    
    For i = 1 To robjRegion.Columns.Count
    
        objCol.Add vstrColumnNamePrefix & CStr(i)
    Next

    Set GetTemporaryFieldTitlesColFromRegion = objCol
End Function

'**---------------------------------------------
'** Find some CurrentRegion ranges from UsedRange
'**---------------------------------------------
'''
'''
'''
Public Function FindCurrentRegionsByTopBottomDirection(ByVal vobjStartingTopLeftRange As Excel.Range) As Collection

    Dim objRanges As Collection
    Dim objSheet As Excel.Worksheet, intStartingRowIndex As Long, intStartingColumnIndex As Long
    
    Dim intFirstRegionStartRowIndex As Long, intFirstRegionEndRowIndex As Long, blnIsFirstRegionFound As Boolean
    Dim varValues As Variant
    
    Dim intUsedRangeStartRowIndex As Long, intUsedRangeEndRowIndex As Long, i As Long, objRegion As Excel.Range
    Dim intCurrentRowIndex As Long, objCurrentTopLeftRange As Excel.Range
    
    
    Set objRanges = New Collection
    
    Set objSheet = vobjStartingTopLeftRange.Worksheet
    
    With vobjStartingTopLeftRange
    
        intStartingRowIndex = .Row
    
        intStartingColumnIndex = .Column
    End With
    
    If IsEmpty(objSheet.Cells(intStartingRowIndex, intStartingColumnIndex).Value) Then
    
        blnIsFirstRegionFound = False
    Else
        blnIsFirstRegionFound = True
    
        With vobjStartingTopLeftRange.CurrentRegion
        
            intFirstRegionStartRowIndex = intStartingRowIndex
        
            intFirstRegionEndRowIndex = .Row + .Rows.Count - 1
        End With
    End If
    
    
    With objSheet.UsedRange
    
        intUsedRangeStartRowIndex = .Row
        
        intUsedRangeEndRowIndex = .Row + .Rows.Count - 1
        
        ReDim varValues(1 To .Rows.Count, 1 To 1)
        
        varValues = objSheet.Cells(.Row, intStartingColumnIndex).Resize(.Rows.Count, 1).Value
        
        i = 1
        
        Do While i <= .Rows.Count
        
            If IsEmpty(varValues(i, 1)) Then
            
                i = i + 1
            Else
                ' find a Range region
                
                intCurrentRowIndex = .Row + i - 1
                
                If blnIsFirstRegionFound And (intFirstRegionStartRowIndex <= intCurrentRowIndex And intCurrentRowIndex <= intFirstRegionEndRowIndex) Then
                
                    Set objCurrentTopLeftRange = objSheet.Cells(intStartingRowIndex, intStartingColumnIndex)
                    
                    objRanges.Add objCurrentTopLeftRange
                    
                    i = i + objCurrentTopLeftRange.CurrentRegion.Rows.Count
                Else
                
                    Set objRegion = objSheet.Cells(intCurrentRowIndex, intStartingColumnIndex).CurrentRegion
                    
                    With objRegion
                    
                        objRanges.Add objSheet.Cells(intCurrentRowIndex, intStartingColumnIndex)
                    
                        i = i + .Rows.Count
                    End With
                End If
            End If
        Loop
    End With
    
    Set FindCurrentRegionsByTopBottomDirection = objRanges
End Function

'''
'''
'''
Public Function FindCurrentRegionsByLeftRightDirection(ByVal vobjStartingTopLeftRange As Excel.Range) As Collection

    Dim objRanges As Collection
    Dim objSheet As Excel.Worksheet, intStartingRowIndex As Long, intStartingColumnIndex As Long
    
    Dim intFirstRegionStartColumnIndex As Long, intFirstRegionEndColumnIndex As Long, blnIsFirstRegionFound As Boolean
    Dim varValues As Variant
    
    Dim intUsedRangeStartColumnIndex As Long, intUsedRangeEndColumnIndex As Long, j As Long, objRegion As Excel.Range
    
    Dim intCurrentColumnIndex As Long, objCurrentTopLeftRange As Excel.Range
    
    
    Set objRanges = New Collection
    
    Set objSheet = vobjStartingTopLeftRange.Worksheet
    
    With vobjStartingTopLeftRange
    
        intStartingRowIndex = .Row
    
        intStartingColumnIndex = .Column
    End With
    
    If IsEmpty(objSheet.Cells(intStartingRowIndex, intStartingColumnIndex).Value) Then
    
        blnIsFirstRegionFound = False
    Else
        blnIsFirstRegionFound = True
    
        With vobjStartingTopLeftRange.CurrentRegion
        
            intFirstRegionStartColumnIndex = intStartingColumnIndex
        
            intFirstRegionEndColumnIndex = .Column + .Columns.Count - 1
        End With
    End If
    
    
    With objSheet.UsedRange
    
        intUsedRangeStartColumnIndex = .Column
        
        intUsedRangeEndColumnIndex = .Column + .Columns.Count - 1
        
        ReDim varValues(1 To 1, 1 To .Columns.Count)
        
        varValues = objSheet.Cells(intStartingRowIndex, .Column).Resize(1, .Columns.Count).Value
        
        j = 1
        
        Do While j <= .Columns.Count
        
            If IsEmpty(varValues(1, j)) Then
            
                j = j + 1
            Else
                ' find a Range region
                
                intCurrentColumnIndex = .Column + j - 1
                
                If blnIsFirstRegionFound And (intFirstRegionStartColumnIndex <= intCurrentColumnIndex And intCurrentColumnIndex <= intFirstRegionEndColumnIndex) Then
                
                    Set objCurrentTopLeftRange = objSheet.Cells(intStartingRowIndex, intStartingColumnIndex)
                    
                    objRanges.Add objCurrentTopLeftRange
                    
                    j = j + objCurrentTopLeftRange.CurrentRegion.Columns.Count
                Else
                
                    Set objRegion = objSheet.Cells(intStartingRowIndex, intCurrentColumnIndex).CurrentRegion
                    
                    With objRegion
                    
                        objRanges.Add objSheet.Cells(intStartingRowIndex, intCurrentColumnIndex)
                    
                        j = j + .Columns.Count
                    End With
                End If
            End If
        Loop
    End With
    
    Set FindCurrentRegionsByLeftRightDirection = objRanges
End Function


'**---------------------------------------------
'** Replace values based on specified patterns of a Range region
'**---------------------------------------------
'''
'''
'''
Public Sub ReplaceRowValuesBasedOnMatchedRowValuesPatternsOnARangeRegion(ByVal vobjKeyToCorrectedValuesDic As Scripting.Dictionary, _
        ByVal vobjKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary, _
        ByVal vobjRegion As Excel.Range)

    ' use the robjKeyToRowIndexToMatchedPatternDic from the GetRowIndexesOnRangeRegionByRowValuesPatterns

    Dim varInputKey As Variant, objIndexToMatchedPatternDic As Scripting.Dictionary, varRowIndex As Variant, i As Long, j As Long
    
    Dim varRCValues() As Variant, varCorrectedValue As Variant, objMatchedPattern As Collection
    Dim intSpecifiedRowIndex As Long
    
    Dim objCorrectedLineValues As Collection, strCorrectedRowValuesDelimitedByComma As String, strValue As String
    
    If Not vobjKeyToCorrectedValuesDic Is Nothing Then
    
        With vobjKeyToCorrectedValuesDic
        
            For Each varInputKey In .Keys
            
                With vobjKeyToRowIndexToMatchedPatternDic
                
                    If .Exists(varInputKey) Then
                    
                        strCorrectedRowValuesDelimitedByComma = vobjKeyToCorrectedValuesDic.Item(varInputKey)
                    
                        Set objIndexToMatchedPatternDic = .Item(varInputKey)
                        
                        With objIndexToMatchedPatternDic
                        
                            For Each varRowIndex In .Keys
                            
                                intSpecifiedRowIndex = varRowIndex
                            
                                Set objMatchedPattern = .Item(varRowIndex)
                                
                                ' Attention - objMatchedPattern.Count <= vobjRegion.Columns.Count
                            
                                With vobjRegion
                            
                                    ReDim varRCValues(1 To 1, 1 To .Columns.Count)
                            
                                    varRCValues = .Cells(intSpecifiedRowIndex, 1).Resize(1, objMatchedPattern.Count).Value
                            
                                    j = 1
                                    
                                    For Each varCorrectedValue In Split(strCorrectedRowValuesDelimitedByComma, ",")
                                    
                                        ' Conversion between string type and general Variant data-type
                                        
                                        UpdateVariantValueFromDifferences varRCValues(1, j), varCorrectedValue
                                    
                                        If j = objMatchedPattern.Count Then
                                        
                                            ' Satisfied objMatchedPattern.Count < vobjRegion.Columns.Count
                                        
                                            Exit For
                                        End If
                                    
                                        j = j + 1
                                    Next
                            
                                    ReleaseMergedArea .Worksheet.Range(.Cells(intSpecifiedRowIndex, 1), .Cells(intSpecifiedRowIndex, objMatchedPattern.Count))
                            
                                    .Cells(intSpecifiedRowIndex, 1).Resize(1, objMatchedPattern.Count).Value = varRCValues
                                End With
                            Next
                        End With
                    End If
                    
                End With
            Next
        End With
    End If
End Sub




'**---------------------------------------------
'** Find values pattern on a Range region
'**---------------------------------------------
'''
''' Get a selected columns table without noize row data
'''
Public Sub GetRCDataTableOfOnlySelectedFieldTitlesAfterCorrectingProcessByFindingRowValuesPatterns(ByRef robjDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vobjSelectedFieldTitles As Collection, _
        ByVal vobjRegion As Excel.Range, _
        ByVal vobjRowValuesPatternToProcessingFlagDic As Scripting.Dictionary, _
        Optional ByVal vobjIsNotNothingConditionFieldTitles As Collection = Nothing, _
        Optional ByVal vobjInUsedValuesConditionFieldTitleToValuesDelimitedCommaDic As Scripting.Dictionary = Nothing)


    Dim varRCValues As Variant, i As Long
    Dim enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag As ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag, strMatchedRowPatternKey As String
    Dim blnIsFieldTitlesColFound As Boolean, objEnabledColumnIndexKeysDic As Scripting.Dictionary, objRowCol As Collection
    Dim objIsNotNothingConditionColumnIndexKeysDic As Scripting.Dictionary, objInUsedValuedConditionColumnIndexKeysDic As Scripting.Dictionary
    
    Dim objInUsedValuesConditionFieldTitleToValuesDic As Scripting.Dictionary
    
    
    blnIsFieldTitlesColFound = False
    
    Set robjDTCol = New Collection
    
    Set objEnabledColumnIndexKeysDic = New Scripting.Dictionary: Set objIsNotNothingConditionColumnIndexKeysDic = New Scripting.Dictionary
    
    Set objInUsedValuedConditionColumnIndexKeysDic = New Scripting.Dictionary
    
    Set objInUsedValuesConditionFieldTitleToValuesDic = GetNestedDicFromKeyToValuesDelimitedCommaDic(vobjInUsedValuesConditionFieldTitleToValuesDelimitedCommaDic)
    
    With vobjRegion
    
        msubGetWholeRowColumnValuesToVariantArray varRCValues, vobjRegion
        
        For i = 1 To .Rows.Count
        
            enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag = mfenmGetCorrectProcessByFindingRowValuesPattern(strMatchedRowPatternKey, varRCValues, i, vobjRowValuesPatternToProcessingFlagDic)
        
            Select Case enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag
            
                Case ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag.NoMatchedOfLineValuesPatternPartialMatched
                
                    ' This row is parmitted to add DataTable body
                    
                    Set objRowCol = mfobjGetRowColWithLimitedSelectedColumnsFromRowIndexAndRCValues(i, varRCValues, objEnabledColumnIndexKeysDic, objIsNotNothingConditionColumnIndexKeysDic, objInUsedValuedConditionColumnIndexKeysDic, objInUsedValuesConditionFieldTitleToValuesDic)
                    
                    If Not objRowCol Is Nothing Then
                        
                        robjDTCol.Add objRowCol
                    End If
                    
                Case ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag.ReconizeFieldTitlesAfterRangeLineValuesMatched
                
                    ' Get field titles
                    
                    If Not blnIsFieldTitlesColFound Then
                    
                        Set robjFieldTitlesCol = mfobjGetRowColFromRowIndexAndRCValuesLimitedByExactMatchedValues(objEnabledColumnIndexKeysDic, objIsNotNothingConditionColumnIndexKeysDic, objInUsedValuedConditionColumnIndexKeysDic, i, varRCValues, vobjSelectedFieldTitles, vobjIsNotNothingConditionFieldTitles, objInUsedValuesConditionFieldTitleToValuesDic)
                        
                        blnIsFieldTitlesColFound = True
                    End If
            End Select
        Next
    End With
End Sub

'''
''' Get a table without noize row data
'''
Public Sub GetRCDataTableAfterCorrectingProcessByFindingRowValuesPatterns(ByRef robjDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vobjRegion As Excel.Range, _
        ByVal vobjRowValuesPatternToProcessingFlagDic As Scripting.Dictionary)


    Dim varRCValues As Variant, i As Long
    Dim enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag As ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag, strMatchedRowPatternKey As String
    
    Dim blnIsFieldTitlesColFound As Boolean
    
    
    blnIsFieldTitlesColFound = False
    
    Set robjDTCol = New Collection
    
    With vobjRegion
    
        msubGetWholeRowColumnValuesToVariantArray varRCValues, vobjRegion
        
        For i = 1 To .Rows.Count
        
            enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag = mfenmGetCorrectProcessByFindingRowValuesPattern(strMatchedRowPatternKey, varRCValues, i, vobjRowValuesPatternToProcessingFlagDic)
        
            Select Case enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag
            
                Case ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag.NoMatchedOfLineValuesPatternPartialMatched
                
                    ' This row is parmitted to add DataTable body
                    
                    robjDTCol.Add mfobjGetRowColFromRowIndexAndRCValues(i, varRCValues)
                    
                Case ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag.ReconizeFieldTitlesAfterRangeLineValuesMatched
                
                    ' Get field titles
                    
                    If Not blnIsFieldTitlesColFound Then
                    
                        Set robjFieldTitlesCol = mfobjGetRowColFromRowIndexAndRCValues(i, varRCValues)
                        
                        blnIsFieldTitlesColFound = True
                    End If
            End Select
        Next
    End With
End Sub

'''
'''
'''
Public Function GetRowIndexesOnRangeRegionByRowValuesPatterns(ByVal vobjRegion As Excel.Range, _
        ByVal vobjRowValuesPatternToProcessingFlagDic As Scripting.Dictionary, _
        Optional ByVal vblnAllowToExitWhenFoundFieldTitlesRow As Boolean = False) As Scripting.Dictionary


    Dim objKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary

    Dim varRCValues As Variant, intRowMax As Long, i As Long
    Dim enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag As ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag, strMatchedRowPatternKey As String
    
    
    Set objKeyToRowIndexToMatchedPatternDic = New Scripting.Dictionary
    
    With vobjRegion
    
        msubGetWholeRowColumnValuesToVariantArray varRCValues, vobjRegion
        
        For i = 1 To .Rows.Count
        
            enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag = mfenmGetCorrectProcessByFindingRowValuesPattern(strMatchedRowPatternKey, varRCValues, i, vobjRowValuesPatternToProcessingFlagDic)
            
            Select Case enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag
            
                Case RecordValuesToCacheAfterRangeLineValuesMatched, ReconizeFieldTitlesAfterRangeLineValuesMatched
            
                    msubSetupRecordValuesToCacheAfterRangeLineValuesMatched objKeyToRowIndexToMatchedPatternDic, strMatchedRowPatternKey, varRCValues, i, vobjRowValuesPatternToProcessingFlagDic
            End Select
            
            If vblnAllowToExitWhenFoundFieldTitlesRow And enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag = ReconizeFieldTitlesAfterRangeLineValuesMatched Then
            
                Exit For
            End If
        Next
    End With
    
    
    Set GetRowIndexesOnRangeRegionByRowValuesPatterns = objKeyToRowIndexToMatchedPatternDic
End Function



'''
'''
'''
''' <Argument>robjKeyToRowIndexToMatchedPatternDic: Output - Dictionary(Of String[Key], Dictionary(Of Long[CurrentRowIndex], Of Collection(Of Values)))</Argument>
''' <Argument>rstrMatchedRowPatternKey: Input</Argument>
Private Sub msubSetupRecordValuesToCacheAfterRangeLineValuesMatched(ByRef robjKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary, _
        ByRef rstrMatchedRowPatternKey As String, _
        ByRef rvarRCValues As Variant, _
        ByRef rintCurrentRowIndex As Long, _
        ByVal vobjRowValuesPatternToProcessingFlagDic As Scripting.Dictionary)

    Dim objActualRowCol As Collection, j As Long, strKey As String, intMatchWordsCount As Long
    

    Set objActualRowCol = New Collection
    
    intMatchWordsCount = CountOfSpecifiedChar(rstrMatchedRowPatternKey, ",") + 1
    
    For j = 1 To UBound(rvarRCValues, 2)
    
        If j <= intMatchWordsCount Then
    
            objActualRowCol.Add rvarRCValues(rintCurrentRowIndex, j)
        End If
    Next

    msubAddKeyToRowIndexToMatchedPatternDic robjKeyToRowIndexToMatchedPatternDic, rstrMatchedRowPatternKey, rintCurrentRowIndex, objActualRowCol
End Sub

'''
'''
'''
Private Sub msubAddKeyToRowIndexToMatchedPatternDic(ByRef robjKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary, _
        ByRef rstrKey As String, _
        ByRef rintCurrentRowIndex As Long, _
        ByRef robjActualRowCol As Collection)

    Dim objIndexToMatchedPatternDic As Scripting.Dictionary
                
    With robjKeyToRowIndexToMatchedPatternDic
    
        If .Exists(rstrKey) Then
        
            Set objIndexToMatchedPatternDic = .Item(rstrKey)
        Else
            Set objIndexToMatchedPatternDic = New Scripting.Dictionary
            
            .Add rstrKey, objIndexToMatchedPatternDic
        End If
    
        objIndexToMatchedPatternDic.Add rintCurrentRowIndex, robjActualRowCol
    End With
End Sub

'''
''' for testing mfblnAreAllRowValuePatternPartialMatched
'''
Public Function GetDataTableColByFindingRowValuesPatterns(ByVal vobjRegion As Excel.Range, ByVal vobjRowValuesPatternsCol As Collection) As Collection

    Dim objDTCol As Collection, varRCValues As Variant, i As Long, j As Long
    Dim objRowCol As Collection
    
    
    Set objDTCol = New Collection
    
    With vobjRegion
    
        msubGetWholeRowColumnValuesToVariantArray varRCValues, vobjRegion
        
        For i = 1 To .Rows.Count
        
            If mfblnFindRowValuesPattern(varRCValues, i, vobjRowValuesPatternsCol) Then
            
                ' able to get DataTable body
            
                Debug.Print "Found row: " & CStr(i)
            
                objDTCol.Add mfobjGetRowColFromRowIndexAndRCValues(i, varRCValues)
            End If
        Next
    End With
    
    Set GetDataTableColByFindingRowValuesPatterns = objDTCol
End Function

'''
'''
'''
''' <Argument>rintRowIndex: Input</Argument>
''' <Argument>rvarRCValues: Input</Argument>
''' <Return>Collection</Return>
Private Function mfobjGetRowColFromRowIndexAndRCValues(ByRef rintRowIndex As Long, _
        ByRef rvarRCValues As Variant) As Collection

    Dim objRowCol As Collection, j As Long

    Set objRowCol = New Collection
    
    For j = 1 To UBound(rvarRCValues, 2)
                    
        objRowCol.Add rvarRCValues(rintRowIndex, j)
    Next

    Set mfobjGetRowColFromRowIndexAndRCValues = objRowCol
End Function

'''
'''
'''
''' <Argument>rintRowIndex: Input</Argument>
''' <Argument>rvarRCValues: Input</Argument>
''' <Argument>robjEnabledColumnIndexKeysDic: Input</Argument>
''' <Return>Collection</Return>
Private Function mfobjGetRowColWithLimitedSelectedColumnsFromRowIndexAndRCValues(ByRef rintRowIndex As Long, _
        ByRef rvarRCValues As Variant, _
        ByRef robjEnabledColumnIndexKeysDic As Scripting.Dictionary, _
        ByRef robjIsNotNothingConditionColumnIndexKeysDic As Scripting.Dictionary, _
        ByRef robjInUsedValuedConditionColumnIndexKeysDic As Scripting.Dictionary, _
        Optional ByVal vobjInUsedValuesConditionFieldTitleToValuesDic As Scripting.Dictionary) As Collection

    Dim objRowCol As Collection, objReturnCol As Collection, j As Long, blnIsNotAtLeastOneEmpty As Boolean, blnIsNotNothingConditionSatisfied As Boolean
    Dim objChildDic As Scripting.Dictionary, blnInUsedValuedConditionFieldTitleSatisfied As Boolean

    Set objReturnCol = Nothing

    Set objRowCol = New Collection
    
    blnIsNotAtLeastOneEmpty = False
    
    blnIsNotNothingConditionSatisfied = False
    
    blnInUsedValuedConditionFieldTitleSatisfied = False
    
    With robjEnabledColumnIndexKeysDic
    
        For j = 1 To UBound(rvarRCValues, 2)
        
            If .Exists(j) Then
        
                If Not IsEmpty(rvarRCValues(rintRowIndex, j)) Then
                
                    blnIsNotAtLeastOneEmpty = True
                End If
        
                objRowCol.Add rvarRCValues(rintRowIndex, j)
            End If
            
            ' Emulate WHERE <FieldTitle> IS NOT NULL
            If robjIsNotNothingConditionColumnIndexKeysDic.Count > 0 Then
            
                If robjIsNotNothingConditionColumnIndexKeysDic.Exists(j) Then
                
                    If IsEmpty(rvarRCValues(rintRowIndex, j)) Then
                    
                        blnIsNotNothingConditionSatisfied = True
                    End If
                End If
            End If
            
            ' Emulate WHERE <FieldTitle> In (,,,)
            If robjInUsedValuedConditionColumnIndexKeysDic.Count > 0 Then
                
                If robjInUsedValuedConditionColumnIndexKeysDic.Exists(j) Then
                
                    With vobjInUsedValuesConditionFieldTitleToValuesDic
                    
                        Set objChildDic = .Item(robjInUsedValuedConditionColumnIndexKeysDic.Item(j))
                    
                        With objChildDic
                        
                            If .Exists(rvarRCValues(rintRowIndex, j)) Then
                        
                                ' this row-collection is to be included
                            Else
                                blnInUsedValuedConditionFieldTitleSatisfied = True
                            End If
                        End With
                    End With
                End If
            End If
        Next
    End With


    If blnIsNotAtLeastOneEmpty And Not blnIsNotNothingConditionSatisfied And Not blnInUsedValuedConditionFieldTitleSatisfied Then
    
        Set objReturnCol = objRowCol
    End If
    
    Set mfobjGetRowColWithLimitedSelectedColumnsFromRowIndexAndRCValues = objReturnCol
End Function


'''
'''
'''
''' <Argument>robjEnabledColumnIndexKeysDic: Output</Argument>
''' <Argument>rintRowIndex: Input</Argument>
''' <Argument>rvarRCValues: Input</Argument>
''' <Argument>robjExactMatchedValues: Input</Argument>
''' <Return>Collection</Return>
Private Function mfobjGetRowColFromRowIndexAndRCValuesLimitedByExactMatchedValues(ByRef robjEnabledColumnIndexKeysDic As Scripting.Dictionary, _
        ByRef robjIsNotNothingConditionColumnIndexKeysDic As Scripting.Dictionary, _
        ByRef robjInUsedValuedConditionColumnIndexKeysDic As Scripting.Dictionary, _
        ByRef rintRowIndex As Long, _
        ByRef rvarRCValues As Variant, _
        ByRef robjExactMatchedValues As Collection, _
        Optional ByVal vobjIsNotNothingConditionFieldTitles As Collection = Nothing, _
        Optional ByVal vobjInUsedValuesConditionFieldTitleToValuesDic As Scripting.Dictionary) As Collection


    Dim objRowCol As Collection, j As Long, varComparingValue As Variant, blnIsNeedToCheckIsNotNothingConditionFieldTitles As Boolean
    Dim blnIsNeedToCheckInUsedValuesConditionFieldTitle As Boolean

    blnIsNeedToCheckIsNotNothingConditionFieldTitles = (Not vobjIsNotNothingConditionFieldTitles Is Nothing)

    blnIsNeedToCheckInUsedValuesConditionFieldTitle = (Not vobjInUsedValuesConditionFieldTitleToValuesDic Is Nothing)

    Set objRowCol = New Collection
    
    For j = 1 To UBound(rvarRCValues, 2)
        
        For Each varComparingValue In robjExactMatchedValues
        
            If rvarRCValues(rintRowIndex, j) = varComparingValue Then
        
                robjEnabledColumnIndexKeysDic.Add j, 0
        
                objRowCol.Add rvarRCValues(rintRowIndex, j)
            End If
        Next
        
        If blnIsNeedToCheckIsNotNothingConditionFieldTitles Then
        
            For Each varComparingValue In vobjIsNotNothingConditionFieldTitles
        
                If rvarRCValues(rintRowIndex, j) = varComparingValue Then
            
                    robjIsNotNothingConditionColumnIndexKeysDic.Add j, 0
                End If
            Next
        End If
        
        If blnIsNeedToCheckInUsedValuesConditionFieldTitle Then
        
            For Each varComparingValue In vobjInUsedValuesConditionFieldTitleToValuesDic.Keys
        
                If rvarRCValues(rintRowIndex, j) = varComparingValue Then
            
                    robjInUsedValuedConditionColumnIndexKeysDic.Add j, rvarRCValues(rintRowIndex, j)
                End If
            Next
        End If
    Next

    Set mfobjGetRowColFromRowIndexAndRCValuesLimitedByExactMatchedValues = objRowCol
End Function


'''
'''
'''
''' <Argument>rstrHitRowPatternKey: Output</Argument>
''' <Argument>rvarRCValues: Input</Argument>
''' <Argument>vintCurrentRowIndex: Input</Argument>
''' <Argument>vobjRowValuesPatternToProcessingFlagDic: Input</Argument>
''' <Return>ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag</Return>
Private Function mfenmGetCorrectProcessByFindingRowValuesPattern(ByRef rstrHitRowPatternKey As String, _
        ByRef rvarRCValues As Variant, _
        ByVal vintCurrentRowIndex As Long, _
        ByVal vobjRowValuesPatternToProcessingFlagDic As Scripting.Dictionary) As ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag


    Dim varRowPatternsKey As Variant
    Dim enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag As ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag
    
    
    enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag = NoMatchedOfLineValuesPatternPartialMatched
    
    With vobjRowValuesPatternToProcessingFlagDic
    
        For Each varRowPatternsKey In .Keys
        
            If "<IsValueNumeric>,月,,預金通帳より自動入出金" = varRowPatternsKey Then
            
                If rvarRCValues(vintCurrentRowIndex, 2) = "月" Then
                
                
                    Debug.Print "this"
                End If
            
            End If
        
            If mfblnAreAllRowValuePatternPartialMatched(rvarRCValues, vintCurrentRowIndex, varRowPatternsKey) Then
            
                enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag = vobjRowValuesPatternToProcessingFlagDic.Item(varRowPatternsKey)
            
                rstrHitRowPatternKey = varRowPatternsKey
            
                Exit For
            End If
        Next
    End With

    mfenmGetCorrectProcessByFindingRowValuesPattern = enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag
End Function

'''
'''
'''
Private Function mfblnFindRowValuesPattern(ByRef rvarRCValues As Variant, _
        ByVal vintCurrentRowIndex As Long, _
        ByVal vobjRowValuesPatternsCol As Collection)


    Dim varRowPatternsKey As Variant, blnIsSomePatternMatched As Boolean
    

    blnIsSomePatternMatched = False

    For Each varRowPatternsKey In vobjRowValuesPatternsCol
        
        If mfblnAreAllRowValuePatternPartialMatched(rvarRCValues, vintCurrentRowIndex, varRowPatternsKey) Then
        
            blnIsSomePatternMatched = True
        
            Exit For
        End If
    Next

    mfblnFindRowValuesPattern = blnIsSomePatternMatched
End Function

'''
''' Attention - 1). this omits strict comparing between floating numerical figures, 2). this serves Empty value as same as "" void-string
'''
Private Function mfblnAreAllRowValuePatternPartialMatched(ByRef rvarRCValues As Variant, _
        ByRef rintCurrentRowIndex As Long, _
        ByVal vstrRowPatternsKey As String, _
        Optional ByVal vblnAllowPartialMatchingAboutString As String = True) As Boolean

    Dim blnAreMatchedAll As Boolean, varPatternValue As Variant, strPatternValue As String, strValue As String
    Dim intColumnIndex As Long, intMax As Long, i As Long


    blnAreMatchedAll = True
        
    intColumnIndex = LBound(rvarRCValues, 2)
    
    i = 1: intMax = UBound(rvarRCValues, 2) - LBound(rvarRCValues, 2) + 1
    
    For Each varPatternValue In Split(vstrRowPatternsKey, ",")
    
        ' serve Empty value as same as "" void-string
        
        blnAreMatchedAll = AreTheseVariantAndSpecializedStringPatternMatched(rvarRCValues(rintCurrentRowIndex, intColumnIndex), varPatternValue, vblnAllowPartialMatchingAboutString)
        
        If Not blnAreMatchedAll Then
        
            Exit For
        End If
        
        If i = intMax Then
        
            Exit For
        End If
        
        intColumnIndex = intColumnIndex + 1
        
        i = i + 1
    Next


    mfblnAreAllRowValuePatternPartialMatched = blnAreMatchedAll
End Function


'''
'''
'''
Public Function GetKeyStringFromRangeLineValuePatternCol(ByRef robjRowPatternCol As Collection) As String

    GetKeyStringFromRangeLineValuePatternCol = GetLineTextFromCol(robjRowPatternCol, "", ";", False)
End Function


'''
'''
'''
Private Sub msubGetWholeRowColumnValuesToVariantArray(ByRef rvarRowColumnValues As Variant, ByRef robjRegion As Excel.Range)

    Dim intRowMax As Long, intColumnMax As Long, strRange
    
    With robjRegion
    
        intRowMax = .Rows.Count: intColumnMax = .Columns.Count
        
        ReDim rvarRowColumnValues(1 To intRowMax, 1 To intColumnMax)
    
        strRange = "A1" ' "A1:" & ConvertXlColumnIndexToLetter(.Columns.Count) & CStr(.Rows.Count)
        
        If intRowMax = 1 And intColumnMax = 1 Then
        
            rvarRowColumnValues(1, 1) = .Range(strRange).Resize(intRowMax, intColumnMax).Value
        Else
            rvarRowColumnValues = .Range(strRange).Resize(intRowMax, intColumnMax).Value
        End If
    End With
End Sub







