Attribute VB_Name = "DataTableSheetExOut"
'
'   Support transposed-output data-table into Excel Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 10/Feb/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' output row-column 2-dimensional collection type data table to sheet
'''
Public Sub TransposeOutputColToSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjCol As Collection, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal vobjTrDataTableSheetFormatter As TrDataTableSheetFormatter, _
        Optional ByVal vblnAllowToSetCellFormulasToRange As Boolean = False)


    Dim blnNoNecessaryToCheckRecordsExists As Boolean, intColumnsCountOfDataTable As Long, objColumnCol As Collection
    
    Dim intRecordTopLeftColumnIndex As Long
    

    blnNoNecessaryToCheckRecordsExists = False
    
    With vobjTrDataTableSheetFormatter
        
        If vobjFieldTitlesCol Is Nothing Then
        
            .AllowToShowFieldTitle = False
        End If
        
        .SetSheetFormatBeforeTableOut
    
        If .AllowToShowFieldTitle Then
        
            TransposeOutputOnlyFieldTitlesColToSheet vobjSheet, vobjFieldTitlesCol, .TopLeftRowIndex, .TopLeftColumnIndex
            
            intRecordTopLeftColumnIndex = .TopLeftColumnIndex + 1
        Else
            intRecordTopLeftColumnIndex = .TopLeftColumnIndex
        End If

        If Not vobjCol Is Nothing Then
        
            blnNoNecessaryToCheckRecordsExists = True
        
            Set objColumnCol = vobjCol.Item(1)
        
            intColumnsCountOfDataTable = objColumnCol.Count
        
            TransposeOutputOnlyDTCol vobjCol, intColumnsCountOfDataTable, vobjSheet, .TopLeftRowIndex, intRecordTopLeftColumnIndex, vblnAllowToSetCellFormulasToRange
        End If
        
        .SetTransposeDataTableSheetFormatAfterTableOut vobjSheet
    End With
End Sub


'''
'''
'''
Public Sub TransposeOutputOnlyDTCol(ByVal vobjDTCol As VBA.Collection, _
        ByVal vintColumnsCountOfDataTable As Long, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintRecordTopLeftRowIndex As Long = 1, _
        Optional ByVal vintRecordTopLeftColumnIndex As Long = 1, _
        Optional ByVal vblnAllowToSetCellFormulasToRange As Boolean = False)

    Dim objRange As Excel.Range, varTransposeValues As Variant
    
    Set objRange = vobjSheet.Cells(vintRecordTopLeftRowIndex, vintRecordTopLeftColumnIndex)
    
    GetTransposeRCValuesFromRCCollection varTransposeValues, vobjDTCol
    
    If vblnAllowToSetCellFormulasToRange Then
    
        objRange.Resize(vintColumnsCountOfDataTable, vobjDTCol.Count).Formula = varTransposeValues
    Else
        objRange.Resize(vintColumnsCountOfDataTable, vobjDTCol.Count).Value = varTransposeValues
    End If
End Sub


'''
''' get transposed Row-Column (2 dimensional array) values from inputted Row-Column collection
'''
''' <Argument>rvarTransposeValues: Output - Variant(n, m) 2-dimensional array Variant </Argument>
''' <Argument>vobjRCCol: Input</Argument>
Public Sub GetTransposeRCValuesFromRCCollection(ByRef rvarTransposeValues As Variant, ByVal vobjRCCol As VBA.Collection)

    Dim objValuesCol As Collection, varItem As Variant, varRowItem As Variant
    Dim intRowMax As Long, intColumnMax As Long, intRowIndex As Long, intColumnIndex As Long, blnIsTheRCColTwoDimensionalCollection As Boolean
    
    
    intRowMax = vobjRCCol.Count
    
    If IsObject(vobjRCCol.Item(1)) Then
    
        Set objValuesCol = vobjRCCol.Item(1)
        
        intColumnMax = objValuesCol.Count
        
        blnIsTheRCColTwoDimensionalCollection = True
    Else
        intColumnMax = 1
        
        blnIsTheRCColTwoDimensionalCollection = False
    End If
    
    
    ReDim rvarTransposeValues(1 To intColumnMax, 1 To intRowMax)
    
    intRowIndex = 1: intColumnIndex = 1
    
    For Each varRowItem In vobjRCCol
    
        If blnIsTheRCColTwoDimensionalCollection Then
        
            Set objValuesCol = varRowItem
            
            intColumnIndex = 1
            
            For Each varItem In objValuesCol
                
                If IsObject(varItem) Then
                
                    If Not varItem Is Nothing Then
                    
                        rvarTransposeValues(intColumnIndex, intRowIndex) = varItem
                    End If
                Else
                    rvarTransposeValues(intColumnIndex, intRowIndex) = varItem
                End If
                
                intColumnIndex = intColumnIndex + 1
            Next
        Else
            rvarTransposeValues(intColumnIndex, intRowIndex) = varRowItem
        End If
    
        intRowIndex = intRowIndex + 1
    Next
End Sub

'''
'''
'''
Public Sub TransposeOutputOnlyFieldTitlesColToSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitlesCol As Collection, _
        Optional vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)

    Dim varValues() As Variant
    
    GetFieldTitleRCValueAndCellFormattingOnSheet varValues, vobjSheet, vobjFieldTitlesCol, vintTopLeftRowIndex, vintTopLeftColumnIndex, True, True
    
    vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Resize(vobjFieldTitlesCol.Count, 1).Value = varValues
End Sub


'''
''' For transpose output, adjust columns width from info-type width value
'''
Public Sub AdjustColumnWidthByInfoTypeWithFindingRightDirectionForTransposeOutput(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjInfoTypeToColumnWidthDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintFieldTitlesColumnIndex As Long = 1)
    
    Dim i As Long, strSheetColumnLetter As String, strValue As String
    Dim objColumnWidthDic As Scripting.Dictionary, dblWidth As Double, strSheetRange As String
    
    Dim objTopLeftRange As Excel.Range
    Dim objDataRange As Excel.Range, intEndRightColumnIndex As Long, intEndBottomRowIndex As Long, objInfoTypeToColumnWidthDic As Scripting.Dictionary

    If vobjInfoTypeToColumnWidthDic Is Nothing Then
    
        Set objInfoTypeToColumnWidthDic = GetSimpleDefaultInfoTypeToColumnWidthDicForTransposeOutput()
    Else
        Set objInfoTypeToColumnWidthDic = vobjInfoTypeToColumnWidthDic
    End If

    
    If Not objInfoTypeToColumnWidthDic Is Nothing Then
    
        Set objTopLeftRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintFieldTitlesColumnIndex) & CStr(vintTopLeftRowIndex))
    
        With objInfoTypeToColumnWidthDic
    
            If .Exists("FieldTitlesColumn") Then
    
                objTopLeftRange.EntireColumn.ColumnWidth = .Item("FieldTitlesColumn")
            End If
        End With
        
        With vobjSheet
        
            With .UsedRange
        
                intEndRightColumnIndex = .Column + .Columns.Count - 1
            
                intEndBottomRowIndex = .Row + .Rows.Count - 1
            End With
        
            If vintFieldTitlesColumnIndex < intEndRightColumnIndex Then
            
                Set objDataRange = .Range(.Cells(vintTopLeftRowIndex, vintFieldTitlesColumnIndex + 1), .Cells(intEndBottomRowIndex, intEndRightColumnIndex))
            
                With objInfoTypeToColumnWidthDic
                
                    If .Exists("DataColumn") Then
                    
                        objDataRange.EntireColumn.ColumnWidth = .Item("DataColumn")
                    End If
                End With
            End If
        End With
    End If
End Sub

'''
'''
'''
Public Function GetSimpleDefaultInfoTypeToColumnWidthDicForTransposeOutput() As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary: Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add "FieldTitlesColumn", 12
        
        .Add "DataColumn", 18
    End With
    
    Set GetSimpleDefaultInfoTypeToColumnWidthDicForTransposeOutput = objDic
End Function


'''
'''
'''
Public Sub DecorateTransposeRangeOfFields(ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintFieldTitlesColumnIndex As Long = 1, _
        Optional ByVal venmInteriorType As FieldTitleInterior = FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQL, _
        Optional ByVal vblnAllowToUseSheetUsedRange As Boolean = False)


    Dim objTopLeftRange As Excel.Range, objFieldsTitlesRange As Excel.Range
    
    Set objTopLeftRange = vobjSheet.Cells(vintTopLeftRowIndex, vintFieldTitlesColumnIndex)
    
    If vblnAllowToUseSheetUsedRange Then
    
        With objTopLeftRange.Worksheet.UsedRange
        
            Set objFieldsTitlesRange = objTopLeftRange.Worksheet.Range(objTopLeftRange, .Worksheet.Cells(.Rows.Count + .Row - 1, objTopLeftRange.Column))
        End With
    Else
        With objTopLeftRange.CurrentRegion
        
            Set objFieldsTitlesRange = objTopLeftRange.Worksheet.Range(objTopLeftRange, .Worksheet.Cells(.Rows.Count + .Row - 1, objTopLeftRange.Column))
        End With
    End If
    
    DecorateRangeOfFieldTitlesForRDB objFieldsTitlesRange, venmInteriorType
End Sub

'''
''' adjust records cell-sizes by the specified standard setting
'''
Public Sub DecorateTransposeRangeOfRecordsAboutBorderFontHeight(ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintFieldTitlesColumnIndex As Long = 1, _
        Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBordersNone, _
        Optional ByVal vblnForceToSetSameRowHeight As Boolean = False, _
        Optional ByVal vblnBasedOnCurrentRegion As Boolean = True, _
        Optional ByVal vsngFontSize As Single = 10, _
        Optional ByVal vsngRowsHeight As Single = 18#)
    
    Dim intColumnMax As Long, objTopLeftRange As Excel.Range, objTableRange As Excel.Range
    Dim strRange As String
    
    With vobjSheet
    
        Set objTopLeftRange = .Range(ConvertXlColumnIndexToLetter(vintFieldTitlesColumnIndex) & CStr(vintTopLeftRowIndex))

        Set objTableRange = objTopLeftRange.CurrentRegion
    
        If vblnBasedOnCurrentRegion Then
    
            If 2 <= objTableRange.Columns.Count Then
    
                With objTableRange
    
                    DecorateRangeAboutBordersAndFont .Worksheet.Range(.Cells(1, 2), .Cells(.Rows.Count, .Columns.Count)), venmRecordCellsBorders, vsngFontSize, True, False
                
                    AdjustRowHeight .Worksheet.Rows(CStr(.Row) & ":" & CStr(.Row + .Rows.Count - 1)), True, vsngRowsHeight
                End With
            End If
        Else
            If (vintFieldTitlesColumnIndex + 1) <= .UsedRange.Columns.Count Then
                
                intColumnMax = .UsedRange.Columns.Count
                
                strRange = ConvertXlColumnIndexToLetter(vintFieldTitlesColumnIndex + 1) & CStr(vintTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intColumnMax) & CStr(.UsedRange.Rows.Count)
                
                DecorateRangeAboutBordersAndFont .Range(strRange), venmRecordCellsBorders, vsngFontSize, True, False
                
                AdjustRowHeight .Rows(CStr(vintTopLeftRowIndex) & ":" & CStr(.UsedRange.Rows.Count)), True, vsngRowsHeight
            End If
        End If
    End With
End Sub

