Attribute VB_Name = "DataTableSheetAutoChange"
'
'   Merge-cells and auto-padding values on Excel Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 28/Fri/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Windows API Declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function GetInputState Lib "user32" () As Boolean
#Else
    Private Declare Function GetInputState Lib "user32" () As Boolean
#End If

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** MergedArea general
'**---------------------------------------------
'''
'''
'''
Public Sub ReleaseMergedArea(ByRef robjRegion As Excel.Range)

    Dim objRange As Excel.Range
    
    For Each objRange In robjRegion
    
        With objRange
        
            If .MergeCells Then
        
                .UnMerge
            End If
        End With
    Next
End Sub


'**---------------------------------------------
'** Modify field-titles simply
'**---------------------------------------------
'''
''' replace field-title from the specifying Dictionary(Of String, String) object
'''
Public Sub ReplaceFieldTitleTexts(ByVal vobjSheet As Excel.Worksheet, ByVal vobjReplaceTextsDic As Scripting.Dictionary, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    Dim i As Long, intColumnMax As Long, objFieldTitlesRange As Excel.Range
    Dim varValues() As Variant, blnIsChanged As Boolean, strValue As String, strReplacedValue As String
    
    
    ReDim varValues(1 To 1, 1 To intColumnMax)
    
    With vobjSheet
    
        With .Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
            'intColumnMax = vobjSheet.UsedRange.Columns.Count
    
            intColumnMax = .Columns.Count
    
            Set objFieldTitlesRange = .Resize(1, intColumnMax)
    
            varValues = objFieldTitlesRange.Value
        End With
            
        blnIsChanged = False
        
        For i = 1 To intColumnMax
        
            If Not IsEmpty(varValues(1, i)) Then
            
                strValue = CStr(varValues(1, i))
                
                With vobjReplaceTextsDic
                
                    If .Exists(strValue) Then
                    
                        strReplacedValue = .Item(strValue)
                        
                        If strValue <> strReplacedValue Then
                        
                            varValues(1, i) = strReplacedValue
                        
                            blnIsChanged = True
                        End If
                    End If
                End With
            End If
        Next
        
        If blnIsChanged Then
        
            objFieldTitlesRange.Value = varValues
        End If
    End With
End Sub

'**---------------------------------------------
'** Modify characters for a region
'**---------------------------------------------
'''
'''
'''
Public Function ReplaceSpecifiedKeywordsByExaxtMatchForEachColumnRecords(ByRef robjCurrentRegion As Excel.Range, _
        ByRef robjReplaceWordDic As Scripting.Dictionary, _
        ByVal vobjSerchingFieldTitles As Collection, _
        Optional ByVal vintRelativeFieldTitlesRowIndex As Long = 1) As Boolean

    Dim objFieldTitleToPositionIndexDic As Scripting.Dictionary, intFieldTitlesRowIndex As Long, intTopLeftColumnIndex As Long, varFieldTitle As Variant
    Dim intColumnIndex As Long, objRecordsRange As Excel.Range, varValues As Variant, i As Long
    Dim blnIsAtLeastOneReplaced As Boolean


    With robjCurrentRegion

        intFieldTitlesRowIndex = .Row + vintRelativeFieldTitlesRowIndex - 1
        
        intTopLeftColumnIndex = .Column

        Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(vobjSerchingFieldTitles, .Worksheet, intFieldTitlesRowIndex, intTopLeftColumnIndex)
    End With

    blnIsAtLeastOneReplaced = False

    If Not objFieldTitleToPositionIndexDic Is Nothing Then

        If objFieldTitleToPositionIndexDic.Count > 0 Then
        
            With objFieldTitleToPositionIndexDic
            
                For Each varFieldTitle In .Keys
                
                    intColumnIndex = .Item(varFieldTitle)
                
                    With robjCurrentRegion
                    
                        Set objRecordsRange = .Worksheet.Range(.Cells(vintRelativeFieldTitlesRowIndex + 1, intColumnIndex), .Cells(.Rows.Count, intColumnIndex))
                    
                        With objRecordsRange
                        
                            ReDim varValues(1 To .Rows.Count, 1 To 1)
                    
                            varValues = .Value
                            
                            For i = 1 To .Rows.Count
                            
                                If IsAtLeastOneReplacedByExactMatchOfKeyForVariant(varValues(i, 1), robjReplaceWordDic) Then
                                
                                    If Not blnIsAtLeastOneReplaced Then
                                    
                                        blnIsAtLeastOneReplaced = True
                                    End If
                                End If
                            Next
                            
                            If blnIsAtLeastOneReplaced Then
                            
                                .Value = varValues
                            End If
                        End With
                    End With
                Next
            End With
        End If
    End If
    
    ReplaceSpecifiedKeywordsByExaxtMatchForEachColumnRecords = blnIsAtLeastOneReplaced
End Function

'''
''' About Japanese half-wide Kana
'''
Public Sub ReplaceHalfWideKanaIntoOrdinaryKanaInRangeInCurrentRegion(ByRef robjTopLeftRange As Excel.Range, _
        Optional ByVal vblnAllowToKeepFormulasInCells As Boolean = True)

    ReplaceHalfWideKanaIntoOrdinaryKanaInRange robjTopLeftRange.CurrentRegion, vblnAllowToKeepFormulasInCells
End Sub

'''
''' About Japanese half-wide Kana
'''
Public Sub ReplaceHalfWideKanaIntoOrdinaryKanaInRange(ByRef robjRange As Excel.Range, _
        Optional ByVal vblnAllowToKeepFormulasInCells As Boolean = True)

    Dim varValues As Variant, blnIsAtLeastOneReplaced As Boolean, i As Long, j As Long
    
    With robjRange
    
        ReDim varValues(1 To .Rows.Count, 1 To .Columns.Count)
    
        If vblnAllowToKeepFormulasInCells Then
        
            varValues = robjRange.Formula
        Else
            varValues = robjRange.Value
        End If
        
        blnIsAtLeastOneReplaced = False
        
        For i = 1 To .Rows.Count
        
            For j = 1 To .Columns.Count
            
                If Not IsEmpty(varValues(i, j)) Then
            
                    If IsSomeHalfWideKanaConvertedToOrdinaryFullWideKanaForVariant(varValues(i, j)) Then
                    
                        If Not blnIsAtLeastOneReplaced Then
                        
                            blnIsAtLeastOneReplaced = True
                        End If
                    End If
                End If
            Next
        Next
    End With
    
    If blnIsAtLeastOneReplaced Then
    
        If vblnAllowToKeepFormulasInCells Then
        
            robjRange.Formula = varValues
        Else
            robjRange.Value = varValues
        End If
    End If
End Sub

'''
''' About Alphabet and figures
'''
Public Sub ReplaceWideAlphaNumIntoOrdinaryAlphaNumInCurrentRegion(ByRef robjTopLeftRange As Excel.Range, _
        Optional ByVal vblnAllowToKeepFormulasInCells As Boolean = True, _
        Optional ByVal vobjExceptionKeywords As Collection = Nothing)

    ReplaceWideAlphaNumIntoOrdinaryAlphaNumInRange robjTopLeftRange.CurrentRegion, vblnAllowToKeepFormulasInCells, vobjExceptionKeywords
End Sub

'''
''' About Alphabet and figures
'''
Public Sub ReplaceWideAlphaNumIntoOrdinaryAlphaNumInRange(ByRef robjRange As Excel.Range, _
        Optional ByVal vblnAllowToKeepFormulasInCells As Boolean = True, _
        Optional ByVal vobjExceptionKeywords As Collection = Nothing)

    Dim varValues As Variant, blnIsAtLeastOneReplaced As Boolean, i As Long, j As Long
    
    With robjRange
    
        ReDim varValues(1 To .Rows.Count, 1 To .Columns.Count)
    
        If vblnAllowToKeepFormulasInCells Then
        
            varValues = robjRange.Formula
        Else
            varValues = robjRange.Value
        End If
        
        blnIsAtLeastOneReplaced = False
        
        For i = 1 To .Rows.Count
        
            For j = 1 To .Columns.Count
            
                If IsSomeWideAlphaNumConvertedToOrdinaryAlphaNumForVariant(varValues(i, j), vobjExceptionKeywords) Then
                
                    If Not blnIsAtLeastOneReplaced Then
                    
                        blnIsAtLeastOneReplaced = True
                    End If
                End If
            Next
        Next
    End With
    
    If blnIsAtLeastOneReplaced Then
    
        If vblnAllowToKeepFormulasInCells Then
        
            robjRange.Formula = varValues
        Else
            robjRange.Value = varValues
        End If
    End If
End Sub

'''
''' About parenthesis
'''
Public Sub ReplaceWideParenIntoOrdinaryParenInCurrentRegion(ByRef robjTopLeftRange As Excel.Range, _
        Optional ByVal vblnAllowToKeepFormulasInCells As Boolean = True)

    ReplaceWideParenIntoOrdinaryParenInRange robjTopLeftRange.CurrentRegion, vblnAllowToKeepFormulasInCells
End Sub

'''
''' About parenthesis
'''
Public Sub ReplaceWideParenIntoOrdinaryParenInRange(ByRef robjRange As Excel.Range, _
        Optional ByVal vblnAllowToKeepFormulasInCells As Boolean = True)

    Dim varValues As Variant, blnIsAtLeastOneReplaced As Boolean, i As Long, j As Long
    
    With robjRange
    
        ReDim varValues(1 To .Rows.Count, 1 To .Columns.Count)
    
        If vblnAllowToKeepFormulasInCells Then
        
            varValues = robjRange.Formula
        Else
            varValues = robjRange.Value
        End If
        
        blnIsAtLeastOneReplaced = False
        
        For i = 1 To .Rows.Count
        
            For j = 1 To .Columns.Count
            
                If IsSomeWideParenConvertedToOrdinaryParenForVariant(varValues(i, j)) Then
                
                    If Not blnIsAtLeastOneReplaced Then
                    
                        blnIsAtLeastOneReplaced = True
                    End If
                End If
            Next
        Next
    End With
    
    If blnIsAtLeastOneReplaced Then
    
        If vblnAllowToKeepFormulasInCells Then
        
            robjRange.Formula = varValues
        Else
            robjRange.Value = varValues
        End If
    End If
End Sub

'**---------------------------------------------
'** set automatic padding values on empty cells
'**---------------------------------------------
'''
''' pad empty values by each near above value for each column with considering all merged cells
'''
Public Sub PadSameAboveValuesOnEmptyCellsForEachColumnOnWorksheet(ByVal vobjSheet As Excel.Worksheet, ByVal vobjPaddingFieldTitles As Collection, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1, Optional ByVal vblnAllowToMergeCellsAfterPadding As Boolean = False, Optional ByVal vblnAllowToSetVerticalAlignmentTop As Boolean = False)

    Dim objTableRange As Excel.Range, objCell As Excel.Range, intStartRowIdx As Long, intEndRowIdx As Long, intRowIndex As Long, intColumnIndex As Long
    Dim objFieldTitleToIndexDic As Scripting.Dictionary, varFieldTitle As Variant
    
    Dim varCurrentCellValue As Variant, varCurrentAboveValue As Variant
    Dim blnIsInEmptyValueCell As Boolean
    
    Dim intStartEmptyRowIndex As Long, intEndEmptyRowIndex As Long, blnNeedToPad As Boolean, i As Long
    Dim blnAtLeastOnePadded As Boolean, objRangeStrToValueDic As Scripting.Dictionary, intSameValueTopEndRowIndex As Long, strColumnLetter As String
    
    
    Set objFieldTitleToIndexDic = GetFieldTitleToPositionIndexDic(vobjPaddingFieldTitles, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
    Set objTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
    If Not objFieldTitleToIndexDic Is Nothing And Not objTableRange Is Nothing Then
        
        If objTableRange.Rows.Count > 1 Then
        
            If vblnAllowToMergeCellsAfterPadding Then
            
                Set objRangeStrToValueDic = New Scripting.Dictionary
            End If
        
            With objFieldTitleToIndexDic
                
                For Each varFieldTitle In .Keys
                
                    intColumnIndex = .Item(varFieldTitle)   ' got relative column index
        
                    strColumnLetter = ConvertXlColumnIndexToLetter(intColumnIndex)  ' Relative column character has no problem, because use objTableRange.Range(<RelativeAddress>)
        
                    varCurrentAboveValue = ""
                    
                    blnNeedToPad = False
                    
                    blnAtLeastOnePadded = False
        
                    blnIsInEmptyValueCell = False
        
                    With objTableRange
                    
                        intStartRowIdx = 2: intEndRowIdx = .Rows.Count
                        
                        ReDim varColumnValues(1 To .Rows.Count, 1 To 1)
                        
                        varColumnValues = .Cells(1, intColumnIndex).Resize(.Rows.Count, 1).Value
                        
                        intRowIndex = intStartRowIdx
                        
                        Do While intRowIndex <= intEndRowIdx
                        
                            Set objCell = .Cells(intRowIndex, intColumnIndex)
                            
                            If IsEmpty(varColumnValues(intRowIndex, 1)) Then
                            
                                If Not blnIsInEmptyValueCell Then
                                
                                    ' Start to cache of cell-address
                                    
                                    intStartEmptyRowIndex = intRowIndex
                                End If
                            
                                blnIsInEmptyValueCell = True
                            
                            ElseIf IsNumeric(varColumnValues(intRowIndex, 1)) Then
                            
                                If blnIsInEmptyValueCell Then
                                
                                    ' Padding process
                                    
                                    intEndEmptyRowIndex = intRowIndex - 1
                                    
                                    blnNeedToPad = True
                                    
                                    If intRowIndex > 2 Then
                                    
                                        varCurrentAboveValue = mfvarGetCurrentAboveValue(intSameValueTopEndRowIndex, varColumnValues, objTableRange, intStartEmptyRowIndex, intColumnIndex)
                                    End If
                                End If
                            
                                blnIsInEmptyValueCell = False
                            
                            ElseIf TypeName(varColumnValues(intRowIndex, 1)) = "String" Then
                            
                                If varColumnValues(intRowIndex, 1) <> "" Then
                                
                                    If blnIsInEmptyValueCell Then
                                    
                                        ' Padding process
                                        
                                        intEndEmptyRowIndex = intRowIndex - 1
                                        
                                        blnNeedToPad = True
                                        
                                        If intRowIndex > 2 Then
                                        
                                            varCurrentAboveValue = mfvarGetCurrentAboveValue(intSameValueTopEndRowIndex, varColumnValues, objTableRange, intStartEmptyRowIndex, intColumnIndex)
                                        End If
                                    End If
                                
                                    blnIsInEmptyValueCell = False
                                Else
                                
                                    ' void string
                                    If Not blnIsInEmptyValueCell Then
                                
                                        ' Start to cache of cell-address
                                        
                                        intStartEmptyRowIndex = intRowIndex
                                    End If
                                
                                    blnIsInEmptyValueCell = True
                                End If
                            End If
                            
                            If blnNeedToPad Then
                            
                                Select Case True
                                
                                    Case IsNumeric(varCurrentAboveValue), varCurrentAboveValue <> ""
                            
                                        blnAtLeastOnePadded = True
                            
                                        For i = intStartEmptyRowIndex To intEndEmptyRowIndex
                                        
                                            varColumnValues(i, 1) = varCurrentAboveValue
                                        Next
                                        
                                        If vblnAllowToMergeCellsAfterPadding Then
                                        
                                            With objTableRange
                                            
                                                objRangeStrToValueDic.Add strColumnLetter & CStr(intSameValueTopEndRowIndex) & ":" & strColumnLetter & CStr(intEndEmptyRowIndex), varCurrentAboveValue
                                            End With
                                        End If
                                End Select
                                
                                intStartEmptyRowIndex = 0: intEndEmptyRowIndex = 0
                                
                                blnNeedToPad = False
                            End If
                            
                            
                            If objCell.MergeCells Then
                                                                                    
                                With objCell.MergeArea
                                
                                    intRowIndex = intRowIndex + .Rows.Count
                                End With
                            Else
                                
                                intRowIndex = intRowIndex + 1
                            End If
                        Loop
        
                        ' Final padding
                        If intStartEmptyRowIndex > 0 Then
                        
                            intEndEmptyRowIndex = intEndRowIdx
                            
                            If intRowIndex > 2 Then
                            
                                varCurrentAboveValue = mfvarGetCurrentAboveValue(intSameValueTopEndRowIndex, varColumnValues, objTableRange, intStartEmptyRowIndex, intColumnIndex)
                            End If
                        
                            Select Case True
                            
                                Case IsNumeric(varCurrentAboveValue), varCurrentAboveValue <> ""
                        
                                    blnAtLeastOnePadded = True
                                    
                                    For i = intStartEmptyRowIndex To intEndEmptyRowIndex
                                    
                                        varColumnValues(i, 1) = varCurrentAboveValue
                                    Next
                                    
                                    If vblnAllowToMergeCellsAfterPadding Then
                                    
                                        With objTableRange
                                        
                                            objRangeStrToValueDic.Add strColumnLetter & CStr(intSameValueTopEndRowIndex) & ":" & strColumnLetter & CStr(intEndEmptyRowIndex), varCurrentAboveValue
                                        End With
                                    End If
                            End Select
                        End If
                        
                        
                        If blnAtLeastOnePadded Then
                        
                            ' Update values
                            .Cells(1, intColumnIndex).Resize(.Rows.Count, 1).Value = varColumnValues
                            
                            If vblnAllowToMergeCellsAfterPadding Then
                            
                                If objRangeStrToValueDic.Count > 0 Then
                                
                                    msubMergeCellsSpecifiedRangeAdressKeyDic objRangeStrToValueDic, objTableRange, vblnAllowToSetVerticalAlignmentTop
                                    
                                    'If GetInputState() Then VBA.Interaction.DoEvents: VBA.Interaction.DoEvents: VBA.Interaction.DoEvents
                                End If
                            End If
                            
                        End If
                    End With
                Next
            End With
        End If
        
    End If
    
End Sub


'''
'''
'''
Private Function mfvarGetCurrentAboveValue(ByRef rintSameValueTopEndRowIndex As Long, ByRef rvarColumnValues As Variant, ByRef robjCurrentRegion As Excel.Range, ByVal vintRowIndex As Long, ByVal vintColumnIndex As Long) As Variant

    Dim objNearestAboveCell As Excel.Range, intValueExistedRowIndex As Long

    With robjCurrentRegion
    
        ' current empty value is rvarColumnValues(vintRowIndex, 1)
        
        ' current empty cell is .Cells(vintRowIndex, vintColumnIndex)
    
        Set objNearestAboveCell = .Cells.Item(vintRowIndex - 1, vintColumnIndex)
    
        With objNearestAboveCell
        
            If .MergeCells Then
            
                intValueExistedRowIndex = .MergeArea.Row - robjCurrentRegion.Row + 1
            Else
                intValueExistedRowIndex = vintRowIndex - 1
            End If
        End With
    
    End With

    rintSameValueTopEndRowIndex = intValueExistedRowIndex

    mfvarGetCurrentAboveValue = rvarColumnValues(intValueExistedRowIndex, 1)
End Function



'**---------------------------------------------
'** set and remove merged-cells for a table
'**---------------------------------------------
'''
''' merge continuous same value cells for the specified field-title columns
'''
Public Sub SetMergedCellsToTable(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitlesToSetMergedCells As Collection, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vblnAllowToSetVerticalAlignmentTop As Boolean = False)

    Dim objTableRange As Excel.Range
    Dim objFieldTitleToIndexDic As Scripting.Dictionary, varFieldTitle As Variant, intColumnIndex As Long
    Dim intRowIndex As Long
    Dim objCell As Excel.Range, intStartRowIdx As Long, intEndRowIdx As Long
    Dim varColumnValues() As Variant, strCellValue As String
    Dim strCurrentValue As String, strPreviousValue As String
    Dim intFirstRowIndex As Long, intLastRowIndex As Long, intContinuousCounter As Long, strColumnLetter As String
    Dim objRangeStrToValueDic As Scripting.Dictionary, objRangeToMerge As Excel.Range, varRangeStr As Variant, objCurrentRange As Excel.Range
    Dim blnOldDisplayAlerts As Boolean
    
    
    Set objFieldTitleToIndexDic = GetFieldTitleToPositionIndexDic(vobjFieldTitlesToSetMergedCells, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
    Set objTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
    If Not objFieldTitleToIndexDic Is Nothing And Not objTableRange Is Nothing Then
        
        If objTableRange.Rows.Count > 1 Then
        
            If GetInputState() Then VBA.Interaction.DoEvents: VBA.Interaction.DoEvents: VBA.Interaction.DoEvents
        
            Set objRangeStrToValueDic = New Scripting.Dictionary
                
            With objFieldTitleToIndexDic
                
                For Each varFieldTitle In .Keys
                
                    intColumnIndex = .Item(varFieldTitle)    ' This column index i is a relative value based on CurrentRegion
                    
                    objRangeStrToValueDic.RemoveAll
                    
                    With objTableRange
                    
                        intStartRowIdx = 2: intEndRowIdx = .Rows.Count
                        
                        ReDim varColumnValues(1 To .Rows.Count, 1 To 1)
                        
                        varColumnValues = .Cells(1, intColumnIndex).Resize(.Rows.Count, 1).Value
                        
                        intFirstRowIndex = 0: strCurrentValue = "": strPreviousValue = ""
                        
                        strColumnLetter = ConvertXlColumnIndexToLetter(intColumnIndex)
                        
                        intRowIndex = 2
                        
                        Do While intRowIndex <= .Rows.Count
                        
                            Set objCurrentRange = .Cells(intRowIndex, intColumnIndex)
                        
                            If objCurrentRange.MergeCells Then
                            
                                msubCellValueContinuingAndIdentifyingRangeToMerge varColumnValues(intRowIndex, 1), intRowIndex, strColumnLetter, strCurrentValue, strPreviousValue, intContinuousCounter, intFirstRowIndex, intLastRowIndex, objRangeStrToValueDic
                                
                                intRowIndex = intRowIndex + .Rows.Count
                            Else
                                msubCellValueContinuingAndIdentifyingRangeToMerge varColumnValues(intRowIndex, 1), intRowIndex, strColumnLetter, strCurrentValue, strPreviousValue, intContinuousCounter, intFirstRowIndex, intLastRowIndex, objRangeStrToValueDic
                                
                                intRowIndex = intRowIndex + 1
                            End If
                        Loop
                        
                        If strCurrentValue <> "" And intContinuousCounter > 1 Then
                        
                            intLastRowIndex = intEndRowIdx
                            
                            objRangeStrToValueDic.Add strColumnLetter & CStr(intFirstRowIndex) & ":" & strColumnLetter & CStr(intLastRowIndex), strCurrentValue
                        End If
                        
                    End With
                
                    If objRangeStrToValueDic.Count > 0 Then
                        
                        msubMergeCellsSpecifiedRangeAdressKeyDic objRangeStrToValueDic, objTableRange, vblnAllowToSetVerticalAlignmentTop
                    End If
                Next
            End With
        End If
    End If
    
End Sub


'''
'''
'''
Private Sub msubCellValueContinuingAndIdentifyingRangeToMerge(ByRef rvarCurrentValue As Variant, _
        ByRef rintRowIndex As Long, _
        ByRef rstrColumnLetter As String, _
        ByRef rstrCurrentValue As String, _
        ByRef rstrPreviousValue As String, _
        ByRef rintContinuousCounter As Long, _
        ByRef rintFirstRowIndex As Long, _
        ByRef rintLastRowIndex As Long, _
        ByRef robjRangeStrToValueDic As Scripting.Dictionary)


    Dim strCellValue As String
    

    If Not IsEmpty(rvarCurrentValue) Then  ' rvarCurrentValue = varValues(rintRowIndex, 1)
                        
        strCellValue = CStr(rvarCurrentValue)
    Else
        strCellValue = ""
    End If

    rstrCurrentValue = strCellValue
    
    If rstrCurrentValue <> rstrPreviousValue Then
    
        If rstrPreviousValue <> "" And rintContinuousCounter > 1 Then
            ' continuous not-null value
            
            rintLastRowIndex = rintRowIndex - 1
            
            robjRangeStrToValueDic.Add rstrColumnLetter & CStr(rintFirstRowIndex) & ":" & rstrColumnLetter & CStr(rintLastRowIndex), rstrPreviousValue
        End If
        
        If rstrCurrentValue <> "" Then
        
            rintFirstRowIndex = rintRowIndex
        End If
    
        rintContinuousCounter = 1
    Else
        If rstrCurrentValue <> "" Then
            
            rintContinuousCounter = rintContinuousCounter + 1
        Else
            rintContinuousCounter = 1
        End If
    End If
    
    rstrPreviousValue = rstrCurrentValue
End Sub



'''
'''
'''
Private Sub msubMergeCellsSpecifiedRangeAdressKeyDic(ByRef robjRangeStrToValueDic As Scripting.Dictionary, _
        ByRef robjTableRange As Excel.Range, _
        Optional ByVal vblnAllowToSetVerticalAlignmentTop As Boolean = False)

    Dim blnOldDisplayAlerts As Boolean, varRangeStr As Variant, objRangeToMerge As Excel.Range

    On Error Resume Next

    'DebugDic objRangeStrToValueDic

    With robjTableRange
    
        blnOldDisplayAlerts = .Application.DisplayAlerts
        
        .Application.DisplayAlerts = False
        
        For Each varRangeStr In robjRangeStrToValueDic.Keys
            
            Set objRangeToMerge = .Range(varRangeStr)
    
            With objRangeToMerge
            
                .Merge
            
                If vblnAllowToSetVerticalAlignmentTop Then
                    
                    .VerticalAlignment = xlTop
                End If
            End With
        Next
        
        .Application.DisplayAlerts = blnOldDisplayAlerts
    End With
    
    On Error GoTo 0
End Sub


'''
'''
'''
Public Sub SanityTestToGetCellXlBorderWeight()

    Dim objRange As Excel.Range
    
    Set objRange = ActiveCell
    
    Debug.Print GetCellBorderWeightTxtFromEnm(objRange.Borders(xlEdgeTop).Weight) & ", " & GetCellBorderWeightTxtFromEnm(objRange.Borders(xlEdgeBottom).Weight)
End Sub

'''
'''
'''
Public Sub SetMergedCellsWithBorderWeightConditionToOneColumnRange(ByRef robjOneColumnRange As Excel.Range, _
        ByVal vobjXlBorderIndexToXlBorderWeightConditionDic As Scripting.Dictionary, _
        Optional ByVal vblnAllowToSetVerticalAlignmentTop As Boolean = False)

    
    Dim objRangeStrToValueDic As Scripting.Dictionary, varValues As Variant, intRowIndex As Long
    Dim strCellValue As String, strCurrentValue As String, strPreviousValue As String
    Dim intFirstRowIndex As Long, intLastRowIndex As Long, intContinuousCounter As Long, strColumnLetter As String
    Dim objCurrentRange As Excel.Range
    Dim i As Long
    

    If robjOneColumnRange.Rows.Count > 1 Then
    
        If GetInputState() Then VBA.Interaction.DoEvents: VBA.Interaction.DoEvents: VBA.Interaction.DoEvents
    
        Set objRangeStrToValueDic = New Scripting.Dictionary
       
        With robjOneColumnRange
        
            ReDim varValues(1 To .Rows.Count, 1 To 1)
            
            varValues = .Worksheet.Range(.Cells(1, 1), .Cells(.Rows.Count, 1)).Value
            
            intFirstRowIndex = 0: strCurrentValue = "": strPreviousValue = ""
            
            strColumnLetter = "A"
            
            intRowIndex = 1
            
            Do While intRowIndex <= .Rows.Count
                
                Set objCurrentRange = .Cells(intRowIndex, 1)
                
                If objCurrentRange.MergeCells Then
                
                    With objCurrentRange.MergeArea
                    
                        msubCellValueContinuingAndIdentifyingRangeToMergeWithCellBorderWeightConditions varValues(intRowIndex, 1), intRowIndex, strColumnLetter, strCurrentValue, strPreviousValue, objCurrentRange.MergeArea, intContinuousCounter, intFirstRowIndex, intLastRowIndex, objRangeStrToValueDic, vobjXlBorderIndexToXlBorderWeightConditionDic
                        
                        intRowIndex = intRowIndex + .Rows.Count
                    End With
                Else
                    msubCellValueContinuingAndIdentifyingRangeToMergeWithCellBorderWeightConditions varValues(intRowIndex, 1), intRowIndex, strColumnLetter, strCurrentValue, strPreviousValue, objCurrentRange, intContinuousCounter, intFirstRowIndex, intLastRowIndex, objRangeStrToValueDic, vobjXlBorderIndexToXlBorderWeightConditionDic
                    
                    intRowIndex = intRowIndex + 1
                End If
            Loop
            
            If strCurrentValue <> "" And intContinuousCounter > 1 Then
            
                intLastRowIndex = robjOneColumnRange.Rows.Count
                
                objRangeStrToValueDic.Add strColumnLetter & CStr(intFirstRowIndex) & ":" & strColumnLetter & CStr(intLastRowIndex), strCurrentValue
            End If
            
        End With
    
        If objRangeStrToValueDic.Count > 0 Then
            
            msubMergeCellsSpecifiedRangeAdressKeyDic objRangeStrToValueDic, robjOneColumnRange, vblnAllowToSetVerticalAlignmentTop
        End If
    End If
End Sub

'''
'''
'''
Private Sub msubCellValueContinuingAndIdentifyingRangeToMergeWithCellBorderWeightConditions(ByRef rvarCurrentValue As Variant, _
        ByRef rintRowIndex As Long, _
        ByRef rstrColumnLetter As String, _
        ByRef rstrCurrentValue As String, _
        ByRef rstrPreviousValue As String, _
        ByRef robjCurrentRange As Excel.Range, _
        ByRef rintContinuousCounter As Long, _
        ByRef rintFirstRowIndex As Long, _
        ByRef rintLastRowIndex As Long, _
        ByRef robjRangeStrToValueDic As Scripting.Dictionary, _
        ByRef robjXlBorderIndexToXlBorderWeightConditionDic As Scripting.Dictionary)


    Dim strCellValue As String
    

    If Not IsEmpty(rvarCurrentValue) Then  ' rvarCurrentValue = varValues(rintRowIndex, 1)
                        
        strCellValue = CStr(rvarCurrentValue)
    Else
        strCellValue = ""
    End If

    rstrCurrentValue = strCellValue
    
    If rstrCurrentValue <> rstrPreviousValue Or mfblnIsMatchedBorderIndexToWeightConditionDic(robjCurrentRange, robjXlBorderIndexToXlBorderWeightConditionDic) Then
    
        If rstrPreviousValue <> "" And rintContinuousCounter > 1 Then
            ' continuous not-null value
            
            rintLastRowIndex = rintRowIndex - 1
            
            robjRangeStrToValueDic.Add rstrColumnLetter & CStr(rintFirstRowIndex) & ":" & rstrColumnLetter & CStr(rintLastRowIndex), rstrPreviousValue
        End If
        
        If rstrCurrentValue <> "" Then
        
            rintFirstRowIndex = rintRowIndex
        End If
    
        rintContinuousCounter = 1
    Else
        If rstrCurrentValue <> "" Then
            
            rintContinuousCounter = rintContinuousCounter + 1
        Else
            rintContinuousCounter = 1
        End If
    End If
    
    rstrPreviousValue = rstrCurrentValue
End Sub


'''
'''
'''
Private Function mfblnIsMatchedBorderIndexToWeightConditionDic(ByRef robjRange As Excel.Range, _
        ByRef robjXlBorderIndexToXlBorderWeightConditionDic As Scripting.Dictionary) As Boolean

    Dim varXlBorderIndex As Variant, enmXlBorderIndex As Excel.XlBordersIndex
    Dim enmXlBorderWeight As Excel.XlBorderWeight

    Dim blnIsAtLeastOneConditionMatched As Boolean

    blnIsAtLeastOneConditionMatched = False

    With robjXlBorderIndexToXlBorderWeightConditionDic
    
        For Each varXlBorderIndex In .Keys
        
            enmXlBorderIndex = varXlBorderIndex
        
            enmXlBorderWeight = .Item(varXlBorderIndex)
        
            If enmXlBorderWeight = robjRange.Borders.Item(enmXlBorderIndex).Weight Then
            
                blnIsAtLeastOneConditionMatched = True
                
                Exit For
            End If
        Next
    End With
    
    mfblnIsMatchedBorderIndexToWeightConditionDic = blnIsAtLeastOneConditionMatched
End Function




'''
''' remove merged-cell and input same values to the removed range
'''
Public Sub RemoveMergedCellsToTable(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitlesToSetMergedCells As Collection, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)


    Dim objTableRange As Excel.Range
    Dim objFieldTitleToIndexDic As Scripting.Dictionary, varFieldTitle As Variant, intColumnIndex As Long, intRowIndex As Long
    Dim objCell As Excel.Range, intStartRowIdx As Long, intEndRowIdx As Long
    Dim varColumnValues() As Variant, strCellValue As String
    Dim varInputValues() As Variant, objMergedCells As Excel.Range, strMergedCellsAddress As String
    Dim i As Long
    
    Dim blnOldDisplayAlerts As Boolean
    
    
    Set objFieldTitleToIndexDic = GetFieldTitleToPositionIndexDic(vobjFieldTitlesToSetMergedCells, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
    Set objTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
        
    blnOldDisplayAlerts = vobjSheet.Application.DisplayAlerts
            
    vobjSheet.Application.DisplayAlerts = False

    With objFieldTitleToIndexDic
    
        For Each varFieldTitle In .Keys
        
            intColumnIndex = .Item(varFieldTitle)
            
            With objTableRange
            
                intStartRowIdx = 2: intEndRowIdx = .Rows.Count
                
                ReDim varColumnValues(1 To .Rows.Count, 1 To 1)
                
                varColumnValues = .Cells(1, intColumnIndex).Resize(.Rows.Count, 1).Value
                
                intRowIndex = intStartRowIdx
                
                Do While intRowIndex <= intEndRowIdx
                
                    Set objCell = .Cells(intRowIndex, intColumnIndex)
                    
                    If objCell.MergeCells Then
                    
                        strCellValue = varColumnValues(intRowIndex, 1)
                        
                        Set objMergedCells = objCell.MergeArea
                                                
                        With objMergedCells
                        
                            intRowIndex = intRowIndex + .Rows.Count
                        
                            strMergedCellsAddress = .Address
                        
                            ReDim varInputValues(1 To .Rows.Count, 1 To 1)
                        
                            For i = 1 To .Rows.Count
                            
                                varInputValues(i, 1) = strCellValue
                            Next
                        
                            .UnMerge
                        End With
                        
                        vobjSheet.Range(strMergedCellsAddress).Value = varInputValues
                    Else
                        intRowIndex = intRowIndex + 1
                    End If
                Loop
            End With
        Next
    End With
    
    vobjSheet.Application.DisplayAlerts = blnOldDisplayAlerts
End Sub



'''
'''
'''
Public Function IsAtLeastOneMergedCellIncluded(ByVal vobjSheet As Excel.Worksheet, ByVal vobjFieldTitlesToSetMergedCells As Collection, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1) As Boolean

    Dim objTableRange As Excel.Range
    Dim objFieldTitleToIndexDic As Scripting.Dictionary, varFieldTitle As Variant, intColumnIndex As Long, intRowIndex As Long
    Dim objCell As Excel.Range, intStartRowIdx As Long, intEndRowIdx As Long
    Dim varColumnValues() As Variant, strCellValue As String
    Dim varInputValues() As Variant, objMergedCells As Excel.Range, strMergedCellsAddress As String
    Dim i As Long
    
    Dim blnIsAtLeastOneMergedCellFound As Boolean
    
    
    blnIsAtLeastOneMergedCellFound = False
    
    Set objFieldTitleToIndexDic = GetFieldTitleToPositionIndexDic(vobjFieldTitlesToSetMergedCells, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
    Set objTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
        
    With objFieldTitleToIndexDic
    
        For Each varFieldTitle In .Keys
        
            intColumnIndex = .Item(varFieldTitle)
            
            With objTableRange
            
                intStartRowIdx = 2: intEndRowIdx = .Rows.Count
                
                intRowIndex = intStartRowIdx
                
                Do While intRowIndex <= intEndRowIdx
                
                    Set objCell = .Cells(intRowIndex, intColumnIndex)
                    
                    If objCell.MergeCells Then
                    
                        With objCell.MergeArea
                        
                            intRowIndex = intRowIndex + .Rows.Count
                        End With
                        
                        blnIsAtLeastOneMergedCellFound = True
                        
                        Exit Do
                    Else
                        intRowIndex = intRowIndex + 1
                    End If
                Loop
            End With
            
            If blnIsAtLeastOneMergedCellFound Then
            
                Exit For
            End If
        Next
    End With

    IsAtLeastOneMergedCellIncluded = blnIsAtLeastOneMergedCellFound
End Function


