Attribute VB_Name = "MakeHyperLinksModule"
'
'   create and control hyper-link string for each specified Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub AddHyperLinkFromLinkSettingColumnAndUNCPathColumn(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vstrLinkSettingColumnName As String, _
        ByVal vstrUNCPathColumnName As String, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vsngFontSize As Single = 10)


    Dim intLinkSettingColumnIdx As Long, intUNCPathColumnIdx As Long
    
    Dim objFieldTitleToPositionIndexDic As Scripting.Dictionary, objSearchingFieldTitles As Collection
    
    Dim varLinkSettingColumn() As Variant, varUNCPathColumn() As Variant
    
    Dim intRowIdx As Long, strLinkSettingValue As String, strUNCPathValue As String
    
    Dim objRange As Excel.Range, strCurrentCellValue As String, objHyperlink As Excel.Hyperlink, objLinksRange As Excel.Range
    
    
    Set objSearchingFieldTitles = New Collection
    
    With objSearchingFieldTitles
    
        .Add vstrLinkSettingColumnName
        
        .Add vstrUNCPathColumnName
    End With
    
    Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(objSearchingFieldTitles, _
            vobjSheet, _
            vintFieldTitlesRowIndex, _
            vintTopLeftColumnIndex)

    intLinkSettingColumnIdx = objFieldTitleToPositionIndexDic.Item(vstrLinkSettingColumnName)
    
    intUNCPathColumnIdx = objFieldTitleToPositionIndexDic.Item(vstrUNCPathColumnName)


    With vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
        If .Rows.Count >= 2 Then
    
            ReDim varLinkSettingColumn(1 To .Rows.Count, 1 To 1)
        
            ReDim varUNCPathColumn(1 To .Rows.Count, 1 To 1)
        
            varLinkSettingColumn = .Worksheet.Range(.Cells(1, intLinkSettingColumnIdx), .Cells(.Rows.Count, intLinkSettingColumnIdx)).Value
        
            varUNCPathColumn = .Worksheet.Range(.Cells(1, intUNCPathColumnIdx), .Cells(.Rows.Count, intUNCPathColumnIdx)).Value
        
            For intRowIdx = 2 To .Rows.Count
        
                strLinkSettingValue = varLinkSettingColumn(intRowIdx, 1)
        
                strUNCPathValue = varUNCPathColumn(intRowIdx, 1)
            
                Set objRange = .Cells(intRowIdx, intLinkSettingColumnIdx)
                
                With objRange
                
                    strCurrentCellValue = CStr(objRange.Value)
                
                    If .Hyperlinks.Count = 0 Then
                    
                        If strCurrentCellValue <> "" Then
                        
                            .Hyperlinks.Add Anchor:=objRange, Address:=strUNCPathValue, TextToDisplay:=strCurrentCellValue
                        Else
                            .Hyperlinks.Add Anchor:=objRange, Address:=strUNCPathValue, TextToDisplay:=strUNCPathValue
                        End If
                    Else
                        Set objHyperlink = .Hyperlinks.Item(1)
                        
                        With objHyperlink
                        
                            If strCurrentCellValue <> "" Then
                            
                                .Address = strUNCPathValue
                                
                                .TextToDisplay = strCurrentCellValue
                            Else
                                .Address = strUNCPathValue
                                
                                .TextToDisplay = strUNCPathValue
                            End If
                        End With
                    End If
                End With
            Next
    
            Set objLinksRange = .Worksheet.Range(.Cells(2, intLinkSettingColumnIdx), .Cells(.Rows.Count, intLinkSettingColumnIdx))
            
            With objLinksRange
            
                With .Font
                
                    .Size = vsngFontSize
                End With
            End With
        End If
    End With
End Sub

'''
'''
'''
Public Sub AddHyperLinkFromKeyColumnAndLinkColumn(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vstrKeyColumnName As String, _
        ByVal vobjKeyToLinkDic As Scripting.Dictionary, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vsngFontSize As Single = 10)


    Dim intKeyColumnIdx As Long
    
    Dim objFieldTitleToPositionIndexDic As Scripting.Dictionary, objSearchingFieldTitles As Collection
    
    Dim varKeyColumn() As Variant, intRowIdx As Long, strKeyValue As String, strLinkValue As String
    
    Dim objRange As Excel.Range, strCurrentCellValue As String, objHyperlink As Excel.Hyperlink, objLinksRange As Excel.Range
    
    
    Set objSearchingFieldTitles = New Collection
    
    With objSearchingFieldTitles
    
        .Add vstrKeyColumnName
    End With
    
    Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(objSearchingFieldTitles, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)

    intKeyColumnIdx = objFieldTitleToPositionIndexDic.Item(vstrKeyColumnName)
    

    With vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
        If .Rows.Count >= 2 Then
    
            ReDim varKeyColumn(1 To .Rows.Count, 1 To 1)
        
            varKeyColumn = .Worksheet.Range(.Cells(1, intKeyColumnIdx), .Cells(.Rows.Count, intKeyColumnIdx)).Value
        
            For intRowIdx = 2 To .Rows.Count
        
                strKeyValue = varKeyColumn(intRowIdx, 1)
        
                If vobjKeyToLinkDic.Exists(strKeyValue) Then
                
                    strLinkValue = vobjKeyToLinkDic.Item(strKeyValue)
                
                    Set objRange = .Cells(intRowIdx, intKeyColumnIdx)
                    
                    With objRange
                    
                        strCurrentCellValue = CStr(objRange.Value)
                    
                        If .Hyperlinks.Count = 0 Then
                        
                            If strCurrentCellValue <> "" Then
                            
                                .Hyperlinks.Add Anchor:=objRange, Address:=strLinkValue, TextToDisplay:=strCurrentCellValue
                            Else
                                .Hyperlinks.Add Anchor:=objRange, Address:=strLinkValue, TextToDisplay:=strLinkValue
                            End If
                        Else
                            Set objHyperlink = .Hyperlinks.Item(1)
                            
                            With objHyperlink
                            
                                If strCurrentCellValue <> "" Then
                                
                                    .Address = strLinkValue
                                    
                                    .TextToDisplay = strCurrentCellValue
                                Else
                                    .Address = strLinkValue
                                    
                                    .TextToDisplay = strLinkValue
                                End If
                            End With
                        End If
                    End With
                End If
            Next
    
            Set objLinksRange = .Worksheet.Range(.Cells(2, intKeyColumnIdx), .Cells(.Rows.Count, intKeyColumnIdx))
            
            With objLinksRange
            
                With .Font
                
                    .Size = vsngFontSize
                End With
            End With
        End If
    End With
End Sub

'''
'''
'''
Public Sub AddHyperLinkAboutUNCPathColumn(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vstrUNCPathColumnName As String, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vsngFontSize As Single = 10)


    Dim intUNCPathColumnIdx As Long, varUNCPathColumn() As Variant, strUNCPath As String
    
    Dim objFieldTitleToPositionIndexDic As Scripting.Dictionary
    
    Dim intRowIdx As Long, objRange As Excel.Range
    
    Dim objHyperlink As Excel.Hyperlink, objLinksRange As Excel.Range


    Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(GetColFromLineDelimitedChar(vstrUNCPathColumnName), _
            vobjSheet, _
            vintFieldTitlesRowIndex, _
            vintTopLeftColumnIndex)

    If Not objFieldTitleToPositionIndexDic Is Nothing Then
    
        If objFieldTitleToPositionIndexDic.Exists(vstrUNCPathColumnName) Then
        
            intUNCPathColumnIdx = objFieldTitleToPositionIndexDic.Item(vstrUNCPathColumnName)
        
            With vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
            
                If .Rows.Count >= 2 Then
            
                    ReDim varUNCPathColumn(1 To .Rows.Count, 1 To 1)
                
                    varUNCPathColumn = .Worksheet.Range(.Cells(1, intUNCPathColumnIdx), .Cells(.Rows.Count, intUNCPathColumnIdx)).Value
                
                    For intRowIdx = 2 To .Rows.Count
                
                        strUNCPath = varUNCPathColumn(intRowIdx, 1)
                        
                        If strUNCPath <> "" Then
                        
                            Set objRange = .Cells(intRowIdx, intUNCPathColumnIdx)
                            
                            With objRange
                            
                                If .Hyperlinks.Count = 0 Then
                                
                                    .Hyperlinks.Add Anchor:=objRange, Address:=strUNCPath, TextToDisplay:=strUNCPath
                                Else
                                    Set objHyperlink = .Hyperlinks.Item(1)
                                    
                                    With objHyperlink
                                    
                                        .Address = strUNCPath
                                        
                                        .TextToDisplay = strUNCPath
                                    End With
                                End If
                            End With
                        End If
                    Next
                    
                    Set objLinksRange = .Worksheet.Range(.Cells(2, intUNCPathColumnIdx), .Cells(.Rows.Count, intUNCPathColumnIdx))
                    
                    With objLinksRange
                    
                        With .Font
                        
                            .Size = vsngFontSize
                        End With
                    End With
                End If
            End With
        End If
    End If
End Sub
