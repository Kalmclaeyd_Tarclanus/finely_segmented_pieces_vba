Attribute VB_Name = "UTfMakeHyperLinksModule"
'
'   Sanity test to create and control hyper-link string for each specified Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 23/Aug/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToUTfMakeHyperLinksModule()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", _
            "MakeHyperLinksModule"
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToAddHyperLinkFromLinkSettingColumnAndUNCPathColumn()

    Dim objDTCol As Collection, objSheet As Excel.Worksheet
    
    Dim strBookPath As String, strOutputPath As String, strInputDir As String
    
    
    strInputDir = GetRepositoryCodesRootDir()
    
    Set objDTCol = GetAnyTypeFileListsColByVbaDir(strInputDir, _
            "FileName,FullPath", _
            "*.cls")
            
    strOutputPath = mfstrGetOutputTempDir() & "\HyperLinkSettingTest01.xlsx"

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputPath, "TestHyperlinks")
    
    OutputColToSheet objSheet, _
            objDTCol, _
            GetColFromLineDelimitedChar("FileName,FullPath"), _
            mfobjGetDTFormatter()
    

    AddHyperLinkFromLinkSettingColumnAndUNCPathColumn objSheet, _
            "FileName", _
            "FullPath"
End Sub


'''
'''
'''
Private Sub msubSanityTestToAddHyperLinkFromKeyColumnAndLinkColumn()

    Dim objDTCol As Collection, objSheet As Excel.Worksheet
    
    Dim strBookPath As String, strOutputPath As String, strInputDir As String
    
    Dim objKeyToLinkDic As Scripting.Dictionary
    
    
    strInputDir = GetRepositoryCodesRootDir()
    
    Set objDTCol = GetAnyTypeFileListsColByVbaDir(strInputDir, _
            "RelativeFullPath", _
            "*.cls,*.bas")
    
    Set objKeyToLinkDic = GetAnyTypeFileInfoDicByVbaDir(strInputDir, _
            "RelativeFullPath,FullPath", _
            "*.cls")
    
    strOutputPath = mfstrGetOutputTempDir() & "\HyperLinkSettingTest02.xlsx"

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputPath, "TestHyperlinks")
    
    OutputColToSheet objSheet, _
            objDTCol, _
            GetColFromLineDelimitedChar("RelativeFullPath"), _
            mfobjGetDTFormatter()
    

    AddHyperLinkFromKeyColumnAndLinkColumn objSheet, _
            "RelativeFullPath", _
            objKeyToLinkDic
End Sub


'''
'''
'''
Private Sub msubSanityTestToAddHyperLinkAboutUNCPathColumn()

    Dim objDTCol As Collection, objSheet As Excel.Worksheet
    
    Dim strBookPath As String, strOutputPath As String, strInputDir As String
    
    
    strInputDir = GetRepositoryCodesRootDir()
    
    Set objDTCol = GetAnyTypeFileListsColByVbaDir(strInputDir, _
            "FullPath", _
            "*.cls,*.bas")
    
    
    strOutputPath = mfstrGetOutputTempDir() & "\HyperLinkSettingTest03.xlsx"

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputPath, "TestHyperlinks")
    
    OutputColToSheet objSheet, _
            objDTCol, _
            GetColFromLineDelimitedChar("FullPath"), _
            mfobjGetDTFormatter()

    AddHyperLinkAboutUNCPathColumn objSheet, _
            "FullPath"
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetDTFormatter() As DataTableSheetFormatter

    Dim objDTFormatter As DataTableSheetFormatter
    
    
    Set objDTFormatter = New DataTableSheetFormatter
    
    With objDTFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
    
        .RecordBordersType = RecordCellsGrayAll
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FileName,28,FullPath,87,RelativeFullPath,65")
    End With

    Set mfobjGetDTFormatter = objDTFormatter
End Function



'''
'''
'''
Private Function mfstrGetOutputTempDir() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\TestToMakeHyperLinks"
    
    If DirectoryExistsByVbaDir(strDir) Then
    
        ForceToCreateDirectoryByVbaDir strDir
    End If

    mfstrGetOutputTempDir = strDir
End Function


