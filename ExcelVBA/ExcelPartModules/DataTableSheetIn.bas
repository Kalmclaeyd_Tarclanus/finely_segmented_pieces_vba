Attribute VB_Name = "DataTableSheetIn"
'
'   get data-table from Excel Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon,  6/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////


'''
''' Function Output Object type
'''
Public Enum VariantOutputDataTableType
    
    OutputAsCollectionObject
    
    OutputAsDictionaryObject
    
    OutputAsVariantArray
    
    OutputAsExcelWorksheetObject
End Enum

'''
''' INCERT INTO , generate literals of Values
'''
Public Enum DataLiteralFormatType
    
    RawLiteral  ' Integer, Real number, or Imaginary number
    
    QuoteSurrounded ' String
    
    DateTimeGeneralLiteral  ' date and time
    
    DateOnlyLiteral ' date
    
    TimeOnlyLiteral ' time
End Enum



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Get dictionary from a expanded key-values of Cells on Worksheet
'**---------------------------------------------
'''
''' This doesn't support the case that the values are collection or some object type
'''
Public Function GetDictionaryFromExpandedKeyValuesWithoutHeaderSimply(ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmDicSheetExpandDirection As DicSheetExpandDirection = DicSheetExpandDirection.DicExpandingKyesColumnsValuesColumnsAsTable) As Scripting.Dictionary


    Dim objDic As Scripting.Dictionary, intRowsCount As Long, intColumnsCount As Long
    Dim varValues As Variant, i As Long, j As Long
    
    With vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).CurrentRegion
    
        intRowsCount = .Rows.Count
    
        intColumnsCount = .Columns.Count
        
        ReDim varValues(1 To intRowsCount, 1 To intColumnsCount)
        
        varValues = .Value
    End With

    
    Set objDic = New Scripting.Dictionary
    
    If venmDicSheetExpandDirection = DicExpandingKyesColumnsValuesColumnsAsTable Then
    
        For i = 1 To intRowsCount
        
            objDic.Add varValues(i, 1), varValues(i, 2)
        Next
    Else
        For j = 1 To intColumnsCount
        
            objDic.Add varValues(1, j), varValues(2, j)
        Next
    End If

    Set GetDictionaryFromExpandedKeyValuesWithoutHeaderSimply = objDic
End Function


'**---------------------------------------------
'** Reading and test outputting
'**---------------------------------------------
'''
''' clear previous all sheets before create new read table
'''
Public Sub OutputReadTableFromActiveSheetToTemporarySheet()


    OutputTableFromActiveSheetToTemporarySheetWithCreatingBook False
End Sub

'''
'''
'''
Public Sub AppendReadTableFromActiveSheetToTemporarySheet()


    OutputTableFromActiveSheetToTemporarySheetWithCreatingBook True
End Sub

'''
''' get Selection to adding values collection VBA codes
'''
Public Sub GetAddingVBACodeIntoImmediateWindowFromSelection()

    Dim objRange As Excel.Range, objSelectedRange As Excel.Range, strAddCode As String
    
    Set objSelectedRange = Selection
    
    
    strAddCode = ""
    
    For Each objRange In objSelectedRange
    
        If strAddCode <> "" Then
        
            strAddCode = strAddCode & ","
        End If
    
        strAddCode = strAddCode & ".Add "
    
        If IsEmpty(objRange.Value) Then
    
            strAddCode = strAddCode & ""
        Else
        
            strAddCode = strAddCode & """" & CStr(objRange.Value) & """"
        End If
    Next
    
    Debug.Print "You can the following VBA code by copying and doing paste:"
    
    Debug.Print strAddCode

End Sub


'''
''' get Range string into the immediate-window of ActiveCell
'''
Public Sub GetRangeIndexesFromActiveCell()

    Dim objRange As Excel.Range, objSheet As Excel.Worksheet, objBook As Excel.Workbook
    
    Set objRange = ActiveCell
    
    
    If Not objRange Is Nothing Then
    
        With objRange
        
            Set objSheet = .Worksheet: Set objBook = objSheet.Parent
        
            Debug.Print "About [" & objBook.Name & "] book, [" & .Worksheet.Name & "] sheet," & vbNewLine & vbTab & "column index: " & ConvertXlColumnIndexToLetter(.Column) & " ( " & CStr(.Column) & " ), row index: " & CStr(.Row)
        End With
    End If
End Sub


'''
'''
'''
Public Sub OutputTableFromActiveSheetToTemporarySheetWithCreatingBook(Optional ByVal vblnAllowToAppendNewSheet As Boolean = True)

    Dim objSelectedRange As Excel.Range, objCurrentRegion As Excel.Range, objInputSheet As Excel.Worksheet, objInputBook As Excel.Workbook
    
    Dim strOutputDir As String, strOutputBookPath As String, objOutputSheet As Excel.Worksheet
    Dim varRCValues As Variant, objFieldTitlesCol As Collection
    
    Set objSelectedRange = ActiveCell
    
    Set objCurrentRegion = objSelectedRange.CurrentRegion
    
    Set objInputSheet = objSelectedRange.Worksheet: Set objInputBook = objInputSheet.Parent



    strOutputDir = GetTemporaryOutputBookDirWithConsideringRemovableDiskFromBook(objInputBook)

    ForceToCreateDirectory strOutputDir
    
    
    
    strOutputBookPath = strOutputDir & "\TemporaryReadDataTable.xlsx"

    If vblnAllowToAppendNewSheet Then

        Set objInputBook = GetWorkbook(strOutputBookPath, True)
        
        Set objOutputSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objInputBook)
    
        objOutputSheet.Name = FindNewSheetNameWhenAlreadyExist(Left("ReadFrom_" & objInputSheet.Name, 31), objInputBook)
        
        DeleteDummySheetWhenItExists objInputBook
    Else

        Set objOutputSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, Left("ReadFrom_" & objInputSheet.Name, 31))
    End If
    
    
    GetRCVariantTableFromCurrentRegion varRCValues, objFieldTitlesCol, objCurrentRegion

    OutputVarTableToSheet objOutputSheet, varRCValues, objFieldTitlesCol, GetDataTableSheetFormatterFromFieldTitlesBySameColumnWidth(objFieldTitlesCol, 12, ColumnNameInteriorOfExcelSheetAdoSQL)

    AddAutoShapeGeneralMessage objOutputSheet, mfstrLogOfReadingExcelSheetDataTable(objCurrentRegion)
End Sub


Private Function mfstrLogOfReadingExcelSheetDataTable(ByRef robjInputCurrentRegion As Excel.Range) As String

    Dim strLog As String, objInputSheet As Excel.Worksheet, objInputBook As Excel.Workbook
    Dim strTopLeftCellRange As String

    With robjInputCurrentRegion
    
        Set objInputSheet = .Worksheet: Set objInputBook = objInputSheet.Parent
    
        strTopLeftCellRange = ConvertXlColumnIndexToLetter(.Column) & CStr(.Row)
    End With
    
    strLog = "Temporary read results" & vbNewLine
    
    strLog = strLog & "Read Excel book path: " & objInputBook.FullName & vbNewLine

    strLog = strLog & "Read sheet name: " & objInputSheet.Name & vbNewLine
    
    strLog = strLog & "Read current region range: " & robjInputCurrentRegion.Address & vbNewLine

    strLog = strLog & "Read count of rows: " & CStr(robjInputCurrentRegion.Rows.Count) & vbNewLine
    
    strLog = strLog & "Logged time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")

    mfstrLogOfReadingExcelSheetDataTable = strLog
End Function


'**---------------------------------------------
'** Get data-table log by loading from Worksheet
'**---------------------------------------------
'''
'''
'''
Public Sub GetCountsOfRowsColumnsFromSheet(ByRef robjSheet As Excel.Worksheet, ByRef rintRowsCount As Long, ByRef rintColumnsCount As Long, ByRef rstrLoggedTimeStamp As String, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    With robjSheet

        With .Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
        
            rintRowsCount = .Rows.Count - 1    ' exception for the field-title first row
            
            rintColumnsCount = .Columns.Count
        End With
        
        rstrLoggedTimeStamp = Format(Now(), "ddd, yyyy/m/d hh:mm:ss") ' FormatDateTimeWithWeekdayByEnglish(Now())
    End With
End Sub


'**---------------------------------------------
'** Load data-table from Worksheet to array of Variant
'**---------------------------------------------
'''
'''
'''
Public Function IsAtLeastOneRecordExistedInSpecifiedRegionWhenFieldTitlesExist(ByVal vobjSheet As Excel.Worksheet, Optional vintFieldTitlesRowIndex As Long = 1, Optional vintTopLeftColumnIndex As Long = 1) As Boolean

    Dim objCurrentRegion As Excel.Range, blnIsAtLeastOneRecordExisted As Boolean
    
    
    blnIsAtLeastOneRecordExisted = False

    Set objCurrentRegion = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
    If objCurrentRegion.Rows.Count > 1 Then
        
        blnIsAtLeastOneRecordExisted = True
    End If

    IsAtLeastOneRecordExistedInSpecifiedRegionWhenFieldTitlesExist = blnIsAtLeastOneRecordExisted
End Function



'''
''' get Row-Column (2 dimensional array) values from a Worksheet
'''
''' <Argument>rvarRowColumnValues: Output - rvarRowColumnValues: Variant(n, m) 2-dimensional array Variant </Argument>
''' <Argument>vobjSheet</Argument>
''' <Argument>vintFieldTitlesRowIndex</Argument>
''' <Argument>vintTopLeftColumnIndex</Argument>
Public Sub GetRCValuesFromSheetWithTopLeftIndexes(ByRef rvarRowColumnValues As Variant, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional vintFieldTitlesRowIndex As Long = 1, _
        Optional vintTopLeftColumnIndex As Long = 1)


    Dim objCurrentRegion As Excel.Range, strRange As String

    Set objCurrentRegion = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion

    GetRCValuesFromCurrentRegion rvarRowColumnValues, objCurrentRegion
End Sub

'''
'''
'''
''' <Argument>rvarRowColumnValues: Output - Variant(vintRestrictedRowsCount, m) 2-dimensional array Variant </Argument>
''' <Argument>vobjSheet</Argument>
''' <Argument>vintFieldTitlesRowIndex</Argument>
''' <Argument>vintTopLeftColumnIndex</Argument>
''' <Argument>vintRestrictedRowsCount</Argument>
Public Sub GetRCValuesOfRestrictedRowsCountFromSheetWithTopLeftIndexes(ByRef rvarRowColumnValues As Variant, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional vintFieldTitlesRowIndex As Long = 1, _
        Optional vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vintRestrictedRowsCount As Long = 1)


    Dim objCurrentRegion As Excel.Range, strRange As String

    Set objCurrentRegion = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion

    GetRCValuesOfRestrictedRowsCountFromCurrentRegion rvarRowColumnValues, objCurrentRegion, vintRestrictedRowsCount
End Sub

'''
'''
'''
Public Sub GetRCValuesFromCurrentRegion(ByRef rvarRowColumnValues As Variant, ByRef robjCurrentRegion As Excel.Range)

    Dim intRowMax As Long, intColumnMax As Long, strRange As String

    With robjCurrentRegion
    
        intRowMax = .Rows.Count - 1
        
        intColumnMax = .Columns.Count
            
        If .Rows.Count > 1 Then
        
            ReDim rvarRowColumnValues(1 To intRowMax, 1 To intColumnMax)
        
            strRange = "A2:" & ConvertXlColumnIndexToLetter(.Columns.Count) & CStr(.Rows.Count)
            
            If intRowMax = 1 And intColumnMax = 1 Then
            
                rvarRowColumnValues(1, 1) = robjCurrentRegion.Range(strRange).Resize(intRowMax, intColumnMax).Value
            Else
                rvarRowColumnValues = robjCurrentRegion.Range(strRange).Resize(intRowMax, intColumnMax).Value
            End If
        End If
    End With
End Sub

'''
'''
'''
Public Sub GetRCValuesOfRestrictedRowsCountFromCurrentRegion(ByRef rvarRowColumnValues As Variant, _
        ByRef robjCurrentRegion As Excel.Range, _
        Optional ByVal vintRestrictedRowsCount As Long = 1)
        
    Dim intRowMax As Long, intColumnMax As Long, strRange As String

    With robjCurrentRegion
    
        intRowMax = .Rows.Count - 1
        
        intColumnMax = .Columns.Count
        
        If vintRestrictedRowsCount > 0 Then
        
            If intRowMax > vintRestrictedRowsCount Then
            
                intRowMax = vintRestrictedRowsCount
            End If
        End If
        
        If intRowMax >= 1 Then
        
            ReDim rvarRowColumnValues(1 To intRowMax, 1 To intColumnMax)
        
            strRange = "A2:" & ConvertXlColumnIndexToLetter(.Columns.Count) & CStr(.Rows.Count)
            
            If intRowMax = 1 And intColumnMax = 1 Then
            
                rvarRowColumnValues(1, 1) = robjCurrentRegion.Range(strRange).Resize(intRowMax, intColumnMax).Value
            Else
                rvarRowColumnValues = robjCurrentRegion.Range(strRange).Resize(intRowMax, intColumnMax).Value
            End If
        End If
    End With
End Sub


'''
'''
'''
Public Sub ReflectRCValuesToCurrentRegion(ByRef rvarRowColumnValues As Variant, ByRef robjCurrentRegion As Excel.Range)

    With robjCurrentRegion

        .Worksheet.Range(.Cells(2, 1), .Cells(.Rows.Count, .Columns.Count)).Value = rvarRowColumnValues
    End With
End Sub

'''
'''
'''
''' <Argument>robjDTCol: Output</Argument>
''' <Argument>robjFieldTitlesCol: Output</Argument>
''' <Argument>vobjSheet: Input</Argument>
''' <Argument>vintFieldTitlesRowIndex: Input</Argument>
''' <Argument>vintTopLeftColumnIndex: Input</Argument>
Public Sub GetDTTableAndFieldTitlesFromSheet(ByRef robjDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)

    Dim varValues As Variant
    
    GetRCVariantTableFromSheet varValues, robjFieldTitlesCol, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex

    Set robjDTCol = GetDTColFromRowColumnVariantTable(varValues)
End Sub


'''
'''
'''
Public Function GetDTColFromRowColumnVariantTable(ByRef rvarRowColumnVallues As Variant) As Collection

    Dim objDTCol As Collection, objRowCol As Collection, i As Long, j As Long
    
    Set objDTCol = New Collection
    
    For i = LBound(rvarRowColumnVallues, 1) To UBound(rvarRowColumnVallues, 1)
    
        Set objRowCol = New Collection
        
        For j = LBound(rvarRowColumnVallues, 2) To UBound(rvarRowColumnVallues, 2)
        
            objRowCol.Add rvarRowColumnVallues(i, j)
        Next
        
        objDTCol.Add objRowCol
    Next
    
    Set GetDTColFromRowColumnVariantTable = objDTCol
End Function


'''
''' get Row-Column (2 dimensional) Variant array from a Worksheet
'''
''' <Argument>rvarRowColumnValues: Output</Argument>
''' <Argument>robjFieldTitlesCol: Output</Argument>
''' <Argument>vobjSheet: Input</Argument>
''' <Argument>vintFieldTitlesRowIndex: Input</Argument>
''' <Argument>vintTopLeftColumnIndex: Input</Argument>
Public Sub GetRCVariantTableFromSheet(ByRef rvarRowColumnValues As Variant, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    Dim objCurrentRegion As Excel.Range
  
    Set objCurrentRegion = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
    GetRCVariantTableFromCurrentRegion rvarRowColumnValues, robjFieldTitlesCol, objCurrentRegion
End Sub

'''
'''
'''
''' <Argument>rvarRowColumnValues: Output</Argument>
''' <Argument>robjFieldTitlesCol: Output</Argument>
''' <Argument>vobjCurrentRegion: Input</Argument>
Public Sub GetRCVariantTableFromCurrentRegion(ByRef rvarRowColumnValues As Variant, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vobjCurrentRegion As Excel.Range)

    GetFieldTitlesFromCurrentRegion robjFieldTitlesCol, vobjCurrentRegion
    
    GetRCValuesFromCurrentRegion rvarRowColumnValues, vobjCurrentRegion
End Sub


'**---------------------------------------------
'** About getting column-titles (field-titles) form a Worksheet
'**---------------------------------------------
'''
'''
'''
Public Sub GetFieldTitlesBasedOnCurrentRegionFromLastSheetOfBookAndTopLeftIndexes(ByRef robjFieldTitles As Collection, _
        ByRef robjBook As Excel.Workbook, _
        ByRef rintTopLeftRowIndex As Long, _
        ByRef rintTopLeftColumnIndex As Long)

    Dim objSheet As Excel.Worksheet
    
    With robjBook.Worksheets
    
        Set objSheet = .Item(.Count)
    End With

    GetFieldTitlesBasedOnCurrentRegionFromSheetAndTopLeftIndexes robjFieldTitles, objSheet, rintTopLeftRowIndex, rintTopLeftColumnIndex
End Sub

'''
''' Get collection of field-titles
'''
Public Sub GetFieldTitlesBasedOnCurrentRegionFromSheetAndTopLeftIndexes(ByRef robjFieldTitles As Collection, _
        ByRef robjSheet As Excel.Worksheet, _
        ByRef rintTopLeftRowIndex As Long, _
        ByRef rintTopLeftColumnIndex As Long)

    Dim objCurrentRegion As Excel.Range
    
    Set objCurrentRegion = robjSheet.Range(ConvertXlColumnIndexToLetter(rintTopLeftColumnIndex) & CStr(rintTopLeftRowIndex)).CurrentRegion

    GetFieldTitlesFromCurrentRegion robjFieldTitles, objCurrentRegion
End Sub

'''
''' Get collection of field-titles
'''
''' <Argument>robjFieldTitles: Output</Argument>
''' <Argument>robjCurrentRegion: Input</Argument>
Public Sub GetFieldTitlesFromCurrentRegion(ByRef robjFieldTitles As Collection, ByRef robjCurrentRegion As Excel.Range)

    Dim varFieldTitles As Variant, intRowMax As Long, intColumnMax As Long, strRange As String, i As Long

    With robjCurrentRegion
    
        intColumnMax = .Columns.Count
        
        ' About field-titles
        ReDim varFieldTitles(1 To 1, 1 To .Columns.Count)
        
        If .Columns.Count = 1 Then
        
            strRange = "A1" ' ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)
        Else
            strRange = "A1:" & ConvertXlColumnIndexToLetter(.Columns.Count) & "1"
        End If
        
        If intColumnMax = 1 Then
        
            varFieldTitles(1, 1) = .Range(strRange).Resize(1, 1).Value
        Else
            varFieldTitles = .Range(strRange).Resize(1, .Columns.Count).Value
        End If
    End With
    
    
    If robjFieldTitles Is Nothing Then Set robjFieldTitles = New Collection
    
    ' About field-titles
    For i = 1 To intColumnMax
    
        robjFieldTitles.Add varFieldTitles(1, i)
    Next
End Sub


'''
''' Get Excel.Range object of field-titles, but when the vintFieldTitleIndex is not the column index of top-left current regions, the vintFieldTitle index is to be used
'''
Public Function GetFieldTitlesRangeFromCurrentRegionWithPriorFieldTitleIdxThanTopLeftCell(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vintFieldTitlesRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long) As Excel.Range
    
    Dim objRange As Excel.Range

    With vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
        If .Columns.Count = 1 Then
            
            Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(.Column) & CStr(vintFieldTitlesRowIndex))
        Else
            Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(.Column) & CStr(vintFieldTitlesRowIndex) & ":" & ConvertXlColumnIndexToLetter(.Column + .Columns.Count - 1) & CStr(vintFieldTitlesRowIndex))
        End If
    End With

    Set GetFieldTitlesRangeFromCurrentRegionWithPriorFieldTitleIdxThanTopLeftCell = objRange
End Function

'''
''' Get Excel.Range object of records, but when the vintFieldTitleIndex is not the column index of top-left current regions, the records range object is got below of the vintFieldTitleIndex
'''
Public Function GetRecordsRangeFromCurrentRegionWithPriorFieldTitleIdxThanTopLeftCell(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vintFieldTitlesRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long) As Excel.Range
    
    Dim objRange As Excel.Range

    With vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
        If .Rows.Count > 1 Then
    
            Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(.Column) & CStr(vintFieldTitlesRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(.Column + .Columns.Count - 1) & CStr(.Row + .Rows.Count - 1))
        End If
    End With

    Set GetRecordsRangeFromCurrentRegionWithPriorFieldTitleIdxThanTopLeftCell = objRange
End Function

'''
'''�@Get Excel.Range object
'''  Attention - get all column title string although the some void string titles are on middle of columns
'''
Public Function GetFieldTitlesRangeFromUsedRange(ByVal vobjSheet As Excel.Worksheet, ByVal vintFieldTitlesRowIndex As Long, ByVal vintTopLeftColumnIndex As Long) As Excel.Range
    
    Dim objRange As Excel.Range

    With vobjSheet.UsedRange
    
        If .Columns.Count = 1 Then
            
            Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
        Else
            If vintTopLeftColumnIndex <= .Columns.Count Then
                
                Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex) & ":" & ConvertXlColumnIndexToLetter(.Columns.Count) & CStr(vintFieldTitlesRowIndex))
            Else
                Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
            End If
        End If
    End With

    Set GetFieldTitlesRangeFromUsedRange = objRange
End Function


'''
''' get position-index for each header titles, using Match WorkSheet function
'''
Public Function GetFieldTitleToPositionIndexDic(ByVal vobjSearchingFieldTitles As Collection, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vblnPriorToUseCurrentRegion As Boolean = True) As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, objRange As Excel.Range
    Dim varTitle As Variant, strTitle As String, intIndex As Long
    
    With vobjSheet.Application
    
        On Error Resume Next
        
        If vblnPriorToUseCurrentRegion Then
        
            Set objRange = GetFieldTitlesRangeFromCurrentRegionWithPriorFieldTitleIdxThanTopLeftCell(vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
        Else
            Set objRange = GetFieldTitlesRangeFromUsedRange(vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
        End If
        
        For Each varTitle In vobjSearchingFieldTitles
        
            strTitle = varTitle
            
            intIndex = -1
            
            intIndex = .WorksheetFunction.Match(strTitle, objRange, 0)
    
            If Err.Number <> 0 Then
                ' Not Found
                Err.Clear
            Else
                If objDic Is Nothing Then
                
                    Set objDic = New Scripting.Dictionary
                End If
                
                objDic.Add strTitle, intIndex
            End If
        Next
    
        On Error GoTo 0
    
    End With

    Set GetFieldTitleToPositionIndexDic = objDic
End Function

'''
''' confirm a field-title exists or not
'''
Public Function IsFieldTitleExisted(ByVal vstrFieldTitle As String, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1) As Boolean
    
    Dim blnIsExisted As Boolean, intIndex As Long, objRange As Excel.Range
    
    With vobjSheet.Application
    
        Set objRange = GetFieldTitlesRangeFromCurrentRegionWithPriorFieldTitleIdxThanTopLeftCell(vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
        
        blnIsExisted = False
        
        On Error Resume Next
        
        intIndex = -1
        
        intIndex = .WorksheetFunction.Match(vstrFieldTitle, objRange, 0)
        
        If Err.Number <> 0 Then
            ' Not Found
            Err.Clear
        Else
            blnIsExisted = True
    End If
    
    On Error GoTo 0

    End With

    IsFieldTitleExisted = blnIsExisted
End Function

'''
''' confirm all field-titles exist or not
'''
Public Function AreAllFieldTitlesExisted(ByVal vobjFieldTitles As Collection, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1) As Boolean
    
    Dim blnAreAllExisted As Boolean, intIndex As Long, objRange As Excel.Range
    Dim varFieldTitle As Variant
    
    
    With vobjSheet.Application
    
        Set objRange = GetFieldTitlesRangeFromCurrentRegionWithPriorFieldTitleIdxThanTopLeftCell(vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
        
        blnAreAllExisted = True
        
        On Error Resume Next
        
        For Each varFieldTitle In vobjFieldTitles
        
            intIndex = -1
            
            intIndex = .WorksheetFunction.Match(varFieldTitle, objRange, 0)
            
            If Err.Number <> 0 Then
                ' Not Found
                Err.Clear
                
                blnAreAllExisted = False
                
                Exit For
            End If
        
        Next
        
        On Error GoTo 0

    End With

    AreAllFieldTitlesExisted = blnAreAllExisted
End Function


'**---------------------------------------------
'** About getting Dictionary form a Worksheet
'**---------------------------------------------
'''
''' get Row-Column (2 dimensional array) dictionary from all table region of a Worksheet
'''
''' <Argument>robjRCTableDic: Output</Argument>
''' <Argument>robjFieldTitlesCol: Output</Argument>
''' <Argument>vobjSheet: Input</Argument>
''' <Argument>vintFieldTitlesRowIndex: Input</Argument>
''' <Argument>vintTopLeftColumnIndex: Input</Argument>
Public Sub GetRCDictionaryFromSheet(ByRef robjRCTableDic As Scripting.Dictionary, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional vintFieldTitlesRowIndex As Long = 1, _
        Optional vintTopLeftColumnIndex As Long = 1)

    Dim varValues() As Variant, intRowMax As Long, intColumnMax As Long, objCurrentRegion As Excel.Range
    Dim strRange As String
    
    Dim varFieldTitles() As Variant, intColumnIdx As Long, intRowIdx As Long
    Dim varKey As Variant, varItem As Variant, objValuesCol As Collection
    
    Dim objRCTableDic As Scripting.Dictionary, objFieldTitlesCol As Collection
    
    
    Set objRCTableDic = New Scripting.Dictionary
    
    Set objCurrentRegion = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion


    GetRCVariantTableFromCurrentRegion varValues, objFieldTitlesCol, objCurrentRegion

    With objCurrentRegion
    
        intRowMax = .Rows.Count - 1
        
        intColumnMax = .Columns.Count
    End With


    ' About data-table
    varItem = Empty
    
    For intRowIdx = 1 To intRowMax
    
        For intColumnIdx = 1 To intColumnMax
            
            If intColumnIdx = 1 Then
                
                varKey = varValues(intRowIdx, intColumnIdx)
                
                If intColumnMax > 2 Then
                    
                    Set objValuesCol = New Collection
                End If
            Else
                If intColumnMax = 2 Then
                    
                    varItem = varValues(intRowIdx, intColumnIdx)
                Else
                    objValuesCol.Add varValues(intRowIdx, intColumnIdx)
                End If
            End If
        Next
        
        If intColumnMax > 2 Then
        
            objRCTableDic.Add varKey, objValuesCol
        Else
            objRCTableDic.Add varKey, varItem
        End If
    Next

    ' Outputs
    Set robjRCTableDic = objRCTableDic
    
    Set robjFieldTitlesCol = objFieldTitlesCol
End Sub

'''
''' get data-table from Excel.Worksheet
'''
''' <Return>Collection(Of Collection(Of Variant)) or Collection(Of String)</Return>
Public Function GetDataTableCollectionForOnlySpecifiedFieldsFromWorksheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitles As Collection, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vblnWithDistinctCondition As Boolean = False) As Collection


    Dim objFieldTitleToPositionIndexDic As Scripting.Dictionary, varFieldTitle As Variant, strFieldTitle As String, objCurrentRegion As Excel.Range
    
    Dim objCol As Collection, objChildCol As Collection
    Dim objDistinctDic As Scripting.Dictionary, strKey As String, intRecordRowsCount As Long
    Dim intFieldColumnIndex As Long
    Dim varColumnValues() As Variant, varDataTableValues() As Variant, i As Long, j As Long
    
    Dim varFieldTtitle As Variant
    

    Set objCol = New Collection
    
    If vblnWithDistinctCondition Then
    
        Set objDistinctDic = New Scripting.Dictionary
    End If

    Set objCurrentRegion = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion

    intRecordRowsCount = objCurrentRegion.Rows.Count - 1
    
    If intRecordRowsCount > 0 Then
    
        Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(vobjFieldTitles, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
        
        With vobjSheet
        
            If vobjFieldTitles.Count = 1 Then
            
                With objCurrentRegion
                
                    ReDim varColumnValues(1 To .Rows.Count, 1 To 1)
            
                    intFieldColumnIndex = CLng(objFieldTitleToPositionIndexDic.Item(vobjFieldTitles.Item(1)))
            
                    varColumnValues = vobjSheet.Cells(vintFieldTitlesRowIndex, intFieldColumnIndex).Resize(.Rows.Count, 1).Value
            
                    For i = 2 To .Rows.Count
                    
                        If vblnWithDistinctCondition Then
                        
                            With objDistinctDic
                            
                                strKey = CStr(varColumnValues(i, 1))
                                
                                If Not .Exists(strKey) Then
                                
                                    .Add strKey, 0
                                
                                    objCol.Add varColumnValues(i, 1)
                                End If
                            End With
                        Else
                            objCol.Add varColumnValues(i, 1)
                        End If
                    Next
                End With
            Else
                With objCurrentRegion
                
                    ReDim varDataTableValues(1 To .Rows.Count, 1 To vobjFieldTitles.Count)
                End With
                
                j = 1
                
                For Each varFieldTitle In objFieldTitleToPositionIndexDic.Keys
                
                    strFieldTitle = varFieldTtitle
                
                    With objCurrentRegion
                    
                        ReDim varColumnValues(1 To .Rows.Count, 1 To 1)
                        
                        intFieldColumnIndex = CLng(objFieldTitleToPositionIndexDic.Item(varFieldTitle))
                        
                        varColumnValues = vobjSheet.Cells(vintFieldTitlesRowIndex, intFieldColumnIndex).Resize(.Rows.Count, 1).Value
                        
                        For i = 2 To .Rows.Count
                        
                            varDataTableValues(i, j) = varColumnValues(i, 1)
                        Next
                    End With
                    
                    j = j + 1
                Next
            End If
        
            With objCurrentRegion
            
                For i = 2 To .Rows.Count
                
                    If vblnWithDistinctCondition Then
                    
                        strKey = ""
                        
                        For j = 1 To vobjFieldTitles.Count
                        
                            strKey = strKey & varDataTableValues(i, j) & ","
                        Next
                    
                        With objDistinctDic
                            If Not .Exists(strKey) Then
                                
                                .Add strKey, 0
                                
                                Set objChildCol = New Collection
                                
                                For j = 1 To vobjFieldTitles.Count
                                
                                    objChildCol.Add varDataTableValues(i, j)
                                Next
                            
                                objCol.Add objChildCol
                            End If
                        End With
                    Else
                    
                        Set objChildCol = New Collection
                                
                        For j = 1 To vobjFieldTitles.Count
                        
                            objChildCol.Add varDataTableValues(i, j)
                        Next
                    
                        objCol.Add objChildCol
                    End If
                
                Next
            
            End With
        
        End With
    End If

    Set GetDataTableCollectionForOnlySpecifiedFieldsFromWorksheet = objCol
End Function



'''
''' get Row-Column (2 dimensional array) dictionary from some part regions of a Worksheet
'''
''' <Return>Dictionary(Of String, String) Or Dictionary(Of String, Collection(Of String))</Return>
Public Function GetDictionaryFromWorksheetFromKeyColumnAndValueColumn(ByVal vobjSheet As Excel.Worksheet, ByVal vstrKeyFieldTitle As String, ByVal vobjValueFieldTitles As Collection, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    Dim objCurrentRegion As Excel.Range, intRecordRowsCount As Long, intFieldColumnsCount As Long

    Dim objAllFieldTitles As Collection, objFieldTitleToPositionIndexDic As Scripting.Dictionary

    Dim varKeys() As Variant, varValues() As Variant, varOneColumnValues() As Variant, i As Long, j As Long, objRowCol As Collection


    Set objDic = New Scripting.Dictionary
    
    Set objCurrentRegion = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion

    With objCurrentRegion
    
        intRecordRowsCount = .Rows.Count - 1
        
        intFieldColumnsCount = .Columns.Count
    End With
    
    If intRecordRowsCount > 0 And intFieldColumnsCount >= 2 Then
    
        Set objAllFieldTitles = GetUnionedColFromOnsStringAndStrCollection(vstrKeyFieldTitle, vobjValueFieldTitles)
    
        Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(objAllFieldTitles, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
    
        With objCurrentRegion
        
            ReDim varKeys(1 To .Rows.Count, 1 To 1)
            ReDim varValues(1 To .Rows.Count, 1 To vobjValueFieldTitles.Count)
            
            varKeys = vobjSheet.Cells(vintFieldTitlesRowIndex, objFieldTitleToPositionIndexDic.Item(vstrKeyFieldTitle)).Resize(.Rows.Count, 1).Value
            
            If vobjValueFieldTitles.Count = 1 Then
            
                varValues = vobjSheet.Cells(vintFieldTitlesRowIndex, objFieldTitleToPositionIndexDic.Item(vobjValueFieldTitles.Item(1))).Resize(.Rows.Count, 1).Value
                
                For i = 2 To .Rows.Count
                
                    With objDic
                    
                        If Not .Exists(varKeys(i, 1)) Then
                        
                            .Add varKeys(i, 1), varValues(i, 1)
                        End If
                    End With
                Next
                
            Else
                For j = 1 To vobjValueFieldTitles.Count
                
                    ReDim varOneColumnValues(1 To .Rows.Count, 1 To 1)
                
                    varOneColumnValues = vobjSheet.Cells(vintFieldTitlesRowIndex, objFieldTitleToPositionIndexDic.Item(vobjValueFieldTitles.Item(j))).Resize(.Rows.Count, 1).Value
                
                    For i = 2 To .Rows.Count
                
                        varValues(i, j) = varOneColumnValues(i, 1)
                    Next
                Next
                
                
                For i = 2 To .Rows.Count
                
                    With objDic
                        If Not .Exists(varKeys(i, 1)) Then
                    
                            Set objRowCol = New Collection
                    
                            For j = 1 To vobjValueFieldTitles.Count
                            
                                objRowCol.Add varValues(i, j)
                            Next
                    
                            .Add varKeys(i, 1), objRowCol
                        End If
                    End With
                Next
            End If
    
        End With
    End If


    Set GetDictionaryFromWorksheetFromKeyColumnAndValueColumn = objDic
    
End Function

'''
'''
'''
''' <Return>Dictionary(Of String, String)</Return>
Public Function GetSimpleDictionaryFromWorksheet(ByVal vobjSheet As Excel.Worksheet, ByVal vstrKeyFieldTitle As String, ByVal vstrValueFieldTitle As String, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1, Optional ByVal vblnOmitDuplicatedKeys As Boolean = True) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, objTableRange As Excel.Range, intRecordRowsCount As Long, intFieldColumnsCount As Long
    Dim objFieldTitleToPositionIndexDic As Scripting.Dictionary
    Dim varKeys() As Variant, varValues() As Variant, i As Long
    
    
    Set objDic = New Scripting.Dictionary
    
    Set objTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion

    With objTableRange
    
        intRecordRowsCount = .Rows.Count - 1
        
        intFieldColumnsCount = .Columns.Count
    End With
    
    If intRecordRowsCount > 0 And intFieldColumnsCount >= 2 Then

        Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(GetColFromLineDelimitedChar(vstrKeyFieldTitle & "," & vstrValueFieldTitle), vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)

        With objTableRange
        
            ReDim varKeys(1 To .Rows.Count, 1 To 1)
            ReDim varValues(1 To .Rows.Count, 1 To 1)
            
            varKeys = vobjSheet.Cells(vintFieldTitlesRowIndex, objFieldTitleToPositionIndexDic.Item(vstrKeyFieldTitle)).Resize(.Rows.Count, 1).Value
            
            varValues = vobjSheet.Cells(vintFieldTitlesRowIndex, objFieldTitleToPositionIndexDic.Item(vstrValueFieldTitle)).Resize(.Rows.Count, 1).Value
            
            
            If vblnOmitDuplicatedKeys Then
            
                For i = 2 To .Rows.Count
                
                    If Not IsEmpty(varKeys(i, 1)) And Not IsEmpty(varValues(i, 1)) Then
                
                        If Not objDic.Exists(varKeys(i, 1)) Then
                        
                            objDic.Add varKeys(i, 1), varValues(i, 1)
                        End If
                    End If
                Next
            Else
            
                For i = 2 To .Rows.Count
                
                    If Not IsEmpty(varKeys(i, 1)) And Not IsEmpty(varValues(i, 1)) Then
                    
                        objDic.Add varKeys(i, 1), varValues(i, 1)
                    End If
                Next
            End If
            
        End With
    End If

    Set GetSimpleDictionaryFromWorksheet = objDic
End Function

'''
'''
'''
''' <Return>Dictionary(Of String, Dictionary(Of String, String))</Return>
Public Function GetSimpleOneNestedDictionaryFromWorksheet(ByVal vobjSheet As Excel.Worksheet, ByVal vstr1stKeyFieldTitle As String, ByVal vstr2ndKeyFieldTitle As String, ByVal vstrValueFieldTitle As String, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1, Optional ByVal vblnOmitAfterSecondDuplicatedKeys As Boolean = True) As Scripting.Dictionary

    Dim objOneNestedDic As Scripting.Dictionary, objChildDic As Scripting.Dictionary, objTableRange As Excel.Range, intRecordRowsCount As Long, intFieldColumnsCount As Long
    Dim objFieldTitleToPositionIndexDic As Scripting.Dictionary
    Dim var1stKeys() As Variant, var2ndKeys() As Variant, varValues() As Variant, i As Long
    
    
    Set objOneNestedDic = New Scripting.Dictionary
    
    Set objTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion

    With objTableRange
    
        intRecordRowsCount = .Rows.Count - 1
        
        intFieldColumnsCount = .Columns.Count
    End With
    
    If intRecordRowsCount > 0 And intFieldColumnsCount >= 3 Then
    
        Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(GetColFromLineDelimitedChar(vstr1stKeyFieldTitle & "," & vstr2ndKeyFieldTitle & "," & vstrValueFieldTitle), vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)

        With objTableRange
        
            ReDim var1stKeys(1 To .Rows.Count, 1 To 1)
            ReDim var2ndKeys(1 To .Rows.Count, 1 To 1)
            ReDim varValues(1 To .Rows.Count, 1 To 1)
            
            var1stKeys = vobjSheet.Cells(vintFieldTitlesRowIndex, objFieldTitleToPositionIndexDic.Item(vstr1stKeyFieldTitle)).Resize(.Rows.Count, 1).Value
            
            var2ndKeys = vobjSheet.Cells(vintFieldTitlesRowIndex, objFieldTitleToPositionIndexDic.Item(vstr2ndKeyFieldTitle)).Resize(.Rows.Count, 1).Value
            
            varValues = vobjSheet.Cells(vintFieldTitlesRowIndex, objFieldTitleToPositionIndexDic.Item(vstrValueFieldTitle)).Resize(.Rows.Count, 1).Value
            
            
            For i = 2 To .Rows.Count
            
                If Not IsEmpty(var1stKeys(i, 1)) And Not IsEmpty(var2ndKeys(i, 1)) And Not IsEmpty(varValues(i, 1)) Then
            
                    With objOneNestedDic
                    
                        If Not .Exists(var1stKeys(i, 1)) Then
                        
                            Set objChildDic = New Scripting.Dictionary
                            
                            .Add var1stKeys(i, 1), objChildDic
                        Else
                            Set objChildDic = .Item(var1stKeys(i, 1))
                        End If
                    
                        With objChildDic
                        
                            If vblnOmitAfterSecondDuplicatedKeys Then
                                
                                If Not .Exists(var2ndKeys(i, 1)) Then
                                
                                    .Add var2ndKeys(i, 1), varValues(i, 1)
                                End If
                            Else
                                .Add var2ndKeys(i, 1), varValues(i, 1)
                            End If
                        End With
                    End With
                End If
            Next
        End With
    End If

    Set GetSimpleOneNestedDictionaryFromWorksheet = objOneNestedDic
End Function



'''
'''
'''
Public Function GetFieldTitlesVarFromSheet(ByVal vobjSheet As Excel.Worksheet, ByVal vintFieldTitlesRowIndex As Long, ByVal vintTopLeftColumnIndex As Long, Optional ByVal vobjColumnsExceptionIndexList As Scripting.Dictionary = Nothing, Optional ByVal vobjColumnsFieldTitleNameExceptionList As Scripting.Dictionary = Nothing, Optional ByVal vblnReturnByCollection As Boolean = True) As Variant

    Dim objFieldTitlesRange As Excel.Range, objRange As Excel.Range, objFieldTitlesCol As Collection, objFieldTitleKeyDic As Scripting.Dictionary
    Dim strFieldTitle As String, varTitles As Variant, i As Long
    
    
    Set objFieldTitlesRange = GetFieldTitlesRangeFromCurrentRegionWithPriorFieldTitleIdxThanTopLeftCell(vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
    Set objFieldTitlesCol = New Collection
    
    i = 1
    
    For Each objRange In objFieldTitlesRange
    
        strFieldTitle = CStr(objRange.Value)
    
        If strFieldTitle <> "" Then
        
            If objFieldTitleKeyDic Is Nothing Then Set objFieldTitleKeyDic = New Scripting.Dictionary
            
            With objFieldTitleKeyDic
            
                If Not .Exists(strFieldTitle) Then
                
                    If Not vobjColumnsFieldTitleNameExceptionList Is Nothing And Not vobjColumnsExceptionIndexList Is Nothing Then
                    
                        If Not vobjColumnsExceptionIndexList.Exists(i) And Not vobjColumnsFieldTitleNameExceptionList.Exists(strFieldTitle) Then
                        
                            .Add strFieldTitle, 0
                        End If
                    ElseIf Not vobjColumnsFieldTitleNameExceptionList Is Nothing Then
                    
                        If Not vobjColumnsFieldTitleNameExceptionList.Exists(strFieldTitle) Then
                        
                            .Add strFieldTitle, 0
                        End If
                    ElseIf Not vobjColumnsExceptionIndexList Is Nothing Then
                        
                        If Not vobjColumnsExceptionIndexList.Exists(i) Then
                        
                            .Add strFieldTitle, 0
                        End If
                    Else
                    
                        .Add strFieldTitle, 0
                    End If
                
                    i = i + 1
                End If
            End With
        End If
    Next

    If vblnReturnByCollection Then
    
        If Not objFieldTitleKeyDic Is Nothing Then
            
            If objFieldTitleKeyDic.Count > 0 Then
            
                Set objFieldTitlesCol = GetEnumeratorKeysColFromDic(objFieldTitleKeyDic)
            End If
        End If
        
        Set varTitles = objFieldTitlesCol
    Else
        Set varTitles = objFieldTitleKeyDic
    End If

    Set GetFieldTitlesVarFromSheet = varTitles
End Function


'''
'''
'''
Public Function GetFieldTitlesColFromSheet(ByVal vobjSheet As Excel.Worksheet, ByVal vintFieldTitlesRowIndex As Long, ByVal vintTopLeftColumnIndex As Long, Optional ByVal vobjColumnsExceptionIndexList As Scripting.Dictionary = Nothing, Optional ByVal vobjColumnsFieldTitleNameExceptionList As Scripting.Dictionary = Nothing) As Collection

    Set GetFieldTitlesColFromSheet = GetFieldTitlesVarFromSheet(vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjColumnsExceptionIndexList, vobjColumnsFieldTitleNameExceptionList, True)
End Function

Public Function GetFieldTitlesDicFromSheet(ByVal vobjSheet As Excel.Worksheet, ByVal vintFieldTitlesRowIndex As Long, ByVal vintTopLeftColumnIndex As Long, Optional ByVal vobjColumnsExceptionIndexList As Scripting.Dictionary = Nothing, Optional ByVal vobjColumnsFieldTitleNameExceptionList As Scripting.Dictionary = Nothing) As Scripting.Dictionary

    Set GetFieldTitlesDicFromSheet = GetFieldTitlesVarFromSheet(vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjColumnsExceptionIndexList, vobjColumnsFieldTitleNameExceptionList, False)
End Function


'''
'''
'''
Public Function GetFieldTitlesColFromWorkbookPathAndSheetName(ByVal vstrBookPath As String, ByVal vstrSheetName As String, ByVal vintFieldTitlesRowIndex As Long, ByVal vintTopLeftColumnIndex As Long, Optional ByVal vobjColumnsExceptionIndexList As Scripting.Dictionary = Nothing, Optional ByVal vobjColumnsFieldTitleNameExceptionList As Scripting.Dictionary = Nothing) As Collection

    Dim objSheet As Excel.Worksheet
    
    Set objSheet = GetWorksheetFromBookPathAndSheetName(vstrBookPath, vstrSheetName)

    Set GetFieldTitlesColFromWorkbookPathAndSheetName = GetFieldTitlesColFromSheet(objSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjColumnsExceptionIndexList, vobjColumnsFieldTitleNameExceptionList)
End Function

'''
'''
'''
Public Function GetFieldTitlesDicFromWorkbookPathAndSheetName(ByVal vstrBookPath As String, ByVal vstrSheetName As String, ByVal vintFieldTitlesRowIndex As Long, ByVal vintTopLeftColumnIndex As Long, Optional ByVal vobjColumnsExceptionIndexList As Scripting.Dictionary = Nothing, Optional ByVal vobjColumnsFieldTitleNameExceptionList As Scripting.Dictionary = Nothing) As Scripting.Dictionary

    Dim objSheet As Excel.Worksheet
    
    Set objSheet = GetWorksheetFromBookPathAndSheetName(vstrBookPath, vstrSheetName)

    Set GetFieldTitlesDicFromWorkbookPathAndSheetName = GetFieldTitlesDicFromSheet(objSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjColumnsExceptionIndexList, vobjColumnsFieldTitleNameExceptionList)
End Function










