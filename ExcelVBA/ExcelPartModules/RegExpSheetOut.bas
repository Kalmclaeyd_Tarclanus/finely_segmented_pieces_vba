Attribute VB_Name = "RegExpSheetOut"
'
'   get new worksheet from extracted Excel.Worksheet data using RegExp objects
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon,  6/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
Public Enum ColumnsJoinLogical

    LogicalAND = 0
    
    LogicalOR = 1
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' filtering simply and keep the original Worksheet name
'''
Public Function GetSheetByQueryingSingleRegExpWithKeepingSheetName(ByVal vobjInputSheet As Excel.Worksheet, _
        ByVal vstrFieldTitle As String, _
        ByVal vstrPattern As String, _
        Optional ByVal vobjRegExpObject As VBScript_RegExp_55.RegExp = Nothing, _
        Optional venmColumnsJoinLogical As ColumnsJoinLogical = ColumnsJoinLogical.LogicalAND, _
        Optional ByVal vblnAllowToAddAutoShapeMessageLogToInputSheet As Boolean = False) As Excel.Worksheet
    
    Const SheetSuffix As String = "_Old"
    
    Dim strSheetName As String, strAnotherSheetName As String
    Dim strMessage As String

    With vobjInputSheet
    
        strSheetName = .Name
    
        If Len(strSheetName & SheetSuffix) <= 31 Then
        
            strAnotherSheetName = FindNewSheetNameWhenAlreadyExist(strSheetName & SheetSuffix, vobjInputSheet.Parent)
        Else
            strAnotherSheetName = FindNewSheetNameWhenAlreadyExist(Left(strSheetName, 26) & SheetSuffix, vobjInputSheet.Parent)
        End If
    
        .Name = strAnotherSheetName
    End With

    strMessage = "To keep original sheet name, changed name:" & vbNewLine & "Original name: " & strSheetName & vbNewLine & "Before filtering name: " & strAnotherSheetName

    AddAutoShapeGeneralMessage vobjInputSheet, strMessage

    Set GetSheetByQueryingSingleRegExpWithKeepingSheetName = GetSheetByQueryingSingleRegExp(strSheetName, vobjInputSheet, vstrFieldTitle, vstrPattern, vobjRegExpObject, venmColumnsJoinLogical, vblnAllowToAddAutoShapeMessageLogToInputSheet)
End Function


'''
'''
'''
Public Function GetSheetByQueryingSingleRegExp(ByVal vstrOutputSheetNewName As String, _
        ByVal vobjInputSheet As Excel.Worksheet, _
        ByVal vstrFieldTitle As String, _
        ByVal vstrPattern As String, _
        Optional vobjRegExpObject As VBScript_RegExp_55.RegExp = Nothing, _
        Optional venmColumnsJoinLogical As ColumnsJoinLogical = ColumnsJoinLogical.LogicalAND, _
        Optional ByVal vblnAllowToAddAutoShapeMessageLogToInputSheet As Boolean = False) As Excel.Worksheet


    Dim objFieldTitleToPatternDic As Scripting.Dictionary, objRegExpObjects As Collection
    
    Dim objOutputSheet As Excel.Worksheet
    
    
    Set objFieldTitleToPatternDic = New Scripting.Dictionary
    
    objFieldTitleToPatternDic.Add vstrFieldTitle, vstrPattern
    
    Set objRegExpObjects = New Collection
    
    If vobjRegExpObject Is Nothing Then
    
        objRegExpObjects.Add New VBScript_RegExp_55.RegExp
    Else
        objRegExpObjects.Add vobjRegExpObject
    End If
    
    Set objOutputSheet = GetSheetByQueryingRegExpWithDic(vstrOutputSheetNewName, _
            vobjInputSheet, _
            objFieldTitleToPatternDic, _
            objRegExpObjects, _
            venmColumnsJoinLogical, _
            vblnAllowToAddAutoShapeMessageLogToInputSheet)

    Set GetSheetByQueryingSingleRegExp = objOutputSheet
End Function

'''
'''
'''
Public Function GetSheetToOutputBookByQueryingSingleRegExp(ByVal vobjOutputBook As Excel.Workbook, _
        ByVal vstrOutputSheetNewName As String, _
        ByVal vobjInputSheet As Excel.Worksheet, _
        ByVal vstrFieldTitle As String, _
        ByVal vstrPattern As String, _
        Optional ByVal vobjRegExpObject As VBScript_RegExp_55.RegExp = Nothing, _
        Optional ByVal venmColumnsJoinLogical As ColumnsJoinLogical = ColumnsJoinLogical.LogicalAND, _
        Optional ByVal vblnAllowToAddAutoShapeMessageLogToInputSheet As Boolean = False) As Excel.Worksheet


    Dim objFieldTitleToPatternDic As Scripting.Dictionary
    
    Dim objRegExpObjects As Collection, objOutputSheet As Excel.Worksheet
    
    
    Set objFieldTitleToPatternDic = New Scripting.Dictionary
    
    objFieldTitleToPatternDic.Add vstrFieldTitle, vstrPattern
    
    Set objRegExpObjects = New Collection
    
    If vobjRegExpObject Is Nothing Then
    
        objRegExpObjects.Add New VBScript_RegExp_55.RegExp
    Else
        objRegExpObjects.Add vobjRegExpObject
    End If
    
    Set objOutputSheet = GetSheetToOutputBookByQueryingRegExpWithDic(vobjOutputBook, _
            vstrOutputSheetNewName, _
            vobjInputSheet, _
            objFieldTitleToPatternDic, _
            objRegExpObjects, _
            venmColumnsJoinLogical, _
            vblnAllowToAddAutoShapeMessageLogToInputSheet)

    Set GetSheetToOutputBookByQueryingSingleRegExp = objOutputSheet
End Function

'''
'''
'''
Public Function GetSheetByQueryingRegExpWithDic(ByVal vstrOutputSheetNewName As String, _
        ByVal vobjInputSheet As Excel.Worksheet, _
        ByVal vobjFieldTitleToPatternDic As Scripting.Dictionary, _
        Optional vobjRegExpObjects As Collection = Nothing, _
        Optional venmColumnsJoinLogical As ColumnsJoinLogical = ColumnsJoinLogical.LogicalAND, _
        Optional ByVal vblnAllowToAddAutoShapeMessageLogToInputSheet As Boolean = False) As Excel.Worksheet
    
    
    Dim objBook As Excel.Workbook
    
    Dim objOutputSheet As Excel.Worksheet
    
    
    Set objBook = vobjInputSheet.Parent

    Set objOutputSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objOutputSheet.Name = FindNewSheetNameWhenAlreadyExist(vstrOutputSheetNewName, objBook)

    QueryRegExpWithDic objOutputSheet, _
            vobjInputSheet, _
            vobjFieldTitleToPatternDic, _
            vobjRegExpObjects, _
            venmColumnsJoinLogical, _
            vblnAllowToAddAutoShapeMessageLogToInputSheet
    
    Set GetSheetByQueryingRegExpWithDic = objOutputSheet
End Function

'''
'''
'''
Public Function GetSheetByQueryingRegExp(ByVal vstrOutputSheetNewName As String, _
        ByVal vobjInputSheet As Excel.Worksheet, _
        ByVal vobjFieldTitleAndPatterns As Collection, _
        Optional vobjRegExpObjects As Collection = Nothing, _
        Optional venmColumnsJoinLogical As ColumnsJoinLogical = ColumnsJoinLogical.LogicalAND, _
        Optional ByVal vblnAllowToAddAutoShapeMessageLogToInputSheet As Boolean = False) As Excel.Worksheet
    
    Dim objBook As Excel.Workbook
    
    Dim objOutputSheet As Excel.Worksheet
    
    
    Set objBook = vobjInputSheet.Parent

    Set objOutputSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objOutputSheet.Name = FindNewSheetNameWhenAlreadyExist(vstrOutputSheetNewName, objBook)

    QueryRegExp objOutputSheet, vobjInputSheet, vobjFieldTitleAndPatterns, vobjRegExpObjects, venmColumnsJoinLogical, vblnAllowToAddAutoShapeMessageLogToInputSheet
    
    Set GetSheetByQueryingRegExp = objOutputSheet
End Function

'''
'''
'''
Public Function GetSheetToOutputBookByQueryingRegExpWithDic(ByVal vobjOutputBook As Workbook, _
        ByVal vstrOutputSheetNewName As String, _
        ByVal vobjInputSheet As Worksheet, _
        ByVal vobjFieldTitleToPatternDic As Scripting.Dictionary, _
        Optional vobjRegExpObjects As Collection = Nothing, _
        Optional venmColumnsJoinLogical As ColumnsJoinLogical = ColumnsJoinLogical.LogicalAND, _
        Optional ByVal vblnAllowToAddAutoShapeMessageLogToInputSheet As Boolean = False) As Excel.Worksheet
    
    Dim objOutputSheet As Excel.Worksheet
    
    
    Set objOutputSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(vobjOutputBook)
    
    objOutputSheet.Name = FindNewSheetNameWhenAlreadyExist(vstrOutputSheetNewName, vobjOutputBook)

    QueryRegExpWithDic objOutputSheet, _
            vobjInputSheet, _
            vobjFieldTitleToPatternDic, _
            vobjRegExpObjects, _
            venmColumnsJoinLogical, _
            vblnAllowToAddAutoShapeMessageLogToInputSheet
    
    
    Set GetSheetToOutputBookByQueryingRegExpWithDic = objOutputSheet
End Function


'''
'''
'''
Public Function GetSheetToOutputBookByQueryingRegExp(ByVal vobjOutputBook As Excel.Workbook, _
        ByVal vstrOutputSheetNewName As String, _
        ByVal vobjInputSheet As Excel.Worksheet, _
        ByVal vobjFieldTitleAndPatterns As Collection, _
        Optional vobjRegExpObjects As Collection = Nothing, _
        Optional venmColumnsJoinLogical As ColumnsJoinLogical = ColumnsJoinLogical.LogicalAND, _
        Optional ByVal vblnAllowToAddAutoShapeMessageLogToInputSheet As Boolean = False) As Excel.Worksheet
    
    Dim objOutputSheet As Excel.Worksheet
    
    
    Set objOutputSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(vobjOutputBook)
    
    objOutputSheet.Name = FindNewSheetNameWhenAlreadyExist(vstrOutputSheetNewName, vobjOutputBook)

    QueryRegExp objOutputSheet, _
            vobjInputSheet, _
            vobjFieldTitleAndPatterns, _
            vobjRegExpObjects, _
            venmColumnsJoinLogical, _
            vblnAllowToAddAutoShapeMessageLogToInputSheet
    
    Set GetSheetToOutputBookByQueryingRegExp = objOutputSheet
End Function

'''
''' query the data-sheet from plural RegExp objects
'''
Public Sub QueryRegExpWithDic(ByRef robjOutputSheet As Excel.Worksheet, _
        ByVal vobjInputSheet As Excel.Worksheet, _
        ByVal vobjFieldTitleToPatternDic As Scripting.Dictionary, _
        Optional ByVal vobjRegExpObjects As Collection = Nothing, _
        Optional ByVal venmColumnsJoinLogical As ColumnsJoinLogical = ColumnsJoinLogical.LogicalAND, _
        Optional ByVal vblnAllowToAddAutoShapeMessageLogToInputSheet As Boolean = False)

    Dim objFieldTitleAndPatterns As Collection, objPair As Collection
    Dim varFieldTitle As Variant
    
    
    Set objFieldTitleAndPatterns = New Collection
    
    For Each varFieldTitle In vobjFieldTitleToPatternDic.Keys
        
        Set objPair = New Collection
        
        With objPair
        
            .Add CStr(varFieldTitle)
            
            .Add CStr(vobjFieldTitleToPatternDic.Item(varFieldTitle))
        End With
    
        objFieldTitleAndPatterns.Add objPair
    Next
    
    QueryRegExp robjOutputSheet, vobjInputSheet, objFieldTitleAndPatterns, vobjRegExpObjects, venmColumnsJoinLogical, vblnAllowToAddAutoShapeMessageLogToInputSheet
End Sub



'''
''' query the data-sheet from plural RegExp objects
'''
Public Sub QueryRegExp(ByRef robjOutputSheet As Worksheet, _
        ByVal vobjInputSheet As Worksheet, _
        ByVal vobjFieldTitleAndPatterns As Collection, _
        Optional ByVal vobjRegExpObjects As Collection = Nothing, _
        Optional ByVal venmColumnsJoinLogical As ColumnsJoinLogical = ColumnsJoinLogical.LogicalAND, _
        Optional ByVal vblnAllowToAddAutoShapeMessageLogToInputSheet As Boolean = False)

    Dim i As Long
    Dim intRowIndex As Long
    Dim blnRemainings() As Boolean
    Dim objDeleteRange As Excel.Range
    
    Dim strPattern As String, strFieldTitle As String
    
    Dim objPair As Collection, varPair As Variant
    
    Dim intInputRowsCount As Long, intInputColumnsCount As Long, strLoggedTimeStamp As String
    Dim intOutputRowsCount As Long, intOutputColumnsCount As Long
    
    Dim objDoubleStopWatch As DoubleStopWatch, strMessage As String, strDateTimeMessage As String, strFilteringInfo As String
    
    
    Set objDoubleStopWatch = New DoubleStopWatch
    
    objDoubleStopWatch.MeasureStart
    

    GetHitRowsFromRegExpPatterns blnRemainings, vobjInputSheet, vobjFieldTitleAndPatterns, 1, vobjRegExpObjects, venmColumnsJoinLogical

    CopySheetWithCellFormatting vobjInputSheet, robjOutputSheet
    
    CopyColumnWidths vobjInputSheet, robjOutputSheet
    
    With robjOutputSheet
    
        i = 0
        
        For intRowIndex = 2 To .UsedRange.Rows.Count
        
            If Not blnRemainings(intRowIndex) Then
            
                If i = 0 Then
                    Set objDeleteRange = .Range(CStr(intRowIndex) & ":" & CStr(intRowIndex))
                Else
                    Set objDeleteRange = Union(objDeleteRange, .Range(CStr(intRowIndex) & ":" & CStr(intRowIndex)))
                End If
            
                i = i + 1
            End If
        Next
    
        If Not objDeleteRange Is Nothing Then
        
            objDeleteRange.Delete Shift:=xlUp
        End If
    End With

    objDoubleStopWatch.MeasureInterval
    
    strDateTimeMessage = "Starting process date-time: " & FormatDateAndWeekdayJapanese(objDoubleStopWatch.DateOfMeasureStart) & "," & FormatDateTime(objDoubleStopWatch.DateOfMeasureStart, vbLongTime)
    
    strFilteringInfo = ""
    
    i = 1
    
    For Each varPair In vobjFieldTitleAndPatterns
        
        Set objPair = varPair
        
        strFieldTitle = objPair.Item(1)
        
        strPattern = objPair.Item(2)
        
        If vobjFieldTitleAndPatterns.Count = 1 Then
        
            strFilteringInfo = strFilteringInfo & "Filtering field name: " & strFieldTitle & vbNewLine & "Filtering pattern: " & strPattern
        Else
            strFilteringInfo = strFilteringInfo & "Filtering field name " & Format(i, "00") & ": " & strFieldTitle & vbNewLine & "Filtering pattern " & Format(i, "00") & ": " & strPattern
        End If
        
        If i < vobjFieldTitleAndPatterns.Count Then
        
            strFilteringInfo = strFilteringInfo & vbNewLine
        End If
        
        i = i + 1
    Next
    
    GetCountsOfRowsColumnsFromSheet vobjInputSheet, intInputRowsCount, intInputColumnsCount, strLoggedTimeStamp
    
    GetCountsOfRowsColumnsFromSheet robjOutputSheet, intOutputRowsCount, intOutputColumnsCount, strLoggedTimeStamp
    
    If vblnAllowToAddAutoShapeMessageLogToInputSheet Then
        ' for the input sheet
        strMessage = "Before RegExp filtering:" & vbNewLine & strDateTimeMessage & vbNewLine & strFilteringInfo
        
        strMessage = strMessage & vbNewLine & "Rows count: " & CStr(intInputRowsCount)
        
        AddAutoShapeGeneralMessage vobjInputSheet, strMessage
    End If
    
    ' for the output sheet
    strMessage = "After RegExp filtering:" & vbNewLine & strDateTimeMessage & vbNewLine & strFilteringInfo & vbNewLine & "Filtering elapsed time: " & objDoubleStopWatch.ElapsedTimeByString
    
    strMessage = strMessage & vbNewLine & "Before RegExp rows count: " & CStr(intInputRowsCount) & vbNewLine & "After RegExp rows count: " & CStr(intOutputRowsCount)
    
    AddAutoShapeGeneralMessage robjOutputSheet, strMessage, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
End Sub

'''
'''
'''
Public Sub GetHitRowsFromRegExpPatterns(ByRef rvarHits As Variant, _
        ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitleAndPatterns As Collection, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional vobjRegExpObjects As Collection = Nothing, _
        Optional venmColumnsJoinLogical As ColumnsJoinLogical = ColumnsJoinLogical.LogicalAND)

    Dim i As Long
    Dim objRegExpObjects As Collection
    Dim objRegExArray() As VBScript_RegExp_55.RegExp
    Dim intCountOfPatterns As Long
    Dim varValues() As Variant
    Dim objTitleToColumnIndexDic As Scripting.Dictionary
    Dim objSearchingFieldTitles As Collection
    Dim varFieldTitle As Variant
    Dim intColumnIndex As Long, intRowIndex As Long
    Dim blnMatchResults() As Boolean, blnValue As Boolean
        
    
    Dim objPair As Collection, varPair As Variant

    
    intCountOfPatterns = vobjFieldTitleAndPatterns.Count
    
    If Not vobjRegExpObjects Is Nothing Then
    
        Set objRegExpObjects = vobjRegExpObjects
    Else
        Set objRegExpObjects = New Collection
        
        For i = 1 To intCountOfPatterns
        
            objRegExpObjects.Add New VBScript_RegExp_55.RegExp
        Next
    End If
    
    ReDim objRegExArray(1 To intCountOfPatterns)
    
    Set objSearchingFieldTitles = New Collection
    
    i = 1
    
    For Each varPair In vobjFieldTitleAndPatterns
    
        Set objPair = varPair
        
        varFieldTitle = CStr(objPair.Item(1))
        
        objSearchingFieldTitles.Add varFieldTitle
    
        Set objRegExArray(i) = objRegExpObjects.Item(i)
        
        objRegExArray(i).Pattern = CStr(objPair.Item(2))
        
        i = i + 1
    Next
    
    Set objTitleToColumnIndexDic = GetFieldTitleToPositionIndexDic(objSearchingFieldTitles, vobjSheet)

    With vobjSheet
        
        ReDim blnMatchResults(1 To .UsedRange.Rows.Count, 1 To intCountOfPatterns)
        
        ReDim rvarHits(1 To .UsedRange.Rows.Count)
        
        For intRowIndex = 1 To .UsedRange.Rows.Count
        
            For i = 1 To intCountOfPatterns
            
                blnMatchResults(intRowIndex, i) = False
            Next
            
            rvarHits(intRowIndex) = False
        Next
    
        i = 1
        
        For Each varPair In vobjFieldTitleAndPatterns
        
            Set objPair = varPair
            
            varFieldTitle = CStr(objPair.Item(1))
            
            intColumnIndex = objTitleToColumnIndexDic.Item(varFieldTitle)
            
            ReDim varValues(1 To .UsedRange.Rows.Count, 1 To 1)
        
            varValues = .Cells(1, intColumnIndex).Resize(.UsedRange.Rows.Count, 1).Value
            
            For intRowIndex = vintFieldTitlesRowIndex + 1 To .UsedRange.Rows.Count
                
                If Not IsEmpty(varValues(intRowIndex, 1)) Then
                
                    If objRegExArray(i).Test(varValues(intRowIndex, 1)) Then
                    
                        blnMatchResults(intRowIndex, i) = True
                    End If
                End If
            Next
            
            i = i + 1
        Next
    
        If intCountOfPatterns = 1 Then
        
            For intRowIndex = vintFieldTitlesRowIndex + 1 To .UsedRange.Rows.Count
        
                If blnMatchResults(intRowIndex, 1) Then
                
                    rvarHits(intRowIndex) = blnMatchResults(intRowIndex, 1)
                End If
            Next
        Else
            For intRowIndex = vintFieldTitlesRowIndex + 1 To .UsedRange.Rows.Count
            
                For i = 1 To intCountOfPatterns
            
                    If i = 1 Then
                        
                        blnValue = blnMatchResults(intRowIndex, i)
                    Else
                        Select Case venmColumnsJoinLogical
                        
                            Case ColumnsJoinLogical.LogicalAND
                                
                                blnValue = blnValue And blnMatchResults(intRowIndex, i)
                            Case ColumnsJoinLogical.LogicalOR
                                
                                blnValue = blnValue Or blnMatchResults(intRowIndex, i)
                        End Select
                    End If
                Next
                
                If blnValue Then
                
                    rvarHits(intRowIndex) = blnValue
                End If
            Next
        End If
    End With
End Sub





