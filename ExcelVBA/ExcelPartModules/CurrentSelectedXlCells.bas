Attribute VB_Name = "CurrentSelectedXlCells"
'
'   Get some type information from the current selected Excel.Worksheet.Range
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 25/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About selected range
'**---------------------------------------------
'''
'''
'''
Public Function CurrentSelectedXlRange() As Excel.Range

    Dim objRange As Excel.Range
    
    Set objRange = Nothing
    
    On Error Resume Next
    
    Set objRange = Selection
    
    On Error GoTo 0
    
    If objRange Is Nothing Then
    
        Set objRange = ActiveCell
    End If
    
    Set CurrentSelectedXlRange = objRange
End Function

'''
'''
'''
Public Function GetSheetNameFromSelectedRange() As String

    Dim objRange As Excel.Range, strSheetName As String, objSheet As Excel.Worksheet
    
    strSheetName = ""
    
    Set objRange = CurrentSelectedXlRange()
    
    If Not objRange Is Nothing Then
    
        Set objSheet = objRange.Worksheet
        
        strSheetName = objSheet.Name
    End If

    GetSheetNameFromSelectedRange = strSheetName
End Function

'''
'''
'''
Public Function GetBookPathFromSelectedRange() As String

    Dim objRange As Excel.Range, strBookPath As String, objSheet As Excel.Worksheet, objBook As Excel.Workbook
    
    strBookPath = ""
    
    Set objRange = CurrentSelectedXlRange()
    
    If Not objRange Is Nothing Then
    
        Set objSheet = objRange.Worksheet
        
        Set objBook = objSheet.Parent
        
        strBookPath = objBook.FullName
    End If

    GetBookPathFromSelectedRange = strBookPath
End Function

'**---------------------------------------------
'** About column width or row height for selected range
'**---------------------------------------------
'''
'''
'''
Public Function GetSelectedCellsSizeLogText(ByVal vobjRange As Excel.Range) As String

    Dim strLog As String
    
    strLog = GetSelectedCellsAddressInfoLogText(vobjRange) & vbNewLine
 
    strLog = strLog & GetSelectedCellsColumnWidthLogText(vobjRange) & vbNewLine
    
    strLog = strLog & GetSelectedCellsRowHeightLogText(vobjRange)

    GetSelectedCellsSizeLogText = strLog
End Function

'''
'''
'''
Public Function GetSelectedCellsAddressInfoLogText(ByVal vobjRange As Excel.Range) As String

    Dim strLog As String
    
    With vobjRange
    
        strLog = "Range address: " & .Address
    
        If .Rows.Count > 1 Then
        
            strLog = strLog & ", Rows count: " & CStr(.Rows.Count)
        End If
        
        If .Columns.Count > 1 Then
        
            strLog = strLog & ", Columns count: " & CStr(.Columns.Count)
        End If
    End With

    GetSelectedCellsAddressInfoLogText = strLog
End Function


'''
'''
'''
Public Function GetSelectedCellsColumnWidthLogText(ByVal vobjRange As Excel.Range) As String

    Dim objMergedRange As Excel.Range, strLog As String
    
    If vobjRange.MergeCells Then
    
        Set objMergedRange = mfobjGetMergedArea(vobjRange)
        
        If Not IsNull(objMergedRange.ColumnWidth) Then
        
            strLog = "Selected merged cell ColumnWidth: " & CStr(GetSumOfColumnWidth(objMergedRange)) & " [Char unit], left-top cell ColumnWidth: " & CStr(vobjRange.Cells(1, 1).ColumnWidth) & " [Char unit]"
        End If
    Else
        If Not IsNull(vobjRange.ColumnWidth) Then
        
            strLog = "Selected cell ColumnWidth: " & CStr(vobjRange.ColumnWidth) & " [Char unit]"
        End If
    End If

    GetSelectedCellsColumnWidthLogText = strLog
End Function

'''
'''
'''
Public Function GetSelectedCellsRowHeightLogText(ByVal vobjRange As Excel.Range) As String

    Dim objMergedRange As Excel.Range, strLog As String
    
    strLog = ""
    
    If vobjRange.MergeCells Then
    
        Set objMergedRange = mfobjGetMergedArea(vobjRange)
        
        If Not IsNull(objMergedRange) Then
        
            strLog = "Selected merged cell RowHeight: " & CStr(GetSumOfRowHeight(objMergedRange)) & " [point], left-top cell RowHeight: " & CStr(vobjRange.Cells(1, 1).RowHeight) & " [point]"
        End If
    Else
        If Not IsNull(vobjRange.RowHeight) Then
        
            strLog = "Selected cell RowHeight: " & CStr(vobjRange.RowHeight) & " [point]"
        End If
    End If

    GetSelectedCellsRowHeightLogText = strLog
End Function

'''
'''
'''
Private Function mfobjGetMergedArea(ByRef robjRange As Excel.Range) As Excel.Range

    Dim objMergedRange As Excel.Range
    
    If robjRange.MergeCells Then
    
        On Error Resume Next
        
        ' If robjRange is a partial area Range object in a whole merged Range, then the following doesn't cause error.
        
        Set objMergedRange = robjRange.MergeArea
        
        If Err.Number <> 0 Then
        
            ' If robjRange is equals to a whole merged Range, then the following will be executed.
        
            Set objMergedRange = robjRange
        End If
        
        On Error GoTo 0
    End If

    Set mfobjGetMergedArea = objMergedRange
End Function


'''
'''
'''
Private Function GetSumOfColumnWidth(ByRef robjPluralOneAreaRange As Excel.Range) As Double

    Dim objRange As Excel.Range, j As Long, dblColumnWidth As Double
    
    dblColumnWidth = 0#
    
    With robjPluralOneAreaRange
    
        For j = 1 To .Columns.Count
        
            dblColumnWidth = dblColumnWidth + .Cells(1, j).ColumnWidth
        Next
    End With

    GetSumOfColumnWidth = dblColumnWidth
End Function

'''
'''
'''
Private Function GetSumOfRowHeight(ByRef robjPluralOneAreaRange As Excel.Range) As Double

    Dim objRange As Excel.Range, i As Long, dblRowHeight As Double
    
    dblRowHeight = 0#
    
    With robjPluralOneAreaRange
    
        For i = 1 To .Rows.Count
        
            dblRowHeight = dblRowHeight + .Cells(i, 1).RowHeight
        Next
    End With
    
    GetSumOfRowHeight = dblRowHeight
End Function

'**---------------------------------------------
'** About selected plural ranges
'**---------------------------------------------
'''
'''
'''
Public Function GetTextOfValueAndColumnWidthDelimitedCommaFromSelectedRange()

    Dim objRange As Excel.Range, strText As String
    
    strText = ""
    
    For Each objRange In CurrentSelectedXlRange()
    
        If strText <> "" Then
        
            strText = strText & ","
        End If
        
        With objRange
        
            strText = strText & CStr(.Value) & "," & CStr(.EntireColumn.ColumnWidth)
        End With
    Next

    GetTextOfValueAndColumnWidthDelimitedCommaFromSelectedRange = strText
End Function


'''
''' get selected-range text
'''
Public Function GetTextFromSelectedRange(Optional ByVal vstrEachItemPrefix As String = "", _
    Optional ByVal vblnExceptForNullValues As Boolean = False, _
    Optional ByVal vstrLeftBracketChar As String = "", _
    Optional ByVal vstrRightBracketChar As String = "", _
    Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True)

    Dim objRange As Excel.Range
    Dim varValues() As Variant, varAValue As Variant
    Dim intRowMax As Long, intColumnMax As Long
    Dim intRowIdx As Long, intColumnIdx As Long, i As Long
    
    Dim strText As String
    
    On Error Resume Next
    
    Set objRange = Selection
    
    On Error GoTo 0

    With objRange
    
        intRowMax = .Rows.Count
        
        intColumnMax = .Columns.Count
    End With
    
    ReDim varValues(1 To intRowMax, 1 To intColumnMax)
    
    varValues = objRange.Resize(intRowMax, intColumnMax).Value
    
    strText = "": i = 0
    
    For intRowIdx = 1 To intRowMax
    
        For intColumnIdx = 1 To intColumnMax
            
            'Debug.Print varValues(intRowIdx, intColumnIdx)
            
            varAValue = varValues(intRowIdx, intColumnIdx)
            
            If vblnExceptForNullValues Then
            
                If Not IsEmpty(varAValue) Then
                    
                    If varAValue <> "" Then
                    
                        If i > 0 Then
                        
                            If vblnIncludeSpaceAfterDelimitedChar Then
                        
                                strText = strText & ", "
                            Else
                                strText = strText & ","
                            End If
                        End If
                    
                        strText = strText & vstrEachItemPrefix & vstrLeftBracketChar & varAValue & vstrRightBracketChar
                        
                        i = i + 1
                    End If
                End If
            Else
                strText = strText & vstrEachItemPrefix & vstrLeftBracketChar & varAValue & vstrRightBracketChar
                
                If intColumnIdx < intColumnMax Or intRowIdx < intRowMax Then
                
                    If vblnIncludeSpaceAfterDelimitedChar Then
                    
                        strText = strText & ", "
                    Else
                        strText = strText & ","
                    End If
                End If
            End If
        Next
    Next

    GetTextFromSelectedRange = strText
End Function
