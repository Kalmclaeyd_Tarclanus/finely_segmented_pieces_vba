Attribute VB_Name = "RegExpSheetIn"
'
'   get information from Excel.Worksheet using RegExp objects
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon,  6/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjXlCellFormulaRegExp As VBScript_RegExp_55.RegExp

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' get '="*"' formula expression from FormatCondition.Formula1, because formula expression texts can be set to Range.Value
'''
Public Function GetInterFormulaOfCellExpression(ByVal vstrFormula As String) As String

    Dim objMatch As VBScript_RegExp_55.Match
    Dim strGetting As String

    strGetting = vstrFormula

    With GetCacheXlCellFormulaRegExp()
    
        If .Test(vstrFormula) Then
            
            With .Execute(vstrFormula)
            
                Set objMatch = .Item(0)
                
                strGetting = objMatch.SubMatches.Item(0)
            End With
        End If
    End With

    GetInterFormulaOfCellExpression = strGetting
End Function

'''
'''
'''
Public Function GetCacheXlCellFormulaRegExp() As VBScript_RegExp_55.RegExp
    
    If mobjXlCellFormulaRegExp Is Nothing Then
        
        Set mobjXlCellFormulaRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjXlCellFormulaRegExp
        
            .Global = True
            
            .Pattern = "\=""([\uFF61-\uFF9F\(\)\[\]\-\/ij`-y-O-X\|\D_a-zA-Z0-9κ-κVXYZ-ρ@-]{0,})"""
        End With
    End If

    Set GetCacheXlCellFormulaRegExp = mobjXlCellFormulaRegExp
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' RegExp hit test
'''
Private Sub msubSanityTestOfCellFormulaRegExp()
    
    Dim objRegExp As VBScript_RegExp_55.RegExp
    
    Set objRegExp = GetCacheXlCellFormulaRegExp()

   
   
    msubHitTest "=""abc""", objRegExp
    
    msubHitTest "=""Z305(ΔΈΞabc)""", objRegExp
    
    Debug.Print GetInterFormulaOfCellExpression("=""A3T1[C³]""")
    
End Sub

'''
'''
'''
Private Sub msubHitTest(ByRef rstrTestString As String, _
        ByRef robjRegExp As VBScript_RegExp_55.RegExp)

    Debug.Print GetLogTextOfRegExpHitTest(rstrTestString, robjRegExp)
End Sub

