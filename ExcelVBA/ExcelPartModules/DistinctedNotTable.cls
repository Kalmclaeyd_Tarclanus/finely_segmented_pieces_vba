VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DistinctedNotTable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Distincted not table area by NotTableDistinctCondition
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on FindForNotTablesOnSheet.bas, VariantTypeConversion.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 20/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjNotTableDistinctCondition As NotTableDistinctCondition

Private mobjMatchedSheet As Excel.Worksheet

Private mstrDistinctedRangeAddress As String

Private mobjAdditionalRowIndexToFoundValuesDic As Scripting.Dictionary

Private mstrIdentifierName As String

Private mobjAdditionalInfoTypeNameToExtractedValueDic As Scripting.Dictionary  ' Dictionary(Of String[AdditionalInfoTypeName], Variant)

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Property Get DistinctCondition() As NotTableDistinctCondition

    Set DistinctCondition = mobjNotTableDistinctCondition
End Property
Property Set DistinctCondition(ByVal vobjNotTableDistinctCondition As NotTableDistinctCondition)

    Set mobjNotTableDistinctCondition = vobjNotTableDistinctCondition
End Property

'''
'''
'''
Property Get MatchedSheet() As Excel.Worksheet

    Set MatchedSheet = mobjMatchedSheet
End Property
Property Set MatchedSheet(ByVal vobjMatchedSheet As Excel.Worksheet)

    Set mobjMatchedSheet = vobjMatchedSheet
End Property

'''
'''
'''
Property Get DistinctedRangeAddress() As String

    DistinctedRangeAddress = mstrDistinctedRangeAddress
End Property
Property Let DistinctedRangeAddress(ByVal vstrDistinctedRangeAddress As String)

    mstrDistinctedRangeAddress = vstrDistinctedRangeAddress
End Property

'''
'''
'''
Property Get AdditionalRowIndexToFoundValuesDic() As Scripting.Dictionary

    Set AdditionalRowIndexToFoundValuesDic = mobjAdditionalRowIndexToFoundValuesDic
End Property
Property Set AdditionalRowIndexToFoundValuesDic(ByVal vobjAdditionalRowIndexToFoundValuesDic As Scripting.Dictionary)

    Set mobjAdditionalRowIndexToFoundValuesDic = vobjAdditionalRowIndexToFoundValuesDic
End Property

'''
''' Dictionary(Of String[AdditionalInfoTypeName], Variant[Extracted from AdditionalRowIndexToFoundValuesDic values])
'''
Property Get AdditionalInfoTypeNameToExtractedValueDic() As Scripting.Dictionary

    Set AdditionalInfoTypeNameToExtractedValueDic = mobjAdditionalInfoTypeNameToExtractedValueDic
End Property
Property Set AdditionalInfoTypeNameToExtractedValueDic(ByVal vobjAdditionalInfoTypeNameToExtractedValueDic As Scripting.Dictionary)

    Set mobjAdditionalInfoTypeNameToExtractedValueDic = vobjAdditionalInfoTypeNameToExtractedValueDic
End Property

'''
'''
'''
Public Property Get IdentifierName() As String

    IdentifierName = mstrIdentifierName
End Property
Public Property Let IdentifierName(ByVal vstrIdentifierName As String)

    mstrIdentifierName = vstrIdentifierName
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub GetRCDataTableAfterCorrectingProcess(ByRef robjDTCol As Collection, ByRef robjFieldTitlesCol As Collection)

    With Me
    
        GetRCDataTableAfterCorrectingProcessByFindingRowValuesPatterns robjDTCol, robjFieldTitlesCol, .MatchedSheet.Range(.DistinctedRangeAddress), .DistinctCondition.ExtractTableLineValuesPatternKeyToProcessingFlagDic
    End With
End Sub

'''
'''
'''
Public Sub GetRCDataTableOfOnlySelectedFieldTitlesAfterCorrectingProcess(ByRef robjDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vobjSelectedFieldTitles As Collection, _
        Optional ByVal vobjIsNotNothingConditionFieldTitles As Collection = Nothing, _
        Optional ByVal vobjInUsedValuesConditionFieldTitleToValuesDelimitedCommaDic As Scripting.Dictionary = Nothing)

    With Me
    
        GetRCDataTableOfOnlySelectedFieldTitlesAfterCorrectingProcessByFindingRowValuesPatterns robjDTCol, robjFieldTitlesCol, vobjSelectedFieldTitles, .MatchedSheet.Range(.DistinctedRangeAddress), .DistinctCondition.ExtractTableLineValuesPatternKeyToProcessingFlagDic, vobjIsNotNothingConditionFieldTitles, vobjInUsedValuesConditionFieldTitleToValuesDelimitedCommaDic
    End With
End Sub



'''
'''
'''
Public Sub SetIdentifierNameAutomaticallyFromDistinctedTableTypeNameAndAdditionalRowIndexToFoundValuesDic()

    Dim strSuffix As String, varItem As Variant
    
    strSuffix = ""
    
    If Not mobjAdditionalInfoTypeNameToExtractedValueDic Is Nothing Then
        
        With mobjAdditionalInfoTypeNameToExtractedValueDic
        
            For Each varItem In .Items
            
                strSuffix = strSuffix & "_" & CStr(varItem)
            Next
        End With
    End If
    
    Me.IdentifierName = Me.DistinctCondition.DistinctedTableTypeName & strSuffix
End Sub


'''
'''
'''
Public Function GetAllConnectedInfosFromAdditionalRowIndexToFoundValuesDic() As String
 
    Dim strText As String, varActualRowValues As Variant, objActualRowValues As Collection
    
    strText = ""
    
    If Not mobjAdditionalRowIndexToFoundValuesDic Is Nothing Then
    
        With mobjAdditionalRowIndexToFoundValuesDic
        
            For Each varActualRowValues In .Items
        
                Set objActualRowValues = varActualRowValues
            
                If strText <> "" Then
                
                    strText = strText & ","
                End If
            
                strText = strText & GetLineTextFromCol(objActualRowValues, "", ",", False)
            Next
        End With
    End If

    GetAllConnectedInfosFromAdditionalRowIndexToFoundValuesDic = strText
End Function

