Attribute VB_Name = "DataTableSecurity"
'
'   Lock and unlock Excel book by password
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 13/Jun/2022    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mintColorOfHiddenToVisible As Long = 714480

Private Const mintColorOfVeryHiddenToVisible As Long = 657930

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToDataTableSecurity()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "OfficeFileSecurity,OfficeBookSecurity"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About locking password of a Workbook
'**---------------------------------------------
'''
'''
'''
Public Sub ChangeLockPasswordOfBook(ByVal vstrBookPath As String, ByVal vstrPreviousPassword As String, ByVal vstrNewPassword As String)

    Dim objBook As Excel.Workbook
    
    Set objBook = GetWorkbook(vstrBookPath, False, False, vstrPassword:=vstrPreviousPassword)
    
    If Not objBook Is Nothing Then
    
        ForceToSaveAsBookWithoutDisplayAlert objBook, vstrBookPath, vstrPassword:=vstrNewPassword
        
        objBook.Close
    End If
End Sub


'**---------------------------------------------
'** About a Worksheet protection
'**---------------------------------------------
'''
''' when the all sheets are unprotected, return false
'''
Public Function IsProtectedSheetIncluded(ByVal vobjBook As Excel.Workbook) As Boolean
    
    Dim objSheet As Object, blnFound As Boolean
    
    blnFound = False
    
    For Each objSheet In vobjBook.Sheets
    
        If objSheet.ProtectContents Or objSheet.ProtectDrawingObjects Then
        
            blnFound = True
        
            Exit For
        End If
    Next

    IsProtectedSheetIncluded = blnFound
End Function


'''
''' when the all sheets are visible, return false
'''
Public Function IsInvisibleSheetIncluded(ByVal vobjBook As Excel.Workbook) As Boolean
    
    Dim objSheet As Object   ' Worksheet or Chart
    Dim blnFound As Boolean
    
    blnFound = False
    
    For Each objSheet In vobjBook.Sheets
    
        If objSheet.Visible <> xlSheetVisible Then
        
            blnFound = True
        
            Exit For
        End If
    Next

    IsInvisibleSheetIncluded = blnFound
End Function

'''
''' change Visible of either the Excel.Worksheet or the Excel.Chart
'''
''' <Argument>vobjBook: Input</Argument>
''' <Argument>vobjSheetNameToVisibleParam: Input - Dictionary(Of Key[String: sheet name], Of Value[XlSheetVisibility])</Argument>
''' <Argument>vstrSheetProtectPassword: Input</Argument>
Public Sub SetVisibilitiesForEachSheet(ByVal vobjBook As Excel.Workbook, _
        ByVal vobjSheetNameToVisibleParam As Scripting.Dictionary, _
        Optional ByVal vstrSheetProtectPassword As String = "")
    
    Dim objSheet As Object   ' Worksheet or Chart
    Dim varSheetName As Variant, strSheetName As String
    Dim enmVisible As XlSheetVisibility, blnAlreadyProtected As Boolean
    
    With vobjSheetNameToVisibleParam
    
        For Each varSheetName In .Keys
        
            strSheetName = varSheetName
        
            Set objSheet = Nothing
            
            On Error Resume Next
            
            Set objSheet = vobjBook.Sheets.Item(strSheetName)   ' Worksheet or Chart
            
            On Error GoTo 0
            
            If Not objSheet Is Nothing Then
            
                blnAlreadyProtected = False
            
                With objSheet
                
                    enmVisible = vobjSheetNameToVisibleParam.Item(varSheetName)
                    
                    If .Visible <> enmVisible Then
                    
                        If .ProtectContents Or .ProtectDrawingObjects Then ' Worksheet or Chart
                        
                            .Unprotect vstrSheetProtectPassword
                            
                            blnAlreadyProtected = True
                        End If
                        
                        .Visible = enmVisible
                        
                        If blnAlreadyProtected Or vstrSheetProtectPassword <> "" Then
                        
                            .Protect vstrSheetProtectPassword
                        End If
                    End If
                End With
            End If
        Next
    End With
End Sub

'''
''' Remove invisible Worksheet and change the sheet tab color
'''
''' <Argument>vobjBook: Input</Argument>
''' <Argument>vstrSheetProtectPassword: Input</Argument>
Public Sub ChangeVisibleWithTabColorForEachSheet(ByVal vobjBook As Excel.Workbook, _
        Optional ByVal vstrSheetProtectPassword As String = "")

    Dim objSheet As Object 'Worksheet
    Dim intChangeColor As Long

    For Each objSheet In vobjBook.Sheets
    
        With objSheet
        
            If .Visible <> xlSheetVisible Then
            
                If .ProtectContents Or .ProtectDrawingObjects Then
                
                    .Unprotect vstrSheetProtectPassword
                End If
                
                Select Case .Visible
                
                    Case XlSheetVisibility.xlSheetHidden
                    
                        intChangeColor = mintColorOfHiddenToVisible
                        
                    Case XlSheetVisibility.xlSheetVeryHidden
                    
                        intChangeColor = mintColorOfVeryHiddenToVisible
                End Select
                
                .Visible = xlSheetVisible
                
                .Tab.Color = intChangeColor
            End If
        End With
    Next
End Sub

'''
''' Set default of the sheet visiblity in accord with the sheet tab color
'''
''' <Argument>vobjBook: Input</Argument>
''' <Argument>vstrSheetProtectPassword: Input</Argument>
Public Sub ReturnVisibilitiesFromTabColorForEachSheet(ByVal vobjBook As Excel.Workbook, _
        Optional ByVal vstrSheetProtectPassword As String = "")

    Dim objSheet As Excel.Worksheet
    Dim intChangeColor As Long, enmVisibility As XlSheetVisibility
    
    For Each objSheet In vobjBook.Sheets
    
        With objSheet
        
            Select Case .Tab.Color
            
                Case mintColorOfHiddenToVisible
                
                    enmVisibility = xlSheetHidden
                    
                Case mintColorOfVeryHiddenToVisible
                
                    enmVisibility = xlSheetVeryHidden
            End Select
        
            If .Visible <> enmVisibility Then
            
                If .ProtectContents Or .ProtectDrawingObjects Then
                
                    .Unprotect vstrSheetProtectPassword
                End If
                
                .Visible = xlSheetVisible
                
                With .Tab
                
                    .Color = RGB(255, 255, 255): .ColorIndex = xlColorIndexNone
                End With
                
                If vstrSheetProtectPassword <> "" Then
                
                    .Protect vstrSheetProtectPassword
                End If
            End If
        End With
    Next
End Sub

'**---------------------------------------------
'** About a Workbook structure protection
'**---------------------------------------------
'''
'''
'''
''' <Argument>vobjBook: Input</Argument>
''' <Argument>vstrSheetProtectPassword: Input</Argument>
Public Sub SetBookStructureProtection(ByVal vobjBook As Excel.Workbook, _
        Optional ByVal vstrBookProtectPassword As String = "")

    If vstrBookProtectPassword <> "" Then
    
        With vobjBook
        
            If Not .ProtectStructure Or Not .ProtectWindows Then
            
                .Protect vstrBookProtectPassword, True, False
            End If
        End With
    End If
End Sub

'''
'''
'''
''' <Argument>vobjBook: Input</Argument>
''' <Argument>vstrSheetProtectPassword: Input</Argument>
Public Sub RemoveBookStructureProtection(ByVal vobjBook As Excel.Workbook, _
        Optional ByVal vstrBookProtectPassword As String = "")
    
    If vstrBookProtectPassword <> "" Then
        
        With vobjBook
            
            If .ProtectStructure Or .ProtectWindows Then
            
                .Unprotect vstrBookProtectPassword
            End If
        End With
    End If
End Sub


'''
''' set readonly recommendation
'''
Public Sub SetReadonlyRecommended(ByVal vobjBook As Excel.Workbook, Optional ByVal vblnWithSaving As Boolean = False)
    
    Dim objApplication As Excel.Application, blnOldDisplayAlerts As Boolean
    
    Set objApplication = vobjBook.Application
    
    On Error GoTo ErrHandler:
    
    blnOldDisplayAlerts = objApplication.DisplayAlerts
    
    objApplication.DisplayAlerts = False
    
    vobjBook.ReadOnlyRecommended = True
    
    If vblnWithSaving Then
    
        vobjBook.SaveAs vobjBook.FullName
    End If
ErrHandler:

    objApplication.DisplayAlerts = blnOldDisplayAlerts
End Sub

'''
''' remove readonly recommendation
'''
Public Sub RemoveReadonlyRecommended(ByVal vobjBook As Excel.Workbook, Optional vblnWithSaving As Boolean = False)
    
    Dim objApplication As Excel.Application, blnOldDisplayAlerts As Boolean
    
    Set objApplication = vobjBook.Application
    
    On Error GoTo ErrHandler:
    
    blnOldDisplayAlerts = objApplication.DisplayAlerts
    
    objApplication.DisplayAlerts = False
    
    vobjBook.ReadOnlyRecommended = False
    
    If vblnWithSaving Then
    
        vobjBook.SaveAs vobjBook.FullName
    End If
    
ErrHandler:
    objApplication.DisplayAlerts = blnOldDisplayAlerts
End Sub

'**---------------------------------------------
'** get Excel.Workbook security state
'**---------------------------------------------
'''
'''
'''
Public Function GetPluralBooksSheetSecurityInfoDic(ByRef robjApplication As Excel.Application) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, objChildDic As Scripting.Dictionary, objBook As Excel.Workbook

    
    Set objDic = New Scripting.Dictionary
    
    For Each objBook In robjApplication.Workbooks
    
        Set objChildDic = GetBookSheetSecurityInfoDic(objBook)
        
        objDic.Add objBook.Name, objChildDic
    Next

    Set GetPluralBooksSheetSecurityInfoDic = objDic
End Function

'''
'''
'''
Public Function GetBookSheetSecurityInfoDic(ByRef robjBook As Excel.Workbook) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, objSheet As Excel.Worksheet, objRowCol As Collection
    
    Set objDic = New Scripting.Dictionary
    
    With robjBook
    
        For Each objSheet In .Worksheets
        
            With objSheet
            
                Set objRowCol = New Collection
                
                objRowCol.Add GetTextFromEnmOfXlSheetVisibility(.Visible)
                
                objRowCol.Add .ProtectContents
            
                objRowCol.Add .ProtectDrawingObjects
                
                objDic.Add .Name, objRowCol
            End With
        Next
    End With

    Set GetBookSheetSecurityInfoDic = objDic
End Function


'**---------------------------------------------
'** About enumeration conversion
'**---------------------------------------------
'''
'''
'''
Public Function GetTextFromEnmOfXlSheetVisibility(ByVal venmXlSheetVisibility As Excel.XlSheetVisibility) As String

    Dim strText As String

    Select Case venmXlSheetVisibility
    
        Case Excel.XlSheetVisibility.xlSheetVisible
        
            strText = "xlSheetVisible"
        
        Case Excel.XlSheetVisibility.xlSheetHidden
        
            strText = "xlSheetHidden"
        
        Case Excel.XlSheetVisibility.xlSheetVeryHidden
        
            strText = "xlSheetVeryHidden"
    End Select

    GetTextFromEnmOfXlSheetVisibility = strText
End Function

'''
'''
'''
Public Function GetEnmFromTextOfXlSheetVisibility(ByVal vstrText As String) As Excel.XlSheetVisibility

    Dim enmValue As Excel.XlSheetVisibility

    Select Case vstrText
    
        Case "xlSheetVisible"
        
            enmValue = Excel.XlSheetVisibility.xlSheetVisible
        
        Case "xlSheetHidden"
        
            enmValue = Excel.XlSheetVisibility.xlSheetHidden
        
        Case "xlSheetVeryHidden"
        
            enmValue = Excel.XlSheetVisibility.xlSheetVeryHidden
    End Select

    GetEnmFromTextOfXlSheetVisibility = enmValue
End Function

