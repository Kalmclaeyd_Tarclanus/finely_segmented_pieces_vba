VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DataTableSheetFormatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'
'   A general table format parameters class for Excel Worksheet objects, however this is dependent on either the ADO or the SQL language
'   This has no relation with Excel.DataTable class of Excel.Chart class.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Independent on either the ADO or the SQL
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 25/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'       Tue,  7/Feb/2023    Kalmclaeyd Tarclanus    Separated ADO dependent codes to ADOSheetFormatter
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IDataTableSheetFormatter

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjCommonDataTableSheetFormatter As CommonDataTableSheetFormatter

Private mitfCommonDataTableSheetFormatter As ICommonDataTableSheetFormatter


Private mintFieldTitlesRowIndex As Long  ' Main state parameters, this is auto adjusted by SQL logging option

'**---------------------------------------------
'** Capsuled objects or parameters
'**---------------------------------------------

Private mblnAllowToWriteInsertTextInSheetHeader As Boolean

Private mobjHeaderInsertTexts As Collection

Private mblnAllowToSetAutoFilter As Boolean


Private mobjFieldTitleToColumnWidthDic As Scripting.Dictionary   ' Dictionary(Of String(column title), Double(column-width))

Private mobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary

Private mobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam

Private mobjColumnsFormatConditionParam As ColumnsFormatConditionParam


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub Class_Initialize()

    Set mobjCommonDataTableSheetFormatter = New CommonDataTableSheetFormatter

    Set mitfCommonDataTableSheetFormatter = mobjCommonDataTableSheetFormatter

    mintFieldTitlesRowIndex = 1
    
    
    mblnAllowToWriteInsertTextInSheetHeader = True
    
    Set mobjHeaderInsertTexts = Nothing

    mblnAllowToSetAutoFilter = False
    
    Set mobjColumnsNumberFormatLocalParam = Nothing
    
    Set mobjColumnsFormatConditionParam = Nothing
    
    
    Set mobjFieldTitleToColumnWidthDic = Nothing
    
    Set mobjFieldTitleOrderToColumnWidthDic = Nothing
End Sub

'''
'''
'''
Private Sub Class_Terminate()

    Set mitfCommonDataTableSheetFormatter = Nothing
    
    Set mobjCommonDataTableSheetFormatter = Nothing
    
    
    Set mobjHeaderInsertTexts = Nothing
    
    Set mobjFieldTitleToColumnWidthDic = Nothing
    
    Set mobjColumnsNumberFormatLocalParam = Nothing
    
    Set mobjColumnsFormatConditionParam = Nothing
End Sub


'///////////////////////////////////////////////
'/// Implemented properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** For ICommonDataTableSheetFormatter
'**---------------------------------------------
'''
'''
'''
Private Property Get IDataTableSheetFormatter_TopLeftRowIndex() As Long

    IDataTableSheetFormatter_TopLeftRowIndex = mitfCommonDataTableSheetFormatter.TopLeftRowIndex
End Property
Private Property Let IDataTableSheetFormatter_TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)

    mitfCommonDataTableSheetFormatter.TopLeftRowIndex = vintTopLeftRowIndex
End Property

Private Property Get IDataTableSheetFormatter_TopLeftColumnIndex() As Long

    IDataTableSheetFormatter_TopLeftColumnIndex = mitfCommonDataTableSheetFormatter.TopLeftColumnIndex
End Property
Private Property Let IDataTableSheetFormatter_TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)

    mitfCommonDataTableSheetFormatter.TopLeftColumnIndex = vintTopLeftColumnIndex
End Property


Private Property Get IDataTableSheetFormatter_AllowToShowFieldTitle() As Boolean

    IDataTableSheetFormatter_AllowToShowFieldTitle = mitfCommonDataTableSheetFormatter.AllowToShowFieldTitle
End Property
Private Property Let IDataTableSheetFormatter_AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToShowFieldTitle = vblnAllowToShowFieldTitle
End Property

Private Property Get IDataTableSheetFormatter_FieldTitleInteriorType() As FieldTitleInterior

    IDataTableSheetFormatter_FieldTitleInteriorType = mitfCommonDataTableSheetFormatter.FieldTitleInteriorType
End Property
Private Property Let IDataTableSheetFormatter_FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)

    mitfCommonDataTableSheetFormatter.FieldTitleInteriorType = venmFieldTitleInteriorType
End Property

Private Property Get IDataTableSheetFormatter_RecordBordersType() As RecordCellsBorders

    IDataTableSheetFormatter_RecordBordersType = mitfCommonDataTableSheetFormatter.RecordBordersType
End Property
Private Property Let IDataTableSheetFormatter_RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)

    mitfCommonDataTableSheetFormatter.RecordBordersType = venmRecordCellsBordersType
End Property

Private Property Get IDataTableSheetFormatter_RecordsFontSize() As Single

    IDataTableSheetFormatter_RecordsFontSize = mitfCommonDataTableSheetFormatter.RecordsFontSize
End Property
Private Property Let IDataTableSheetFormatter_RecordsFontSize(ByVal vsngRecordsFontSize As Single)

    mitfCommonDataTableSheetFormatter.RecordsFontSize = vsngRecordsFontSize
End Property


'''
''' When it is true, then ConvertDataForColumns procedure is used.
'''
Private Property Get IDataTableSheetFormatter_AllowToConvertDataForSpecifiedColumns() As Boolean

    IDataTableSheetFormatter_AllowToConvertDataForSpecifiedColumns = mitfCommonDataTableSheetFormatter.AllowToConvertDataForEachFieldTitles
End Property
Private Property Let IDataTableSheetFormatter_AllowToConvertDataForSpecifiedColumns(ByVal vblnAllowToConvertDataForSpecifiedColumns As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToConvertDataForEachFieldTitles = vblnAllowToConvertDataForSpecifiedColumns
End Property


'''
''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
'''
Private Property Set IDataTableSheetFormatter_FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)

    Set mitfCommonDataTableSheetFormatter.FieldTitlesToColumnDataConvertTypeDic = mitfCommonDataTableSheetFormatter.FieldTitlesToColumnDataConvertTypeDic
End Property
Private Property Get IDataTableSheetFormatter_FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary

    Set IDataTableSheetFormatter_FieldTitlesToColumnDataConvertTypeDic = mitfCommonDataTableSheetFormatter.FieldTitlesToColumnDataConvertTypeDic
End Property



'''
''' merge cells when the same values continue
'''
Private Property Get IDataTableSheetFormatter_AllowToMergeCellsByContinuousSameValues() As Boolean

    IDataTableSheetFormatter_AllowToMergeCellsByContinuousSameValues = mitfCommonDataTableSheetFormatter.AllowToMergeCellsByContinuousSameValues
End Property
Private Property Let IDataTableSheetFormatter_AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
End Property


Private Property Get IDataTableSheetFormatter_AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean

   IDataTableSheetFormatter_AllowToSetVerticalTopAlignmentTopOfMergeCells = mitfCommonDataTableSheetFormatter.AllowToSetVerticalTopAlignmentTopOfMergeCells
End Property
Private Property Let IDataTableSheetFormatter_AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
End Property


Private Property Get IDataTableSheetFormatter_FieldTitlesToMergeCells() As Collection

    Set IDataTableSheetFormatter_FieldTitlesToMergeCells = mitfCommonDataTableSheetFormatter.FieldTitlesToMergeCells
End Property
Private Property Set IDataTableSheetFormatter_FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)

    Set mitfCommonDataTableSheetFormatter.FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
End Property



'''
''' Enabling WrapText for each cells
'''
Private Property Get IDataTableSheetFormatter_AllowToSetEnablingWrapText() As Boolean

    IDataTableSheetFormatter_AllowToSetEnablingWrapText = mitfCommonDataTableSheetFormatter.AllowToSetEnablingWrapText
End Property
Private Property Let IDataTableSheetFormatter_AllowToSetEnablingWrapText(ByVal vblnAllowToSetEnablingWrapText As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToSetEnablingWrapText = vblnAllowToSetEnablingWrapText
End Property


Private Property Get IDataTableSheetFormatter_FieldTitlesToSetEnablingWrapTextCells() As Collection

    Set IDataTableSheetFormatter_FieldTitlesToSetEnablingWrapTextCells = mitfCommonDataTableSheetFormatter.FieldTitlesToSetEnablingWrapTextCells
End Property
Private Property Set IDataTableSheetFormatter_FieldTitlesToSetEnablingWrapTextCells(ByVal vobjFieldTitlesToSetEnablingWrapTextCells As Collection)

    Set mitfCommonDataTableSheetFormatter.FieldTitlesToSetEnablingWrapTextCells = vobjFieldTitlesToSetEnablingWrapTextCells
End Property

'''
''' Dictionary(Of String[Field-title], Excel.Constants)
'''
Private Property Get IDataTableSheetFormatter_FieldTitleToHorizontalAlignmentDic() As Scripting.Dictionary

    Set IDataTableSheetFormatter_FieldTitleToHorizontalAlignmentDic = mitfCommonDataTableSheetFormatter.FieldTitleToHorizontalAlignmentDic
End Property
Private Property Set IDataTableSheetFormatter_FieldTitleToHorizontalAlignmentDic(ByVal vobjDic As Scripting.Dictionary)

    Set mitfCommonDataTableSheetFormatter.FieldTitleToHorizontalAlignmentDic = vobjDic
End Property

'''
''' If mblnAllowToAutoFitRowsHeight is true, Excel.Worksheet.Rows.AutoFit method is to be executed
'''
Private Property Get IDataTableSheetFormatter_AllowToAutoFitRowsHeight() As Boolean

    IDataTableSheetFormatter_AllowToAutoFitRowsHeight = mitfCommonDataTableSheetFormatter.AllowToAutoFitRowsHeight
End Property
Private Property Let IDataTableSheetFormatter_AllowToAutoFitRowsHeight(ByVal vblnAllowToAutoFitRowsHeight As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToAutoFitRowsHeight = vblnAllowToAutoFitRowsHeight
End Property


'**---------------------------------------------
'** For only IDataTableSheetFormatter
'**---------------------------------------------
'''
''' This is decided from both
'''
Private Property Get IDataTableSheetFormatter_FieldTitlesRowIndex() As Long

    IDataTableSheetFormatter_FieldTitlesRowIndex = mintFieldTitlesRowIndex
End Property
Private Property Let IDataTableSheetFormatter_FieldTitlesRowIndex(ByVal vintFieldTitlesRowIndex As Long)

    mintFieldTitlesRowIndex = vintFieldTitlesRowIndex
End Property



Private Property Get IDataTableSheetFormatter_AllowToSetAutoFilter() As Boolean

    IDataTableSheetFormatter_AllowToSetAutoFilter = mblnAllowToSetAutoFilter
End Property
Private Property Let IDataTableSheetFormatter_AllowToSetAutoFilter(ByVal vblnAllowToSetAutoFilter As Boolean)

    mblnAllowToSetAutoFilter = vblnAllowToSetAutoFilter
End Property

'''
''' Dictionary(Of String(column title), Double(column-width))
'''
Private Property Get IDataTableSheetFormatter_FieldTitleToColumnWidthDic() As Scripting.Dictionary

    Set IDataTableSheetFormatter_FieldTitleToColumnWidthDic = mobjFieldTitleToColumnWidthDic
End Property
Private Property Set IDataTableSheetFormatter_FieldTitleToColumnWidthDic(ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary)

    Set mobjFieldTitleToColumnWidthDic = vobjFieldTitleToColumnWidthDic
End Property

'''
''' Dictionary(Of String(column title order, such as 'Col1', 'Col07'), Double(column-width))
'''
Private Property Get IDataTableSheetFormatter_FieldTitleOrderToColumnWidthDic() As Scripting.Dictionary

    Set IDataTableSheetFormatter_FieldTitleOrderToColumnWidthDic = mobjFieldTitleOrderToColumnWidthDic
End Property
Private Property Set IDataTableSheetFormatter_FieldTitleOrderToColumnWidthDic(ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary)

    Set mobjFieldTitleOrderToColumnWidthDic = vobjFieldTitleOrderToColumnWidthDic
End Property


'''
''' ColumnsNumberFormatLocalParam
'''
Private Property Get IDataTableSheetFormatter_ColumnsNumberFormatLocal() As ColumnsNumberFormatLocalParam

    Set IDataTableSheetFormatter_ColumnsNumberFormatLocal = mobjColumnsNumberFormatLocalParam
End Property
Private Property Set IDataTableSheetFormatter_ColumnsNumberFormatLocal(ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)

    Set mobjColumnsNumberFormatLocalParam = vobjColumnsNumberFormatLocalParam
End Property


'''
''' set format-condition for each column
'''
Private Property Get IDataTableSheetFormatter_ColumnsFormatCondition() As ColumnsFormatConditionParam

    Set IDataTableSheetFormatter_ColumnsFormatCondition = mobjColumnsFormatConditionParam
End Property
Private Property Set IDataTableSheetFormatter_ColumnsFormatCondition(ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)

    Set mobjColumnsFormatConditionParam = vobjColumnsFormatConditionParam
End Property


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** Public interface read-only properties
'**---------------------------------------------
'''
'''
'''
Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter

    Set DataTableSheetFormattingInterface = Me
End Property

'''
'''
'''
Public Property Get CommonDataTableSheetFormattingInterface() As ICommonDataTableSheetFormatter

    Set CommonDataTableSheetFormattingInterface = mitfCommonDataTableSheetFormatter
End Property

'**---------------------------------------------
'** normal DataTableSheetFormatter properties
'**---------------------------------------------
'''
''' This is decided from both
'''
Public Property Get FieldTitlesRowIndex() As Long

    FieldTitlesRowIndex = IDataTableSheetFormatter_FieldTitlesRowIndex
End Property
Public Property Let FieldTitlesRowIndex(ByVal vintFieldTitlesRowIndex As Long)

    IDataTableSheetFormatter_FieldTitlesRowIndex = vintFieldTitlesRowIndex
End Property

'''
'''
'''
Public Property Get TopLeftRowIndex() As Long

    TopLeftRowIndex = IDataTableSheetFormatter_TopLeftRowIndex
End Property
Public Property Let TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)

    IDataTableSheetFormatter_TopLeftRowIndex = vintTopLeftRowIndex
End Property

Public Property Get TopLeftColumnIndex() As Long

    TopLeftColumnIndex = IDataTableSheetFormatter_TopLeftColumnIndex
End Property
Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)

    IDataTableSheetFormatter_TopLeftColumnIndex = vintTopLeftColumnIndex
End Property



Public Property Get AllowToShowFieldTitle() As Boolean

    AllowToShowFieldTitle = IDataTableSheetFormatter_AllowToShowFieldTitle
End Property
Public Property Let AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)

    IDataTableSheetFormatter_AllowToShowFieldTitle = vblnAllowToShowFieldTitle
End Property

Public Property Get FieldTitleInteriorType() As FieldTitleInterior

    FieldTitleInteriorType = IDataTableSheetFormatter_FieldTitleInteriorType
End Property
Public Property Let FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)

    IDataTableSheetFormatter_FieldTitleInteriorType = venmFieldTitleInteriorType
End Property

Public Property Get RecordBordersType() As RecordCellsBorders

    RecordBordersType = IDataTableSheetFormatter_RecordBordersType
End Property
Public Property Let RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)

    IDataTableSheetFormatter_RecordBordersType = venmRecordCellsBordersType
End Property

Public Property Get RecordsFontSize() As Single

    RecordsFontSize = IDataTableSheetFormatter_RecordsFontSize
End Property
Public Property Let RecordsFontSize(ByVal vsngRecordsFontSize As Single)

    IDataTableSheetFormatter_RecordsFontSize = vsngRecordsFontSize
End Property


'**---------------------------------------------
'** Properties - connoted sheet-formatting objects or paramters
'**---------------------------------------------
Public Property Get AllowToWriteInsertTextInSheetHeader() As Boolean

    AllowToWriteInsertTextInSheetHeader = mblnAllowToWriteInsertTextInSheetHeader
End Property
Public Property Let AllowToWriteInsertTextInSheetHeader(ByVal vblnAllowToWriteInsertTextInSheetHeader As Boolean)

    mblnAllowToWriteInsertTextInSheetHeader = vblnAllowToWriteInsertTextInSheetHeader
End Property


Public Property Get HeaderInsertTexts() As Collection

    Set HeaderInsertTexts = mobjHeaderInsertTexts
End Property
Public Property Set HeaderInsertTexts(ByVal vobjHeaderInsertTexts As Collection)

    Set mobjHeaderInsertTexts = vobjHeaderInsertTexts
End Property


Public Property Get AllowToSetAutoFilter() As Boolean

    AllowToSetAutoFilter = IDataTableSheetFormatter_AllowToSetAutoFilter
End Property
Public Property Let AllowToSetAutoFilter(ByVal vblnAllowToSetAutoFilter As Boolean)

    IDataTableSheetFormatter_AllowToSetAutoFilter = vblnAllowToSetAutoFilter
End Property


'''
''' Dictionary(Of String(column title), Double(column-width))
'''
Public Property Get FieldTitleToColumnWidthDic() As Scripting.Dictionary

    Set FieldTitleToColumnWidthDic = IDataTableSheetFormatter_FieldTitleToColumnWidthDic
End Property
Public Property Set FieldTitleToColumnWidthDic(ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary)

    Set IDataTableSheetFormatter_FieldTitleToColumnWidthDic = vobjFieldTitleToColumnWidthDic
End Property


'''
''' Dictionary(Of String(column title order, such as 'Col1', 'Col07'), Double(column-width))
'''
Public Property Get FieldTitleOrderToColumnWidthDic() As Scripting.Dictionary

    Set FieldTitleOrderToColumnWidthDic = IDataTableSheetFormatter_FieldTitleOrderToColumnWidthDic
End Property
Public Property Set FieldTitleOrderToColumnWidthDic(ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary)

    Set IDataTableSheetFormatter_FieldTitleOrderToColumnWidthDic = vobjFieldTitleOrderToColumnWidthDic
End Property


'''
''' When it is true, then ConvertDataForColumns procedure is used.
'''
Public Property Get AllowToConvertDataForSpecifiedColumns() As Boolean

    AllowToConvertDataForSpecifiedColumns = IDataTableSheetFormatter_AllowToConvertDataForSpecifiedColumns
End Property
Public Property Let AllowToConvertDataForSpecifiedColumns(ByVal vblnAllowToConvertDataForSpecifiedColumns As Boolean)

    IDataTableSheetFormatter_AllowToConvertDataForSpecifiedColumns = vblnAllowToConvertDataForSpecifiedColumns
End Property
'''
''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
'''
Public Property Get FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary

    Set FieldTitlesToColumnDataConvertTypeDic = IDataTableSheetFormatter_FieldTitlesToColumnDataConvertTypeDic
End Property
Public Property Set FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)

    Set IDataTableSheetFormatter_FieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
End Property


'''
''' ColumnsNumberFormatLocalParam
'''
Public Property Get ColumnsNumberFormatLocal() As ColumnsNumberFormatLocalParam

    Set ColumnsNumberFormatLocal = IDataTableSheetFormatter_ColumnsNumberFormatLocal
End Property

Public Property Set ColumnsNumberFormatLocal(ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)

    Set IDataTableSheetFormatter_ColumnsNumberFormatLocal = vobjColumnsNumberFormatLocalParam
End Property

'''
''' set format-condition for each column
'''
Public Property Get ColumnsFormatCondition() As ColumnsFormatConditionParam

    Set ColumnsFormatCondition = IDataTableSheetFormatter_ColumnsFormatCondition
End Property
Public Property Set ColumnsFormatCondition(ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)

    Set IDataTableSheetFormatter_ColumnsFormatCondition = vobjColumnsFormatConditionParam
End Property


'''
''' merge cells when the same values continue
'''
Public Property Get AllowToMergeCellsByContinuousSameValues() As Boolean

    AllowToMergeCellsByContinuousSameValues = IDataTableSheetFormatter_AllowToMergeCellsByContinuousSameValues
End Property
Public Property Let AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)

    IDataTableSheetFormatter_AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
End Property


Public Property Get AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean

    AllowToSetVerticalTopAlignmentTopOfMergeCells = IDataTableSheetFormatter_AllowToSetVerticalTopAlignmentTopOfMergeCells
End Property
Public Property Let AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)

    IDataTableSheetFormatter_AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
End Property


Public Property Get FieldTitlesToMergeCells() As Collection

    Set FieldTitlesToMergeCells = IDataTableSheetFormatter_FieldTitlesToMergeCells
End Property
Public Property Set FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)

    Set IDataTableSheetFormatter_FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
End Property



'''
''' Enabling WrapText for each cells
'''
Public Property Get AllowToSetEnablingWrapText() As Boolean

    AllowToSetEnablingWrapText = IDataTableSheetFormatter_AllowToSetEnablingWrapText
End Property
Public Property Let AllowToSetEnablingWrapText(ByVal vblnAllowToSetEnablingWrapText As Boolean)

    IDataTableSheetFormatter_AllowToSetEnablingWrapText = vblnAllowToSetEnablingWrapText
End Property

Public Property Get FieldTitlesToSetEnablingWrapTextCells() As Collection

    Set FieldTitlesToSetEnablingWrapTextCells = IDataTableSheetFormatter_FieldTitlesToSetEnablingWrapTextCells
End Property
Public Property Set FieldTitlesToSetEnablingWrapTextCells(ByVal vobjFieldTitlesToSetEnablingWrapTextCells As Collection)

    Set IDataTableSheetFormatter_FieldTitlesToSetEnablingWrapTextCells = vobjFieldTitlesToSetEnablingWrapTextCells
End Property

'''
''' Dictionary(Of String[Field-title], Excel.Constants)
'''
Public Property Get FieldTitleToHorizontalAlignmentDic() As Scripting.Dictionary

    Set FieldTitleToHorizontalAlignmentDic = IDataTableSheetFormatter_FieldTitleToHorizontalAlignmentDic
End Property
Public Property Set FieldTitleToHorizontalAlignmentDic(ByVal vobjDic As Scripting.Dictionary)

    Set IDataTableSheetFormatter_FieldTitleToHorizontalAlignmentDic = vobjDic
End Property

'''
''' If mblnAllowToAutoFitRowsHeight is true, Excel.Worksheet.Rows.AutoFit method is to be executed
'''
Public Property Get AllowToAutoFitRowsHeight() As Boolean

    AllowToAutoFitRowsHeight = IDataTableSheetFormatter_AllowToAutoFitRowsHeight
End Property
Public Property Let AllowToAutoFitRowsHeight(ByVal vblnAllowToAutoFitRowsHeight As Boolean)

    IDataTableSheetFormatter_AllowToAutoFitRowsHeight = vblnAllowToAutoFitRowsHeight
End Property

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub SetSheetFormatBeforeTableOut()

    mintFieldTitlesRowIndex = mitfCommonDataTableSheetFormatter.TopLeftRowIndex

    If Not Me.HeaderInsertTexts Is Nothing And mblnAllowToWriteInsertTextInSheetHeader Then
    
        mintFieldTitlesRowIndex = mintFieldTitlesRowIndex + Me.HeaderInsertTexts.Count
    End If

    If Not Me.AllowToShowFieldTitle Then
    
        mintFieldTitlesRowIndex = mintFieldTitlesRowIndex - 1
    End If
End Sub


'''
'''
'''
Public Sub SetSheetFormatAfterTableOut(ByVal vobjSheet As Excel.Worksheet)

    InsertInputtedHeaderTexts vobjSheet, True

    SetDataTableSheetFormatAfterTableOut vobjSheet
End Sub

'''
'''
'''
Public Sub SetDataTableSheetFormatAfterTableOut(ByVal vobjSheet As Excel.Worksheet)

    With Me
        If .AllowToShowFieldTitle Then
        
            AdjustColumnWidthAndDecorateFieldTitles vobjSheet, .FieldTitlesRowIndex, .TopLeftColumnIndex, .FieldTitleInteriorType, .FieldTitleToColumnWidthDic, .FieldTitleOrderToColumnWidthDic
        Else
            If Not .FieldTitleOrderToColumnWidthDic Is Nothing Then
        
                AdjustColumnWidthByTitleOrder vobjSheet, .FieldTitleOrderToColumnWidthDic, .TopLeftRowIndex, .TopLeftColumnIndex
            End If
        End If
        
        msubSetRecordPartSheetFormatAfterTableOut vobjSheet
        
    End With
End Sub


'''
'''
'''
Private Sub msubSetRecordPartSheetFormatAfterTableOut(ByVal vobjSheet As Excel.Worksheet)

    If Me.AllowToShowFieldTitle Then
    
        With mitfCommonDataTableSheetFormatter
    
            If .AllowToConvertDataForEachFieldTitles And Not .FieldTitlesToColumnDataConvertTypeDic Is Nothing Then
                
                If .FieldTitlesToColumnDataConvertTypeDic.Count > 0 Then
                    
                    ConvertDataForColumns vobjSheet, .FieldTitlesToColumnDataConvertTypeDic, Me.FieldTitlesRowIndex, Me.TopLeftColumnIndex
                End If
            End If
            
            
            If Not Me.ColumnsNumberFormatLocal Is Nothing Then
                
                With Me.ColumnsNumberFormatLocal
                
                    .FieldTitlesRowIndex = Me.FieldTitlesRowIndex
                
                    .TopLeftColumnIndex = Me.TopLeftColumnIndex
                End With
                
                SetColumnNumberFormatLocal vobjSheet, Me.ColumnsNumberFormatLocal
            End If
            
            If Not Me.ColumnsFormatCondition Is Nothing Then
            
                With Me.ColumnsFormatCondition
                
                    .FieldTitlesRowIndex = Me.FieldTitlesRowIndex
                
                    .TopLeftColumnIndex = Me.TopLeftColumnIndex
                End With
                
                SetColumnCellFormatCondition vobjSheet, Me.ColumnsFormatCondition
            End If
        End With
    End If
    
    
    With mitfCommonDataTableSheetFormatter
    
        If Me.AllowToShowFieldTitle Then
        
            DecorateRangeOfRecordsAboutBorderFontHeight vobjSheet, Me.FieldTitlesRowIndex, Me.TopLeftColumnIndex, .RecordBordersType, vsngFontSize:=.RecordsFontSize
        Else
            DecorateRangeOfRecordsAboutBorderFontHeightIncludingNoFieldTitles vobjSheet, Me.TopLeftRowIndex, Me.TopLeftColumnIndex, .RecordBordersType, vsngFontSize:=.RecordsFontSize
        End If
    End With

    

    If Me.AllowToShowFieldTitle Then
        
        With mitfCommonDataTableSheetFormatter
        
            If .AllowToMergeCellsByContinuousSameValues And Not .FieldTitlesToMergeCells Is Nothing Then
            
                If .FieldTitlesToMergeCells.Count > 0 Then
                
                    SetMergedCellsToTable vobjSheet, .FieldTitlesToMergeCells, Me.FieldTitlesRowIndex, Me.TopLeftColumnIndex, .AllowToSetVerticalTopAlignmentTopOfMergeCells
                End If
            End If
            
            ' Set Range.WrapText to True
            If .AllowToSetEnablingWrapText And Not .FieldTitlesToSetEnablingWrapTextCells Is Nothing Then
            
                If .FieldTitlesToSetEnablingWrapTextCells.Count > 0 Then
                
                    SetEnablingWrapTextCellsToTable vobjSheet, .FieldTitlesToSetEnablingWrapTextCells, Me.FieldTitlesRowIndex, Me.TopLeftColumnIndex
                End If
            End If
        
        End With
    End If
    
    With mitfCommonDataTableSheetFormatter
    
        ' Set Range.HorizontalAlignment
        If Not .FieldTitleToHorizontalAlignmentDic Is Nothing Then
        
            SetHorizontalAlignmentCellsToTable vobjSheet, .FieldTitleToHorizontalAlignmentDic, Me.FieldTitlesRowIndex, Me.TopLeftColumnIndex
        End If
    
    
        If .AllowToAutoFitRowsHeight Then
            
            With vobjSheet.Range(ConvertXlColumnIndexToLetter(Me.TopLeftColumnIndex) & CStr(Me.FieldTitlesRowIndex)).CurrentRegion
            
                .Rows.AutoFit
            End With
            
            'vobjSheet.Rows.AutoFit
        End If
        
        If Me.AllowToShowFieldTitle Then
        
            SetFreezePanesOn vobjSheet, Me.FieldTitlesRowIndex
        End If
    End With
End Sub


'''
'''
'''
Public Sub InsertInputtedHeaderTexts(ByVal vobjSheet As Excel.Worksheet, Optional ByVal vblnAllowToMakeVacantFirstRow As Boolean = False)

    With Me
    
        InsertHeaderTextsIntoCellsOnSheet vobjSheet, .HeaderInsertTexts, .TopLeftRowIndex, .TopLeftColumnIndex, .AllowToWriteInsertTextInSheetHeader, vblnAllowToMakeVacantFirstRow
    End With
End Sub



