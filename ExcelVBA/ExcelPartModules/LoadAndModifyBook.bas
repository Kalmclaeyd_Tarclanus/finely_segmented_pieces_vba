Attribute VB_Name = "LoadAndModifyBook"
'
'   load and modify Excel.Workbook object with various options
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel, this also uses ThisWorkbook object name
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function GetInputState Lib "user32" () As Boolean
#Else
    Private Declare Function GetInputState Lib "user32" () As Boolean
#End If

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const IS_THIS_EXCEL_AIP_LABEL_CONTROLED = False    ' AIP is the Azure Information Protection

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Public Const mstrDummySheetName As String = "dummy"

Private mstrAIPLabeledBookPath As String    ' cache


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' When the workbook has already been opened, return the reference of the opened book,
''' When not, open the workbook and return the reference
'''
Public Function FindOrLoadWorkbook(ByVal vstrPath As String) As Excel.Workbook
    
    Set FindOrLoadWorkbook = GetWorkbook(vstrPath, False)
End Function

'''
''' find the specified name Worksheet from the opend workbooks
'''
Public Function FindWorksheetFromThisWorkbook(ByVal vstrSheetName As String) As Excel.Worksheet
    
    Set FindWorksheetFromThisWorkbook = Nothing

    Dim objBook As Excel.Workbook, objWorksheet As Excel.Worksheet, objApplication As Excel.Application
    
    
    Set objApplication = ThisWorkbook.Application

    On Error Resume Next
    
    For Each objBook In objApplication.Workbooks
        
        Set objWorksheet = Nothing
        
        Set objWorksheet = objBook.Worksheets(vstrSheetName)
        
        If Not objWorksheet Is Nothing Then
            
            Exit For
        End If
    Next
    
    On Error GoTo 0

    Set FindWorksheetFromThisWorkbook = objWorksheet
End Function

'''
''' confirming existence of the specified sheet
'''
Public Function DoesTheWorksheetExist(ByVal vobjBook As Excel.Workbook, ByVal vstrSheetName As String) As Boolean
    
    Dim blnSheetExists As Boolean, objWorksheet As Excel.Worksheet
    
    blnSheetExists = False
    
    On Error Resume Next
    
    Set objWorksheet = Nothing
    
    Set objWorksheet = vobjBook.Sheets(vstrSheetName)
    
    If Not objWorksheet Is Nothing Then
    
        blnSheetExists = True
    End If
    
    On Error GoTo 0

    DoesTheWorksheetExist = blnSheetExists
End Function

'''
'''
'''
Public Function FindSheetNamesByAKeyword(ByRef robjBook As Excel.Workbook, _
        ByVal vstrSheetNameKeyword As String) As Collection


    Dim objNames As Collection, objSheet As Excel.Worksheet
    
    Set objNames = New Collection
    
    For Each objSheet In robjBook.Worksheets
    
        If InStr(1, objSheet.Name, vstrSheetNameKeyword) > 0 Then
        
            objNames.Add objSheet.Name
        End If
    Next
    
    Set FindSheetNamesByAKeyword = objNames
End Function


'''
''' testing function...
'''
Public Function GetWorkbookByAnotherProcess(ByVal vstrPath As String, _
        Optional vblnForceToCreate As Boolean = True, _
        Optional vblnReadOnly As Boolean = False, _
        Optional vblnOpenWithPreventingEvents As Boolean = False, _
        Optional vblnIgnoreReadOnlyRecommended As Boolean = False) As Excel.Workbook


    Set GetWorkbookByAnotherProcess = GetWorkbook(vstrPath, vblnForceToCreate, vblnReadOnly, vblnOpenWithPreventingEvents, vblnIgnoreReadOnlyRecommended, True)
End Function

'''
'''
'''
Public Function IsOpenedInThisWorkbookProcess(ByVal vstrBookPath As String) As Boolean
    
    Dim objBook As Excel.Workbook, blnExistedFileFound As Boolean, strBookName As String
    Dim objApplication As Excel.Application

    With New Scripting.FileSystemObject
    
        strBookName = .GetFileName(vstrBookPath)
    End With
    
    Set objApplication = ThisWorkbook.Application

    blnExistedFileFound = False
    
    For Each objBook In objApplication.Workbooks
    
        If StrComp(UCase(objBook.Name), UCase(strBookName)) = 0 Then
        
            blnExistedFileFound = True
            
            Exit For
        End If
    Next

    IsOpenedInThisWorkbookProcess = blnExistedFileFound
End Function


'''
''' Open the workbook when it has not already been opened.
'''
''' <Argument>vstrPath: Input</Argument>
''' <Argument>vblnForceToCreate: Input</Argument>
''' <Argument>vblnReadOnly: Input</Argument>
''' <Argument>vblnOpenWithPreventingEvents: Input</Argument>
''' <Argument>vstrPassword: Input</Argument>
''' <Argument>vstrWriteResPassword: Input</Argument>
''' <Argument>vblnIgnoreReadOnlyRecommended: Input</Argument>
''' <Argument>vblnStartByAnotherProcess: Input</Argument>
''' <Argument>vblnAddToMru: Input</Argument>
''' <Argument>vblnAllowToRequestToInputPasswordToOpenBook: Input</Argument>
''' <Argument>vblnAllowToKeepBookPasswordTemporarily: Input</Argument>
''' <Argument>vblnExcelApplicationVisible: Input</Argument>
Public Function GetWorkbook(ByVal vstrPath As String, _
        Optional ByVal vblnForceToCreate As Boolean = True, _
        Optional ByVal vblnReadOnly As Boolean = False, _
        Optional ByVal vblnOpenWithPreventingEvents As Boolean = False, _
        Optional ByVal vstrPassword As String, _
        Optional ByVal vstrWriteResPassword As String, _
        Optional ByVal vblnIgnoreReadOnlyRecommended As Boolean = False, _
        Optional ByVal vblnStartByAnotherProcess As Boolean = False, _
        Optional ByVal vblnAddToMru As Boolean = False, _
        Optional ByVal vblnAllowToRequestToInputPasswordToOpenBook As Boolean = False, _
        Optional ByVal vblnAllowToKeepBookPasswordTemporarily As Boolean = True, _
        Optional ByVal vblnExcelApplicationVisible As Boolean = True) As Excel.Workbook
    
    
    Dim objFS As Scripting.FileSystemObject
    Dim strBookName As String, blnExistedFileFound As Boolean, blnOldDisplayAlerts As Boolean, blnOldEnableEvents As Boolean
    Dim strPassword As String, strInputted As String
    Dim blnIsPasswordGotFromCache As Boolean
    
    Dim objBook As Excel.Workbook, objFoundBook As Excel.Workbook, objApplication As Excel.Application
    
    
    blnIsPasswordGotFromCache = False
    
    Set objApplication = GetOfficeApplicationObject("Excel", vblnStartByAnotherProcess) ' Avoid using the keyword ThisWorkbook, Set objApplication = ThisWorkbook.Application
    
    
    If Trim(Replace(LCase(Application.Name), "microsoft", "")) = "excel" Then
    
        ' Only when use other Excel.Application process, vblnExcelApplicationVisible is used
    
        ' The reason why use the VBA.CallByName() is that this statement can avoid a VBA syntax error in both Word or PowerPoint
    
        If GetThisApplicationHWnd() <> objApplication.hwnd Then
    
            If objApplication.Visible <> vblnExcelApplicationVisible Then
            
                objApplication.Visible = vblnExcelApplicationVisible
            End If
        End If
    Else
        ' If this VBA execution process is from either Word or PowerPoint, then the following is used
    
        objApplication.Visible = vblnExcelApplicationVisible
    End If
    
    
    blnOldDisplayAlerts = objApplication.DisplayAlerts
    
    On Error GoTo ErrHandler:
    
    objApplication.DisplayAlerts = False
    
    If vblnOpenWithPreventingEvents Then
    
        blnOldEnableEvents = objApplication.EnableEvents
        
        objApplication.EnableEvents = False
    End If
    
    Set objFS = New Scripting.FileSystemObject
    
    strBookName = objFS.GetFileName(vstrPath)
    
    Set objFoundBook = Nothing
    
    blnExistedFileFound = False
    
    ' find a Workbook from already loaded books
    For Each objBook In objApplication.Workbooks
        
        Select Case True
        
            Case StrComp(UCase(objBook.Name), UCase(strBookName)) = 0, _
                    StrComp(UCase(objFS.GetBaseName(objBook.Name)), UCase(strBookName)) = 0
        
                blnExistedFileFound = True
            
                Set objFoundBook = objBook
                
                Exit For
        End Select
    Next


    If Not blnExistedFileFound Then
        
        ' check file-system drive existence
        
        If IsDriveOfPathStringHasSomeError(vstrPath, objFS) Then
        
            Exit Function
        End If
        
        strPassword = ""
        
        If Not IsEmpty(vstrPassword) And vstrPassword <> "" Then
        
            strPassword = vstrPassword
            
        ElseIf vblnAllowToRequestToInputPasswordToOpenBook Then
        
            ' Solute password from cache if possible
        
            If vblnAllowToKeepBookPasswordTemporarily Then
            
                strPassword = GetStringFromCallBackInterfaceAndOneArgument("GetLockingBookPasswordByPath", vstrPath)
                
                If strPassword <> "" Then
            
                    blnIsPasswordGotFromCache = True
                End If
            End If
        
            If Not blnIsPasswordGotFromCache Then
            
                strPassword = InputPasswordSimpleBoxToOpenPasswordLockedExcelBook(strBookName)
            End If
            
        ElseIf vstrPassword = "" And vblnAllowToKeepBookPasswordTemporarily Then
        
            ' try to get a password from cache
        
            'strPassword = GetLockingBookPasswordByPath(vstrPath)
            
            strPassword = GetStringFromCallBackInterfaceAndOneArgument("GetLockingBookPasswordByPath", vstrPath)
            
            If strPassword <> "" Then
            
                blnIsPasswordGotFromCache = True
            End If
        End If
        
        
        If objFS.FileExists(vstrPath) Then
            
            If Not vblnIgnoreReadOnlyRecommended Then
                
                Set objFoundBook = objApplication.Workbooks.Open(vstrPath, _
                        ReadOnly:=vblnReadOnly, _
                        Password:=strPassword, _
                        WriteResPassword:=vstrWriteResPassword, _
                        AddToMru:=vblnAddToMru)
            Else
                Set objFoundBook = objApplication.Workbooks.Open(vstrPath, _
                        ReadOnly:=vblnReadOnly, _
                        Password:=vstrPassword, _
                        WriteResPassword:=vstrWriteResPassword, _
                        IgnoreReadOnlyRecommended:=vblnIgnoreReadOnlyRecommended, _
                        AddToMru:=vblnAddToMru)
            End If
            
            If Not blnIsPasswordGotFromCache And Not objFoundBook Is Nothing And vblnAllowToKeepBookPasswordTemporarily And strPassword <> "" Then
            
                'StorePasswordOfBookTemporarily vstrPath, strPassword
                
                CallBackInterfaceByTwoArguments "StorePasswordOfBookTemporarily", vstrPath, strPassword
            End If
        Else
            If vblnForceToCreate Then
            
                If Not objFS.FolderExists(objFS.GetParentFolderName(vstrPath)) Then
                
                    On Error Resume Next
                    
                    ForceToCreateDirectory objFS.GetParentFolderName(vstrPath), objFS
                    
                    On Error GoTo ErrHandler:
                End If
                
#If IS_THIS_EXCEL_AIP_LABEL_CONTROLED Then  ' AIP is the Azure Information Protection
                
                mstrAIPLabeledBookPath = GetPreparedAIPLabeledExcelBookPath()
                
                Set objFoundBook = objApplication.Workbooks.Add(mstrAIPLabeledBookPath)
#Else
                Set objFoundBook = objApplication.Workbooks.Add
#End If
                If objFS.FolderExists(objFS.GetParentFolderName(vstrPath)) Then
                
                    If vstrPassword = "" Then
                
                        objFoundBook.SaveAs vstrPath
                    Else
                        objFoundBook.SaveAs vstrPath, Password:=vstrPassword
                        
                        If vblnAllowToKeepBookPasswordTemporarily Then
                        
                            CallBackInterfaceByTwoArguments "StorePasswordOfBookTemporarily", vstrPath, strPassword
                        End If
                    End If
                End If
            End If
        End If
    End If

ErrHandler:
    objApplication.DisplayAlerts = blnOldDisplayAlerts
    
    If vblnOpenWithPreventingEvents Then
        
        objApplication.EnableEvents = blnOldEnableEvents
    End If
    
    Set GetWorkbook = objFoundBook
End Function


'''
''' Such as USB removable disk and SD card
'''
Public Function GetWorkbookInRemovableDisk(ByVal vstrVolumeName As String, _
        ByVal vstrSubRightPath As String, _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vstrWriteResPassword As String = "", _
        Optional ByVal vblnAllowToRequestToInputPasswordToOpenBook As Boolean = False) As Excel.Workbook


    Set GetWorkbookInRemovableDisk = GetWorkbookWithDriveType(Removable, vstrVolumeName, vstrSubRightPath, vstrPassword, vstrWriteResPassword, vblnAllowToRequestToInputPasswordToOpenBook)
End Function

'''
''' Such as SSD and HDD
'''
Public Function GetWorkbookInLocalHardDrive(ByVal vstrVolumeName As String, _
        ByVal vstrSubRightPath As String, _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vstrWriteResPassword As String = "", _
        Optional ByVal vblnAllowToRequestToInputPasswordToOpenBook As Boolean = False) As Excel.Workbook


    Set GetWorkbookInLocalHardDrive = GetWorkbookWithDriveType(Fixed, vstrVolumeName, vstrSubRightPath, vstrPassword, vstrWriteResPassword, vblnAllowToRequestToInputPasswordToOpenBook)
End Function


'''
'''
'''
Public Function GetWorkbookPathFromSpecifiedDrive(ByVal venmDriveType As Scripting.DriveTypeConst, ByVal vstrVolumeName As String, ByVal vstrSubRightPath As String) As String

    Dim strBookPath As String, strDrivePath As String
    
    strBookPath = ""
    
    strDrivePath = FindWindowsDrivePath(venmDriveType, vstrVolumeName)

    If strDrivePath <> "" Then
    
        strBookPath = strDrivePath & vstrSubRightPath
    End If

    GetWorkbookPathFromSpecifiedDrive = strBookPath
End Function

'''
'''
'''
Private Function GetWorkbookWithDriveType(ByVal venmDriveType As Scripting.DriveTypeConst, _
        ByVal vstrVolumeName As String, _
        ByVal vstrSubRightPath As String, _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vstrWriteResPassword As String = "", _
        Optional ByVal vblnAllowToRequestToInputPasswordToOpenBook As Boolean = False) As Excel.Workbook

    Dim strBookPath As String, strDrivePath As String, objBook As Excel.Workbook
    
    strDrivePath = FindWindowsDrivePath(venmDriveType, vstrVolumeName)

    If strDrivePath <> "" Then
    
        strBookPath = strDrivePath & vstrSubRightPath
    
        With New Scripting.FileSystemObject
        
            If .FileExists(strBookPath) Then
            
                Set objBook = GetWorkbook(strBookPath, _
                        False, _
                        False, _
                        False, _
                        vstrPassword, _
                        vstrWriteResPassword, _
                        False, _
                        False, _
                        False, _
                        vblnAllowToRequestToInputPasswordToOpenBook)
            End If
        End With
    End If

    Set GetWorkbookWithDriveType = objBook
End Function
'''
'''
'''
Public Function GetWorkbookIfItExists(ByVal vstrPath As String, _
        Optional vblnReadOnly As Boolean = False, _
        Optional vblnOpenWithPreventingEvents As Boolean = False, _
        Optional vblnIgnoreReadOnlyRecommended As Boolean = False, _
        Optional vblnStartByAnotherProcess As Boolean = False) As Excel.Workbook


    Set GetWorkbookIfItExists = GetWorkbook(vstrPath, _
            False, _
            vblnReadOnly, _
            vblnOpenWithPreventingEvents, _
            vblnIgnoreReadOnlyRecommended, _
            vblnStartByAnotherProcess)
End Function



'''
''' get Workbook instance after the existed file is deleted
'''
Public Function GetWorkbookForLoggingAfterDeleteExistedFile(ByVal vstrPath As String) As Excel.Workbook

    With New Scripting.FileSystemObject
    
        If .FileExists(vstrPath) Then
            
            On Error Resume Next
        
            .DeleteFile vstrPath, True
            
            On Error GoTo 0
        End If
    End With

    Set GetWorkbookForLoggingAfterDeleteExistedFile = GetWorkbookAndPrepareForUnitTest(vstrPath)
End Function


'''
''' get Workbook instance and preparation for unit-test
'''
Public Function GetWorkbookAndPrepareForUnitTest(ByVal vstrPath As String) As Excel.Workbook
    
    Dim objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    On Error Resume Next
    
    Set objBook = Nothing
    
    Set objBook = GetWorkbook(vstrPath, False)
    
    On Error GoTo 0
    
    If objBook Is Nothing Then
    
        Set objBook = GetWorkbook(vstrPath, True)
        
        objBook.Worksheets(1).Name = mstrDummySheetName
        
        DeleteDefaultSheet objBook
    Else
        DeleteAllSheetsWithoutDummySheetButWhenTheDummySheetDoesntExistThenItIsAdded objBook
    End If
    
    Set GetWorkbookAndPrepareForUnitTest = objBook
End Function

'''
''' get an Worksheet instance and preparation for unit-test
'''
Public Function GetWorkbookAndAnWorksheetPrepareForUnitTest(ByVal vstrPath As String, _
        ByVal vstrSheetName As String) As Excel.Worksheet


    Dim objBook As Excel.Workbook, objSheet As Excel.Worksheet


    Set objBook = GetWorkbookAndPrepareForUnitTest(vstrPath)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet.Name = vstrSheetName

    DeleteDummySheetWhenItExists objBook

    Set GetWorkbookAndAnWorksheetPrepareForUnitTest = objSheet
End Function

'''
'''
'''
Public Sub DeleteAllSheetsWithoutDummySheetButWhenTheDummySheetDoesntExistThenItIsAdded(ByVal vobjBook As Excel.Workbook)
    
    AddDummySheetWhenItIsNotIncluded vobjBook
    
    DeleteSheetsExceptSpecifiedSheet vobjBook, mstrDummySheetName
End Sub



'''
''' close Workbook if it is opened now.
'''
Public Sub CloseBookIfItIsOpened(ByVal vstrPath As String)
    
    Dim strBookName As String, blnExistedFileFound As Boolean, blnOldDisplayAlerts As Boolean
    
    Dim objBook As Excel.Workbook, objFoundBook As Excel.Workbook, objApplication As Excel.Application
    
    
    Set objApplication = ThisWorkbook.Application
    
    blnOldDisplayAlerts = objApplication.DisplayAlerts
    
    On Error GoTo ErrHandler:
    
    objApplication.DisplayAlerts = False
    
    With New Scripting.FileSystemObject
    
        strBookName = .GetFileName(vstrPath)
    End With
    
    Set objFoundBook = Nothing
    
    blnExistedFileFound = False
    
    For Each objBook In objApplication.Workbooks
        
        If StrComp(UCase(objBook.Name), UCase(strBookName)) = 0 Then
        
            blnExistedFileFound = True
            
            Set objFoundBook = objBook
            
            Exit For
        End If
    Next
    
    If Not objFoundBook Is Nothing Then
    
        objFoundBook.Saved = True
        
        objFoundBook.Close
    End If
    
ErrHandler:

    objApplication.DisplayAlerts = blnOldDisplayAlerts
End Sub

'''
''' close Workbook without Excel DisplayAlerts
'''
Public Sub CloseBookWithoutDisplayAlerts(ByRef robjBook As Excel.Workbook)

    If robjBook Is Nothing Then Exit Sub

    Dim objApplication As Excel.Application, blnOldDisplayAlerts As Boolean
    
    Set objApplication = robjBook.Application

    blnOldDisplayAlerts = objApplication.DisplayAlerts

    objApplication.DisplayAlerts = False

    If Not robjBook Is Nothing Then
    
        robjBook.Saved = True
        
        robjBook.Close
    End If
    
    Set robjBook = Nothing

ErrHandler:
    objApplication.DisplayAlerts = blnOldDisplayAlerts

End Sub


'''
''' need for the ADO multi-processes parallel run
'''
Public Sub SaveAndCloseBookAndGetBookPathAndSheetName(ByRef rstrBookPath As String, _
        ByRef rstrSheetName As String, _
        ByVal vobjSheet As Excel.Worksheet)
    
    
    Dim objBook As Excel.Workbook
    
    rstrSheetName = vobjSheet.Name
    
    Set objBook = vobjSheet.Parent
    
    ForceToSaveBookWithoutDisplayAlert objBook
    
    rstrBookPath = objBook.FullName
    
    objBook.Close
End Sub

'''
''' need for the ADO multi-processes parallel run
'''
Public Function GetWorksheetFromBookPathAndSheetName(ByVal vstrExistedBookPath As String, _
        ByVal vstrSheetName As String) As Excel.Worksheet
    
    Dim objBook As Excel.Workbook, objWorksheet As Excel.Worksheet
    
    Set objWorksheet = Nothing
    
    Set objBook = GetWorkbook(vstrExistedBookPath, False)
    
    If Not objBook Is Nothing Then
        
        Set objWorksheet = objBook.Worksheets(vstrSheetName)
    End If

    Set GetWorksheetFromBookPathAndSheetName = objWorksheet
End Function


'''
''' get opend workbook.
'''
Public Function GetOpenedWorkbook(ByVal vstrPath As String) As Excel.Workbook
    
    Dim strBookName As String, blnExistedFileFound As Boolean, objFS As Scripting.FileSystemObject
    
    Dim objBook As Excel.Workbook, objFoundBook As Excel.Workbook, objApplication As Excel.Application
    
    
    Set objApplication = ThisWorkbook.Application
    
    Set objFS = New Scripting.FileSystemObject
    
    With objFS
    
        strBookName = .GetFileName(vstrPath)
    End With
    
    blnExistedFileFound = False
    
    Set objFoundBook = Nothing
    
    For Each objBook In objApplication.Workbooks
        
        Select Case True
        
            Case StrComp(UCase(objBook.Name), UCase(strBookName)) = 0, StrComp(UCase(objFS.GetBaseName(objBook.Name)), UCase(strBookName)) = 0
        
                blnExistedFileFound = True
            
                Set objFoundBook = objBook
                
                Exit For
        End Select
    Next

    Set GetOpenedWorkbook = objFoundBook
End Function



'''
'''
'''
Public Sub ForceToSaveBookWithoutDisplayAlert(ByVal vobjBook As Excel.Workbook)
    
    Dim blnDisplayAlerts As Boolean, objApplication As Excel.Application
    
    Set objApplication = vobjBook.Application
    
    'Debug.Print "FullName, ForceToSaveBookWithoutDisplayAlert: " & vobjBook.FullName
    
    blnDisplayAlerts = objApplication.DisplayAlerts
    
    On Error GoTo ErrHandler:
    
    objApplication.DisplayAlerts = False
    
    vobjBook.Save
    
ErrHandler:
    objApplication.DisplayAlerts = blnDisplayAlerts
End Sub

'''
''' SaveAs execution without display-alart
'''
Public Sub ForceToSaveAsBookWithoutDisplayAlert(ByVal vobjBook As Excel.Workbook, _
        ByVal vstrPath As String, _
        Optional ByVal venmFileFormat As Excel.XlFileFormat = 0, _
        Optional ByVal vblnForceToCreateDirectoryWhenThereIsNotIt As Boolean = False, _
        Optional ByVal vstrPassword As String = "")
    
    Dim blnDisplayAlerts As Boolean
    Dim objApplication As Excel.Application
    
    
    If vblnForceToCreateDirectoryWhenThereIsNotIt Then
        
        With New Scripting.FileSystemObject
        
            CheckDriveExistence vstrPath
        
            If Not .FolderExists(.GetParentFolderName(vstrPath)) Then
            
                ForceToCreateDirectory .GetParentFolderName(vstrPath)
            End If
        End With
    End If
    
    Set objApplication = vobjBook.Application
    
    blnDisplayAlerts = objApplication.DisplayAlerts
    
    On Error GoTo ErrHandler:
    
    objApplication.DisplayAlerts = False
    
    If venmFileFormat > 0 Then
    
        If vstrPassword <> "" Then
        
            vobjBook.SaveAs vstrPath, venmFileFormat, vstrPassword
        Else
            vobjBook.SaveAs vstrPath, venmFileFormat
        End If
    Else
        If vstrPassword <> "" Then
        
            vobjBook.SaveAs vstrPath, Password:=vstrPassword
        Else
            vobjBook.SaveAs vstrPath
        End If
    End If
    
ErrHandler:
    objApplication.DisplayAlerts = blnDisplayAlerts
End Sub

'''
''' SaveAs a macro-book execution without display-alart
'''
Public Sub ForceToSaveAsMacroBookWithoutDisplayAlert(ByVal vobjBook As Excel.Workbook, _
        ByVal vstrPath As String, _
        Optional ByVal vblnForceToCreateDirectoryWhenThereIsNotIt As Boolean = False)
    
    
    With New Scripting.FileSystemObject
        
        If StrComp(LCase(.GetExtensionName(vstrPath)), "xlsm") = 0 Then
        
            ForceToSaveAsBookWithoutDisplayAlert vobjBook, _
                    vstrPath, _
                    XlFileFormat.xlOpenXMLWorkbookMacroEnabled, _
                    vblnForceToCreateDirectoryWhenThereIsNotIt
        End If
    End With
End Sub

'''
'''
'''
Public Function DeleteBookInfoByPath(ByVal vstrBookPath As String) As Boolean

    DeleteBookInfoByPath = False
    
    Dim objBook As Workbook, blnIsOpenedBeforeThisFunction As Boolean
    
    On Error GoTo ErrHandler:
    
    blnIsOpenedBeforeThisFunction = False
    
    If IsOpenedInThisWorkbookProcess(vstrBookPath) Then
    
        blnIsOpenedBeforeThisFunction = True
    End If
    
    Set objBook = GetWorkbook(vstrBookPath, vblnForceToCreate:=False, vblnIgnoreReadOnlyRecommended:=True)
    
    DeleteBookInfoByPath = DeleteBookInfo(objBook)
    
    ForceToSaveBookWithoutDisplayAlert objBook
    
    If Not blnIsOpenedBeforeThisFunction Then
    
        objBook.Close
    End If
    
ErrHandler:

End Function


'''
'''
'''
Public Function DeleteBookInfo(ByVal vobjBook As Excel.Workbook) As Boolean
    
    DeleteBookInfo = DeleteOfficeFilePropertiesInfo(vobjBook)
End Function



'''
''' Get new sheet from the existed workbook as the first worksheet
'''
Public Function GetNewWorksheet(ByVal vstrExcelBookPath As String) As Excel.Worksheet

    With GetWorkbook(vstrExcelBookPath).Worksheets
    
        Set GetNewWorksheet = .Add()    ' The option 'Before:=' will be selected
    End With
End Function


'''
''' Get new sheet from the workbook by spcified file-path after the existed all sheets
'''
''' Attention: If the book doesn't exist, the new book is to be created.
'''
Public Function GetNewWorksheetAfterAllExistedSheets(ByRef rstrExcelBookPath As String) As Excel.Worksheet

    Set GetNewWorksheetAfterAllExistedSheets = GetNewWorksheetAfterAllExistedSheetsFromBook(GetWorkbook(rstrExcelBookPath))
End Function

'''
''' Get new sheet from the workbook after the existed all sheets
'''
Public Function GetNewWorksheetAfterAllExistedSheetsFromBook(ByRef robjBook As Excel.Workbook) As Excel.Worksheet

    With robjBook.Worksheets
    
        Set GetNewWorksheetAfterAllExistedSheetsFromBook = .Add(After:=.Item(.Count))    ' If it is nothing that the "After" specifying, the "Before" option will be selected
    End With
End Function

'''
'''
'''
Public Function GetNewWorksheetAfterAllExistedSheetsAndFindNewSheetName(ByRef rstrExcelBookPath As String, ByRef rstrNewSheetName As String) As Excel.Worksheet

    Dim objSheet As Excel.Worksheet
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheets(rstrExcelBookPath)

    With objSheet
    
        .Name = FindNewSheetNameWhenAlreadyExist(rstrNewSheetName, .Parent)    ' SQL query
    End With

    Set GetNewWorksheetAfterAllExistedSheetsAndFindNewSheetName = objSheet
End Function

'''
'''
'''
Public Function GetNewWorksheetAfterSpecifiedSheets(ByVal vobjExistedSheet As Excel.Worksheet, _
        ByVal vstrSheetName As String, _
        Optional ByVal vblnFindNewSheetNameIfTheNameHasAlreadyExisted As Boolean = False) As Excel.Worksheet


    Dim objBook As Excel.Workbook, objNewSheet As Excel.Worksheet
    
    Set objBook = vobjExistedSheet.Parent
    
    With objBook.Worksheets
    
        Set objNewSheet = .Add(After:=vobjExistedSheet)
        
        If vblnFindNewSheetNameIfTheNameHasAlreadyExisted Then
        
            objNewSheet.Name = FindNewSheetNameWhenAlreadyExist(vstrSheetName, objBook)
        Else
            objNewSheet.Name = vstrSheetName
        End If
    End With

    Set GetNewWorksheetAfterSpecifiedSheets = objNewSheet
End Function


'''
'''
'''
Public Function GetWorksheetFromSheetNameWithIgnoreError(ByVal vobjBook As Excel.Workbook, _
        ByVal vstrWorksheetName As String) As Excel.Worksheet

    On Error Resume Next
    
    Set GetWorksheetFromSheetNameWithIgnoreError = vobjBook.Worksheets(vstrWorksheetName)
    
    On Error GoTo 0
End Function


'''
''' when the sheet-name has been already existed, find new sheet-name
'''
Public Function FindNewSheetNameWhenAlreadyExist(ByVal vstrSheetName As String, _
        ByVal vobjBook As Excel.Workbook) As String
    
    
    FindNewSheetNameWhenAlreadyExist = vstrSheetName
    
    Dim objSheet As Excel.Worksheet, intCount As Long, strNewSheetName As String
    
    
    strNewSheetName = vstrSheetName
    
    On Error Resume Next
    
    Set objSheet = Nothing: Set objSheet = vobjBook.Worksheets.Item(vstrSheetName)
    
    intCount = 1
    
    While Not objSheet Is Nothing
    
        intCount = intCount + 1
        
        strNewSheetName = vstrSheetName & "_" & PadStringLeftZero2Digits(intCount)
        
        Set objSheet = Nothing: Set objSheet = vobjBook.Worksheets.Item(strNewSheetName)
    Wend
    
    If StrComp(strNewSheetName, vstrSheetName) <> 0 Then
    
        FindNewSheetNameWhenAlreadyExist = strNewSheetName
    End If
    
    On Error GoTo 0
End Function


'''
''' when count of the used-range of the sheet has exceeded the limit, find new sheet
'''
''' <Argument>vstrSheetName: Input</Argument>
''' <Argument>vobjBook: Input</Argument>
''' <Argument>vintLimitCountOfUsedRows: Input</Argument>
''' <Argument>vblnAllowToAddNewWorksheetByFirstOrderOfWorkbook: Input</Argument>
''' <Return>Excel.Worksheet</Return>
Public Function GetSheetWithFindingNewNameWhenUsedRangeRowsCountExceedsLimit(ByVal vstrSheetName As String, _
        ByVal vobjBook As Excel.Workbook, _
        Optional ByVal vintLimitCountOfUsedRows As Long = 300, _
        Optional ByVal vblnAllowToAddNewWorksheetByFirstOrderOfWorkbook As Boolean = True) As Excel.Worksheet
    
    
    Dim objSheet As Excel.Worksheet, intCount As Long, strNewSheetName As String, objPreviousSheet As Excel.Worksheet
    
    
    strNewSheetName = vstrSheetName
    
    On Error Resume Next
    
    Set objSheet = vobjBook.Worksheets.Item(vstrSheetName)
    
    If objSheet Is Nothing Then
    
        If vblnAllowToAddNewWorksheetByFirstOrderOfWorkbook Then
        
            Set objSheet = vobjBook.Worksheets.Add()
        Else
            Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(vobjBook)
        End If
    
        objSheet.Name = vstrSheetName
    Else
        intCount = 1
        
        While objSheet.UsedRange.Rows.Count > vintLimitCountOfUsedRows
        
            intCount = intCount + 1
            
            Set objPreviousSheet = objSheet
            
            strNewSheetName = vstrSheetName & "_" & Format(intCount, "00")
            
            Set objSheet = vobjBook.Worksheets.Item(strNewSheetName)
            
            If objSheet Is Nothing Then
            
                Set objSheet = vobjBook.Worksheets.Add(After:=objPreviousSheet)
    
                objSheet.Name = strNewSheetName
            End If
        Wend
    End If
    
    On Error GoTo 0
    
    Set GetSheetWithFindingNewNameWhenUsedRangeRowsCountExceedsLimit = objSheet
End Function

'''
'''
'''
Public Sub FindBottomRowIndexAfterGetSheetWithFindingNewNameWhenUsedRangeRowsCountExceedsLimit(ByRef robjFoundSheet As Excel.Worksheet, _
        ByRef rintBottomRowIndex As Long, _
        ByVal vstrSheetName As String, _
        ByVal vobjBook As Excel.Workbook, _
        Optional ByVal vintLimitCountOfUsedRows As Long = 300, _
        Optional ByVal vblnAllowToAddNewWorksheetByFirstOrderOfWorkbook As Boolean = True)

    Dim objFoundSheet As Excel.Worksheet

    Set objFoundSheet = GetSheetWithFindingNewNameWhenUsedRangeRowsCountExceedsLimit(vstrSheetName, vobjBook, vintLimitCountOfUsedRows, vblnAllowToAddNewWorksheetByFirstOrderOfWorkbook)

    ' Outputs

    Set robjFoundSheet = objFoundSheet
    
    rintBottomRowIndex = GetBottomRowIndexFromWorksheetUsedRange(objFoundSheet)
End Sub

'''
'''
'''
Public Function GetBottomRowIndexFromWorksheetUsedRange(ByRef robjSheet As Excel.Worksheet) As Long

    Dim intBottomRowIndex As Long

    With robjSheet.UsedRange
    
        If .Row = 1 And .Rows.Count = 1 Then    ' the sheet should have a Empty value cell in a first row index and a first column index
    
            intBottomRowIndex = 0
        Else
            intBottomRowIndex = .Row + .Rows.Count - 1
        End If
    End With

    GetBottomRowIndexFromWorksheetUsedRange = intBottomRowIndex
End Function


'''
'''
'''
Public Sub AddDummySheetWhenItIsNotIncluded(ByVal vobjBook As Excel.Workbook)
    
    Dim objSheet As Excel.Worksheet

    Set objSheet = Nothing
    
    On Error Resume Next
    
    Set objSheet = vobjBook.Worksheets.Item(mstrDummySheetName)
    
    On Error GoTo 0
    
    If objSheet Is Nothing Then
    
        Set objSheet = vobjBook.Worksheets.Add()
            
        objSheet.Name = mstrDummySheetName
    End If
End Sub

'''
'''
'''
Public Sub DeleteDefaultSheetWithAddingDummySheet(ByVal vobjBook As Excel.Workbook)
    
    AddDummySheetWhenItIsNotIncluded vobjBook

    DeleteDefaultSheet vobjBook
End Sub

'''
''' delete "Sheet1", "Sheet2", and "Sheet3"
'''
Public Sub DeleteDefaultSheet(ByVal vobjBook As Excel.Workbook)
    
    Dim objSheet As Excel.Worksheet, strFigure As String
    Dim blnOldDisplayAlerts As Boolean, objApplication As Excel.Application
    
    Set objApplication = vobjBook.Application
    
    blnOldDisplayAlerts = objApplication.DisplayAlerts
    
    On Error GoTo ErrHandler:
    
    objApplication.DisplayAlerts = False
    
    For Each objSheet In vobjBook.Worksheets
    
        If InStr(UCase(objSheet.Name), "SHEET") > 0 Then
        
            strFigure = Right(UCase(objSheet.Name), Len(objSheet.Name) - Len("SHEET"))
            
            If IsNumeric(strFigure) Then
            
                objSheet.Delete
            End If
        End If
    Next
    
ErrHandler:
    objApplication.DisplayAlerts = blnOldDisplayAlerts
    
    On Error GoTo 0
End Sub


'''
'''
'''
Public Sub DeleteSheetsExceptSpecifiedSheet(ByVal vobjBook As Excel.Workbook, ByVal vstrNoDeleteSheetName As String)

    Dim objSheet As Excel.Worksheet, strFigure As String
    Dim blnOldDisplayAlerts As Boolean, objApplication As Excel.Application
    
    
    Set objApplication = vobjBook.Application
    
    blnOldDisplayAlerts = objApplication.DisplayAlerts
    
    On Error GoTo ErrHandler:
    
    objApplication.DisplayAlerts = False
    
    For Each objSheet In vobjBook.Worksheets
    
        If StrComp(objSheet.Name, vstrNoDeleteSheetName) <> 0 Then
        
            objSheet.Delete
        End If
    Next

ErrHandler:
    objApplication.DisplayAlerts = blnOldDisplayAlerts
End Sub

'''
'''
'''
Public Sub DeleteDummySheetWhenItExists(ByVal vobjBook As Excel.Workbook)
    
    Dim objDummySheet As Excel.Worksheet, blnOldDisplayAlerts As Boolean
    Dim objApplication As Excel.Application
    
    
    Set objApplication = vobjBook.Application
    
    blnOldDisplayAlerts = objApplication.DisplayAlerts
    
    On Error Resume Next
    
    Set objDummySheet = Nothing
    
    Set objDummySheet = vobjBook.Worksheets.Item(mstrDummySheetName)
    
    On Error GoTo ErrHandler
    
    objApplication.DisplayAlerts = False
    
    If Not objDummySheet Is Nothing Then
    
        If vobjBook.Worksheets.Count >= 2 Then
        
            objDummySheet.Delete
        End If
    End If
    
ErrHandler:
    objApplication.DisplayAlerts = blnOldDisplayAlerts
End Sub

'''
'''
'''
Public Sub DeleteDummySheetWhenItExistsBySheets(ByVal vobjSheets As Excel.Sheets)
    
    DeleteDummySheetWhenItExists vobjSheets.Parent
End Sub

'''
'''
'''
Public Sub DeleteSheetsWithoutDummySheet(ByVal vobjBook As Excel.Workbook)
    
    Dim objDummySheet As Excel.Worksheet, objSheet As Excel.Worksheet
    Dim blnOldDisplayAlerts As Boolean, objApplication As Excel.Application
    
    Set objApplication = vobjBook.Application
    
    blnOldDisplayAlerts = objApplication.DisplayAlerts
    
    On Error Resume Next
    
    Set objDummySheet = Nothing
    
    Set objDummySheet = vobjBook.Worksheets.Item(mstrDummySheetName)
    
    If objDummySheet Is Nothing Then
    
        Set objDummySheet = vobjBook.Worksheets.Add()
        
        objDummySheet.Name = mstrDummySheetName
    End If
    
    On Error GoTo ErrHandler
    
    objApplication.DisplayAlerts = False
    
    For Each objSheet In vobjBook.Worksheets
    
        If Not objDummySheet Is objSheet Then
        
            objSheet.Delete
        End If
        
    Next

ErrHandler:
    objApplication.DisplayAlerts = blnOldDisplayAlerts
End Sub

'''
'''
'''
Public Sub DeleteSheetIfExists(ByVal vobjBook As Excel.Workbook, ByVal vstrDeletingSheetName As String)

    Dim objSheet As Excel.Worksheet
    Dim blnOldDisplayAlerts As Boolean
    Dim objXl As Excel.Application
    
    Set objXl = vobjBook.Application
    
    blnOldDisplayAlerts = objXl.DisplayAlerts

    objXl.DisplayAlerts = False
    
    On Error Resume Next
    
    Set objSheet = Nothing
    
    Set objSheet = vobjBook.Worksheets.Item(vstrDeletingSheetName)
    
    If Not objSheet Is Nothing Then
        
        objSheet.Delete
    End If
    
    On Error GoTo ErrHandler
    
ErrHandler:
    objXl.DisplayAlerts = blnOldDisplayAlerts
End Sub


Public Function GetColFromRange(ByVal vobjRange As Excel.Range) As Collection
    
    Dim objCol As Collection, objChildRange As Excel.Range
    
    Set objCol = New Collection
    
    For Each objChildRange In vobjRange

        objCol.Add objChildRange.Value
    Next

    Set GetColFromRange = objCol
End Function


'''
'''
'''
Public Function GetWookbooksColFromBookPathCol(ByVal vobjBookPathCol As Collection) As Collection
    
    Dim objCol As Collection
    Dim varBookPath As Variant, strBookPath As String, objBook As Excel.Workbook
    
    
    Set objCol = New Collection
    
    For Each varBookPath In vobjBookPathCol
        
        strBookPath = varBookPath
    
        On Error Resume Next
    
        Set objBook = Nothing
    
        Set objBook = GetWorkbook(strBookPath, False, True)
        
        On Error GoTo 0
        
        If Not objBook Is Nothing Then
        
            objCol.Add objBook
        End If
    Next

    Set GetWookbooksColFromBookPathCol = objCol
End Function


'''
''' copy all cells without using clip-board
'''
Public Sub CopySheet(ByVal vobjSrcSheet As Excel.Worksheet, _
        ByVal vobjDstSheet As Excel.Worksheet, _
        Optional ByVal vblnAllowToUseXlRangeValueXMLSpreadsheetOption As Boolean = False, _
        Optional ByVal vblnAllowToCopyColumnWidths As Boolean = False)


    Dim objSrcBook As Excel.Workbook, objDstBook As Excel.Workbook
    
    Dim objSrcRange As Excel.Range, objDstRange As Excel.Range
    
    
    Set objSrcBook = vobjSrcSheet.Parent
    
    Set objDstBook = vobjDstSheet.Parent
    
    If StrComp(objSrcBook.Name, objDstBook.Name) <> 0 Then
    
        vobjDstSheet.Name = vobjSrcSheet.Name
    End If
    
    Set objSrcRange = vobjSrcSheet.UsedRange
    
    Set objDstRange = vobjDstSheet.Range(objSrcRange.Address)

    If vblnAllowToUseXlRangeValueXMLSpreadsheetOption Then
    
        ' Avoid an application-error...
        
        objDstRange.Worksheet.Activate
        
        If GetInputState() Then
        
            DoEvents: DoEvents: DoEvents
        End If
    
        objDstRange.Value(xlRangeValueXMLSpreadsheet) = objSrcRange.Value(xlRangeValueXMLSpreadsheet)   ' No use clip-board
    Else
        objDstRange.Value = objSrcRange.Value   ' No use clip-board
    End If
    
    If vblnAllowToCopyColumnWidths Then
    
        CopyColumnWidths vobjSrcSheet, vobjDstSheet
    End If
End Sub

'''
''' using xlRangeValueXMLSpreadsheet
'''
Public Sub CopySheetWithCellFormatting(ByVal vobjSrcSheet As Excel.Worksheet, ByVal vobjDstSheet As Excel.Worksheet)

    CopySheet vobjSrcSheet, vobjDstSheet, True
End Sub


'''
''' copy all cells without using clip-board
'''
Public Sub CopySheetToBook(ByVal vobjSrcSheet As Excel.Worksheet, _
        ByVal vobjDstBook As Excel.Workbook, _
        Optional ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern, _
        Optional ByVal venmRecordCellsBorders As RecordCellsBorders)
    
    
    Dim objNewDstSheet As Excel.Worksheet
    
    Set objNewDstSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(vobjDstBook)
    
    CopySheet vobjSrcSheet, objNewDstSheet

    CopyColumnWidths vobjSrcSheet, objNewDstSheet

    DecorateRangeSmartly objNewDstSheet.UsedRange, _
            venmManualDecoratingFieldTitlesRangePattern, _
            venmRecordCellsBorders
End Sub

'''
'''
'''
Public Sub CopyColumnWidths(ByVal vobjSrcSheet As Excel.Worksheet, _
        ByVal vobjDstSheet As Excel.Worksheet)
    
    Dim intColumnIdx As Long, strColumnLetter As String, strRange As String
    
    With vobjSrcSheet
        
        For intColumnIdx = 1 To .UsedRange.Columns.Count
        
            strColumnLetter = ConvertXlColumnIndexToLetter(intColumnIdx)
            
            strRange = strColumnLetter & ":" & strColumnLetter
            
            vobjDstSheet.Columns(strRange).ColumnWidth = .Columns(strRange).ColumnWidth
        Next
    End With
End Sub


'''
'''
'''
Public Sub CopyTitles(ByVal vobjSrcSheet As Excel.Worksheet, ByVal vobjDstSheet As Excel.Worksheet)

    Dim intColumnMax As Long, strColumnMax As String, strRange As String


    intColumnMax = vobjSrcSheet.UsedRange.Columns.Count
    
    strColumnMax = ConvertXlColumnIndexToLetter(intColumnMax)
    
    strRange = "A1:" & strColumnMax & "1"
    
    vobjDstSheet.Range(strRange).Value = vobjSrcSheet.Range(strRange).Value ' No use clip-board
End Sub

'''
'''
'''
Public Sub ShowBottomUsedRangeOfWorksheet(ByRef robjSheet As Excel.Worksheet)

    Dim intRowIndex As Long, intColumnIndex As Long
    
    With robjSheet
    
        If Not .UsedRange Is Nothing Then
    
            With .UsedRange
        
                intColumnIndex = .Column
        
                intRowIndex = .Row + .Rows.Count - 1
            End With
        End If
        
        If intRowIndex > 1 Or intColumnIndex > 1 Then
        
            .Range(ConvertXlColumnIndexToLetter(intColumnIndex) & CStr(intRowIndex)).Show
        End If
    End With
End Sub

'''
'''
'''
Public Sub CreateDefaultWorkbookAndCloseIfItDoesntExist(ByVal vstrBookPath As String)

    Dim objBook As Excel.Workbook
    
    Set objBook = Nothing
    
    Set objBook = GetWorkbook(vstrBookPath, False)
    
    If objBook Is Nothing Then
    
        Set objBook = GetWorkbook(vstrBookPath, True)
    
        With New Scripting.FileSystemObject
        
            Select Case LCase(.GetExtensionName(vstrBookPath))
        
                Case "xls", "xlsx"
        
                    ForceToSaveAsBookWithoutDisplayAlert objBook, vstrBookPath
        
                Case "xlsm"
                
                    ForceToSaveAsMacroBookWithoutDisplayAlert objBook, vstrBookPath
            End Select
        End With
    End If
    
    objBook.Close
End Sub


'**---------------------------------------------
'** About PrintArea
'**---------------------------------------------
'''
'''
'''
Public Sub SetPrintAreaOnWorksheetBasedOnExistedColumnsPrintArea(ByRef robjSheet As Excel.Worksheet, ByVal vintPrintAreaRowsCount As Long)

    Dim objPrintAreaRange As Excel.Range
    
    With robjSheet
    
        If .PageSetup.PrintArea <> "" Then
    
            With .Range(.PageSetup.PrintArea)
            
                Set objPrintAreaRange = robjSheet.Range(.Cells(1, 1), .Cells(vintPrintAreaRowsCount, .Columns.Count))
            End With
            
            .PageSetup.PrintArea = objPrintAreaRange.Address
        End If
    End With
End Sub


