Attribute VB_Name = "ColumnsDataConvertForXl"
'
'   Convert some type data from one type to another type on a Excel.Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  4/May/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Columns data convert main
'**---------------------------------------------
'''
'''
'''
Public Sub ConvertDataForColumns(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitleToConvertType As Scripting.Dictionary, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    Dim objTableRange As Excel.Range, objFieldTitleToIndexDic As Scripting.Dictionary
    Dim varFieldTitle As Variant, intColumnIndex As Long, intRowIndex As Long
    Dim intStartRowIdx As Long, intEndRowIdx As Long
    Dim enmColumnDataConvertType As ColumnDataConvertType, blnIsChanged As Boolean
    
    Set objFieldTitleToIndexDic = GetFieldTitleToPositionIndexDic(GetEnumeratorKeysColFromDic(vobjFieldTitleToConvertType), vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
    If Not objFieldTitleToIndexDic Is Nothing Then
    
        Set objTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
        If Not objTableRange Is Nothing Then
        
            If objTableRange.Rows.Count > 1 Then
            
                With objFieldTitleToIndexDic
                
                    For Each varFieldTitle In .Keys
                    
                        intColumnIndex = .Item(varFieldTitle)
                        
                        enmColumnDataConvertType = vobjFieldTitleToConvertType.Item(varFieldTitle)
                        
                        blnIsChanged = False
                        
                        With objTableRange
                        
                            intStartRowIdx = 2
                            
                            intEndRowIdx = .Rows.Count
                            
                            ReDim varColumnValues(1 To .Rows.Count, 1 To 1)
                            
                            varColumnValues = .Cells(1, intColumnIndex).Resize(.Rows.Count, 1).Value
                            
                            For intRowIndex = intStartRowIdx To intEndRowIdx
                                
                                If ConvertValueFromOneTypeToAnotherTypeBy(varColumnValues(intRowIndex, 1), enmColumnDataConvertType) Then
                                
                                    blnIsChanged = True
                                End If
                            Next
                            
                            If blnIsChanged Then
                            
                                .Cells(1, intColumnIndex).Resize(.Rows.Count, 1).Value = varColumnValues
                            End If
                        End With
                    Next
                End With
            End If
        End If
    End If
End Sub



'**---------------------------------------------
'** Worksheet name conversions
'**---------------------------------------------
Public Sub ChangeSheetNameFromHalfWidthKanaToFullWidthKana(ByVal vobjSheet As Excel.Worksheet)
    
    With vobjSheet
    
        .Name = ConvertKanaFromHalfWidthToFullWidth(.Name)
    End With
End Sub

Public Sub ChangeBookSheetNamesFromHalfWidthKanaToFullWidthKana(ByVal vobjBook As Excel.Workbook)
    
    Dim objSheet As Excel.Worksheet
    
    For Each objSheet In vobjBook.Sheets

        With objSheet
            
            .Name = ConvertKanaFromHalfWidthToFullWidth(.Name)
        End With
    Next
End Sub


'**---------------------------------------------
'** Deleting by RegExp hit
'**---------------------------------------------
'''
'''
'''
Public Sub DeleteCellsByRegExpPatternForColumns(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vstrDeletingPattern As String, _
        ByVal vobjFieldTitles As Collection, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    
    Dim objRegExp As VBScript_RegExp_55.RegExp, objFieldTitleToIndexDic As Scripting.Dictionary
    Dim varFieldTitle As Variant, intColumnIndex As Long, intRowIndex As Long, intRowMax As Long
    Dim varValues As Variant, strValue As String, blnIsChanged As Boolean

    If vstrDeletingPattern <> "" Then
       
        Set objFieldTitleToIndexDic = GetFieldTitleToPositionIndexDic(vobjFieldTitles, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
        Set objRegExp = New VBScript_RegExp_55.RegExp
        
        With objRegExp
        
            .Global = True
            
            .Pattern = vstrDeletingPattern
        End With
    
        With vobjSheet
        
            intRowMax = .UsedRange.Rows.Count
        
            For Each varFieldTitle In objFieldTitleToIndexDic.Keys
            
                intColumnIndex = objFieldTitleToIndexDic.Item(varFieldTitle)
                
                ReDim varValues(1 To intRowMax, 1 To 1)
                
                varValues = .Cells(1, intColumnIndex).Resize(intRowMax, 1).Value
                
                blnIsChanged = False
                 
                For intRowIndex = vintFieldTitlesRowIndex + 1 To intRowMax
                    
                    If Not IsEmpty(varValues(intRowIndex, 1)) Then
                        
                        strValue = CStr(varValues(intRowIndex, 1))
                    
                        If objRegExp.Test(strValue) Then
                        
                            varValues(intRowIndex, 1) = ""
                        
                            blnIsChanged = True
                        End If
                    End If
                Next
                
                If blnIsChanged Then
                
                    .Cells(1, intColumnIndex).Resize(intRowMax, 1).Value = varValues
                End If
            Next
        End With
    End If
End Sub
