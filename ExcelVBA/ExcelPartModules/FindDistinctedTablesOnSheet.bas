Attribute VB_Name = "FindDistinctedTablesOnSheet"
'
'   Find distincted tables from not-tables sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on FindForNotTablesOnSheet.bas, NotTableDistinctCondition.bas, DistinctedNotTable.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 17/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Get result log of extracting by DistinctedNotTable
'**---------------------------------------------
'''
'''
'''
Public Function GetTableSelectingParamsLogDic(Optional ByVal vobjSelectingFieldTitlesCol As Collection = Nothing, _
        Optional ByVal vobjIsNotNothingConditionFieldTitlesCol As Collection = Nothing, _
        Optional ByVal vobjInUsedValuesConditionFieldTitleToValuesDelimitedCommaDic As Scripting.Dictionary = Nothing) As Scripting.Dictionary


    Dim objLogDic As Scripting.Dictionary
    
    Set objLogDic = New Scripting.Dictionary
    
    With objLogDic
    
        If Not vobjSelectingFieldTitlesCol Is Nothing Then
        
            .Add "Selecting-field-titles", GetLineTextFromCol(vobjSelectingFieldTitlesCol)
        End If
        
        If Not vobjIsNotNothingConditionFieldTitlesCol Is Nothing Then
        
            .Add "Is-not-null conditions", GetLineTextFromCol(vobjIsNotNothingConditionFieldTitlesCol)
        End If
        
        If Not vobjInUsedValuesConditionFieldTitleToValuesDelimitedCommaDic Is Nothing Then
        
            .Add "In-included-values conditions", GetLineTextFromDic(vobjInUsedValuesConditionFieldTitleToValuesDelimitedCommaDic, "", ";")
        End If
    End With

    Set GetTableSelectingParamsLogDic = objLogDic
End Function


'''
'''
'''
Public Function GetDistinctedNotTableResultLogDic(ByRef robjDistinctedNotTable As DistinctedNotTable) As Scripting.Dictionary

    Dim objLogDic As Scripting.Dictionary, objBook As Excel.Workbook, i As Long, varKey As Variant


    Set objLogDic = New Scripting.Dictionary
    
    With robjDistinctedNotTable
    
        Set objBook = .MatchedSheet.Parent
        
        objLogDic.Add "Source book path", objBook.FullName
    
        objLogDic.Add "Source sheet name", .MatchedSheet.Name
        
        objLogDic.Add "Distincted area address", .DistinctedRangeAddress
        
        If Not .AdditionalInfoTypeNameToExtractedValueDic Is Nothing Then
        
            With .AdditionalInfoTypeNameToExtractedValueDic
            
                i = 1
                
                For Each varKey In .Keys
                
                    objLogDic.Add "Extracted additional information" & CStr(i) & " (" & CStr(varKey) & ")", .Item(varKey)
                
                    i = i + 1
                Next
            End With
        End If
    End With

    Set GetDistinctedNotTableResultLogDic = objLogDic
End Function


'**---------------------------------------------
'** Emphasize font and field-title
'**---------------------------------------------
'''
'''
'''
Public Sub ChangeFontSizeForAdditionalInfo(ByRef robjRegion As Excel.Range, ByRef robjRowIndexToAdditionalInfoDic As Scripting.Dictionary, ByVal vstrKeyWord As String, ByVal vsngFontSize As Single)

    Dim varRowIndex As Variant, intRowIndex As Long, objAdditionalInfoCol As Collection, varInfo As Variant, objRange As Excel.Range
    Dim blnNeedToChangeFontSize As Boolean
    
    With robjRowIndexToAdditionalInfoDic
    
        For Each varRowIndex In .Keys
        
            intRowIndex = varRowIndex
            
            blnNeedToChangeFontSize = False
            
            Set objAdditionalInfoCol = .Item(varRowIndex)
            
            For Each varInfo In objAdditionalInfoCol
            
                If InStr(1, CStr(varInfo), vstrKeyWord) > 0 Then
                
                    blnNeedToChangeFontSize = True
                    
                    Exit For
                End If
            Next
            
            If blnNeedToChangeFontSize Then
            
                With robjRegion
                
                    Set objRange = .Worksheet.Range(.Cells(intRowIndex, 1), .Cells(intRowIndex, objAdditionalInfoCol.Count))
                    
                    With objRange.Font
                    
                        .Size = vsngFontSize
                    End With
                End With
            End If
        Next
    End With
End Sub


'**---------------------------------------------
'** Distinct not-tables from a sheet
'**---------------------------------------------
'''
'''
'''
Public Function GetDistinctTablesDicFromSomeRegions(ByRef robjRegions As Collection, _
        ByRef robjNotTableDistinctConditions As Collection, _
        Optional ByVal vstrExceptionKeywordsDelimitedByComma As String = "") As Scripting.Dictionary

    Dim objRegions As Collection, varTopLeftRange As Excel.Range, objTopLeftRange As Excel.Range, objRegion As Excel.Range
    
    Dim varNotTableDistinctCondition As Variant, objNotTableDistinctCondition As NotTableDistinctCondition
    Dim varDistinctKey As Variant
    Dim objKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary
    Dim enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag As ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag
    
    
    Dim objAdditionalRowIndexToFoundValuesDic As Scripting.Dictionary, blnIsANotTableDistinctConditionMatched As Boolean, varValue As Variant
    Dim objDistinctedNotTable As DistinctedNotTable
    Dim objIdentifierNameToDistinctedNotTableDic As Scripting.Dictionary
    
    
    Set objIdentifierNameToDistinctedNotTableDic = New Scripting.Dictionary
    
    For Each varTopLeftRange In robjRegions
    
        Set objTopLeftRange = varTopLeftRange
        
        Set objRegion = objTopLeftRange.CurrentRegion
        
        
        Set objDistinctedNotTable = GetADistinctTableFromARegion(objRegion, robjNotTableDistinctConditions, vstrExceptionKeywordsDelimitedByComma)
        
        If Not objDistinctedNotTable Is Nothing Then
        
            objIdentifierNameToDistinctedNotTableDic.Add objDistinctedNotTable.IdentifierName, objDistinctedNotTable
        End If
    Next
   
    Set GetDistinctTablesDicFromSomeRegions = objIdentifierNameToDistinctedNotTableDic
End Function

'''
'''
'''
Public Function GetADistinctTableFromARegion(ByRef robjRegion As Excel.Range, _
        ByRef robjNotTableDistinctConditions As Collection, _
        Optional ByVal vstrExceptionKeywordsDelimitedByComma As String = "") As DistinctedNotTable

    Dim objDistinctedNotTable As DistinctedNotTable
    Dim varNotTableDistinctCondition As Variant, objNotTableDistinctCondition As NotTableDistinctCondition
    Dim varDistinctKey As Variant
    Dim objKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary
    Dim enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag As ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag
    
    
    Dim objAdditionalRowIndexToFoundValuesDic As Scripting.Dictionary, blnIsANotTableDistinctConditionMatched As Boolean, varValue As Variant
    

    'Set robjRegion = objTopLeftRange.CurrentRegion
        
        
    Set objDistinctedNotTable = Nothing
    
    ' replace wide-parenthesis for Japanese environment Excel
    
    ReplaceWideParenIntoOrdinaryParenInRange robjRegion
    
    If vstrExceptionKeywordsDelimitedByComma <> "" Then
    
        ReplaceWideAlphaNumIntoOrdinaryAlphaNumInCurrentRegion robjRegion, True, GetColFromLineDelimitedChar(vstrExceptionKeywordsDelimitedByComma)
    Else
        ReplaceWideAlphaNumIntoOrdinaryAlphaNumInCurrentRegion robjRegion, True
    End If
    
    ReplaceHalfWideKanaIntoOrdinaryKanaInRange robjRegion
                
                
    For Each varNotTableDistinctCondition In robjNotTableDistinctConditions
    
        Set objNotTableDistinctCondition = varNotTableDistinctCondition
    
        Set objAdditionalRowIndexToFoundValuesDic = New Scripting.Dictionary
    
        GetAdditionalRowIndexToFoundValuesDicFromDistinctedAreaOfDistinctedNotTable blnIsANotTableDistinctConditionMatched, objAdditionalRowIndexToFoundValuesDic, robjRegion, objNotTableDistinctCondition
    
        If blnIsANotTableDistinctConditionMatched Then
        
            Set objDistinctedNotTable = GetDistinctedNotTableBasedOnFoundInformations(robjRegion, objNotTableDistinctCondition, objAdditionalRowIndexToFoundValuesDic)
            
            'Debug.Print "Found additional keyword: " & objNotTableDistinctCondition.DistinctIncludedKeyword & ", Found sheet address: " & robjRegion.Address
        
        
            ' Extract characteristic values by NotTableDistinctCondition.AdditionalInfoTypeNameToExtractionRegExpPatternDic
            
            ' Use IsRegExpPatternMatchedAboutATextString in FindKeywords.bas
            
            InitializeAdditionalInformationFromDistinctedAreaOfDistinctedNotTable objDistinctedNotTable
        
            CorrectColumnHeaderTitles robjRegion, objNotTableDistinctCondition
        
            Exit For
        End If
    Next

    Set GetADistinctTableFromARegion = objDistinctedNotTable
End Function



'''
'''
'''
''' <Argument>rblnIsANotTableDistinctConditionMatched: Output</Argument>
''' <Argument>robjAdditionalRowIndexToFoundValuesDic: Output</Argument>
''' <Argument>robjRegion: Input</Argument>
''' <Argument>robjNotTableDistinctCondition: Input</Argument>
Public Sub GetAdditionalRowIndexToFoundValuesDicFromDistinctedAreaOfDistinctedNotTable(ByRef rblnIsANotTableDistinctConditionMatched As Boolean, _
        ByRef robjAdditionalRowIndexToFoundValuesDic As Scripting.Dictionary, _
        ByRef robjRegion As Excel.Range, _
        ByRef robjNotTableDistinctCondition As NotTableDistinctCondition)


    Dim varDistinctKey As Variant
    Dim objKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary
    Dim enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag As ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag

    rblnIsANotTableDistinctConditionMatched = False
    
    With robjNotTableDistinctCondition
    
        Set objKeyToRowIndexToMatchedPatternDic = GetRowIndexesOnRangeRegionByRowValuesPatterns(robjRegion, .DistinctLineValuesPatternKeyToProcessingFlagDic, True)
        
        With .DistinctLineValuesPatternKeyToProcessingFlagDic
        
            For Each varDistinctKey In .Keys
            
                enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag = .Item(varDistinctKey)
            
                If enmProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag = RecordValuesToCacheAfterRangeLineValuesMatched Then
                
                    msubFindKeywordFromAddtionalInfoOfNotTableRegion rblnIsANotTableDistinctConditionMatched, robjAdditionalRowIndexToFoundValuesDic, objKeyToRowIndexToMatchedPatternDic, varDistinctKey, robjNotTableDistinctCondition
                End If
            Next
        End With
    End With
End Sub


'''
'''
'''
Public Sub ResetAdditionalRowIndexToFoundValuesDicFromDistinctedAreaOfDistinctedNotTable(ByRef robjDistinctedNotTable As DistinctedNotTable)

    Dim objRegion As Excel.Range
    Dim blnIsANotTableDistinctConditionMatched As Boolean, objAdditionalRowIndexToFoundValuesDic As Scripting.Dictionary

    With robjDistinctedNotTable
    
        If Not .AdditionalRowIndexToFoundValuesDic Is Nothing Then
        
            .AdditionalRowIndexToFoundValuesDic.RemoveAll
        End If
    
    
        Set objRegion = .MatchedSheet.Range(.DistinctedRangeAddress)
    
        Set objAdditionalRowIndexToFoundValuesDic = New Scripting.Dictionary
    
        GetAdditionalRowIndexToFoundValuesDicFromDistinctedAreaOfDistinctedNotTable blnIsANotTableDistinctConditionMatched, objAdditionalRowIndexToFoundValuesDic, objRegion, .DistinctCondition
        
        Set .AdditionalRowIndexToFoundValuesDic = objAdditionalRowIndexToFoundValuesDic
    End With
End Sub


'''
'''
'''
Public Sub ResetAdditionalInformationFromDistinctedAreaOfDistinctedNotTable(ByRef robjDistinctedNotTable As DistinctedNotTable)

    ResetAdditionalRowIndexToFoundValuesDicFromDistinctedAreaOfDistinctedNotTable robjDistinctedNotTable

    With robjDistinctedNotTable

        If Not .AdditionalInfoTypeNameToExtractedValueDic Is Nothing Then
        
            .AdditionalInfoTypeNameToExtractedValueDic.RemoveAll
        End If

        InitializeAdditionalInformationFromDistinctedAreaOfDistinctedNotTable robjDistinctedNotTable
    End With
End Sub


'''
'''
'''
Public Sub InitializeAdditionalInformationFromDistinctedAreaOfDistinctedNotTable(ByRef robjDistinctedNotTable As DistinctedNotTable)

    With robjDistinctedNotTable
    
        FindCharacteristicAdditionalInformationFromDistinctedArea robjDistinctedNotTable
            
        robjDistinctedNotTable.SetIdentifierNameAutomaticallyFromDistinctedTableTypeNameAndAdditionalRowIndexToFoundValuesDic
    End With
End Sub



'''
'''
'''
Public Function GetAdditionalInformationColFromIdentifierNameSuffix(ByVal vstrSuffix As String) As Collection

    Dim strInfoString As String, varItem As Variant, objCol As Collection
    
    If InStr(1, vstrSuffix, "_") = 1 Then
    
        strInfoString = Right(vstrSuffix, Len(vstrSuffix) - 1)
    Else
        strInfoString = vstrSuffix
    End If

    Set objCol = New Collection

    For Each varItem In Split(strInfoString, "_")
    
        objCol.Add GetVariantValueFromGeneralStringInformation(CStr(varItem))
    Next

    Set GetAdditionalInformationColFromIdentifierNameSuffix = objCol
End Function


'''
'''
'''
Public Sub FindCharacteristicAdditionalInformationFromDistinctedArea(ByRef robjDistinctedNotTable As DistinctedNotTable)

    Dim strFoundAdditionalInfoText As String, varName As Variant, strRegExpPattern As String, varFoundValue As Variant

    With robjDistinctedNotTable
    
        If Not .DistinctCondition.AdditionalInfoTypeNameToExtractionRegExpPatternDic Is Nothing Then
        
            strFoundAdditionalInfoText = .GetAllConnectedInfosFromAdditionalRowIndexToFoundValuesDic()
        
            If strFoundAdditionalInfoText <> "" Then
            
                With .DistinctCondition.AdditionalInfoTypeNameToExtractionRegExpPatternDic
                
                    For Each varName In .Keys
                    
                        strRegExpPattern = .Item(varName)
                    
                        varFoundValue = ExtractVariantValueFromTextByRegExpPattern(strFoundAdditionalInfoText, strRegExpPattern)
                    
                        If Not IsEmpty(varFoundValue) Then
                        
                            If robjDistinctedNotTable.AdditionalInfoTypeNameToExtractedValueDic Is Nothing Then Set robjDistinctedNotTable.AdditionalInfoTypeNameToExtractedValueDic = New Scripting.Dictionary
                        
                            With robjDistinctedNotTable.AdditionalInfoTypeNameToExtractedValueDic
                            
                                .Add varName, varFoundValue
                            End With
                        End If
                    Next
                End With
            End If
        End If
    End With
End Sub

'''
'''
'''
Public Sub CorrectColumnHeaderTitles(ByRef robjRegion As Excel.Range, ByRef robjNotTableDistinctCondition As NotTableDistinctCondition)

    Dim objKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary

    With robjNotTableDistinctCondition
    
        Set objKeyToRowIndexToMatchedPatternDic = GetRowIndexesOnRangeRegionByRowValuesPatterns(robjRegion, .DistinctLineValuesPatternKeyToProcessingFlagDic)
        
        'DebugDic objKeyToRowIndexToMatchedPatternDic
        
        ReplaceRowValuesBasedOnMatchedRowValuesPatternsOnARangeRegion .LineValuesPatternKeyToCorrectedValuesStringDic, objKeyToRowIndexToMatchedPatternDic, robjRegion
    End With
End Sub

'''
'''
'''
Public Sub CorrectSpecifiedRowRecordsBasedOnVariantStringConversion(ByRef robjRegion As Excel.Range, ByRef robjSrcDTCol As Collection, ByRef robjDstDTCol As Collection)

    Dim objKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary, objLinesPatternKeyToProcessFlagDic As Scripting.Dictionary
    Dim objLinesPatternKeyToCorrectedValuesStringDic As Scripting.Dictionary
    
    
    GetConversionDictinariesFromSourceDTColAndDestinationDTCol objLinesPatternKeyToProcessFlagDic, objLinesPatternKeyToCorrectedValuesStringDic, robjSrcDTCol, robjDstDTCol
    
    Set objKeyToRowIndexToMatchedPatternDic = GetRowIndexesOnRangeRegionByRowValuesPatterns(robjRegion, objLinesPatternKeyToProcessFlagDic)
    
    ReplaceRowValuesBasedOnMatchedRowValuesPatternsOnARangeRegion objLinesPatternKeyToCorrectedValuesStringDic, objKeyToRowIndexToMatchedPatternDic, robjRegion
End Sub

'''
'''
'''
Public Sub GetConversionDictinariesFromSourceDTColAndDestinationDTCol(ByRef robjLinesPatternKeyToProcessFlagDic As Scripting.Dictionary, _
        ByRef robjLinesPatternKeyToCorrectedValuesStringDic As Scripting.Dictionary, _
        ByRef robjSrcDTCol As Collection, _
        ByRef robjDstDTCol As Collection)
        
    
    Dim varSrcRowCol As Variant, objSrcRowCol As Collection, i As Long, objDstRowCol As Collection
    Dim strSrcRowPattern As String, strDstRowPattern As String
    
    
    Set robjLinesPatternKeyToProcessFlagDic = New Scripting.Dictionary
    
    Set robjLinesPatternKeyToCorrectedValuesStringDic = New Scripting.Dictionary
    
    i = 1
    
    For Each varSrcRowCol In robjSrcDTCol
    
        Set objSrcRowCol = varSrcRowCol
        
        Set objDstRowCol = robjDstDTCol.Item(i)
        
        strSrcRowPattern = GetLineTextFromCol(objSrcRowCol, vblnIncludeSpaceAfterDelimitedChar:=False)
    
        strDstRowPattern = GetLineTextFromCol(objDstRowCol, vblnIncludeSpaceAfterDelimitedChar:=False)
    
        robjLinesPatternKeyToProcessFlagDic.Add strDstRowPattern, ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag.RecordValuesToCacheAfterRangeLineValuesMatched
    
        robjLinesPatternKeyToCorrectedValuesStringDic.Add strDstRowPattern, strSrcRowPattern
    
        i = i + 1
    Next
    
End Sub
        
        
        
        


'''
'''
'''
Public Sub ReplaceSpecifiedKeywordsByExaxtMatchForEachColumnRecordsOfDistinctedTable(ByRef robjDistinctedNotTable As DistinctedNotTable, _
        ByRef robjReplaceWordsDic As Scripting.Dictionary, _
        ByVal vobjTargetFieldTitlesCol As Collection)

    Dim intFieldTitlesRowIndex As Long, objRegion As Excel.Range
    
    With robjDistinctedNotTable
    
        Set objRegion = .MatchedSheet.Range(.DistinctedRangeAddress)
        
        intFieldTitlesRowIndex = GetFieldTitlesRowIndexInNotTableCurrentRegion(objRegion, .DistinctCondition)
        
        ReplaceSpecifiedKeywordsByExaxtMatchForEachColumnRecords objRegion, robjReplaceWordsDic, vobjTargetFieldTitlesCol, intFieldTitlesRowIndex
    End With
End Sub

'''
'''
'''
Public Function GetFieldTitlesRowIndexInNotTableCurrentRegion(ByRef robjRegion As Excel.Range, ByRef robjNotTableDistinctCondition As NotTableDistinctCondition) As Long

    Dim objKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary, objDic As Scripting.Dictionary
    Dim objRowIndexToActualPatternDic As Scripting.Dictionary
    Dim intRowIndex As Long
    
    With robjNotTableDistinctCondition
    
        Set objDic = New Scripting.Dictionary
        
        objDic.Add .CorrectedColumnHeaderTitlesPattern, ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag.ReconizeFieldTitlesAfterRangeLineValuesMatched
    
        Set objKeyToRowIndexToMatchedPatternDic = GetRowIndexesOnRangeRegionByRowValuesPatterns(robjRegion, objDic, True)
        
        Set objRowIndexToActualPatternDic = objKeyToRowIndexToMatchedPatternDic.Item(.CorrectedColumnHeaderTitlesPattern)
        
        intRowIndex = objRowIndexToActualPatternDic.Keys(0)
    End With
    
    GetFieldTitlesRowIndexInNotTableCurrentRegion = intRowIndex
End Function


'''
'''
'''
Public Function GetFieldTitlesAllRange(ByRef robjRegion As Excel.Range, ByRef robjNotTableDistinctCondition As NotTableDistinctCondition) As Excel.Range

    Dim objKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary, objDic As Scripting.Dictionary
    Dim objRowIndexToActualPatternDic As Scripting.Dictionary, varRowIndex As Variant, intRowIndex As Long, objRange As Excel.Range, objUnionRange As Excel.Range

    With robjNotTableDistinctCondition
    
        Set objDic = New Scripting.Dictionary: objDic.Add .CorrectedColumnHeaderTitlesPattern, ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag.ReconizeFieldTitlesAfterRangeLineValuesMatched
    
        Set objKeyToRowIndexToMatchedPatternDic = GetRowIndexesOnRangeRegionByRowValuesPatterns(robjRegion, objDic)
        
        
        
        Set objUnionRange = Nothing
        
        Set objRowIndexToActualPatternDic = objKeyToRowIndexToMatchedPatternDic.Item(.CorrectedColumnHeaderTitlesPattern)
        
        With objRowIndexToActualPatternDic
        
            For Each varRowIndex In .Keys
            
                intRowIndex = varRowIndex
            
                With robjRegion
                
                    Set objRange = .Worksheet.Range(.Cells(intRowIndex, 1), .Cells(intRowIndex, .Columns.Count))
                    
                    If objUnionRange Is Nothing Then
                    
                        Set objUnionRange = objRange
                    Else
                        Set objUnionRange = Union(objUnionRange, objRange)
                    End If
                End With
            Next
        End With
    End With


    Set GetFieldTitlesAllRange = objUnionRange
End Function



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>rblnIsANotTableDistinctConditionMatched: Output</Argument>
''' <Argument>robjAdditionalRowIndexToFoundValuesDic: Output</Argument>
''' <Argument>robjKeyToRowIndexToMatchedPatternDic: Input</Argument>
''' <Argument>rvarDistinctKey: Input</Argument>
''' <Argument>robjCurrentNotTableDistinctCondition: Input</Argument>
Private Sub msubFindKeywordFromAddtionalInfoOfNotTableRegion(ByRef rblnIsANotTableDistinctConditionMatched As Boolean, _
        ByRef robjAdditionalRowIndexToFoundValuesDic As Scripting.Dictionary, _
        ByRef robjKeyToRowIndexToMatchedPatternDic As Scripting.Dictionary, _
        ByRef rvarDistinctKey As Variant, _
        ByRef robjCurrentNotTableDistinctCondition As NotTableDistinctCondition)

    Dim objIndexToMatchedPatternDic As Scripting.Dictionary, varRowIndex As Variant, objActualValuesCol As Collection
    Dim varValue As Variant

    With robjKeyToRowIndexToMatchedPatternDic
                            
        If .Exists(rvarDistinctKey) Then
    
            Set objIndexToMatchedPatternDic = .Item(rvarDistinctKey)
            
            For Each varRowIndex In objIndexToMatchedPatternDic.Keys
            
                ' The following means some additional information
                
                Set objActualValuesCol = objIndexToMatchedPatternDic.Item(varRowIndex)
                
                For Each varValue In objActualValuesCol
                
                    If Not IsNumeric(varValue) Then
                    
                        If InStr(1, CStr(varValue), robjCurrentNotTableDistinctCondition.DistinctIncludedKeyword) > 0 Then
                        
                            rblnIsANotTableDistinctConditionMatched = True
                        End If
                    End If
                Next
                
                robjAdditionalRowIndexToFoundValuesDic.Add CLng(varRowIndex), objActualValuesCol
            Next
        End If
    End With
End Sub

'''
'''
'''
Private Function GetDistinctedNotTableBasedOnFoundInformations(ByRef robjRegion As Excel.Range, _
        ByRef robjNotTableDistinctCondition As NotTableDistinctCondition, _
        ByRef robjAdditionalRowIndexToFoundValuesDic As Scripting.Dictionary) As DistinctedNotTable

    Dim objDistinctedNotTable As DistinctedNotTable
    
    Set objDistinctedNotTable = New DistinctedNotTable

    With objDistinctedNotTable
                
        Set .DistinctCondition = robjNotTableDistinctCondition
    
        Set .MatchedSheet = robjRegion.Worksheet
        
        .DistinctedRangeAddress = robjRegion.Address
        
        Set .AdditionalRowIndexToFoundValuesDic = robjAdditionalRowIndexToFoundValuesDic
    End With

    Set GetDistinctedNotTableBasedOnFoundInformations = objDistinctedNotTable
End Function

