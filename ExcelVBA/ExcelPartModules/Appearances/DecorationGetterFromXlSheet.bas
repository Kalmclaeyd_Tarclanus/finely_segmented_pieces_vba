Attribute VB_Name = "DecorationGetterFromXlSheet"
'
'   get individual cell formats from existed Excel-book
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleKey As String = "DecorationGetterFromXlSheet"   ' 1st part of registry sub-key

'**---------------------------------------------
'** Constant Keys for appearances properties
'**---------------------------------------------
' Excel.FormatCondition parameters Collection each keys
Private Const mstrFcFormula1Key As String = "FcFormula1"

Private Const mstrFcFormula2Key As String = "FcFormula2"

Private Const mstrFcTextKey As String = "FcText"

Private Const mstrFcTypeKey As String = "FcType"

Private Const mstrFcOperatorKey As String = "FcOperator"

Private Const mstrFcTextOperatorKey As String = "FcTextOperator"

Private Const mstrFcDateOperatorKey As String = "FcDateOperator"


Private Const mstrFcInteriorKey As String = "FcInterior"   ' FormatCondition.Interior

Private Const mstrFcFontKey As String = "FcFont"           ' FromatCondition.Font

Private Const mstrFcBorders As String = "FcBorders"     ' FormatCondition.Borders

' Excel.Interior parameters Collection keys
Private Const mstrGInteriorPatternKey As String = "GInteriorPattern"

Private Const mstrGPatternColorIndex As String = "GPatternColorIndex"

Private Const mstrGPatternTintAndShadeKey As String = "GPatternTintAndShade"

Private Const mstrGGradientDegreeKey As String = "GGradientDegree"


' Excel.Font parameters Collection keys
Private Const mstrGFontNameKey As String = "GFontName"  ' Font.Name

Private Const mstrGFontSizeKey As String = "GFontSize"  ' Font.Size

Private Const mstrGThemeColorKey As String = "GFontThemeColor" ' Font.ThemeColor, Interior.ThemeColor

Private Const mstrGTintAndShadeKey As String = "GTintAndShade" ' Font.TintAndShade

Private Const mstrGColorKey As String = "GColor"   ' Font.Color


' Excel.ColorStops parameters Collection keys
Private Const mstrGColorStopsCountKey As String = "GColorStopsCount"    ' ColorStops.Count

Private Const mstrGColorStopPrefix As String = "GColorStop"

Private Const mstrGColorStopPosition As String = "GColorStopPosition"   ' ColorStop.Position    (As Double)

' Excel.Borders parameters Collection each keys


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About getting Cells Range Interior information
'**---------------------------------------------
'''
''' Use Scripting.Dictionary in order to cut processing waste time with using AdvancedFilter
'''
Public Function GetDistinctXlRangeValuesByFieldTitle(ByVal vstrFieldTitle As String, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1) As Collection

    Set GetDistinctXlRangeValuesByFieldTitle = GetDistinctXlRangeValues(GetRecordRangeFromFieldTitleByCurrentRegion(vstrFieldTitle, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex))
End Function

'''
''' Use Scripting.Dictionary in order to cut processing waste time with using AdvancedFilter
'''
Public Function GetDistinctXlRangeValues(ByVal vobjRange As Excel.Range) As Collection
    
    Dim objCol As Collection, strAddressArray() As String, varAddress As Variant
    Dim strIndividualAddress As String, objRange As Excel.Range, i As Long
    Dim objDic As Scripting.Dictionary, varValue As Variant, intRowIndex As Long, intColumnIndex As Long
    Dim varValues() As Variant
    

    Set objCol = Nothing

    If Not vobjRange Is Nothing Then
    
        Set objDic = New Scripting.Dictionary
    
        For Each varAddress In Split(vobjRange.Address, ",")
            
            Set objRange = vobjRange.Worksheet.Range(varAddress)
            
            With objRange
                
                If .Rows.Count = 1 And .Columns.Count = 1 Then
                
                    varValue = objRange.Value
                
                    If Not IsEmpty(varValue) Then
                        
                        With objDic
                            
                            If Not .Exists(varValue) Then
                            
                                .Add varValue, Nothing
                            End If
                        End With
                    End If
                Else
                    ReDim varValues(1 To .Rows.Count, 1 To .Columns.Count)
                
                    varValues = objRange.Value
                    
                    For intRowIndex = 1 To .Rows.Count
                        
                        For intColumnIndex = 1 To .Columns.Count
                        
                            If Not IsEmpty(varValues(intRowIndex, intColumnIndex)) Then
                            
                                varValue = varValues(intRowIndex, intColumnIndex)
                            
                                With objDic
                                
                                    If Not .Exists(varValue) Then
                                    
                                        .Add varValue, Nothing
                                    End If
                                End With
                            End If
                        Next
                    Next
                End If
            End With
        Next

        Set objCol = GetEnumeratorKeysColFromDic(objDic)
    End If

    Set GetDistinctXlRangeValues = objCol
End Function

'''
'''
'''
''' <Argument>vobjRange: Input</Argument>
''' <Return>Dictionary</Return>
Public Function GetValueToXlInteriorParamsDic(ByVal vobjRange As Excel.Range) As Scripting.Dictionary
    
    Dim strAddressArray() As String, strIndividualAddress As String, objRange As Excel.Range, i As Long
    Dim intRowIndex As Long, intColumnIndex As Long
    
    Dim objTextToInteriorParamDic As Scripting.Dictionary
    Dim varValues() As Variant, objInteriorValues As Collection, varValue As Variant, strValue As String
    
    
    Set objTextToInteriorParamDic = New Scripting.Dictionary
    
    strAddressArray = Split(vobjRange.Address, ",")
    
    For i = LBound(strAddressArray) To UBound(strAddressArray)
        
        Set objRange = vobjRange.Worksheet.Range(strAddressArray(i))
    
        With objRange
        
            If .Rows.Count = 1 And .Columns.Count = 1 Then
            
                varValue = objRange.Value
            
                With objTextToInteriorParamDic
                
                    If Not .Exists(varValue) Then
                                
                        Set objInteriorValues = New Collection
                    
                        ' About Interior
                        msubGetInteriorPropertiesToValues objInteriorValues, objRange.Interior
                    
                        .Add varValue, objInteriorValues
                    End If
                End With
            Else
                ReDim varValues(1 To .Rows.Count, 1 To .Columns.Count)
            
                varValues = objRange.Value
                
                For intRowIndex = 1 To .Rows.Count
                    
                    For intColumnIndex = 1 To .Columns.Count
                    
                        If Not IsEmpty(varValues(intRowIndex, intColumnIndex)) Then
                        
                            varValue = varValues(intRowIndex, intColumnIndex)
                        
                            With objTextToInteriorParamDic
                            
                                If Not .Exists(varValue) Then
                                
                                    Set objInteriorValues = New Collection
                                
                                    ' About Interior
                                    msubGetInteriorPropertiesToValues objInteriorValues, objRange.Cells(intRowIndex, intColumnIndex).Interior
                                
                                    .Add varValue, objInteriorValues
                                End If
                            End With
                        End If
                    Next
                Next
            End If
        End With
    Next

    Set GetValueToXlInteriorParamsDic = objTextToInteriorParamDic
End Function

'''
'''
'''
Public Sub OutputGettingFormatConditionsToSheet(ByVal vobjSheet As Excel.Worksheet, ByVal vobjFormatConditionInfoDic As Scripting.Dictionary, Optional ByVal vobjFormatConditionParamDic As Scripting.Dictionary = Nothing)
    
    Dim objFieldTitlesCol As Collection

    Set objFieldTitlesCol = GetColFromLineDelimitedChar("TextOrFormula,Formula1,Formula2,Text,FormatConditionType,FormatConditionOperator,TextOperator,DateOperator,InteriorColorR,InteriorColorG,InteriorColorB")

    OutputDicToSheet vobjSheet, vobjFormatConditionInfoDic, objFieldTitlesCol, mfobjGetDataTableSheetFormatterForFormatConditionsInformation(vobjFormatConditionParamDic)
End Sub


'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterForFormatConditionsInformation(Optional ByVal vobjFormatConditionParamDic As Scripting.Dictionary = Nothing) As DataTableSheetFormatter
    
    Dim objDataTableSheetFormatter As DataTableSheetFormatter, objFormatConditionExpander As FormatConditionExpander
    
    Set objDataTableSheetFormatter = New DataTableSheetFormatter

    With objDataTableSheetFormatter
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("TextOrFormula,15,Formula1,12,Formula2,12,Text,12,FormatConditionType,24,FormatConditionOperator,24")
    
        .FieldTitleInteriorType = ColumnNameInteriorOfMetaProperties
    
        If Not vobjFormatConditionParamDic Is Nothing Then
        
            Set .ColumnsFormatCondition = New ColumnsFormatConditionParam
            
            With .ColumnsFormatCondition
                
                .FieldTitlesRowIndex = 1
                
                Set objFormatConditionExpander = New FormatConditionExpander
                
                objFormatConditionExpander.AddFormulasConditionsFromParamDic vobjFormatConditionParamDic
                
                .FieldTitleToCellFormatCondition.Add "Formula1", objFormatConditionExpander
                
                .FieldTitleToCellFormatCondition.Add "Formula2", objFormatConditionExpander
            End With
        End If
    End With

    Set mfobjGetDataTableSheetFormatterForFormatConditionsInformation = objDataTableSheetFormatter
End Function

'''
'''
'''
Public Function GetFormatConditionInfoOfTextHittingFromFieldTitleByActiveCell() As Scripting.Dictionary
    
    Dim objSheet As Excel.Worksheet, strFieldTitle As String, intFieldTitlesRow As Long, intTopLeftColumnIndex As Long
    Dim intCurrentColumnIndex As Long
    
    With GetOfficeApplicationObject("Excel").Application.ActiveCell   ' at the current Excel.EXE process
        
        Set objSheet = .Worksheet
    
        intCurrentColumnIndex = .Column
    
        With .CurrentRegion
        
            intFieldTitlesRow = .Row
            
            intTopLeftColumnIndex = .Column
        End With
    
        strFieldTitle = .Worksheet.Range(ConvertXlColumnIndexToLetter(.Column) & CStr(intFieldTitlesRow)).Value
    End With
    
    Set GetFormatConditionInfoOfTextHittingFromFieldTitleByActiveCell = GetFormatConditionInfoOfTextHittingFromFieldTitle(strFieldTitle, _
            objSheet, _
            intFieldTitlesRow, _
            intTopLeftColumnIndex)
End Function


'''
''' this collection contents are defined by GetFormatConditionInfoOfTextHittingFromRange output
'''
Public Function GetFormatFormulasConditionFromValuesCol(ByVal vobjValues As Collection, _
        Optional ByVal vstrFormulaPrefix As String = "", _
        Optional ByVal vstrFormulaSuffix As String = "") As FormatFormulasCondition
    
    
    Dim objFormatFormulasCondition As FormatFormulasCondition, objInteriorValues As Collection
    
    Set objFormatFormulasCondition = New FormatFormulasCondition
    
    SetKeysPropertiesOfFormatFormulasConditionFromValuesCol objFormatFormulasCondition, vobjValues, vstrFormulaPrefix, vstrFormulaSuffix
        
    SetDecorationPropertiesOfFormatFormulasConditionFromValuesCol objFormatFormulasCondition, vobjValues
    
    Set GetFormatFormulasConditionFromValuesCol = objFormatFormulasCondition
End Function

'''
'''
'''
Public Sub SetKeysPropertiesOfFormatFormulasConditionFromValuesCol(ByRef robjFormatFormulasCondition As FormatFormulasCondition, _
        ByVal vobjValues As Collection, _
        Optional ByVal vstrFormulaPrefix As String = "", _
        Optional ByVal vstrFormulaSuffix As String = "")


    With robjFormatFormulasCondition
        
        .Formula1 = vobjValues.Item(mstrFcFormula1Key)
        
        .Formula2 = vobjValues.Item(mstrFcFormula2Key)
        
        .Text = vobjValues.Item(mstrFcTextKey)
        
        If vstrFormulaPrefix <> "" Then
            
            If .Formula1 <> "" Then
            
                .Formula1 = "=""" & vstrFormulaPrefix & GetInterFormulaOfCellExpression(.Formula1) & """"
            End If
            
            If .Formula2 <> "" Then
            
                .Formula2 = "=""" & vstrFormulaPrefix & GetInterFormulaOfCellExpression(.Formula2) & """"
            End If
            
            If .Text <> "" Then
            
                .Text = vstrFormulaPrefix & .Text
            End If
        End If
        
        If vstrFormulaSuffix <> "" Then
        
            If .Formula1 <> "" Then
            
                .Formula1 = "=""" & GetInterFormulaOfCellExpression(.Formula1) & vstrFormulaSuffix & """"
            End If
            
            If .Formula2 <> "" Then
            
                .Formula2 = "=""" & GetInterFormulaOfCellExpression(.Formula2) & vstrFormulaSuffix & """"
            End If
            
            If .Text <> "" Then
            
                .Text = .Text & vstrFormulaSuffix
            End If
        End If
    End With
End Sub

'''
'''
'''
''' <Argument>robjFormatFormulasCondition: Output</Argument>
''' <Argument>vobjValues: Input</Argument>
Public Sub SetDecorationPropertiesOfFormatFormulasConditionFromValuesCol(ByRef robjFormatFormulasCondition As FormatFormulasCondition, _
        ByVal vobjValues As Collection)
    
    Dim enmXlFormatConditionOperator As Excel.XlFormatConditionOperator
    
    With robjFormatFormulasCondition

        .FormatConditionType = vobjValues.Item(mstrFcTypeKey)
        
        
        enmXlFormatConditionOperator = 0
        
        On Error Resume Next
        
        enmXlFormatConditionOperator = vobjValues.Item(mstrFcOperatorKey)
        
        If Err.Number = 0 Then
        
            .FormatConditionOperator = enmXlFormatConditionOperator
        Else
            Err.Clear
        End If
        
        On Error GoTo 0
        
        Set .InteriorValues = vobjValues.Item(mstrFcInteriorKey)
    End With
End Sub



'''
'''
'''
Public Function ConvertKeyToFormatConditionValuesDicFromValuesToFomulasFormatCondition(ByVal vobjFormatConditionParamDic As Scripting.Dictionary, _
        Optional ByVal vstrFormulaPrefix As String = "", _
        Optional ByVal vstrFormulaSuffix As String = "") As Scripting.Dictionary
    
    
    Dim varKey As Variant, objValues As Collection
    Dim objKeyToFormatFomulasConditionDic As Scripting.Dictionary
    
    Set objKeyToFormatFomulasConditionDic = New Scripting.Dictionary
    
    With vobjFormatConditionParamDic
    
        For Each varKey In .Keys
        
            Set objValues = .Item(varKey)

            objKeyToFormatFomulasConditionDic.Add varKey, GetFormatFormulasConditionFromValuesCol(objValues, vstrFormulaPrefix, vstrFormulaSuffix)
        Next
    End With

    Set ConvertKeyToFormatConditionValuesDicFromValuesToFomulasFormatCondition = objKeyToFormatFomulasConditionDic
End Function

'''
'''
'''
Public Function ConvertKeyToFormatConditionValuesDicToFormatFomulasConditions(ByVal vobjFormatConditionParamDic As Scripting.Dictionary, _
        Optional ByVal vstrFormulaPrefix As String = "", _
        Optional ByVal vstrFormulaSuffix As String = "") As Collection
    
    Dim varKey As Variant, objValues As Collection, objFormatFomulasConditions As Collection
    
    Set objFormatFomulasConditions = New Collection
    
    With vobjFormatConditionParamDic
        
        For Each varKey In .Keys
        
            Set objValues = .Item(varKey)

            objFormatFomulasConditions.Add GetFormatFormulasConditionFromValuesCol(objValues, vstrFormulaPrefix, vstrFormulaSuffix)
        Next
    End With
    
    Set ConvertKeyToFormatConditionValuesDicToFormatFomulasConditions = objFormatFomulasConditions
End Function

'''
'''
'''
Public Sub DecorateFormulasFormatConditionRange(ByVal vobjFormatFomulasConditions As Collection, _
        ByVal vobjRange As Excel.Range)
    
    
    Dim objXlFormatCondition As Excel.FormatCondition
    Dim objFormatFormulasCondition As FormatFormulasCondition, varItem As Variant

    If Not vobjRange Is Nothing Then
    
        With vobjRange
        
            .FormatConditions.Delete
            
            If vobjFormatFomulasConditions.Count > 0 Then
            
                For Each varItem In vobjFormatFomulasConditions
                
                    Set objFormatFormulasCondition = varItem
                    
                    With objFormatFormulasCondition
                    
                        If .Formula1 <> "" And .Formula2 <> "" Then
                        
                            Set objXlFormatCondition = vobjRange.FormatConditions.Add(.FormatConditionType, Operator:=.FormatConditionOperator, Formula1:=.Formula1, Formula2:=.Formula2)
                            
                        ElseIf .Formula1 <> "" Then
                        
                            Set objXlFormatCondition = vobjRange.FormatConditions.Add(.FormatConditionType, Operator:=.FormatConditionOperator, Formula1:=.Formula1)
                            
                        ElseIf .Text <> "" Then
                        
                            Set objXlFormatCondition = vobjRange.FormatConditions.Add(.FormatConditionType, Operator:=.FormatConditionOperator, String:=.Text)
                        End If
                
                        With objXlFormatCondition
                        
                            DecorateFormatConditionInteriorFromValuesToInterior .Interior, objFormatFormulasCondition.InteriorValues
                        End With
                    End With
                Next
            End If
        End With
    End If
End Sub

'''
'''
'''
Public Sub DecorateFormulasFormatConditionsByFieldTitle(ByVal vobjFormatFomulasConditions As Collection, _
        ByVal vstrFieldTitle As String, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)

    DecorateFormulasFormatConditionRange vobjFormatFomulasConditions, GetRecordRangeFromFieldTitleByCurrentRegion(vstrFieldTitle, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
End Sub



'''
'''
'''
Public Function GetRecordRangeFromFieldTitleByCurrentRegion(ByVal vstrFieldTitle As String, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1) As Excel.Range
    
    
    Dim objRange As Excel.Range, intColumnIndex As Long, objFieldTitleToPositionIndexDic As Scripting.Dictionary
    
    Set objRange = Nothing
    
    Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(GetColFromLineDelimitedChar(vstrFieldTitle), vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
    If Not objFieldTitleToPositionIndexDic Is Nothing Then
        
        With objFieldTitleToPositionIndexDic
        
            If .Exists(vstrFieldTitle) Then
        
                intColumnIndex = .Item(vstrFieldTitle)
            
                With vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
                
                    'Set objRange = .Range(ConvertXlColumnIndexToLetter(intColumnIndex) & CStr(vintFieldTitlesRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(intColumnIndex) & CStr(.UsedRange.Rows.Count))
                    
                    Set objRange = .Worksheet.Range(.Cells(2, intColumnIndex), .Cells(.Rows.Count, intColumnIndex))
                End With
            End If
        End With
    End If

    Set GetRecordRangeFromFieldTitleByCurrentRegion = objRange
End Function


'''
'''
'''
Public Function GetFormatConditionInfoOfTextHittingFromFieldTitle(ByVal vstrFieldTitle As String, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjApplyingValues As Collection = Nothing) As Scripting.Dictionary
    
    
    Set GetFormatConditionInfoOfTextHittingFromFieldTitle = GetFormatConditionInfoOfTextHittingFromRange(GetRecordRangeFromFieldTitleByCurrentRegion(vstrFieldTitle, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex), vobjApplyingValues)
End Function

'''
'''
'''
Public Sub GetFormatConditionInfoOfTextHittingFromFieldTitleWithPreviousData(ByRef robjDic As Scripting.Dictionary, _
        ByVal vstrFieldTitle As String, _
        ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjApplyingValues As Collection = Nothing)


    GetFormatConditionInfoOfTextHittingFromRangeWithPreviousData robjDic, GetRecordRangeFromFieldTitleByCurrentRegion(vstrFieldTitle, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex), vobjApplyingValues
End Sub

'''
'''
'''
''' <Argument>vobjRange: Input</Argument>
''' <Argument>vobjApplyingValues: Input</Argument>
''' <Return>vobjApplyingValues</Return>
Public Function GetFormatConditionInfoOfTextHittingFromRange(ByVal vobjRange As Excel.Range, _
        Optional ByVal vobjApplyingValues As Collection = Nothing) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary

    GetFormatConditionInfoOfTextHittingFromRangeWithPreviousData objDic, vobjRange, vobjApplyingValues

    Set GetFormatConditionInfoOfTextHittingFromRange = objDic
End Function

'''
'''
'''
''' <Argument>robjDic: Output</Argument>
''' <Argument>vobjRange: Input</Argument>
''' <Argument>vobjApplyingValues: Input</Argument>
Public Sub GetFormatConditionInfoOfTextHittingFromRangeWithPreviousData(ByRef robjDic As Scripting.Dictionary, _
        ByVal vobjRange As Excel.Range, _
        Optional ByVal vobjApplyingValues As Collection = Nothing)
    
    
    Dim objFormatCondition As Excel.FormatCondition, objValuesCol As Collection
    Dim objPropertyNameToValidDic As Scripting.Dictionary, objInspectingProperties As Collection, blnContinue As Boolean
    Dim strKey As String
    
    
    Set objInspectingProperties = GetColFromLineDelimitedChar("Formula1,Formula2,Text,Operator,TextOperator,DateOperator")
    
    For Each objFormatCondition In vobjRange.FormatConditions
        
        Set objPropertyNameToValidDic = GetPropertyNameToValidDicAboutFormatCondition(objFormatCondition, objInspectingProperties)
        
        With objPropertyNameToValidDic
            
            If .Item("Formula1") Or .Item("Formula2") Or .Item("Text") Then
        
                Set objValuesCol = New Collection
        
                ' About Formula1, Formula2, Text of FormatCondition
                msubGetBasicFormatConditionExpressionPropertiesToValues objValuesCol, strKey, objFormatCondition, objPropertyNameToValidDic
                
                If Not robjDic.Exists(strKey) Then
                
                    ' About Type, Operator, TextOperator, DateOperator of FormatCondition
                    msubGetBasicFormatConditionTypeAndOperatorPropertiesToValues objValuesCol, objFormatCondition, objPropertyNameToValidDic
                    
                    ' About Interior
                    msubGetInteriorPropertiesToValues objValuesCol, objFormatCondition.Interior
                    
                    ' About Font
                    msubGetFontPropertiesToValues objValuesCol, objFormatCondition.Font
                    
                    With robjDic
                    
                        blnContinue = True
                        
                        'Debug.Print strKey
                        
                        ' filtering by columns values
                        If Not vobjApplyingValues Is Nothing Then
                        
                            If Not mfblnIsToBeAppliedCondition(objValuesCol, vobjApplyingValues) Then
                            
                                blnContinue = False
                            End If
                        End If
                    
                        If blnContinue Then
                        
                            .Add strKey, objValuesCol
                        End If
                    End With
                Else
                    Debug.Print "FormatCondition, duplicated key: " & strKey
                End If
            End If
        End With
    Next
End Sub


'''
'''
'''
Public Function ConvertFormatConditionInfoDicFromKeyValuesToStringValues(ByVal vobjDic As Scripting.Dictionary) As Scripting.Dictionary

    Dim varKey As Variant, strKey As String, objValues As Collection
    
    Dim objConvertedDic As Scripting.Dictionary, objConvertedValues As Collection
    
    Set objConvertedDic = New Scripting.Dictionary
    
    For Each varKey In vobjDic.Keys
    
        strKey = varKey
    
        Set objValues = vobjDic.Item(strKey)
        
        Set objConvertedValues = New Collection
    
        ' About Formula1, Formula2, Text of FormatCondition
        msubConvertBasicFormatConditionExpressionPropertiesFromKeyValuesToStringValues objValues, objConvertedValues
    
        ' About Type, Operator, TextOperator, DateOperator of FormatCondition
        msubConvertBasicFormatConditionTypeAndOperatorPropertiesFromKeyValuesToStringValues objValues, objConvertedValues
        
        ' About Interior
        msubConvertInteriorPropertiesFromKeyValuesToStringValues objValues, objConvertedValues
    
        ' About Font
        msubConvertFontPropertiesFromKeyValuesToStringValues objValues, objConvertedValues
    
    
        objConvertedDic.Add strKey, objConvertedValues
    Next
    
    Set ConvertFormatConditionInfoDicFromKeyValuesToStringValues = objConvertedDic
End Function

'''
''' convert Formula1, Formula2, Text of FormatCondition
'''
Private Sub msubConvertBasicFormatConditionExpressionPropertiesFromKeyValuesToStringValues(ByVal vobjValues As Collection, _
        ByRef robjConvertedValues As Collection)
    
    With robjConvertedValues
    
        .Add vobjValues.Item(mstrFcFormula1Key)
        
        .Add vobjValues.Item(mstrFcFormula2Key)
        
        .Add vobjValues.Item(mstrFcTextKey)
    End With
End Sub

'''
''' convert Type, Operator, TextOperator, DateOperator of FormatCondition
'''
Private Sub msubConvertBasicFormatConditionTypeAndOperatorPropertiesFromKeyValuesToStringValues(ByVal vobjValues As Collection, _
        ByRef robjConvertedValues As Collection)
    
    Dim enmOperator As Excel.XlFormatConditionOperator, enmTextOperator As Excel.XlContainsOperator, enmDateOperator As Excel.XlTimePeriods
    
    With robjConvertedValues

        .Add GetXlFormatConditionTypeTextFromEnm(vobjValues.Item(mstrFcTypeKey))
        
        On Error Resume Next
        
        enmOperator = vobjValues.Item(mstrFcOperatorKey)
        
        If Err.Number = 0 Then
        
            .Add GetXlFormatConditionOperatorTextFromEnm(enmOperator)
        Else
            .Add ""
            
            Err.Clear
        End If
        
        enmTextOperator = vobjValues.Item(mstrFcTextOperatorKey)
        
        If Err.Number = 0 Then
        
            .Add GetXlTextContainsOperatorTextFromEnm(enmTextOperator)
        Else
            .Add ""
            
            Err.Clear
        End If
        
        enmDateOperator = vobjValues.Item(mstrFcDateOperatorKey)
        
        If Err.Number = 0 Then
        
            .Add GetXlDateOperatorTextFromEnm(enmDateOperator)
        Else
            .Add ""
            
            Err.Clear
        End If
        
        On Error GoTo 0
        
    End With
End Sub

'''
''' convert Interior
'''
Private Sub msubConvertInteriorPropertiesFromKeyValuesToStringValues(ByVal vobjValues As Collection, ByRef robjConvertedValues As Collection)
    
    Dim intR As Long, intG As Long, intB As Long
    
    Dim objInteriorValues As Collection
    
    With robjConvertedValues
        
        Set objInteriorValues = vobjValues.Item(mstrFcInteriorKey)

        GetLongValueToRGB intR, intG, intB, objInteriorValues.Item(mstrGColorKey)

        .Add intR: .Add intG: .Add intB
    End With
End Sub

'''
''' convert Font
'''
Private Sub msubConvertFontPropertiesFromKeyValuesToStringValues(ByVal vobjValues As Collection, _
        ByRef robjConvertedValues As Collection)


End Sub

'''
'''
'''
Private Function mfblnIsToBeAppliedCondition(ByVal vobjFormatConditionValuesCol As Collection, _
        ByVal vobjApplyingValues As Collection) As Boolean
    
    
    Dim blnFoundValue As Boolean, strFormula1Value As String, strFormula2Value As String, strText As String
    Dim varCellValue As Variant, enmType As Excel.XlFormatConditionType
    Dim enmOperator As Excel.XlFormatConditionOperator, enmTextOperator As Excel.XlContainsOperator
    
    
    blnFoundValue = False
    
    enmType = vobjFormatConditionValuesCol.Item(mstrFcTypeKey)
    
    Select Case enmType
        
        Case Excel.XlFormatConditionType.xlCellValue
        
            On Error Resume Next
            
            enmOperator = 0
            
            enmOperator = vobjFormatConditionValuesCol.Item(mstrFcOperatorKey)
        
            On Error GoTo 0
        
            If enmOperator = xlEqual Then
        
                strFormula1Value = GetInterFormulaOfCellExpression(vobjFormatConditionValuesCol.Item(mstrFcFormula1Key))
        
                For Each varCellValue In vobjApplyingValues
                    
                    If StrComp(strFormula1Value, varCellValue) = 0 Then
                    
                        blnFoundValue = True
                    
                        Exit For
                    End If
                Next
        
            ElseIf enmOperator = xlBetween Then
            
                strFormula1Value = GetInterFormulaOfCellExpression(vobjFormatConditionValuesCol.Item(mstrFcFormula1Key))
                
                strFormula2Value = GetInterFormulaOfCellExpression(vobjFormatConditionValuesCol.Item(mstrFcFormula2Key))
            
                For Each varCellValue In vobjApplyingValues
                    
                    If strFormula1Value <= varCellValue And varCellValue <= strFormula2Value Then
                    
                        blnFoundValue = True
                    
                        Exit For
                    End If
                Next
            End If
    
        Case Excel.XlFormatConditionType.xlTextString

            enmTextOperator = 0
            
            enmTextOperator = vobjFormatConditionValuesCol.Item(mstrFcTextOperatorKey)
            
            strText = vobjFormatConditionValuesCol.Item(mstrFcTextKey)
            
            If enmTextOperator = xlContains Then
            
                For Each varCellValue In vobjApplyingValues
                    
                    If InStr(1, varCellValue, strText) > 0 Then
                    
                        blnFoundValue = True
                    
                        Exit For
                    End If
                Next
            End If
    End Select

    mfblnIsToBeAppliedCondition = blnFoundValue
End Function


'''
'''
'''
Private Sub msubGetInteriorPropertiesToValues(ByRef robjValuesCol As Collection, ByVal vobjInterior As Excel.Interior)
                    
    robjValuesCol.Add GetDecorationXlInteriorPropertiesCol(vobjInterior), key:=mstrFcInteriorKey
End Sub


Private Sub msubGetFontPropertiesToValues(ByRef robjValuesCol As Collection, ByVal vobjFont As Excel.Font)

    robjValuesCol.Add GetDecorationXlFontPropertiesCol(vobjFont), key:=mstrFcFontKey
End Sub


'''
'''
'''
''' <Argument>robjValuesCol: Output</Argument>
''' <Argument>rstrDicKey: Output - dictinary-key</Argument>
''' <Argument>vobjFormatCondition: Input</Argument>
''' <Argument>vobjPropertyNameToValidDicAboutFormatCondition: Input</Argument>
''' <Argument>vblnKeyValueMode: Input</Argument>
Private Sub msubGetBasicFormatConditionExpressionPropertiesToValues(ByRef robjValuesCol As Collection, _
        ByRef rstrDicKey As String, _
        ByVal vobjFormatCondition As Excel.FormatCondition, _
        ByVal vobjPropertyNameToValidDicAboutFormatCondition As Scripting.Dictionary)
    
    
    Dim strKey As String
    Dim strText As String, strFormula1 As String, strFormula2 As String

    strKey = ""
    
    With vobjPropertyNameToValidDicAboutFormatCondition

        If .Item("Text") Then
                
            strKey = vobjFormatCondition.Text
            
            strText = vobjFormatCondition.Text
        
        ElseIf .Item("Formula1") And .Item("Formula2") Then
        
            strKey = GetInterFormulaOfCellExpression(vobjFormatCondition.Formula1) & "; " & GetInterFormulaOfCellExpression(vobjFormatCondition.Formula2)
            
            strFormula1 = vobjFormatCondition.Formula1
            
            strFormula2 = vobjFormatCondition.Formula2
        
        ElseIf .Item("Formula1") Then
        
            strKey = GetInterFormulaOfCellExpression(vobjFormatCondition.Formula1)
            
            strFormula1 = vobjFormatCondition.Formula1
        End If
        
        With robjValuesCol
        
            .Add strFormula1, key:=mstrFcFormula1Key
            
            .Add strFormula2, key:=mstrFcFormula2Key
            
            .Add strText, key:=mstrFcTextKey
        End With
    End With

    rstrDicKey = strKey
End Sub


'''
'''
'''
Private Sub msubGetBasicFormatConditionTypeAndOperatorPropertiesToValues(ByRef robjValuesCol As Collection, _
        ByVal vobjFormatCondition As Excel.FormatCondition, _
        ByVal vobjPropertyNameToValidDicAboutFormatCondition As Scripting.Dictionary)


    With vobjPropertyNameToValidDicAboutFormatCondition

        If Not IsEmpty(vobjFormatCondition.Type) Then

            robjValuesCol.Add vobjFormatCondition.Type, key:=mstrFcTypeKey
        Else
            robjValuesCol.Add ""
        End If
        
        If .Item("Operator") Then
           
            robjValuesCol.Add vobjFormatCondition.Operator, key:=mstrFcOperatorKey
        Else
            robjValuesCol.Add ""
        End If
        
        If .Item("TextOperator") Then
            
            robjValuesCol.Add vobjFormatCondition.TextOperator, key:=mstrFcTextOperatorKey
        Else
            robjValuesCol.Add ""
        End If
        
        If .Item("DateOperator") Then
            
            robjValuesCol.Add vobjFormatCondition.DateOperator, key:=mstrFcDateOperatorKey
        Else
            robjValuesCol.Add ""
        End If
    End With
End Sub


'''
'''
'''
Private Function GetDecorationXlInteriorPropertiesCol(ByVal vobjInterior As Excel.Interior) As Collection
    
    Dim objCol As Collection, objColorStops As Excel.ColorStops, objColorStop As Excel.ColorStop
    Dim objLinearGradient As Excel.LinearGradient, objRectangularGradient As Excel.RectangularGradient
    Dim i As Long


    Set objCol = New Collection

    With vobjInterior
    
        objCol.Add .Pattern, key:=mstrGInteriorPatternKey
    
        If IsNull(.Pattern) Then
        
            objCol.Add .ThemeColor, key:=mstrGThemeColorKey
            
            objCol.Add .TintAndShade, key:=mstrGTintAndShadeKey
            
            objCol.Add .Color, key:=mstrGColorKey
        Else
            Select Case .Pattern
            
                Case xlSolid
                
                    objCol.Add .PatternColorIndex, key:=mstrGPatternColorIndex
                    
                    objCol.Add .ThemeColor, key:=mstrGThemeColorKey
                    
                    objCol.Add .TintAndShade, key:=mstrGTintAndShadeKey
                    
                    objCol.Add .Color, key:=mstrGColorKey
                    
                    objCol.Add .PatternTintAndShade, key:=mstrGPatternTintAndShadeKey
            
                Case xlPatternLinearGradient
                
                    Set objLinearGradient = .Gradient
                
                    objCol.Add objLinearGradient.Degree, key:=mstrGGradientDegreeKey
                
                    Set objColorStops = objLinearGradient.ColorStops
            
                    objCol.Add objColorStops.Count, key:=mstrGColorStopsCountKey
            
                    i = 1
                    
                    For Each objColorStop In objColorStops
                    
                        objCol.Add GetDecorationXlColorStopPropertiesCol(objColorStop), key:=mstrGColorStopPrefix & CStr(i)
                    
                        i = i + 1
                    Next
            
                Case Excel.XlPattern.xlPatternRectangularGradient
                
                    Set objRectangularGradient = .Gradient
                    
                    objCol.Add objRectangularGradient.Degree, key:=mstrGGradientDegreeKey
                    
                    Set objColorStops = objRectangularGradient.ColorStops
                    
                    objCol.Add objColorStops.Count, key:=mstrGColorStopsCountKey
                    
                    i = 1
                    
                    For Each objColorStop In objColorStops
                    
                        objCol.Add GetDecorationXlColorStopPropertiesCol(objColorStop), key:=mstrGColorStopPrefix & CStr(i)
                    
                        i = i + 1
                    Next
            End Select
        End If
    End With

    Set GetDecorationXlInteriorPropertiesCol = objCol
End Function

'''
'''
'''
Public Sub DecorateFormatConditionInteriorFromValuesToInterior(ByRef robjInterior As Excel.Interior, ByVal vobjValues As Collection)
    
    Dim objCol As Collection, objColorStops As Excel.ColorStops, objColorStop As Excel.ColorStop
    
    Dim objLinearGradient As Excel.LinearGradient, objRectangularGradient As Excel.RectangularGradient
    Dim varPattern As Variant, enmPattern As Excel.XlPattern, enmThemeColor As Excel.XlThemeColor
    Dim i As Long, intColorStopsCount As Long
    Dim objColorStopValues As Collection

    enmPattern = 0
    
    varPattern = vobjValues.Item(mstrGInteriorPatternKey)

    With robjInterior
    
        If IsNull(varPattern) Then
        
            enmThemeColor = vobjValues.Item(mstrGThemeColorKey)
        
            If IsThemeColorSpecified(enmThemeColor) Then
            
                .ThemeColor = enmThemeColor
                
                .TintAndShade = vobjValues.Item(mstrGTintAndShadeKey)
            Else
                .Color = vobjValues.Item(mstrGColorKey)
            End If
        Else
            enmPattern = varPattern
        
            Select Case enmPattern
            
                Case Excel.XlPattern.xlPatternSolid
                
                    enmThemeColor = vobjValues.Item(mstrGThemeColorKey)
            
                    .PatternColorIndex = vobjValues.Item(mstrGPatternColorIndex)
            
                    If IsThemeColorSpecified(enmThemeColor) Then
                    
                        .ThemeColor = enmThemeColor
                        
                        .TintAndShade = vobjValues.Item(mstrGTintAndShadeKey)
                    Else
                        .Color = vobjValues.Item(mstrGColorKey)
                    End If
            
                    .PatternTintAndShade = vobjValues.Item(mstrGPatternTintAndShadeKey)
            
                Case Excel.XlPattern.xlPatternLinearGradient
                
                    Set objLinearGradient = .Gradient
                    
                    With objLinearGradient
                    
                        .ColorStops.Clear
                        
                        .Degree = vobjValues.Item(mstrGPatternTintAndShadeKey)
                        
                        intColorStopsCount = vobjValues.Item(mstrGColorStopsCountKey)
                        
                        With .ColorStops
                        
                            For i = 1 To intColorStopsCount
                                
                                Set objColorStopValues = vobjValues.Item(mstrGColorStopPrefix & CStr(i))
                            
                                With .Add(objColorStopValues.Item(mstrGColorStopPosition))
                                
                                    enmThemeColor = objColorStopValues.Item(mstrGThemeColorKey)
                                
                                    If IsThemeColorSpecified(enmThemeColor) Then
                                    
                                        .ThemeColor = enmThemeColor
                                        
                                        .TintAndShade = objColorStopValues.Item(mstrGTintAndShadeKey)
                                    Else
                                        .Color = objColorStopValues.Item(mstrGColorKey)
                                    End If
                                End With
                            Next
                        End With
                    End With
                    
                Case Excel.XlPattern.xlPatternRectangularGradient
            
                    Set objRectangularGradient = .Gradient
                    
                    With objRectangularGradient
                    
                        .ColorStops.Clear
                        
                        .Degree = vobjValues.Item(mstrGPatternTintAndShadeKey)
                        
                        intColorStopsCount = vobjValues.Item(mstrGColorStopsCountKey)
                        
                        With .ColorStops
                        
                            For i = 1 To intColorStopsCount
                                
                                Set objColorStopValues = vobjValues.Item(mstrGColorStopPrefix & CStr(i))
                            
                                With .Add(objColorStopValues.Item(mstrGColorStopPosition))
                                
                                    enmThemeColor = objColorStopValues.Item(mstrGThemeColorKey)
                                
                                    If IsThemeColorSpecified(enmThemeColor) Then
                                    
                                        .ThemeColor = enmThemeColor
                                        
                                        .TintAndShade = objColorStopValues.Item(mstrGTintAndShadeKey)
                                    Else
                                        .Color = objColorStopValues.Item(mstrGColorKey)
                                    End If
                                End With
                            Next
                        End With
                    End With
            End Select
        End If
    End With
End Sub


'''
'''
'''
Private Function GetDecorationXlColorStopPropertiesCol(ByVal vobjColorStop As Excel.ColorStop) As Collection
    
    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With vobjColorStop
    
        objCol.Add .Position, key:=mstrGColorStopPosition
        
        objCol.Add .ThemeColor, key:=mstrGThemeColorKey
        
        objCol.Add .TintAndShade, key:=mstrGTintAndShadeKey
        
        objCol.Add .Color, key:=mstrGColorKey
    End With

    Set GetDecorationXlColorStopPropertiesCol = objCol
End Function


'''
'''
'''
Private Function GetDecorationXlFontPropertiesCol(ByVal vobjFont As Excel.Font) As Collection
    
    Dim objCol As Collection, enmThemeColor As Excel.XlThemeColor


    Set objCol = New Collection

    With vobjFont
        
        objCol.Add .Name, key:=mstrGFontNameKey
        
        objCol.Add .Size, key:=mstrGFontSizeKey
    
        On Error Resume Next
        
        enmThemeColor = .ThemeColor
    
        If Err.Number <> 0 Then
        
            objCol.Add enmThemeColor, key:=mstrGThemeColorKey
            
            objCol.Add .TintAndShade, key:=mstrGTintAndShadeKey

            Err.Clear
        Else
            objCol.Add .Color, key:=mstrGColorKey
        End If
    
        On Error GoTo 0

    End With

    Set GetDecorationXlFontPropertiesCol = objCol
End Function


'''
'''
'''
Private Function GetPropertyNameToValidDicAboutFormatCondition(ByVal vobjFormatCondition As Excel.FormatCondition, _
        ByVal vobjInspectingProperties As Collection) As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary
    Dim varPropertyName As Variant, varValue As Variant, blnIsValid As Boolean, varArgs() As Variant


    Set objDic = New Scripting.Dictionary

    For Each varPropertyName In vobjInspectingProperties
        
        On Error Resume Next
        
        blnIsValid = True
        
        varValue = VBA.CallByName(vobjFormatCondition, varPropertyName, VBA.VbCallType.VbGet)

        If Err.Number <> 0 Then
        
            blnIsValid = False
            
            Err.Clear
        Else
            If IsEmpty(varValue) Then
            
                blnIsValid = False
            End If
        End If
        
        objDic.Add varPropertyName, blnIsValid
        
        On Error GoTo 0
    Next

    Set GetPropertyNameToValidDicAboutFormatCondition = objDic
End Function


'''
'''
'''
''' <Argument>rblnIsFormula1Valid: Output</Argument>
''' <Argument>rblnIsFormula2Valid: Output</Argument>
''' <Argument>rblnIsTextValid: Output</Argument>
''' <Argument>vobjFormatCondition: Input</Argument>
Private Sub DetectValidPropertiesFromFormatCondition(ByRef rblnIsFormula1Valid As Boolean, _
        ByRef rblnIsFormula2Valid As Boolean, _
        ByRef rblnIsTextValid As Boolean, _
        ByVal vobjFormatCondition As Excel.FormatCondition)
    
    
    Dim blnEmpty As Boolean, strValue As String
    Dim varPropertyName As Variant, objInspectingProperties As Collection, blnIsValid As Boolean, varArgs() As Variant


    Set objInspectingProperties = GetColFromLineDelimitedChar("Formula1,Formula2,Text")

    For Each varPropertyName In objInspectingProperties

        On Error Resume Next
        
        blnIsValid = True
        
        strValue = VBA.CallByName(vobjFormatCondition, varPropertyName, VBA.VbCallType.VbGet)

        If Err.Number <> 0 Then
        
            blnIsValid = False
            
            Err.Clear
        Else
            If strValue = "" Then
            
                blnIsValid = False
            End If
        End If
        
        Select Case CStr(varPropertyName)
        
            Case "Formula1"
            
                rblnIsFormula1Valid = blnIsValid
                
            Case "Formula2"
            
                rblnIsFormula2Valid = blnIsValid
                
            Case "Text"
            
                rblnIsTextValid = blnIsValid
        End Select
        
        On Error GoTo 0
    Next
End Sub


'''
'''
'''
Public Function GetXlFormatConditionOperatorTextFromEnm(ByVal venmValue As Excel.XlFormatConditionOperator) As String
    
    Dim strText As String
    
    strText = ""
    
    Select Case venmValue
    
        Case XlFormatConditionOperator.xlBetween
        
            strText = "xlBetween"
            
        Case XlFormatConditionOperator.xlEqual
        
            strText = "xlEqual"
            
        Case XlFormatConditionOperator.xlGreater
        
            strText = "xlGreater"
            
        Case XlFormatConditionOperator.xlGreaterEqual
        
            strText = "xlGreaterEqual"
            
        Case XlFormatConditionOperator.xlLess
        
            strText = "xlLess"
            
        Case XlFormatConditionOperator.xlLessEqual
        
            strText = "xlLessEqual"
            
        Case XlFormatConditionOperator.xlNotBetween
        
            strText = "xlNotBetween"
            
        Case XlFormatConditionOperator.xlNotEqual
        
            strText = "xlNotEqual"
    End Select

    GetXlFormatConditionOperatorTextFromEnm = strText
End Function

'''
'''
'''
Public Function GetXlFormatConditionTypeTextFromEnm(ByVal venmValue As Excel.XlFormatConditionType) As String
    
    Dim strText As String
    
    strText = ""
    
    Select Case venmValue
    
        Case Excel.XlFormatConditionType.xlAboveAverageCondition
        
            strText = "xlAboveAverageCondition"
            
        Case Excel.XlFormatConditionType.xlBlanksCondition
        
            strText = "xlBlanksCondition"
            
        Case Excel.XlFormatConditionType.xlCellValue
        
            strText = "xlCellValue"
            
        Case Excel.XlFormatConditionType.xlColorScale
        
            strText = "xlColorScale"
            
        Case Excel.XlFormatConditionType.xlDatabar
        
            strText = "xlDatabar"
            
        Case Excel.XlFormatConditionType.xlErrorsCondition
        
            strText = "xlErrorsCondition"
            
        Case Excel.XlFormatConditionType.xlExpression
        
            strText = "xlExpression"
            
        Case Excel.XlFormatConditionType.xlIconSets
        
            strText = "xlIconSets"
            
        Case Excel.XlFormatConditionType.xlNoBlanksCondition
        
            strText = "xlNoBlanksCondition"
            
        Case Excel.XlFormatConditionType.xlNoErrorsCondition
        
            strText = "xlNoErrorsCondition"
            
        Case Excel.XlFormatConditionType.xlTextString
        
            strText = "xlTextString"
            
        Case Excel.XlFormatConditionType.xlTimePeriod
        
            strText = "xlTimePeriod"
            
        Case Excel.XlFormatConditionType.xlTop10
        
            strText = "xlTop10"
            
        Case Excel.XlFormatConditionType.xlUniqueValues
        
            strText = "xlUniqueValues"
    End Select

    GetXlFormatConditionTypeTextFromEnm = strText
End Function

'''
'''
'''
Public Function GetXlTextContainsOperatorTextFromEnm(ByVal venmValue As Excel.XlContainsOperator) As String
    
    Dim strText As String
    
    strText = ""
    
    Select Case venmValue
    
        Case Excel.XlContainsOperator.xlBeginsWith
        
            strText = "xlBeginsWith"
            
        Case Excel.XlContainsOperator.xlContains
        
            strText = "xlContains"
            
        Case Excel.XlContainsOperator.xlDoesNotContain
        
            strText = "xlDoesNotContain"
            
        Case Excel.XlContainsOperator.xlEndsWith
        
            strText = "xlEndsWith"
    End Select

    GetXlTextContainsOperatorTextFromEnm = strText
End Function

'''
'''
'''
Public Function GetXlDateOperatorTextFromEnm(ByVal venmValue As Excel.XlTimePeriods) As String
    
    Dim strText As String
    
    strText = ""
    
    Select Case venmValue
    
        Case Excel.XlTimePeriods.xlLast7Days
        
            strText = "xlLast7Days"
            
        Case Excel.XlTimePeriods.xlLastMonth
        
            strText = "xlLastMonth"
            
        Case Excel.XlTimePeriods.xlLastWeek
        
            strText = "xlLastWeek"
            
        Case Excel.XlTimePeriods.xlNextMonth
        
            strText = "xlNextMonth"
            
        Case Excel.XlTimePeriods.xlNextWeek
        
            strText = "xlNextWeek"
        Case Excel.XlTimePeriods.xlThisMonth
        
            strText = "xlThisMonth"
            
        Case Excel.XlTimePeriods.xlThisWeek
        
            strText = "xlThisWeek"
            
        Case Excel.XlTimePeriods.xlToday
        
            strText = "xlToday"
            
        Case Excel.XlTimePeriods.xlTomorrow
        
            strText = "xlTomorrow"
            
        Case Excel.XlTimePeriods.xlYesterday
        
            strText = "xlYesterday"
    End Select
    
    GetXlDateOperatorTextFromEnm = strText
End Function

'''
'''
'''
Public Function GetXlThemeColorTextFromEnm(ByVal venmValue As Excel.XlThemeColor) As String
    
    Dim strText As String
    
    strText = ""

    Select Case venmValue
    
        Case Excel.XlThemeColor.xlThemeColorAccent1
        
            strText = "xlThemeColorAccent1"
        
        Case Excel.XlThemeColor.xlThemeColorAccent2
        
            strText = "xlThemeColorAccent2"
            
        Case Excel.XlThemeColor.xlThemeColorAccent3
        
            strText = "xlThemeColorAccent3"
            
        Case Excel.XlThemeColor.xlThemeColorAccent4
        
            strText = "xlThemeColorAccent4"
            
        Case Excel.XlThemeColor.xlThemeColorAccent5
        
            strText = "xlThemeColorAccent5"
            
        Case Excel.XlThemeColor.xlThemeColorAccent6
        
            strText = "xlThemeColorAccent6"
            
        Case Excel.XlThemeColor.xlThemeColorDark1
        
            strText = "xlThemeColorDark1"
            
        Case Excel.XlThemeColor.xlThemeColorDark2
        
            strText = "xlThemeColorDark2"
        
        Case Excel.XlThemeColor.xlThemeColorFollowedHyperlink
        
            strText = "xlThemeColorFollowedHyperlink"
        
        Case Excel.XlThemeColor.xlThemeColorHyperlink
        
            strText = "xlThemeColorHyperlink"
            
        Case Excel.XlThemeColor.xlThemeColorLight1
        
            strText = "xlThemeColorLight1"
            
        Case Excel.XlThemeColor.xlThemeColorLight2
        
            strText = "xlThemeColorLight2"
            
        Case Else
            If venmValue = 0 Then
            
                strText = "0"
            Else
                Debug.Print "XlThemeColor value is " & CStr(venmValue)
            End If
    End Select
    
    GetXlThemeColorTextFromEnm = strText
End Function

'''
'''
'''
Public Function IsThemeColorSpecified(ByVal venmValue As Excel.XlThemeColor) As Boolean
    
    Dim blnIsSpecified As Boolean
    
    blnIsSpecified = False
    
    Select Case venmValue
    
        Case XlThemeColor.xlThemeColorAccent1, _
                XlThemeColor.xlThemeColorAccent2, _
                XlThemeColor.xlThemeColorAccent3, _
                XlThemeColor.xlThemeColorAccent4, _
                XlThemeColor.xlThemeColorAccent5, _
                XlThemeColor.xlThemeColorAccent6, _
                XlThemeColor.xlThemeColorDark1, _
                XlThemeColor.xlThemeColorDark2, _
                XlThemeColor.xlThemeColorFollowedHyperlink, _
                XlThemeColor.xlThemeColorHyperlink, _
                XlThemeColor.xlThemeColorLight1, _
                XlThemeColor.xlThemeColorLight2
    
    
            blnIsSpecified = True
    End Select

    IsThemeColorSpecified = blnIsSpecified
End Function


'**---------------------------------------------
'** About column widths
'**---------------------------------------------
'''
''' get ColumnWidth from existed current worksheet
'''
Public Sub GetColumnWidth()
    
    Dim objCurrentSheet As Excel.Worksheet, objTitleCell As Excel.Range
    Dim strRange As String, strMessage As String
    
    With ActiveCell
    
        Set objCurrentSheet = .Parent
        
        Set objTitleCell = objCurrentSheet.Cells(1, .Column)
        
        
        strRange = ConvertXlColumnIndexToLetter(.Column)
        
        strRange = strRange & ":" & strRange
        
        strMessage = "Field Title = """ & objTitleCell.Value & """" & vbCrLf & _
            "ColumnIndex = " & .Column & vbCrLf & _
            "ColumnWidth = " & ActiveCell.ColumnWidth & vbCrLf & _
            "Address = """ & strRange & """" & vbCrLf & vbCrLf & _
            "Code: " & vbCrLf & _
            ".Columns(""" & strRange & """).ColumnWidth = " & .ColumnWidth & vbTab & vbTab & "' " & objTitleCell.Value & vbCrLf
        
        'MsgBox strMessage
        Debug.Print strMessage
    End With
End Sub

'''
''' get CellValueText and column-width from the selected cells
'''
Public Sub GetSelectedCellsColumnWidthTexts()
    
    Dim objSelectedRange As Excel.Range, objCell As Excel.Range
    
    
    Set objSelectedRange = Selection
    
    With objSelectedRange
    
        For Each objCell In .Cells
            
            ' for Scripting.Dictionary.Add method codes
            
            Debug.Print ".Add " & """" & objCell.Value & """, " & CDbl(objCell.ColumnWidth)
        Next
    End With
End Sub

'''
'''
'''
Public Function GetColumnWidthTextDelimitedByCommaInSelectedCells() As String

    Dim objSelectedRange As Excel.Range, objCell As Excel.Range, strText As String, i As Long
    
    strText = ""
    
    Set objSelectedRange = Selection
    
    With objSelectedRange
    
        i = 1
        
        For Each objCell In .Cells
            
            ' for Scripting.Dictionary.Add method codes
            
            strText = strText & CStr(objCell.Value) & "," & Format(CDbl(objCell.ColumnWidth), "0.0")

            If i < .Cells.Count Then
            
                strText = strText & ","
            End If

            i = i + 1
        Next
    End With

    GetColumnWidthTextDelimitedByCommaInSelectedCells = strText
End Function


'**---------------------------------------------
'** Getting tools of Excel.XlBordersIndex value collection
'**---------------------------------------------
'''
'''
'''
Public Function GetXlBorderIndexEnmFromText(ByVal vstrXlBorderIndexType As String) As Excel.XlBordersIndex
    
    Dim enmValue As Excel.XlBordersIndex
    
    enmValue = 0
    
    Select Case Trim(vstrXlBorderIndexType)
        
        Case "xlEdgeLeft"
            enmValue = Excel.XlBordersIndex.xlEdgeLeft
    
        Case "xlEdgeTop"
            enmValue = Excel.XlBordersIndex.xlEdgeTop
            
        Case "xlEdgeBottom"
            enmValue = Excel.XlBordersIndex.xlEdgeBottom
        
        Case "xlEdgeRight"
            enmValue = Excel.XlBordersIndex.xlEdgeRight
        
        Case "xlInsideVertical"
            enmValue = Excel.XlBordersIndex.xlInsideVertical
         
        Case "xlInsideHorizontal"
            enmValue = Excel.XlBordersIndex.xlInsideHorizontal
    
        Case "xlDiagonalDown"
            enmValue = Excel.XlBordersIndex.xlDiagonalDown
            
        Case "xlDiagonalUp"
            enmValue = Excel.XlBordersIndex.xlDiagonalUp
    End Select

    GetXlBorderIndexEnmFromText = enmValue
End Function

'''
'''
'''
Public Function GetXlBorderIndexTxtFromEnm(ByVal venmXlBorderIndex As Excel.XlBordersIndex) As String

    Dim strText As String
    
    strText = ""
    
    Select Case venmXlBorderIndex
    
        Case Excel.XlBordersIndex.xlEdgeLeft
        
            strText = "xlEdgeLeft"
            
        Case Excel.XlBordersIndex.xlEdgeTop
        
            strText = "xlEdgeTop"
        
        Case Excel.XlBordersIndex.xlEdgeBottom
        
            strText = "xlEdgeBottom"
        
        Case Excel.XlBordersIndex.xlEdgeRight
        
            strText = "xlEdgeRight"
            
        Case Excel.XlBordersIndex.xlInsideVertical
        
            strText = "xlInsideVertical"
            
        Case Excel.XlBordersIndex.xlInsideHorizontal
        
            strText = "xlInsideHorizontal"
            
        Case Excel.XlBordersIndex.xlDiagonalDown
        
            strText = "xlDiagonalDown"
            
        Case Excel.XlBordersIndex.xlDiagonalUp
    
            strText = "xlDiagonalUp"
    End Select
    
    GetXlBorderIndexTxtFromEnm = strText
End Function

'''
'''
'''
Public Function GetXlBorderIndexColFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Collection
    
    Dim objCol As Collection, i As Long, strArray() As String

    Set objCol = New Collection
    
    If vstrLineDelimited <> "" Then
    
        strArray = Split(vstrLineDelimited, vstrDelimiter)
        
        For i = LBound(strArray) To UBound(strArray)
            
            objCol.Add GetXlBorderIndexEnmFromText(strArray(i))
        Next
    End If
    
    Set GetXlBorderIndexColFromLineDelimitedChar = objCol
End Function

'''
'''
'''
Public Function GetXlBorderIndexColForDataTableRecordArea(Optional ByVal vblnNoIncludeFieldTitlesRow As Boolean = False, _
        Optional ByVal vblnNoIncludeFieldTitlesColumn As Boolean = True, _
        Optional ByVal vblnIsInsideHorizontalEnabled As Boolean = True, _
        Optional ByVal vblnIsInsideVerticalEnabled As Boolean = True) As Collection

    Dim objAreaCol As Collection

    If vblnNoIncludeFieldTitlesRow Then
    
        If vblnNoIncludeFieldTitlesColumn Then
    
            Set objAreaCol = GetXlBorderIndexColFromOptions(IsInsideVerticalEnabled:=vblnIsInsideVerticalEnabled, IsInsideHorizontalEnabled:=vblnIsInsideHorizontalEnabled)
        Else
            Set objAreaCol = GetXlBorderIndexColFromOptions(IsEdgeLeftEnabled:=False, IsInsideVerticalEnabled:=vblnIsInsideVerticalEnabled, IsInsideHorizontalEnabled:=vblnIsInsideHorizontalEnabled)
        End If
    Else
        If vblnNoIncludeFieldTitlesColumn Then
        
            Set objAreaCol = GetXlBorderIndexColFromOptions(IsEdgeTopEnabled:=False, IsInsideVerticalEnabled:=vblnIsInsideVerticalEnabled, IsInsideHorizontalEnabled:=vblnIsInsideHorizontalEnabled)
        Else
            Set objAreaCol = GetXlBorderIndexColFromOptions(IsEdgeLeftEnabled:=False, IsEdgeTopEnabled:=False, IsInsideVerticalEnabled:=vblnIsInsideVerticalEnabled, IsInsideHorizontalEnabled:=vblnIsInsideHorizontalEnabled)
        End If
    End If
    
    Set GetXlBorderIndexColForDataTableRecordArea = objAreaCol
End Function

'''
'''
'''
Public Function GetXlBorderIndexColForTransposedDataTableRecordArea(Optional ByVal vblnNoIncludeFieldTitlesRow As Boolean = False, Optional ByVal vblnIsInsideVerticalEnabled As Boolean = True) As Collection

    If vblnNoIncludeFieldTitlesRow Then
    
        Set GetXlBorderIndexColForTransposedDataTableRecordArea = GetXlBorderIndexColFromOptions(IsInsideVerticalEnabled:=vblnIsInsideVerticalEnabled)
    Else
        Set GetXlBorderIndexColForTransposedDataTableRecordArea = GetXlBorderIndexColFromOptions(IsEdgeLeftEnabled:=False, IsInsideVerticalEnabled:=vblnIsInsideVerticalEnabled)
    End If
End Function

'''
'''
'''
Public Function GetXlBorderIndexColFromOptions(Optional ByVal IsEdgeLeftEnabled As Boolean = True, _
        Optional ByVal IsEdgeTopEnabled As Boolean = True, _
        Optional ByVal IsEdgeRightEnabled As Boolean = True, _
        Optional ByVal IsEdgeBottomEnabled As Boolean = True, _
        Optional ByVal IsInsideVerticalEnabled As Boolean = True, _
        Optional ByVal IsInsideHorizontalEnabled As Boolean = True, _
        Optional ByVal IsDiagonalUpEnabled As Boolean = False, _
        Optional ByVal IsDiagonalDownEnabled As Boolean = False) As Collection
    
    
    Dim objCol As Collection, i As Long
    
    Set objCol = New Collection
    
    With objCol
        If IsEdgeLeftEnabled Then .Add Excel.XlBordersIndex.xlEdgeLeft
        
        If IsEdgeTopEnabled Then .Add Excel.XlBordersIndex.xlEdgeTop
        
        If IsEdgeRightEnabled Then .Add Excel.XlBordersIndex.xlEdgeRight
        
        If IsEdgeBottomEnabled Then .Add Excel.XlBordersIndex.xlEdgeBottom
        
        If IsInsideVerticalEnabled Then .Add Excel.XlBordersIndex.xlInsideVertical
        
        If IsInsideHorizontalEnabled Then .Add Excel.XlBordersIndex.xlInsideHorizontal
        
        If IsDiagonalUpEnabled Then .Add Excel.XlBordersIndex.xlDiagonalUp
        
        If IsDiagonalDownEnabled Then .Add Excel.XlBordersIndex.xlDiagonalDown
    End With

    Set GetXlBorderIndexColFromOptions = objCol
End Function

'**---------------------------------------------
'** Using ActiveCell
'**---------------------------------------------
'''
'''
'''
Public Function GetFieldTitleFromActiveCell() As String
    
    Dim objApplication As Excel.Application, objSheet As Excel.Worksheet, strFieldTitle As String
    
    With GetOfficeApplicationObject("Excel").Application.ActiveCell  ' at the current Excel.EXE process

        Debug.Print .Address

        GetFieldTitleFromActiveCell = .Worksheet.Range(ConvertXlColumnIndexToLetter(.Column) & CStr(.CurrentRegion.Row)).Value
    End With
End Function


'**---------------------------------------------
'** Using ActiveCell about row heights
'**---------------------------------------------
'''
''' get RowHeight from existed current worksheet
'''
Public Sub GetRowHeightFromActiveCell()
    
    Dim objCurrentSheet As Excel.Worksheet, objLeftCell As Excel.Range
    Dim strRange As String, strMessage As String
    
    With ActiveCell
    
        Set objCurrentSheet = .Parent
        
        Set objLeftCell = objCurrentSheet.Cells(.Row, 1)
        
        strRange = CStr(.Row)
        
        strRange = strRange & ":" & strRange
        
        strMessage = "Record Title = """ & objLeftCell.Value & """" & vbCrLf & _
            "RowIndex = " & .Row & vbCrLf & _
            "RowHeight = " & ActiveCell.RowHeight & vbCrLf & _
            "Address = """ & strRange & """" & vbCrLf & vbCrLf & _
            "Code: " & vbCrLf & _
            ".Rows(""" & strRange & """).RowHeight = " & .RowHeight & vbTab & vbTab & "' " & objLeftCell.Value & vbCrLf
        
        'MsgBox strMessage
        Debug.Print strMessage
    End With
End Sub

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' check string format of Range.Address property
'''
Private Sub msubSanityTestOfRangeOfUnioned()

    Dim objRange As Excel.Range, objSheet As Excel.Worksheet
    
    Set objSheet = GetOfficeApplicationObject("Excel").ActiveSheet  ' This can be executed from either Word VBE or PowerPoint VBE
    
    Set objRange = objSheet.Range("A1:B2")
    
    Debug.Print objRange.Address
    
    Set objRange = Union(objRange, objSheet.Range("C3:D4"))
    
    Debug.Print objRange.Address
End Sub

'''
'''
'''
Private Sub msubSanityTestOfInteriorActiveCell()
    
    Dim objRange As Excel.Range
    
    Set objRange = ActiveCell
    
    With objRange.Interior
        
        Debug.Print "Interior.Color:= " & GetTextFromRGB(.Color)
        
        Debug.Print "Interior.ThemeColor:= " & GetXlThemeColorTextFromEnm(.ThemeColor)
        
        Debug.Print "Interior.TintAndShade:= " & .TintAndShade
    End With

End Sub


'**---------------------------------------------
'** About getting FormatCondition information
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestOfGettingFormatConditionsFromActiveCell()
    
    Dim objSheet As Excel.Worksheet, strBookPath As String
    Dim strFieldTitle As String, objFormatConditionInfoDic As Scripting.Dictionary
    
    
    strFieldTitle = GetFieldTitleFromActiveCell()
    
    Set objFormatConditionInfoDic = GetFormatConditionInfoOfTextHittingFromFieldTitleByActiveCell()
    
    strBookPath = GetCurrentBookOutputDir() & "\FormatConditionsGettingResults.xlsx"
    
    ' the ActiveCell is changed by the below processing
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "Title_" & strFieldTitle)
    
    OutputGettingFormatConditionsToSheet objSheet, objFormatConditionInfoDic
End Sub

