VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "FormatFormulasCondition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Excel.FormatCondition relative formulas partial properties wrapper
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 22/May/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mstrFormula1 As String

Private mstrFormula2 As String

Private mstrText As String

Private menmFormatConditionType As Excel.XlFormatConditionType

Private menmFormatConditionOperator As Excel.XlFormatConditionOperator


Private mobjInteriorValues As Collection


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()
    
    mstrFormula1 = ""
    
    mstrFormula2 = ""
    
    mstrText = ""
    
    
    menmFormatConditionType = xlCellValue
    
    menmFormatConditionOperator = xlEqual
    
    Set mobjInteriorValues = Nothing
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** expression conditions of Excel.FormatCondition
'**---------------------------------------------
Public Property Get Formula1() As String

    Formula1 = mstrFormula1
End Property
Public Property Let Formula1(ByVal vstrFormula1 As String)

    mstrFormula1 = vstrFormula1
End Property


Public Property Get Formula2() As String

    Formula2 = mstrFormula2
End Property
Public Property Let Formula2(ByVal vstrFormula2 As String)

    mstrFormula2 = vstrFormula2
End Property


Public Property Get Text() As String

    Text = mstrText
End Property
Public Property Let Text(ByVal vstrText As String)

    mstrText = vstrText
End Property


Public Property Get FormatConditionType() As Excel.XlFormatConditionType

    FormatConditionType = menmFormatConditionType
End Property
Public Property Let FormatConditionType(ByVal venmFormatConditionType As Excel.XlFormatConditionType)

    menmFormatConditionType = venmFormatConditionType
End Property


Public Property Get FormatConditionOperator() As Excel.XlFormatConditionOperator

    FormatConditionOperator = menmFormatConditionOperator
End Property
Public Property Let FormatConditionOperator(ByVal venmFormatConditionOperator As Excel.XlFormatConditionOperator)

    menmFormatConditionOperator = venmFormatConditionOperator
End Property

'**---------------------------------------------
'** Appearances common properites
'**---------------------------------------------
Public Property Get InteriorValues() As Collection

    Set InteriorValues = mobjInteriorValues
End Property
Public Property Set InteriorValues(ByVal vobjInteriorValues As Collection)

    Set mobjInteriorValues = vobjInteriorValues
End Property
