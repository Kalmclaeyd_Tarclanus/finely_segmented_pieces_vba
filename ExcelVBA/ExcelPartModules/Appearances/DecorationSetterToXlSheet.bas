Attribute VB_Name = "DecorationSetterToXlSheet"
'
'   set individual cell formats with specified parameters
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' apearance field-title pattern enumeration, the 'Manual' means that User specify it, but not auto
'''
Public Enum ManualDecoratingFieldTitlesRangePattern

    WhiteFontGrayBgArial = 31    ' White Arial font and gray background
    
    WhiteFontGrayBgMeiryo = 32   ' White Meiryo font and gray background
    
    DarkBrownFontLightSilverBgMeiryo = 33 ' Dark Brown Meiryo font and light-silver graduation background
    
    SepiaFontLightToDarkBrownGraduationBgMeiryo = 34  ' Sepia Meiryo font and dark-brown graduation background
    
    WhiteFontDarkGreenGraduationBgMeiryo = 2   ' White Meiryo font and dark green graduation background
    
    DefaultCellFormatAndMeiryo = 35
    
    WhiteFontDarkOrangeGraduationBgMeiryo = 36  ' White Meiryo font and dark orange graduation background
    
    
    DarkBrownFontPaleGradientGreenBgMeiryo = 37
    
    DarkBrownFontMorePaleGradientGreenBgMeiryo = 38
    
End Enum

'**---------------------------------------------
'** Cell individual background enumeration
'**---------------------------------------------
Public Enum EmphasizingRecordsInterior

    LightYellowGradiant = 21
    
    LightRedGradiant = 22
End Enum

'''
''' grid borders appearance
'''
Public Enum RecordCellsBorders

    RecordCellsBordersNone = 0
    
    RecordCellsGrayAll = 1
    
    RecordCellsBlacksAndGrayInsideHorizontal = 2    ' for normal (m, n) output
    
    RecordCellsBlacksAndGrayInsideVertical = 3    ' for transpose (n, m) output
End Enum


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToDecorationSetterToXlSheet()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "DecorationGetterFromXlSheet,UTfDecorationSetterToXlSheet,DataTableSheetOut"
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About cell-grid appearances
'**---------------------------------------------
'''
''' After you select a sheet and you execute this (with the debug-mode), this aligns the appearance
'''
Public Sub DecorateGridSimplyForActiveSheet()
    
    DecorateGridSimply ActiveSheet, ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgMeiryo     ' .DarkBrownFontLightSilverBgMeiryo ' WhiteFontGrayBgMeiryo
End Sub

'''
''' Based on Excel.Range.CurrentRegion
'''
Public Sub DecorateGridBasedOnCurrentRegionOfActiveCell()

    DecorateGridBasedOnCurrentRegion ActiveCell, ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgMeiryo
End Sub

'''
'''
'''
Public Sub DecorateGridSimplybyBgDarkBrownForActiveSheet()

    DecorateGridSmartly ActiveSheet, DarkBrownFontLightSilverBgMeiryo
    
    'DecorateGridSmartly ActiveSheet, SepiaFontLightToDarkBrownGraduationBgMeiryo
End Sub

'''
'''
'''
Public Sub ClearFormatRecordUnusedRangeOnActiveSheet()

    On Error Resume Next

    ThisWorkbook.Application.Run "ClearFormatRecordUnusedRange", ActiveSheet
    
    On Error GoTo 0
End Sub

Public Sub ClearFormatRecordGridAppearnceOfActiveSheet()

    ClearFormatDecoratedRecordGrid ActiveSheet
End Sub

'**---------------------------------------------
'** Conversion enumeration Excel.Constants
'**---------------------------------------------
'''
'''
'''
Public Function GetExcelConstantsEnmFromText(ByVal vstrText As String) As Excel.Constants

    Dim enmConstants As Excel.Constants
    
    enmConstants = xlGeneral
    
    Select Case vstrText
    
        Case "xlRight"
        
            enmConstants = xlRight ' for HorizontalAlignment
        
        Case "xlLeft"
        
            enmConstants = xlLeft ' for HorizontalAlignment
        
        Case "xlGeneral"
        
            enmConstants = xlGeneral ' for HorizontalAlignment
        
        Case "xlJustify"
        
            enmConstants = xlJustify ' for HorizontalAlignment, Japanese is as ���[����
        
        Case "xlCenter"
        
            enmConstants = xlCenter ' for VerticalAlignment
            
        Case "xlTop"
        
            enmConstants = xlTop ' for VerticalAlignment
            
        Case "xlBottom"
        
            enmConstants = xlBottom ' for VerticalAlignment
    End Select

    GetExcelConstantsEnmFromText = enmConstants
End Function


'''
''' convert a String into Excel.Constants delimited some character into Collection object
'''
Public Function GetExcelConstantsColFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Collection
    
    Dim objCol As Collection, varItem As Variant

    
    Set objCol = New Collection
    
    If vstrLineDelimited <> "" Then
        
        For Each varItem In Split(vstrLineDelimited, vstrDelimiter)
        
            objCol.Add GetExcelConstantsEnmFromText(Trim(varItem))
        Next
    End If
    
    Set GetExcelConstantsColFromLineDelimitedChar = objCol
End Function

'''
''' from continuous text delimiting some character, get Dictionary(Of String, Excel.Constants)
'''
Public Function GetTextToExcelConstantsDicFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, i As Long, strArray() As String
    
    Set objDic = New Scripting.Dictionary
    
    If vstrLineDelimited <> "" Then
    
        strArray = Split(vstrLineDelimited, vstrDelimiter)
    
        For i = LBound(strArray) To UBound(strArray) Step 2
        
            If i + 1 <= UBound(strArray) Then
            
                objDic.Add Trim(strArray(i)), GetExcelConstantsEnmFromText(Trim(strArray(i + 1)))
            End If
        Next
    End If
     
    Set GetTextToExcelConstantsDicFromLineDelimitedChar = objDic
End Function

'**---------------------------------------------
'** Decoration enumeration conversions
'**---------------------------------------------
'''
'''
'''
Public Function GetManualDecoratingFieldTitlesRangePatternTextFromEnm(ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern) As String

    Dim strText As String
    
    strText = ""
    
    Select Case venmManualDecoratingFieldTitlesRangePattern
    
        Case ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgArial
        
            strText = "WhiteFontGrayBgArial"
        
        Case ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgMeiryo
        
            strText = "WhiteFontGrayBgMeiryo"
        
        Case ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo
        
            strText = "DarkBrownFontLightSilverBgMeiryo"
        
        Case ManualDecoratingFieldTitlesRangePattern.SepiaFontLightToDarkBrownGraduationBgMeiryo
        
            strText = "SepiaFontLightToDarkBrownGraduationBgMeiryo"
            
        Case ManualDecoratingFieldTitlesRangePattern.WhiteFontDarkGreenGraduationBgMeiryo
    
            strText = "WhiteFontDarkGreenGraduationBgMeiryo"
            
        Case ManualDecoratingFieldTitlesRangePattern.DefaultCellFormatAndMeiryo
        
            strText = "DefaultCellFormatAndMeiryo"
            
        Case ManualDecoratingFieldTitlesRangePattern.WhiteFontDarkOrangeGraduationBgMeiryo
        
            strText = "WhiteFontDarkOrangeGraduationBgMeiryo"
            
        Case ManualDecoratingFieldTitlesRangePattern.DarkBrownFontPaleGradientGreenBgMeiryo
        
            strText = "DarkBrownFontPaleGradientGreenBgMeiryo"
        
        Case ManualDecoratingFieldTitlesRangePattern.DarkBrownFontMorePaleGradientGreenBgMeiryo
            
            strText = "DarkBrownFontMorePaleGradientGreenBgMeiryo"
    End Select

    GetManualDecoratingFieldTitlesRangePatternTextFromEnm = strText
End Function

'''
'''
'''
Public Function GetRecordBordersTextFromEnm(ByVal venmRecordCellsBorders As RecordCellsBorders) As String

    Dim strText As String
    
    strText = ""
    
    Select Case venmRecordCellsBorders
    
        Case RecordCellsBorders.RecordCellsBordersNone
    
            strText = "RecordCellsBordersNone"
        
        Case RecordCellsBorders.RecordCellsGrayAll
        
            strText = "RecordCellsGrayAll"
            
        Case RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal    ' for normal output
    
            strText = "RecordCellsBlacksAndGrayInsideHorizontal"
            
        Case RecordCellsBorders.RecordCellsBlacksAndGrayInsideVertical  ' for transpose output
        
            strText = "RecordCellsBlacksAndGrayInsideVertial"
    End Select

    GetRecordBordersTextFromEnm = strText
End Function


'**---------------------------------------------
'** Decoration detail processing bodies
'**---------------------------------------------
'''
'''
'''
Public Sub DecorateGridSimply(ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal venmManualApearanceFieldTitlePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, _
        Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal, _
        Optional ByVal vsngRecordsFontSize As Single = 9)
    
    Dim strRange As String
    
    With vobjSheet
    
        With .Range(ConvertXlColumnIndexToLetter(.UsedRange.Column) & CStr(.UsedRange.Row)).CurrentRegion
    
            If .Rows.Count > 1 Then
                
                strRange = "A2:" & ConvertXlColumnIndexToLetter(.Columns.Count) & CStr(.Rows.Count)
        
                DecorateRangeOfRecords .Range(strRange), False, venmRecordCellsBorders, vsngRecordsFontSize
            End If
    
            strRange = "A1:" & ConvertXlColumnIndexToLetter(.Columns.Count) & "1"
            
            DecorateRangeOfFieldTitleInformationsForManualGrids .Range(strRange), venmManualApearanceFieldTitlePattern
        End With
    End With
End Sub


'''
'''
'''
Public Sub DecorateGridBasedOnCurrentRegion(ByVal vobjTopLeftRange As Excel.Range, _
        Optional ByVal venmManualApearanceFieldTitlePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, _
        Optional ByVal venmRecordBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal, _
        Optional ByVal vsngFontSize As Single = 9, _
        Optional ByVal vblnHeaderTitlesExists As Boolean = True)
    
    Dim objRegion As Excel.Range, strRange As String, strSpecifiedFirstRowRange As String, objRange As Excel.Range
    
    If Not IsEmpty(vobjTopLeftRange.Cells(1, 1).Value) Then
    
        Set objRegion = vobjTopLeftRange.CurrentRegion
    
        If vblnHeaderTitlesExists Then
        
            With objRegion
            
                If .Rows.Count > 1 Then
                
                    Set objRange = .Worksheet.Range(.Cells(2, 1), .Cells(.Rows.Count, .Columns.Count))
                
                    DecorateRangeOfRecords objRange, False, venmRecordBorders, vsngFontSize
                End If
            
                Set objRange = .Worksheet.Range(.Cells(1, 1), .Cells(1, .Columns.Count))
            
                DecorateRangeOfFieldTitleInformationsForManualGrids objRange, venmManualApearanceFieldTitlePattern
            End With
        Else
            DecorateRangeOfRecords objRegion, False, venmRecordBorders, vsngFontSize
        End If
            
    End If
End Sub



'''
''' Transpose appearance setting for ActiveSheet
'''
Public Sub DecorateTransposedGridSimplyForActiveSheet()

    DecorateTransposedGridSimply ActiveSheet
End Sub


'''
''' Transpose appearance setting simply
'''
Public Sub DecorateTransposedGridSimply(ByVal vobjSheet As Excel.Worksheet, Optional ByVal venmManualApearanceFieldTitlePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgMeiryo, Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal, Optional ByVal vsngFontSize As Single = 9, Optional ByVal vintStartingRow As Long = 1, Optional ByVal vintFieldTitlesColumn As Long = 1)
    
    Dim objTopLeftRange As Excel.Range, strTopLeftRange As String
    
    strTopLeftRange = ConvertXlColumnIndexToLetter(vintFieldTitlesColumn) & CStr(vintStartingRow)
    
    Set objTopLeftRange = vobjSheet.Range(strTopLeftRange)

    DecorateTransposedGrid objTopLeftRange, venmManualApearanceFieldTitlePattern, venmRecordCellsBorders, vsngFontSize
End Sub


'''
''' Transpose appearance setting for details
'''
Public Sub DecorateTransposedGrid(ByVal vobjTopLeftRange As Excel.Range, Optional ByVal venmManualApearanceFieldTitlePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgMeiryo, Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal, Optional ByVal vsngFontSize As Single = 9)
    
    Dim objSheet As Excel.Worksheet, objTableRange As Excel.Range, strRange As String

    Set objSheet = vobjTopLeftRange.Worksheet
    
    Set objTableRange = vobjTopLeftRange.CurrentRegion

    With objTableRange
    
        If .Columns.Count > 1 Then
        
            strRange = ConvertXlColumnIndexToLetter(.Column + 1) & CStr(.Row) & ":" & ConvertXlColumnIndexToLetter(.Column + .Columns.Count - 1) & CStr(.Row + .Rows.Count - 1)
        
            DecorateRangeOfRecords .Range(strRange), False, venmRecordCellsBorders, vsngFontSize
        End If
    
        strRange = ConvertXlColumnIndexToLetter(.Column) & CStr(.Row) & ":" & ConvertXlColumnIndexToLetter(.Column) & CStr(.Row + .Rows.Count - 1)
    
        DecorateRangeOfFieldTitleInformationsForManualGrids .Range(strRange), venmManualApearanceFieldTitlePattern
    End With

End Sub

'''
'''
'''
Public Sub DecorateSelectedRangeSmartly()

    DecorateRangeSmartly Selection, ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgMeiryo
End Sub


Public Sub DecorateGridSmartly(ByVal vobjSheet As Excel.Worksheet, Optional venmManualApearanceFieldTitlePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, Optional venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal, Optional vsngFontSize As Single = 9)

    DecorateRangeSmartly vobjSheet.UsedRange, venmManualApearanceFieldTitlePattern, venmRecordCellsBorders, vsngFontSize
End Sub

'''
''' The Smart means two points,
''' 1st, if title is merged cells and there are more than one rows, this supports the field title rows
''' 2nd, if field title is null string, this doesn't change the appearance.
'''
Public Sub DecorateRangeSmartly(ByVal vobjTableRange As Excel.Range, _
        Optional ByVal venmManualApearanceFieldTitlePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, _
        Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal, _
        Optional ByVal vsngFontSize As Single = 9)
    
    
    Dim strRange As String, intFieldTitlesRowMax As Long, i As Long, objRange As Excel.Range
    Dim objExistedColumnRangeDic As Scripting.Dictionary    ' left lower Column Index to right upper Column Index dictionary
    
    Dim intFoundLeftColumnIndex As Long, intFoundRightColumnIndex As Long
    Dim intRowIndex As Long, blnTitleTextFound As Boolean, varKey As Variant
    
    Dim objSheet As Excel.Worksheet, intLeftFieldIndex As Long, intTopRowIndex As Long, intBottomRowIndex As Long
    
    
    Set objSheet = vobjTableRange.Worksheet
    
    With objSheet
    
        intFieldTitlesRowMax = 1
        
        'Set objRange = .Range("A1")
        Set objRange = vobjTableRange.Cells(1, 1)
        
        intTopRowIndex = objRange.Row
        
        intLeftFieldIndex = objRange.Column
        
        If objRange.MergeCells Then
        
            Debug.Print objRange.MergeArea.Address
            
            If objRange.MergeArea.Rows.Count > 1 Then
            
                intFieldTitlesRowMax = objRange.MergeArea.Rows.Count
            End If
            
            Debug.Print objRange.Value
        End If
        
        Set objExistedColumnRangeDic = New Scripting.Dictionary
        
        intBottomRowIndex = intTopRowIndex + intFieldTitlesRowMax - 1
        
        intFoundLeftColumnIndex = 0
        
        intFoundRightColumnIndex = 0
        
        blnTitleTextFound = False
        
        i = intLeftFieldIndex
        
        Do
            blnTitleTextFound = False
            
            For intRowIndex = intTopRowIndex To intBottomRowIndex
                
                If Not IsEmpty(.Cells(intRowIndex, i).Value) Then
                    
                    If "" <> CStr(.Cells(intRowIndex, i).Value) Then
                        
                        blnTitleTextFound = True
                        
                        Exit For
                    End If
                End If
            Next
            
            If blnTitleTextFound Then
                
                If intFoundLeftColumnIndex = 0 Then
                    
                    intFoundLeftColumnIndex = i
                End If
            Else
            
                intFoundRightColumnIndex = i - 1
            
                objExistedColumnRangeDic.Add intFoundLeftColumnIndex, intFoundRightColumnIndex
            
                intFoundLeftColumnIndex = 0
                
                intFoundRightColumnIndex = 0
            End If
        
            i = i + 1
            
        Loop While i <= .UsedRange.Columns.Count
        
        If intFoundLeftColumnIndex > 0 And intFoundRightColumnIndex = 0 Then
            
            objExistedColumnRangeDic.Add intFoundLeftColumnIndex, .UsedRange.Columns.Count
        End If
        
        For Each varKey In objExistedColumnRangeDic.Keys
            
            intFoundLeftColumnIndex = CLng(varKey)
            
            intFoundRightColumnIndex = CLng(objExistedColumnRangeDic.Item(varKey))
        
            strRange = ConvertXlColumnIndexToLetter(intFoundLeftColumnIndex) & CStr(intBottomRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(intFoundRightColumnIndex) & CStr(vobjTableRange.Row + vobjTableRange.Rows.Count - 1)

            DecorateRangeOfRecords .Range(strRange), False, venmRecordCellsBorders, vsngFontSize
            
            strRange = ConvertXlColumnIndexToLetter(intFoundLeftColumnIndex) & CStr(intTopRowIndex) & ":" & ConvertXlColumnIndexToLetter(intFoundRightColumnIndex) & CStr(intBottomRowIndex)
            
            DecorateRangeOfFieldTitleInformationsForManualGrids .Range(strRange), venmManualApearanceFieldTitlePattern
        Next
    End With
End Sub

'''
''' After you select a range in a sheet and you execute this (with the debug-mode), this aligns the appearance in the selected-range
'''
Public Sub DecorateSelectedRangeOfRecords()

    DecorateRangeOfRecords Selection
End Sub

'''
''' After you select a field title range in a sheet and you execute this (with the debug-mode), this aligns the field title appearance in the selected-range
'''
Public Sub DecorateSelectedRangeOfFieldTitles()

    DecorateRangeOfFieldTitleInformationsForManualGrids Selection, DarkBrownFontLightSilverBgMeiryo
End Sub


'''
'''
'''
''' <Argument>vobjSheet: Input-Output</Argument>
''' <Argument>vintFieldTitlesRowIndex: Input</Argument>
''' <Argument>vintTopLeftColumnIndex: Input</Argument>
Public Sub ClearFormatDecoratedRecordGrid(ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    With vobjSheet
    
        With .Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(.UsedRange.Columns.Count) & CStr(.UsedRange.Rows.Count))
    
            .ClearFormats
        End With
    End With
End Sub

'**---------------------------------------------
'** Horizontal alignment of Excel.Worksheet
'**---------------------------------------------
'''
'''
'''
Public Sub SetHorizontalAlignmentCellsToTable(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitlesToHorizontalAlignmentDic As Scripting.Dictionary, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)

    Dim objFieldTitleToIndexDic As Scripting.Dictionary, objTableRange As Excel.Range, varFieldTitle As Variant
    Dim enmConstants As Excel.Constants, intColumnIndex As Long

    Dim objAColumnRecordRange As Excel.Range


    Set objFieldTitleToIndexDic = GetFieldTitleToPositionIndexDic(GetEnumeratorKeysColFromDic(vobjFieldTitlesToHorizontalAlignmentDic), vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
    Set objTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
    
    If Not objFieldTitleToIndexDic Is Nothing And Not objTableRange Is Nothing Then
        
        If objTableRange.Rows.Count > 1 Then
        
            With objFieldTitleToIndexDic
            
                For Each varFieldTitle In .Keys
                    
                    intColumnIndex = .Item(varFieldTitle)
                    
                    With objTableRange
                        
                        Set objAColumnRecordRange = vobjSheet.Range(.Cells(2, intColumnIndex), .Cells(.Rows.Count, intColumnIndex))
                        
                        With objAColumnRecordRange
                        
                            'Debug.Print .Address
                        
                            enmConstants = vobjFieldTitlesToHorizontalAlignmentDic.Item(varFieldTitle)
                        
                            .HorizontalAlignment = enmConstants
                        End With
                    End With
                Next
            End With
        End If
    End If
End Sub

'**---------------------------------------------
'** Settings of WrapText on Excel.Worksheet
'**---------------------------------------------
'''
'''
'''
Public Sub SetEnablingWrapTextCellsToTable(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitlesToSetEnablingWrapTextCells As Collection, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)

    Dim objFieldTitleToIndexDic As Scripting.Dictionary, objTableRange As Excel.Range, varFieldTitle As Variant
    Dim intRowIndex As Long, intRowMax As Long, intColumnIndex As Long
    Dim intStartRowIdx As Long, intEndRowIdx As Long, objCell As Excel.Range, objMergedCells As Excel.Range


    Set objFieldTitleToIndexDic = GetFieldTitleToPositionIndexDic(vobjFieldTitlesToSetEnablingWrapTextCells, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
    Set objTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
    
    
    If Not objFieldTitleToIndexDic Is Nothing And Not objTableRange Is Nothing Then
        
        If objTableRange.Rows.Count > 1 Then
        
            With objFieldTitleToIndexDic
            
                For Each varFieldTitle In .Keys
                    
                    intColumnIndex = .Item(varFieldTitle)
                    
                    With objTableRange
                    
                        intStartRowIdx = 2
                        
                        intEndRowIdx = .Rows.Count
                        
                    
                        intRowIndex = intStartRowIdx
                        
                        Do While intRowIndex <= intEndRowIdx
                            
                            Set objCell = .Cells(intRowIndex, intColumnIndex)
                            
                            If objCell.MergeCells Then
                                
                                Set objMergedCells = objCell.MergeArea
                                                        
                                With objMergedCells
                                    
                                    .WrapText = True
                                    
                                    intRowIndex = intRowIndex + .Rows.Count
                                End With
                            Else
                                objCell.WrapText = True
                            
                                intRowIndex = intRowIndex + 1
                            End If
                        Loop
                    End With
                Next
            End With
        End If
    End If

End Sub

'''
'''
'''
Public Sub DecorateRangeOfRecords(ByVal vobjSelectedRange As Excel.Range, _
        Optional ByVal vblnForceToSetSameRowHeight As Boolean = False, _
        Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal, _
        Optional ByVal vsngFontSize As Single = 9, _
        Optional ByVal vblnNoIncludeFieldTitlesRow As Boolean = False, _
        Optional ByVal vblnNoIncludeFieldTitlesColumn As Boolean = True)
    
    ' About RowHeight
    AdjustRowHeight vobjSelectedRange, vblnForceToSetSameRowHeight, 18
    
    ' About Borders and FontSize
    DecorateRangeAboutBordersAndFont vobjSelectedRange, venmRecordCellsBorders, vsngFontSize, vblnNoIncludeFieldTitlesRow, vblnNoIncludeFieldTitlesColumn
End Sub

'''
'''
'''
Public Sub DecorateRangeOfVariousMetaInfo(ByVal vobjSelectedRange As Excel.Range, _
    Optional venmApearanceFieldTitlePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgArial)


    DecorateRangeOfFieldTitleInformationsForManualGrids vobjSelectedRange, venmApearanceFieldTitlePattern
End Sub


'''
'''
'''
''' <Argument>vobjSelectedRange: Input-Output</Argument>
''' <Argument>venmApearanceFieldTitlePattern: Input</Argument>
Public Sub DecorateRangeOfFieldTitleInformationsForManualGrids(ByVal vobjSelectedRange As Excel.Range, _
        Optional venmApearanceFieldTitlePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgArial)
    
    Dim varBorder As Variant, enmBorder As Excel.XlBordersIndex
    
    Dim strRange As String

    With vobjSelectedRange
        
        strRange = CStr(.Row) & ":" & CStr(.Rows(.Rows.Count).Row)
        
        .Worksheet.Range(strRange).RowHeight = 18
        
        .VerticalAlignment = xlBottom
        
        Select Case venmApearanceFieldTitlePattern
        
            Case ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgArial
        
                DecorateFieldTitleFontAndInteriorForWhiteFontGrayBgArial vobjSelectedRange
        
            Case WhiteFontGrayBgMeiryo
        
                DecorateFieldTitleFontAndInteriorForWhiteFontGrayBgMeiryo vobjSelectedRange
        
            Case ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo
            
                DecorateFieldTitleFontAndInteriorForDarkBrownFontLightSilverBgMeiryo vobjSelectedRange
        
            Case ManualDecoratingFieldTitlesRangePattern.SepiaFontLightToDarkBrownGraduationBgMeiryo
            
                DecorateFieldTitleFontAndInteriorForSepiaFontLightToDarkBrownGraduationBgMeiryo vobjSelectedRange
        
            Case ManualDecoratingFieldTitlesRangePattern.WhiteFontDarkGreenGraduationBgMeiryo
            
                DecorateFieldTitleFontAndInteriorForWhiteFontDarkGreenGraduationBgMeiryo vobjSelectedRange
                
            Case ManualDecoratingFieldTitlesRangePattern.DefaultCellFormatAndMeiryo
            
                DecorateFieldTitleFontAndInteriorForDefaultCellFormatAndMeiryo vobjSelectedRange
                
            Case WhiteFontDarkOrangeGraduationBgMeiryo
            
                DecorateFieldTitleFontAndInteriorForWhiteFontDarkOrangeGraduationBgMeiryo vobjSelectedRange
            
            
            Case ManualDecoratingFieldTitlesRangePattern.DarkBrownFontPaleGradientGreenBgMeiryo
            
                DecorateFieldTitleFontAndInteriorForDarkBrownFontPaleGradientGreenBgMeiryo vobjSelectedRange
                
            Case ManualDecoratingFieldTitlesRangePattern.DarkBrownFontMorePaleGradientGreenBgMeiryo
            
                DecorateFieldTitleFontAndInteriorForDarkBrownFontMorePaleGradientGreenBgMeiryo vobjSelectedRange
        End Select
        
        
        .Borders(xlDiagonalDown).LineStyle = xlNone
        
        .Borders(xlDiagonalUp).LineStyle = xlNone
        
        
        For Each varBorder In GetXlBorderIndexColFromOptions()
        
            enmBorder = varBorder
            
            With .Borders(enmBorder)
            
                .LineStyle = xlContinuous
                
                .ColorIndex = xlAutomatic
                
                .TintAndShade = 0
                
                .Weight = xlThin
            End With
        Next
    End With
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Public Sub DecorateRangeToNeedToEmphasize(ByVal vobjRange As Excel.Range, ByVal venmEmphasizingRecordsInterior As EmphasizingRecordsInterior)

    Select Case venmEmphasizingRecordsInterior
    
        Case EmphasizingRecordsInterior.LightYellowGradiant
            With vobjRange.Interior
                
                .Pattern = xlPatternLinearGradient
            
                With .Gradient
                
                    .Degree = 90
                    
                    With .ColorStops
                    
                        .Clear
                
                        With .Add(0)
                        
                            .Color = VBA.RGB(255, 249, 231) ' 15202815
                        End With
                        
                        With .Add(0.5)
                            
                            .Color = VBA.RGB(242, 227, 80) ' 5301234
                        End With
                        
                        With .Add(1)
                            
                            .Color = VBA.RGB(255, 249, 231) ' 15202815
                        End With
                    End With
                End With
                
            End With
        
        Case EmphasizingRecordsInterior.LightRedGradiant
            
            With vobjRange.Interior
                
                .Pattern = xlPatternLinearGradient
            
                With .Gradient
                    
                    .Degree = 125   ' 135
                    
                    With .ColorStops
                    
                        .Clear
                
                        With .Add(0)
                        
                            .Color = VBA.RGB(221, 84, 47) ' 3101917
                        End With
                        
                        With .Add(0.5)
                            
                            .Color = VBA.RGB(255, 239, 235) ' 15462399
                        End With
                        
                        With .Add(1)
                            
                            .Color = VBA.RGB(221, 84, 47) ' 3101917
                        End With
                    End With
                End With
            End With
    End Select

End Sub


'''
'''
'''
Public Sub DecorateRowsRangeForMatchingValues(ByVal vobjSheet As Excel.Worksheet, ByVal vstrTargetFieldTitle As String, ByVal vobjMatchValueToSomethingDic As Scripting.Dictionary, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1, Optional ByVal venmEmphasizingRecordsInterior As EmphasizingRecordsInterior = EmphasizingRecordsInterior.LightYellowGradiant)
    
    Dim objCurrentRegion As Excel.Range, objFieldTitleToPositionIndexDic As Scripting.Dictionary, objFieldTitles As Collection
    Dim intRecordRowsCount As Long
    Dim varTargetColumnValues As Variant, intTargetFieldColumnIndex As Long, i As Long, varValue As Variant
    Dim strMinColumnLetter As String, strMaxColumnLetter As String, strRowRange As String, objRange As Excel.Range

    Set objCurrentRegion = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion

    intRecordRowsCount = objCurrentRegion.Rows.Count - 1

    If intRecordRowsCount > 0 Then
    
        Set objFieldTitles = GetFieldTitlesColFromSheet(vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
        Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(objFieldTitles, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
        With vobjSheet
        
            Set objRange = Nothing
        
            With objCurrentRegion
            
                ReDim varTargetColumnValues(1 To .Rows.Count, 1 To 1)
                
                intTargetFieldColumnIndex = CLng(objFieldTitleToPositionIndexDic.Item(vstrTargetFieldTitle))
                
                varTargetColumnValues = vobjSheet.Cells(vintFieldTitlesRowIndex, intTargetFieldColumnIndex).Resize(.Rows.Count, 1).Value
            
                strMinColumnLetter = ConvertXlColumnIndexToLetter(.Column)
                
                strMaxColumnLetter = ConvertXlColumnIndexToLetter(.Columns.Count + .Column - 1)
            
                For i = 2 To .Rows.Count
                
                    varValue = varTargetColumnValues(i, 1)
                
                    If vobjMatchValueToSomethingDic.Exists(varValue) Then
                    
                        ' need to paint the specified interior back-ground color
                        
                        strRowRange = strMinColumnLetter & CStr(i + .Row - 1) & ":" & strMaxColumnLetter & CStr(i + .Row - 1)
                        
                        If objRange Is Nothing Then
                        
                            Set objRange = vobjSheet.Range(strRowRange)
                        Else
                            Set objRange = Excel.Union(objRange, vobjSheet.Range(strRowRange))
                        End If
                    End If
                Next
            End With
        End With
        
        If Not objRange Is Nothing Then
        
            DecorateRangeToNeedToEmphasize objRange, venmEmphasizingRecordsInterior
        End If
    End If
End Sub


Public Sub DecorateRowsRangeForMatchingValuesByCol(ByVal vobjSheet As Excel.Worksheet, ByVal vstrTargetFieldTitle As String, ByVal vobjMatchValues As Collection, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1)

    DecorateRowsRangeForMatchingValues vobjSheet, vstrTargetFieldTitle, GetDicFromColButValuesAreAllNothing(vobjMatchValues), vintFieldTitlesRowIndex, vintTopLeftColumnIndex
End Sub

'**---------------------------------------------
'** Insert boundary border line based on Worksheet.UsedRange
'**---------------------------------------------
'''
'''
'''
Public Sub WriteTopAndBottomBoundaryBorderLineOfSpecifiedRow(ByRef robjSheet As Excel.Worksheet, ByVal vintTopLeftRowIndex As Long)

    If vintTopLeftRowIndex > 1 Then
    
        ' Write cells border line above the boundary
    
        With robjSheet
        
            With .Range(ConvertXlColumnIndexToLetter(.UsedRange.Column) & CStr(vintTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(.UsedRange.Column + .UsedRange.Columns.Count - 1) & CStr(vintTopLeftRowIndex)).Borders.Item(xlEdgeTop)
            
                .LineStyle = xlDouble: .ThemeColor = 6: .Weight = xlThick
                
                .TintAndShade = -0.249946592608417
            End With
        End With
    End If
End Sub



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' set WrapText for each column of a table
'''
Private Sub msubSanityTestOfSetEnablingWrapTextCellsToTable()

    Dim objFieldTitlesToSetEnablingWrapTextCells As Collection
    
    Set objFieldTitlesToSetEnablingWrapTextCells = GetColFromLineDelimitedChar("MetaClassification")

    SetEnablingWrapTextCellsToTable ActiveSheet, objFieldTitlesToSetEnablingWrapTextCells
End Sub
