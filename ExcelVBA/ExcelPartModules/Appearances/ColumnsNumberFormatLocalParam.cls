VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ColumnsNumberFormatLocalParam"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 16/Aug/2022    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mintFieldTitlesRowIndex As Long

Private mintTopLeftColumnIndex As Long


Private mobjNumberFormatLocalToFieldTitles As Scripting.Dictionary  ' Dictionary(Of String[NumberFormatLocal], Of Collection(Of String[Field Titles]))


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////

Private Sub Class_Initialize()

    mintFieldTitlesRowIndex = 1
    
    mintTopLeftColumnIndex = 1
    
    Set mobjNumberFormatLocalToFieldTitles = New Scripting.Dictionary
End Sub
Private Sub Class_Terminate()

    Set mobjNumberFormatLocalToFieldTitles = Nothing
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Get FieldTitlesRowIndex() As Long

    FieldTitlesRowIndex = mintFieldTitlesRowIndex
End Property
Public Property Let FieldTitlesRowIndex(ByVal vintFieldTitlesRowIndex As Long)

    mintFieldTitlesRowIndex = vintFieldTitlesRowIndex
End Property

Public Property Get TopLeftColumnIndex() As Long

    TopLeftColumnIndex = mintTopLeftColumnIndex
End Property
Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)

    mintTopLeftColumnIndex = vintTopLeftColumnIndex
End Property


'''
''' Dictionary(Of String[NumberFormatLocal], Of Collection(Of String[Field Titles]))
'''
Public Property Get NumberFormatLocalToFieldTitles() As Scripting.Dictionary

    Set NumberFormatLocalToFieldTitles = mobjNumberFormatLocalToFieldTitles
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////

Public Sub SetNumberFormatLocalAndApplyFieldTitles(ByVal vstrNumberFormatLocal As String, ByVal vobjFieldTitles As Collection)

    If Not vobjFieldTitles Is Nothing Then
    
        If vobjFieldTitles.Count > 0 And vstrNumberFormatLocal <> "" Then
        
            With mobjNumberFormatLocalToFieldTitles
            
                If Not .Exists(vstrNumberFormatLocal) Then
                
                    .Add vstrNumberFormatLocal, vobjFieldTitles
                End If
            End With
        End If
    End If

End Sub

Public Sub ClearNumberFormatLocalToFieldTitles()

    mobjNumberFormatLocalToFieldTitles.RemoveAll
End Sub

'''
'''
'''
Public Function GetAllRelativeFieldTitles() As Collection

    Dim varCol As Variant, varFieldTitle As Variant, objCol As Collection, strFieldTitle As String
    Dim objAllCol As Collection
    Dim objCheckDic As Scripting.Dictionary


    Set objAllCol = New Collection

    If Not mobjNumberFormatLocalToFieldTitles Is Nothing Then
    
        If mobjNumberFormatLocalToFieldTitles.Count > 0 Then
        
            Set objCheckDic = New Scripting.Dictionary
        
            With mobjNumberFormatLocalToFieldTitles
            
                For Each varCol In .Items
                
                    Set objCol = varCol
            
                    For Each varFieldTitle In objCol
                    
                        strFieldTitle = varFieldTitle
                        
                        If Not objCheckDic.Exists(strFieldTitle) Then
                        
                            objCheckDic.Add strFieldTitle, vbNullString
                        
                            objAllCol.Add strFieldTitle
                        End If
                    Next
                Next
            End With
        End If
    End If


    Set GetAllRelativeFieldTitles = objAllCol
End Function
