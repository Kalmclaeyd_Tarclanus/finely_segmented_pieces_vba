VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "FormatConditionExpander"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 16/Aug/2022    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private menmCellFormatConditionType As CellBackground

Private mobjFormatVariousConditions As Collection

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    menmCellFormatConditionType = CellBackground.FormatConditionTextContain

    Set mobjFormatVariousConditions = New Collection
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get CellFormatConditionType() As CellBackground

    CellFormatConditionType = menmCellFormatConditionType
End Property
Public Property Let CellFormatConditionType(ByVal vintCellFormatConditionType As CellBackground)

    menmCellFormatConditionType = vintCellFormatConditionType
End Property



Public Property Let CondtionText(ByVal vstrCondtionText As String)

    Dim objFormatTextContainCondition As FormatTextContainCondition
    
    If mobjFormatVariousConditions.Count = 0 Then
    
        Set objFormatTextContainCondition = New FormatTextContainCondition
        
        mobjFormatVariousConditions.Add objFormatTextContainCondition
    Else
        Set objFormatTextContainCondition = mobjFormatVariousConditions.Item(1)
    End If

    objFormatTextContainCondition.CondtionText = vstrCondtionText
End Property
Public Property Get CondtionText() As String

    Dim objFormatTextContainCondition As FormatTextContainCondition
    
    If mobjFormatVariousConditions.Count = 0 Then
    
        Set objFormatTextContainCondition = New FormatTextContainCondition
        
        mobjFormatVariousConditions.Add objFormatTextContainCondition
    Else
        Set objFormatTextContainCondition = mobjFormatVariousConditions.Item(1)
    End If
    
    CondtionText = objFormatTextContainCondition.CondtionText
End Property


Public Property Let CellColorArrangementType(ByVal venmCellColorArrangement As CellColorArrangement)

    Dim objFormatTextContainCondition As FormatTextContainCondition
    
    If mobjFormatVariousConditions.Count = 0 Then
    
        Set objFormatTextContainCondition = New FormatTextContainCondition
        
        mobjFormatVariousConditions.Add objFormatTextContainCondition
    Else
        Set objFormatTextContainCondition = mobjFormatVariousConditions.Item(1)
    End If
    
    objFormatTextContainCondition.CellColorArrangementType = venmCellColorArrangement
End Property
Public Property Get CellColorArrangementType() As CellColorArrangement

    Dim objFormatTextContainCondition As FormatTextContainCondition
    
    If mobjFormatVariousConditions.Count = 0 Then
    
        Set objFormatTextContainCondition = New FormatTextContainCondition
        
        mobjFormatVariousConditions.Add objFormatTextContainCondition
    Else
        Set objFormatTextContainCondition = mobjFormatVariousConditions.Item(1)
    End If
    
    CellColorArrangementType = objFormatTextContainCondition.CellColorArrangementType
End Property

'///////////////////////////////////////////////
'/// Operation
'///////////////////////////////////////////////
'''
'''
'''
Public Sub AddTextContainCondition(ByVal vstrCondtionText As String, ByVal venmCellColorArrangement As CellColorArrangement)

    Dim objFormatTextContainCondition As FormatTextContainCondition
    
    Set objFormatTextContainCondition = New FormatTextContainCondition
    
    With objFormatTextContainCondition
    
        .CondtionText = vstrCondtionText
        
        .CellColorArrangementType = venmCellColorArrangement
    End With

    mobjFormatVariousConditions.Add objFormatTextContainCondition
End Sub

'''
'''
'''
Public Sub AddFormulasConditionFromParameters(ByVal vstrFormula1 As String, _
        venmFormatConditionType As Excel.XlFormatConditionType, _
        venmFormatConditionOperator As Excel.XlFormatConditionOperator, _
        ByVal vintInteriorRGBColor As Long, _
        Optional ByVal vstrFormula2 As String = "", _
        Optional ByVal vstrText As String = "")
    
    menmCellFormatConditionType = FormatConditionFormulas

    Dim objFormatFolumasCondition As FormatFormulasCondition
    
    
    Set objFormatFolumasCondition = New FormatFormulasCondition
    
    With objFormatFolumasCondition
    
        .FormatConditionType = venmFormatConditionType
        
        .FormatConditionOperator = venmFormatConditionOperator
        
        .Formula1 = vstrFormula1
        
        .Formula2 = vstrFormula2
        
        .Text = vstrText
    
        'Set .InteriorValues =
        '.InteriorColor = vintInteriorRGBColor
    
    End With
 
    mobjFormatVariousConditions.Add objFormatFolumasCondition
End Sub

'''
''' AddFormulasConditionFromParamDic
'''
Public Sub AddFormulasConditionsFromParamDic(ByVal vobjFormatConditionParamDic As Scripting.Dictionary, _
        Optional ByVal vstrFormulaPrefix As String = "", _
        Optional ByVal vstrFormulaSuffix As String = "")
    
    
    menmCellFormatConditionType = FormatConditionFormulas

    Dim varKey As Variant, objValues As Collection

    For Each varKey In vobjFormatConditionParamDic.Keys
    
        Set objValues = vobjFormatConditionParamDic.Item(varKey)
    
        mobjFormatVariousConditions.Add GetFormatFormulasConditionFromValuesCol(objValues, vstrFormulaPrefix, vstrFormulaSuffix)
    Next
End Sub

'''
'''
'''
Public Sub AddFormulasCondition(ByVal vobjFormatFormulasCondition As FormatFormulasCondition)
    
    menmCellFormatConditionType = FormatConditionFormulas

    mobjFormatVariousConditions.Add vobjFormatFormulasCondition
End Sub

'''
'''
'''
Public Sub AddFormulasConditions(ByVal vobjFormatFormulasConditions As Collection)
    
    Dim varFormatFormulasCondition As Variant, objFormatFormulasCondition As FormatFormulasCondition
    
    
    menmCellFormatConditionType = FormatConditionFormulas

    For Each varFormatFormulasCondition In vobjFormatFormulasConditions
        
        Set objFormatFormulasCondition = varFormatFormulasCondition
        
        mobjFormatVariousConditions.Add objFormatFormulasCondition
    Next
End Sub


'''
''' change format condition for specified range
'''
Public Sub ChangeCellFormatCondition(ByVal vobjRange As Excel.Range)
    
    Dim objFormatCondition As Excel.FormatCondition
    
    Dim objFormatTextContainCondition As FormatTextContainCondition ' defined interface by reducing parameters
    Dim objFormatFormulasCondition As FormatFormulasCondition
    Dim varItem As Variant
    
    With vobjRange
        
        Select Case menmCellFormatConditionType
        
            Case CellBackground.FormatConditionTextContain
                
                ' set Excel.FormatCondition from specified CellColorArrangement relative parameters
                msubChangeTextContainSpecifiedFormatConditions vobjRange
                
            Case CellBackground.FormatConditionFormulas
            
                ' reproduce FormatCondition from scanning the existed Excel.Worksheet which is set some FormatCondition objects into some cells.
                msubChangeFormulasFormatConditions vobjRange
        End Select
    End With
End Sub

'''
''' reproduce Excel.FormatCondition from scanning the existed Excel.Worksheet
'''
Private Sub msubChangeFormulasFormatConditions(ByVal vobjRange As Excel.Range)

    DecorateFormulasFormatConditionRange mobjFormatVariousConditions, vobjRange
End Sub


'''
'''�@set Excel.FormatCondition from specified CellColorArrangement relative parameters
'''
Private Sub msubChangeTextContainSpecifiedFormatConditions(ByVal vobjRange As Excel.Range)
    
    Dim objFormatCondition As Excel.FormatCondition
    
    Dim objFormatTextContainCondition As FormatTextContainCondition ' defined interface by reducing parameters
    Dim varItem As Variant

    With vobjRange
    
        .FormatConditions.Delete
                    
        If mobjFormatVariousConditions.Count > 0 Then
            
            For Each varItem In mobjFormatVariousConditions
                
                Set objFormatTextContainCondition = varItem
            
                Set objFormatCondition = .FormatConditions.Add(xlTextString, String:=objFormatTextContainCondition.CondtionText, TextOperator:=xlContains)
                
                SetFormatCondtionTextFillColorsFromCellColorArrangement objFormatCondition, objFormatTextContainCondition.CellColorArrangementType
               
            Next
            '.StopIfTrue = False
            
    '                    If .FormatConditions.Count > 0 Then
    '                        .FormatConditions(.FormatConditions.Count).SetFirstPriority
    '                    End If
    
        End If
    End With

End Sub



