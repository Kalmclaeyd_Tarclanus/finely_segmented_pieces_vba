Attribute VB_Name = "DecorateXlCellFontAndInterior"
'
'   Decorate Excel Worksheet cells interior and font without changing neither cell-value or cell-formulas
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  2/May/2023    Kalmclaeyd Tarclanus    Separated from DecorationSetterToXlSheet.bas and DataTableSheetOut.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToDecorateXlCellFontAndInterior()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UTfDecorationSetterToXlSheet,DecorationSetterToXlSheet"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About FieldTitleInterior, decorate both interior and font of Worksheet.Cells
'**---------------------------------------------
'''
'''
'''
Public Sub DecorateFieldTitleFontAndInteriorForDefaultTypeRDB(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForDefaultTypeRDB robjFieldTitlesRange
End Sub

'''
'''
'''
Public Sub DecorateFieldTitleFontAndInteriorForDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForDarkBrownFontLightSilver robjFieldTitlesRange
End Sub

'''
'''
'''
Public Sub DecorateFieldTitleFontAndInteriorForDefaultTypeSQLBySepiaFontLightToDarkBrownGraduationBgMeiryo(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForSepiaFontLightToDarkBrownGraduation robjFieldTitlesRange
End Sub


'''
''' decorate field-title by ColumnNameInteriorOfGatheredColTable
'''
Public Sub DecorateFieldTitleFontAndInteriorForGatheredColTable(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForGatheredColTable robjFieldTitlesRange
End Sub

'''
''' decorate field-title by ColumnNameInteriorOfGatheredColTableByOrangeTone
'''
Public Sub DecorateFieldTitleFontAndInteriorForGatheredColTableByOrangeTone(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForWhiteFontDarkOrangeGraduation robjFieldTitlesRange, 225
End Sub

'''
''' decorate field-title by ColumnNameInteriorOfExcelSheetAdoSQL
'''
Public Sub DecorateFieldTitleFontAndInteriorForExcelSheetSQL(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForExcelSheetSQL robjFieldTitlesRange
End Sub



'''
''' decorate field-title by ColumnNameInteriorOfMetaProperties
'''
Public Sub DecorateFieldTitleFontAndInteriorForMetaProperties(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForMetaProperties robjFieldTitlesRange
End Sub


'''
''' ColumnNameInteriorOfMSAccessRDBSQL
'''
Public Sub DecorateFieldTitleFontAndInteriorForMSAccessRDBSQL(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForMSAccessRDBSQL robjFieldTitlesRange
End Sub

'''
''' ColumnNameInteriorOfCsvAdoSQL
'''
Public Sub DecorateFieldTitleFontAndInteriorForCsvAdoSQL(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForCsvAdoSQL robjFieldTitlesRange
End Sub


'''
''' ColumnNameInteriorOfPostgreSQL
'''
Public Sub DecorateFieldTitleFontAndInteriorForInteriorPostgreSQLRDB(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForInteriorPostgreSQLRDB robjFieldTitlesRange
End Sub


'''
''' ColumnNameInteriorOfLoadedINIFileAsTable
'''
Public Sub DecorateFieldTitleFontAndInteriorForLoadedINIFileAsTable(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForLoadedINIFileAsTable robjFieldTitlesRange
End Sub



'''
''' ColumnNameInteriorOfLoadedWinRegistryAsTable
'''
Public Sub DecorateFieldTitleFontAndInteriorForLoadedWinRegistryAsTable(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForLoadedWinRegistryAsTable robjFieldTitlesRange
End Sub


'''
''' ColumnNameInteriorOfExtractedTableFromSheet
'''
Public Sub DecorateFieldTitleFontAndInteriorForExtractedTableFromSheet(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForExtractedTableFromSheet robjFieldTitlesRange
End Sub


'**---------------------------------------------
'** About only font color and interior of ManualDecoratingFieldTitlesRangePattern
'**---------------------------------------------

'''
'''
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForDefaultTypeRDB(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange ' ColumnNameInteriorOfDefaultTypeSQL
    
        With .Font
        
            .ThemeColor = xlThemeColorDark1: .TintAndShade = 0
        End With
    
        With .Interior
        
            .ThemeColor = xlThemeColorLight1: .TintAndShade = 0.35
            
            .PatternTintAndShade = 0
        End With
    End With
End Sub

'''
''' decorate field-title by ColumnNameInteriorOfGatheredColTable
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForGatheredColTable(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange
    
        With .Font
        
            .ThemeColor = xlThemeColorDark1: .TintAndShade = 0
        End With
        
        With .Interior
        
            .Color = RGB(45, 70, 155): .TintAndShade = 0
            
            .PatternTintAndShade = 0
        End With
    End With
End Sub

'''
''' decorate field-title by ColumnNameInteriorOfExcelSheetAdoSQL
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForExcelSheetSQL(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange
    
        With .Font
        
            .ThemeColor = xlThemeColorDark1: .TintAndShade = 0
        End With
        
        With .Interior
                
            .Pattern = xlPatternLinearGradient
            
            With .Gradient
                
                .Degree = 270
                
                With .ColorStops
                
                    .Clear
                
                    With .Add(0): .Color = RGB(40, 55, 30): .TintAndShade = 0: End With
                    
                    With .Add(1): .Color = RGB(35, 75, 20): .TintAndShade = 0: End With
                End With
            End With
        End With
    End With
End Sub

'''
'''
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForDarkBrownFontPaleGradientGreenBgMeiryo(ByRef robjFieldTitlesRange As Excel.Range, _
        Optional ByVal vblnUseMorePaleColor As Boolean = False)

    Dim intFirstColor As Long, intSecondColor As Long

    intFirstColor = RGB(255, 255, 255)
    
    If vblnUseMorePaleColor Then
    
        intSecondColor = RGB(235, 239, 225)  ' HSL 56 Hue, Saturation 78, Lightness 232
    Else
        intSecondColor = RGB(235, 241, 222) ' HSL 56 Hue, Saturation 103, Lightness 232
    End If

    With robjFieldTitlesRange
    
        With .Font
        
            .Color = RGB(74, 69, 41)
        End With
        
        With .Interior
                
            .Pattern = xlPatternLinearGradient
            
            With .Gradient
                
                .Degree = 90
                
                With .ColorStops
                
                    .Clear
                
                    With .Add(0): .Color = intFirstColor: .TintAndShade = 0: End With
                    
                    With .Add(0.5): .Color = intSecondColor: .TintAndShade = 0: End With
                    
                    With .Add(1): .Color = intFirstColor: .TintAndShade = 0: End With
                End With
            End With
        End With
    End With
End Sub


'''
''' decorate field-title by ColumnNameInteriorOfMetaProperties
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForMetaProperties(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange
    
        With .Font
        
            .Color = RGB(252, 218, 254)
        End With
        
        With .Interior
                
            .Pattern = xlPatternLinearGradient
            
            With .Gradient
            
                .Degree = 28
                
                With .ColorStops
                
                    .Clear
                
                    With .Add(0): .Color = RGB(252, 182, 234): End With
    
                    With .Add(0.18): .Color = RGB(140, 14, 95): End With
                    
                    With .Add(0.5): .Color = RGB(196, 22, 59): End With
                    
                    With .Add(0.82): .Color = RGB(140, 14, 95): End With
    
                    With .Add(1): .Color = RGB(241, 71, 179): End With
                End With
            End With
        End With
    End With
End Sub

'''
''' ColumnNameInteriorOfMSAccessRDBSQL
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForMSAccessRDBSQL(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange
    
        With .Font
        
            .ThemeColor = xlThemeColorDark1: .TintAndShade = 0
        End With
    
        With .Interior

            .Pattern = xlPatternLinearGradient

            With .Gradient

                .Degree = 270

                .ColorStops.Clear

                With .ColorStops.Add(0): .Color = RGB(175, 65, 60): End With

                With .ColorStops.Add(1): .Color = RGB(125, 55, 50): End With
            End With
        End With
    End With
End Sub

'''
''' ColumnNameInteriorOfCsvAdoSQL
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForCsvAdoSQL(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange
    
        With .Font
            
            .Color = RGB(249, 249, 249)
        End With
    
        With .Interior

            .Pattern = xlPatternLinearGradient

            With .Gradient

                .Degree = 97 ' 83

                With .ColorStops
                
                    .Clear
    
                    With .Add(0): .Color = RGB(159, 17, 17): End With
                    
                    With .Add(0.09): .Color = RGB(246, 241, 172): End With
    
                    With .Add(0.14): .Color = RGB(150, 42, 42): End With
                    
                    With .Add(0.5): .Color = RGB(38, 38, 38): End With
                    
                    With .Add(0.86): .Color = RGB(53, 23, 23): End With
        
'                    With .Add(0.91)
'
'                        .Color = RGB(246, 241, 172)
'                    End With
        
                    With .Add(1): .Color = RGB(159, 17, 17): End With
                End With
            End With
        End With
    End With
End Sub

'''
''' ColumnNameInteriorOfPostgreSQL
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForInteriorPostgreSQLRDB(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange
    
        With .Font
            
            .ThemeColor = xlThemeColorDark1: .TintAndShade = 0
        End With
    
        With .Interior
                
'            .Pattern = xlSolid
'
'            .PatternColorIndex = xlAutomatic
            
            .Color = RGB(205, 80, 10)
            
            .PatternTintAndShade = 0
        End With
    End With
End Sub

'''
''' ColumnNameInteriorOfLoadedINIFileAsTable
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForLoadedINIFileAsTable(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange
    
        With .Font
            
            .Color = RGB(70, 70, 70)
        End With
    
        With .Interior

            .Pattern = xlPatternLinearGradient

            With .Gradient
            
'                .RectangleLeft = 1: .RectangleRight = 1
'
'                .RectangleTop = 0: .RectangleBottom = 0
                
                '.Degree = 12  '.Degree = 348
                
                .Degree = 192
                
                With .ColorStops
                
                    .Clear
                    
                    With .Add(0): .Color = RGB(255, 255, 255): End With
                    
                    With .Add(0.15): .Color = RGB(240, 239, 251): End With
                    
                    With .Add(0.38): .Color = RGB(240, 239, 251): End With
                
                    With .Add(0.7): .Color = RGB(223, 221, 247): End With
                    
                    With .Add(0.87): .Color = RGB(142, 176, 222): End With
                
                    With .Add(0.91): .Color = RGB(247, 215, 177): End With

                    With .Add(0.94): .Color = RGB(246, 244, 178): End With
                    
                    With .Add(0.96): .Color = RGB(41, 199, 255): End With
                    
                    With .Add(0.98): .Color = RGB(140, 176, 240): End With
                    
                    With .Add(1): .Color = RGB(27, 93, 211): End With
                End With
            End With
        End With
    End With
End Sub

'''
''' ColumnNameInteriorOfLoadedWinRegistryAsTable
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForLoadedWinRegistryAsTable(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange
    
        With .Font
        
            .Color = RGB(70, 70, 70)
        End With
    
        With .Interior

            .Pattern = xlPatternLinearGradient

            With .Gradient

                .Degree = 192
                
                With .ColorStops
                
                    .Clear
                    
                    With .Add(0): .Color = RGB(255, 255, 255): End With
                    
                    With .Add(0.15): .Color = RGB(251, 240, 239): End With
                    
                    With .Add(0.38): .Color = RGB(251, 240, 239): End With
                
                    With .Add(0.7): .Color = RGB(247, 224, 221): End With
                    
                    With .Add(0.87): .Color = RGB(244, 120, 120): End With
                
                    With .Add(0.91): .Color = RGB(246, 174, 160): End With

                    With .Add(0.94): .Color = RGB(246, 244, 178): End With
                    
                    With .Add(0.96): .Color = RGB(45, 89, 251): End With
                    
                    With .Add(0.98): .Color = RGB(223, 92, 19): End With
                    
                    With .Add(1): .Color = RGB(136, 24, 18): End With
                    
                End With
            End With
        End With
    End With
End Sub


'''
''' ColumnNameInteriorOfExtractedTableFromSheet
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForExtractedTableFromSheet(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange
    
        With .Font
            
            .Color = RGB(70, 70, 70)
        End With
    
        With .Interior

            .Pattern = xlPatternLinearGradient

            With .Gradient

                .Degree = 35 ' 90
                
                With .ColorStops
                
                    .Clear
                    
                    With .Add(0): .Color = RGB(255, 255, 255): End With
                    
                    With .Add(0.15): .Color = RGB(239, 251, 242): End With
                    
                    With .Add(0.38): .Color = RGB(247, 253, 248): End With
                
                    With .Add(0.7): .Color = RGB(213, 239, 216): End With
                    
                    With .Add(0.87): .Color = RGB(119, 191, 165): End With

                    With .Add(0.94): .Color = RGB(92, 118, 230): End With
                    
                    With .Add(0.96): .Color = RGB(85, 211, 175): End With
              
                    With .Add(1): .Color = RGB(30, 108, 14): End With
                End With
            End With
        End With
    End With
End Sub




'''
'''
'''
Public Sub DecorateFieldTitleCellsBordersForCommon(ByRef robjRange As Excel.Range, ByVal venmInteriorType As FieldTitleInterior)

    Dim varBorderType As Variant

    With robjRange

        ' About Borders
        .Borders(xlDiagonalDown).LineStyle = xlNone: .Borders(xlDiagonalUp).LineStyle = xlNone
        
        For Each varBorderType In GetXlBorderIndexColFromOptions()
        
            With .Borders(varBorderType)
            
                .LineStyle = xlContinuous
                
                .ThemeColor = 2
                
                If venmInteriorType = ColumnNameInteriorOfPostgreSQL Then
                
                    .TintAndShade = 0.4
                Else
                    .TintAndShade = 0.15
                End If
                
                .Weight = xlThin
                
            End With
        Next
    End With
End Sub


'**---------------------------------------------
'** About ManualDecoratingFieldTitlesRangePatter, decorate both interior and font of Worksheet.Cells
'**---------------------------------------------

'''
''' decorate field-title by ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgArial
'''
Public Sub DecorateFieldTitleFontAndInteriorForWhiteFontGrayBgArial(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange

        With .Font
        
            .Name = "Arial": .FontStyle = "Regular"
            
            .Size = 10
        End With
    End With
    
    DecorateFieldTitleOnlyFontAndInteriorForWhiteFontGray robjFieldTitlesRange
End Sub

'''
''' decorate field-title by WhiteFontGrayBgMeiryo
'''
Public Sub DecorateFieldTitleFontAndInteriorForWhiteFontGrayBgMeiryo(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontAndInteriorForWhiteFontGray robjFieldTitlesRange
End Sub

'''
''' decorate field-title by ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo
'''
Public Sub DecorateFieldTitleFontAndInteriorForDarkBrownFontLightSilverBgMeiryo(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange
    
    DecorateFieldTitleOnlyFontColorAndInteriorForDarkBrownFontLightSilver robjFieldTitlesRange
End Sub

'''
''' decorate field-title by ManualDecoratingFieldTitlesRangePattern.SepiaFontLightToDarkBrownGraduationBgMeiryo
'''
Public Sub DecorateFieldTitleFontAndInteriorForSepiaFontLightToDarkBrownGraduationBgMeiryo(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForSepiaFontLightToDarkBrownGraduation robjFieldTitlesRange
End Sub


'''
''' decorate field-title by ManualDecoratingFieldTitlesRangePattern.DefaultCellFormatAndMeiryo
'''
Public Sub DecorateFieldTitleFontAndInteriorForDefaultCellFormatAndMeiryo(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForDefaultCellFormat robjFieldTitlesRange
End Sub


'''
''' decorate field-title by ManualDecoratingFieldTitlesRangePattern.WhiteFontDarkGreenGraduationBgMeiryo
'''
Public Sub DecorateFieldTitleFontAndInteriorForWhiteFontDarkGreenGraduationBgMeiryo(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange
    
    DecorateFieldTitleOnlyFontColorAndInteriorForExcelSheetSQL robjFieldTitlesRange
    
    'DecorateFieldTitleOnlyFontColorAndInteriorForWhiteFontDarkGreenGraduation robjFieldTitlesRange
End Sub

'''
'''
'''
Public Sub DecorateFieldTitleFontAndInteriorForDarkBrownFontPaleGradientGreenBgMeiryo(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange
    
    DecorateFieldTitleOnlyFontColorAndInteriorForDarkBrownFontPaleGradientGreenBgMeiryo robjFieldTitlesRange, False
End Sub

'''
'''
'''
Public Sub DecorateFieldTitleFontAndInteriorForDarkBrownFontMorePaleGradientGreenBgMeiryo(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange
    
    DecorateFieldTitleOnlyFontColorAndInteriorForDarkBrownFontPaleGradientGreenBgMeiryo robjFieldTitlesRange, True
End Sub


'''
''' decorate field-title by ManualDecoratingFieldTitlesRangePattern.WhiteFontDarkOrangeGraduationBgMeiryo
'''
Public Sub DecorateFieldTitleFontAndInteriorForWhiteFontDarkOrangeGraduationBgMeiryo(ByRef robjFieldTitlesRange As Excel.Range)

    msubDecorateFieldTitleFontStyleMeiryo robjFieldTitlesRange

    DecorateFieldTitleOnlyFontColorAndInteriorForWhiteFontDarkOrangeGraduation robjFieldTitlesRange
End Sub


'**---------------------------------------------
'** About only font color and interior of ManualDecoratingFieldTitlesRangePattern
'**---------------------------------------------
'''
''' decorate field-title by WhiteFontGrayBgMeiryo
'''
Public Sub DecorateFieldTitleOnlyFontAndInteriorForWhiteFontGray(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange

        With .Font
        
            .ThemeColor = xlThemeColorDark1: .TintAndShade = 0
        End With

        With .Interior
        
            .ThemeColor = xlThemeColorLight1
            
            .TintAndShade = 0.35
            
            .PatternTintAndShade = 0
        End With
    End With
End Sub

'''
''' decorate field-title by ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForDarkBrownFontLightSilver(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange

        With .Font
        
            .Color = RGB(74, 69, 41)    ' near to xlThemeColorDark2
            
            .TintAndShade = 0
        End With

        With .Interior
        
            .Pattern = xlPatternLinearGradient
            
            With .Gradient
            
                .Degree = 90
                
                With .ColorStops
                
                    .Clear
                
                    With .Add(0)
                        
                        .ThemeColor = xlThemeColorAccent3: .TintAndShade = 0.8
                    End With
                    
                    With .Add(0.5)
                        
                        .ThemeColor = xlThemeColorDark1: .TintAndShade = 0
                    End With
                    
                    With .Add(1)
                        
                        .ThemeColor = xlThemeColorAccent3: .TintAndShade = 0.8
                    End With
                End With
            End With
        End With
    End With
End Sub


'''
''' decorate field-title by ManualDecoratingFieldTitlesRangePattern.SepiaFontLightToDarkBrownGraduationBgMeiryo
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForSepiaFontLightToDarkBrownGraduation(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange

        With .Font
        
            ' Sepia color
            .Color = RGB(79, 56, 39)    ' 2570319
        End With

        With .Interior
        
            .Pattern = xlPatternLinearGradient
            
            With .Gradient
            
                .Degree = 90
            
                With .ColorStops
                
                    .Clear
                    
                    With .Add(0)
                    
                        .Color = RGB(241, 241, 245) ' 16118257
                    End With
                    
                    With .Add(1)
                    
                        .Color = RGB(173, 164, 145) ' 9544877
                    End With
                End With
            End With
        End With
    End With
End Sub


'''
''' decorate field-title by ManualDecoratingFieldTitlesRangePattern.DefaultCellFormatAndMeiryo
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForDefaultCellFormat(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange

        '.ClearFormats

        With .Font
        
            .ThemeColor = XlThemeColor.xlThemeColorLight1: .TintAndShade = 0
        End With
        
        With .Interior
        
            .Pattern = xlSolid: .PatternColorIndex = xlAutomatic
        End With
    End With
End Sub


'''
''' decorate field-title by ManualDecoratingFieldTitlesRangePattern.WhiteFontDarkGreenGraduationBgMeiryo
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForWhiteFontDarkGreenGraduation(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange

        With .Font
        
            .ThemeColor = xlThemeColorDark1: .TintAndShade = 0
        End With

        With .Interior
        
            ' same as FieldTitleInterior.ColumnNameInteriorOfExcelSheetAdoSQL  ' for Excel book
                    
            .Pattern = xlPatternLinearGradient
            
            With .Gradient
            
                .Degree = 270
                
                With .ColorStops
                
                    .Clear
                
                    With .Add(0): .Color = RGB(41, 52, 32): End With    ' 2110505
                    
                    With .Add(1): .Color = RGB(34, 78, 22): End With    ' 1461794
                End With
            End With
        End With
    End With
End Sub


'''
''' decorate field-title by ManualDecoratingFieldTitlesRangePattern.WhiteFontDarkOrangeGraduationBgMeiryo
'''
Public Sub DecorateFieldTitleOnlyFontColorAndInteriorForWhiteFontDarkOrangeGraduation(ByRef robjFieldTitlesRange As Excel.Range, _
        Optional ByVal vintGradientDegree As Long = 270)

    With robjFieldTitlesRange

        With .Font
        
            .ThemeColor = xlThemeColorDark1: .TintAndShade = 0
        End With

        With .Interior
        
            ' same as FieldTitleInterior.ColumnNameInteriorOfExcelSheetAdoSQL  ' for Excel book
                    
            .Pattern = xlPatternLinearGradient
            
            With .Gradient
            
                .Degree = vintGradientDegree
                
                With .ColorStops
                
                    .Clear
                
                    With .Add(0): .Color = RGB(233, 111, 43): End With    ' 2846697
                    
                    With .Add(1): .Color = RGB(162, 77, 6): End With ' 413090
                End With
            End With
        End With
    End With
End Sub


'**---------------------------------------------
'** About records of data-tables, decorate border and interior and font of Worksheet.Cells
'**---------------------------------------------
'''
''' Decorate fonts and borders about records of data-table
'''
Public Sub DecorateRangeAboutBordersAndFont(ByVal vobjRange As Excel.Range, _
        Optional venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBordersNone, _
        Optional ByVal vsngFontSize As Single = 10, _
        Optional ByVal vblnNoIncludeFieldTitlesRow As Boolean = False, _
        Optional ByVal vblnNoIncludeFieldTitlesColumn As Boolean = True)
    
    With vobjRange
        
        With .Font
            
            .Name = "Arial": .FontStyle = "Regular"
            
            .Size = vsngFontSize
        End With

        .HorizontalAlignment = xlGeneral ' : .VerticalAlignment = xlBottom
        
        DecorateRangeBorders vobjRange, venmRecordCellsBorders, vblnNoIncludeFieldTitlesRow, vblnNoIncludeFieldTitlesColumn
    End With
End Sub

'''
'''
'''
Public Sub DecorateRangeBorders(ByVal vobjRange As Excel.Range, _
        Optional venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBordersNone, _
        Optional ByVal vblnNoIncludeFieldTitlesRow As Boolean = False, _
        Optional ByVal vblnNoIncludeFieldTitlesColumn As Boolean = True)

    Select Case venmRecordCellsBorders
    
        Case RecordCellsBorders.RecordCellsGrayAll
            
            DecorateRecordCellsBordersForGrayAll vobjRange, vblnNoIncludeFieldTitlesRow, vblnNoIncludeFieldTitlesColumn
            
        Case RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal
    
            DecorateRecordCellsBordersForBlacksAndGrayInsideHorizontal vobjRange, vblnNoIncludeFieldTitlesRow, vblnNoIncludeFieldTitlesColumn
    
        Case RecordCellsBorders.RecordCellsBlacksAndGrayInsideVertical  ' for transpose output
        
            DecorateRecordCellsBordersForBlacksAndGrayInsideVertical vobjRange, vblnNoIncludeFieldTitlesRow, vblnNoIncludeFieldTitlesColumn
    End Select
End Sub


'''
'''
'''
Public Sub DecorateRecordCellsBordersForGrayAll(ByRef robjRange As Excel.Range, _
        Optional ByVal vblnNoIncludeFieldTitlesRow As Boolean = False, _
        Optional ByVal vblnNoIncludeFieldTitlesColumn As Boolean = True)

    Dim varBorderType As Variant

    With robjRange
    
        .Borders(xlDiagonalDown).LineStyle = xlNone: .Borders(xlDiagonalUp).LineStyle = xlNone
        
        For Each varBorderType In GetXlBorderIndexColForDataTableRecordArea(vblnNoIncludeFieldTitlesRow, vblnNoIncludeFieldTitlesColumn)
            
            With .Borders(varBorderType)
                
                .LineStyle = xlContinuous
                
                .ThemeColor = 1
                
                .TintAndShade = -0.5
                
                .Weight = xlThin
            End With
        Next
    End With
End Sub

'''
'''
'''
Public Sub DecorateRecordCellsBordersForBlacksAndGrayInsideHorizontal(ByRef robjRange As Excel.Range, _
        Optional ByVal vblnNoIncludeFieldTitlesRow As Boolean = False, _
        Optional ByVal vblnNoIncludeFieldTitlesColumn As Boolean = True)

    Dim varBorderType As Variant

    With robjRange
    
        .Borders(xlDiagonalDown).LineStyle = xlNone: .Borders(xlDiagonalUp).LineStyle = xlNone
        
        For Each varBorderType In GetXlBorderIndexColForDataTableRecordArea(vblnNoIncludeFieldTitlesRow, vblnNoIncludeFieldTitlesColumn, vblnIsInsideHorizontalEnabled:=False)
            
            With .Borders(varBorderType)
                
                .LineStyle = xlContinuous
                
                .ColorIndex = xlAutomatic
                
                .TintAndShade = 0
                
                '.Weight = Excel.XlBorderWeight.xlHairline
                
                '.Weight = Excel.XlBorderWeight.xlMedium
                
                .Weight = xlThin
            End With
        Next
        
        With .Borders(xlInsideHorizontal)
            
            .LineStyle = xlContinuous
            
            .ThemeColor = 1
            
            .TintAndShade = -0.35
            
            .Weight = xlThin
        End With
    End With
End Sub


'''
''' for transpose output
'''
Public Sub DecorateRecordCellsBordersForBlacksAndGrayInsideVertical(ByRef robjRange As Excel.Range, _
        Optional ByVal vblnNoIncludeFieldTitlesRow As Boolean = False, _
        Optional ByVal vblnNoIncludeFieldTitlesColumn As Boolean = True)

    Dim varBorderType As Variant

    With robjRange
    
        .Borders(xlDiagonalDown).LineStyle = xlNone: .Borders(xlDiagonalUp).LineStyle = xlNone
        
        For Each varBorderType In GetXlBorderIndexColForDataTableRecordArea(vblnNoIncludeFieldTitlesRow, vblnNoIncludeFieldTitlesColumn, vblnIsInsideVerticalEnabled:=False)
            
            With .Borders(varBorderType)
                
                .LineStyle = xlContinuous
                
                .ColorIndex = xlAutomatic
                
                .TintAndShade = 0
                
                '.Weight = Excel.XlBorderWeight.xlHairline
                
                '.Weight = Excel.XlBorderWeight.xlMedium
                
                .Weight = xlThin
            End With
        Next
        
        With .Borders(xlInsideVertical)
            
            .LineStyle = xlContinuous
            
            .ThemeColor = 1
            
            .TintAndShade = -0.35
            
            .Weight = xlThin
        End With
    End With
End Sub


'''
'''
'''
Public Function GetCellBorderWeightTxtFromEnm(ByVal venmXlBorderWeight As Excel.XlBorderWeight) As String

    Dim strText As String
    
    strText = ""
    
    Select Case venmXlBorderWeight
    
        Case Excel.XlBorderWeight.xlThin
        
            strText = "xlThin"
            
        Case Excel.XlBorderWeight.xlThick
    
            strText = "xlThick"
            
        Case Excel.XlBorderWeight.xlMedium
        
            strText = "xlMedium"
        Case Excel.XlBorderWeight.xlHairline
    
            strText = "xlHairline"
    End Select

    GetCellBorderWeightTxtFromEnm = strText
End Function

'''
'''
'''
Public Function GetCellBorderLineStyleTxtFromEnm(ByVal venmXlLineStyle As Excel.XlLineStyle) As String

    Dim strText As String
    
    strText = ""
    
    Select Case venmXlLineStyle
    
        Case Excel.XlLineStyle.xlContinuous
        
            strText = "xlContinuous"
            
        Case Excel.XlLineStyle.xlDash
        
            strText = "xlDash"
            
        Case Excel.XlLineStyle.xlDashDot
        
            strText = "xlDashDot"
            
        Case Excel.XlLineStyle.xlDashDotDot
        
            strText = "xlDashDotDot"
            
        Case Excel.XlLineStyle.xlDot
        
            strText = "xlDot"
            
        Case Excel.XlLineStyle.xlDouble
        
            strText = "xlDouble"
            
        Case Excel.XlLineStyle.xlLineStyleNone
        
            strText = "xlLineStyleNone"
            
        Case Excel.XlLineStyle.xlSlantDashDot
        
            strText = "xlSlantDashDot"
    
    End Select

    GetCellBorderLineStyleTxtFromEnm = strText
End Function

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubDecorateFieldTitleFontStyleMeiryo(ByRef robjFieldTitlesRange As Excel.Range)

    With robjFieldTitlesRange.Font
        
        .Name = "Meiryo"
        
        .FontStyle = "Regular"
        
        .Size = 11
    End With
End Sub

