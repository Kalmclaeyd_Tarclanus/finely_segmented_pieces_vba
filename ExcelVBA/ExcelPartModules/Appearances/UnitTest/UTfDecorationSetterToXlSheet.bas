Attribute VB_Name = "UTfDecorationSetterToXlSheet"
'
'   Sanity tests for both DecorationSetterToXlSheet and DataTableSheetOut
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 28/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
#Else
    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
#End If

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
'''
'''
Private Enum LogAutoShapeTypeForCellDecorationsFlag

    NoLogAutoShapeTypeForCellDecorations = 0
    
    LogAutoShapeOfRowColumnAndPositionForCellDecorations = &H1
    
    LogAutoShapeOfFieldTitleDecorations = &H2
    
    LogAutoShapeOfRecordBorderDecorations = &H4

End Enum

'''
'''
'''
Public Enum ReplacingSampleRandomValueOptionFlag

    ReplaceSampleRandomForLeftTopOfTable = &H1
    
    ReplaceSampleRandomForLeftBottomOfTable = &H2

    ReplaceSampleRandomForRightTopOfTable = &H4
    
    ReplaceSampleRandomForRightBottomOfTable = &H3
    
    ReplaceSampleRandomForAllFourCornerOfTable = ReplaceSampleRandomForLeftTopOfTable Or ReplaceSampleRandomForLeftBottomOfTable Or ReplaceSampleRandomForRightTopOfTable Or ReplaceSampleRandomForRightBottomOfTable
End Enum



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub GenerateSimpleTableSheetForSpecifiedBookWithStringParams(ByRef robjBook As Excel.Workbook, _
        ByVal vstrSheetName As String, _
        ByVal vstrTableIntegerParamsOfRowMaxAndColumnMaxAndTopLeftRowIndexAndLeftColumnIndexDelimitedByComma As String, _
        Optional ByVal vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon As String = "")
        
    Dim strInputs() As String
    
    strInputs = Split(vstrTableIntegerParamsOfRowMaxAndColumnMaxAndTopLeftRowIndexAndLeftColumnIndexDelimitedByComma, ",")
    
    GenerateSimpleTableSheetForSpecifiedBook robjBook, vstrSheetName, CInt(Trim(strInputs(0))), CInt(Trim(strInputs(1))), CInt(Trim(strInputs(2))), CInt(Trim(strInputs(3))), vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon
End Sub

'''
'''
'''
Public Sub GenerateSimpleTableSheetForSpecifiedBook(ByRef robjBook As Excel.Workbook, _
        ByVal vstrSheetName As String, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long, _
        Optional ByVal vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon As String = "")

    If vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon <> "" Then

        msubSimpleTableOutputTestWithSpecifyingSheetName robjBook, vstrSheetName, vintRowMax, vintColumnMax, vintTopLeftRowIndex, vintTopLeftColumnIndex, False, WhiteFontDarkOrangeGraduationBgMeiryo, RecordCellsBlacksAndGrayInsideHorizontal, LogAutoShapeOfRowColumnAndPositionForCellDecorations, vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon
    Else
        msubSimpleTableOutputTestWithSpecifyingSheetName robjBook, vstrSheetName, vintRowMax, vintColumnMax, vintTopLeftRowIndex, vintTopLeftColumnIndex
    End If
End Sub

'**---------------------------------------------
'** Create some simplest integer data-tables for comprehensive tests
'**---------------------------------------------
'''
''' About two-dimensional Variant array
'''
Public Sub SetupPadSimpleIntegerForRCTableAndFieldTitles(ByRef rvarRowColumnTable As Variant, ByRef robjFieldTitlesCol As Collection, ByVal vintRowMax As Long, ByVal vintColumnMax As Long)

    ReDim rvarRowColumnTable(1 To vintRowMax, 1 To vintColumnMax)

    msubSetupPadIntegerOnRCTableVariant rvarRowColumnTable

    msubSetupPadIntegerColumnTitleOnFieldTitlesCol robjFieldTitlesCol, vintColumnMax
End Sub

'''
''' About two-dimensional Collection
'''
Public Sub SetupPadSimpleIntegerForRCTableColAndFieldTitles(ByRef robjDTCol As Collection, ByRef robjFieldTitlesCol As Collection, ByVal vintRowMax As Long, ByVal vintColumnMax As Long)

    msubSetupPadIntegerOnRCTableCol robjDTCol, vintRowMax, vintColumnMax
    
    msubSetupPadIntegerColumnTitleOnFieldTitlesCol robjFieldTitlesCol, vintColumnMax
End Sub

'''
''' About two-dimensional Dictionary
'''
Public Sub SetupPadSimpleIntegerForRCTableDicAndFieldTitles(ByRef robjDTDic As Scripting.Dictionary, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long)

    msubSetupPadIntegerOnRCTableDic robjDTDic, vintRowMax, vintColumnMax
    
    msubSetupPadIntegerColumnTitleOnFieldTitlesCol robjFieldTitlesCol, vintColumnMax
End Sub

'''
''' About one-nested Dictionary
'''
Public Sub SetupPadSimpleIntegerForOneNestedDicAndFieldTitles(ByRef robjNestedDTDic As Scripting.Dictionary, _
        ByRef robjFieldTitlesCol As Collection, _
        ByRef rblnIsInterpretedOfSingleDimensionColAsRowData As Boolean, _
        ByVal vintDicCountMax As Long, _
        ByVal vintColumnMax As Long, _
        ByVal vintChildRowMax As Long, _
        Optional ByVal venmOneNestedDicType As OneNestedDicType = OneNestedDicType.NestedDicToDicWithOneDimentionalType)

    msubSetupPadIntegerOnNestedDicWithChildRCTableCol robjNestedDTDic, rblnIsInterpretedOfSingleDimensionColAsRowData, vintDicCountMax, vintColumnMax, vintChildRowMax, venmOneNestedDicType
    
    msubSetupPadIntegerColumnTitleOnFieldTitlesCol robjFieldTitlesCol, vintColumnMax
End Sub

'''
''' About two-dimensional integer-and-string data Collection
'''
Public Sub SetupPadSimpleIntegerAndStringForRCTableColAndFieldTitles(ByRef robjDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        Optional ByVal vintLengthOfChars As Long = 3)


    msubSetupPadIntegerAndStringOnRCTableCol robjDTCol, vintRowMax, vintColumnMax, vintLengthOfChars
    
    msubSetupPadIntegerColumnTitleOnFieldTitlesCol robjFieldTitlesCol, vintColumnMax
End Sub

'''
''' About two-dimensional integer-and-string data Collection
'''
Public Sub SetupPadSimpleIntegerAndStringWithNullColumnsForRCTableColAndFieldTitles(ByRef robjDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        Optional ByVal vintLengthOfChars As Long = 3, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "")


    msubSetupPadIntegerAndStringWithNullColumnsOnRCTableCol robjDTCol, vintRowMax, vintColumnMax, vintLengthOfChars, vstrNullColumnIndexDelimitedComma
    
    msubSetupPadIntegerColumnTitleOnFieldTitlesCol robjFieldTitlesCol, vintColumnMax
End Sub


'''
''' About two-dimensional integer-and-string data Collection
'''
Public Sub SetupPadSimpleIntegerAndStringWithSampleRandomValueAtSpecifiedIndexesAndNullColumnsForRCTableColAndFieldTitles(ByRef robjDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        Optional ByVal vintLengthOfChars As Long = 3, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
        Optional ByVal vobjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic As Scripting.Dictionary, _
        Optional ByVal vintCountOfRandomSampleValuePatternType As Long = 4, _
        Optional ByVal vblnIncludeNullAndEmptyValue As Boolean = False)


    msubSetupPadIntegerAndStringWithNullColumnsOnRCTableCol robjDTCol, _
            vintRowMax, _
            vintColumnMax, _
            vintLengthOfChars, _
            vstrNullColumnIndexDelimitedComma, _
            vobjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic, _
            vintCountOfRandomSampleValuePatternType, _
            vblnIncludeNullAndEmptyValue
    
    msubSetupPadIntegerColumnTitleOnFieldTitlesCol robjFieldTitlesCol, vintColumnMax
End Sub


'''
''' With additional information, about two-dimensional integer-and-string data Collection
'''
Public Sub SetupPadSimpleIntegerAndStringWithNullColumnsForRCTableColAndFieldTitlesWithAdditionalInfos(ByRef robjDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        ByRef robjFieldTitleToColumnDescriptionDic As Scripting.Dictionary, _
        ByRef robjFieldTitleToColumnTypeNameDic As Scripting.Dictionary, _
        ByRef robjFieldTitleToColumnTypeLengthDic As Scripting.Dictionary, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        Optional ByVal vintLengthOfChars As Long = 3, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "")


    msubSetupPadIntegerAndStringWithNullColumnsOnRCTableCol robjDTCol, vintRowMax, vintColumnMax, vintLengthOfChars, vstrNullColumnIndexDelimitedComma
    
    msubSetupPadIntegerColumnTitleOnFieldTitlesCol robjFieldTitlesCol, vintColumnMax
    
    SetupPadAdditionalInfoDicsAboutColumnNames robjFieldTitleToColumnDescriptionDic, robjFieldTitleToColumnTypeNameDic, robjFieldTitleToColumnTypeLengthDic, vintColumnMax
End Sub

'''
''' get random values table
'''
Public Sub SetupPadSampleRandomSomeValueWithNullColumnsOnRCTableAndFieldTitles(ByRef robjDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        Optional ByVal vintCountOfRandomSampleValuePatternType As Long = 4, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
        Optional ByVal vblnAllowToVbaRandomize As Boolean = True, _
        Optional ByVal vblnIncludeNullAndEmptyValue As Boolean = False)


    msubSetupPadSampleRandomSomeValueWithNullColumnsOnRCTable robjDTCol, _
            vintRowMax, _
            vintColumnMax, _
            vintCountOfRandomSampleValuePatternType, _
            vstrNullColumnIndexDelimitedComma, _
            vblnAllowToVbaRandomize, _
            vblnIncludeNullAndEmptyValue
    
    msubSetupPadIntegerColumnTitleOnFieldTitlesCol robjFieldTitlesCol, vintColumnMax
End Sub

'**---------------------------------------------
'** Create data-table region parameters for comprehensive tests
'**---------------------------------------------
'''
'''
'''
Public Sub GetDataTableRegionParamsOfCreateTableOnSheet(ByRef rintRowMax As Long, ByRef rintColumnMax As Long, ByRef rintTopLeftRowIndex As Long, ByRef rintStartColumnIndex As Long, ByRef robjRowCol As Collection)

    With robjRowCol
    
        rintRowMax = .Item(1)
        
        rintColumnMax = .Item(2)
        
        rintTopLeftRowIndex = .Item(3)
        
        rintStartColumnIndex = .Item(4)
    End With
End Sub

'''
'''
'''
'''
Public Function GetDataTableSheetCreateTestConditionPatternsDTCol(ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long, _
        Optional ByVal vblnExceptForSheetLeftTopAddress As Boolean = False)

    Dim objDTCol As Collection, objRowCol As Collection, objTextCol As Collection, varText As Variant
    Dim varLeftSide As Variant, varRightSide As Variant, strLeftDelimitedChar As String, strRightDelimitedChar As String
    
    
    Set objDTCol = New Collection
    
    Set objTextCol = New Collection
    
    If vblnExceptForSheetLeftTopAddress Then
    
        strLeftDelimitedChar = "a,1,;1,b,;a,b,;b,a,"
            
        strRightDelimitedChar = "c,1;1,d;c,d;d,c"
    Else
        strLeftDelimitedChar = "1,1,;a,1,;1,b,;a,b,"
            
        strRightDelimitedChar = "1,1;c,1;1,d;c,d"
    End If
    
    
    For Each varLeftSide In GetColFromLineDelimitedChar(strLeftDelimitedChar, ";")
    
        For Each varRightSide In GetColFromLineDelimitedChar(strRightDelimitedChar, ";")
        
            objTextCol.Add Replace(Replace(Replace(Replace(CStr(varLeftSide & varRightSide), "a", CStr(vintRowMax)), "b", CStr(vintColumnMax)), "c", CStr(vintTopLeftRowIndex)), "d", CStr(vintTopLeftColumnIndex))
        Next
    Next

    For Each varText In objTextCol
    
        objDTCol.Add GetIntegerColFromLineDelimitedChar(varText)
    Next

    Set GetDataTableSheetCreateTestConditionPatternsDTCol = objDTCol
End Function

'''
'''
'''
Public Sub GetDataTableTopLeftWhenCreateTableOnSheet(ByRef rintTopLeftRowIndex As Long, ByRef rintTopLeftColumnIndex As Long, ByRef robjRowCol As Collection)

    With robjRowCol
    
        rintTopLeftRowIndex = .Item(1)
        
        rintTopLeftColumnIndex = .Item(2)
    End With
End Sub


'''
'''
'''
Public Function GetDataTableSheetCreateTestConditionOnlyTopLeftDTCol(ByVal vintTopLeftRowIndex As Long, ByVal vintTopLeftColumnIndex As Long)

    Dim objDTCol As Collection, objTextCol As Collection, varText As Variant

    Set objDTCol = New Collection: Set objTextCol = New Collection

    With objTextCol
    
        .Add "1,1"
        
        .Add CStr(vintTopLeftRowIndex) & ",1"
        
        .Add "1," & CStr(vintTopLeftColumnIndex)
    
        .Add CStr(vintTopLeftRowIndex) & "," & CStr(vintTopLeftColumnIndex)
        
        .Add CStr(vintTopLeftColumnIndex) & "," & CStr(vintTopLeftRowIndex)
    End With


    For Each varText In objTextCol
    
        objDTCol.Add GetIntegerColFromLineDelimitedChar(varText)
    Next

    Set GetDataTableSheetCreateTestConditionOnlyTopLeftDTCol = objDTCol
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Field titles decoration tests of data-tables
'**---------------------------------------------
'''
''' sanity-test for comprehensive ManualDecoratingFieldTitlesRangePattern enumeration-type tests
'''
Private Sub msubSanityTestOfManualDecoratingFieldTitlesRangePatternEnum()

    Dim strBookPath As String, objBook As Excel.Workbook
    Dim varManualDecoratingFieldTitlesRangePattern As Variant
    Dim intRowMax As Long, intColumnMax As Long, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    
    
    intRowMax = 4: intColumnMax = 5: intTopLeftRowIndex = 2: intTopLeftColumnIndex = 2

    strBookPath = mfstrGetSheetDecorationTestBookDir() & "\ComprehensiveSheetDecorationManualDecoratingFieldTitlesRangePatternTests.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    For Each varManualDecoratingFieldTitlesRangePattern In mfobjGetManualDecoratingFieldTitlesRangePatternCol()

        msubSimpleTableOutputTest objBook, intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex, False, varManualDecoratingFieldTitlesRangePattern, RecordCellsGrayAll, LogAutoShapeOfFieldTitleDecorations
    Next

    DeleteDummySheetWhenItExists objBook
End Sub


'''
''' sanity-test for DataTableSheetOut.msubDecorateRangeOfFieldTitles about about FieldTitleInterior
'''
Private Sub msubSanityTestOfFieldTitleInteriorEnum()

    Dim strBookPath As String, objBook As Excel.Workbook
    Dim varFieldTitleInterior As Variant
    Dim intRowMax As Long, intColumnMax As Long, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    
    
    intRowMax = 4: intColumnMax = 5: intTopLeftRowIndex = 2: intTopLeftColumnIndex = 2

    strBookPath = mfstrGetSheetDecorationTestBookDir() & "\ComprehensiveSheetFieldTitleInteriorTests.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    For Each varFieldTitleInterior In mfobjGetFieldTitleInteriorCol()

        msubSimpleTableOutputTestForFieldTitleInterior objBook, intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex, varFieldTitleInterior, RecordCellsGrayAll
    Next

    DeleteDummySheetWhenItExists objBook
End Sub


'''
''' sanity-test for comprehensive RecordCellsBorders enumeration-type tests
'''
Private Sub msubSanityTestOfRecordCellsBordersEnum()

    Dim strBookPath As String, objBook As Excel.Workbook
    Dim intRowMax As Long, intColumnMax As Long, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    
    Dim varRecordCellsBorders As Variant
    
    
    intRowMax = 8: intColumnMax = 4: intTopLeftRowIndex = 2: intTopLeftColumnIndex = 2

    strBookPath = mfstrGetSheetDecorationTestBookDir() & "\ComprehensiveSheetDecorationRecordCellsBorders.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    For Each varRecordCellsBorders In mfobjGetRecordCellsBordersCol()

        msubSimpleTableOutputTest objBook, intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex, False, DarkBrownFontLightSilverBgMeiryo, varRecordCellsBorders, LogAutoShapeOfRecordBorderDecorations
    Next

    DeleteDummySheetWhenItExists objBook
End Sub


'**---------------------------------------------
'** Tests for GetFieldTitleToPositionIndexDic
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestForGetFieldTitleToPositionIndexDic()

    Dim strBookPath As String, objBook As Excel.Workbook
    Dim intRowMax As Long, intColumnMax As Long, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    Dim objCurrentSheet As Excel.Worksheet, objDic As Scripting.Dictionary
    
    
    strBookPath = mfstrGetSheetDecorationTestBookDir() & "\GettingIndexTestAboutGetFieldTitleToPositionIndexDic.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)

    intRowMax = 3: intColumnMax = 5
    
    ' 1st test
    intTopLeftRowIndex = 1: intTopLeftColumnIndex = 1

    msubGetCurrentSheetAndFieldTitleToPositionIndexDicForTesting objDic, objCurrentSheet, objBook, intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex

    OutputDicToAutoShapeLogTextOnSheet objCurrentSheet, objDic, "Key-values result of GetFieldTitleToPositionIndexDic"


    ' 2nd test
    intTopLeftRowIndex = 3: intTopLeftColumnIndex = 3

    msubGetCurrentSheetAndFieldTitleToPositionIndexDicForTesting objDic, objCurrentSheet, objBook, intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex

    OutputDicToAutoShapeLogTextOnSheet objCurrentSheet, objDic, "Key-values result of GetFieldTitleToPositionIndexDic"



    ' 3rd test
    intTopLeftRowIndex = 5: intTopLeftColumnIndex = 5

    msubGetCurrentSheetAndFieldTitleToPositionIndexDicForTesting objDic, objCurrentSheet, objBook, intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex

    OutputDicToAutoShapeLogTextOnSheet objCurrentSheet, objDic, "Key-values result of GetFieldTitleToPositionIndexDic"
   
    
    ' 4th test
    intTopLeftRowIndex = 1: intTopLeftColumnIndex = 5
    
    msubGetCurrentSheetAndFieldTitleToPositionIndexDicForTesting objDic, objCurrentSheet, objBook, intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex

    OutputDicToAutoShapeLogTextOnSheet objCurrentSheet, objDic, "Key-values result of GetFieldTitleToPositionIndexDic"
   
   
    ' 5th test
    intTopLeftRowIndex = 5: intTopLeftColumnIndex = 1

    msubGetCurrentSheetAndFieldTitleToPositionIndexDicForTesting objDic, objCurrentSheet, objBook, intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex

    OutputDicToAutoShapeLogTextOnSheet objCurrentSheet, objDic, "Key-values result of GetFieldTitleToPositionIndexDic"
   
    
    DeleteDummySheetWhenItExists objBook
    
End Sub

'''
'''
'''
Private Sub msubOutputInfoOfCurrentRegion()

    Dim objRange As Excel.Range, objCurrentRegion As Excel.Range
    
    Set objRange = ActiveCell
    
    Set objCurrentRegion = objRange.CurrentRegion
    
    With objCurrentRegion
    
        Debug.Print "TopLeft Row: " & CStr(.Row) & ", TopLeft Column: " & CStr(.Column)
    
        Debug.Print "Count of rows: " & CStr(.Rows.Count) & ", Count of columns: " & CStr(.Columns.Count)
    End With
End Sub


'''
'''
'''
''' <Argument>robjOutputFieldTitleToPositionIndexDic: Output</Argument>
''' <Argument>robjOutputCurrentSheet: Output</Argument>
''' <Argument>robjBook: Input</Argument>
''' <Argument>rintRowMax: Input</Argument>
''' <Argument>rintColumnMax: Input</Argument>
''' <Argument>rintTopLeftRowIndex: Input</Argument>
''' <Argument>rintTopLeftColumnIndex: Input</Argument>
Private Sub msubGetCurrentSheetAndFieldTitleToPositionIndexDicForTesting(ByRef robjOutputFieldTitleToPositionIndexDic As Scripting.Dictionary, _
        ByRef robjOutputCurrentSheet As Excel.Worksheet, _
        ByRef robjBook As Excel.Workbook, _
        ByRef rintRowMax As Long, _
        ByRef rintColumnMax As Long, _
        ByRef rintTopLeftRowIndex As Long, _
        ByRef rintTopLeftColumnIndex As Long)

    Dim objFieldTitlesCol As Collection

    msubSimpleTableOutputTest robjBook, rintRowMax, rintColumnMax, rintTopLeftRowIndex, rintTopLeftColumnIndex, False

    With robjBook.Worksheets
    
        Set robjOutputCurrentSheet = .Item(.Count)
    End With

    GetFieldTitlesBasedOnCurrentRegionFromSheetAndTopLeftIndexes objFieldTitlesCol, robjOutputCurrentSheet, rintTopLeftRowIndex, rintTopLeftColumnIndex

    Set robjOutputFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(objFieldTitlesCol, robjOutputCurrentSheet, rintTopLeftRowIndex, rintTopLeftColumnIndex)
End Sub




'**---------------------------------------------
'** Tests to putting Table out to any Cell positions and read it
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityComprehensiveTestToCreateSampleTableToSheet()

    Dim strBookPath As String
    Dim objBook As Excel.Workbook, intRowMax As Long, intColumnMax As Long, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    Dim objRowCol As Collection, varRowCol As Variant

    
    strBookPath = mfstrGetSheetDecorationTestBookDir() & "\ComprehensiveSheetDecorationTests.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    For Each varRowCol In GetDataTableSheetCreateTestConditionPatternsDTCol(2, 3, 3, 4)

        Set objRowCol = varRowCol

        GetDataTableRegionParamsOfCreateTableOnSheet intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex, objRowCol

        msubSimpleTableOutputTest objBook, intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex, True
    Next

    DeleteDummySheetWhenItExists objBook
End Sub

'''
'''
'''
Private Sub msubSanityComprehensiveTestToCreateSampleRandomSomeValuesTableToSheet()

    Dim strBookPath As String
    Dim objBook As Excel.Workbook, intRowMax As Long, intColumnMax As Long, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    Dim objRowCol As Collection, varRowCol As Variant

    
    strBookPath = GetUnitTestingSheetRandomizedDataBookDir() & "\ComprehensiveSheetRandomizedValuesTests.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    For Each varRowCol In GetDataTableSheetCreateTestConditionPatternsDTCol(7, 12, 3, 4)

        Set objRowCol = varRowCol

        GetDataTableRegionParamsOfCreateTableOnSheet intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex, objRowCol

        GetSampleRandomSomeValuesTableSheetForTest objBook, mfstrGetDefaultSampleSheetName(objBook), intRowMax, intColumnMax, intTopLeftRowIndex, intTopLeftColumnIndex, 4, "", False
    Next

    DeleteDummySheetWhenItExists objBook
End Sub


'''
'''
'''
Private Sub msubSanityTestToCreateSampleTableToSheet()

    Dim varRCTable As Variant, objFieldTitlesCol As Collection, intRowMax As Long, intColumnMax As Long
    Dim strBookPath As String, objBook As Excel.Workbook, objSheet01 As Excel.Worksheet, objSheet02 As Excel.Worksheet
    
    
    intRowMax = 2: intColumnMax = 3
    
    SetupPadSimpleIntegerForRCTableAndFieldTitles varRCTable, objFieldTitlesCol, intRowMax, intColumnMax
    
    strBookPath = mfstrGetSheetDecorationTestBookDir() & "\BasicSheetDecorationTests.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    Set objSheet01 = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    objSheet01.Name = "Sample01"


    OutputVarTableToSheetWithoutAnyDecorationFormatting objSheet01, varRCTable, objFieldTitlesCol, 1, 1

    DecorateGridSimply objSheet01
    
    
    Set objSheet02 = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet02.Name = "Sample02"
    
    OutputVarTableToSheetWithoutAnyDecorationFormatting objSheet02, varRCTable, objFieldTitlesCol, 3, 4
    
    DecorateGridSimply objSheet02, SepiaFontLightToDarkBrownGraduationBgMeiryo
    
    
    DeleteDummySheetWhenItExists objBook
End Sub


'''
'''
'''
Private Sub msubSanitTestToGetSheetCreateTestConditionPatternsDTCol()

    DebugCol GetDataTableSheetCreateTestConditionPatternsDTCol(2, 3, 3, 4)
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>robjBook: Output</Argument>
''' <Argument>vintRowMax: Input</Argument>
''' <Argument>vintColumnMax: Input</Argument>
''' <Argument>vintTopLeftRowIndex: Input</Argument>
''' <Argument>vintTopLeftColumnIndex: Input</Argument>
Private Sub msubSimpleTableOutputTest(ByRef robjBook As Excel.Workbook, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long, _
        Optional ByVal vblnAllowToTestToReadSheetAndCopy As Boolean = False, _
        Optional ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, _
        Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal, _
        Optional ByVal venmLogAutoShapeTypeForCellDecorationsFlag As LogAutoShapeTypeForCellDecorationsFlag = LogAutoShapeTypeForCellDecorationsFlag.LogAutoShapeOfRowColumnAndPositionForCellDecorations)


    Dim strSheetName As String

    strSheetName = mfstrGetDefaultSampleSheetName(robjBook)

    msubSimpleTableOutputTestWithSpecifyingSheetName robjBook, strSheetName, vintRowMax, vintColumnMax, vintTopLeftRowIndex, vintTopLeftColumnIndex, vblnAllowToTestToReadSheetAndCopy, venmManualDecoratingFieldTitlesRangePattern, venmRecordCellsBorders, venmLogAutoShapeTypeForCellDecorationsFlag
End Sub

'''
'''
'''
Public Function GetSampleRandomSomeValuesTableSheetForTest(ByRef robjBook As Excel.Workbook, _
        ByVal vstrSheetName As String, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long, _
        Optional ByVal vintCountOfRandomSampleValuePatternType As Long = 4, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
        Optional ByVal vblnAllowToVbaRandomize As Boolean = True) As Excel.Worksheet


    Dim objSheet As Excel.Worksheet, objDTCol As VBA.Collection, objFieldTitlesCol As VBA.Collection

    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(robjBook)
    
    objSheet.Name = vstrSheetName

    SetupPadSampleRandomSomeValueWithNullColumnsOnRCTableAndFieldTitles objDTCol, objFieldTitlesCol, vintRowMax, vintColumnMax, vintCountOfRandomSampleValuePatternType, vstrNullColumnIndexDelimitedComma, vblnAllowToVbaRandomize

    OutputColToSheet objSheet, objDTCol, objFieldTitlesCol, GetDataTableSheetFormatterOfSampleRandomSomeValueTable(vintTopLeftRowIndex, vintTopLeftColumnIndex)
    
    Set GetSampleRandomSomeValuesTableSheetForTest = objSheet
End Function

'''
'''
'''
Public Function GetTransposeSampleRandomSomeValuesTableSheetForTest(ByRef robjBook As Excel.Workbook, _
        ByVal vstrSheetName As String, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long, _
        Optional ByVal vintCountOfRandomSampleValuePatternType As Long = 4, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
        Optional ByVal vblnAllowToVbaRandomize As Boolean = True) As Excel.Worksheet


    Dim objSheet As Excel.Worksheet, objDTCol As VBA.Collection, objFieldTitlesCol As VBA.Collection

    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(robjBook)
    
    objSheet.Name = vstrSheetName

    SetupPadSampleRandomSomeValueWithNullColumnsOnRCTableAndFieldTitles objDTCol, objFieldTitlesCol, vintRowMax, vintColumnMax, vintCountOfRandomSampleValuePatternType, vstrNullColumnIndexDelimitedComma, vblnAllowToVbaRandomize

    TransposeOutputColToSheet objSheet, objDTCol, objFieldTitlesCol, GetTransposeDataTableSheetFormatterOfSampleRandomSomeValueTable(vintTopLeftRowIndex, vintTopLeftColumnIndex)
    
    Set GetTransposeSampleRandomSomeValuesTableSheetForTest = objSheet
End Function

'''
'''
'''
Public Function GetDataTableSheetFormatterOfSampleRandomSomeValueTable(ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long) As DataTableSheetFormatter

    Dim objDataTableSheetFormatter As DataTableSheetFormatter: Set objDataTableSheetFormatter = New DataTableSheetFormatter
    
    With objDataTableSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
    
        .RecordBordersType = RecordCellsGrayAll
        
        .TopLeftRowIndex = vintTopLeftRowIndex
        
        .TopLeftColumnIndex = vintTopLeftColumnIndex
        
    End With

    Set GetDataTableSheetFormatterOfSampleRandomSomeValueTable = objDataTableSheetFormatter
End Function

'''
'''
'''
Public Function GetTransposeDataTableSheetFormatterOfSampleRandomSomeValueTable(ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long) As TrDataTableSheetFormatter

    Dim objTrDataTableSheetFormatter As TrDataTableSheetFormatter: Set objTrDataTableSheetFormatter = New TrDataTableSheetFormatter
    
    With objTrDataTableSheetFormatter.TransposeDataTableSheetFormattingInterface
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
    
        .RecordBordersType = RecordCellsGrayAll
        
        .TopLeftRowIndex = vintTopLeftRowIndex
        
        .TopLeftColumnIndex = vintTopLeftColumnIndex
        
        Set .InfoTypeToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FieldTitlesColumn,8,DataColumn,9")
    End With

    Set GetTransposeDataTableSheetFormatterOfSampleRandomSomeValueTable = objTrDataTableSheetFormatter
End Function


'''
'''
'''
''' <Argument>robjBook: Output</Argument>
''' <Argument>vstrSheetName: Input</Argument>
''' <Argument>vintRowMax: Input</Argument>
''' <Argument>vintColumnMax: Input</Argument>
''' <Argument>vintTopLeftRowIndex: Input</Argument>
''' <Argument>vintTopLeftColumnIndex: Input</Argument>
''' <Argument>vblnAllowToTestToReadSheetAndCopy: Input - If it is true, test the GetRCVariantTableFromSheet procedure in DataTableSheetIn.bas</Argument>
''' <Argument>venmManualDecoratingFieldTitlesRangePattern: Input</Argument>
''' <Argument>venmRecordCellsBorders: Input</Argument>
''' <Argument>venmLogAutoShapeTypeForCellDecorationsFlag: Input</Argument>
''' <Argument>vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon: Input</Argument>
Private Sub msubSimpleTableOutputTestWithSpecifyingSheetName(ByRef robjBook As Excel.Workbook, _
        ByVal vstrSheetName As String, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long, _
        Optional ByVal vblnAllowToTestToReadSheetAndCopy As Boolean = False, _
        Optional ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, _
        Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal, _
        Optional ByVal venmLogAutoShapeTypeForCellDecorationsFlag As LogAutoShapeTypeForCellDecorationsFlag = LogAutoShapeTypeForCellDecorationsFlag.LogAutoShapeOfRowColumnAndPositionForCellDecorations, _
        Optional ByVal vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon As String = "")

    Dim objSheet As Excel.Worksheet, strLog As String
    Dim varRCTable As Variant, objFieldTitlesCol As Collection
    
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(robjBook)
    
    objSheet.Name = vstrSheetName
    

    SetupPadSimpleIntegerForRCTableAndFieldTitles varRCTable, objFieldTitlesCol, vintRowMax, vintColumnMax

    OutputVarTableToSheetWithoutAnyDecorationFormatting objSheet, varRCTable, objFieldTitlesCol, vintTopLeftRowIndex, vintTopLeftColumnIndex

    If vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon <> "" Then
    
        msubChangeSheetCellValueFromInfoString objSheet, vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon
    End If

    DecorateGridSimply objSheet, venmManualDecoratingFieldTitlesRangePattern, venmRecordCellsBorders
    
    If vblnAllowToTestToReadSheetAndCopy Then
    
        msubSimpleTableCopyTest objSheet, vintTopLeftRowIndex, vintTopLeftColumnIndex
    End If
    
    msubAddAutoShapeDecorationTestLogs objSheet, vintRowMax, vintColumnMax, vintTopLeftRowIndex, vintTopLeftColumnIndex, venmManualDecoratingFieldTitlesRangePattern, venmRecordCellsBorders, venmLogAutoShapeTypeForCellDecorationsFlag
End Sub

'''
'''
'''
Private Function mfstrGetDefaultSampleSheetName(ByRef robjBook As Excel.Workbook, Optional ByVal vstrSheetName As String = "") As String

    Dim strSheetName As String

    If vstrSheetName = "" Then
    
        strSheetName = FindNewSheetNameWhenAlreadyExist("Sample" & Format(robjBook.Worksheets.Count - 1, "000"), robjBook)
    Else
        strSheetName = FindNewSheetNameWhenAlreadyExist(vstrSheetName, robjBook)
    End If

    mfstrGetDefaultSampleSheetName = strSheetName
End Function


'''
''' change Cell values specified from the vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon information
'''
''' <Argument>robjSheet: Output</Argument>
''' <Argument>vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon: Input</Argument>
Private Sub msubChangeSheetCellValueFromInfoString(ByRef robjSheet As Excel.Worksheet, _
        ByVal vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon As String)

    Dim strInfos() As String, i As Long, intRowIndex As Long, intColumnIndex As Long, varValue As Variant
    
    strInfos = Split(vstrAfterChangeSheetValueFromStringInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon, ";")

    i = 0
    
    Do
        If (i + 2) <= UBound(strInfos) Then
        
            intRowIndex = CInt(Trim(strInfos(i)))
        
            intColumnIndex = CInt(Trim(strInfos(i + 1)))
            
            varValue = GetVariantValueFromGeneralStringInformation(Trim(strInfos(i + 2)))
            
            robjSheet.Cells(intRowIndex, intColumnIndex) = varValue
        End If
    
        i = i + 3
        
    Loop While i <= UBound(strInfos)
End Sub



'''
''' test for FieldTitleInterior
'''
Private Sub msubSimpleTableOutputTestForFieldTitleInterior(ByRef robjBook As Excel.Workbook, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        ByVal vintTopLeftRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long, _
        Optional ByVal venmFieldTitleInterior As FieldTitleInterior = FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQL, _
        Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal)

    Dim objSheet As Excel.Worksheet, strSheetName As String, strLog As String
    Dim varRCTable As Variant, objFieldTitlesCol As Collection
    
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(robjBook)
    
    strSheetName = "SampleRDB" & Format(robjBook.Worksheets.Count - 1, "000")
    
    objSheet.Name = strSheetName

    SetupPadSimpleIntegerForRCTableAndFieldTitles varRCTable, objFieldTitlesCol, vintRowMax, vintColumnMax

    OutputVarTableToSheetWithoutAnyDecorationFormatting objSheet, varRCTable, objFieldTitlesCol, vintTopLeftRowIndex, vintTopLeftColumnIndex



    DecorateDataTableSimply objSheet, venmFieldTitleInterior, venmRecordCellsBorders
    

    strLog = mfstrGetLogOfFieldTitleInteriorTests(venmFieldTitleInterior)
    
    AddAutoShapeGeneralMessage objSheet, strLog
End Sub

'''
'''
'''
Private Sub msubAddAutoShapeDecorationTestLogs(ByRef robjSheet As Excel.Worksheet, _
        ByRef rintRowMax As Long, _
        ByRef rintColumnMax As Long, _
        ByRef rintTopLeftRowIndex As Long, _
        ByRef rintStartColumnIndex As Long, _
        ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern, _
        ByVal venmRecordCellsBorders As RecordCellsBorders, _
        ByVal venmLogAutoShapeTypeForCellDecorationsFlag As LogAutoShapeTypeForCellDecorationsFlag)

    Dim strLog As String

    If (venmLogAutoShapeTypeForCellDecorationsFlag And LogAutoShapeOfRowColumnAndPositionForCellDecorations) > 0 Then
    
        strLog = "Simple table generation tests" & vbNewLine & mfstrGetLogOfParamsToCreateTableOnSheet(rintRowMax, rintColumnMax, rintTopLeftRowIndex, rintStartColumnIndex)
        
        AddAutoShapeGeneralMessage robjSheet, strLog
    End If


    If (venmLogAutoShapeTypeForCellDecorationsFlag And LogAutoShapeOfFieldTitleDecorations) > 0 Then
    
        strLog = mfstrGetLogOfManualDecoratingFieldTitlesRangePattern(venmManualDecoratingFieldTitlesRangePattern)
        
        AddAutoShapeGeneralMessage robjSheet, strLog
    End If

    If (venmLogAutoShapeTypeForCellDecorationsFlag And LogAutoShapeOfRecordBorderDecorations) > 0 Then
    
        strLog = mfstrGetLogOfRecordCellsBorders(venmRecordCellsBorders)
        
        AddAutoShapeGeneralMessage robjSheet, strLog
    End If
End Sub



'''
'''
'''
Private Function mfstrGetLogOfManualDecoratingFieldTitlesRangePattern(ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern) As String

    Dim strLog As String
    
    strLog = "Simple field-title ManualDecoratingFieldTitlesRangePattern decoration tests" & vbNewLine
    
    strLog = strLog & "Field-titles decoration type name: " & GetManualDecoratingFieldTitlesRangePatternTextFromEnm(venmManualDecoratingFieldTitlesRangePattern)

    mfstrGetLogOfManualDecoratingFieldTitlesRangePattern = strLog
End Function

'''
'''
'''
Private Function mfstrGetLogOfFieldTitleInteriorTests(ByVal venmFieldTitleInterior As FieldTitleInterior) As String

    Dim strLog As String
    
    strLog = "Simple field-title FieldTitleInterior decoration tests" & vbNewLine
    
    strLog = strLog & "Field-titles decoration type name: " & GetFieldTitleInteriorTextFromEnm(venmFieldTitleInterior)

    mfstrGetLogOfFieldTitleInteriorTests = strLog
End Function

'''
'''
'''
Private Function mfstrGetLogOfRecordCellsBorders(ByVal venmRecordCellsBorders As RecordCellsBorders) As String

    Dim strLog As String
    
    strLog = "Simple records area RecordCellsBorders decoration tests" & vbNewLine
    
    strLog = strLog & "Record area decoration type name: " & GetRecordBordersTextFromEnm(venmRecordCellsBorders)

    mfstrGetLogOfRecordCellsBorders = strLog
End Function


'''
'''
'''
Private Function mfstrGetLogOfParamsToCreateTableOnSheet(ByRef rintRowMax As Long, ByRef rintColumnMax As Long, ByRef rintTopLeftRowIndex As Long, ByRef rintStartColumnIndex As Long) As String

    Dim strLog As String
    
    strLog = "Row count of table: " & CStr(rintRowMax) & vbNewLine
    
    strLog = strLog & "Column count of table: " & CStr(rintColumnMax) & vbNewLine
    
    strLog = strLog & "Field start-row index on sheet: " & CStr(rintTopLeftRowIndex) & vbNewLine
    
    strLog = strLog & "Left start column index on sheet: " & CStr(rintStartColumnIndex) & vbNewLine
    
    strLog = strLog & "Logged time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")

    mfstrGetLogOfParamsToCreateTableOnSheet = strLog
End Function



'''
''' Test of the GetRCVariantTableFromSheet procedure in DataTableSheetIn.bas
'''
Private Sub msubSimpleTableCopyTest(ByRef robjPreviousSheet As Excel.Worksheet, ByVal vintTopLeftRowIndex As Long, ByVal vintTopLeftColumnIndex As Long)

    Dim objBook As Excel.Workbook, objSheet As Excel.Worksheet, strSheetName As String, strLog As String
    Dim varRCValues As Variant, objFieldTitlesCol As Collection
    
    Const intRowShift As Long = 2, intColumnShift As Long = 2
    
    Set objBook = robjPreviousSheet.Parent
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    strSheetName = "DTCopy" & Format(objBook.Worksheets.Count - 1, "000")
    
    objSheet.Name = strSheetName
    
    ' DataTableSheetIn module, GetRCVariantTableFromSheet procedure test
    
    GetRCVariantTableFromSheet varRCValues, objFieldTitlesCol, robjPreviousSheet, vintTopLeftRowIndex, vintTopLeftColumnIndex


    OutputVarTableToSheetWithoutAnyDecorationFormatting objSheet, varRCValues, objFieldTitlesCol, vintTopLeftRowIndex + intRowShift, vintTopLeftColumnIndex + intColumnShift

    DecorateGridSimply objSheet

    strLog = "Data-table sheet reading tests" & vbNewLine & mfstrGetLogOfParamsToCreateTableOnSheet(UBound(varRCValues, 1), UBound(varRCValues, 2), vintTopLeftRowIndex + intRowShift, vintTopLeftColumnIndex + intColumnShift)
    
    AddAutoShapeGeneralMessage objSheet, strLog
End Sub



'**---------------------------------------------
'** Prepare sample data-table
'**---------------------------------------------
'''
'''
'''
Private Sub msubSetupPadIntegerOnRCTableCol(ByRef robjDTCol As Collection, ByVal vintRowMax As Long, ByVal vintColumnMax As Long)

    Dim objRowCol As Collection, i As Long, j As Long, intCount As Long
    
    If robjDTCol Is Nothing Then Set robjDTCol = New Collection
    
    intCount = 1
    
    For i = 1 To vintRowMax
    
        If vintColumnMax = 1 Then
        
            robjDTCol.Add intCount
        
            intCount = intCount + 1
        Else
            Set objRowCol = New Collection
        
            For j = 1 To vintColumnMax
            
                objRowCol.Add intCount
                
                intCount = intCount + 1
            Next
            
            robjDTCol.Add objRowCol
        End If
    Next
End Sub

'''
'''
'''
Private Sub msubSetupPadIntegerAndStringOnRCTableCol(ByRef robjDTCol As Collection, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        Optional ByVal vintLengthOfChars As Long = 3)

    Dim objRowCol As Collection, i As Long, j As Long, intCount As Long, intCharIndex As Long
    
    Const intModControlValueOfStringColumn As Long = 2
    
    If robjDTCol Is Nothing Then Set robjDTCol = New Collection
    
    intCount = 1
    
    intCharIndex = 1
    
    For i = 1 To vintRowMax
    
        If vintColumnMax = 1 Then
        
            If (intCount Mod intModControlValueOfStringColumn) = 0 Then
            
                robjDTCol.Add mfstrGetSimpleChars(intCharIndex, vintLengthOfChars)
                
                intCharIndex = intCharIndex + vintLengthOfChars
            Else
                robjDTCol.Add intCount
            End If
        
            intCount = intCount + 1
        Else
    
            Set objRowCol = New Collection
        
            For j = 1 To vintColumnMax
            
                If (j Mod intModControlValueOfStringColumn) = 0 Then
            
                    objRowCol.Add mfstrGetSimpleChars(intCharIndex, vintLengthOfChars)
                
                    intCharIndex = intCharIndex + vintLengthOfChars
                Else
                    objRowCol.Add intCount
                End If
                
                intCount = intCount + 1
            Next
            
            robjDTCol.Add objRowCol
        End If
    Next
End Sub



'''
'''
'''
Public Sub AddIndexesOfReplacingSampleRandomeValueByReplacingSampleRandomValueOptionFlagToNestedDic(ByVal venmReplacingSampleRandomValueOptionFlag As ReplacingSampleRandomValueOptionFlag, _
        ByVal vintRowMax As Long, _
        ByRef vintColumnMax As Long, _
        ByRef robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic As Scripting.Dictionary)
        

    If (venmReplacingSampleRandomValueOptionFlag And ReplaceSampleRandomForLeftTopOfTable) = ReplaceSampleRandomForLeftTopOfTable Then
    
        AddIndexesOfReplacingSampleRandomeValueToNestedDic 1, 1, robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic
    End If
    
    If (venmReplacingSampleRandomValueOptionFlag And ReplaceSampleRandomForLeftBottomOfTable) = ReplaceSampleRandomForLeftBottomOfTable Then
    
        AddIndexesOfReplacingSampleRandomeValueToNestedDic vintRowMax, 1, robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic
    End If
    
    If (venmReplacingSampleRandomValueOptionFlag And ReplaceSampleRandomForRightTopOfTable) = ReplaceSampleRandomForRightTopOfTable Then
    
        AddIndexesOfReplacingSampleRandomeValueToNestedDic 1, vintColumnMax, robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic
    End If
    
    If (venmReplacingSampleRandomValueOptionFlag And ReplaceSampleRandomForRightBottomOfTable) = ReplaceSampleRandomForRightBottomOfTable Then
    
        AddIndexesOfReplacingSampleRandomeValueToNestedDic vintRowMax, vintColumnMax, robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic
    End If
End Sub

'''
'''
'''
Public Sub AddIndexesOfReplacingSampleRandomeValueToNestedDic(ByRef rintRowIndex As Long, _
        ByRef rintColumnIndex As Long, _
        ByRef robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic As Scripting.Dictionary)

    Dim objInnerDic As Scripting.Dictionary

    If robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic Is Nothing Then Set robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic = New Scripting.Dictionary


    With robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic
    
        If Not .Exists(rintRowIndex) Then
        
            Set objInnerDic = New Scripting.Dictionary
        
            .Add rintRowIndex, objInnerDic
        Else
            Set objInnerDic = .Item(rintRowIndex)
        End If
    
        With objInnerDic
        
            If Not .Exists(rintColumnIndex) Then
            
                .Add rintColumnIndex, 0
            
                Set robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic.Item(rintRowIndex) = objInnerDic
            End If
        End With
    End With
End Sub




'''
''' for compress null-columns test, in ADOExSheetOut.bas
'''
''' <Argument>robjDTCol: Output - Collection(Of Collection(Of Value))</Argument>
''' <Argument>vintRowMax: Input</Argument>
''' <Argument>vintColumnMax: Input</Argument>
''' <Argument>vintLengthOfChars: Input</Argument>
''' <Argument>vstrNullColumnIndexDelimitedComma: Input</Argument>
Private Sub msubSetupPadIntegerAndStringWithNullColumnsOnRCTableCol(ByRef robjDTCol As Collection, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        Optional ByVal vintLengthOfChars As Long = 3, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
        Optional ByVal vobjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic As Scripting.Dictionary, _
        Optional ByVal vintCountOfRandomSampleValuePatternType As Long = 4, _
        Optional ByVal vblnIncludeNullAndEmptyValue As Boolean = False)

    Dim objRowCol As Collection, i As Long, j As Long, intCount As Long, intCharIndex As Long
    Dim objNullColumnIndexKeyDic As Scripting.Dictionary
    Dim varNumber As Variant
    
    Dim blnNeedToCheckReplacingToRandomValue As Boolean
    
    
    blnNeedToCheckReplacingToRandomValue = False
    
    If Not vobjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic Is Nothing Then
    
        If vobjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic.Count > 0 Then
        
            blnNeedToCheckReplacingToRandomValue = True
        End If
    End If
    
    
    Set objNullColumnIndexKeyDic = mfobjGetNullColumnIndexKeyDic(vstrNullColumnIndexDelimitedComma)
    
    Const intModControlValueOfStringColumn As Long = 2
    
    If robjDTCol Is Nothing Then Set robjDTCol = New Collection
    
    intCount = 1
    
    intCharIndex = 1
    
    For i = 1 To vintRowMax
    
        If vintColumnMax = 1 Then
        
            msubPadIntegerAndStringForOneColumn robjDTCol, i, intCount, intCharIndex, intModControlValueOfStringColumn, vintLengthOfChars, _
                    blnNeedToCheckReplacingToRandomValue, _
                    vobjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic, _
                    vintCountOfRandomSampleValuePatternType, _
                    vblnIncludeNullAndEmptyValue
        Else
            Set objRowCol = New Collection
        
            For j = 1 To vintColumnMax
            
                msubPadIntegerAndStringForMoreThanOneColumn objRowCol, _
                    i, j, _
                    intCount, _
                    intCharIndex, _
                    intModControlValueOfStringColumn, _
                    vintLengthOfChars, _
                    objNullColumnIndexKeyDic, _
                    blnNeedToCheckReplacingToRandomValue, _
                    vobjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic, _
                    vintCountOfRandomSampleValuePatternType, _
                    vblnIncludeNullAndEmptyValue
            Next
            
            robjDTCol.Add objRowCol
        End If
    Next
End Sub

'''
'''
'''
Private Sub msubPadIntegerAndStringForOneColumn(ByRef robjDTCol As Collection, _
        ByRef rintRowIndex As Long, _
        ByRef rintCount As Long, _
        ByRef rintCharIndex As Long, _
        ByRef rintModControlValueOfStringColumn As Long, _
        ByRef rintLengthOfChars As Long, _
        ByRef rblnNeedToCheckReplacingToRandomValue As Boolean, _
        Optional ByVal vobjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic As Scripting.Dictionary, _
        Optional ByVal vintCountOfRandomSampleValuePatternType As Long = 4, _
        Optional ByVal vblnIncludeNullAndEmptyValue As Boolean = False)

    Dim blnIsRandomValueNeeded As Boolean
    
    blnIsRandomValueNeeded = False
    
    If rblnNeedToCheckReplacingToRandomValue Then
    
        If mfblnIsRandomValueNeeded(rintRowIndex, 1, vobjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic) Then
        
            blnIsRandomValueNeeded = True
        End If
    End If


    If blnIsRandomValueNeeded Then
    
        robjDTCol.Add mfvarGetSomeSampleValueFromPatternCount(vintCountOfRandomSampleValuePatternType, vblnIncludeNullAndEmptyValue)
    Else
        If (rintCount Mod rintModControlValueOfStringColumn) = 0 Then
        
            robjDTCol.Add mfstrGetSimpleChars(rintCharIndex, rintLengthOfChars)
            
            rintCharIndex = rintCharIndex + rintLengthOfChars
        Else
            robjDTCol.Add rintCount
        End If
    End If

    rintCount = rintCount + 1
End Sub


'''
'''
'''
Private Sub msubPadIntegerAndStringForMoreThanOneColumn(ByRef robjRowCol As Collection, _
        ByRef rintRowIndex As Long, _
        ByRef rintColumnIndex As Long, _
        ByRef rintCount As Long, _
        ByRef rintCharIndex As Long, _
        ByRef rintModControlValueOfStringColumn As Long, _
        ByRef rintLengthOfChars As Long, _
        ByRef robjNullColumnIndexKeyDic As Scripting.Dictionary, _
        ByRef rblnNeedToCheckReplacingToRandomValue As Boolean, _
        Optional ByVal vobjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic As Scripting.Dictionary, _
        Optional ByVal vintCountOfRandomSampleValuePatternType As Long = 4, _
        Optional ByVal vblnIncludeNullAndEmptyValue As Boolean = False)


    Dim blnIsRandomValueNeeded As Boolean

    If robjNullColumnIndexKeyDic.Exists(rintColumnIndex) Then
    
        If (rintColumnIndex Mod rintModControlValueOfStringColumn) = 0 Then
        
            rintCharIndex = rintCharIndex + rintLengthOfChars
        End If
    
        robjRowCol.Add Null
    Else
        blnIsRandomValueNeeded = False
        
        If rblnNeedToCheckReplacingToRandomValue Then
        
            If mfblnIsRandomValueNeeded(rintRowIndex, rintColumnIndex, vobjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic) Then
            
                blnIsRandomValueNeeded = True
            End If
        End If
        
    
        If blnIsRandomValueNeeded Then
        
            If (rintColumnIndex Mod rintModControlValueOfStringColumn) = 0 Then
            
                rintCharIndex = rintCharIndex + rintLengthOfChars
            End If
        
            robjRowCol.Add mfvarGetSomeSampleValueFromPatternCount(vintCountOfRandomSampleValuePatternType, vblnIncludeNullAndEmptyValue)
        Else
            If (rintColumnIndex Mod rintModControlValueOfStringColumn) = 0 Then
        
                robjRowCol.Add mfstrGetSimpleChars(rintCharIndex, rintLengthOfChars)
            
                rintCharIndex = rintCharIndex + rintLengthOfChars
            Else
                robjRowCol.Add rintCount
            End If
        End If
    End If
    
    rintCount = rintCount + 1
End Sub

'''
'''
'''
Private Function mfblnIsRandomValueNeeded(ByRef rintRowIndex As Long, _
        ByRef rintColumnIndex As Long, _
        ByRef robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic As Scripting.Dictionary)

    Dim blnIsNeeded As Boolean

    Dim objInnerDic As Scripting.Dictionary


    blnIsNeeded = False
    
    With robjReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic
    
        If .Exists(rintRowIndex) Then
        
            Set objInnerDic = .Item(rintRowIndex)
        
            With objInnerDic
            
                If .Exists(rintColumnIndex) Then
            
                    blnIsNeeded = True
                End If
            End With
        End If
    End With

    mfblnIsRandomValueNeeded = blnIsNeeded
End Function

'''
'''
'''
Private Sub msubSetupPadSampleRandomSomeValueWithNullColumnsOnRCTable(ByRef robjDTCol As Collection, _
        ByVal vintRowMax As Long, _
        ByVal vintColumnMax As Long, _
        Optional ByVal vintCountOfRandomSampleValuePatternType As Long = 4, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
        Optional ByVal vblnAllowToVbaRandomize As Boolean = True, _
        Optional ByVal vblnIncludeNullAndEmptyValue As Boolean = False)

    Dim i As Long, j As Long, objRowCol As Collection, objNullColumnIndexKeyDic As Scripting.Dictionary
    

    If robjDTCol Is Nothing Then Set robjDTCol = New VBA.Collection

    Set objNullColumnIndexKeyDic = mfobjGetNullColumnIndexKeyDic(vstrNullColumnIndexDelimitedComma)

    If vblnAllowToVbaRandomize Then
    
        VBA.Math.Randomize
    End If

    For i = 1 To vintRowMax
    
        If vintColumnMax = 1 Then
        
            robjDTCol.Add mfvarGetSomeSampleValueFromPatternCount(vintCountOfRandomSampleValuePatternType, vblnIncludeNullAndEmptyValue)
        Else
            Set objRowCol = New Collection
        
            For j = 1 To vintColumnMax
            
                If objNullColumnIndexKeyDic.Exists(j) Then
                
                    objRowCol.Add Null
                Else
                    objRowCol.Add mfvarGetSomeSampleValueFromPatternCount(vintCountOfRandomSampleValuePatternType, vblnIncludeNullAndEmptyValue)
                End If
            Next
        
            robjDTCol.Add objRowCol
        End If
    Next
End Sub

'''
'''
'''
Private Function mfvarGetSomeSampleValueFromPatternCount(ByRef rintCountOfValuePatternType As Long, _
        Optional ByVal vblnIncludeNullAndEmptyValue As Boolean = False) As Variant

    mfvarGetSomeSampleValueFromPatternCount = mfvarGetSomeSampleValueFrom(GetRandomInteger(0, rintCountOfValuePatternType - 1), vblnIncludeNullAndEmptyValue)
End Function


'''
'''
'''
Private Function mfvarGetSomeSampleValueFrom(ByRef rintIndex As Long, _
        Optional ByVal vblnIncludeNullAndEmptyValue As Boolean = False) As Variant

    Dim varRet As Variant

    Select Case rintIndex
    
        Case 0
        
            varRet = 5
        Case 1
        
            varRet = "abcd"
        Case 2
        
            If Not vblnIncludeNullAndEmptyValue Then
        
                varRet = 12
            Else
                varRet = Null
            End If
        Case 3
        
            If Not vblnIncludeNullAndEmptyValue Then
            
                varRet = "DEFG"
            Else
                varRet = Empty
            End If
        Case 4
        
            varRet = 5.827
        Case 5
        
            varRet = CDate("2024/2/10 07:32:16")
        Case 6
        
            varRet = 0.002845
        Case 7
            
            varRet = "Func025Ex"
    End Select

    mfvarGetSomeSampleValueFrom = varRet
End Function


'''
'''
'''
Private Function mfobjGetNullColumnIndexKeyDic(ByVal vstrNullColumnIndexDelimitedComma As String) As Scripting.Dictionary

    Dim objNullColumnIndexKeyDic As Scripting.Dictionary, varNumber As Variant


    Set objNullColumnIndexKeyDic = New Scripting.Dictionary
    
    If vstrNullColumnIndexDelimitedComma <> "" Then
    
        For Each varNumber In Split(vstrNullColumnIndexDelimitedComma, ",")
    
            If IsNumeric(varNumber) Then
            
                objNullColumnIndexKeyDic.Add CLng(varNumber), Nothing
            End If
        Next
    End If

    Set mfobjGetNullColumnIndexKeyDic = objNullColumnIndexKeyDic
End Function


'''
'''
'''
Private Sub msubSetupPadIntegerOnRCTableDic(ByRef robjDTDic As Scripting.Dictionary, ByVal vintRowMax As Long, ByVal vintColumnMax As Long)

    Dim objRowCol As Collection, i As Long, j As Long, intCount As Long, intColumnMax As Long
    Dim intKey As Long
    
    If robjDTDic Is Nothing Then Set robjDTDic = New Scripting.Dictionary
    
    If vintColumnMax < 2 Then
    
        intColumnMax = 2
    Else
        intColumnMax = vintColumnMax
    End If
    
    intCount = 1
    
    For i = 1 To vintRowMax
    
        If intColumnMax = 2 Then
        
            robjDTDic.Add intCount, intCount + 1
        
            intCount = intCount + 2
        Else
            intKey = intCount
            
            intCount = intCount + 1
    
            Set objRowCol = New Collection
        
            For j = 1 To intColumnMax - 1
            
                objRowCol.Add intCount
                
                intCount = intCount + 1
            Next
            
            robjDTDic.Add intKey, objRowCol
        End If
    Next
End Sub

'''
'''
'''
Private Sub msubSetupPadIntegerOnRCTableVariant(ByRef rvarRowColumnTable As Variant)

    Dim i As Long, j As Long, intValue As Long
    
    intValue = 1
    
    For i = LBound(rvarRowColumnTable, 1) To UBound(rvarRowColumnTable, 1)
    
        For j = LBound(rvarRowColumnTable, 2) To UBound(rvarRowColumnTable, 2)
        
            rvarRowColumnTable(i, j) = intValue
        
            intValue = intValue + 1
        Next
    Next
End Sub

'''
'''
'''
''' <Argument>robjFieldTitlesCol: Output</Argument>
''' <Argument>vintColumnMax: Input</Argument>
Private Sub msubSetupPadIntegerColumnTitleOnFieldTitlesCol(ByRef robjFieldTitlesCol As Collection, ByVal vintColumnMax As Long)

    Dim i As Long, strTitle As String
    
    If robjFieldTitlesCol Is Nothing Then Set robjFieldTitlesCol = New Collection
    
    For i = 1 To vintColumnMax

        strTitle = "Column" & CStr(i)
        
        robjFieldTitlesCol.Add strTitle
    Next
End Sub

'''
''' for one-nested dictionary testing data
'''
''' <Argument>robjDTCol: Output - Collection(Of Collection(Of Value))</Argument>
''' <Argument>rblnIsInterpretedOfSingleDimensionColAsRowData: Output</Argument>
''' <Argument>vintDicCountMax: Input</Argument>
''' <Argument>vintColumnMax: Input</Argument>
''' <Argument>vintChildRowMax: Input</Argument>
''' <Argument>venmOneNestedDicType: Input</Argument>
Private Sub msubSetupPadIntegerOnNestedDicWithChildRCTableCol(ByRef robjNestedDic As Scripting.Dictionary, _
        ByRef rblnIsInterpretedOfSingleDimensionColAsRowData As Boolean, _
        ByVal vintDicCountMax As Long, _
        ByVal vintColumnMax As Long, _
        ByVal vintChildRowMax As Long, _
        Optional ByVal venmOneNestedDicType As OneNestedDicType = OneNestedDicType.NestedDicToDicWithOneDimentionalType)
        
            
    Dim intCurrentDicIndex As Long, i As Long, j As Long, intChildColumnMax As Long
    
    Dim intValue As Long, intChildValue As Long, objChildDic As Scripting.Dictionary
    Dim objChildRowCol As Collection, objChildDTCol As Collection, varKey As Variant, varChildKey As Variant
    
    
    Set robjNestedDic = New Scripting.Dictionary
    
    
    rblnIsInterpretedOfSingleDimensionColAsRowData = True
    
    Select Case venmOneNestedDicType
    
        Case OneNestedDicType.NestedDicToDicWithOneDimentionalType
    
            intChildColumnMax = vintColumnMax - 2
    
            If intChildColumnMax > 3 And vintChildRowMax = 1 Then
            
                rblnIsInterpretedOfSingleDimensionColAsRowData = True
                
            ElseIf vintChildRowMax > 1 And intChildColumnMax = 3 Then
            
                rblnIsInterpretedOfSingleDimensionColAsRowData = False
            End If
    
        Case OneNestedDicType.NestedDicToDTColType
    
            intChildColumnMax = vintColumnMax - 1
    End Select
    
    intValue = 1
    
    For intCurrentDicIndex = 1 To vintDicCountMax
    
        Select Case venmOneNestedDicType
        
            Case OneNestedDicType.NestedDicToDicWithOneDimentionalType
            
                Set objChildDic = New Scripting.Dictionary
                
                varKey = intValue
                
                For i = 1 To vintChildRowMax
                
                    varChildKey = intValue + 1
'
                    If intChildColumnMax = 1 Then
                    
                        intValue = varChildKey + 1
                    
                        objChildDic.Add varChildKey, intValue
                        
                        'intValue = intValue + 1
                    Else
                        Set objChildRowCol = New Collection
                    
                        intValue = varChildKey + 1
                        
                        For j = 1 To intChildColumnMax
                        
                            objChildRowCol.Add intValue
                        
                            If j < intChildColumnMax Then
                            
                                intValue = intValue + 1
                            End If
                        Next
                    
                        objChildDic.Add varChildKey, objChildRowCol
                    End If
                Next
            
                If intChildColumnMax = 1 Then
                
                    'intValue = intValue + 1
                
                    robjNestedDic.Add varKey, objChildDic
                    
                    'intValue = intValue + 1
                Else
                
                    'intValue = intValue + 1
                    
                    robjNestedDic.Add varKey, objChildDic
                    
                    'intValue = intValue + 1
                End If
            
            Case OneNestedDicType.NestedDicToDTColType
        
                intChildValue = intValue + 1
                
                Set objChildRowCol = Nothing
                
                Set objChildDTCol = Nothing
                
                For i = 1 To vintChildRowMax
                
                    If intChildColumnMax = 1 Then
                
                        If vintChildRowMax = 1 Then
                        
                            ' Norting to do
                        Else
                        
                            If objChildRowCol Is Nothing Then Set objChildRowCol = New Collection
                        
                            objChildRowCol.Add intChildValue
                            
                            intChildValue = intChildValue + 1
                        End If
                    Else
                        If vintChildRowMax = 1 Then
                        
                            If objChildRowCol Is Nothing Then Set objChildRowCol = New Collection
                        
                            For j = 1 To intChildColumnMax
                        
                                objChildRowCol.Add intChildValue
                                
                                intChildValue = intChildValue + 1
                            Next
                        Else
                            If objChildDTCol Is Nothing Then Set objChildDTCol = New Collection
                        
                            Set objChildRowCol = New Collection
                        
                            For j = 1 To intChildColumnMax
                        
                                objChildRowCol.Add intChildValue
                                
                                intChildValue = intChildValue + 1
                            Next
                        
                            objChildDTCol.Add objChildRowCol
                        End If
                    End If
                Next
        
                
                If vintChildRowMax = 1 And intChildColumnMax = 1 Then
                
                    robjNestedDic.Add intValue, intValue + 1
                    
                    intValue = intValue + 2
                    
                ElseIf vintChildRowMax > 1 And intChildColumnMax > 1 Then
                
                    robjNestedDic.Add intValue, objChildDTCol
                
                    intValue = intChildValue
                Else
                    robjNestedDic.Add intValue, objChildRowCol
                
                    intValue = intChildValue
                End If
        End Select
    Next
End Sub

'''
'''
'''
''' <Argument>robjFieldTitleToColumnDescriptionDic: Output</Argument>
''' <Argument>robjFieldTitleToColumnTypeNameDic: Output</Argument>
''' <Argument>robjFieldTitleToColumnTypeLengthDic: Output</Argument>
''' <Argument>vintColumnMax: Input</Argument>
Public Sub SetupPadAdditionalInfoDicsAboutColumnNames(ByRef robjFieldTitleToColumnDescriptionDic As Scripting.Dictionary, _
        ByRef robjFieldTitleToColumnTypeNameDic As Scripting.Dictionary, _
        ByRef robjFieldTitleToColumnTypeLengthDic As Scripting.Dictionary, _
        ByVal vintColumnMax As Long)
        
        
    Dim i As Long, strFieldTitle As String, strColumnDescription As String, strTypeName As String, varTypeLength As Variant
    
    
    Set robjFieldTitleToColumnDescriptionDic = New Scripting.Dictionary
    
    Set robjFieldTitleToColumnTypeNameDic = New Scripting.Dictionary
    
    Set robjFieldTitleToColumnTypeLengthDic = New Scripting.Dictionary
    
    
    For i = 1 To vintColumnMax

        strFieldTitle = "Column" & CStr(i)
        
        strColumnDescription = "Description" & Format(i, "000")
        
        strTypeName = "Type" & CStr(i)
        
        If (i Mod 3) = 0 Then
        
            varTypeLength = i
            
        ElseIf (i Mod 3) = 1 Then
        
            varTypeLength = Null
        Else
            varTypeLength = Empty
        End If
        
        
        robjFieldTitleToColumnDescriptionDic.Add strFieldTitle, strColumnDescription
        
        robjFieldTitleToColumnTypeNameDic.Add strFieldTitle, strTypeName
        
        robjFieldTitleToColumnTypeLengthDic.Add strFieldTitle, varTypeLength
    Next
End Sub


'''
'''
'''
Private Function mfstrGetSimpleChars(ByRef rintCharIndex As Long, Optional ByVal vintLengthOfChars As Long = 3)

    Dim intStartingCharIndex As Long, i As Long, intCharCode As Long
    Dim strAlphaChars As String, intAlphaBoundCode As Long
    Const intAlphaBound As Long = 26
    
    If rintCharIndex < 1 Then
    
        rintCharIndex = 1
    End If
    
    If rintCharIndex > intAlphaBound Then
    
        rintCharIndex = (rintCharIndex Mod intAlphaBound)
    End If
    
    If (rintCharIndex Mod intAlphaBound) = 0 Then
    
        intStartingCharIndex = rintCharIndex Mod intAlphaBound
    Else
        intStartingCharIndex = rintCharIndex
    End If

    
    strAlphaChars = ""
    
    intCharCode = Asc("A") - 1 + rintCharIndex
    
    intAlphaBoundCode = Asc("Z") + 1
    
    For i = 1 To vintLengthOfChars

        If intCharCode = intAlphaBoundCode Then
        
            intCharCode = Asc("A")
        End If

        strAlphaChars = strAlphaChars & Chr(intCharCode)
        
        intCharCode = intCharCode + 1
    Next

    mfstrGetSimpleChars = strAlphaChars
End Function


'**---------------------------------------------
'** Field titles decoration tests of data-tables
'**---------------------------------------------
'''
''' About ManualDecoratingFieldTitlesRangePattern enumeration
'''
Private Function mfobjGetManualDecoratingFieldTitlesRangePatternCol() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgArial

        .Add ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgMeiryo

        .Add DarkBrownFontLightSilverBgMeiryo
        
        .Add SepiaFontLightToDarkBrownGraduationBgMeiryo
        
        .Add WhiteFontDarkGreenGraduationBgMeiryo
        
        .Add DefaultCellFormatAndMeiryo
        
        .Add WhiteFontDarkOrangeGraduationBgMeiryo
    End With
    
    Set mfobjGetManualDecoratingFieldTitlesRangePatternCol = objCol
End Function

'''
''' About FieldTitleInterior
'''
Private Function mfobjGetFieldTitleInteriorCol() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add ColumnNameInteriorOfDefaultTypeSQL
        
        .Add FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo
        
        .Add FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQLBySepiaFontLightToDarkBrownGraduationBgMeiryo

        .Add ColumnNameInteriorOfGatheredColTable
        
        .Add FieldTitleInterior.ColumnNameInteriorOfGatheredColTableByOrangeTone

        .Add ColumnNameInteriorOfExcelSheetAdoSQL
        
        .Add ColumnNameInteriorOfCsvAdoSQL
        
        .Add ColumnNameInteriorOrMSAccessRDBSQL
        
        .Add ColumnNameInteriorOfLoadedINIFileAsTable
        
        .Add ColumnNameInteriorOfLoadedWinRegistryAsTable
        
        .Add ColumnNameInteriorOfExtractedTableFromSheet
        
        .Add ColumnNameInteriorOfMetaProperties
        
        .Add ColumnNameInteriorOfPostgreSQL
    End With
    
    Set mfobjGetFieldTitleInteriorCol = objCol
End Function

'''
'''
'''
Private Function mfobjGetRecordCellsBordersCol() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add RecordCellsBorders.RecordCellsBordersNone
        
        .Add RecordCellsBorders.RecordCellsGrayAll
        
        .Add RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal
    
    End With

    Set mfobjGetRecordCellsBordersCol = objCol
End Function


'**---------------------------------------------
'** About file-path
'**---------------------------------------------
'''
'''
'''
Private Function mfstrGetSheetDecorationTestBookDir() As String

    Dim strDir As String

    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\DecorationSheetTest"

    ForceToCreateDirectory strDir
    
    mfstrGetSheetDecorationTestBookDir = strDir
End Function

'''
'''
'''
Public Function GetUnitTestingSheetRandomizedDataBookDir() As String

    Dim strDir As String: strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\RandomizedSheetTest"

    ForceToCreateDirectory strDir
    
    GetUnitTestingSheetRandomizedDataBookDir = strDir
End Function

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToSetupPadIntegerAndStringOnRCTableCol()

    Dim objDTCol As Collection
    
    
    Set objDTCol = Nothing: msubSetupPadIntegerAndStringOnRCTableCol objDTCol, 1, 1

    DebugCol objDTCol


    Set objDTCol = Nothing: msubSetupPadIntegerAndStringOnRCTableCol objDTCol, 2, 1

    DebugCol objDTCol

    Set objDTCol = Nothing: msubSetupPadIntegerAndStringOnRCTableCol objDTCol, 1, 2

    DebugCol objDTCol

    Set objDTCol = Nothing: msubSetupPadIntegerAndStringOnRCTableCol objDTCol, 2, 2

    DebugCol objDTCol
    
    Set objDTCol = Nothing: msubSetupPadIntegerAndStringOnRCTableCol objDTCol, 2, 3

    DebugCol objDTCol
    
    Set objDTCol = Nothing: msubSetupPadIntegerAndStringOnRCTableCol objDTCol, 3, 5

    DebugCol objDTCol
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetSimpleChars()

    Debug.Print mfstrGetSimpleChars(1)

    Debug.Print mfstrGetSimpleChars(0)

    Debug.Print mfstrGetSimpleChars(-1)

    Debug.Print mfstrGetSimpleChars(25)

    Debug.Print mfstrGetSimpleChars(26)
    
    Debug.Print mfstrGetSimpleChars(27)
    
    Debug.Print mfstrGetSimpleChars(27, 4)
    
    Debug.Print mfstrGetSimpleChars(27, 6)
    
    Debug.Print mfstrGetSimpleChars(3, 28)
End Sub

