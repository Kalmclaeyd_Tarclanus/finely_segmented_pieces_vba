Attribute VB_Name = "UTfDecorationGetterFromXlSheet"
'
'   Sanity tests to get cell Font and Interior from Excel Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  6/Jun/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubGetDecorationInfoFromActiveCell()

    Dim objRange As Excel.Range
    
    Set objRange = ActiveCell
    
    With objRange
    
        With .Interior
    
            Debug.Print GetTextFromRGB(.Color)
    
            If .ThemeColor <> 0 Then
    
                Debug.Print GetXlThemeColorTextFromEnm(.ThemeColor)
                
                Debug.Print .TintAndShade
            End If
            
            If IsNull(.Pattern) Then
            
                Debug.Print "Pattern type is null"
            Else
            
                Debug.Print .Pattern
            End If
            
        End With
    
    End With
    
End Sub


'''
'''
'''
Private Sub msubSanityTestToSampleCellDecorations()

    Dim strPath As String, objSheet As Excel.Worksheet, objRange As Excel.Range
    
    
    strPath = mfstrGetDecorationSampleExcelBookDir() & "\CellDecorationTestSheet.xlsx"

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strPath, "CellFontAndInterior")

    With objSheet

        Set objRange = .Cells(2, 1)
        
        With objRange.Interior
        
            .ThemeColor = XlThemeColor.xlThemeColorAccent2
            
            .TintAndShade = 0.8
        End With
        
        Set objRange = .Cells(2, 2)
        
        With objRange.Interior
        
            .Color = RGB(60, 70, 170)
        End With
        
    End With

End Sub

'''
'''
'''
Private Function mfstrGetDecorationSampleExcelBookDir() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
    
    ForceToCreateDirectory strDir
    
    mfstrGetDecorationSampleExcelBookDir = strDir
End Function




