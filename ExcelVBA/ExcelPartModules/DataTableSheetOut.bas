Attribute VB_Name = "DataTableSheetOut"
'
'   output data-table into Excel Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 25/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Data expansion type
'**---------------------------------------------
Public Enum OneNestedDicType

    NestedDicToDicWithOneDimentionalType    ' Dictionary(Of Key, Of Dictinary(Of Key, Item( Variant Or One-dimensional Collection )))

    NestedDicToDTColType    ' Dictionary(Of Key, Of Collection(Of Collection))
End Enum

'**---------------------------------------------
'** Cell background design enumeration
'**---------------------------------------------
Public Enum CellBackground
    
    NoChange = 0


    FormatConditionTextContain = 11     ' Use TextOperator condition
    
    FormatConditionFormulas = 12        ' Use Formula condition
End Enum

'''
''' color arrangement constants for FormatCondition
'''
Public Enum CellColorArrangement

    FontDeepRedBgLightRed = 1
    
    FontDeepGreenBgLightGreen = 2
    
    FontDeepPurpleBgLightPurpleGradiation = 3
    
    FontDeepBrownBgLightYellowTiltedGradiation = 4
    
    FontDeepBlueBgLightBlue = 5
    
    FontDeepGrayBgLightPink = 6
    
    FontPaleGrayBgDefault = 7
End Enum

'**---------------------------------------------
'** Range background design constants, for field-title appearance
'**---------------------------------------------
Public Enum FieldTitleInterior

    ColumnNameInteriorOfDefaultTypeSQL = &H0   ' Default type RDB table title background default
    
    ColumnNameInteriorOfGatheredColTable = 1    ' Gathered Collection Table
    
    ColumnNameInteriorOfExcelSheetAdoSQL = 2        ' Excel Sheet ADO-SQL
    
    ColumnNameInteriorOrMSAccessRDBSQL = 3          ' Microsoft Access RDB SQL
    
    ColumnNameInteriorOfCsvAdoSQL = 4               ' CSV file with ADO connection
    
    ColumnNameInteriorOfLoadedINIFileAsTable = 5        ' When a INI file information is loaded, or for INI compatible table of three columns
    
    ColumnNameInteriorOfLoadedWinRegistryAsTable = 6    ' When some Windows registry information are loaded
    
    ColumnNameInteriorOfExtractedTableFromSheet = 7     ' Extracted data-table from non-table spread grid by some rules
    
    ColumnNameInteriorOfMetaProperties = 8      ' Meta-property name, self-condition informations
    
    ColumnNameInteriorOfPostgreSQL = 10  ' PostgreSQL RDB table title background
    
    
    ' other types of ColumnNameInteriorOfGatheredColTable
    
    ColumnNameInteriorOfGatheredColTableByOrangeTone = 51    ' Gathered Collection Table by orange color
    
    ' other types of ColumnNameInteriorOfDefaultTypeSQL
    
    ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo = 33
    
    ColumnNameInteriorOfDefaultTypeSQLBySepiaFontLightToDarkBrownGraduationBgMeiryo = 34
End Enum


'**---------------------------------------------
'** Range background design constants, for log item title appearance for log area
'**---------------------------------------------
Public Enum SQLSheetLogTitleInterior

    CellsInteriorOfSynchronizedSQLExec = &H0    ' Synchronized Query or Command execution
    
    CellsInteriorOfAsynchronousSQLExec = &H1    ' Asynchronous Query or Command execution
    
    CellsInteriorOfSQLExecutionErrorOccured = &H2        ' Error occured log
End Enum

'''
'''
'''
Public Enum DicSheetExpandDirection

    DicExpandingKyesColumnsValuesColumnsAsTable = 0

    DicExpandingKeysRowsValuesRows = 1
End Enum


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToDataTableSheetOut()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "FindForNotTablesOnSheet,DataTableSheetIn,UTfDataTableSheetOut,UTfDataTableTextOut,DataTableSheetFormatter"
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Expand dictinary key-values to Cells on Worksheet
'**---------------------------------------------
'''
''' This doesn't support the case that the values are collection or some object type
'''
Public Sub OutputDicWithoutHeaderSimplyToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByRef robjDic As Scripting.Dictionary, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmDicSheetExpandDirection As DicSheetExpandDirection = DicSheetExpandDirection.DicExpandingKyesColumnsValuesColumnsAsTable, _
        Optional ByVal venmKeysInterior As FieldTitleInterior = FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQL, _
        Optional ByVal venmExpandedCellsBorder As RecordCellsBorders = RecordCellsBorders.RecordCellsGrayAll)


    Dim varValues As Variant, intRowsCount As Long, intColumnsCount As Long, i As Long, j As Long
    Dim varKey As Variant, varValue As Variant
    
    If venmDicSheetExpandDirection = DicExpandingKyesColumnsValuesColumnsAsTable Then
    
        intRowsCount = robjDic.Count: intColumnsCount = 2
    Else
        intRowsCount = 2: intColumnsCount = robjDic.Count
    End If
    
    ReDim varValues(1 To intRowsCount, 1 To intColumnsCount)
    
    If venmDicSheetExpandDirection = DicExpandingKyesColumnsValuesColumnsAsTable Then
    
        With robjDic
        
            i = 1
            
            For Each varKey In .Keys
        
                varValues(i, 1) = varKey
            
                varValues(i, 2) = .Item(varKey)
                
                i = i + 1
            Next
        End With
        
        With vobjSheet.Cells(vintTopLeftRowIndex, vintTopLeftColumnIndex)
        
            .Resize(intRowsCount, intColumnsCount).Value = varValues
            
            DecorateRangeOfFieldTitlesForRDB .Resize(intRowsCount, 1), venmKeysInterior
        
            DecorateRangeBorders .Resize(intRowsCount, intColumnsCount), venmExpandedCellsBorder, True
        End With
    Else
        With robjDic
        
            j = 1
    
            For Each varKey In .Keys
            
                varValues(1, j) = varKey
            
                varValues(2, j) = .Item(varKey)
                
                j = j + 1
            Next
        End With
        
        With vobjSheet.Cells(vintTopLeftRowIndex, vintTopLeftColumnIndex)
        
            .Resize(intRowsCount, intColumnsCount).Value = varValues
            
            DecorateRangeOfFieldTitlesForRDB .Resize(1, intColumnsCount), venmKeysInterior
        
            DecorateRangeBorders .Resize(intRowsCount, intColumnsCount), venmExpandedCellsBorder, True
        End With
    End If
End Sub

'**---------------------------------------------
'** Decoration function for comparing ManualDecoratingFieldTitlesRangePattern
'**---------------------------------------------
'''
''' Use FieldTitleInterior enumeration
'''
Public Sub DecorateDataTableSimply(ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal venmFieldTitleInterior As FieldTitleInterior = FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQL, _
        Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBlacksAndGrayInsideHorizontal, _
        Optional ByVal vsngRecordsFontSize As Single = 9)

    Dim strRange As String

    With vobjSheet

        With .Range(ConvertXlColumnIndexToLetter(.UsedRange.Column) & CStr(.UsedRange.Row)).CurrentRegion

            If .Rows.Count > 1 Then

                strRange = "A2:" & ConvertXlColumnIndexToLetter(.Columns.Count) & CStr(.Rows.Count)

                DecorateRangeOfRecords .Range(strRange), False, venmRecordCellsBorders, vsngRecordsFontSize
            End If

            strRange = "A1:" & ConvertXlColumnIndexToLetter(.Columns.Count) & "1"

            DecorateRangeOfFieldTitlesForRDB .Range(strRange), venmFieldTitleInterior
        End With
    End With
End Sub

'''
''' get FieldTitleInterior string
'''
Public Function GetFieldTitleInteriorTextFromEnm(ByVal venmFieldTitleInterior As FieldTitleInterior) As String

    Dim strText As String
    
    strText = ""
    
    Select Case venmFieldTitleInterior
    
        Case FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQL
        
            strText = "ColumnNameInteriorOfDefaultTypeSQL"
        
        Case FieldTitleInterior.ColumnNameInteriorOfGatheredColTable
        
            strText = "ColumnNameInteriorOfGatheredColTable"
        
        Case FieldTitleInterior.ColumnNameInteriorOfExcelSheetAdoSQL
        
            strText = "ColumnNameInteriorOfExcelSheetAdoSQL"
        
        Case FieldTitleInterior.ColumnNameInteriorOrMSAccessRDBSQL
        
            strText = "ColumnNameInteriorOrMSAccessRDBSQL"
        
        Case FieldTitleInterior.ColumnNameInteriorOfCsvAdoSQL
        
            strText = "ColumnNameInteriorOfCsvAdoSQL"
            
        Case FieldTitleInterior.ColumnNameInteriorOfLoadedINIFileAsTable
        
            strText = "ColumnNameInteriorOfLoadedINIFileAsTable"
        
        Case FieldTitleInterior.ColumnNameInteriorOfLoadedWinRegistryAsTable
        
            strText = "ColumnNameInteriorOfLoadedWinRegistryAsTable"
        
        Case FieldTitleInterior.ColumnNameInteriorOfExtractedTableFromSheet
        
            strText = "ColumnNameInteriorOfExtractedTableFromSheet"
        
        Case FieldTitleInterior.ColumnNameInteriorOfMetaProperties
        
            strText = "ColumnNameInteriorOfMetaProperties"
        
        Case FieldTitleInterior.ColumnNameInteriorOfPostgreSQL
        
            strText = "ColumnNameInteriorOfPostgreSQL"
    
        Case FieldTitleInterior.ColumnNameInteriorOfGatheredColTableByOrangeTone    ' other types of ColumnNameInteriorOfGatheredColTabl
        
            strText = "ColumnNameInteriorOfGatheredColTableByOrangeTone"
    
        Case FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo    ' other types of ColumnNameInteriorOfDefaultTypeSQL
        
            strText = "ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo"
            
        Case FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQLBySepiaFontLightToDarkBrownGraduationBgMeiryo
        
            strText = "ColumnNameInteriorOfDefaultTypeSQLBySepiaFontLightToDarkBrownGraduationBgMeiryo"
    
    End Select
    
    GetFieldTitleInteriorTextFromEnm = strText
End Function


'**---------------------------------------------
'** calculate DataTableSheetRangeAddresses
'**---------------------------------------------
'''
'''
'''
Public Function GetDataTableSheetRangeAddressesFromDTColAndDTFormatter(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjCol As Collection, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByRef ritfDataTableSheetFormatter As IDataTableSheetFormatter, _
        Optional ByVal vblnAllowToAddSheetNameToAddress As Boolean = True) As DataTableSheetRangeAddresses


    Dim intColumnsCountOfRecords As Long, intRowsCountOfRecords As Long
    
    If vobjCol.Count > 0 Then

        intRowsCountOfRecords = vobjCol.Count
        
        If TypeOf vobjCol.Item(1) Is Collection Then
        
            intColumnsCountOfRecords = vobjCol.Item(1).Count
        Else
            intColumnsCountOfRecords = 1
        End If
    End If
    
    Set GetDataTableSheetRangeAddressesFromDTColAndDTFormatter = GetDataTableSheetRangeAddressesFromDTIndexesAndDTFormatter(vobjSheet, _
            intRowsCountOfRecords, _
            intColumnsCountOfRecords, _
            vobjFieldTitlesCol, _
            ritfDataTableSheetFormatter, _
            vblnAllowToAddSheetNameToAddress)
End Function


'''
'''
'''
Public Function GetDataTableSheetRangeAddressesFromDTDicAndDTFormatter(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjDic As Scripting.Dictionary, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal ritfDataTableSheetFormatter As IDataTableSheetFormatter, _
        Optional ByVal vblnAllowToAddSheetNameToAddress As Boolean = True) As DataTableSheetRangeAddresses


    Dim intColumnsCountOfRecords As Long, intRowsCountOfRecords As Long, objRowCol As Collection, objChildDic As Scripting.Dictionary
    
    If vobjDic.Count > 0 Then

        intRowsCountOfRecords = vobjDic.Count
        
        If TypeOf vobjDic.Items(0) Is Collection Then
        
            Set objRowCol = vobjDic.Items(0)
        
            intColumnsCountOfRecords = 1 + objRowCol.Count
            
        ElseIf TypeOf vobjDic.Items(0) Is Scripting.Dictionary Then
        
            ' a short-name is as OneNestedDic in this VBA project
        
            Set objChildDic = vobjDic.Items(0)
            
            If TypeOf objChildDic.Items(0) Is Collection Then
            
                Set objRowCol = objChildDic.Items(0)
            
                intColumnsCountOfRecords = 2 + objRowCol.Count
            Else
                intColumnsCountOfRecords = 3
            End If
        Else
            intColumnsCountOfRecords = 2
        End If
    End If
    
    Set GetDataTableSheetRangeAddressesFromDTDicAndDTFormatter = GetDataTableSheetRangeAddressesFromDTIndexesAndDTFormatter(vobjSheet, _
            intRowsCountOfRecords, _
            intColumnsCountOfRecords, _
            vobjFieldTitlesCol, _
            ritfDataTableSheetFormatter, _
            vblnAllowToAddSheetNameToAddress)
End Function


'''
'''
'''
Public Function GetDataTableSheetRangeAddressesFromDTVarAndDTFormatter(ByVal vobjSheet As Excel.Worksheet, _
        ByRef rvarDTValues As Variant, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByRef ritfDataTableSheetFormatter As IDataTableSheetFormatter, _
        Optional ByVal vblnAllowToAddSheetNameToAddress As Boolean = True) As DataTableSheetRangeAddresses


    Dim intColumnsCountOfRecords As Long, intRowsCountOfRecords As Long, objRowCol As Collection, objChildDic As Scripting.Dictionary
    
    If IsArrayInitialized(rvarDTValues) Then

        intRowsCountOfRecords = UBound(rvarDTValues, 1)
        
        intColumnsCountOfRecords = UBound(rvarDTValues, 2)
    End If
    
    Set GetDataTableSheetRangeAddressesFromDTVarAndDTFormatter = GetDataTableSheetRangeAddressesFromDTIndexesAndDTFormatter(vobjSheet, _
            intRowsCountOfRecords, _
            intColumnsCountOfRecords, _
            vobjFieldTitlesCol, _
            ritfDataTableSheetFormatter, _
            vblnAllowToAddSheetNameToAddress)
End Function


'''
'''
'''
Private Function GetDataTableSheetRangeAddressesFromDTIndexesAndDTFormatter(ByVal vobjSheet As Excel.Worksheet, _
        ByRef rintRowsCountOfRecords As Long, _
        ByRef rintColumnsCountOfRecords As Long, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByRef ritfDataTableSheetFormatter As IDataTableSheetFormatter, _
        Optional ByVal vblnAllowToAddSheetNameToAddress As Boolean = True) As DataTableSheetRangeAddresses


    Set GetDataTableSheetRangeAddressesFromDTIndexesAndDTFormatter = GetDataTableSheetRangeAddressesFromDTIndexesAndFieldsCountAndDTFormatter(vobjSheet, _
            rintRowsCountOfRecords, _
            rintColumnsCountOfRecords, _
            vobjFieldTitlesCol.Count, _
            ritfDataTableSheetFormatter, _
            vblnAllowToAddSheetNameToAddress)
End Function

'''
'''
'''
Public Function GetDataTableSheetRangeAddressesFromDTIndexesAndFieldsCountAndDTFormatter(ByVal vobjSheet As Excel.Worksheet, _
        ByRef rintRowsCountOfRecords As Long, _
        ByRef rintColumnsCountOfRecords As Long, _
        ByVal vintFieldsCount As Long, _
        ByRef ritfDataTableSheetFormatter As IDataTableSheetFormatter, _
        Optional ByVal vblnAllowToAddSheetNameToAddress As Boolean = True) As DataTableSheetRangeAddresses


    Dim objDataTableSheetRangeAddresses As DataTableSheetRangeAddresses
    
    Dim strFieldTitlesRange As String, strRecordsRange As String
    
    
    Set objDataTableSheetRangeAddresses = New DataTableSheetRangeAddresses

    With ritfDataTableSheetFormatter
    
        If .AllowToShowFieldTitle Then
        
            ' About field titles
            
            strFieldTitlesRange = GetDataTableFieldTitlesBaseAddressFromIndexesAndFieldsCount(vintFieldsCount, .FieldTitlesRowIndex, .TopLeftColumnIndex)
            
            If vblnAllowToAddSheetNameToAddress Then
            
                strFieldTitlesRange = vobjSheet.Name & "$" & strFieldTitlesRange
            End If
            
            objDataTableSheetRangeAddresses.FieldTitlesRangeAddress = strFieldTitlesRange
        End If
        
        strRecordsRange = GetDataTableRecordBaseAddressFromRecordIndexesAndDTFormatter(rintRowsCountOfRecords, rintColumnsCountOfRecords, ritfDataTableSheetFormatter)

        If vblnAllowToAddSheetNameToAddress Then
            
            strRecordsRange = vobjSheet.Name & "$" & strRecordsRange
        End If
        
        objDataTableSheetRangeAddresses.RecordAreaRangeAddress = strRecordsRange
    End With

    Set GetDataTableSheetRangeAddressesFromDTIndexesAndFieldsCountAndDTFormatter = objDataTableSheetRangeAddresses
End Function


'''
'''
'''
Public Function GetDataTableFieldTitlesBaseAddressFromIndexes(ByRef robjFieldTitlesCol As Collection, _
        ByRef rintFieldTitlesRowIndex As Long, _
        ByRef rintTopLeftColumnIndex As Long)


    GetDataTableFieldTitlesBaseAddressFromIndexes = GetDataTableFieldTitlesBaseAddressFromIndexesAndFieldsCount(robjFieldTitlesCol.Count, _
            rintFieldTitlesRowIndex, _
            rintTopLeftColumnIndex)
End Function

'''
'''
'''
Public Function GetDataTableFieldTitlesBaseAddressFromIndexesAndFieldsCount(ByRef rintFieldsCount As Long, _
        ByRef rintFieldTitlesRowIndex As Long, _
        ByRef rintTopLeftColumnIndex As Long)


    Dim strFieldTitlesRange As String

    If rintFieldsCount = 1 Then
    
        strFieldTitlesRange = ConvertXlColumnIndexToLetter(rintTopLeftColumnIndex) & CStr(rintFieldTitlesRowIndex)
    Else
        strFieldTitlesRange = ConvertXlColumnIndexToLetter(rintTopLeftColumnIndex) & CStr(rintFieldTitlesRowIndex) & ":" & ConvertXlColumnIndexToLetter(rintTopLeftColumnIndex + rintFieldsCount - 1) & CStr(rintFieldTitlesRowIndex)
    End If

    GetDataTableFieldTitlesBaseAddressFromIndexesAndFieldsCount = strFieldTitlesRange
End Function


'''
'''
'''
Public Function GetDataTableRecordBaseAddressFromRecordIndexesAndDTFormatter(ByRef rintRowsCountOfRecords As Long, _
        ByRef rintColumnsCountOfRecords As Long, _
        ByRef ritfDataTableSheetFormatter As IDataTableSheetFormatter) As String

    With ritfDataTableSheetFormatter
    
        GetDataTableRecordBaseAddressFromRecordIndexesAndDTFormatter = GetDataTableRecordBaseAddressFromRecordIndexes(rintRowsCountOfRecords, _
                rintColumnsCountOfRecords, _
                .FieldTitlesRowIndex, _
                .TopLeftColumnIndex, _
                .AllowToShowFieldTitle)
    End With
End Function

'''
'''
'''
Public Function GetDataTableRecordBaseAddressFromRecordIndexes(ByRef rintRowsCountOfRecords As Long, _
        ByRef rintColumnsCountOfRecords As Long, _
        ByRef rintFieldTitlesRowIndex As Long, _
        ByRef rintTopLeftColumnIndex As Long, _
        ByRef rblnAllowToShowFieldTitle As Boolean) As String

    Dim strRecordsRange As String
    
    strRecordsRange = ""
    
    If rintRowsCountOfRecords > 0 Then
    
        If rblnAllowToShowFieldTitle Then
        
            If rintRowsCountOfRecords = 1 And rintColumnsCountOfRecords = 1 Then
            
                strRecordsRange = ConvertXlColumnIndexToLetter(rintTopLeftColumnIndex) & CStr(rintFieldTitlesRowIndex + 1)
            Else
                strRecordsRange = ConvertXlColumnIndexToLetter(rintTopLeftColumnIndex) & CStr(rintFieldTitlesRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(rintTopLeftColumnIndex + rintColumnsCountOfRecords - 1) & CStr(rintFieldTitlesRowIndex + rintRowsCountOfRecords)
            End If
        Else
            If rintRowsCountOfRecords = 1 And rintColumnsCountOfRecords = 1 Then
            
                strRecordsRange = ConvertXlColumnIndexToLetter(rintTopLeftColumnIndex) & CStr(rintFieldTitlesRowIndex + 1)
            Else
                strRecordsRange = ConvertXlColumnIndexToLetter(rintTopLeftColumnIndex) & CStr(rintFieldTitlesRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(rintTopLeftColumnIndex + rintColumnsCountOfRecords - 1) & CStr(rintFieldTitlesRowIndex + rintRowsCountOfRecords)
            End If
        End If
    End If

    GetDataTableRecordBaseAddressFromRecordIndexes = strRecordsRange
End Function


'**---------------------------------------------
'** Output data-table of various data-type, such as Collection, Dictionary, and two dimensional array of Variant, with some various formatting
'**---------------------------------------------
'''
''' output row-column 2-dimensional collection type data table to sheet
'''
Public Sub OutputColToSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjCol As Collection, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal vobjDataTableSheetFormatter As DataTableSheetFormatter, _
        Optional ByVal vblnAllowToSetCellFormulasToRange As Boolean = False)


    With vobjDataTableSheetFormatter
        
        If vobjFieldTitlesCol Is Nothing Then
        
            .AllowToShowFieldTitle = False
        End If
        
        .SetSheetFormatBeforeTableOut
    
        If .AllowToShowFieldTitle Then
        
            OutputOnlyFieldTitlesColToSheet vobjSheet, vobjFieldTitlesCol, .FieldTitlesRowIndex, .TopLeftColumnIndex
        End If

        If Not vobjCol Is Nothing Then
        
            OutputOnlyDataTableColToSheet vobjSheet, vobjCol, .FieldTitlesRowIndex, .TopLeftColumnIndex, vblnAllowToSetCellFormulasToRange
        End If
    End With
    
    If vobjDataTableSheetFormatter.AllowToSetAutoFilter Then
    
        SetAutoFilterIfItIsNeeded vobjSheet, vobjDataTableSheetFormatter
    End If
    
    vobjDataTableSheetFormatter.SetSheetFormatAfterTableOut vobjSheet
End Sub

'''
''' The collection of the field-titles is generated from FieldTitlesToWidthDic dictionary of DataTableSheetFormmatter
'''
Public Sub OutputColToSheetRestrictedByDataTableSheetFormatter(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjCol As Collection, _
        ByVal vobjDataTableSheetFormatter As DataTableSheetFormatter)


    Dim objFieldTitlesCol As Collection
    
    Set objFieldTitlesCol = GetEnumeratorKeysColFromDic(vobjDataTableSheetFormatter.FieldTitleToColumnWidthDic)

    OutputColToSheet vobjSheet, vobjCol, objFieldTitlesCol, vobjDataTableSheetFormatter
End Sub


'''
'''
'''
Public Sub SetAutoFilterIfItIsNeeded(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjDataTableSheetFormatter As DataTableSheetFormatter, _
        Optional ByVal vblnNecessaryUsedRangeForSingleSheetWithIncludingLoggingCells As Boolean = False)

    Dim strRange As String, objCurrentRegion As Excel.Range

    If vobjDataTableSheetFormatter.AllowToSetAutoFilter Then
    
        With vobjSheet
            
            If vblnNecessaryUsedRangeForSingleSheetWithIncludingLoggingCells Then
            
                With vobjDataTableSheetFormatter
                
                    strRange = ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex) & ":" & ConvertXlColumnIndexToLetter(vobjSheet.UsedRange.Columns.Count) & CStr(vobjSheet.UsedRange.Rows.Count)
                End With
            
                .Range(strRange).AutoFilter
            Else
                With vobjDataTableSheetFormatter
                
                    Set objCurrentRegion = vobjSheet.Range(ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex)).CurrentRegion
                End With
            
                objCurrentRegion.AutoFilter
            End If
        End With
    End If
End Sub

'''
'''
'''
''' <Argument>vobjSheet: Input-Output</Argument>
''' <Argument>vobjFieldTitlesCol: Input</Argument>
''' <Argument>vintFieldTitlesRowIndex: Input</Argument>
''' <Argument>vintTopLeftColumnIndex: Input</Argument>
Public Sub OutputOnlyFieldTitlesColToSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal vintFieldTitlesRowIndex As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    
    If Not (IsNull(vobjFieldTitlesCol) Or IsEmpty(vobjFieldTitlesCol) Or (vobjFieldTitlesCol Is Nothing)) Then
        
        OutputFieldTitlesToCellsOnSheet vobjSheet, vobjFieldTitlesCol, vintFieldTitlesRowIndex, vintTopLeftColumnIndex
    End If
End Sub

'''
'''
'''
Public Sub OutputOnlyDataTableColToSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjCol As Collection, _
        ByVal vintFieldTitlesRowIndex As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vblnAllowToSetCellFormulasToRange As Boolean = False)
    
    Dim varValues As Variant, objChildCol As Collection, intRowIndex As Long
    Dim intColumnMax As Long, objRange As Excel.Range
    
    
    If Not vobjCol Is Nothing Then
    
        If vobjCol.Count > 0 Then
                   
            If IsObject(vobjCol.Item(1)) Then
            
                Set objChildCol = vobjCol.Item(1)
                
                intColumnMax = objChildCol.Count
            Else
                intColumnMax = 1
            End If
            
            If intColumnMax > 0 Then
            
                With vobjSheet
                
                    Set objRange = vobjSheet.Cells(vintFieldTitlesRowIndex + 1, vintTopLeftColumnIndex)
                    
                    GetRCValuesFromRCCollection varValues, vobjCol
                    
                    OutputTwoDimensionalArrayToWorksheetRange objRange, varValues, vblnAllowToSetCellFormulasToRange
                End With
            End If
        End If
    End If
End Sub

'''
'''
'''
Public Sub OutputDataTableColIntoCellsOnSheet(ByRef robjSheet As Excel.Worksheet, _
        ByRef robjDTCol As Collection, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vblnAllowToSetCellFormulasToRange As Boolean = False)

    Dim objChildCol As Collection, intColumnMax As Long, objRange As Excel.Range, varValues As Variant

    If Not robjDTCol Is Nothing Then
    
        If robjDTCol.Count > 0 Then
                   
            If IsObject(robjDTCol.Item(1)) Then
            
                Set objChildCol = robjDTCol.Item(1)
                
                intColumnMax = objChildCol.Count
            Else
                intColumnMax = 1
            End If
            
            If intColumnMax > 0 Then
            
                With robjSheet
                
                    Set objRange = robjSheet.Cells(vintTopLeftRowIndex, vintTopLeftColumnIndex)
                    
                    GetRCValuesFromRCCollection varValues, robjDTCol
                    
                    OutputTwoDimensionalArrayToWorksheetRange objRange, varValues, vblnAllowToSetCellFormulasToRange
                End With
            End If
        End If
    End If
End Sub


'''
'''
'''
Public Sub OutputOnlyDataTableColToTopLeftRangeOnSheet(ByVal vobjTopLeftRange As Excel.Range, _
        ByVal vobjDTCol As Collection, _
        Optional ByVal vblnAllowToSetCellFormulasToRange As Boolean = False)

    Dim varValues As Variant

    GetRCValuesFromRCCollection varValues, vobjDTCol

    OutputTwoDimensionalArrayToWorksheetRange vobjTopLeftRange, varValues, vblnAllowToSetCellFormulasToRange
End Sub

'''
''' output row-column dictionary type data table to sheet
'''
''' <Argument>vobjSheet</Argument>
''' <Argument>vobjDic: Dictionary(Of String, String) or Dictionary(Of String, Collection(Of String))</Argument>
''' <Argument>vobjFieldTitlesCol: Collection(Of String)</Argument>
''' <Argument>vobjDataTableSheetFormatter</Argument>
Public Sub OutputDicToSheet(ByVal vobjSheet As Excel.Worksheet, ByVal vobjDic As Scripting.Dictionary, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal vobjDataTableSheetFormatter As DataTableSheetFormatter)
    
    
    Dim blnNoNecessaryToCheckRecordsExists As Boolean

    blnNoNecessaryToCheckRecordsExists = False
    
    With vobjDataTableSheetFormatter

        If vobjFieldTitlesCol Is Nothing Then
        
            .AllowToShowFieldTitle = False
        End If

        .SetSheetFormatBeforeTableOut
    
        If .AllowToShowFieldTitle Then
        
            OutputOnlyFieldTitlesColToSheet vobjSheet, vobjFieldTitlesCol, .FieldTitlesRowIndex, .TopLeftColumnIndex
        End If
    
        If Not vobjDic Is Nothing Then
        
            blnNoNecessaryToCheckRecordsExists = True
        
            OutputOnlyDataTableDicToSheet vobjSheet, vobjDic, .FieldTitlesRowIndex, .TopLeftColumnIndex
        End If
    End With

    SetAutoFilterIfItIsNeeded vobjSheet, vobjDataTableSheetFormatter

    vobjDataTableSheetFormatter.SetSheetFormatAfterTableOut vobjSheet
End Sub

'''
'''
'''
Public Sub OutputDicToSheetRestrictedByDataTableSheetFormatter(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjDic As Scripting.Dictionary, _
        ByVal vobjDataTableSheetFormatter As DataTableSheetFormatter)


    Dim objFieldTitlesCol As Collection
    
    Set objFieldTitlesCol = GetEnumeratorKeysColFromDic(vobjDataTableSheetFormatter.FieldTitleToColumnWidthDic)

    OutputDicToSheet vobjSheet, vobjDic, objFieldTitlesCol, vobjDataTableSheetFormatter
End Sub

'''
''' output one-nested dictionaries (Dictionary(Of String, Dictionary(Of String, String Or Collection))) type data table to sheet
'''
''' or (Dictionary(Of String, Collection(Of String Or Collection))) type
'''
''' <Argument>vobjSheet</Argument>
''' <Argument>vobjOneNestedDic: Dictionary(Of String, Dictionary(Of String, String))</Argument>
''' <Argument>vobjFieldTitlesCol: Collection(Of String)</Argument>
''' <Argument>vobjDataTableSheetFormatter</Argument>
''' <Argument>vblnAllowToInterpretSingleDimensionColAsRowData</Argument>
Public Sub OutputOneNestedDicToSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjOneNestedDic As Scripting.Dictionary, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal vobjDataTableSheetFormatter As DataTableSheetFormatter, _
        Optional ByVal vblnAllowToInterpretSingleDimensionColAsRowData As Boolean = True, _
        Optional ByVal vblnAllowToAddAutoShapeLogOfOneNestedDicCharacteristicsInfo As Boolean = True)

    Dim varRCTable() As Variant

    msubSetRCTableFromOneNestedDic varRCTable, vobjOneNestedDic, vblnAllowToInterpretSingleDimensionColAsRowData

    OutputVarTableToSheet vobjSheet, varRCTable, vobjFieldTitlesCol, vobjDataTableSheetFormatter
    
    If vblnAllowToAddAutoShapeLogOfOneNestedDicCharacteristicsInfo Then
    
        AddAutoShapeGeneralMessage vobjSheet, _
                mfstrGetInfoLogOfOneNestedDicTableExpansion(varRCTable, vobjOneNestedDic, vblnAllowToAddAutoShapeLogOfOneNestedDicCharacteristicsInfo)
    End If
End Sub

'''
'''
'''
Private Function mfstrGetInfoLogOfOneNestedDicTableExpansion(ByRef rvarRCTable() As Variant, _
        ByRef robjOneNestedDic As Scripting.Dictionary, _
        ByRef rblnAllowToInterpretSingleDimensionColAsRowData As Boolean) As String


    Dim strLog As String
    
    
    strLog = "Expanded table rows count: " & CStr(UBound(rvarRCTable, 1)) & vbNewLine
    
    strLog = strLog & "Expanded table columns count: " & CStr(UBound(rvarRCTable, 2)) & vbNewLine
    
    strLog = strLog & "Count of nested-Dictionary: " & CStr(robjOneNestedDic.Count) & vbNewLine
    
    strLog = strLog & "Nested Dictionary type structure: " & GetTypeNameOfDicStructure(robjOneNestedDic) & vbNewLine
    
    strLog = strLog & "Interpreting single-dimension Collection: " & IIf(rblnAllowToInterpretSingleDimensionColAsRowData, "Row-vector data", "Column-vector data")
    
    mfstrGetInfoLogOfOneNestedDicTableExpansion = strLog
End Function


'''
''' output one-nested dictionaries (Dictionary(Of String, Dictionary(Of String, String))) type data table to sheet, which the field-titles collection is generated from FieldTitlesToWidthDic dictionary of DataTableSheetFormmatter
'''
''' <Argument>vobjSheet:Input </Argument>
''' <Argument>vobjOneNestedDic: Input - Dictionary(Of String, Dictionary(Of String, String)) or Dictionary(Of String, Dictionary(Of String, Collection(Of String)))</Argument>
''' <Argument>vobjDataTableSheetFormatter: Input</Argument>
Public Sub OutputOneNestedDicToSheetRestrictedByDataTableSheetFormatter(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjOneNestedDic As Scripting.Dictionary, _
        ByVal vobjDataTableSheetFormatter As DataTableSheetFormatter, _
        Optional ByVal vblnAllowToInterpretSingleDimensionColAsRowData As Boolean = True)

    Dim objFieldTitlesCol As Collection
    
    Set objFieldTitlesCol = GetEnumeratorKeysColFromDic(vobjDataTableSheetFormatter.FieldTitleToColumnWidthDic)

    OutputOneNestedDicToSheet vobjSheet, vobjOneNestedDic, objFieldTitlesCol, vobjDataTableSheetFormatter, vblnAllowToInterpretSingleDimensionColAsRowData
End Sub


'''
'''
'''
Private Sub msubSetRCTableFromOneNestedDic(ByRef rvarRCTable() As Variant, _
        ByVal vobjOneNestedDic As Scripting.Dictionary, _
        Optional ByVal vblnAllowToInterpretSingleDimensionColAsRowData As Boolean = True)

    Dim objChildDic As Scripting.Dictionary, intRowCount As Long, varFirstKey As Variant
    Dim blnIsLastChildTypeCollection As Boolean, objCol As Collection, intCountOfLastChildCollection As Long
    
    Dim enmOneNestedDicType As OneNestedDicType
    
    intRowCount = 0
    
    blnIsLastChildTypeCollection = False
    
    With vobjOneNestedDic
    
        ' identify OneNestedDicType
        
        For Each varFirstKey In .Keys
        
            If TypeOf .Item(varFirstKey) Is Scripting.Dictionary Then
            
                enmOneNestedDicType = NestedDicToDicWithOneDimentionalType
            
            ElseIf TypeOf .Item(varFirstKey) Is Collection Then
            
                enmOneNestedDicType = NestedDicToDTColType
            End If
            
            Exit For
        Next
    
    
        Select Case enmOneNestedDicType
        
            Case OneNestedDicType.NestedDicToDicWithOneDimentionalType
    
                msubGetInfoOfNestedDicToDicWithOneDimentionalType intRowCount, _
                        intCountOfLastChildCollection, _
                        blnIsLastChildTypeCollection, _
                        vobjOneNestedDic, _
                        vblnAllowToInterpretSingleDimensionColAsRowData
    
                msubSetRCTableFromOneNestedDicWithInfos rvarRCTable, _
                        vobjOneNestedDic, _
                        intRowCount, _
                        intCountOfLastChildCollection, _
                        blnIsLastChildTypeCollection, _
                        vblnAllowToInterpretSingleDimensionColAsRowData
                
            Case OneNestedDicType.NestedDicToDTColType
    
                msubGetInfoOfNestedDicToDTColType intRowCount, _
                        intCountOfLastChildCollection, _
                        blnIsLastChildTypeCollection, _
                        vobjOneNestedDic
    
                msubSetRCTableFromOneNestedDicOfDTColWithInfos rvarRCTable, _
                        vobjOneNestedDic, _
                        intRowCount, _
                        intCountOfLastChildCollection, _
                        blnIsLastChildTypeCollection
        End Select
    End With
End Sub

'''
'''
'''
Private Sub msubGetInfoOfNestedDicToDTColType(ByRef rintRowCount As Long, _
        ByRef rintCountOfLastChildCollection As Long, _
        ByRef rblnIsLastChildTypeCollection As Boolean, _
        ByVal vobjOneNestedDic As Scripting.Dictionary)


    Dim i As Long, varFirstKey As Variant, objChildDTCol As Collection, objRowCol As Collection

    rintRowCount = 0

    i = 1
    
    With vobjOneNestedDic
    
        For Each varFirstKey In .Keys
        
            Set objChildDTCol = .Item(varFirstKey)
            
            rintRowCount = rintRowCount + objChildDTCol.Count
        
            If i = 1 Then
            
                msubGetChildRowColInfoFromChildDicOrChildCol rintCountOfLastChildCollection, _
                        rblnIsLastChildTypeCollection, _
                        objChildDTCol
            End If
        
            i = i + 1
        Next
    End With
End Sub



'''
'''
'''
Private Sub msubSetRCTableFromOneNestedDicWithInfos(ByRef rvarRCTable() As Variant, _
        ByVal vobjOneNestedDic As Scripting.Dictionary, _
        ByVal vintRowCount As Long, _
        ByVal vintCountOfLastChildCollection As Long, _
        ByVal vblnIsLastChildTypeCollection As Boolean, _
        Optional ByVal vblnAllowToInterpretSingleDimensionColAsRowData As Boolean = True)


    Dim intColumnCount As Long, i As Long, j As Long, varFirstKey As Variant, varSecondKey As Variant
    Dim objChildDic As Scripting.Dictionary, objCol As Collection, varItem As Variant
    

    If vblnIsLastChildTypeCollection Then
        
        If vblnAllowToInterpretSingleDimensionColAsRowData Then
        
            intColumnCount = 2 + vintCountOfLastChildCollection
        Else
            intColumnCount = 3
        End If
    Else
        intColumnCount = 3
    End If


    ReDim rvarRCTable(1 To vintRowCount, 1 To intColumnCount)
    
    i = 1
    
    With vobjOneNestedDic
    
        For Each varFirstKey In .Keys
        
            Set objChildDic = .Item(varFirstKey)
            
            With objChildDic
            
                If vblnAllowToInterpretSingleDimensionColAsRowData Then
            
                    For Each varSecondKey In .Keys
                        
                        j = 1
            
                        rvarRCTable(i, j) = varFirstKey
                        
                        j = 2
                        
                        rvarRCTable(i, j) = varSecondKey
                        
                        j = 3
                        
                        If vblnIsLastChildTypeCollection Then
                        
                            Set objCol = .Item(varSecondKey)
                            
                            For Each varItem In objCol
                            
                                rvarRCTable(i, j) = varItem
                                
                                j = j + 1
                            Next
                        Else
                            rvarRCTable(i, j) = .Item(varSecondKey)
                        End If
                        
                        i = i + 1
                    Next
                Else
                    For Each varSecondKey In .Keys
                        
                        If vblnIsLastChildTypeCollection Then
                        
                            Set objCol = .Item(varSecondKey)
                            
                            For Each varItem In objCol
                            
                                j = 1
                    
                                rvarRCTable(i, j) = varFirstKey
                                
                                j = 2
                                
                                rvarRCTable(i, j) = varSecondKey
                            
                                j = 3
                            
                                rvarRCTable(i, j) = varItem
                                
                                i = i + 1
                            Next
                        Else
                            j = 1
                
                            rvarRCTable(i, j) = varFirstKey
                            
                            j = 2
                            
                            rvarRCTable(i, j) = varSecondKey
                            
                            j = 3
                        
                            rvarRCTable(i, j) = .Item(varSecondKey)
                        End If
                        
                        i = i + 1
                    Next
                End If
            End With
        Next
    End With
End Sub

'''
'''
'''
Private Sub msubSetRCTableFromOneNestedDicOfDTColWithInfos(ByRef rvarRCTable() As Variant, _
        ByVal vobjOneNestedDic As Scripting.Dictionary, _
        ByVal vintRowCount As Long, _
        ByVal vintCountOfLastChildCollection As Long, _
        ByVal vblnIsLastChildTypeCollection As Boolean)

    Dim intColumnCount As Long, i As Long, j As Long, varFirstKey As Variant, varSecondKey As Variant
    Dim objChildDTCol As Collection, objRowCol As Collection, varRowValue As Variant, varChildItem As Variant
    

    If vblnIsLastChildTypeCollection Then
        
        intColumnCount = 1 + vintCountOfLastChildCollection
    Else
        intColumnCount = 2
    End If


    ReDim rvarRCTable(1 To vintRowCount, 1 To intColumnCount)
    
    i = 1
    
    With vobjOneNestedDic
    
        For Each varFirstKey In .Keys
        
            Set objChildDTCol = .Item(varFirstKey)
            
            For Each varRowValue In objChildDTCol
            
                j = 1
                
                rvarRCTable(i, j) = varFirstKey
            
                j = 2
                
                If vblnIsLastChildTypeCollection Then
            
                    Set objRowCol = varRowValue
                
                    For Each varChildItem In objRowCol
                    
                        rvarRCTable(i, j) = varChildItem
                    
                        j = j + 1
                    Next
                Else
                    rvarRCTable(i, j) = varRowValue
                End If
                
                i = i + 1
            Next
        Next
    End With
End Sub



'''
'''
'''
Private Sub msubGetInfoOfNestedDicToDicWithOneDimentionalType(ByRef rintRowCount As Long, _
        ByRef rintCountOfLastChildCollection As Long, _
        ByRef rblnIsLastChildTypeCollection As Boolean, _
        ByVal vobjOneNestedDic As Scripting.Dictionary, _
        Optional ByVal vblnAllowToInterpretSingleDimensionColAsRowData As Boolean = True)

    
    Dim i As Long, varFirstKey As Variant, objChildDic As Scripting.Dictionary, objCol As Collection

    Dim varSecondKey As Variant, objChildColumnCol As Collection
    

    rintRowCount = 0

    i = 1
    
    With vobjOneNestedDic
    
        For Each varFirstKey In .Keys
        
            Set objChildDic = .Item(varFirstKey)
            
            If vblnAllowToInterpretSingleDimensionColAsRowData Then
            
                rintRowCount = rintRowCount + objChildDic.Count
            Else
                
                With objChildDic
                
                    For Each varSecondKey In .Keys
        
                        If TypeOf .Item(varSecondKey) Is Collection Then
                        
                            rblnIsLastChildTypeCollection = True
                        Else
                            rblnIsLastChildTypeCollection = False
                        End If
                        
                        Exit For
                    Next
                    
                    If rblnIsLastChildTypeCollection Then
                    
                        For Each varSecondKey In .Keys
            
                            Set objChildColumnCol = .Item(varSecondKey)
            
                            rintRowCount = rintRowCount + objChildColumnCol.Count
                        Next
                    Else
                        rintRowCount = rintRowCount + objChildDic.Count
                    End If
                End With
            End If
        
            If i = 1 Then
            
                msubGetChildRowColInfoFromChildDicOrChildCol rintCountOfLastChildCollection, rblnIsLastChildTypeCollection, objChildDic
            End If
        
            i = i + 1
        Next
    End With
End Sub

'''
'''
'''
Private Sub msubGetChildRowColInfoFromChildDicOrChildCol(ByRef rintCountOfLastChildCollection As Long, _
        ByRef rblnIsLastChildTypeCollection As Boolean, _
        ByRef rvarDicOrCol As Variant)

    Dim objChildDic As Scripting.Dictionary, objChildDTCol As Collection, objRowCol As Collection
    
    If IsObject(rvarDicOrCol) Then
    
        If TypeOf rvarDicOrCol Is Scripting.Dictionary Then
        
            Set objChildDic = rvarDicOrCol
            
            With objChildDic
            
                If TypeOf .Items(0) Is Collection Then
                    
                    rblnIsLastChildTypeCollection = True
                    
                    Set objRowCol = .Items(0)
                    
                    rintCountOfLastChildCollection = objRowCol.Count
                End If
            End With
            
        ElseIf TypeOf rvarDicOrCol Is Collection Then
        
            Set objChildDTCol = rvarDicOrCol
        
            With objChildDTCol
            
                If TypeOf .Item(1) Is Collection Then
                    
                    rblnIsLastChildTypeCollection = True
                    
                    Set objRowCol = .Item(1)
                    
                    rintCountOfLastChildCollection = objRowCol.Count
                End If
            End With
        End If
    End If
End Sub


'''
'''
'''
Public Sub OutputOnlyDataTableDicToSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjDic As Scripting.Dictionary, _
        ByVal vintFieldTitlesRowIndex As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    
    Dim varValues As Variant, varKey As Variant, varItem As Variant, blnIsNestedCollection As Boolean
    
    Dim intColumnIndex As Long, intColumnMax As Long, objRange As Excel.Range
    
    If Not vobjDic Is Nothing Then
    
        If vobjDic.Count > 0 Then
        
            blnIsNestedCollection = IsDictionaryValuesNestedCollection(vobjDic)
            
            With vobjSheet
            
                Set objRange = vobjSheet.Cells(vintFieldTitlesRowIndex + 1, vintTopLeftColumnIndex)
                
                GetRCValuesFromRCDictionary varValues, vobjDic
                
                OutputTwoDimensionalArrayToWorksheetRange objRange, varValues
            End With
        End If
    End If
End Sub



'''
''' output row-column 2-dimensional variant array data table to sheet
'''
''' <Argument>vobjSheet: Input-Output</Argument>
''' <Argument>rvarRCTable: Input - must be 2-dimensional Variant array</Argument>
''' <Argument>vobjFieldTitlesCol: Input</Argument>
''' <Argument>vobjDataTableSheetFormatter: Input</Argument>
Public Sub OutputVarTableToSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByRef rvarRCTable As Variant, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal vobjDataTableSheetFormatter As DataTableSheetFormatter)


    Dim blnNoNecessaryToCheckRecordsExists As Boolean

    blnNoNecessaryToCheckRecordsExists = False

    With vobjDataTableSheetFormatter
        
        If vobjFieldTitlesCol Is Nothing Then
            
            .AllowToShowFieldTitle = False
        End If
        
        .SetSheetFormatBeforeTableOut
    End With

    With vobjDataTableSheetFormatter
    
        OutputOnlyFieldTitlesColToSheet vobjSheet, vobjFieldTitlesCol, .FieldTitlesRowIndex, .TopLeftColumnIndex
    End With
    
    If IsArrayInitialized(rvarRCTable) Then
    
        blnNoNecessaryToCheckRecordsExists = True
    
        With vobjDataTableSheetFormatter
        
            OutputOnlyDataTableVarToSheet vobjSheet, rvarRCTable, .FieldTitlesRowIndex, .TopLeftColumnIndex
        End With
    End If

    SetAutoFilterIfItIsNeeded vobjSheet, vobjDataTableSheetFormatter

    vobjDataTableSheetFormatter.SetSheetFormatAfterTableOut vobjSheet
End Sub


'''
'''
'''
Public Sub OutputVarTableToSheetRestrictedByDataTableSheetFormatter(ByVal vobjSheet As Excel.Worksheet, _
        ByRef rvarRCTable As Variant, _
        ByVal vobjDataTableSheetFormatter As DataTableSheetFormatter)

    Dim objFieldTitlesCol As Collection
    
    Set objFieldTitlesCol = GetEnumeratorKeysColFromDic(vobjDataTableSheetFormatter.FieldTitleToColumnWidthDic)

    OutputVarTableToSheet vobjSheet, rvarRCTable, objFieldTitlesCol, vobjDataTableSheetFormatter
End Sub


'''
''' for Worksheet formatting decoration tests
'''
Public Sub OutputVarTableToSheetWithoutAnyDecorationFormatting(ByVal vobjSheet As Excel.Worksheet, _
        ByRef rvarRCTable As Variant, _
        ByRef robjFieldTitlesCol As Collection, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)


    OutputOnlyFieldTitlesColToSheet vobjSheet, robjFieldTitlesCol, vintFieldTitlesRowIndex, vintTopLeftColumnIndex
    
    If IsArrayInitialized(rvarRCTable) Then
    
        OutputOnlyDataTableVarToSheet vobjSheet, rvarRCTable, vintFieldTitlesRowIndex, vintTopLeftColumnIndex
    End If
End Sub

'''
'''
'''
''' <Argument>vobjSheet: Output sheet</Argument>
''' <Argument>rvarRowColumnTable: input-array - must be 2-dimensional Variant array</Argument>
Public Sub OutputOnlyDataTableVarToSheet(ByRef robjSheet As Excel.Worksheet, _
        ByRef rvarRowColumnTable As Variant, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vblnAllowToSetCellFormulasToRange As Boolean = False)
    
    If Not IsEmpty(rvarRowColumnTable) Then
        
        Dim objRange As Excel.Range
        
        Set objRange = robjSheet.Cells(vintFieldTitlesRowIndex + 1, vintTopLeftColumnIndex)
        
        OutputTwoDimensionalArrayToWorksheetRange objRange, rvarRowColumnTable, vblnAllowToSetCellFormulasToRange
    End If
End Sub


'''
'''
'''
''' <Argument>robjTopLeftRange: Output an area of top-left of the output sheet</Argument>
''' <Argument>rvarTwoDimensionalArrayWhichFirstIndexIsOne: Variant two dimensional array which the first index is always one, or ReDim rvarTwoDimensionalArrayWhichFirstIndexIsOne(1 To Row-max, 1 To Column-max)</Argument>
Public Sub OutputTwoDimensionalArrayToWorksheetRange(ByRef robjTopLeftRange As Excel.Range, _
        ByRef rvarTwoDimensionalArrayWhichFirstIndexIsOne As Variant, _
        Optional ByVal vblnAllowToSetCellFormulasToRange As Boolean = False)

    Dim objRange As Excel.Range

    ' This argument rvarTwoDimensionalArrayWhichFirstIndexIsOne must be;
    '
    '   LBound(rvarTwoDimensionalArrayWhichFirstIndexIsOne, 1) = 1
    '
    '   LBound(rvarTwoDimensionalArrayWhichFirstIndexIsOne, 2) = 1

    Set objRange = robjTopLeftRange.Resize(UBound(rvarTwoDimensionalArrayWhichFirstIndexIsOne, 1), UBound(rvarTwoDimensionalArrayWhichFirstIndexIsOne, 2))

    With objRange
    
        If vblnAllowToSetCellFormulasToRange Then
        
            .Formula = rvarTwoDimensionalArrayWhichFirstIndexIsOne
        Else
            .Value = rvarTwoDimensionalArrayWhichFirstIndexIsOne
        End If
    End With
End Sub





'**---------------------------------------------
'** About getting simple DataTableSheetFormatter object
'**---------------------------------------------
'''
'''
'''
Public Function GetDataTableSheetFormatterFromFieldTitlesBySameColumnWidth(ByVal vobjFieldTitlesCol As Collection, _
        Optional ByVal vdblColumnWidth As Double = 12#, _
        Optional ByVal venmFieldTitleInterior As FieldTitleInterior = FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQL, _
        Optional ByVal venmRecordBorderType As RecordCellsBorders = RecordCellsBorders.RecordCellsGrayAll)

    Dim objDataTableSheetFormatter As DataTableSheetFormatter, varFieldTitle As Variant
    
    Set objDataTableSheetFormatter = New DataTableSheetFormatter
    
    With objDataTableSheetFormatter
    
        .FieldTitleInteriorType = venmFieldTitleInterior
    
        .RecordBordersType = venmRecordBorderType
    
        Set .FieldTitleToColumnWidthDic = New Scripting.Dictionary
        
        With .FieldTitleToColumnWidthDic
        
            For Each varFieldTitle In vobjFieldTitlesCol
            
                If Not IsEmpty(varFieldTitle) Then
            
                    If varFieldTitle <> "" Then
            
                        .Add varFieldTitle, vdblColumnWidth
                    End If
                End If
            Next
        End With
    End With
    
    Set GetDataTableSheetFormatterFromFieldTitlesBySameColumnWidth = objDataTableSheetFormatter
End Function


'**---------------------------------------------
'** Tools for data conversion on Excel.Worksheet
'**---------------------------------------------
'''
''' Refactoring
'''
''' Ref. https://support.microsoft.com/ja-jp/help/833402/how-to-convert-excel-column-numbers-into-alphabetical-characters
'''
Public Function ConvertXlColumnIndexToLetter(ByVal vintColumn As Integer) As String
   
   Dim intAlpha As Integer, intRemainder As Integer
   
   intAlpha = Int((vintColumn - 1) / 26)
   
   intRemainder = vintColumn - (intAlpha * 26)
   
   If intAlpha > 0 Then
      
      ConvertXlColumnIndexToLetter = Chr(intAlpha + 64)
   End If
   
   If intRemainder > 0 Then
      
      ConvertXlColumnIndexToLetter = ConvertXlColumnIndexToLetter & Chr(intRemainder + 64)
   End If
End Function

'''
''' Set 'FreezePanes' to be certainly enabled
'''
Public Sub SetFreezePanesOn(ByVal vobjSheet As Excel.Worksheet, Optional vintFieldTitlesRowIndex As Long = 1)

    Dim objWindow As Excel.Window, objOldWindow As Excel.Window, objBook As Excel.Workbook
    Dim blnOldScreenUpdatingAboutWindow As Boolean, blnOldEnableEvents As Boolean
    
    With vobjSheet.Application
        
        blnOldScreenUpdatingAboutWindow = .ScreenUpdating
        
        blnOldEnableEvents = .EnableEvents
        
        On Error GoTo ErrorFreezePanesOn
        
        .ScreenUpdating = False
        
        Set objBook = vobjSheet.Parent
        
        Set objWindow = .Windows.Item(objBook.Name)
        
        Set objOldWindow = Nothing
        
        If Not (ActiveWindow Is objWindow) Then
        
            Set objOldWindow = ActiveWindow
            
            objWindow.Activate
        End If
        
    
        vobjSheet.Rows(CStr(vintFieldTitlesRowIndex + 1) & ":" & CStr(vintFieldTitlesRowIndex + 1)).Select
        
        .EnableEvents = False
        
        objWindow.FreezePanes = True
        
ErrorFreezePanesOn:
        
        .ScreenUpdating = blnOldScreenUpdatingAboutWindow
        
        .EnableEvents = blnOldEnableEvents
        
        If Not objOldWindow Is Nothing Then
        
            objOldWindow.Activate
        End If
    End With

End Sub


'''
''' get Row-Column (2 dimensional array) values from inputted Row-Column collection
'''
''' <Return>Variant(m, n) 2-dimensional array Variant </Return>
Public Sub GetRCValuesFromRCCollection(ByRef rvarValues As Variant, ByVal vobjRCCol As Collection)

    Dim objValuesCol As Collection, varItem As Variant, varRowItem As Variant
    Dim intRowMax As Long, intColumnMax As Long, intRowIndex As Long, intColumnIndex As Long, blnIsTheRCColTwoDimensionalCollection As Boolean
    
    
    intRowMax = vobjRCCol.Count
    
    If IsObject(vobjRCCol.Item(1)) Then
    
        Set objValuesCol = vobjRCCol.Item(1)
        
        intColumnMax = objValuesCol.Count
        
        blnIsTheRCColTwoDimensionalCollection = True
    Else
        intColumnMax = 1
        
        blnIsTheRCColTwoDimensionalCollection = False
    End If
    
    
    
    ReDim rvarValues(1 To intRowMax, 1 To intColumnMax)
    
    intRowIndex = 1: intColumnIndex = 1
    
    For Each varRowItem In vobjRCCol
    
        If blnIsTheRCColTwoDimensionalCollection Then
        
            Set objValuesCol = varRowItem
            
            intColumnIndex = 1
            
            For Each varItem In objValuesCol
                
                If IsObject(varItem) Then
                
                    If Not varItem Is Nothing Then
                    
                        rvarValues(intRowIndex, intColumnIndex) = varItem
                    End If
                Else
                    rvarValues(intRowIndex, intColumnIndex) = varItem
                End If
                
                intColumnIndex = intColumnIndex + 1
            Next
        Else
            rvarValues(intRowIndex, intColumnIndex) = varRowItem
        End If
    
        intRowIndex = intRowIndex + 1
    Next
End Sub



'''
''' get Row-Column (2 dimensional array) values from inputted Row-Column collection
'''
''' <Return>Variant(n, m) 2-dimensional array Variant </Return>
Public Sub GetRCValuesFromRCDictionary(ByRef rvarValues As Variant, ByVal vobjRCDic As Scripting.Dictionary)

    Dim objValuesCol As Collection, varItem As Variant, varRowItem As Variant
    Dim intRowMax As Long, intColumnMax As Long, intRowIndex As Long, intColumnIndex As Long
    
    Dim blnIsTheRCColTwoDimensionalCollection As Boolean, varKey As Variant
    
    
    With vobjRCDic
        
        intRowMax = .Count
        
        ' find maximum index of columns
        
        If IsObject(.Items(0)) Then
        
            If TypeOf .Items(0) Is Collection Then
            
                Set objValuesCol = .Items(0)
                
                intColumnMax = objValuesCol.Count + 1
                
                blnIsTheRCColTwoDimensionalCollection = True
                
            ElseIf TypeOf .Items(0) Is Dictionary Then
            
                ' Is this necessary?
            
                Debug.Assert False
            End If
        Else
            intColumnMax = 2
                
            blnIsTheRCColTwoDimensionalCollection = False
        End If
        
    End With
    
    ReDim rvarValues(1 To intRowMax, 1 To intColumnMax)
    
    intRowIndex = 1
    
    intColumnIndex = 1
    
    With vobjRCDic
        
        For Each varKey In .Keys
            
            If blnIsTheRCColTwoDimensionalCollection Then
                
                Set varRowItem = .Item(varKey)
                
                Set objValuesCol = varRowItem
                
                
                rvarValues(intRowIndex, 1) = varKey  ' first row value
                
                intColumnIndex = 2  ' from second row value item
                
                For Each varItem In objValuesCol
                    
                    If IsObject(varItem) Then
                        
                        If Not varItem Is Nothing Then
                            
                            rvarValues(intRowIndex, intColumnIndex) = varItem
                        End If
                    Else
                        rvarValues(intRowIndex, intColumnIndex) = varItem
                    End If
                    
                    intColumnIndex = intColumnIndex + 1
                Next
            Else
                varRowItem = .Item(varKey)
                
                rvarValues(intRowIndex, 1) = varKey
                
                rvarValues(intRowIndex, 2) = varRowItem
            End If
        
            intRowIndex = intRowIndex + 1
        Next
    End With
End Sub


'''
''' adjust columns width from field-title text
'''
Public Sub AdjustColumnWidthByTitleText(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary, _
        Optional vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    Dim i As Long, strSheetColumnLetter As String, strValue As String
    Dim objColumnWidthDic As Scripting.Dictionary, dblWidth As Double, strSheetRange As String

    
    If Not vobjFieldTitleToColumnWidthDic Is Nothing Then
        
        Set objColumnWidthDic = vobjFieldTitleToColumnWidthDic
    Else
        Set objColumnWidthDic = GetThisSystemDefaultColumnWidthDic
    End If

    With vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion

        For i = .Column To .Columns.Count + .Column - 1
            
            If Not IsEmpty(vobjSheet.Cells(vintFieldTitlesRowIndex, i).Value) Then
                
                strValue = CStr(vobjSheet.Cells(vintFieldTitlesRowIndex, i).Value)
                
                With objColumnWidthDic
                    
                    If .Exists(strValue) Then
                        
                        dblWidth = CDbl(.Item(strValue))
                        
                        strSheetColumnLetter = ConvertXlColumnIndexToLetter(i)
                        
                        strSheetRange = strSheetColumnLetter & ":" & strSheetColumnLetter
                        
                        With vobjSheet.Columns(strSheetRange).EntireColumn
                        
                            .ColumnWidth = dblWidth
                        End With
                    End If
                End With
            End If
        Next
    End With
End Sub


'''
''' adjust columns width from field-title text with specifying
'''
Public Sub AdjustColumnWidthByTitleTextWithFindingRightDirection(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)
    
    Dim i As Long, strSheetColumnLetter As String, strValue As String
    Dim objColumnWidthDic As Scripting.Dictionary, dblWidth As Double, strSheetRange As String
    
    Dim objTopLeftRange As Excel.Range, objTopRightRange As Excel.Range, objFieldTitlesRange As Excel.Range

    
    If Not vobjFieldTitleToColumnWidthDic Is Nothing Then
        
        Set objColumnWidthDic = vobjFieldTitleToColumnWidthDic
    Else
        Set objColumnWidthDic = GetThisSystemDefaultColumnWidthDic
    End If
    
    Set objTopLeftRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))

    Set objTopRightRange = objTopLeftRange.End(xlToRight)

    With objTopLeftRange

        Set objFieldTitlesRange = vobjSheet.Range(.Cells(1, 1), .Cells(1, objTopRightRange.Column - objTopLeftRange.Column + 1))

        With objFieldTitlesRange
    
            For i = .Column To .Columns.Count + .Column - 1
                
                If Not IsEmpty(vobjSheet.Cells(vintFieldTitlesRowIndex, i).Value) Then
                    
                    strValue = CStr(vobjSheet.Cells(vintFieldTitlesRowIndex, i).Value)
                    
                    With objColumnWidthDic
                        
                        If .Exists(strValue) Then
                            
                            dblWidth = CDbl(.Item(strValue))
                            
                            strSheetColumnLetter = ConvertXlColumnIndexToLetter(i)
                            
                            strSheetRange = strSheetColumnLetter & ":" & strSheetColumnLetter
                            
                            With vobjSheet.Columns(strSheetRange).EntireColumn
                            
                                .ColumnWidth = dblWidth
                            End With
                        End If
                    End With
                End If
            Next
        End With
    End With
End Sub


'''
''' adjust column widths from specified column orders
'''
Public Sub AdjustColumnWidthByTitleOrder(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary, _
        Optional vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)


    Dim strSheetRange As String, strSheetColumnLetter As String, intColMinimumIndex As Long, intColMaximumIndex As Long
    Dim varOrder As Variant, strOrder As String, intOrder As Long, dblWidth As Double
    
    
    With vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
        
        intColMinimumIndex = 1
        
        intColMaximumIndex = .Columns.Count
    End With
  
    With vobjFieldTitleOrderToColumnWidthDic
            
        For Each varOrder In .Keys
        
            strOrder = varOrder
            
            strOrder = Replace(strOrder, "Col", "")
            
            If IsNumeric(strOrder) Then
            
                intOrder = CInt(strOrder)
                
                If intOrder >= intColMinimumIndex And intOrder <= intColMaximumIndex Then
                
                    dblWidth = .Item(varOrder)
            
                    strSheetColumnLetter = ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex + intOrder - 1)
                    
                    strSheetRange = strSheetColumnLetter & ":" & strSheetColumnLetter
                    
                    With vobjSheet.Columns(strSheetRange).EntireColumn
                    
                        .ColumnWidth = dblWidth
                    End With
                End If
            End If
        Next
    End With
End Sub

'''
'''
'''
Public Function GetFieldTitleOrderToColumnWidthDicForFixedWidthFromFieldTitleCol(ByRef robjFieldTitlesCol As Collection, ByVal vdblFixedColumnWidth As Double) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary

    If robjFieldTitlesCol.Count = 1 Then
    
        Set objDic = GetFieldTitleOrderToColumnWidthDicForFixedWidth(CStr(robjFieldTitlesCol.Count), vdblFixedColumnWidth)
    Else
        Set objDic = GetFieldTitleOrderToColumnWidthDicForFixedWidth("1-" & CStr(robjFieldTitlesCol.Count), vdblFixedColumnWidth)
    End If
    
    Set GetFieldTitleOrderToColumnWidthDicForFixedWidthFromFieldTitleCol = objDic
End Function

'''
'''
'''
Public Function GetFieldTitleOrderToColumnWidthDicForFixedWidth(ByVal vstrRangeLiteral As String, ByVal vdblFixedColumnWidth As Double) As Scripting.Dictionary

    Dim varIntegerRange As Variant, intP1 As Integer
    Dim strValues() As String, i As Long, intMax As Long
    Dim objDic As Scripting.Dictionary
    
    
    Set objDic = New Scripting.Dictionary
    
    For Each varIntegerRange In Split(vstrRangeLiteral, ",")

        intP1 = InStr(1, varIntegerRange, "-")
        
        If intP1 > 0 Then
        
            strValues = Split(varIntegerRange, "-")
            
            For i = CLng(strValues(0)) To CLng(strValues(1))
            
                objDic.Add "Col" & CStr(i), vdblFixedColumnWidth
            Next
        Else
            objDic.Add "Col" & CStr(CLng(varIntegerRange)), vdblFixedColumnWidth
        End If
    Next
    
    Set GetFieldTitleOrderToColumnWidthDicForFixedWidth = objDic
End Function


'''
''' adjust the column width and change the appearance of the field-titles
'''
Public Sub AdjustColumnWidthAndDecorateFieldTitles(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vintFieldTitlesRowIndex As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmInteriorType As FieldTitleInterior = FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQL, _
        Optional ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vblnBasedOnCurrentRegion As Boolean = True)
    
    Dim strRange As String
    
    AdjustColumnWidthByTitleText vobjSheet, vobjFieldTitleToColumnWidthDic, vintFieldTitlesRowIndex, vintTopLeftColumnIndex

    If Not vobjFieldTitleOrderToColumnWidthDic Is Nothing Then
    
        AdjustColumnWidthByTitleOrder vobjSheet, vobjFieldTitleOrderToColumnWidthDic, vintFieldTitlesRowIndex, vintTopLeftColumnIndex
    End If

    With vobjSheet

        If vblnBasedOnCurrentRegion Then

            ' By CurrentRegion

            With .Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion

                strRange = ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex) & ":" & ConvertXlColumnIndexToLetter(.Columns.Count + vintTopLeftColumnIndex - 1) & CStr(vintFieldTitlesRowIndex)
            End With
        Else
            With .UsedRange
            
                strRange = ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex) & ":" & ConvertXlColumnIndexToLetter(.UsedRange.Columns.Count) & CStr(vintFieldTitlesRowIndex)
            End With
        End If
        
        DecorateRangeOfFieldTitlesForRDB .Range(strRange), venmInteriorType
    End With
End Sub


'''
''' adjust the column width and change the appearance of the field-titles
'''
Public Sub AdjustColumnWidthAndDecorateFieldTitlesWhenNoFieldTitlesRow(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vintTopLeftRowIndex As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary = Nothing)
    
    
    If Not vobjFieldTitleOrderToColumnWidthDic Is Nothing Then
    
        AdjustColumnWidthByTitleOrder vobjSheet, vobjFieldTitleOrderToColumnWidthDic, vintTopLeftColumnIndex, vintTopLeftColumnIndex
    End If
End Sub

'''
'''
'''
Public Sub ReDecorateFieldTitlesOnDataTableSheet(ByVal robjSheet As Excel.Worksheet, _
        ByVal venmInteriorType As FieldTitleInterior, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)

    Dim objRegion As Excel.Range
    
    Set objRegion = robjSheet.Cells(vintFieldTitlesRowIndex, vintTopLeftColumnIndex).CurrentRegion
    
    With objRegion

        DecorateRangeOfFieldTitlesForRDB .Worksheet.Range(.Cells(1, 1), .Cells(1, .Columns.Count)), venmInteriorType
    End With
End Sub


'''
''' adjust records cell-sizes by the specified standard setting
'''
''' <Argument>vobjSheet: Input-Output</Argument>
''' <Argument>vintFieldTitlesRowIndex: Input</Argument>
''' <Argument>vintTopLeftColumnIndex: Input</Argument>
''' <Argument>venmRecordCellsBorders: Input</Argument>
''' <Argument>vblnForceToSetSameRowHeight: Input</Argument>
''' <Argument>vblnBasedOnCurrentRegion: Input</Argument>
''' <Argument>vsngFontSize: Input</Argument>
''' <Argument>vsngRowsHeight: Input</Argument>
Public Sub DecorateRangeOfRecordsAboutBorderFontHeight(ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBordersNone, _
        Optional ByVal vblnForceToSetSameRowHeight As Boolean = False, _
        Optional ByVal vblnBasedOnCurrentRegion As Boolean = True, _
        Optional ByVal vsngFontSize As Single = 10, _
        Optional ByVal vsngRowsHeight As Single = 18#)
    
    Dim intColumnMax As Long, objTopLeftRange As Excel.Range, objTableRange As Excel.Range
    Dim strRange As String
    
    With vobjSheet
    
        Set objTopLeftRange = .Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))

        Set objTableRange = objTopLeftRange.CurrentRegion
    
        If vblnBasedOnCurrentRegion Then
    
            If 2 <= objTableRange.Rows.Count Then
    
                strRange = ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(objTableRange.Columns.Count + vintTopLeftColumnIndex - 1) & CStr(objTableRange.Rows.Count + vintFieldTitlesRowIndex - 1)
                
                DecorateRangeAboutBordersAndFont .Range(strRange), venmRecordCellsBorders, vsngFontSize
                
                AdjustRowHeight .Rows(CStr(vintFieldTitlesRowIndex + 1) & ":" & CStr(objTableRange.Rows.Count + vintFieldTitlesRowIndex - 1)), True, vsngRowsHeight
            End If
        Else
            If (vintFieldTitlesRowIndex + 1) <= .UsedRange.Rows.Count Then
                
                intColumnMax = .UsedRange.Columns.Count
                
                strRange = ConvertXlColumnIndexToLetter(1) & CStr(vintFieldTitlesRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(intColumnMax) & CStr(.UsedRange.Rows.Count)
                
                DecorateRangeAboutBordersAndFont .Range(strRange), venmRecordCellsBorders, vsngFontSize
                
                AdjustRowHeight .Rows(CStr(vintFieldTitlesRowIndex + 1) & ":" & CStr(.UsedRange.Rows.Count)), True, vsngRowsHeight
            End If
        End If
    End With
End Sub


'''
''' adjust records appearance when the data-table range has no field titles
'''
Public Sub DecorateRangeOfRecordsAboutBorderFontHeightIncludingNoFieldTitles(ByVal vobjSheet As Worksheet, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal venmRecordCellsBorders As RecordCellsBorders = RecordCellsBorders.RecordCellsBordersNone, _
        Optional ByVal vblnForceToSetSameRowHeight As Boolean = False, _
        Optional ByVal vsngFontSize As Single = 10)

    ' For no filed-titles

    Dim intColumnMax As Long, objTopLeftRange As Excel.Range, objTableRange As Excel.Range, strRange As String
    
    With vobjSheet
    
        Set objTopLeftRange = .Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex))

        Set objTableRange = objTopLeftRange.CurrentRegion
        
        With objTableRange
        
            If .Columns.Count = 1 And .Rows.Count = 1 Then
        
                strRange = ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)
            Else
                strRange = ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(.Columns.Count + vintTopLeftColumnIndex - 1) & CStr(.Rows.Count + vintTopLeftRowIndex - 1)
            End If
        End With
        
        DecorateRangeAboutBordersAndFont .Range(strRange), venmRecordCellsBorders, vsngFontSize, True
        
        AdjustRowHeight .Rows(CStr(vintTopLeftRowIndex) & ":" & CStr(objTableRange.Rows.Count + vintTopLeftRowIndex - 1)), True, 18
    End With
End Sub


'''
'''
'''
Public Sub AdjustRowHeight(ByVal vobjRange As Excel.Range, Optional ByVal vblnForceToSetSameRowHeight As Boolean = False, Optional vsngHeight As Single = 18)
    
    Dim strRange As String
    Dim i As Long, intFoundRangeCounter As Long
    Dim objCurrentRange As Excel.Range, objRowRange As Excel.Range
            
    With vobjRange
    
        strRange = CStr(.Row) & ":" & CStr(.Rows(.Rows.Count).Row)
        
        If vblnForceToSetSameRowHeight Then
        
            .Worksheet.Range(strRange).RowHeight = vsngHeight
        Else
            
            intFoundRangeCounter = 0
            
            For i = .Row To .Rows(.Rows.Count).Row
            
                Set objCurrentRange = .Worksheet.Rows(CStr(i) & ":" & CStr(i))
                
                With objCurrentRange
                
                    If .RowHeight < vsngHeight Then
                    
                        If intFoundRangeCounter = 0 Then
                        
                            Set objRowRange = objCurrentRange
                        Else
                            Set objRowRange = Union(objRowRange, objCurrentRange)
                            
                        End If
                        
                        intFoundRangeCounter = intFoundRangeCounter + 1
                    End If
                End With
            Next
            
            If intFoundRangeCounter > 0 Then
            
                objRowRange.RowHeight = vsngHeight
            End If
        End If
    End With
End Sub

'''
'''
'''
Public Sub AdjustRowHeightForIndividualCellLineFeedsCountOfSpecifiedColumns(ByRef robjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitlesCol As Collection, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vsngBaseHeight As Single = 18, _
        Optional ByVal vsngAdditionalHeightOfOneLine As Single = 14)


    Dim objFieldTitleToIndexDic As Scripting.Dictionary, objCurrentRegion As Excel.Range, i As Long, intColumnIndex As Long, strColumnLetter As String
    Dim objRange As Excel.Range, strValue As String, sngProperRowHeight As Single
    Dim varFieldTitle As Variant, intCountOfLines As Long


    Set objFieldTitleToIndexDic = GetFieldTitleToPositionIndexDic(vobjFieldTitlesCol, robjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)

    Set objCurrentRegion = robjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion


    With objCurrentRegion
    
        If .Rows.Count > 1 Then
        
            For Each varFieldTitle In objFieldTitleToIndexDic.Keys
        
                intColumnIndex = objFieldTitleToIndexDic.Item(varFieldTitle)
                
                strColumnLetter = ConvertXlColumnIndexToLetter(intColumnIndex)
        
                i = 2
                
                Do
                    Set objRange = .Range(strColumnLetter & CStr(i))
                    
                    If objRange.MergeCells Then
                    
                        With objRange.MergeArea
                        
                            strValue = .Value
                        
                            If strValue <> "" Then
                        
                                intCountOfLines = 1 + CountOfCellLineFeedChar(strValue)
                                
                                sngProperRowHeight = vsngBaseHeight + vsngAdditionalHeightOfOneLine * (intCountOfLines - 1)
                                
                                If .RowHeight < sngProperRowHeight Then
                                
                                    .Range(CStr(1) & ":" & CStr(.Rows.Count)).EntireRow.RowHeight = sngProperRowHeight
                                End If
                            End If
                            
                            i = i + .Rows.Count
                        End With
                    Else
                    
                        strValue = objRange.Value
                        
                        If strValue <> "" Then
                    
                            intCountOfLines = 1 + CountOfCellLineFeedChar(strValue)
                            
                            sngProperRowHeight = vsngBaseHeight + vsngAdditionalHeightOfOneLine * (intCountOfLines - 1)
                            
                            If objRange.RowHeight < sngProperRowHeight Then
                            
                                On Error Resume Next
                            
                                .Range(CStr(i) & ":" & CStr(i)).EntireRow.RowHeight = sngProperRowHeight
                                
                                If Err.Number <> 0 Then
                                
                                    Debug.Print Err.Description
                                    
                                    Debug.Assert False
                                End If
                                
                                On Error GoTo 0
                            End If
                        End If
                        
                        i = i + 1
                    End If
                
                Loop While i <= .Rows.Count
            Next
    
        End If
    End With

End Sub


'''
''' Return the column witdh size corresponding to the field name
'''
Public Function GetThisSystemDefaultColumnWidthDic() As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, objPlugInDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary   ' necessary

    Set objPlugInDic = GetDictionaryFromCallBackInterface("GetPlugInDefaultColumnWidthDic")

    If Not objPlugInDic Is Nothing Then
    
        UnionDoubleDictionariesToSingle objDic, objPlugInDic
    End If

    Set GetThisSystemDefaultColumnWidthDic = objDic
End Function


'''
''' adjust the column width of the loaded fields from the RDB by ColumnsNumberFormatLocalParam data
'''
Public Sub SetColumnNumberFormatLocal(ByVal vobjSheet As Worksheet, ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)
    
    ' Assertion
    Debug.Assert Not vobjColumnsNumberFormatLocalParam Is Nothing
    
    
    Dim i As Long, strColumnLetter As String, strRange As String, objCurrentRegion As Excel.Range
    Dim varNumberFormatLocal As Variant, strNumberFormatLocal As String
    Dim objFieldTitles As Collection, objFieldTitleToIndexDic As Scripting.Dictionary, varFieldTitle As Variant
    
    
    With vobjColumnsNumberFormatLocalParam
    
        Set objFieldTitleToIndexDic = GetFieldTitleToPositionIndexDic(.GetAllRelativeFieldTitles, vobjSheet, .FieldTitlesRowIndex, .TopLeftColumnIndex)
    End With
    
    If Not objFieldTitleToIndexDic Is Nothing Then
    
        If objFieldTitleToIndexDic.Count > 0 Then
        
            With vobjColumnsNumberFormatLocalParam
            
                Set objCurrentRegion = vobjSheet.Range(ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex)).CurrentRegion
            End With
        
            For Each varNumberFormatLocal In vobjColumnsNumberFormatLocalParam.NumberFormatLocalToFieldTitles.Keys
            
                strNumberFormatLocal = varNumberFormatLocal
                
                Set objFieldTitles = vobjColumnsNumberFormatLocalParam.NumberFormatLocalToFieldTitles.Item(varNumberFormatLocal)
                
                With vobjSheet
                    
                    For Each varFieldTitle In objFieldTitles
                    
                        i = objFieldTitleToIndexDic.Item(varFieldTitle) ' This column index i is a relative value based on CurrentRegion
                        
                        With objCurrentRegion
                        
                            strColumnLetter = ConvertXlColumnIndexToLetter(i)
                        
                            strRange = strColumnLetter & "2:" & strColumnLetter & CStr(.Rows.Count)
                            
                            With .Range(strRange)
                        
                                .NumberFormatLocal = strNumberFormatLocal
                            End With
                        End With
                    Next
                End With
            Next
        End If
    End If
End Sub


'''
''' set FormatCondition into records range within specified columns
'''
Public Sub SetColumnCellFormatCondition(ByVal vobjSheet As Excel.Worksheet, ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)
    
    Dim i As Long, strColumnLetter As String, strRange As String, objFormatConditionExpander As FormatConditionExpander
    Dim objFieldTitleToIndexDic As Scripting.Dictionary, varFieldTitle As Variant, objCurrentRegion As Excel.Range
    
    
    With vobjColumnsFormatConditionParam
    
        Set objFieldTitleToIndexDic = GetFieldTitleToPositionIndexDic(GetEnumeratorKeysColFromDic(.FieldTitleToCellFormatCondition), vobjSheet, .FieldTitlesRowIndex)
    End With
    
    If Not objFieldTitleToIndexDic Is Nothing Then
    
        If objFieldTitleToIndexDic.Count > 0 Then
        
            With vobjColumnsFormatConditionParam
            
                Set objCurrentRegion = vobjSheet.Range(ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex)).CurrentRegion
            End With
        
            With vobjSheet
                
                For Each varFieldTitle In objFieldTitleToIndexDic.Keys
                
                    i = objFieldTitleToIndexDic.Item(varFieldTitle) ' This column index i is a relative value based on CurrentRegion
                
                    strColumnLetter = ConvertXlColumnIndexToLetter(i)
                    
                    With objCurrentRegion
                    
                        strRange = strColumnLetter & "2:" & strColumnLetter & CStr(.Rows.Count)
                        
                        Set objFormatConditionExpander = vobjColumnsFormatConditionParam.FieldTitleToCellFormatCondition.Item(varFieldTitle)
                        
                        objFormatConditionExpander.ChangeCellFormatCondition .Range(strRange)
                    End With
                Next
            End With
        End If
    End If
End Sub

'''
'''
'''
Public Sub SetFormatCondtionTextFillColorsFromCellColorArrangement(ByRef robjFormatCondition As Excel.FormatCondition, ByVal venmCellColorArrangement As CellColorArrangement)

    With robjFormatCondition
    
         Select Case venmCellColorArrangement
             
             Case CellColorArrangement.FontDeepGreenBgLightGreen
             
                 With .Font
                 
                     .Color = RGB(0, 97, 0) ' -16752384
                     
                     .TintAndShade = 0
                 End With
                 
                 With .Interior
                     
                     .PatternColorIndex = xlAutomatic
                     
                     .Color = RGB(198, 239, 206) ' 13561798
                     
                     .TintAndShade = 0
                 End With
                
             Case CellColorArrangement.FontDeepRedBgLightRed
             
                 With .Font
                 
                     .Color = RGB(156, 0, 6) ' -16383844
                     
                     .TintAndShade = 0
                 End With
                 
                 With .Interior
                     
                     .PatternColorIndex = xlAutomatic
                     
                     .Color = RGB(255, 199, 206) ' 13551615
                     
                     .TintAndShade = 0
                 End With
                
             Case CellColorArrangement.FontDeepPurpleBgLightPurpleGradiation
                 
                 .Font.Color = RGB(74, 32, 106)
                 
                 With .Interior
                 
                     .Pattern = xlPatternLinearGradient
                     
                     With .Gradient
                     
                        .Degree = 90
                        
                        .ColorStops.Clear
                        
                        With .ColorStops.Add(0): .Color = RGB(225, 204, 240): End With
                        
                        With .ColorStops.Add(0.5): .Color = RGB(177, 126, 216): End With
                        
                        With .ColorStops.Add(1): .Color = RGB(225, 204, 240): End With
                     End With
                 End With
                 
             Case CellColorArrangement.FontDeepBrownBgLightYellowTiltedGradiation
                 
                 .Font.Color = RGB(69, 43, 31)
                 
                 
                 With .Interior
                 
                     .Pattern = xlPatternLinearGradient
                     
                     With .Gradient
                     
                        .Degree = 35
                        
                        .ColorStops.Clear
                        
                        With .ColorStops.Add(0): .Color = RGB(245, 246, 194): End With
                        
                        With .ColorStops.Add(0.5): .Color = RGB(225, 229, 75): End With
                        
                        With .ColorStops.Add(1): .Color = RGB(245, 246, 194): End With
                     End With
                 End With
            
            Case CellColorArrangement.FontDeepBlueBgLightBlue
            
                .Font.Color = RGB(37, 64, 113)
                 
                 With .Interior
                 
                     .Pattern = xlPatternLinearGradient
                     
                     With .Gradient
                     
                        .Degree = 90
                        
                        .ColorStops.Clear
                        
                        With .ColorStops.Add(0): .Color = RGB(203, 217, 241): End With
                        
                        With .ColorStops.Add(0.5): .Color = RGB(154, 174, 230): End With
                        
                        With .ColorStops.Add(1): .Color = RGB(203, 217, 241): End With
                     End With
                 End With
            
            Case CellColorArrangement.FontDeepGrayBgLightPink
            
                .Font.Color = RGB(70, 70, 70)
                 
                 With .Interior
                 
                     .Pattern = xlPatternLinearGradient
                     
                     With .Gradient
                     
                        .Degree = 90
                        
                        .ColorStops.Clear
                        
                        With .ColorStops.Add(0): .Color = RGB(253, 211, 242): End With
                        
                        With .ColorStops.Add(0.5): .Color = RGB(255, 245, 255): End With
                        
                        With .ColorStops.Add(1): .Color = RGB(253, 211, 242): End With
                     End With
                 End With
                 
            Case CellColorArrangement.FontPaleGrayBgDefault
            
                .Font.Color = RGB(140, 140, 140)
            
        End Select
    End With
End Sub


'''
'''
'''
Public Sub OutputFieldTitlesToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFieldTitlesCol As Collection, _
        Optional vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)

    Dim varValues() As Variant
    
    GetFieldTitleRCValueAndCellFormattingOnSheet varValues, vobjSheet, vobjFieldTitlesCol, vintTopLeftRowIndex, vintTopLeftColumnIndex, False, True
    
    vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Resize(1, vobjFieldTitlesCol.Count).Value = varValues
End Sub


'''
'''
'''
Public Sub GetFieldTitleRCValueAndCellFormattingOnSheet(ByRef rvarValues As Variant, _
        ByRef robjSheet As Excel.Worksheet, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vintTopLeftColumnIndex As Long, _
        ByVal vintTopLeftRowIndex As Long, _
        Optional ByVal vblnAllowToTranspose As Boolean = False, _
        Optional ByVal vblnAllowToSetNumberFormatLocalForZeroCharacterString As Boolean = True)

    Dim varItem As Variant, objRange As Excel.Range, i As Long
    
    If vblnAllowToTranspose Then
    
        ReDim rvarValues(1 To robjFieldTitlesCol.Count, 1 To 1)
    Else
        ReDim rvarValues(1 To 1, 1 To robjFieldTitlesCol.Count)
    End If
    
    i = 1
    
    For Each varItem In robjFieldTitlesCol
    
        If vblnAllowToSetNumberFormatLocalForZeroCharacterString Then
        
            If IsNumeric(varItem) And StrComp(Left(varItem, 1), "0") = 0 Then
                
                If vblnAllowToTranspose Then
                
                    Set objRange = robjSheet.Cells(vintTopLeftRowIndex + i - 1, vintTopLeftColumnIndex)
                Else
                    Set objRange = robjSheet.Cells(vintTopLeftRowIndex, vintTopLeftColumnIndex + i - 1)
                End If
                
                objRange.NumberFormatLocal = "@"
            End If
        End If
        
        If vblnAllowToTranspose Then
        
            rvarValues(i, 1) = CStr(varItem)
        Else
            rvarValues(1, i) = CStr(varItem)
        End If
        
        i = i + 1
    Next
End Sub

'''
'''
'''
Public Sub DecorateRangeOfFieldTitlesForRDB(ByVal vobjRange As Excel.Range, _
        Optional venmInteriorType As FieldTitleInterior = FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQL)
    
    Select Case venmInteriorType
    
        Case FieldTitleInterior.ColumnNameInteriorOfDefaultTypeSQL
        
            DecorateFieldTitleFontAndInteriorForDefaultTypeRDB vobjRange
        
        Case FieldTitleInterior.ColumnNameInteriorOfGatheredColTable
        
            DecorateFieldTitleFontAndInteriorForGatheredColTable vobjRange
    
        Case FieldTitleInterior.ColumnNameInteriorOfExcelSheetAdoSQL
    
            DecorateFieldTitleFontAndInteriorForExcelSheetSQL vobjRange
    
        Case FieldTitleInterior.ColumnNameInteriorOrMSAccessRDBSQL
        
            DecorateFieldTitleFontAndInteriorForMSAccessRDBSQL vobjRange
    
        Case FieldTitleInterior.ColumnNameInteriorOfCsvAdoSQL
    
            DecorateFieldTitleFontAndInteriorForCsvAdoSQL vobjRange
    
        Case FieldTitleInterior.ColumnNameInteriorOfLoadedINIFileAsTable
        
            DecorateFieldTitleFontAndInteriorForLoadedINIFileAsTable vobjRange
        
        Case FieldTitleInterior.ColumnNameInteriorOfLoadedWinRegistryAsTable
        
            DecorateFieldTitleFontAndInteriorForLoadedWinRegistryAsTable vobjRange
            
        Case FieldTitleInterior.ColumnNameInteriorOfExtractedTableFromSheet
        
            DecorateFieldTitleFontAndInteriorForExtractedTableFromSheet vobjRange
    
        Case FieldTitleInterior.ColumnNameInteriorOfMetaProperties
    
            DecorateFieldTitleFontAndInteriorForMetaProperties vobjRange
    
        Case ColumnNameInteriorOfPostgreSQL
        
            DecorateFieldTitleFontAndInteriorForInteriorPostgreSQLRDB vobjRange
    
        Case ColumnNameInteriorOfGatheredColTableByOrangeTone
    
            DecorateFieldTitleFontAndInteriorForGatheredColTableByOrangeTone vobjRange
    
        Case ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo
        
            DecorateFieldTitleFontAndInteriorForDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo vobjRange
            
        Case ColumnNameInteriorOfDefaultTypeSQLBySepiaFontLightToDarkBrownGraduationBgMeiryo
        
            DecorateFieldTitleFontAndInteriorForDefaultTypeSQLBySepiaFontLightToDarkBrownGraduationBgMeiryo vobjRange
        
    End Select
    
    DecorateFieldTitleCellsBordersForCommon vobjRange, venmInteriorType
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** get some typical type DataTableSheetFormatter objects
'**---------------------------------------------
'''
''' for Dictionary(Of Key, Value) object
'''
Public Function GetKeyValueDataTableSheetFormatter(Optional ByVal vstrKeyValueFieldTitlesDelimitedByComma As String = "Key,Value", Optional ByVal vintKeyValueColumnWidth As Long = 15) As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter, varTitle As Variant
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        Set .FieldTitleToColumnWidthDic = New Scripting.Dictionary
        
        For Each varTitle In Split(vstrKeyValueFieldTitlesDelimitedByComma, ",")
        
            .FieldTitleToColumnWidthDic.Add CStr(varTitle), CDbl(vintKeyValueColumnWidth)
        Next
        
        .RecordBordersType = RecordCellsGrayAll
    End With

    Set GetKeyValueDataTableSheetFormatter = objDTSheetFormatter
End Function


'''
''' for Dictionary(Of NestedFirstKey, Dictionary(Of Key, Value)) object
'''
Public Function GetOneNestedKeyValueDicDataTableSheetFormatter(Optional ByVal vstrFieldTitleToColumnWidthDelimitedByComma As String = "Section,15,Key,15,Value,15") As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter, varTitle As Variant
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar(vstrFieldTitleToColumnWidthDelimitedByComma)
        
        .RecordBordersType = RecordCellsGrayAll
        
        
        .AllowToMergeCellsByContinuousSameValues = True
        
        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        Set .FieldTitlesToMergeCells = GetEnumeratorKeysColFromDic(.FieldTitleToColumnWidthDic)
    End With

    Set GetOneNestedKeyValueDicDataTableSheetFormatter = objDTSheetFormatter
End Function

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' sanity test of UBound(<variable>, <dimension>) function
'''
Private Sub msubDebugUBoundTest()

    Dim varTable(3, 5) As Variant
    
    Debug.Print UBound(varTable, 1) & ", " & UBound(varTable, 2)
End Sub
