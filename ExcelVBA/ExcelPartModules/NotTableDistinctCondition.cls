VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "NotTableDistinctCondition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Data-class to distinct a data-table and additional informations for a not-table format CurrentRegion on a Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on FindForNotTablesOnSheet.bas, VariantTypeConversion.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 20/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjDistinctLineValuesPatternKeyToProcessingFlagDic As Scripting.Dictionary ' Dictionary(Of String[CellValuesPatternDelimitedComma], ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag)

Private mobjLineValuesPatternKeyToCorrectedValuesStringDic As Scripting.Dictionary  ' Dictionary(Of String[CellValuesPatternDelimitedComma], String[CorrectedValuesPatternDelimitedComma])

Private mobjExtractTableLineValuesPatternKeyToProcessingFlagDic As Scripting.Dictionary ' Dictionary(Of String[CellValuesPatternDelimitedComma], ProcessTypeFlagAfterSomeLineValuesPatternMatchedFlag)


'Private mstrDistinctCellValuesPatternKey As String  ' CellValuesPatternDelimitedComma

Private mstrDistinctIncludedKeyword As String

' The following is decided by each local rule
Private mstrDistinctedTableTypeName As String

Private mstrCorrectedColumnHeaderTitlesPattern As String

Private mobjAdditionalInfoTypeNameToExtractionRegExpPatternDic As Scripting.Dictionary  ' Dictionary(Of String[AdditionalInfoTypeName], String[RegExpPattern for extracting information])


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get DistinctLineValuesPatternKeyToProcessingFlagDic() As Scripting.Dictionary

    Set DistinctLineValuesPatternKeyToProcessingFlagDic = mobjDistinctLineValuesPatternKeyToProcessingFlagDic
End Property
Public Property Set DistinctLineValuesPatternKeyToProcessingFlagDic(ByVal vobjDistinctLineValuesPatternKeyToProcessingFlagDic As Scripting.Dictionary)

    Set mobjDistinctLineValuesPatternKeyToProcessingFlagDic = vobjDistinctLineValuesPatternKeyToProcessingFlagDic
End Property

'''
'''
'''
Public Property Get LineValuesPatternKeyToCorrectedValuesStringDic() As Scripting.Dictionary

    Set LineValuesPatternKeyToCorrectedValuesStringDic = mobjLineValuesPatternKeyToCorrectedValuesStringDic
End Property
Public Property Set LineValuesPatternKeyToCorrectedValuesStringDic(ByVal vobjLineValuesPatternKeyToCorrectedValuesStringDic As Scripting.Dictionary)

    Set mobjLineValuesPatternKeyToCorrectedValuesStringDic = vobjLineValuesPatternKeyToCorrectedValuesStringDic
End Property

'''
'''
'''
Public Property Get ExtractTableLineValuesPatternKeyToProcessingFlagDic() As Scripting.Dictionary

    Set ExtractTableLineValuesPatternKeyToProcessingFlagDic = mobjExtractTableLineValuesPatternKeyToProcessingFlagDic
End Property
Public Property Set ExtractTableLineValuesPatternKeyToProcessingFlagDic(ByVal vobjExtractTableLineValuesPatternKeyToProcessingFlagDic As Scripting.Dictionary)

    Set mobjExtractTableLineValuesPatternKeyToProcessingFlagDic = vobjExtractTableLineValuesPatternKeyToProcessingFlagDic
End Property

'''
'''
'''
Public Property Get DistinctIncludedKeyword() As String

    DistinctIncludedKeyword = mstrDistinctIncludedKeyword
End Property
Public Property Let DistinctIncludedKeyword(ByVal vstrDistinctIncludedKeyword As String)

    mstrDistinctIncludedKeyword = vstrDistinctIncludedKeyword
End Property

'''
'''
'''
Public Property Get DistinctedTableTypeName() As String

    If mstrDistinctedTableTypeName = "" Then
    
        mstrDistinctedTableTypeName = Me.DistinctIncludedKeyword
    End If

    DistinctedTableTypeName = mstrDistinctedTableTypeName
End Property
Public Property Let DistinctedTableTypeName(ByVal vstrDistinctedTableTypeName As String)

    mstrDistinctedTableTypeName = vstrDistinctedTableTypeName
End Property

'''
'''
'''
Public Property Get CorrectedColumnHeaderTitlesPattern() As String

    CorrectedColumnHeaderTitlesPattern = mstrCorrectedColumnHeaderTitlesPattern
End Property
Public Property Let CorrectedColumnHeaderTitlesPattern(ByVal vstrCorrectedColumnHeaderTitlesPattern As String)

    mstrCorrectedColumnHeaderTitlesPattern = vstrCorrectedColumnHeaderTitlesPattern
End Property

'''
''' Dictionary(Of String[AdditionalInfoTypeName], String[RegExpPattern for extracting information])
'''
Public Property Get AdditionalInfoTypeNameToExtractionRegExpPatternDic() As Scripting.Dictionary

    Set AdditionalInfoTypeNameToExtractionRegExpPatternDic = mobjAdditionalInfoTypeNameToExtractionRegExpPatternDic
End Property
Public Property Set AdditionalInfoTypeNameToExtractionRegExpPatternDic(ByVal vobjAdditionalInfoTypeNameToExtractionRegExpPatternDic As Scripting.Dictionary)

    Set mobjAdditionalInfoTypeNameToExtractionRegExpPatternDic = vobjAdditionalInfoTypeNameToExtractionRegExpPatternDic
End Property



