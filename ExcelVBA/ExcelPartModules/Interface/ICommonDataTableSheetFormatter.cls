VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ICommonDataTableSheetFormatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'
'   Interface class - common interfaces of both DataTableSheetFormatter class and TrDataTableSheetFormatter class (transposed-DataTableSheetFormatter)
'
'   This has no relation with Excel.DataTable class of Chart object.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Independent on either the ADO or the SQL
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 10/Feb/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Property interfaces
'///////////////////////////////////////////////

Public TopLeftRowIndex As Long

Public TopLeftColumnIndex As Long


Public AllowToShowFieldTitle As Boolean

Public FieldTitleInteriorType As FieldTitleInterior

Public RecordBordersType As RecordCellsBorders

Public RecordsFontSize As Single


Public AllowToConvertDataForEachFieldTitles As Boolean

Public FieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary


Public AllowToMergeCellsByContinuousSameValues As Boolean

Public AllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean

Public FieldTitlesToMergeCells As Collection


Public AllowToSetEnablingWrapText As Boolean

Public FieldTitlesToSetEnablingWrapTextCells As Collection

Public FieldTitleToHorizontalAlignmentDic As Scripting.Dictionary


Public AllowToAutoFitRowsHeight As Boolean


