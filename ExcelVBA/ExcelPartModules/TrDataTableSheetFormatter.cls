VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "TrDataTableSheetFormatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   A general transposed-table format parameters class for Excel Worksheet objects, however this is dependent on either the ADO or the SQL language
'
'   This has no relation with Excel.DataTable class of Excel.Chart class.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Independent on either the ADO or the SQL
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 10/Feb/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements ITrDataTableSheetFormatter

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjCommonDataTableSheetFormatter As CommonDataTableSheetFormatter

Private mitfCommonDataTableSheetFormatter As ICommonDataTableSheetFormatter

Private mintFieldTitlesColumnIndex As Long


Private mobjInfoTypeToColumnWidthDic As Scripting.Dictionary   ' Dictionary(Of String(row title), Double(column-width))


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub Class_Initialize()

    Set mobjCommonDataTableSheetFormatter = New CommonDataTableSheetFormatter

    Set mitfCommonDataTableSheetFormatter = mobjCommonDataTableSheetFormatter

    Me.TransposeDataTableSheetFormattingInterface.RecordBordersType = RecordCellsBlacksAndGrayInsideHorizontal

    mintFieldTitlesColumnIndex = 1
End Sub

'''
'''
'''
Private Sub Class_Terminate()

    Set mitfCommonDataTableSheetFormatter = Nothing
    
    Set mobjCommonDataTableSheetFormatter = Nothing
End Sub


'///////////////////////////////////////////////
'/// Implemented properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** For ICommonDataTableSheetFormatter
'**---------------------------------------------
'''
'''
'''
Private Property Get ITrDataTableSheetFormatter_TopLeftRowIndex() As Long

    ITrDataTableSheetFormatter_TopLeftRowIndex = mitfCommonDataTableSheetFormatter.TopLeftRowIndex
End Property
Private Property Let ITrDataTableSheetFormatter_TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)

    mitfCommonDataTableSheetFormatter.TopLeftRowIndex = vintTopLeftRowIndex
End Property

Private Property Get ITrDataTableSheetFormatter_TopLeftColumnIndex() As Long

    ITrDataTableSheetFormatter_TopLeftColumnIndex = mitfCommonDataTableSheetFormatter.TopLeftColumnIndex
End Property
Private Property Let ITrDataTableSheetFormatter_TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)

    mitfCommonDataTableSheetFormatter.TopLeftColumnIndex = vintTopLeftColumnIndex
End Property



Private Property Get ITrDataTableSheetFormatter_AllowToShowFieldTitle() As Boolean

    ITrDataTableSheetFormatter_AllowToShowFieldTitle = mitfCommonDataTableSheetFormatter.AllowToShowFieldTitle
End Property
Private Property Let ITrDataTableSheetFormatter_AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToShowFieldTitle = vblnAllowToShowFieldTitle
End Property

Private Property Get ITrDataTableSheetFormatter_FieldTitleInteriorType() As FieldTitleInterior

    ITrDataTableSheetFormatter_FieldTitleInteriorType = mitfCommonDataTableSheetFormatter.FieldTitleInteriorType
End Property
Private Property Let ITrDataTableSheetFormatter_FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)

    mitfCommonDataTableSheetFormatter.FieldTitleInteriorType = venmFieldTitleInteriorType
End Property

Private Property Get ITrDataTableSheetFormatter_RecordBordersType() As RecordCellsBorders

    ITrDataTableSheetFormatter_RecordBordersType = mitfCommonDataTableSheetFormatter.RecordBordersType
End Property
Private Property Let ITrDataTableSheetFormatter_RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)

    mitfCommonDataTableSheetFormatter.RecordBordersType = venmRecordCellsBordersType
End Property

Private Property Get ITrDataTableSheetFormatter_RecordsFontSize() As Single

    ITrDataTableSheetFormatter_RecordsFontSize = mitfCommonDataTableSheetFormatter.RecordsFontSize
End Property
Private Property Let ITrDataTableSheetFormatter_RecordsFontSize(ByVal vsngRecordsFontSize As Single)

    mitfCommonDataTableSheetFormatter.RecordsFontSize = vsngRecordsFontSize
End Property


'''
''' When it is true, then ConvertDataForColumns procedure is used.
'''
Private Property Get ITrDataTableSheetFormatter_AllowToConvertDataForSpecifiedRows() As Boolean

    ITrDataTableSheetFormatter_AllowToConvertDataForSpecifiedRows = mitfCommonDataTableSheetFormatter.AllowToConvertDataForEachFieldTitles
End Property
Private Property Let ITrDataTableSheetFormatter_AllowToConvertDataForSpecifiedRows(ByVal vblnAllowToConvertDataForSpecifiedRows As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToConvertDataForEachFieldTitles = vblnAllowToConvertDataForSpecifiedRows
End Property


'''
''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
'''
Private Property Set ITrDataTableSheetFormatter_FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)

    Set mitfCommonDataTableSheetFormatter.FieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
End Property
Private Property Get ITrDataTableSheetFormatter_FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary

    Set ITrDataTableSheetFormatter_FieldTitlesToColumnDataConvertTypeDic = mitfCommonDataTableSheetFormatter.FieldTitlesToColumnDataConvertTypeDic
End Property



'''
''' merge cells when the same values continue
'''
Private Property Get ITrDataTableSheetFormatter_AllowToMergeCellsByContinuousSameValues() As Boolean

    ITrDataTableSheetFormatter_AllowToMergeCellsByContinuousSameValues = mitfCommonDataTableSheetFormatter.AllowToMergeCellsByContinuousSameValues
End Property
Private Property Let ITrDataTableSheetFormatter_AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
End Property


Private Property Get ITrDataTableSheetFormatter_AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean

   ITrDataTableSheetFormatter_AllowToSetVerticalTopAlignmentTopOfMergeCells = mitfCommonDataTableSheetFormatter.AllowToSetVerticalTopAlignmentTopOfMergeCells
End Property
Private Property Let ITrDataTableSheetFormatter_AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
End Property


Private Property Get ITrDataTableSheetFormatter_FieldTitlesToMergeCells() As Collection

    Set ITrDataTableSheetFormatter_FieldTitlesToMergeCells = mitfCommonDataTableSheetFormatter.FieldTitlesToMergeCells
End Property
Private Property Set ITrDataTableSheetFormatter_FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)

    Set mitfCommonDataTableSheetFormatter.FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
End Property



'''
''' Enabling WrapText for each cells
'''
Private Property Get ITrDataTableSheetFormatter_AllowToSetEnablingWrapText() As Boolean

    ITrDataTableSheetFormatter_AllowToSetEnablingWrapText = mitfCommonDataTableSheetFormatter.AllowToSetEnablingWrapText
End Property
Private Property Let ITrDataTableSheetFormatter_AllowToSetEnablingWrapText(ByVal vblnAllowToSetEnablingWrapText As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToSetEnablingWrapText = vblnAllowToSetEnablingWrapText
End Property


Private Property Get ITrDataTableSheetFormatter_FieldTitlesToSetEnablingWrapTextCells() As Collection

    Set ITrDataTableSheetFormatter_FieldTitlesToSetEnablingWrapTextCells = mitfCommonDataTableSheetFormatter.FieldTitlesToSetEnablingWrapTextCells
End Property
Private Property Set ITrDataTableSheetFormatter_FieldTitlesToSetEnablingWrapTextCells(ByVal vobjFieldTitlesToSetEnablingWrapTextCells As Collection)

    Set mitfCommonDataTableSheetFormatter.FieldTitlesToSetEnablingWrapTextCells = vobjFieldTitlesToSetEnablingWrapTextCells
End Property

'''
''' Dictionary(Of String[Field-title], Excel.Constants)
'''
Private Property Get ITrDataTableSheetFormatter_FieldTitleToHorizontalAlignmentDic() As Scripting.Dictionary

    Set ITrDataTableSheetFormatter_FieldTitleToHorizontalAlignmentDic = mitfCommonDataTableSheetFormatter.FieldTitleToHorizontalAlignmentDic
End Property
Private Property Set ITrDataTableSheetFormatter_FieldTitleToHorizontalAlignmentDic(ByVal vobjDic As Scripting.Dictionary)

    Set mitfCommonDataTableSheetFormatter.FieldTitleToHorizontalAlignmentDic = vobjDic
End Property

'''
''' If mblnAllowToAutoFitRowsHeight is true, Excel.Worksheet.Rows.AutoFit method is to be executed
'''
Private Property Get ITrDataTableSheetFormatter_AllowToAutoFitRowsHeight() As Boolean

    ITrDataTableSheetFormatter_AllowToAutoFitRowsHeight = mitfCommonDataTableSheetFormatter.AllowToAutoFitRowsHeight
End Property
Private Property Let ITrDataTableSheetFormatter_AllowToAutoFitRowsHeight(ByVal vblnAllowToAutoFitRowsHeight As Boolean)

    mitfCommonDataTableSheetFormatter.AllowToAutoFitRowsHeight = vblnAllowToAutoFitRowsHeight
End Property


'**---------------------------------------------
'** For only ITrDataTableSheetFormatter
'**---------------------------------------------
Private Property Get ITrDataTableSheetFormatter_FieldTitlesColumnIndex() As Long

    ITrDataTableSheetFormatter_FieldTitlesColumnIndex = mintFieldTitlesColumnIndex
End Property
Private Property Let ITrDataTableSheetFormatter_FieldTitlesColumnIndex(ByVal vintFieldTitlesColumnIndex As Long)

    mintFieldTitlesColumnIndex = vintFieldTitlesColumnIndex
End Property

'''
'''
'''
Private Property Get ITrDataTableSheetFormatter_InfoTypeToColumnWidthDic() As Scripting.IDictionary

    Set ITrDataTableSheetFormatter_InfoTypeToColumnWidthDic = mobjInfoTypeToColumnWidthDic
End Property
Private Property Set ITrDataTableSheetFormatter_InfoTypeToColumnWidthDic(ByVal vobjInfoTypeToColumnWidthDic As Scripting.IDictionary)

    Set mobjInfoTypeToColumnWidthDic = vobjInfoTypeToColumnWidthDic
End Property


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** Public interface read-only properties
'**---------------------------------------------
'''
'''
'''
Public Property Get TransposeDataTableSheetFormattingInterface() As ITrDataTableSheetFormatter

    Set TransposeDataTableSheetFormattingInterface = Me
End Property

'''
'''
'''
Public Property Get CommonDataTableSheetFormattingInterface() As ICommonDataTableSheetFormatter

    Set CommonDataTableSheetFormattingInterface = mitfCommonDataTableSheetFormatter
End Property

'**---------------------------------------------
'** (Transpose) TrDataTableSheetFormatter properties
'**---------------------------------------------
'''
'''
'''
Public Property Get InfoTypeToColumnWidthDic() As Scripting.Dictionary

    Set InfoTypeToColumnWidthDic = ITrDataTableSheetFormatter_InfoTypeToColumnWidthDic
End Property
Public Property Set InfoTypeToColumnWidthDic(ByVal vobjInfoTypeToColumnWidthDic As Scripting.Dictionary)

    Set ITrDataTableSheetFormatter_InfoTypeToColumnWidthDic = vobjInfoTypeToColumnWidthDic
End Property

'''
'''
'''
Public Property Get TopLeftRowIndex() As Long

    TopLeftRowIndex = ITrDataTableSheetFormatter_TopLeftRowIndex
End Property
Public Property Let TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)

    ITrDataTableSheetFormatter_TopLeftRowIndex = vintTopLeftRowIndex
End Property

'''
'''
'''
Public Property Get TopLeftColumnIndex() As Long

    TopLeftColumnIndex = ITrDataTableSheetFormatter_TopLeftColumnIndex
End Property
Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)

    ITrDataTableSheetFormatter_TopLeftColumnIndex = vintTopLeftColumnIndex
End Property

'''
'''
'''
Public Property Get FieldTitlesColumnIndex() As Long

    FieldTitlesColumnIndex = ITrDataTableSheetFormatter_FieldTitlesColumnIndex
End Property
Public Property Let FieldTitlesColumnIndex(ByVal vintFieldTitlesColumnIndex As Long)

    ITrDataTableSheetFormatter_FieldTitlesColumnIndex = vintFieldTitlesColumnIndex
End Property

'''
'''
'''
Public Property Get AllowToShowFieldTitle() As Boolean

    AllowToShowFieldTitle = ITrDataTableSheetFormatter_AllowToShowFieldTitle
End Property
Public Property Let AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)

    ITrDataTableSheetFormatter_AllowToShowFieldTitle = vblnAllowToShowFieldTitle
End Property

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////

'''
'''
'''
Public Sub SetSheetFormatBeforeTableOut()

    mintFieldTitlesColumnIndex = mitfCommonDataTableSheetFormatter.TopLeftColumnIndex

'    If Not Me.HeaderInsertTexts Is Nothing And mblnAllowToWriteInsertTextInSheetHeader Then
'
'        mintFieldTitlesRowIndex = mintFieldTitlesRowIndex + Me.HeaderInsertTexts.Count
'    End If

    If Not Me.AllowToShowFieldTitle Then
    
        mintFieldTitlesColumnIndex = mintFieldTitlesColumnIndex - 1
    End If
End Sub

'''
'''
'''
Public Sub SetTransposeDataTableSheetFormatAfterTableOut(ByVal vobjSheet As Excel.Worksheet)

    Dim itfTrDataTableSheetFormatter As ITrDataTableSheetFormatter

    Set itfTrDataTableSheetFormatter = Me

    With itfTrDataTableSheetFormatter
    
        If .AllowToShowFieldTitle Then
        
            AdjustColumnWidthByInfoTypeWithFindingRightDirectionForTransposeOutput vobjSheet, .InfoTypeToColumnWidthDic, .TopLeftRowIndex, .FieldTitlesColumnIndex
            
            DecorateTransposeRangeOfFields vobjSheet, .TopLeftRowIndex, .FieldTitlesColumnIndex, .FieldTitleInteriorType
        End If
        
        msubSetTransposeRecordPartSheetFormatAfterTableOut vobjSheet
        
    End With
End Sub


'''
'''
'''
Private Sub msubSetTransposeRecordPartSheetFormatAfterTableOut(ByVal vobjSheet As Excel.Worksheet)

    Dim itfTrDataTableSheetFormatter As ITrDataTableSheetFormatter

    Set itfTrDataTableSheetFormatter = Me
    
    With itfTrDataTableSheetFormatter
    
        If .AllowToShowFieldTitle Then
    
        
    
'            If .AllowToConvertDataForEachFieldTitles And Not .FieldTitlesToColumnDataConvertTypeDic Is Nothing Then
'
'                If .FieldTitlesToColumnDataConvertTypeDic.Count > 0 Then
'
'                    ConvertDataForColumns vobjSheet, .FieldTitlesToColumnDataConvertTypeDic, Me.TopLeftRowIndex, Me.FieldTitlesColumnIndex
'                End If
'            End If
            
            
'            If Not Me.ColumnsNumberFormatLocal Is Nothing Then
'
'                With Me.ColumnsNumberFormatLocal
'
'                    .TopLeftRowIndex = Me.TopLeftRowIndex
'
'                    .FieldTitlesColumnIndex = Me.FieldTitlesColumnIndex
'                End With
'
'                SetColumnNumberFormatLocal vobjSheet, Me.ColumnsNumberFormatLocal
'            End If
'
'            If Not Me.ColumnsFormatCondition Is Nothing Then
'
'                With Me.ColumnsFormatCondition
'
'                    .TopLeftRowIndex = Me.TopLeftRowIndex
'
'                    .FieldTitlesColumnIndex = Me.FieldTitlesColumnIndex
'                End With
'
'                SetColumnCellFormatCondition vobjSheet, Me.ColumnsFormatCondition
'            End If
        
        End If
    
    
        If .AllowToShowFieldTitle Then
        
            DecorateTransposeRangeOfRecordsAboutBorderFontHeight vobjSheet, .TopLeftRowIndex, .FieldTitlesColumnIndex, .RecordBordersType, vsngFontSize:=.RecordsFontSize
        Else
            DecorateRangeOfRecordsAboutBorderFontHeightIncludingNoFieldTitles vobjSheet, Me.TopLeftRowIndex, Me.TopLeftColumnIndex, .RecordBordersType, vsngFontSize:=.RecordsFontSize
        End If
    

    

        If .AllowToShowFieldTitle Then
        
       
        
'            If .AllowToMergeCellsByContinuousSameValues And Not .FieldTitlesToMergeCells Is Nothing Then
'
'                If .FieldTitlesToMergeCells.Count > 0 Then
'
'                    SetMergedCellsToTable vobjSheet, .FieldTitlesToMergeCells, Me.FieldTitlesRowIndex, Me.TopLeftColumnIndex, .AllowToSetVerticalTopAlignmentTopOfMergeCells
'                End If
'            End If
'
'            ' Set Range.WrapText to True
'            If .AllowToSetEnablingWrapText And Not .FieldTitlesToSetEnablingWrapTextCells Is Nothing Then
'
'                If .FieldTitlesToSetEnablingWrapTextCells.Count > 0 Then
'
'                    SetEnablingWrapTextCellsToTable vobjSheet, .FieldTitlesToSetEnablingWrapTextCells, Me.FieldTitlesRowIndex, Me.TopLeftColumnIndex
'                End If
'            End If
        
        End If
    
    
    
'        ' Set Range.HorizontalAlignment
'        If Not .FieldTitleToHorizontalAlignmentDic Is Nothing Then
'
'            SetHorizontalAlignmentCellsToTable vobjSheet, .FieldTitleToHorizontalAlignmentDic, Me.FieldTitlesRowIndex, Me.TopLeftColumnIndex
'        End If
    
    
        If .AllowToAutoFitRowsHeight Then
            
            With vobjSheet.Range(ConvertXlColumnIndexToLetter(Me.TopLeftColumnIndex) & CStr(Me.TopLeftRowIndex)).CurrentRegion
            
                .Rows.AutoFit
            End With
            
            'vobjSheet.Rows.AutoFit
        End If
        
'        If Me.AllowToShowFieldTitle Then
'
'            SetFreezePanesOn vobjSheet, Me.FieldTitlesRowIndex
'        End If

    End With
End Sub

