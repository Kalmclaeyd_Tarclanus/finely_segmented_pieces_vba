Attribute VB_Name = "DataTableSecurityToReport"
'
'   output the state report of Excel.Worksheet on cells on sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 17/Feb/2024    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub OutputExcelBookSheetSecurityInformationToReportingBook(ByVal vstrReportingBookPath As String, ByVal vobjInvestigatingBook As Excel.Workbook)

    Dim objReportingBook As Excel.Workbook, objSheet As Excel.Worksheet, objInvestigatedResultDic As Scripting.Dictionary
    
    
    Set objReportingBook = GetWorkbookAndPrepareForUnitTest(vstrReportingBookPath)

    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objReportingBook)

    objSheet.Name = "SheetSecurityInfo"

    
    Set objInvestigatedResultDic = GetBookSheetSecurityInfoDic(vobjInvestigatingBook)

    OutputDicToSheet objSheet, objInvestigatedResultDic, GetColFromLineDelimitedChar("SheetName,SheetVisibility,SheetProtectContents,SheetProtectDrawingObjects"), mfobjGetDataTableSheetFormatterOfExcelBookSheetSecurityInformation()

    DeleteDummySheetWhenItExists objReportingBook
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToOutputExcelBookSheetSecurityInformationToReportingBook()

    Dim strOutputBookPath As String
    
    strOutputBookPath = mfstrGetDataTableSecurityBookInfoReportDir() & "\ExcelBookSheetSecurityInfo.xlsx"

    OutputExcelBookSheetSecurityInformationToReportingBook strOutputBookPath, ThisWorkbook
End Sub


'''
'''
'''
Private Function mfstrGetDataTableSecurityBookInfoReportDir() As String

    Dim strTmpDir As String
    
    strTmpDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\DataTableSecurityBookReportTest"

    If Not DirectoryExistsByVbaDir(strTmpDir) Then
    
        ForceToCreateDirectoryByVbaDir strTmpDir
    End If

    mfstrGetDataTableSecurityBookInfoReportDir = strTmpDir
End Function



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterOfExcelBookSheetSecurityInformation() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter

        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("SheetName,22,SheetVisibility,14,SheetProtectContents,20,SheetProtectDrawingObjects,26")
        
        .RecordBordersType = RecordCellsGrayAll

        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        .AllowToMergeCellsByContinuousSameValues = True
        
        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("SheetVisibility,SheetProtectContents,SheetProtectDrawingObjects")

    End With

    Set mfobjGetDataTableSheetFormatterOfExcelBookSheetSecurityInformation = objDTSheetFormatter
End Function






