Attribute VB_Name = "DataTableLogTextSheetOut"
'
'   logging text to sheet after getting results of using such as ADO or the SQL language
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Independent on either the ADO or the SQL
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  4/May/2023    Kalmclaeyd Tarclanus    Separated from DataTableSheetFormatter.cls
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** RegExp cache
'**---------------------------------------------
Private mobjDirectoryTagValueRegExp As VBScript_RegExp_55.RegExp

Private mobjFileTagValueRegExp As VBScript_RegExp_55.RegExp


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub InsertHeaderTextsIntoCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByRef robjHeaderInsertTexts As Collection, _
        Optional ByVal vintTopLeftRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
        Optional ByVal vblnAllowToWriteInsertTextInSheetHeader As Boolean = False, _
        Optional ByVal vblnAllowToMakeVacantFirstRow As Boolean = False)

    Dim blnIsDirectoryPathFound As Boolean
    
    Dim varText As Variant, strText As String, intP1 As Integer
    Dim strTexts() As String, objRowRange As Excel.Range
    Dim intRowIdx As Long
    
    
    If Not robjHeaderInsertTexts Is Nothing Then
        
        If vblnAllowToWriteInsertTextInSheetHeader Then
        
            intRowIdx = vintTopLeftRowIndex
        
            If vblnAllowToMakeVacantFirstRow Then
            
                intRowIdx = intRowIdx + 1   ' Row index 1 is ,for example, a SQL string
            End If
            
            If Not robjHeaderInsertTexts Is Nothing And vblnAllowToWriteInsertTextInSheetHeader Then
        
                Set objRowRange = Nothing
                
                With vobjSheet
                    
                    For Each varText In robjHeaderInsertTexts
                    
                        strText = CStr(varText)
                        
                        intP1 = InStr(1, strText, ":")
                        
                        With GetDirectoryTagValueRegExp()
                        
                            blnIsDirectoryPathFound = .Test(strText)
                        End With
                        
                        If Not blnIsDirectoryPathFound And (intP1 > 0) Then
                        
                            msubSetRangeTextForKeyAndValue vobjSheet, strText, intRowIdx, vintTopLeftColumnIndex
                        
                            msubUnionRangeAtCurrentRow objRowRange, intRowIdx, vobjSheet
                            
                        ElseIf blnIsDirectoryPathFound Then
                        
                            msubSetFileOrDirectoryPathRangeText vobjSheet, varText, intRowIdx, vintTopLeftColumnIndex
                        Else
                            msubSetOtherRangeText objRowRange, vobjSheet, varText, intRowIdx, vintTopLeftColumnIndex
                        End If
                        
                        intRowIdx = intRowIdx + 1
                    Next

                    If Not objRowRange Is Nothing Then
                    
                        objRowRange.RowHeight = 24
                    End If
                End With
            End If
        End If
    End If
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** RegExp settings
'**---------------------------------------------
'''
'''
'''
Private Function GetDirectoryTagValueRegExp() As VBScript_RegExp_55.RegExp

    If mobjDirectoryTagValueRegExp Is Nothing Then
    
        Set mobjDirectoryTagValueRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjDirectoryTagValueRegExp
        
            .Pattern = "(<CSV Directory>|<Directory>)[ \t]{0,}[a-zA-Z]{1}\:\\"
        End With
    End If

    Set GetDirectoryTagValueRegExp = mobjDirectoryTagValueRegExp
End Function

'''
'''
'''
Private Function GetFileTagValueRegExp() As VBScript_RegExp_55.RegExp

    If mobjFileTagValueRegExp Is Nothing Then
    
        Set mobjFileTagValueRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjFileTagValueRegExp
        
            .Pattern = "<File Name>[ \t]{0,}[\w\.]{1,}(xls|xlsx)"
        End With
    End If

    Set GetFileTagValueRegExp = mobjFileTagValueRegExp
End Function

Private Sub msubClearRegExpCache()

    Set mobjFileTagValueRegExp = Nothing
    
    Set mobjDirectoryTagValueRegExp = Nothing
End Sub

'**---------------------------------------------
'** logging on sheet
'**---------------------------------------------
'''
'''
'''
''' <Argument>robjSheet: Input-Output</Argument>
''' <Argument>rstrText: Input</Argument>
''' <Argument>rintRowIdx: Input</Argument>
''' <Argument>vintTopLeftColumnIndex: Input</Argument>
Private Sub msubSetRangeTextForKeyAndValue(ByRef robjSheet As Excel.Worksheet, _
        ByRef rstrText As String, _
        ByRef rintRowIdx As Long, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)

    Dim strTexts() As String
    
    With robjSheet
    
        strTexts = Split(rstrText, ":", 2)
                            
        With .Cells(rintRowIdx, vintTopLeftColumnIndex)
        
            ' description of the value
            
            .Value = strTexts(0)
            
            .Font.Size = 9: .VerticalAlignment = xlBottom
        End With
        
        With .Cells(rintRowIdx, vintTopLeftColumnIndex + 1)
        
            ' the value
        
            .Value = strTexts(1)
            
            .Font.Size = 12: .VerticalAlignment = xlBottom
        End With
        
        With .Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(rintRowIdx) & ":" & ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex + 1) & CStr(rintRowIdx)).Font
        
            .Name = "Meiryo": .FontStyle = "Regular"
            
            .Color = RGB(94, 94, 94) ' 6184542
            
            .TintAndShade = 0
        End With
    End With
End Sub

'''
'''
'''
Private Sub msubSetFileOrDirectoryPathRangeText(ByRef robjSheet As Excel.Worksheet, ByRef rvarText As Variant, ByRef rintRowIdx As Long, Optional ByVal vintTopLeftColumnIndex As Long = 1)

    Dim objDecorationTargetTextSettings As Collection

    With robjSheet

        With .Cells(rintRowIdx, vintTopLeftColumnIndex)
            
             .Value = rvarText
            
            With .Font
            
                .Color = RGB(128, 128, 128)
            
                .Name = "Arial Narrow": .FontStyle = "Regular"
            End With
        End With
        
        If objDecorationTargetTextSettings Is Nothing Then
            
            Set objDecorationTargetTextSettings = New Collection
            
            With objDecorationTargetTextSettings

                .Add "<File Name>;9.5;trRGBDarkOrangeFontColor"
                .Add "<Directory>;9.5;trRGBDarkOrangeFontColor"
                .Add "<CSV Directory>;9.5;trRGBDarkOrangeFontColor"
            End With
        End If
        
        ChageTextDecorationInRange .Cells(rintRowIdx, vintTopLeftColumnIndex), objDecorationTargetTextSettings
    End With
End Sub


'''
'''
'''
Private Sub msubSetOtherRangeText(ByRef robjRowRange As Excel.Range, ByRef robjSheet As Excel.Worksheet, ByRef rvarText As Variant, ByRef rintRowIdx As Long, Optional ByVal vintTopLeftColumnIndex As Long = 1)

    With robjSheet

        With .Cells(rintRowIdx, vintTopLeftColumnIndex)
            
            With .Font
            
                .Name = "Meiryo": .FontStyle = "Regular"
            End With
            
            If GetFileTagValueRegExp().Test(rvarText) Then
            
                .Value = rvarText    ' connected Excel book path
            
                With .Font
                
                    .TintAndShade = 0
                    
                    .ThemeFont = xlThemeFontNone
                    
                    .Size = 10
                End With
            Else
                .Value = rvarText
                
                .VerticalAlignment = xlBottom
                
                With .Font
                
                    .TintAndShade = 0
                    
                    .Color = RGB(94, 94, 94) ' 6184542
                    
                    .Size = 12
                End With
                
                msubUnionRangeAtCurrentRow robjRowRange, rintRowIdx, robjSheet
            End If
        End With
    End With

End Sub


'''
'''
'''
Private Sub msubUnionRangeAtCurrentRow(ByRef robjRowRange As Excel.Range, ByRef rintRowIdx As Long, ByRef robjSheet As Excel.Worksheet)

    With robjSheet
    
        If robjRowRange Is Nothing Then
        
            Set robjRowRange = .Range(CStr(rintRowIdx) & ":" & CStr(rintRowIdx))
        Else
            Set robjRowRange = Union(robjRowRange, .Range(CStr(rintRowIdx) & ":" & CStr(rintRowIdx)))
        End If
    End With
End Sub





