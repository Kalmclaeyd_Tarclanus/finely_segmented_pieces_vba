Attribute VB_Name = "CurrentUserDrivesUtilityForXl"
'
'   Output searched result Excel Worksheet with collecting this computer disk drives information
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Excel and Windows OS
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon,  6/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrTestOutputBookBaseName As String = "ThisComputerDrivesInfo"


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' Only ready drives
'''
Public Sub OutputThisComputerCurrentReadyDiskDriveCapacitiesToSheet()

    Dim objSheet As Excel.Worksheet, objDTCol As Collection
    
    Set objDTCol = GetThisComputerReadyDiskDrivesInfoDTCol()
    
    Set objSheet = mfobjExtractThisComputerReadyDrivesColToSheet(objDTCol, mfstrGetThisComputerDiskDrivesInfoBookPath(), "ThisComputerReadyDrives")

    msubAddThisDrivesSearchLog objSheet, objDTCol
End Sub


'''
'''
'''
Public Sub OutputThisComputerCurrentConnectedDiskDriveStateToSheet()

    Dim objSheet As Excel.Worksheet, objDTCol As Collection
    
    Set objDTCol = GetThisComputerDiskDrivesInfoDTCol()
    
    Set objSheet = mfobjExtractThisComputerDrivesColToSheet(objDTCol, mfstrGetThisComputerDiskDrivesInfoBookPath(), "ThisComputerDrives")

    msubAddThisDrivesSearchLog objSheet, objDTCol
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
Private Function mfstrGetThisComputerDiskDrivesInfoBookPath() As String

    mfstrGetThisComputerDiskDrivesInfoBookPath = GetCurrentBookOutputDir() & "\" & mstrTestOutputBookBaseName & ".xlsx"
End Function


'''
'''
'''
''' <Return>Excel.Worksheet</Return>
Private Function mfobjExtractThisComputerReadyDrivesColToSheet(ByVal vobjDTCol As Collection, ByVal vstrBookPath As String, ByVal vstrSheetName As String) As Excel.Worksheet
    
    Dim objSheet As Excel.Worksheet
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrBookPath, vstrSheetName)
    
    objSheet.Name = vstrSheetName

    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, vobjDTCol, mfobjGetDataTableSheetFormatterOfThisComputerReadyDrivesInfo()

    Set mfobjExtractThisComputerReadyDrivesColToSheet = objSheet
End Function

'''
'''
'''
''' <Return>Excel.Worksheet</Return>
Private Function mfobjExtractThisComputerDrivesColToSheet(ByVal vobjDTCol As Collection, ByVal vstrBookPath As String, ByVal vstrSheetName As String) As Excel.Worksheet
    
    Dim objSheet As Excel.Worksheet
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrBookPath, vstrSheetName)
    
    objSheet.Name = vstrSheetName

    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, vobjDTCol, mfobjGetDataTableSheetFormatterOfThisComputerDrivesInfo()

    Set mfobjExtractThisComputerDrivesColToSheet = objSheet
End Function

'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterOfThisComputerReadyDrivesInfo() As DataTableSheetFormatter

    Dim objDataTableSheetFormatter As DataTableSheetFormatter
    
    Set objDataTableSheetFormatter = New DataTableSheetFormatter
    
    With objDataTableSheetFormatter
        
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        
        .RecordBordersType = RecordCellsGrayAll
        
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("DriveLetter,9,DriveType,13.5,FileSystem,10,TotalSize,13.5,AvailableSpace,13.5,FreeSpace,13.5,DriveVolumeName,15,NetworkShareName,36.5")
        
        
        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
        
        With .ColumnsNumberFormatLocal
        
            .SetNumberFormatLocalAndApplyFieldTitles "#,##0.00_ ""GB""", GetColFromLineDelimitedChar("TotalSize,AvailableSpace,FreeSpace")
        End With
        
        .AllowToMergeCellsByContinuousSameValues = True
        
        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("DriveType,TotalSize,FileSystem")
        
    End With

    Set mfobjGetDataTableSheetFormatterOfThisComputerReadyDrivesInfo = objDataTableSheetFormatter
End Function


'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterOfThisComputerDrivesInfo() As DataTableSheetFormatter

    Dim objDataTableSheetFormatter As DataTableSheetFormatter, objFormatConditionExpander As FormatConditionExpander
    
    Set objDataTableSheetFormatter = New DataTableSheetFormatter
    
    With objDataTableSheetFormatter
        
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        .RecordBordersType = RecordCellsGrayAll
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("DriveLetter,9,IsReady,9,DriveType,13.5,FileSystem,10,TotalSize,13.5,AvailableSpace,13.5,FreeSpace,13.5,DriveVolumeName,15,NetworkShareName,36.5")
        
        Set .ColumnsFormatCondition = New ColumnsFormatConditionParam
        
        With .ColumnsFormatCondition
        
            Set objFormatConditionExpander = New FormatConditionExpander
            
            With objFormatConditionExpander
                
                '.AddTextContainCondition "True", FontDeepGreenBgLightGreen
                .AddTextContainCondition "False", FontDeepRedBgLightRed
            End With
        
            .FieldTitleToCellFormatCondition.Add "IsReady", objFormatConditionExpander
        End With
        
        
        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
        
        With .ColumnsNumberFormatLocal
        
            .SetNumberFormatLocalAndApplyFieldTitles "#,##0.00_ ""GB""", GetColFromLineDelimitedChar("TotalSize,AvailableSpace,FreeSpace")
        End With
        
        .AllowToMergeCellsByContinuousSameValues = True
        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("DriveType,TotalSize,FileSystem")
        
    End With

    Set mfobjGetDataTableSheetFormatterOfThisComputerDrivesInfo = objDataTableSheetFormatter
End Function


'''
'''
'''
Private Sub msubAddThisDrivesSearchLog(ByVal vobjSheet As Excel.Worksheet, ByVal vobjDataTableCol As Collection)

    Dim objDecorationTagValueSettings As Collection, objDecorationTargetTextSettings As Collection, strLog As String
    
    strLog = GetThisComputerDiskDrivesSearchLog(vobjDataTableCol, objDecorationTagValueSettings, objDecorationTargetTextSettings)

    AddAutoShapeGeneralMessage vobjSheet, strLog, vobjDecorationTagValueSettings:=objDecorationTagValueSettings, vobjDecorationTargetTextSettings:=objDecorationTargetTextSettings
End Sub
