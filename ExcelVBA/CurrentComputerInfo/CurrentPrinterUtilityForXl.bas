Attribute VB_Name = "CurrentPrinterUtilityForXl"
'
'   Output collected this computer registered printers data-table into a Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on either Windows OS or Access
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  1/Jun/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "CurrentPrinterUtilityForXl"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Key-Value cache preparation for CurrentPrinterUtilityForXl
'**---------------------------------------------
Private mobjStrKeyValueCurrentPrinterUtilityForXlDic As Scripting.Dictionary

Private mblnIsStrKeyValueCurrentPrinterUtilityForXlDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub OutputThisComputerPrintersInformationDTColFromWMIToSheet()

    Dim objDTCol As Collection, objSheet As Excel.Worksheet

    LoadThisComputerRegisteredPrinterInfoFromWMI objDTCol

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(mfstrGetPrintersSettingLogBookPath(), "PrintersByWMI")

    OutputColToSheet objSheet, objDTCol, GetColFromLineDelimitedChar("PrinterName,DriverName,Port,DefaultSelected"), mfobjGetDTFormatterOfThisComputerPrintersInfo()
    
    OutputThisComputerGeneralInfoToAutoShapeLogTextOnSheet objSheet, XlShapeTextLogInteriorFillDefault
End Sub

'''
''' If Microsoft Access isn't installed in this computer, the following will stops
'''
Public Sub OutputThisComputerPrintersInformationDTColFromMSAccessToSheet()

    Dim objDTCol As Collection, objSheet As Excel.Worksheet

    LoadThisComputerRegisteredPrinterInfoFromMSAccess objDTCol

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(mfstrGetPrintersSettingLogBookPath(), "PrintersByMSAccess")

    OutputColToSheet objSheet, objDTCol, GetColFromLineDelimitedChar("PrinterName,DriverName,Port"), mfobjGetDTFormatterOfThisComputerPrintersInfo()
    
    OutputThisComputerGeneralInfoToAutoShapeLogTextOnSheet objSheet, XlShapeTextLogInteriorFillDefault
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetDTFormatterOfThisComputerPrintersInfo() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter, objFormatConditionExpander As FormatConditionExpander
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("PrinterName,27,DriverName,29,Port,23,DefaultSelected,10")
    
        .RecordBordersType = RecordCellsGrayAll
        
        
        Set .ColumnsFormatCondition = New ColumnsFormatConditionParam
        
        With .ColumnsFormatCondition
        
            Set objFormatConditionExpander = New FormatConditionExpander
            
            With objFormatConditionExpander
                
                .CondtionText = "True"
                
                .CellFormatConditionType = CellBackground.FormatConditionTextContain
                
                .CellColorArrangementType = FontDeepGreenBgLightGreen
            End With
        
            .FieldTitleToCellFormatCondition.Add "DefaultSelected", objFormatConditionExpander
        End With
    End With

    Set mfobjGetDTFormatterOfThisComputerPrintersInfo = objDTSheetFormatter
End Function

'''
'''
'''
Private Function mfstrGetPrintersSettingLogBookPath() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AboutThisComputer"

    ForceToCreateDirectory strDir
    
    mfstrGetPrintersSettingLogBookPath = strDir & "\CurrentPrinterInfo.xlsx"
End Function




'**---------------------------------------------
'** Key-Value cache preparation for CurrentPrinterUtilityForXl
'**---------------------------------------------
'''
''' get string Value from STR_KEY_CONFIRM_THIS_WINDOWS_COMPUTER_ENABLED_PRINTERS
'''
''' automatically-added for CurrentPrinterUtilityForXl string-key-value management for standard module and class module
Public Function GetTextOfStrKeyConfirmThisWindowsComputerEnabledPrinters() As String

    If Not mblnIsStrKeyValueCurrentPrinterUtilityForXlDicInitialized Then

        msubInitializeTextForCurrentPrinterUtilityForXl
    End If

    GetTextOfStrKeyConfirmThisWindowsComputerEnabledPrinters = mobjStrKeyValueCurrentPrinterUtilityForXlDic.Item("STR_KEY_CONFIRM_THIS_WINDOWS_COMPUTER_ENABLED_PRINTERS")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for CurrentPrinterUtilityForXl string-key-value management for standard module and class module
Private Sub msubInitializeTextForCurrentPrinterUtilityForXl()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueCurrentPrinterUtilityForXlDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForCurrentPrinterUtilityForXlByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForCurrentPrinterUtilityForXlByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueCurrentPrinterUtilityForXlDicInitialized = True
    End If

    Set mobjStrKeyValueCurrentPrinterUtilityForXlDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for CurrentPrinterUtilityForXl key-values cache
'''
''' automatically-added for CurrentPrinterUtilityForXl string-key-value management for standard module and class module
Private Sub AddStringKeyValueForCurrentPrinterUtilityForXlByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_CONFIRM_THIS_WINDOWS_COMPUTER_ENABLED_PRINTERS", "Confirm this Windows enabled printers"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for CurrentPrinterUtilityForXl key-values cache
'''
''' automatically-added for CurrentPrinterUtilityForXl string-key-value management for standard module and class module
Private Sub AddStringKeyValueForCurrentPrinterUtilityForXlByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_CONFIRM_THIS_WINDOWS_COMPUTER_ENABLED_PRINTERS", "Windowsに設定済のプリンタを確認"
    End With
End Sub

'''
''' Remove Keys for CurrentPrinterUtilityForXl key-values cache
'''
''' automatically-added for CurrentPrinterUtilityForXl string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueCurrentPrinterUtilityForXl(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_CONFIRM_THIS_WINDOWS_COMPUTER_ENABLED_PRINTERS") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_CONFIRM_THIS_WINDOWS_COMPUTER_ENABLED_PRINTERS"
        End If
    End With
End Sub

