Attribute VB_Name = "ColorDefinedByUserForXl"
'
'   color conversion between a defined enumeration and color value
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 21/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToOutputColorDTToSheet()

    OutputColorDTToSheet GetSampleRGBColorTextCol(), False
End Sub


Private Sub msubSanityTestToOutputRGBAndHSBColorDTToSheet()

    OutputColorDTToSheet GetSampleRGBColorTextCol(), True
End Sub



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub OutputColorDTToSheet(ByVal vobjColorTextCol As Collection, Optional ByVal vblnAllowToIncludeHSBValues As Boolean = True)

    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    Dim objDTCol As Collection, strLog As String
    
    If vblnAllowToIncludeHSBValues Then
    
        strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\ColorRGBAndHSBSamples.xlsx"
    
        Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "SampleRGBAndHSBColors")
    Else
        strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\ColorSamples.xlsx"
    
        Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "SampleColors")
    End If
    
    
    Set objDTCol = GetRGBAndHSBColorDTCol(vobjColorTextCol, vblnAllowToIncludeHSBValues)
    
    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, objDTCol, mfobjGetDataTableSheetFormatterForSimpleColorTable(vblnAllowToIncludeHSBValues)
    
    SetColorToCellsInteriorBackColorRangeOnSheet objSheet

    If vblnAllowToIncludeHSBValues Then
    
        strLog = "Used HSB conversion function: RGBToHSB" & vbNewLine
    
        strLog = strLog & "Range of Hue: from 0 To 359" & vbNewLine
        
        strLog = strLog & "Range of Saturation: from 0 To 100" & vbNewLine
        
        strLog = strLog & "Range of Brightness: from 0 To 100" & vbNewLine
        
        strLog = strLog & "Logged date-time: " & Format(Now(), "ddd, yyyy/h/m hh:mm:ss")
        
        AddAutoShapeGeneralMessage objSheet, strLog
    End If
End Sub


'''
'''
'''
Public Sub SetColorToCellsInteriorBackColorRangeOnSheet(ByVal vobjSheet As Excel.Worksheet, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1)

    Dim objTopLeftRange As Excel.Range, objCurrentRegion As Excel.Range
    Dim objFieldTitleToPositionIndexDic As Scripting.Dictionary
    Dim intColumnMax As Long, intPaintColumnIndex As Long
    Dim varRGBValues As Variant, intRowMax As Long, intRGBColumnIndex As Long, strRange As String, i As Long, strColumnChar As String
    
    Dim intRGB As Long, strNameOfRGB As String
    
    Const strRGBColumnName As String = "RGB_Val"
    
    With vobjSheet
    
        Set objTopLeftRange = .Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
    
        Set objCurrentRegion = objTopLeftRange.CurrentRegion
        
        Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(GetColFromLineDelimitedChar(strRGBColumnName), vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
        intColumnMax = objCurrentRegion.Columns.Count + vintFieldTitlesRowIndex - 1
    
        intPaintColumnIndex = intColumnMax + 1
        
        strColumnChar = ConvertXlColumnIndexToLetter(intPaintColumnIndex)
        
        With .Range(strColumnChar & CStr(vintFieldTitlesRowIndex))
        
            .Value = "Painted"
        End With
    
        With .Range(strColumnChar & ":" & strColumnChar)
        
            .EntireColumn.ColumnWidth = 21
        End With
    
        With objCurrentRegion
    
            intRowMax = .Rows.Count - 1
            intColumnMax = .Columns.Count
            
            intRGBColumnIndex = objFieldTitleToPositionIndexDic.Item(strRGBColumnName)
            
            ReDim varRGBValues(1 To (.Rows.Count - 1), 1 To 1)
            
            If .Rows.Count > 1 Then
            
                strRange = ConvertXlColumnIndexToLetter(intRGBColumnIndex) & CStr(vintFieldTitlesRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(intRGBColumnIndex) & CStr(vintFieldTitlesRowIndex + .Rows.Count - 1)
                
                If intRowMax = 1 And intColumnMax = 1 Then
                
                    varRGBValues(1, 1) = vobjSheet.Range(strRange).Resize(intRowMax, intColumnMax).Value
                Else
                    varRGBValues = vobjSheet.Range(strRange).Resize(intRowMax, intColumnMax).Value
                End If
            End If
        End With
    
        For i = 1 To intRowMax
        
            intRGB = varRGBValues(i, 1)
        
            With .Range(strColumnChar & CStr(vintFieldTitlesRowIndex + i))
            
                .Interior.Color = intRGB
                
                With GetTrRGBValueToTextCacheDic()
                
                    If .Exists(intRGB) Then
                    
                        strNameOfRGB = .Item(intRGB)
                    Else
                        strNameOfRGB = "Nameless-color"
                    End If
                End With
                
                .Value = strNameOfRGB
                
                If IsWhiteFontColorRecommendedWithUsingCache(.Interior.Color) Then
                
                    '.Font.Color = RGB(255, 255, 255)
                    
                    .Font.Color = RGB(240, 240, 240)
                End If
                
            End With
            
            With .Range(ConvertXlColumnIndexToLetter(intRGBColumnIndex) & CStr(vintFieldTitlesRowIndex + i))
            
                .Font.Color = intRGB
            End With
        Next
    End With
    
    DecorateGridSimply vobjSheet, WhiteFontGrayBgMeiryo, RecordCellsGrayAll
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterForSimpleColorTable(Optional ByVal vblnAllowToIncludeHSBValues As Boolean = True) As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        
        If vblnAllowToIncludeHSBValues Then
        
            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("R_Val,8,G_Val,8,B_Val,8,RGB_Val,11,Hue_Val,10,Saturation_Val,10,Brightness_Val,10")
        Else
            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("R_Val,8,G_Val,8,B_Val,8,RGB_Val,11")
        End If
    
    
        .FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQL
        
        .RecordBordersType = RecordCellsGrayAll
    End With

    Set mfobjGetDataTableSheetFormatterForSimpleColorTable = objDTSheetFormatter
End Function





