Attribute VB_Name = "UTfFileSysFilesInfoForXl"
'
'   List up files below the selected directory by plural method, such as Dir function and FileSystemObject
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  3/Aug/2023    Kalmclaeyd Tarclanus    Separated from UTfFileSysFilesInfo.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrSampleFileInformationBookBaseName As String = "TestSampleFilesInformations"

Private Const mstrComprehensiveTestBookBaseName As String = "ComprehensiveFilesInformationTests"


'///////////////////////////////////////////////
'/// Sanity test
'///////////////////////////////////////////////
'**---------------------------------------------
'** Tests for serch directories partially
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToSearchDirsPartially()

    Dim objSearchCondition As FileSysSearchCondition, strSystemDirvePath As String
    Dim objResult As FileSysSearchResult
    
    Dim strOutputDir As String, strOutputBookPath As String, objOutputBook As Excel.Workbook, objSheet As Excel.Worksheet


    Set objSearchCondition = New FileSysSearchCondition
    
    With objSearchCondition
    
        .FileInfomationTypesDelimitedByComma = "FullPath"
    
        .MinimumHierarchicalOrderToSearch = 0
        
        .MaximumHierarchicalOrderToSearch = 2
    
        .RawWildCardsByDelimitedChar = "*SQLite*"
    
        .IsDirectoryInfoNeeded = True
        
        .IsFileInfoNeeded = False
    End With

    strSystemDirvePath = GetThisLocalWindowsSystemDriveLetter() & ":\"

    Debug.Print strSystemDirvePath
    
    
    ' Using FSO
    'Set objResult = GetFileSysSearchedResultCollectionByConditionUsingFSO(strSystemDirvePath, objSearchCondition)
    
    ' Using VBA.Dir
    Set objResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(strSystemDirvePath, objSearchCondition)
    
    'DebugCol objResult.ResultDataTableCol
    
    strOutputDir = GetFileSysSearchedResultsLoggingDirectoryPath()

    strOutputBookPath = strOutputDir & "\TestSearchedDirectoriesResults.xlsx"

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "TestSearched")

    OutputResultOfFilesGeneralInformationIntoExcelSheet objSheet, objResult
End Sub

'''
'''
'''
Private Sub msubSanityTestToSearchFilesPartially()

    Dim objSearchCondition As FileSysSearchCondition, strSystemDirvePath As String
    Dim objResult As FileSysSearchResult
    
    Dim strOutputDir As String, strOutputBookPath As String, objOutputBook As Excel.Workbook, objSheet As Excel.Worksheet


    Set objSearchCondition = New FileSysSearchCondition
    
    With objSearchCondition
    
        .FileInfomationTypesDelimitedByComma = "FullPath"
    
        .MinimumHierarchicalOrderToSearch = 0
        
        .MaximumHierarchicalOrderToSearch = 2
    
        '.RawWildCardsByDelimitedChar = "*SQLite*"
        
        .RawWildCardsByDelimitedChar = "sqlite3.exe"
    
        .IsDirectoryInfoNeeded = False
        
        .IsFileInfoNeeded = True
    End With

    strSystemDirvePath = GetThisLocalWindowsSystemDriveLetter() & ":\"

    Debug.Print strSystemDirvePath
    
    ' Using FSO
    Set objResult = GetFileSysSearchedResultCollectionByConditionUsingFSO(strSystemDirvePath, objSearchCondition)
    
    ' Using VBA.Dir
    'Set objResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(strSystemDirvePath, objSearchCondition)
    
    'DebugCol objResult.ResultDataTableCol
    
    strOutputDir = GetFileSysSearchedResultsLoggingDirectoryPath()

    strOutputBookPath = strOutputDir & "\TestSearchedFilesResults.xlsx"

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "TestSearched")

    OutputResultOfFilesGeneralInformationIntoExcelSheet objSheet, objResult
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetDirectoryPathsForAllHarddrivesUsingVbaDir()

    DebugCol GetDirectoryPathsForAllHarddrivesUsingVbaDir("*SQLite*", 0, 2)
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetFilePathsForAllHarddrivesUsingVbaDir()

    DebugCol GetFilePathsForAllHarddrivesUsingVbaDir("sqlite3.exe", 0, 2)
End Sub


'**---------------------------------------------
'** Tests for FileSysSearchResult object
'**---------------------------------------------
'''
''' use FileSystemObject
'''
Private Sub msubSanityTestToOutputResultOfFilesGeneralInformationIntoExcelSheetForFSO()

    Dim strWorkDir As String, strOutputDir As String, strOutputBookPath As String, objOutputBook As Excel.Workbook, objSheet As Excel.Worksheet
    Dim strSearchingDir As String, objSearchCondition As FileSysSearchCondition, objResult As FileSysSearchResult
    
    strWorkDir = GetTemporaryDevelopmentVBARootDir()
    
    ForceToCreateDirectory strWorkDir
    
    strSearchingDir = GetTemporaryDevelopmentVBARootDir() & "\"
    
    Set objSearchCondition = New FileSysSearchCondition
    
    With objSearchCondition
    
        
    
    End With
    
    ' use FileSystemObject
    Set objResult = GetFileSysSearchedResultCollectionByConditionUsingFSO(strSearchingDir, objSearchCondition)
    
    strOutputDir = GetFileSysSearchedResultsLoggingDirectoryPath()
    
    strOutputBookPath = strOutputDir & "\TestSearchedResults.xlsx"
    
    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objOutputBook)
    
    objSheet.Name = "TestSearched"
    
    OutputResultOfFilesGeneralInformationIntoExcelSheet objSheet, objResult
End Sub

'''
''' use VBA.Dir
'''
Private Sub msubSanityTestToOutputResultOfFilesGeneralInformationIntoExcelSheetForVbaDir()

    Dim strWorkDir As String, strOutputDir As String, strOutputBookPath As String, objOutputBook As Excel.Workbook, objSheet As Excel.Worksheet
    Dim strSearchingDir As String, objSearchCondition As FileSysSearchCondition, objResult As FileSysSearchResult
    
    strWorkDir = GetTemporaryDevelopmentVBARootDir()
    
    ForceToCreateDirectory strWorkDir
    
    strSearchingDir = GetTemporaryDevelopmentVBARootDir() & "\"
    
    Set objSearchCondition = New FileSysSearchCondition
    
    With objSearchCondition
    
        
    
    End With
    
    ' use VBA.Dir
    Set objResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(strSearchingDir, objSearchCondition)
    
    strOutputDir = GetFileSysSearchedResultsLoggingDirectoryPath()
    
    strOutputBookPath = strOutputDir & "\TestSearchedResults.xlsx"
    
    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objOutputBook)
    
    objSheet.Name = "TestSearched"
    
    OutputResultOfFilesGeneralInformationIntoExcelSheet objSheet, objResult
End Sub


'**---------------------------------------------
'** Tests with outputting Excel.Worksheet
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCreateFileAttributesTable()

    Dim strFilePath As String
    
    strFilePath = GetCurrentBookOutputDir() & "\" & mstrSampleFileInformationBookBaseName & ".xlsx"
End Sub


'''
'''
'''
Private Sub msubSanityTestToWriteFilesGeneralInformationType01()

    msubSanityTestToWriteFilesGeneralInformationByExtensionsDelimited "FileName,FullPath,FileSize,LastModifiedDate"
End Sub

Private Sub msubSanityTestToWriteFilesGeneralInformationType02()

    msubSanityTestToWriteFilesGeneralInformationByExtensionsDelimited "FileBaseName,FileExtension,ParentDirectoryPath,RelativeDirectoryPath,LastModifiedDate,CreatedDate"
End Sub

'''
'''
'''
Private Sub msubSanityTestToWriteFilesGeneralInformationByExtensionsDelimited(ByVal vstrFileInfomationTypesDelimitedByComma As String)

    Dim objNecessaryFileInfoTypeCol As Collection, strFilePath As String
    
    Set objNecessaryFileInfoTypeCol = GetFileGeneralInfoColFromLineDelimitedChar(vstrFileInfomationTypesDelimitedByComma)

    With New Scripting.FileSystemObject
    
        strFilePath = GetCurrentBookOutputDir() & "\" & mstrSampleFileInformationBookBaseName & ".xlsx"
    End With

    WriteFilesGeneralInformationByExtensionsDelimited GetCurrentOfficeFileObject().FullName, GetCurrentOfficeFileObject().Path, True, objNecessaryFileInfoTypeCol, GenerateNewBookWithDeletingOldSheets, strFilePath
End Sub

'**---------------------------------------------
'** Tests and output into the immediate window
'**---------------------------------------------
'''
'''
'''
Public Function GetLoggingWorkbookPathForListingFilesUpInformationComprehensiveTest() As String

    GetLoggingWorkbookPathForListingFilesUpInformationComprehensiveTest = GetTemporaryDevelopmentVBARootDir() & "\" & mstrComprehensiveTestBookBaseName & ".xlsx"
End Function





