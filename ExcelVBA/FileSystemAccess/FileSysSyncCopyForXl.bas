Attribute VB_Name = "FileSysSyncCopyForXl"
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       Dependent on IWshRuntimeLibrary.WshShell for managing short-cut file
'       Dependent on ModelessProgressBar.bas
'       Dependent on both Excel and Windows OS
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 10/Feb/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////

'**---------------------------------------------
'** Output results to Excel book
'**---------------------------------------------
'''
''' About PDF files
'''
Public Sub OutputPDFAndDocumentsListToExcelBook(ByVal vstrSearchingDirectoryPath As String, _
        ByVal vstrBookPath As String, _
        Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "FullPath,LinkDestination")

    Dim objDataTableCol As Collection

    Set objDataTableCol = GetPDFAndDocumentsListsCol(vstrSearchingDirectoryPath, vstrStringOfFileInfomationTypesDelimitedByComma)

    OutputFileInformationListToExcelBook objDataTableCol, vstrBookPath, "PDFDocumentList", vstrStringOfFileInfomationTypesDelimitedByComma, vstrSearchingDirectoryPath
End Sub


'''
''' About directories
'''
Public Sub OutputDirectoryListToExcelBook(ByVal vstrSearchingDirectoryPath As String, _
        ByVal vstrBookPath As String, _
        Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "FullPath")

    Dim objDataTableCol As Collection

    Set objDataTableCol = GetDirectoriesListsCol(vstrSearchingDirectoryPath, vstrStringOfFileInfomationTypesDelimitedByComma)
    
    OutputFileInformationListToExcelBook objDataTableCol, _
            vstrBookPath, _
            "DirectoryList", _
            vstrStringOfFileInfomationTypesDelimitedByComma, _
            vstrSearchingDirectoryPath
End Sub


'**---------------------------------------------
'** Searching files which name includes Underbar with eight digits numbers
'**---------------------------------------------
'''
'''
'''
Public Sub SearchNoUnderbar8DigitsDateSuffixFiles(ByVal vobjFilePaths As Collection)

    Dim objFilteredPaths As Collection, objSummeryDataTableCol As Collection, objDoubleStopWatch As DoubleStopWatch

    With New DoubleStopWatch

        .MeasureStart
        
        Set objFilteredPaths = FilterNoUnderbar8DigitsDateSuffixFiles(vobjFilePaths)
    
        Set objSummeryDataTableCol = GetSummaryFileInformationTableFromFilePaths(objFilteredPaths)
    
        .MeasureInterval
    
        OutputNoUnderbar8DigitsDateFilesInfoToTemporaryBook objSummeryDataTableCol, .ElapsedTimeByString
    End With
End Sub

'**---------------------------------------------
'** Log output about overwriting files
'**---------------------------------------------
'''
'''
'''
Public Sub OutputLogOfOverwritingFilesToSheet(ByVal vobjLogDTCol As Collection, _
        ByVal vstrSourcePathPart As String, _
        ByVal vstrDestinationPathPart As String)

    Dim objBook As Excel.Workbook, strBookPath As String, objSheet As Excel.Worksheet
    
    Dim objFieldTitlesCol As Collection, strLog As String
    
    Dim objDecorationTagValueSettings As Collection, objDecorationTargetTextSettings As Collection
    
    
    strBookPath = GetCurrentBookOutputDir() & "\LogOfOverwritingFilesInUpdating.xlsx"

    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)

    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet.Name = "LogToOverwritingFiles"

    Set objFieldTitlesCol = GetColFromLineDelimitedChar("DoCopy,SecondDiffOfModifiedDate,DayDiffOfModified,SourceFileName,SourceFileSize,DestinationFileName,DestinationFileSize,SourceDateLastModified,DestinationDateLastModified,SourcePath,DestinationPath")

    OutputColToSheet objSheet, vobjLogDTCol, objFieldTitlesCol, mfobjGetDataTableSheetFormatter()

    DeleteDummySheetWhenItExists objBook
    
    strLog = mfstrGetLogOfUpdatingFiles(vobjLogDTCol, vstrSourcePathPart, vstrDestinationPathPart, objDecorationTagValueSettings, objDecorationTargetTextSettings)
    
    AddAutoShapeGeneralMessage objSheet, strLog, vobjDecorationTagValueSettings:=objDecorationTagValueSettings, vobjDecorationTargetTextSettings:=objDecorationTargetTextSettings
End Sub


'''
'''
'''
Public Sub OutputDestinationDeletingFilesInfoBecauseOfChangingReletivePathToTemporaryBook(ByVal vobjSummaryDataTableCol As Collection)

    Dim strFileInfomationTypesDelimitedByComma As String, strTempBookPath As String, objSheet As Excel.Worksheet

    If Not vobjSummaryDataTableCol Is Nothing Then

        If vobjSummaryDataTableCol.Count > 0 Then

            strFileInfomationTypesDelimitedByComma = "FullPath,LastModifiedDate"

            Set objSheet = mfobjGetSheetOfAnyTypeFilesInfoToTemporaryBook(vobjSummaryDataTableCol, "ToDeleteFilesBecauseOfChangingRelativePath.xlsx", "ToDeletePaths", strFileInfomationTypesDelimitedByComma)

            AddHyperLinkAboutUNCPathColumn objSheet, "FullPath"
        End If
    End If
End Sub

'''
'''
'''
Public Function GetTemporaryOutputBookDirectoryPath() As String

    GetTemporaryOutputBookDirectoryPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
End Function

'**---------------------------------------------
'** About deleting duplicated files
'**---------------------------------------------
'''
'''
'''
Public Sub OutputDuplicatedFilesInfoToTemporaryBook(ByVal vobjSummaryDataTableCol As Collection, _
        ByVal vstrElapsedTime As String)

    msubOutputSummeryFilesInfoToTemporaryBook vobjSummaryDataTableCol, _
            vstrElapsedTime, _
            "DuplicatedToFilesToReduceIntoOne.xlsx", _
            "DuplicatedFiles"
End Sub

'''
'''
'''
Public Sub OutputNoUnderbar8DigitsDateFilesInfoToTemporaryBook(ByVal vobjSummaryDataTableCol As Collection, _
        ByVal vstrElapsedTime As String)

    msubOutputSummeryFilesInfoToTemporaryBook vobjSummaryDataTableCol, _
            vstrElapsedTime, _
            "NoUnderbar8DigitFiles.xlsx", _
            "FilesInformation"
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrGetLogOfUpdatingFiles(ByVal vobjLogDTCol As Collection, ByVal vstrSourcePathPart As String, ByVal vstrDestinationPathPart As String, ByRef robjDecorationTagValueSettings As Collection, ByRef robjDecorationTargetTextSettings As Collection) As String

    Dim strLog As String, objDecorationTagValueSettings As Collection, objDecorationTargetTextSettings As Collection, objDriveInfoDecorationTagValueSettings As Collection
    
    Dim objNetwork As WshNetwork
    
    Set objNetwork = New WshNetwork
    
    
    strLog = "Log of updating files" & vbNewLine & vbNewLine
    
    With objNetwork
    
        strLog = strLog & "Computer name: " & .ComputerName & vbNewLine
        
        strLog = strLog & "User name: " & .UserName & vbNewLine
    
        strLog = strLog & "User domain: " & .UserDomain & vbNewLine
    End With
    
    strLog = strLog & "Logged date: " & Format(Now(), "ddd, yyyy/m/d hh:mm:dd") & vbNewLine & vbNewLine
    
    strLog = strLog & "Count of updated files: " & mfintGetCountOfUpdatedFiles(vobjLogDTCol) & vbNewLine
    
    strLog = strLog & "Count of different DateModified files: " & CStr(vobjLogDTCol.Count) & vbNewLine & vbNewLine
    
    
    strLog = strLog & "Source path: " & vstrSourcePathPart & vbNewLine & GetDriveInfoLog(vstrSourcePathPart, objDriveInfoDecorationTagValueSettings, "    ") & vbNewLine & vbNewLine
    
    strLog = strLog & "Destination path: " & vstrDestinationPathPart & vbNewLine & GetDriveInfoLog(vstrDestinationPathPart, objDriveInfoDecorationTagValueSettings, "    ")
    
    
    Set objDecorationTagValueSettings = New Collection
    
    With objDecorationTagValueSettings
    
        .Add "Computer name:;" & vbNewLine & ";11;trRGBWhiteFontColor; "
        .Add "User name:;" & vbNewLine & ";11;trRGBWhiteFontColor; "
        .Add "User domain:;" & vbNewLine & ";11;trRGBWhiteFontColor; "
    
        .Add "Logged date:;" & vbNewLine & ";11;trRGBWhiteFontColor; "
        
        .Add "Count of updated files:;" & vbNewLine & ";11;trRGBWhiteFontColor; "
        .Add "Count of different DateModified files:;" & vbNewLine & ";10.5;trRGBLightGrayFontColor; "
        
        
        .Add "Source path:;" & vbNewLine & ";11;trRGBWhiteFontColor; "
        .Add "Destination path:;" & vbNewLine & ";11;trRGBWhiteFontColor; "
    End With
    
    UnionDoubleCollectionsToSingle objDecorationTagValueSettings, objDriveInfoDecorationTagValueSettings
    
    Set objDecorationTargetTextSettings = New Collection
    
    With objDecorationTargetTextSettings
    
        .Add "Log of updating files;15;trRGBWhiteFontColor"
    End With
    
    
    Set robjDecorationTagValueSettings = objDecorationTagValueSettings
    Set robjDecorationTargetTextSettings = objDecorationTargetTextSettings
    
    mfstrGetLogOfUpdatingFiles = strLog
End Function

'''
'''
'''
Private Function mfintGetCountOfUpdatedFiles(ByVal vobjLogDTCol As Collection) As Long

    Dim varRowCol As Variant, objRowCol As Collection, blnDoCopy As Boolean, intCount As Long
    
    intCount = 0
    
    For Each varRowCol In vobjLogDTCol
    
        Set objRowCol = varRowCol
        
        blnDoCopy = CBool(objRowCol.Item(1))
        
        If blnDoCopy Then
        
            intCount = intCount + 1
        End If
    Next
    
    mfintGetCountOfUpdatedFiles = intCount
End Function


'''
'''
'''
Private Function mfobjGetDataTableSheetFormatter() As DataTableSheetFormatter

    Dim objSheetFormatter As DataTableSheetFormatter, objFormatConditionExpander As FormatConditionExpander
    
    Set objSheetFormatter = New DataTableSheetFormatter
    
    With objSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        
        Set .FieldTitleToColumnWidthDic = GetTextToTextDicFromLineDelimitedChar("DoCopy,7,SecondDiffOfModifiedDate,9,DayDiffOfModified,9,SourceFileName,53,SourceFileSize,11,DestinationFileName,53,DestinationFileSize,11,SourceDateLastModified,16,DestinationDateLastModified,16,SourcePath,20,DestinationPath,20")
    
        .RecordBordersType = RecordCellsGrayAll
        
        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
        
        With .ColumnsNumberFormatLocal
        
            .SetNumberFormatLocalAndApplyFieldTitles "0.00 ""[kB]""", GetColFromLineDelimitedChar("SourceFileSize,DestinationFileSize")
        End With
        
        
        Set .ColumnsFormatCondition = New ColumnsFormatConditionParam
        
        With .ColumnsFormatCondition
        
            Set objFormatConditionExpander = New FormatConditionExpander
            
            With objFormatConditionExpander
                
                .AddTextContainCondition "True", FontDeepGreenBgLightGreen
                .AddTextContainCondition "False", FontDeepRedBgLightRed
            End With
        
            .FieldTitleToCellFormatCondition.Add "DoCopy", objFormatConditionExpander
        
        End With
    End With

    Set mfobjGetDataTableSheetFormatter = objSheetFormatter
End Function


'**---------------------------------------------
'** Configure each data-table sheet formatter object
'**---------------------------------------------
'''
'''
'''
Private Function GetDataTableSheetFormatterForFileInformationLists(ByVal vobjNecessaryFileInfoTypeCol As Collection) As DataTableSheetFormatter

    Dim objDataTableFormatter As DataTableSheetFormatter

    Set objDataTableFormatter = New DataTableSheetFormatter
    
    With objDataTableFormatter
    
        Set .FieldTitleToColumnWidthDic = GetFieldTitleToColumnWidthDicOfFilesInformationFrom(vobjNecessaryFileInfoTypeCol)
    End With

    Set GetDataTableSheetFormatterForFileInformationLists = objDataTableFormatter
    
End Function


'**---------------------------------------------
'** Output results to Excel sheet
'**---------------------------------------------
'''
'''
'''
Private Sub OutputFileInformationListToExcelBook(ByVal vobjDataTableCol As Collection, _
        ByVal vstrBookPath As String, _
        ByVal vstrSheetName As String, _
        Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "RelativeFullPath", _
        Optional ByVal vstrSearchingParentDirectoryPath As String = "")


    Dim objBook As Excel.Workbook, objSheet As Excel.Worksheet

    Set objBook = GetWorkbookAndPrepareForUnitTest(vstrBookPath)

    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet.Name = vstrSheetName

    OutputColToSheet objSheet, vobjDataTableCol, GetColFromLineDelimitedChar(vstrStringOfFileInfomationTypesDelimitedByComma), GetDataTableSheetFormatterForFileInformationLists(GetFileGeneralInfoColFromLineDelimitedChar(vstrStringOfFileInfomationTypesDelimitedByComma))

    If GetFileGeneralInfoFlagValueFromLineDelimitedChar(vstrStringOfFileInfomationTypesDelimitedByComma) And giLinkDestination > 0 Then

        AddHyperLinkAboutUNCPathColumn objSheet, "LinkDestination"
    End If

    If vstrSearchingParentDirectoryPath <> "" Then
    
        AddAutoShapeGeneralMessage objSheet, "Searching directory: " & vstrSearchingParentDirectoryPath & vbNewLine & "Searched date: " & FormatDateAndWeekdayJapanese(Now())
    End If

    DeleteDummySheetWhenItExists objBook
End Sub




'**---------------------------------------------
'** About deleting duplicated files
'**---------------------------------------------
'''
'''
'''
Private Sub msubOutputSummeryFilesInfoToTemporaryBook(ByVal vobjSummaryDataTableCol As Collection, _
        ByVal vstrElapsedTime As String, _
        ByVal vstrOutputBookName As String, _
        ByVal vstrSheetName As String)

    Dim strFileInfomationTypesDelimitedByComma As String, objNecessaryFileInfoTypeCol As Collection
    
    Dim strTempBookPath As String, objSheet As Excel.Worksheet

    If Not vobjSummaryDataTableCol Is Nothing Then

        If vobjSummaryDataTableCol.Count > 0 Then

            strFileInfomationTypesDelimitedByComma = "FileName,LastModifiedDate,CreatedDate,ParentDirectoryPath,FullPath"

            Set objSheet = mfobjGetSheetOfAnyTypeFilesInfoToTemporaryBook(vobjSummaryDataTableCol, _
                    vstrOutputBookName, _
                    vstrSheetName, _
                    strFileInfomationTypesDelimitedByComma)

            AddHyperLinkFromLinkSettingColumnAndUNCPathColumn objSheet, "FileName", "FullPath"

            AddHyperLinkAboutUNCPathColumn objSheet, "ParentDirectoryPath"

            msubAddAutoShapeGeneralMessageAboutFilesInformations objSheet, vstrElapsedTime
        End If
    End If
End Sub


'''
'''
'''
Private Sub msubAddAutoShapeGeneralMessageAboutFilesInformations(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vstrElapsedTime As String)

    Dim intInputRowsCount As Long, intInputColumnsCount As Long, strLoggedTimeStamp As String
    
    Dim strMessage As String
    
    GetCountsOfRowsColumnsFromSheet vobjSheet, intInputRowsCount, intInputColumnsCount, strLoggedTimeStamp
   
    strMessage = "Elapsed time to search files:" & vbNewLine & vstrElapsedTime
    
    strMessage = strMessage & vbNewLine & "Files count: " & CStr(intInputRowsCount)
    
    AddAutoShapeGeneralMessage vobjSheet, strMessage
End Sub


'''
'''
'''
Private Function mfobjGetSheetOfAnyTypeFilesInfoToTemporaryBook(ByVal vobjSummaryDataTableCol As Collection, _
        ByVal vstrTemporaryBookName As String, _
        ByVal vstrSheetName As String, _
        ByVal vstrFileInfomationTypesDelimitedByComma As String) As Excel.Worksheet


    Dim objNecessaryFileInfoTypeCol As Collection
    Dim strTempBookPath As String, objSheet As Excel.Worksheet

    If Not vobjSummaryDataTableCol Is Nothing Then

        If vobjSummaryDataTableCol.Count > 0 Then

            Set objNecessaryFileInfoTypeCol = GetFileGeneralInfoColFromLineDelimitedChar(vstrFileInfomationTypesDelimitedByComma)
        
            strTempBookPath = GetTemporaryOutputBookDirectoryPath() & "\" & vstrTemporaryBookName
        
            Set objSheet = OutputFilesGeneralInformationDTColIntoExcelSheetByOutputBookPath(vobjSummaryDataTableCol, GetColFromLineDelimitedChar(vstrFileInfomationTypesDelimitedByComma), GenerateNewBookWithDeletingOldSheets, strTempBookPath, vstrSheetName, objNecessaryFileInfoTypeCol)
        End If
    End If

    Set mfobjGetSheetOfAnyTypeFilesInfoToTemporaryBook = objSheet
End Function



