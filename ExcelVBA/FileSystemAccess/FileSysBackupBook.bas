Attribute VB_Name = "FileSysBackupBook"
'
'   Back up Excel workbook. this codes include ThisWorkbook keyword
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 13/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Back up office files
'///////////////////////////////////////////////
'''
''' for safety, you can also make back up
'''
Public Sub SaveThisWorkbookAsBackup()

    SaveWorkbookAsAutoBackupWithoutDisplayAlert ThisWorkbook, "_SelfBackUp_" & WeekdayEnglishShort3Chars(Now()) & "_" & Format(Now(), "yyyymmdd_hhnnss") & "ForSafety"
End Sub



'''
''' save as auto backup within changing Application.DisplayAlert
'''
Public Sub SaveWorkbookAsAutoBackupWithoutDisplayAlert(ByVal vobjBook As Excel.Workbook, Optional ByVal vstrSuffix As String = "")
    
    Const strChildDirName As String = "BackupExcelVBA"
    
    Dim blnDisplayAlerts As Boolean
    Dim objFS As Scripting.FileSystemObject
    Dim strPath As String, strDir As String
    Dim objXl As Excel.Application
    
    Set objXl = vobjBook.Application
    
    Set objFS = New Scripting.FileSystemObject
    
    blnDisplayAlerts = objXl.DisplayAlerts
    
    On Error GoTo ErrHandler:
    
    objXl.DisplayAlerts = False
    
    With objFS
    
        strDir = .GetParentFolderName(vobjBook.FullName) & "\" & strChildDirName
        
        If Not .FolderExists(strDir) Then
        
            ForceToCreateDirectory strDir
        End If
    
        If vstrSuffix = "" Then
        
            strPath = strDir & "\" & .GetBaseName(vobjBook.FullName) & "_AutoBak_" & WeekdayEnglishShort3Chars(Now()) & "_" & Format(Now(), "yyyymmdd_hhnnss") & "." & .GetExtensionName(vobjBook.FullName)
        Else
            strPath = strDir & "\" & .GetBaseName(vobjBook.FullName) & vstrSuffix & "." & .GetExtensionName(vobjBook.FullName)
        End If
    
        .CopyFile vobjBook.FullName, strPath
    End With
     
    'vobjBook.SaveAs strPath
    
ErrHandler:
    objXl.DisplayAlerts = blnDisplayAlerts
End Sub


'''
''' easy backup of ThisWorkbook with date info suffix
'''
Public Sub SaveThisWorkbookAsAutoBackupWithSuffixToSpecifiedDirectoryWithoutDisplayAlert()
    
    Dim blnDisplayAlerts As Boolean
    Dim objFS As Scripting.FileSystemObject, strPath As String, strDir As String
    Dim objXl As Excel.Application
    
    
    Set objXl = ThisWorkbook.Application
    
    Set objFS = New Scripting.FileSystemObject
    
    blnDisplayAlerts = objXl.DisplayAlerts
    
    On Error GoTo ErrHandler:
    
    objXl.DisplayAlerts = False
    
    With objFS
        
        strDir = GetDevelopmentVBARootDir() & "\OldBackUp\bak" & Format(Now, "yyyymmdd")
        
        ForceToCreateDirectory strDir
        
        strPath = strDir & "\" & .GetBaseName(ThisWorkbook.FullName) & "_" & Format(Now(), "yyyymmdd") & "." & .GetExtensionName(ThisWorkbook.FullName)
    
        .CopyFile ThisWorkbook.FullName, strPath
    End With
     
ErrHandler:
    objXl.DisplayAlerts = blnDisplayAlerts
End Sub
