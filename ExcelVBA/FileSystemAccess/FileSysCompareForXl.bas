Attribute VB_Name = "FileSysCompareForXl"
'
'   Output compared results of plural file-sytem directories to Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on CompareDictionaries.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 14/Jun/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub OutputComparedResultsOfExistedFilesBetweenSourceAndDestinationByVbaDirToExcelSheet(ByVal vstrOutputBookPath As String, _
        ByVal vstrSrcDirectoryPath As String, _
        ByVal vstrDstDirectoryPath As String)


    Const strFileInfomationTypesDelimitedByComma As String = "RelativeFullPath,LastModifiedDate"

    Dim objSrcCommonKeyDic As Scripting.Dictionary, objDstCommonKeyDic As Scripting.Dictionary, objSrcIndependentKeyDic As Scripting.Dictionary, objDstIndependentKeyDic As Scripting.Dictionary
    Dim blnIsAtLeastAFileFound As Boolean, blnOnlyCommonFilesExists As Boolean, blnOnlySrcSideFilesExists As Boolean, blnOnlyDstSideFilesExists As Boolean

    Dim objBook As Excel.Workbook, objSheet As Excel.Worksheet


    CompareExistedFilesBetweenSourceAndDestinationByVbaDir objSrcCommonKeyDic, objDstCommonKeyDic, objSrcIndependentKeyDic, objDstIndependentKeyDic, vstrSrcDirectoryPath, vstrDstDirectoryPath
    
    blnIsAtLeastAFileFound = False
    
    Select Case True
    
        Case IsAtLeastOneDictionaryKeyValueExists(objSrcCommonKeyDic), IsAtLeastOneDictionaryKeyValueExists(objDstCommonKeyDic), IsAtLeastOneDictionaryKeyValueExists(objSrcIndependentKeyDic), IsAtLeastOneDictionaryKeyValueExists(objDstIndependentKeyDic)
    
            blnIsAtLeastAFileFound = True
    End Select
    
    
    
    If blnIsAtLeastAFileFound Then
    
        blnOnlyCommonFilesExists = False: blnOnlySrcSideFilesExists = False: blnOnlyDstSideFilesExists = False
    
        If IsAtLeastOneDictionaryKeyValueExists(objSrcCommonKeyDic) Then
        
            If Not IsAtLeastOneDictionaryKeyValueExists(objSrcIndependentKeyDic) And Not IsAtLeastOneDictionaryKeyValueExists(objDstIndependentKeyDic) Then
            
                blnOnlyCommonFilesExists = True
            End If
        Else
            If Not IsAtLeastOneDictionaryKeyValueExists(objSrcIndependentKeyDic) And IsAtLeastOneDictionaryKeyValueExists(objDstIndependentKeyDic) Then
            
                blnOnlyDstSideFilesExists = True
                
            ElseIf IsAtLeastOneDictionaryKeyValueExists(objSrcIndependentKeyDic) And Not IsAtLeastOneDictionaryKeyValueExists(objDstIndependentKeyDic) Then
            
                blnOnlySrcSideFilesExists = True
            End If
        End If
    
    
        Set objBook = GetWorkbookAndPrepareForUnitTest(vstrOutputBookPath)
    
        If IsAtLeastOneDictionaryKeyValueExists(objSrcCommonKeyDic) Then
        
            Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
            
            objSheet.Name = "CommonExistedFiles"
            
            OutputDicToSheet objSheet, objSrcCommonKeyDic, GetColFromLineDelimitedChar(strFileInfomationTypesDelimitedByComma), GetDataTableSheetFormatterForFilesInfoDataTable(GetFileGeneralInfoColFromLineDelimitedChar(strFileInfomationTypesDelimitedByComma))
            
            AddAutoShapeGeneralMessage objSheet, "Existed files in both Src and Dst" & vbNewLine & "Count of found files: " & CStr(objSrcCommonKeyDic.Count)
            
            If blnOnlyCommonFilesExists Then
            
                AddAutoShapeGeneralMessage objSheet, "Only common files exists", XlShapeTextLogInteriorFillGradientYellow
            End If
        End If
    
        If IsAtLeastOneDictionaryKeyValueExists(objSrcIndependentKeyDic) Then
        
            Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
            
            objSheet.Name = "SrcOnlyExistedFiles"
        
            OutputDicToSheet objSheet, objSrcIndependentKeyDic, GetColFromLineDelimitedChar(strFileInfomationTypesDelimitedByComma), GetDataTableSheetFormatterForFilesInfoDataTable(GetFileGeneralInfoColFromLineDelimitedChar(strFileInfomationTypesDelimitedByComma))
            
            AddAutoShapeGeneralMessage objSheet, "Existed files in only Src" & vbNewLine & "Count of found files: " & CStr(objSrcIndependentKeyDic.Count)
            
            If blnOnlySrcSideFilesExists Then
            
                AddAutoShapeGeneralMessage objSheet, "Only source side files exists", XlShapeTextLogInteriorFillGradientYellow
            End If
        End If
    
        If IsAtLeastOneDictionaryKeyValueExists(objDstIndependentKeyDic) Then
            
            Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
            
            objSheet.Name = "DstOnlyExistedFiles"
        
            OutputDicToSheet objSheet, objDstIndependentKeyDic, GetColFromLineDelimitedChar(strFileInfomationTypesDelimitedByComma), GetDataTableSheetFormatterForFilesInfoDataTable(GetFileGeneralInfoColFromLineDelimitedChar(strFileInfomationTypesDelimitedByComma))
            
            AddAutoShapeGeneralMessage objSheet, "Count of found files: " & CStr(objDstIndependentKeyDic.Count)
            
            If blnOnlyDstSideFilesExists Then
            
                AddAutoShapeGeneralMessage objSheet, "Only destination side files exists", XlShapeTextLogInteriorFillGradientYellow
            End If
        End If
        
        DeleteDummySheetWhenItExists objBook
    End If
End Sub


