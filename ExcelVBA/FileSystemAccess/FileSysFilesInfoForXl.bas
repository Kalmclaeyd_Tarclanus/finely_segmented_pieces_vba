Attribute VB_Name = "FileSysFilesInfoForXl"
'
'   Output the results to Excel Workbook after search files in the specified directries and get these attribute information
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun, 29/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub OutputResultOfFilesGeneralInformationIntoExcelSheet(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjFileSysSeachResult As FileSysSearchResult)

    Dim strSheetName As String, objNecessaryFileInfoTypeCol As Collection, objFieldTitlesCol As Collection, objDTSheetFormatter As DataTableSheetFormatter
    
    With vobjFileSysSeachResult
        
        With .FileSystemSearchCondition
        
            Set objNecessaryFileInfoTypeCol = GetFileGeneralInfoColFromLineDelimitedChar(.FileInfomationTypesDelimitedByComma)
            
            Set objFieldTitlesCol = GetFileGeneralInfoFieldTitles(objNecessaryFileInfoTypeCol)
            
            Set objDTSheetFormatter = GetDataTableSheetFormatterForFilesInfoDataTable(objNecessaryFileInfoTypeCol)
            
            Select Case .SearchedDataTableType
            
                Case InMemoryDataTableType.ExistedAsCollectionObjectInMemory
            
                    OutputColToSheet vobjSheet, vobjFileSysSeachResult.ResultDataTableCol, objFieldTitlesCol, objDTSheetFormatter
            
                Case InMemoryDataTableType.ExistedAsDictionaryObjectInMemory
                
                    OutputDicToSheet vobjSheet, vobjFileSysSeachResult.ResultDataTableDic, objFieldTitlesCol, objDTSheetFormatter
            End Select
        End With
        
        AddAutoShapeGeneralMessage vobjSheet, .GetMessageOfFileSystemSearchResult()
        
        AddAutoShapeGeneralMessage vobjSheet, GetLogOfFileSystemSearchCondition(.FileSystemSearchCondition), AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
    End With
End Sub


'''
'''
'''
Public Function GetFileSysSearchedResultsLoggingDirectoryPath() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\FileSysSearchedResults"
    
    ForceToCreateDirectory strDir

    GetFileSysSearchedResultsLoggingDirectoryPath = strDir
End Function


'''
'''
'''
Public Function GetLogOfFileSystemSearchCondition(ByVal vobjFileSysSearchCondition As FileSysSearchCondition) As String

    Dim strLog As String
    
    strLog = ""
    
    With vobjFileSysSearchCondition
    
        strLog = GetLogAboutSearchingInfoTypeAndWildCard(.FileInfomationTypesDelimitedByComma, .ExtensionTextByDelimitedChar, .RawWildCardsByDelimitedChar, .DelimitedChar) & vbNewLine
        
        strLog = strLog & GetLogOfSearchingHierarchicalOrder(vobjFileSysSearchCondition)
    End With
    
    GetLogOfFileSystemSearchCondition = strLog
End Function

'**---------------------------------------------
'** general Excel sheet preparation operations
'**---------------------------------------------
'''
'''
'''
Public Function GetWorkbookByDTVisualizationType(ByVal vstrBookPath As String, _
        ByVal venmDTVisualizationType As LogDTVisualizationType, _
        Optional ByVal vblnAllowToCraeteWhenIfItIsNothing As Boolean = True) As Excel.Workbook
    
    Dim objBook As Excel.Workbook
    
    Set objBook = Nothing

    If venmDTVisualizationType = GenerateNewBookWithDeletingOldSheets Then
    
        Set objBook = GetWorkbookAndPrepareForUnitTest(vstrBookPath)
        
    ElseIf venmDTVisualizationType = GenerateNewBookWithAddingSheet Then
        
        If vblnAllowToCraeteWhenIfItIsNothing Then
            
            Set objBook = GetWorkbook(vstrBookPath)
        Else
            Set objBook = GetWorkbookIfItExists(vstrBookPath)
        End If
    End If

    Set GetWorkbookByDTVisualizationType = objBook
End Function


'''
'''
'''
Public Function GetNewWorksheetByLoggingDataTableType(ByVal vstrOutputBookPath As String, _
        ByVal venmLogDTVisualizationType As LogDTVisualizationType, _
        ByVal vstrSheetName As String) As Excel.Worksheet
    
    
    Dim objSheet As Excel.Worksheet, objOutputBook As Excel.Workbook

    Select Case venmLogDTVisualizationType
    
        Case LogDTVisualizationType.GenerateNewBookWithAddingSheet
        
            Set objOutputBook = GetWorkbookByDTVisualizationType(vstrOutputBookPath, venmLogDTVisualizationType)
            
            Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objOutputBook)
        
            objSheet.Name = FindNewSheetNameWhenAlreadyExist(vstrSheetName, objOutputBook)
        
        Case LogDTVisualizationType.GenerateNewBookWithDeletingOldSheets
        
            Set objOutputBook = GetWorkbookByDTVisualizationType(vstrOutputBookPath, venmLogDTVisualizationType)
            
            DeleteSheetIfExists objOutputBook, vstrSheetName
            
            Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objOutputBook)
        
            objSheet.Name = vstrSheetName
        
        Case LogDTVisualizationType.AddNewSheetToCurrentBook
        
            If LCase(Trim(Replace(Application.Name, "Microsoft", ""))) = "excel" Then
        
                Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(GetCurrentOfficeFileObject())
        
                objSheet.Name = FindNewSheetNameWhenAlreadyExist(vstrSheetName, GetCurrentOfficeFileObject())
            End If
    End Select

    Set GetNewWorksheetByLoggingDataTableType = objSheet
End Function


'''
'''
'''
Public Sub OutputFilesGeneralInformationDTColIntoExcelSheet(ByRef robjSheet As Excel.Worksheet, _
        ByVal vobjDTCol As Collection, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection)
    
    Dim objOutputBook As Excel.Workbook, objSheet As Excel.Worksheet, objDataTableSheetFormatter As DataTableSheetFormatter
    
    OutputColToSheet objSheet, vobjDTCol, vobjFieldTitlesCol, GetDataTableSheetFormatterForFilesInfoDataTable(vobjNecessaryFileInfoTypeCol)
End Sub




'''
'''
'''
Public Function OutputFilesGeneralInformationDTColIntoExcelSheetByOutputBookPath(ByVal vobjDTCol As Collection, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal venmLogDTVisualizationType As LogDTVisualizationType, _
        ByVal vstrOutputBookPath As String, _
        ByVal vstrSheetName As String, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection) As Excel.Worksheet
    
    Dim objOutputBook As Excel.Workbook, objSheet As Excel.Worksheet, objDataTableSheetFormatter As DataTableSheetFormatter
    
    
    msubPrepareWorksheetAndFormatterForFilesInfomationDT objSheet, objOutputBook, objDataTableSheetFormatter, vstrOutputBookPath, venmLogDTVisualizationType, vstrSheetName, vobjNecessaryFileInfoTypeCol
    
    OutputColToSheet objSheet, vobjDTCol, vobjFieldTitlesCol, objDataTableSheetFormatter

    msubDeleteDummySheetOfLoggingWorkbookWhenItExists objOutputBook, venmLogDTVisualizationType
    
    Set OutputFilesGeneralInformationDTColIntoExcelSheetByOutputBookPath = objSheet
End Function


'''
'''
'''
Public Function OutputFilesGeneralInformationDTDicIntoExcelSheetByOutputBookPath(ByVal vobjDTDic As Scripting.Dictionary, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal venmLogDTVisualizationType As LogDTVisualizationType, _
        ByVal vstrOutputBookPath As String, _
        ByVal vstrSheetName As String, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection) As Excel.Worksheet
    
    
    Dim objOutputBook As Excel.Workbook, objSheet As Excel.Worksheet, objDataTableSheetFormatter As DataTableSheetFormatter
    
    
    msubPrepareWorksheetAndFormatterForFilesInfomationDT objSheet, objOutputBook, objDataTableSheetFormatter, vstrOutputBookPath, venmLogDTVisualizationType, vstrSheetName, vobjNecessaryFileInfoTypeCol
    
    OutputDicToSheet objSheet, vobjDTDic, vobjFieldTitlesCol, objDataTableSheetFormatter

    msubDeleteDummySheetOfLoggingWorkbookWhenItExists objOutputBook, venmLogDTVisualizationType
    
    Set OutputFilesGeneralInformationDTDicIntoExcelSheetByOutputBookPath = objSheet
End Function


'///////////////////////////////////////////////
'/// Internal Functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>robjSheet: Output</Argument>
''' <Argument>robjOutputBook: Output</Argument>
''' <Argument>robjFormatter: Output</Argument>
''' <Argument>vstrOutputBookPat: Input</Argument>
''' <Argument>venmLogDTVisualizationType: Input</Argument>
''' <Argument>vstrSheetName: Input</Argument>
''' <Argument>vobjNecessaryFileInfoTypeCol: Input</Argument>
Private Sub msubPrepareWorksheetAndFormatterForFilesInfomationDT(ByRef robjSheet As Excel.Worksheet, _
        ByRef robjOutputBook As Excel.Workbook, _
        ByRef robjFormatter As DataTableSheetFormatter, _
        ByVal vstrOutputBookPath As String, _
        ByVal venmLogDTVisualizationType As LogDTVisualizationType, _
        ByVal vstrSheetName As String, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection)


    Dim objDataTableSheetFormatter As DataTableSheetFormatter
    
    Set robjFormatter = GetDataTableSheetFormatterForFilesInfoDataTable(vobjNecessaryFileInfoTypeCol)

    Set robjSheet = GetNewWorksheetByLoggingDataTableType(vstrOutputBookPath, venmLogDTVisualizationType, vstrSheetName)
    
    Set robjOutputBook = robjSheet.Parent
End Sub


'''
'''
'''
''' <Argument>vobjOutputBook: Input</Argument>
''' <Argument>venmLogDTVisualizationType: Input</Argument>
Private Sub msubDeleteDummySheetOfLoggingWorkbookWhenItExists(ByVal vobjOutputBook As Excel.Workbook, _
        ByVal venmLogDTVisualizationType As LogDTVisualizationType)

    Select Case venmLogDTVisualizationType
    
        Case LogDTVisualizationType.GenerateNewBookWithDeletingOldSheets, LogDTVisualizationType.GenerateNewBookWithAddingSheet
        
            DeleteDummySheetWhenItExists vobjOutputBook
    End Select
End Sub

'''
'''
'''
Public Function GetDataTableSheetFormatterForFilesInfoDataTable(ByVal vobjNecessaryFileInfoTypeCol As Collection) As DataTableSheetFormatter

    Dim objDataTableSheetFormatter As DataTableSheetFormatter

    Set objDataTableSheetFormatter = New DataTableSheetFormatter
    
    With objDataTableSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
    
        Set .FieldTitleToColumnWidthDic = GetFieldTitleToColumnWidthDicOfFilesInformationFrom(vobjNecessaryFileInfoTypeCol)
    
        Set .ColumnsNumberFormatLocal = mfobjGetColumnsNumberFormatLocalFrom(vobjNecessaryFileInfoTypeCol)
    
        .RecordBordersType = RecordCellsGrayAll
    End With
    
    Set GetDataTableSheetFormatterForFilesInfoDataTable = objDataTableSheetFormatter
End Function


'''
'''
'''
Public Function GetFieldTitleToColumnWidthDicOfFilesInformationFrom(ByVal vobjNecessaryFileInfoTypeCol As Collection) As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary
    Dim varFileGeneralInfo As Variant, enmFileGeneralInfo As FileGeneralInfo
    
    Set objDic = New Scripting.Dictionary
    
    For Each varFileGeneralInfo In vobjNecessaryFileInfoTypeCol
    
        enmFileGeneralInfo = varFileGeneralInfo
    
        Select Case enmFileGeneralInfo
        
            Case FileGeneralInfo.giFileBaseName
            
                objDic.Add GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo), 84    ' 46
        
            Case FileGeneralInfo.giFileName, giLinkDestFileName
            
                objDic.Add GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo), 88    ' 50
            
            Case FileGeneralInfo.giFileSize
            
                objDic.Add GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo), 11
        
            Case FileGeneralInfo.giLastModifiedDate, FileGeneralInfo.giCreatedDate
            
                objDic.Add GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo), 22
            
            Case FileGeneralInfo.giFileExtension
            
                objDic.Add GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo), 12
            
            Case FileGeneralInfo.giParentDirectoryPath
            
                objDic.Add GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo), 37
                
            Case FileGeneralInfo.giRelativeFullPath, FileGeneralInfo.giRelativeDirectoryPath
            
                objDic.Add GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo), 32
            
            Case giReadOnly, giHidden, giSystem, giVolume, giDirectory, giArchive, giAlias, giCompressed
            
                objDic.Add GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo), 9.5
            
            Case FileGeneralInfo.giLinkDestination, FileGeneralInfo.giFullPath
            
                objDic.Add GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo), 70
        End Select
    Next

    Set GetFieldTitleToColumnWidthDicOfFilesInformationFrom = objDic
End Function


'''
'''
'''
Private Function mfobjGetColumnsNumberFormatLocalFrom(ByVal vobjNecessaryFileInfoTypeCol As Collection) As ColumnsNumberFormatLocalParam
    
    Dim objColumnsNumberFormatLocal As ColumnsNumberFormatLocalParam
    Dim varFileGeneralInfo As Variant, enmFileGeneralInfo As FileGeneralInfo
    
    
    Set objColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
    
    With objColumnsNumberFormatLocal
        
        For Each varFileGeneralInfo In vobjNecessaryFileInfoTypeCol
        
            enmFileGeneralInfo = varFileGeneralInfo
            
            Select Case enmFileGeneralInfo
            
                Case FileGeneralInfo.giFileSize
            
                    .SetNumberFormatLocalAndApplyFieldTitles "0.00 ""[kB]""", GetColFromLineDelimitedChar(GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo))
            End Select
        Next
        
        .SetNumberFormatLocalAndApplyFieldTitles "yyyy/m/d ""(""aaa"")"" hh:mm:ss", GetColFromLineDelimitedChar(GetTitlesTextDelimitedByCommaFromFileGeneralInfoGettings(vobjNecessaryFileInfoTypeCol, "Date"))
    End With
    
    Set mfobjGetColumnsNumberFormatLocalFrom = objColumnsNumberFormatLocal
End Function


