Attribute VB_Name = "SqlUTfSqLiteOdbcSheetExpander"
'
'   Sanity test to output SQL results into Excel sheets for a SQLite database using ADO ODBC
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'       Dependent on an installed SQLite ODBC driver
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrTestTableName As String = "Test_Table"


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToSQLUTfSqLiteConnector()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "SqLiteAdoConnector,SqLiteAdoSheetExpander,UTfOperateSqLite3,OperateSqLite3,UTfAdoConnectSqLiteDb,ADOConStrOfOdbcSqLite,ADOConStrOfDsnlessSqLite"
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** SqLiteAdoSheetExpander
'**---------------------------------------------
'''
''' Create an in-line table from the only SELECT sql inside
'''
Private Sub msubSanityTestToConnectByVirtualTableSqLiteAdoSheetExpander()
    
    Dim strSQL As String

    With New SqLiteAdoSheetExpander

        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")

        .AllowToRecordSQLLog = True

        'strSQL = "SELECT 1"

        'strSQL = "SELECT 'ABC', 'DEF'"

        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"

        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
        
        .CloseAll
    End With
End Sub


'''
''' Create an in-line table from the only SELECT sql inside
'''
Private Sub msubSanityTestToConfirmSqLiteVersion()
    
    Dim strSQL As String

    With New SqLiteAdoSheetExpander

        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")

        .AllowToRecordSQLLog = True

        strSQL = "SELECT sqlite_version()"

        .OutputInExistedBookByUnitTestMode strSQL, "SqLiteVersion"
         
        .CloseAll
    End With
End Sub

'''
'''
'''
Private Sub msubOpenCmdWithSqLiteDbParentFolder()

    OpenCmdWithSpecifyingBothWorkingDirectoryAndCmdWindowTitleText GetTemporarySqLiteDataBaseDir(), "Test SQLite db"
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetTableList()
    
    Dim strSQL As String

    With New SqLiteAdoSheetExpander

        With .DataTableSheetFormattingInterface
        
            .RecordBordersType = RecordCellsGrayAll
        End With

        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")

        .AllowToRecordSQLLog = True


        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"

        .OutputInExistedBookByUnitTestMode strSQL, "SqLiteTableList", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable

        .CloseAll
    End With
End Sub


'''
'''
'''
Private Sub msubSanityTestToCreateTable()
    
    Dim strSQL As String, strDbPath As String
    
    strDbPath = GetTemporarySqLiteDataBaseDir() & "TestDb02OfSqLiteAdoSheetExpander.sqlitedb"

    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
    
    With New SqLiteAdoSheetExpander

        .SetODBCParametersWithoutDSN strDbPath

        .AllowToRecordSQLLog = True

        ' strSQL = "CREATE TABLE " & mstrTestTableName & "(Col1, Col2, Col3)"
        
        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3)"

        .OutputCommandLogInExistedBookByUnitTestMode strSQL

        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"
        
        .OutputInExistedBookByUnitTestMode strSQL, "TableList", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
        
        .CloseAll
    End With
End Sub

'''
''' SQLite database sanity-test for SQL INSERT
'''
Private Sub msubSanityTestToConnectToSqLiteBySqlInsertTable()
    
    Dim strSQL As String

    With New SqLiteAdoSheetExpander

        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"

        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll

        .AllowToRecordSQLLog = True

        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface


        strSQL = "INSERT INTO " & mstrTestTableName & " VALUES (13, 14, 15, 'Type3')"

        .OutputCommandLogToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath()



        strSQL = "SELECT * FROM Test_Table"

        .OutputToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlResult", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable

        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"
        
        .OutputToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "TableList", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
        
        .CloseAll
    End With
End Sub

'''
''' SQLite database sanity-test for SQL DELETE
'''
Private Sub msubSanityTestToConnectToSqLiteBySqlDeleteTable()
    
    Dim strSQL As String, strDbPath As String

    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"

    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
    
    With New SqLiteAdoSheetExpander

        .SetODBCParametersWithoutDSN strDbPath

        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll

        .AllowToRecordSQLLog = True

        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface


        strSQL = "DELETE FROM " & mstrTestTableName & " WHERE ColText = 'Type1'"

        .OutputCommandLogInExistedBookByUnitTestMode strSQL



        strSQL = "SELECT * FROM Test_Table"

        .OutputInExistedBookByUnitTestMode strSQL, "SqlResult", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet

        .CloseAll
    End With
End Sub


'''
''' SQLite database sanity-test for SQL UPDATE
'''
Private Sub msubSanityTestToConnectToSqLiteBySqlUpdateTable()
    
    Dim strSQL As String, strDbPath As String

    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"

    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
    
    With New SqLiteAdoSheetExpander

        .SetODBCParametersWithoutDSN strDbPath

        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll

        .AllowToRecordSQLLog = True

        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface


        strSQL = "UPDATE " & mstrTestTableName & " SET Col1 = 31, Col2 = 32 WHERE Col3 = 9 AND ColText = 'Type2'"

        .OutputCommandLogToSpecifiedBook strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlUpdate", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable



        strSQL = "SELECT * FROM Test_Table"

        .OutputToSpecifiedBook strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlResult", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable

        .CloseCacheOutputBookOfOpenByPresetModeProcess
    End With
End Sub
