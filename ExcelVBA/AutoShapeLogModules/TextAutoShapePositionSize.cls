VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "TextAutoShapePositionSize"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   position and size parameters of Excel.Shape objects
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  3/May/2023    Kalmclaeyd Tarclanus    Derived from TextAutoShapeParam.cls
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

' Private menmAppearanceLogType As AutoShapeLog

Private menmAutoShapeType As Office.MsoAutoShapeType   '   Value of Office.MsoAutoShapeType

Private mudtRegion As AutoShapeRegion

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub Class_Initialize()
    
    menmAutoShapeType = msoShapeRectangle
End Sub
    

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Let AutoShapeType(ByVal vintType As Office.MsoAutoShapeType)

    menmAutoShapeType = vintType
End Property
Public Property Get AutoShapeType() As Office.MsoAutoShapeType

    AutoShapeType = menmAutoShapeType
End Property

'''
''' left x value of TopLeft on a sheet
'''
Public Property Let Left(ByVal vintLeft As Single)

    mudtRegion.Left = vintLeft
End Property
Public Property Get Left() As Single

    Left = mudtRegion.Left
End Property

'''
''' Top y value of TopLeft on a sheet
'''
Public Property Let Top(ByVal vintTop As Single)

    mudtRegion.Top = vintTop
End Property
Public Property Get Top() As Single

    Top = mudtRegion.Top
End Property

'''
''' Width of rectangle region on a sheet
'''
Public Property Let Width(ByVal vintWidth As Single)

    mudtRegion.Width = vintWidth
End Property
Public Property Get Width() As Single

    Width = mudtRegion.Width
End Property

'''
''' Height of rectangle region on a sheet
'''
Public Property Let Height(ByVal vintHeight As Single)

    mudtRegion.Height = vintHeight
End Property
Public Property Get Height() As Single

    Height = mudtRegion.Height
End Property


Public Property Get ShapeRegion() As AutoShapeRegion

    ShapeRegion = mudtRegion
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetSpecifiedShape(ByVal vobjSheet As Excel.Worksheet) As Excel.Shape

    Dim objShape As Excel.Shape

    With Me
    
        Set objShape = vobjSheet.Shapes.AddShape(.AutoShapeType, .Left, .Top, .Width, .Height)
    End With

    Set GetSpecifiedShape = objShape
End Function


