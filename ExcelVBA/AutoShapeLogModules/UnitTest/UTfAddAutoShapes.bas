Attribute VB_Name = "UTfAddAutoShapes"
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is dependent on Excel refercence
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 10/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

' Sanity test sheet name
Private Const mstrSheetNamePrefix = "ASTAddingTest" ' "AutoShapeTextAddingTest"

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' unit-test mode change enumeration
'''
Private Enum AddShapeTestMode

    Normal = 0
    
    AddtionalHeaderText = 1
    
    IndependentMessageAdd = 2
End Enum


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Public Sub SanityTestToAddAutoShapeMetaDesignInformation()

    Dim objSheet As Excel.Worksheet, strBookPath As String
    
    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeGeneratingSanityTests.xlsx"
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "TestOfAutoShapeLogs")
    
    msubSanityTestsToAddAutoShapeMetaDesignInformation objSheet
End Sub

'''
'''
'''
Public Sub SanityTestToAddAutoShapeMetaDesignInformationOnActiveSheet()

    Dim objSheet As Excel.Worksheet
    
    Set objSheet = ActiveSheet
    
    msubSanityTestsToAddAutoShapeMetaDesignInformation objSheet
End Sub

'''
'''
'''
Private Sub msubSanityTestsToAddAutoShapeMetaDesignInformation(ByRef robjSheet As Excel.Worksheet)

    AddAutoShapeMetaDesignInformation robjSheet, "TestInfo1"

    AddAutoShapeMetaDesignInformation robjSheet, "TestInfo2", "Test-Subject"
     
    AddAutoShapeMetaDesignInformation robjSheet, "Key06: Opq", "Test-Subject02"
End Sub


'''
'''
'''
Public Sub SanityTestToAddAutoShapeGeneralMessage()

    Dim objSheet As Excel.Worksheet, strBookPath As String
    
    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeGeneratingSanityTests.xlsx"

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "TestOfAutoShapeLogs")
    
    ClearCacheOfBackgroundRGBToBoolOfUsingWhiteFontDic
    
    msubSanityTestToAddAutoShapeGeneralMessageAboutAutoShapeMessageFillPatternType objSheet
End Sub

'''
'''
'''
Public Sub SanityTestToAddAutoShapeGeneralMessageOnActiveSheet()

    Dim objSheet As Excel.Worksheet
    
    Set objSheet = ActiveSheet
    
    ClearCacheOfBackgroundRGBToBoolOfUsingWhiteFontDic
    
    msubSanityTestToAddAutoShapeGeneralMessageAboutAutoShapeMessageFillPatternType objSheet
End Sub

'''
'''
'''
Private Sub msubSanityTestToAddAutoShapeGeneralMessageAboutAutoShapeMessageFillPatternType(ByRef robjSheet As Excel.Worksheet)

    AddAutoShapeGeneralMessage robjSheet, "Test1"

    AddAutoShapeGeneralMessage robjSheet, "Key02: Abc"

    AddAutoShapeGeneralMessage robjSheet, "Test2", AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow

    AddAutoShapeGeneralMessage robjSheet, "Key03: Def", AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow

    AddAutoShapeGeneralMessage robjSheet, "Test3", XlShapeTextLogInteriorFillOfWinRegistry

    AddAutoShapeGeneralMessage robjSheet, "Key04: Hij", XlShapeTextLogInteriorFillOfWinRegistry

    AddAutoShapeGeneralMessage robjSheet, "Key05: Klm", XlShapeTextLogInteriorFillRedCornerGradient
End Sub


'''
''' get cell position of the ActiveCell
'''
Private Sub GetActiveCellPositionToMsgBox()
    
    Dim strRes As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    Dim objWindow As Excel.Window, objShape As Excel.Shape
    
    Set objBook = ActiveCell.Worksheet.Parent
    
    Set objWindow = Application.Windows.Item(objBook.Name)
    
    With ActiveCell
    
        strRes = "Cell Left: " & CStr(.Left) & ", Cell Top: " & CStr(.Top) & ", Cell Width: " & CStr(.Width) & ", Cell Height: " & CStr(.Height)
    End With
    
    With objWindow
        
        strRes = strRes & vbCrLf & "Window Left: " & CStr(.Left) & ", Window Top: " & CStr(.Top) & ", Window Width: " & CStr(.Width) & ", Window Height: " & CStr(.Height)
    End With
        
    
    'MsgBox strRes
    Debug.Print strRes

    
    
    On Error Resume Next
    
    Set objSheet = ActiveCell.Worksheet
    
    Set objShape = Nothing
    
    Debug.Print TypeName(Selection)
    
    For Each objShape In objSheet.Shapes
    
        With objShape
        
            strRes = "ShapeName: = " & .Name & ",  Left: " & CStr(.Left) & ",  Top: " & CStr(.Top) & ",  Width: " & CStr(.Width) & ",  Height: " & CStr(.Height)
        End With
        
        Debug.Print strRes
    Next

    On Error GoTo 0

End Sub

'''
''' delete auto created shapes by unit test
'''
Private Sub msubDeleteAutoAddedRectangleTestShapes()

    Dim blnOldDisplayAlerts As Boolean
    Dim objSheet As Excel.Worksheet
    Dim objApplication As Excel.Application
    
    Set objApplication = ThisWorkbook.Application

    With objApplication

        blnOldDisplayAlerts = .DisplayAlerts
    
        .DisplayAlerts = False
        
        On Error GoTo ErrorHandler:
        
        For Each objSheet In ThisWorkbook.Worksheets
        
            If InStr(1, objSheet.Name, mstrSheetNamePrefix) > 0 Then
            
                objSheet.Delete
            End If
        Next
        
ErrorHandler:
        .DisplayAlerts = blnOldDisplayAlerts
    End With

End Sub


'''
'''
'''
Private Sub msubAddRectangleShapeUnitTest()

    Dim objBook As Excel.Workbook, strBookPath As String
    
    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeSimplyGenerateTests.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)


    msubAddGeneralMessageWithSheetPadding objBook, 1, 3
    
    msubAddGeneralMessageWithSheetPadding objBook, 55, 5

    msubAddGeneralMessageWithSheetPadding objBook, 5, 55, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfWinRegistry

    msubAddGeneralMessageWithSheetPadding objBook, 20, 5, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
    
    msubAddGeneralMessageWithSheetPadding objBook, 20, 5, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillRedCornerGradient
    
    
    DeleteDummySheetWhenItExists objBook
End Sub


'''
'''
'''
Private Sub msubSanityTestToCreateObjectOfTextAutoShapePositionSize()

    Dim objTextAutoShapePositionSize As TextAutoShapePositionSize
    
    Set objTextAutoShapePositionSize = New TextAutoShapePositionSize
    
    Debug.Print "Created a TextAutoShapePositionSize object"
End Sub

'''
'''
'''
Private Sub msubSanityTestToCreateObjectOfTextAutoShapeFontInterior()

    Dim objTextAutoShapeFontInterior As TextAutoShapeFontInterior
    
    Set objTextAutoShapeFontInterior = New TextAutoShapeFontInterior
    
    Debug.Print "Created a TextAutoShapeFontInterior object"

End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
''' test pattern codes
'''
Private Sub msubAddGeneralMessageWithSheetPadding(ByVal vobjBook As Excel.Workbook, _
        ByVal vintCountOfRows As Long, _
        ByVal vintCountOfColumns As Long, _
        Optional ByVal venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType = AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault)
    
    
    Dim objSheet As Excel.Worksheet, objShape As Excel.Shape, strSheetName As String
    
    strSheetName = mstrSheetNamePrefix & "GMessage_R" & CStr(vintCountOfRows) & "_C" & CStr(vintCountOfColumns)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(vobjBook)
    
    With objSheet
    
        .Name = FindNewSheetNameWhenAlreadyExist(strSheetName, vobjBook)
    End With
    
    SetDummyValues objSheet, vintCountOfRows, vintCountOfColumns
    
    AddAutoShapeGeneralMessage objSheet, "General Single Message adding-test at " & FormatDateTime(Now(), vbLongDate) & ", " & FormatDateTime(Now(), vbLongTime), venmAutoShapeMessageFillPatternType
End Sub



'''
'''
'''
Private Sub SetDummyValues(ByVal vobjSheet As Excel.Worksheet, ByVal vintCountOfRows As Long, ByVal vintCountOfColumns As Long)

    Dim varValues() As Variant, intRowIdx As Long, intColumnIdx As Long
    
    
    ReDim varValues(1 To vintCountOfRows, 1 To vintCountOfColumns)
    
    For intRowIdx = 1 To UBound(varValues, 1)
    
        For intColumnIdx = 1 To UBound(varValues, 2)
        
            varValues(intRowIdx, intColumnIdx) = "(" & CStr(intRowIdx) & ", " & CStr(intColumnIdx) & ")"
        Next
    Next

    vobjSheet.Range("A1").Resize(vintCountOfRows, vintCountOfColumns).Value = varValues
End Sub
