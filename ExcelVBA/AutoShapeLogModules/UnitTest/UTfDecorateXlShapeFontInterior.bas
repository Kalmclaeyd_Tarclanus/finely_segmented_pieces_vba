Attribute VB_Name = "UTfDecorateXlShapeFontInterior"
'
'   Sanity test for decorate Excel Auto-shape interior and font
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  3/May/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' comprehensive AutoShapeLog enumeration test
'''
Private Sub msubComprehensiveTestToCreateExcelShapeOfAutoShapeLogType()

    Dim objSheet As Excel.Worksheet, strBookPath As String, objShape As Excel.Shape
    Dim enmAutoShapeLog As AutoShapeLog, objAutoShapeAdjuster As ASWindowPositionAdjuster, varAutoShapeLog As Variant
    
    
    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeGeneratingComprehensiveTests.xlsx"
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "TypesOfAutoShapeLogs")
    
    Set objAutoShapeAdjuster = New ASWindowPositionAdjuster
    
    For Each varAutoShapeLog In mfobjGetAutoShapeLogCol()
    
        enmAutoShapeLog = varAutoShapeLog

        Set objShape = GetAdjustedPositionXlAutoShapeByAutoShapeLogType(objSheet, enmAutoShapeLog, objAutoShapeAdjuster)
    
        objShape.TextFrame2.TextRange.Characters.Text = "Enumeration " & GetAutoShapeLogTextFromEnm(enmAutoShapeLog) & " test-text"
    
        DecorateFontForeColorAndInterior objShape, enmAutoShapeLog
    Next
    
    ' logging
    objSheet.Cells(1.1).Value = "Logged time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
    
End Sub

'''
''' test for GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics operation
'''
Private Sub msubSanityTestToGetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics()

    ' GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics
    
    Dim objSheet As Excel.Worksheet, strBookPath As String, objShape As Excel.Shape, strDisplayText As String
    Dim enmAutoShapeLog As AutoShapeLog, objAutoShapeAdjuster As ASWindowPositionAdjuster, varAutoShapeLog As Variant
    
    
    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeGeneratingComprehensiveTests.xlsx"
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "TypesOfAutoShapeLogs")
    
    Set objAutoShapeAdjuster = New ASWindowPositionAdjuster
    
    For Each varAutoShapeLog In mfobjGetAutoShapeLogCol()
    
        enmAutoShapeLog = varAutoShapeLog

        strDisplayText = "Enumeration " & GetAutoShapeLogTextFromEnm(enmAutoShapeLog) & " test-text"

        Set objShape = GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics(objSheet, objAutoShapeAdjuster, enmAutoShapeLog, strDisplayText)
    Next
    
    ' logging
    objSheet.Cells(1, 1).Value = "Sanity-test for GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics"
    
    objSheet.Cells(2, 1).Value = "Logged time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
End Sub



'''
''' comprehensive AutoShapeMessageFillPatternType enumeration test
'''
Private Sub msubComprehensiveTestToCreateExcelShapeOfAutoShapeMessageFillPatternType()

    Dim objSheet As Excel.Worksheet, strBookPath As String, objShape As Excel.Shape
    Dim enmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType, objAutoShapeAdjuster As ASWindowPositionAdjuster, varAutoShapeMessageFillPatternType As Variant
    
    
    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeGeneratingComprehensiveTests.xlsx"
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "TypesOfShapeFillPattern")
    
    Set objAutoShapeAdjuster = New ASWindowPositionAdjuster
    
    
    For Each varAutoShapeMessageFillPatternType In mfobjGetAutoShapeMessageFillPatternTypeCol()
    
        enmAutoShapeMessageFillPatternType = varAutoShapeMessageFillPatternType

        Set objShape = GetAdjustedPositionXlAutoShapeByAutoShapeLogType(objSheet, XlShapeTextLogOfGeneral, objAutoShapeAdjuster)
    
        objShape.TextFrame2.TextRange.Characters.Text = "Enumeration " & GetAutoShapeMessageFillPatternTypeTextFromEnm(enmAutoShapeMessageFillPatternType) & " test-text"
    
        DecorateFontForeColorAndInterior objShape, XlShapeTextLogOfGeneral, enmAutoShapeMessageFillPatternType
    Next
    
    ' logging
    objSheet.Cells(1.1).Value = "Logged time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetAutoShapeLogCol() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add AutoShapeLog.XlShapeTextLogOfGeneral
        
        .Add AutoShapeLog.XlShapeTextLogOfUsedActualSQL
        
        .Add AutoShapeLog.XlShapeTextLogOfElapsedTimeAndRowCountAppearance
        
        .Add AutoShapeLog.XlShapeTextLogOfInputSourceTableFilePath
        
        .Add AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
    End With

    Set mfobjGetAutoShapeLogCol = objCol
End Function

'''
'''
'''
Private Function mfobjGetAutoShapeMessageFillPatternTypeCol() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault
        
        .Add AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
        
        .Add AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillRedCornerGradient
        
        .Add AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfWinRegistry
        
        .Add AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfNearPureBlackForDataTableTexts
    End With

    Set mfobjGetAutoShapeMessageFillPatternTypeCol = objCol

End Function


