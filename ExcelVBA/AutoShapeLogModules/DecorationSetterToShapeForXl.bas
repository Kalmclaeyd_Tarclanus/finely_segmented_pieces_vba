Attribute VB_Name = "DecorationSetterToShapeForXl"
'
'   decorate Excel auto-shape object
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 12/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// User defined types
'///////////////////////////////////////////////
Private Type AutoShapeTagValueFontDecorationSetType
    
    StartTag As String
    
    EndTag As String
    
    FontSize As Single
    
    RGBValue As Long
    
    ThemeColorIndex As Office.MsoThemeColorIndex
    
    IsUsedToThemeColorIndex As Boolean
    
    AreMultiTagsExisted As Boolean
End Type

'''
'''
'''
Private Type TargetTextFontDecorationSetType

    TargetText As String
    
    FontSize As Single
    
    RGBValue As Long
End Type


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjSingleWordRegExp As VBScript_RegExp_55.RegExp



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Decorate text font on Excel.Shape
'**---------------------------------------------
'''
'''
'''
Public Sub ChangeAutoShapeTagValueFonts(ByVal vobjShape As Excel.Shape, ByVal vobjDecorationTagValueSettings As Collection)

    Dim varDecoratingSetting As Variant
    Dim strDecoratingSetting As String, udtDecorationSetting As AutoShapeTagValueFontDecorationSetType

    If Not vobjDecorationTagValueSettings Is Nothing Then
    
        If vobjDecorationTagValueSettings.Count > 0 Then
        
            For Each varDecoratingSetting In vobjDecorationTagValueSettings
    
                strDecoratingSetting = varDecoratingSetting
    
                GetAutoShapeTagValueFontDecorationSetType udtDecorationSetting, strDecoratingSetting
    
                ChangeAutoShapeTagValueFont vobjShape, udtDecorationSetting
            Next
        End If
    End If
End Sub

'''
'''
'''
Public Sub ChangeAutoShapeTargetTextFonts(ByVal vobjShape As Excel.Shape, ByVal vobjDecorationTargetTextSettings As Collection)

    Dim varDecoratingSetting As Variant
    Dim strDecoratingSetting As String, udtTargetTextFontDecorationSetType As TargetTextFontDecorationSetType

    If Not vobjDecorationTargetTextSettings Is Nothing Then
    
        If vobjDecorationTargetTextSettings.Count > 0 Then
            
            For Each varDecoratingSetting In vobjDecorationTargetTextSettings
            
                strDecoratingSetting = varDecoratingSetting
            
                GetTargetTextFontDecorationSetType udtTargetTextFontDecorationSetType, strDecoratingSetting
                
                ChangeTargetTextFontDecorationForShape vobjShape, udtTargetTextFontDecorationSetType
            Next
        End If
    End If

End Sub


'''
'''
'''
Public Sub ChageTextDecorationInRange(ByVal vobjRange As Excel.Range, ByVal vobjDecorationTargetTextSettings As Collection)

    Dim varDecoratingSetting As Variant
    Dim strDecoratingSetting As String, udtTargetTextFontDecorationSetType As TargetTextFontDecorationSetType
    

    If Not vobjDecorationTargetTextSettings Is Nothing Then
    
        If vobjDecorationTargetTextSettings.Count > 0 Then
            
            For Each varDecoratingSetting In vobjDecorationTargetTextSettings
            
                strDecoratingSetting = varDecoratingSetting
            
                GetTargetTextFontDecorationSetType udtTargetTextFontDecorationSetType, strDecoratingSetting
                
                ChangeTargetTextFontDecorationForRange vobjRange, udtTargetTextFontDecorationSetType
            Next
        End If
    End If

End Sub


'**---------------------------------------------
'** About Form-controls on Excel
'**---------------------------------------------
'''
'''
'''
Public Function GetExcelShapeFormControlTypeName(ByVal venmFormControlType As Excel.XlFormControl) As String

    Dim strTypeName As String

    strTypeName = ""

    Select Case venmFormControlType
    
        Case Excel.XlFormControl.xlButtonControl
    
            strTypeName = "xlButtonControl"
    
        Case Excel.XlFormControl.xlCheckBox
    
            strTypeName = "xlCheckBox"
            
        Case Excel.XlFormControl.xlDropDown
            
            strTypeName = "xlDropDown"
            
        Case Excel.XlFormControl.xlEditBox
        
            strTypeName = "xlEditBox"
    
        Case Excel.XlFormControl.xlGroupBox
        
            strTypeName = "xlGroupBox"
    
        Case Excel.XlFormControl.xlLabel
    
            strTypeName = "xlLabel"
    
        Case Excel.XlFormControl.xlListBox
        
            strTypeName = "xlListBox"
        
        Case Excel.XlFormControl.xlOptionButton
        
            strTypeName = "xlOptionButton"
        
        Case Excel.XlFormControl.xlScrollBar
        
            strTypeName = "xlScrollBar"
    
        Case Excel.XlFormControl.xlSpinner
        
            strTypeName = "xlSpinner"
    
    End Select

    GetExcelShapeFormControlTypeName = strTypeName
End Function

'**---------------------------------------------
'** RegExp cache tools
'**---------------------------------------------
'''
'''
'''
Public Function GetSingleWordRegExp() As VBScript_RegExp_55.RegExp
    
    Const strJapaneseCharPattern As String = "-ñ@-ħ-ŬŜßê-êE["
    
    If mobjSingleWordRegExp Is Nothing Then
    
        Set mobjSingleWordRegExp = New VBScript_RegExp_55.RegExp
    
        With mobjSingleWordRegExp
        
            .Pattern = "[\w\." & strJapaneseCharPattern & "]{1,}"
        
        End With
    End If

    Set GetSingleWordRegExp = mobjSingleWordRegExp
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////


'''
'''
'''
Private Sub msutUnitTestOfChageTextDecorationInRange()

    Dim objRange As Excel.Range
    Dim strTargetText As String, objDecorationTextSettings As Collection
    
    strTargetText = "<File Name>"
    
    Set objDecorationTextSettings = New Collection
    
    With objDecorationTextSettings
    
        .Add "<File Name>;8;trRGBDarkOrangeFontColor"
        
        .Add "<Directory>;8;trRGBDarkOrangeFontColor"
    End With
    
    Set objRange = ActiveCell
    
    ChageTextDecorationInRange objRange, objDecorationTextSettings

End Sub


Private Sub msubSanityTestForCreateOneTestAutoShape02()
    
    Dim objSheet As Excel.Worksheet
    Dim objShape As Excel.Shape, udtDate As Date
    Dim objDecorationSettings As Collection
    
    
    Set objSheet = ActiveSheet
    
    udtDate = Now()
    
    Set objShape = AddAutoShapeGeneralMessage(objSheet, "Test text message at " & FormatDateAndWeekdayJapanese(udtDate) & " " & Format(udtDate, "hh:mm:ss"))
    
    With objShape
    
        If .TextFrame2.HasText Then
            With .TextFrame2.TextRange
            
                If .Text <> "" Then .Text = .Text & vbNewLine
            
                .Text = .Text & "An Tag01: ABC 2023" & vbNewLine
                .Text = .Text & "An Tag02: Def 2024 [Hour]" & vbNewLine
                .Text = .Text & "An Tag03: Def 2024"
            
            End With
        End If
    End With

    Set objDecorationSettings = New Collection
    
    With objDecorationSettings
    
        .Add "An Tag01:;" & vbNewLine & ";14;trRGBDeepBlueFontColor; "
        
        .Add "An Tag02:;" & "[Hour]" & ";15; ;ThemeColorAccent1"
    
        .Add "An Tag03:;" & vbNewLine & ";14; ;ThemeColorAccent5"
    End With
    
  
    ChangeAutoShapeTagValueFonts objShape, objDecorationSettings

End Sub


Private Sub msubSanityTestForCreateOneTestAutoShape()

    Dim objShape As Excel.Shape

    Set objShape = mfobjGetTestingAutoShape()

    With objShape
    
        If .TextFrame2.HasText Then
        
            Debug.Print .TextFrame2.TextRange.Text
        
            With .TextFrame2.TextRange
            
                If .Text <> "" Then .Text = .Text & vbNewLine
            
                .Text = .Text & "An Tag01: ABC 2023" & vbNewLine
                .Text = .Text & "An Tag02: Def 2024 [Hour]" & vbNewLine
                .Text = .Text & "An Tag03: Def 2024"
            
            End With
        
        End If
    End With

End Sub

'''
'''
'''
Private Sub msubSanityTestForDecoratePartOfCharsOfAutoShape()

    Dim objShape As Excel.Shape
    Dim objDecorationSettings As Collection
    
    'Dim strDecoratingSetting As String, udtDecorationSetting As AutoShapeTagValueFontDecorationSetType

    Set objShape = mfobjGetTestingAutoShape()

    Set objDecorationSettings = New Collection
    
    With objDecorationSettings
    
        .Add "An Tag01:;" & vbNewLine & ";14;trRGBDeepBlueFontColor; "
        
        .Add "An Tag02:;" & "[Hour]" & ";15; ;ThemeColorAccent1"
    
        .Add "An Tag03:;" & vbNewLine & ";14; ;ThemeColorAccent5"
    End With
    
  
    ChangeAutoShapeTagValueFonts objShape, objDecorationSettings

End Sub


Private Sub msubSanityOfGetRGBValueFromText()

    Debug.Print GetRGBValueFromText("RGB(1, 16, 255)")
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub ChangeAutoShapeTagValueFont(ByVal vobjShape As Excel.Shape, ByRef rudtDecorationSetting As AutoShapeTagValueFontDecorationSetType)

    Dim strText As String
    Dim intP1 As Long, intP2 As Long, strCutText As String, intStartPosition As Long, intStartTagPosition As Long

    With vobjShape.TextFrame2
    
        If .HasText Then
        
            With .TextRange
            
                intStartPosition = 1
            
                Do
                
                    intStartTagPosition = InStr(intStartPosition, .Text, rudtDecorationSetting.StartTag)
                
                    If intStartTagPosition > 0 Then
                
                        intP1 = intStartTagPosition + Len(rudtDecorationSetting.StartTag)
                    
                        If intP1 > 0 Then
                        
                            If rudtDecorationSetting.EndTag = vbNewLine Then
                            
                                rudtDecorationSetting.EndTag = vbLf
                            End If
                        
                            intP2 = InStr(intP1, .Text, rudtDecorationSetting.EndTag)
                            
                            If intP2 <= 0 Then
                            
                                intP2 = Len(.Text) + 1
                            End If
                            
                            With .Characters(intP1, intP2 - intP1)
                            
                                .Font.Size = rudtDecorationSetting.FontSize
                                
                                With .Font.Fill
                                
                                    If rudtDecorationSetting.IsUsedToThemeColorIndex Then
                                    
                                        .ForeColor.ObjectThemeColor = rudtDecorationSetting.ThemeColorIndex
                                    Else
                                        
                                        .ForeColor.RGB = rudtDecorationSetting.RGBValue
                                    End If
                                End With
                            
                            End With
                            
                            intStartPosition = intP2 + Len(rudtDecorationSetting.EndTag) - 1
                        End If
                    End If
                
                Loop While rudtDecorationSetting.AreMultiTagsExisted And (intStartTagPosition > 0)
            
            End With
        
        End If
    End With

End Sub

'''
'''
'''
Private Sub ChangeTargetTextFontDecorationForShape(ByVal vobjShape As Excel.Shape, ByRef rudtDecorationSetting As TargetTextFontDecorationSetType)

    Dim intP1 As Long

    With vobjShape.TextFrame2.TextRange
    
        ' Debug.Print .Text
    
        intP1 = InStr(1, .Text, rudtDecorationSetting.TargetText)
        
        If intP1 > 0 Then
        
            With .Characters(intP1, Len(rudtDecorationSetting.TargetText))
            
                With .Font
                    
                    .Size = rudtDecorationSetting.FontSize
                    
                    .Fill.ForeColor.RGB = rudtDecorationSetting.RGBValue
                    
                End With
            End With
        End If
    End With

End Sub

'''
'''
'''
Private Sub ChangeTargetTextFontDecorationForRange(ByVal vobjRange As Excel.Range, ByRef rudtDecorationSetting As TargetTextFontDecorationSetType)

    Dim intP1 As Long

    With vobjRange
    
        'Debug.Print .Text
    
        intP1 = InStr(1, .Text, rudtDecorationSetting.TargetText)
        
        If intP1 > 0 Then
        
            With .Characters(intP1, Len(rudtDecorationSetting.TargetText))
            
                With .Font
                    
                    .Size = rudtDecorationSetting.FontSize
                    
                    .Color = rudtDecorationSetting.RGBValue
                End With
            End With
        End If
    End With

End Sub




'''
'''
'''
Private Sub GetAutoShapeTagValueFontDecorationSetType(ByRef rudtAutoShapeTagValueFontDecorationSetType As AutoShapeTagValueFontDecorationSetType, ByVal vstrDecoratingSetting As String)

    Const strDelimiterChar As String = ";"
    
    Dim strValues() As String, strRGBText As String, strThemeColorIndex As String
    Dim enmThemeColorIndex As MsoThemeColorIndex
    
    strValues = Split(vstrDecoratingSetting, strDelimiterChar)
    
    With rudtAutoShapeTagValueFontDecorationSetType
    
        ' initialize
        .RGBValue = 0: .ThemeColorIndex = msoNotThemeColor
    
    
        .StartTag = strValues(0)
        
        .EndTag = strValues(1)
        
        .FontSize = CSng(strValues(2))
        
        strRGBText = Trim(strValues(3))
        
        If Not IsEmpty(strRGBText) Then
            
            If strRGBText <> "" Then
            
                .RGBValue = mfintGetRGBColorFromText(strRGBText)
                
                .IsUsedToThemeColorIndex = False
            End If
        End If
    
        strThemeColorIndex = Trim(strValues(4))
        
        If Not IsEmpty(strThemeColorIndex) Then
        
            If strThemeColorIndex <> "" Then
            
                enmThemeColorIndex = GetThemeColorIndexFromText(strThemeColorIndex)
            
                If enmThemeColorIndex <> msoNotThemeColor Then
                
                    .IsUsedToThemeColorIndex = True
                
                    .ThemeColorIndex = enmThemeColorIndex
                End If
            End If
        End If
        
        .AreMultiTagsExisted = False
        
        If UBound(strValues) >= 5 Then
        
            Select Case strValues(5)
            
                Case "MultiTags"
            
                    .AreMultiTagsExisted = True
            End Select
        End If
        
    End With
End Sub

'''
'''
'''
Private Sub GetTargetTextFontDecorationSetType(ByRef rudtTargetTextFontDecorationSetType As TargetTextFontDecorationSetType, ByVal vstrDecoratingSetting As String)

    Const strDelimiterChar As String = ";"
    
    Dim strValues() As String, strRGBText As String
    
    
    strValues = Split(vstrDecoratingSetting, strDelimiterChar)
    
    With rudtTargetTextFontDecorationSetType
    
        .TargetText = strValues(0)
        
        .FontSize = CSng(strValues(1))
        
        strRGBText = Trim(strValues(2))
        
        If Not IsEmpty(strRGBText) Then
            
            .RGBValue = mfintGetRGBColorFromText(strRGBText)
        End If
    End With
End Sub


'''
'''
'''
Private Function mfintGetRGBColorFromText(ByRef rstrRGBText As String) As Long

    Dim intRGBValue As Long, blnIsRGBValueDecided As Boolean
    

    blnIsRGBValueDecided = False
            
    With GetTrRGBTextToValueCacheDic()
    
        If .Exists(rstrRGBText) Then
        
            intRGBValue = .Item(rstrRGBText)
            
            blnIsRGBValueDecided = True
        End If
    End With
    
    If Not blnIsRGBValueDecided Then
    
        If rstrRGBText <> "" And InStr(1, UCase(rstrRGBText), "RGB") > 0 Then
        
            intRGBValue = GetRGBValueFromText(rstrRGBText)
            
            blnIsRGBValueDecided = True
        End If
    End If

    mfintGetRGBColorFromText = intRGBValue
End Function



'**---------------------------------------------
'** About getting a existed shape
'**---------------------------------------------
'''
'''
'''
Private Function GetAutoShapeRectanglesOrShapeFoldedCorners(ByVal vobjSheet As Excel.Worksheet) As Collection

    Dim objShape As Excel.Shape, objCol As Collection
    
    Set objCol = New Collection
    
    For Each objShape In vobjSheet.Shapes
    
        Select Case objShape.AutoShapeType
        
            Case msoShapeRectangle, msoShapeFoldedCorner, msoShapeVerticalScroll
        
                objCol.Add objShape
                
        End Select
    Next

    Set GetAutoShapeRectanglesOrShapeFoldedCorners = objCol
End Function



'''
'''
'''
Private Function CountOfRectangleOrShapeFoldedCornersOfSheet(ByVal vobjSheet As Excel.Worksheet) As Long

    Dim objShape As Excel.Shape, intCount As Long
    
    intCount = 0
    
    For Each objShape In vobjSheet.Shapes
    
        Select Case objShape.AutoShapeType
        
            Case msoShapeRectangle, msoShapeFoldedCorner, msoShapeVerticalScroll
            
                intCount = intCount + 1
        End Select
    Next
    
    CountOfRectangleOrShapeFoldedCornersOfSheet = intCount
End Function



Private Function mfobjGetTestingAutoShape() As Excel.Shape

    Dim objSheet As Excel.Worksheet, udtDate As Date
    
    Set objSheet = ActiveSheet

    If CountOfRectangleOrShapeFoldedCornersOfSheet(objSheet) = 0 Then

        udtDate = Now()

        AddAutoShapeGeneralMessage objSheet, "Test text message at " & FormatDateAndWeekdayJapanese(udtDate) & " " & Format(udtDate, "hh:mm:ss")
    End If

    Set mfobjGetTestingAutoShape = GetAutoShapeRectanglesOrShapeFoldedCorners(objSheet).Item(1)
End Function

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////


Private Sub msubSanityTestToGetTrRGBTextToValueCacheDicAfterAddAutoShapeMessage()

    Dim objSheet As Excel.Worksheet, objBook As Excel.Workbook
    Dim strLog As String, objDecorationTargetTextSettings As Collection
    
    msubGetTestingSheet objSheet, objBook
    
    Set objDecorationTargetTextSettings = New Collection
            
    With objDecorationTargetTextSettings

        .Add "The item;11;trRGBDarkOrangeFontColor"
        
        '.Add "The item;11;RGB(198,89,17)"
        
    End With
    
    strLog = "The item: ABCDE 1st"
    
    AddAutoShapeGeneralMessage objSheet, strLog, vobjDecorationTargetTextSettings:=objDecorationTargetTextSettings
    
    
    Set objDecorationTargetTextSettings = New Collection
            
    With objDecorationTargetTextSettings

        .Add "The item;11;trRGBDarkOrangeFontColor"
        
    End With

    strLog = "The item: ABCDE 2nd"

    AddAutoShapeGeneralMessage objSheet, strLog, vobjDecorationTargetTextSettings:=objDecorationTargetTextSettings
End Sub


'''
'''
'''
Private Sub msubSanityTestToAddAutoShapeMessage01()

    Dim udtDate As Date, objSheet As Excel.Worksheet, objBook As Excel.Workbook
    
    msubGetTestingSheet objSheet, objBook
    
    udtDate = Now()

    AddAutoShapeGeneralMessage objSheet, "Test text message at " & FormatDateAndWeekdayJapanese(udtDate) & " " & Format(udtDate, "hh:mm:ss")

End Sub


'''
'''
'''
Private Sub msubSanityTestOfShapesInformationOfActiveSheet()

    Dim objSheet As Excel.Worksheet, objShape As Excel.Shape
    
    Set objSheet = ActiveSheet

    For Each objShape In objSheet.Shapes
    
        Debug.Print "Shape name : [" & objShape.Name & "]"
        Debug.Print "Type : " & GetOfficeShapeTypeName(objShape.Type)
        Debug.Print "ShapeStyleIndex : " & GetOfficeShapeStyleIndexTypeName(objShape.ShapeStyle)
        Debug.Print "AutoShapeType : " & GetOfficeAutoShapeTypeName(objShape.AutoShapeType)
        Debug.Print ""
    Next

End Sub


'''
'''
'''
Private Sub msubGetTestingSheet(ByRef robjSheet As Excel.Worksheet, ByRef robjBook As Excel.Workbook)

    Dim strBookPath As String
    
    strBookPath = mfstrGetOutputBookDir() & "\TestOfAddingAutoShapeTexts.xlsx"

    Set robjBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    Set robjSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(robjBook)
    
    robjSheet.Name = "AutoShapeTests"
    
    DeleteDummySheetWhenItExists robjBook
End Sub


'''
'''
'''
Private Function mfstrGetOutputBookDir() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
    
    ForceToCreateDirectory strDir

    mfstrGetOutputBookDir = strDir
End Function


