Attribute VB_Name = "DataTableXlAutoShapeOut"
'
'   output some very small data-tables into Excel.Shape text on a sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on CurrentUserDomainUtility.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  4/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** General auto-shape logging
'**---------------------------------------------
'''
''' For serving small Collection data-table
'''
Public Sub OutputColToAutoShapeLogTextOnSheet(ByVal vobjSheet As Excel.Worksheet, ByVal vobjDTCol As Collection, Optional ByVal vstrSubjectOfMessage As String = "", Optional ByVal venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType = AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfNearPureBlackForDataTableTexts)

    Dim strDTText As String
    
    strDTText = GetTwoDimentionalTextFromCol(vobjDTCol)

    If vstrSubjectOfMessage <> "" Then
    
        strDTText = vstrSubjectOfMessage & vbNewLine & strDTText
    End If

    AddAutoShapeGeneralMessage vobjSheet, strDTText, venmAutoShapeMessageFillPatternType
End Sub

'''
''' For serving small Dictionary data-table
'''
Public Sub OutputDicToAutoShapeLogTextOnSheet(ByVal vobjSheet As Excel.Worksheet, ByVal vobjDTDic As Scripting.Dictionary, Optional ByVal vstrSubjectOfMessage As String = "", Optional ByVal venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType = AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfNearPureBlackForDataTableTexts)

    Dim strDTText As String
    
    strDTText = GetTwoDimentionalTextFromDic(vobjDTDic, "", ",", True, True)

    If vstrSubjectOfMessage <> "" Then
    
        strDTText = vstrSubjectOfMessage & vbNewLine & strDTText
    End If
    
    AddAutoShapeGeneralMessage vobjSheet, strDTText, venmAutoShapeMessageFillPatternType
End Sub


'**---------------------------------------------
'** Current this computer auto-shape logging
'**---------------------------------------------
'''
'''
'''
Public Sub OutputThisComputerGeneralInfoToAutoShapeLogTextOnSheet(ByVal vobjSheet As Excel.Worksheet, _
        Optional ByVal venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType = AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfNearPureBlackForDataTableTexts, _
        Optional ByVal vblnAllowToAddTimeStampToInfo As Boolean = True)

    Dim objThisComputerInfoDic As Scripting.Dictionary
    
    Set objThisComputerInfoDic = GetCurrentUserAndThisComputerInfoDic()
    
    If vblnAllowToAddTimeStampToInfo Then
    
        objThisComputerInfoDic.Add "Logged time", Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
    End If
    
    OutputDicToAutoShapeLogTextOnSheet vobjSheet, objThisComputerInfoDic, "About this computer", venmAutoShapeMessageFillPatternType
End Sub

