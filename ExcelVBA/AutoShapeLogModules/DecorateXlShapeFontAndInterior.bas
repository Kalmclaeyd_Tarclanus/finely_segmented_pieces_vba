Attribute VB_Name = "DecorateXlShapeFontAndInterior"
'
'   Decorate Excel Auto-shape interior and font
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  3/May/2023    Kalmclaeyd Tarclanus    Separated from DecorationSetterToXlSheet.bas and DataTableSheetOut.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About TextAutoShapeFontInterior
'**---------------------------------------------
'''
''' about shape-type and font
'''
Public Sub SetDefaultTextAutoShapeInteriorFontByShapeLogType(ByRef robjTextAutoShapeFontInterior As TextAutoShapeFontInterior, ByVal venmAutoShapeLog As AutoShapeLog)

    With robjTextAutoShapeFontInterior
    
        Select Case venmAutoShapeLog
        
            Case AutoShapeLog.XlShapeTextLogOfUsedActualSQL
        
                .FontNameFarEast = "Meiryo"
                
                .FontName = "Arial"
                
                .FontSize = 11
        
            Case AutoShapeLog.XlShapeTextLogOfElapsedTimeAndRowCountAppearance
            
                .FontNameFarEast = "MS P Gothic"
                
                .FontName = "Arial"
                
                .FontSize = 10.5
                
            Case AutoShapeLog.XlShapeTextLogOfInputSourceTableFilePath
            
                .FontNameFarEast = "MS P Gothic"
            
                .FontName = "Arial Narrow"
                
                .FontSize = 10
            
            Case AutoShapeLog.XlShapeTextLogOfGeneral
            
                .FontNameFarEast = "Meiryo"
                
                .FontName = "Arial"
                
                .FontSize = 10
                
            Case AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
            
                .FontNameFarEast = "Meiryo"
            
                .FontName = "Arial"
                
                .FontSize = 10
        End Select
    End With
End Sub


'''
'''
'''
Public Sub SetPositionAndSizeOfTextAutoShape(ByRef robjTextAutoShapePositionSize As TextAutoShapePositionSize, ByRef robjRegion As AutoShapeRegion)

    With robjTextAutoShapePositionSize
    
        .Left = robjRegion.Left
        
        .Top = robjRegion.Top
        
        .Width = robjRegion.Width
        
        .Height = robjRegion.Height
    End With
End Sub


'**---------------------------------------------
'** Decorate interiors for Excel.Shape objects
'**---------------------------------------------
'''
''' About XlShapeTextLogOfUsedActualSQL
'''
Public Sub DecorateXlShapeInteriorForSQLAppearance(ByRef robjShape As Excel.Shape)

    With robjShape.Fill

        With .ForeColor
        
            .ObjectThemeColor = msoThemeColorBackground1
            
            .TintAndShade = 0
            
            .Brightness = 0
        End With
        
        .Transparency = 0
        
        .Solid
    End With
End Sub

'''
''' About XlShapeTextLogOfElapsedTimeAndRowCountAppearance
'''
Public Sub DecorateXlShapeInteriorForElapsedTimeAndRowCountAppearance(ByRef robjShape As Excel.Shape)

    With robjShape.Fill

        .TwoColorGradient msoGradientHorizontal, 1

        .ForeColor.RGB = RGB(215, 230, 190)
        
        .BackColor.RGB = RGB(240, 245, 235)
        
        .Visible = msoTrue
    End With
End Sub

'''
''' About XlShapeTextLogOfInputSourceTableFilePath
'''
Public Sub DecorateXlShapeInteriorForTableSheetBookPathAppearance(ByRef robjShape As Excel.Shape)

    With robjShape.Fill

        .TwoColorGradient msoGradientHorizontal, 1
        
        If InStr(1, robjShape.TextFrame2.TextRange.Characters.Text, "CSV Directory") > 0 Then
        
            .ForeColor.RGB = RGB(115, 45, 25)
            
            .BackColor.RGB = RGB(190, 75, 60)
        Else
        
            .ForeColor.RGB = RGB(80, 98, 40)
            
            .BackColor.RGB = RGB(120, 145, 60)
        End If
    
        .Visible = msoTrue
    End With
End Sub


'''
''' AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
'''
Public Sub DecorateXlShapeInteriorForMetaDesignInformation(ByRef robjShape As Excel.Shape)

    With robjShape.Fill

        .TwoColorGradient msoGradientFromCorner, 4
        
        With .GradientStops
        
            .Insert RGB(251, 247, 243), 1, 0.02
            
            .Insert RGB(250, 227, 202), 0.82, 0.11
            
            .Insert RGB(246, 242, 226), 0.33, 0.18
            
            .Insert RGB(254, 253, 252), 0, 0.13
                             
            .Delete 1 ' delete default blue-color
            .Delete 1 ' delete default white-color
        End With
    End With
End Sub

'''
''' AutoShapeLog.XlShapeTextLogOfGeneral and XlShapeTextLogInteriorFillDefault
'''
Public Sub DecorateXlShapeInteriorForGeneralLogOfDefaultFill(ByRef robjShape As Excel.Shape)

    With robjShape.Fill

        .Visible = msoTrue
        
        With .ForeColor
        
            .RGB = RGB(68, 114, 196)
        
            ' temporary disabling...
            
'            .ObjectThemeColor = msoThemeColorAccent1
'
'            .TintAndShade = 0
'
'            .Brightness = 0
        End With
        
        .Transparency = 0.2
        
        .Solid
    End With
End Sub

'''
''' AutoShapeLog.XlShapeTextLogOfGeneral and XlShapeTextLogInteriorFillGradientYellow
'''
Public Sub DecorateXlShapeInteriorForGeneralLogOfGradientYellow(ByRef robjShape As Excel.Shape)

    With robjShape.Fill

        .TwoColorGradient msoGradientHorizontal, 1
                            
        With .GradientStops
        
            .Insert RGB(220, 140, 5), 0
            
            .Insert RGB(135, 130, 115), 0.35
            
            .Insert RGB(220, 140, 5), 0.53
            
            .Insert RGB(135, 130, 115), 0.82
            
            .Insert RGB(215, 140, 5), 1
        End With
        
        .GradientAngle = 60
        
        .Transparency = 0
        
        .Visible = msoTrue
    End With
End Sub

'''
''' AutoShapeLog.XlShapeTextLogOfGeneral and XlShapeTextLogInteriorFillRedCornerGradient
'''
Public Sub DecorateXlShapeInteriorForGeneralLogOfRedCornerGradient(ByRef robjShape As Excel.Shape)

    With robjShape.Fill

        .TwoColorGradient msoGradientFromCorner, 4
        
        With .GradientStops
        
            .Insert RGB(255, 50, 150), 1
            
            .Insert RGB(1, 165, 145), 0.25, 0.1
            
            .Insert RGB(85, 105, 50), 0.5, 0.2
            
            .Insert RGB(255, 105, 50), 0.75, 0.05
            
            .Insert RGB(50, 105, 255), 0
                             
                             
            .Delete 1 ' delete default blue-color
            .Delete 1 ' delete default white-color
                                  
        End With
                              
        .Visible = msoTrue
    End With
End Sub

'''
''' AutoShapeLog.XlShapeTextLogOfGeneral and XlShapeTextLogInteriorFillOfWinRegistry
'''
Public Sub DecorateXlShapeInteriorForGeneralLogOfWinRegistryLog(ByRef robjShape As Excel.Shape)

    With robjShape.Fill

         .TwoColorGradient msoGradientHorizontal, 1
         
         With .GradientStops
         
             .Insert RGB(200, 75, 20), 0, 0.22
             
             .Insert RGB(245, 125, 92), 0.35, 0.16
             
             .Insert RGB(120, 25, 33), 0.8, 0.46
             
             .Insert RGB(190, 70, 65), 1, 0.07
             
             .Delete 1 ' delete default blue-color
             .Delete 1 ' delete default white-color
         End With
         
         .GradientAngle = 90
        
         .Visible = msoTrue
    End With
End Sub

'''
''' AutoShapeLog.XlShapeTextLogOfGeneral and XlShapeTextLogInteriorFillOfNearPureBlackForDataTableTexts
'''
Public Sub DecorateXlShapeInteriorForGeneralLogOfNearPureBlackForDataTableTexts(ByRef robjShape As Excel.Shape)

    Dim i As Long, intCountOfGradients As Long

    With robjShape.Fill

         .TwoColorGradient msoGradientFromCenter, 1
         
         '.PresetGradient msoGradientFromCenter, 1, msoGradientSilver
         
         With .GradientStops
         
            intCountOfGradients = .Count
         
            .Insert RGB(9, 8, 7), 0, 0.3
            
            .Insert RGB(30, 21, 18), 0.33, 0.18
            
            .Insert RGB(43, 27, 27), 0.84, 0.46667
            
            .Insert RGB(30, 30, 30), 1, 0.2
             
             ' delete default color settings
             For i = 1 To intCountOfGradients
             
                .Delete 1
             Next

         End With
         
         .Visible = msoTrue
    End With
End Sub

'**---------------------------------------------
'** Decorate fonts for Excel.Shape objects
'**---------------------------------------------
'''
''' XlShapeTextLogOfUsedActualSQL
'''
Public Sub DecorateXlShapeFontsForGeneralLogOfSQLAppearance(ByRef robjShape As Excel.Shape)

    With robjShape.TextFrame2.TextRange.Characters.Font.Fill

        '.Visible = msoTrue

        .ForeColor.RGB = RGB(0, 0, 0)
        
        .Transparency = 0
        
        .Solid
    End With
End Sub

'''
''' XlShapeTextLogOfElapsedTimeAndRowCountAppearance
'''
Public Sub DecorateXlShapeFontsForGeneralLogOfElapsedTimeAndRowCountAppearance(ByRef robjShape As Excel.Shape)

    Dim objDecorationSettings As Collection
    
    
    DecorateXlShapeFontsForGeneralLogOfSQLAppearance robjShape

    Set objDecorationSettings = New Collection

    With objDecorationSettings

        .Add GetTextOfStrKeyAddAutoLogShapesSqlExecutionElapsedTime() & ":;" & GetTextOfStrKeyAddAutoLogShapesUnitSecond() & ";14; ;ThemeColorAccent2"
        
        .Add GetTextOfStrKeyAddAutoLogShapesExcelSheetProcessingElapsedTime() & ":;" & GetTextOfStrKeyAddAutoLogShapesUnitSecond() & ";14; ;ThemeColorAccent2"

        .Add GetTextOfStrKeyAddAutoLogShapesTableRecordCount() & ":;" & vbLf & ";14; ;ThemeColorAccent5"
        
        .Add GetTextOfStrKeyAddAutoLogShapesTableFieldCount() & ":;" & vbLf & ";14; ;ThemeColorAccent5"
        
        .Add "Records address:;" & vbLf & ";11;RGB(15, 15, 15); "
        
        .Add "Field-titles address:;" & vbLf & ";11;RGB(15, 15, 15); "
    End With

    ChangeAutoShapeTagValueFonts robjShape, objDecorationSettings
End Sub

'''
''' XlShapeTextLogOfInputSourceTableFilePath
'''
Public Sub DecorateXlShapeFontsForGeneralLogOfTableSheetBookPathAppearance(ByRef robjShape As Excel.Shape)

    With robjShape.TextFrame2.TextRange.Characters.Font.Fill

        '.Visible = msoTrue
                        
        With .ForeColor
        
            .ObjectThemeColor = msoThemeColorBackground1
            
            .TintAndShade = 0
            
            .Brightness = -0.05
        End With
        
        .Transparency = 0
        
        .Solid
    End With
End Sub

'''
''' General
'''
Public Sub DecorateXlShapeFontsForGeneralLogOfGeneral(ByRef robjShape As Excel.Shape)

    With robjShape.TextFrame2.TextRange.Characters.Font.Fill

        '.Visible = msoTrue
        
        With .ForeColor
        
            .ObjectThemeColor = msoThemeColorLight1
            
            .TintAndShade = 0
            
            .Brightness = 0
        End With
        
        .Transparency = 0
        
        .Solid
    End With
End Sub


'''
''' XlShapeTextLogOfMetaDesignInformation
'''
Public Sub DecorateXlShapeFontsForGeneralLogOfMetaDesignInformation(ByRef robjShape As Excel.Shape)

    With robjShape.TextFrame2.TextRange.Characters.Font.Fill

        .ForeColor.RGB = RGB(15, 15, 15)
        
        .Transparency = 0
        
        .Solid
    End With
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** Capture Excel.Shape fill-pattern parameters
'**---------------------------------------------
'''
''' catch the Fill.GradientStops properties from the current Worksheet
'''
Private Sub msubGetGradientStopsVBACodesIntoImmediateWindowByShapeNameOnCurrentActiveSheet()

    Dim objShape As Excel.Shape, objGradientStop As Office.GradientStop
    Dim intR As Long, intG As Long, intB As Long
    
    On Error Resume Next
    
    Set objShape = mfobjGetShapeByNameOnCurrentSheet("Test04")
    
    On Error GoTo 0
    
    If Not objShape Is Nothing Then
    
        Debug.Print "Shape name: " & objShape.Name & vbNewLine
    
        
    
        With objShape.Fill
        
            If .GradientStops.Count > 0 Then
            
                With objShape.Fill
                
                    Debug.Print "GradientColorType: " & CStr(.GradientColorType)
            
                    'Debug.Print "GradientDegreee: " & CStr(.GradientDegree)
                    
                    Debug.Print "GradientStyle: " & CStr(.GradientStyle)
                    
                    Debug.Print "GradientVariant: " & CStr(.GradientVariant)
                End With
            
                Debug.Print ""
            
                Debug.Print "With .GradientStops"
                
                For Each objGradientStop In .GradientStops
            
                    With objGradientStop
            
                        'Debug.Print GetTextFromRGB(.Color) & ", " & CStr(.Position) & ", " & CStr(.Transparency)
            
                        GetLongValueToRGB intR, intG, intB, .Color
                        
                        Debug.Print vbTab & ".Insert RGB(" & CStr(intR) & ", " & CStr(intG) & ", " & CStr(intB) & "), " & CStr(.Position) & ", " & CStr(.Transparency)
                        
                    End With
                Next
            End If
        End With
        
        Debug.Print "End With"
    End If

End Sub


'''
'''
'''
Private Function mfobjGetShapeByNameOnCurrentSheet(ByVal vstrShapeName As String) As Excel.Shape

    Dim objSheet As Excel.Worksheet, objShape As Excel.Shape, objFoundShape As Excel.Shape
    Dim blnIsFound As Boolean
    
    Set objSheet = ActiveSheet
    
    blnIsFound = False
    
    With objSheet
    
        For Each objShape In .Shapes
        
            If InStr(1, objShape.Name, vstrShapeName) > 0 Then
            
                Set objFoundShape = objShape
                
                blnIsFound = True
                
                Exit For
            End If
        Next
        
    End With
    
    Set mfobjGetShapeByNameOnCurrentSheet = objFoundShape
End Function
