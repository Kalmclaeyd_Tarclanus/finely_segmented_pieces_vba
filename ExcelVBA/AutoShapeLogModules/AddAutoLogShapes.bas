Attribute VB_Name = "AddAutoLogShapes"
'
'   Add some log onto the specified Excel sheet by auto-shape.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 10/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "AddAutoLogShapes"


Private Const msngBoundaryRacioWidth As Single = 1 / 2 ' 3 / 5
Private Const msngBoundaryRacioHeight As Single = 1 / 2 ' 3 / 5

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' auto-shape log appearance type enumeration
'''
Public Enum AutoShapeLog

    XlShapeTextLogOfGeneral = 0
    
    XlShapeTextLogOfUsedActualSQL = 1
    
    XlShapeTextLogOfElapsedTimeAndRowCountAppearance = 2
    
    XlShapeTextLogOfInputSourceTableFilePath = 3
    
    XlShapeTextLogOfMetaDesignInformation = 4
End Enum

'''
''' changing fill pattern type for general-message
'''
Public Enum AutoShapeMessageFillPatternType
    
    XlShapeTextLogInteriorFillDefault = 0
    
    XlShapeTextLogInteriorFillGradientYellow = 1      ' for example, Warning message
    
    XlShapeTextLogInteriorFillRedCornerGradient = 2   ' for example, Danger attension message
    
    XlShapeTextLogInteriorFillOfWinRegistry = 3       ' for Windows Registry Operation logging, horizontal gradient
    
    XlShapeTextLogInteriorFillOfNearPureBlackForDataTableTexts = 4    ' for example, small data-tables of both Collection or Dictionary, such as small size Key-Values
End Enum

'///////////////////////////////////////////////
'/// User defined types
'///////////////////////////////////////////////
Public Type AutoShapeRegion

    Left As Single  ' Left position of Excel active-window
    
    Top As Single   ' Top position of Excel active-window
    
    Width As Single ' Width of auto-shape rectangle in Excel active-window
    
    Height As Single    ' Height of auto-shape rectangle in Excel active-window
End Type

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Key-Value cache preparation for AddAutoLogShapes
'**---------------------------------------------
Private mobjStrKeyValueAddAutoLogShapesDic As Scripting.Dictionary

Private mblnIsStrKeyValueAddAutoLogShapesDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.



'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToAddAutoLogShapes()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "DecorationSetterToShape,DecorationSetterToShapeForXl,DecorateXlShapeFontAndInterior,UTfDecorateXlShapeFontInterior,UTfAddAutoShapes,ASWindowPositionAdjuster,TextAutoShapeFontInterior,TextAutoShapePositionSize"
End Sub


'**---------------------------------------------
'** Shape decoration enumeration conversions
'**---------------------------------------------
'''
'''
'''
Public Function GetAutoShapeLogTextFromEnm(ByVal venmAutoShapeLog As AutoShapeLog) As String

    Dim strText As String
    
    strText = ""
    
    Select Case venmAutoShapeLog
    
        Case AutoShapeLog.XlShapeTextLogOfUsedActualSQL
        
            strText = "XlShapeTextLogOfUsedActualSQL"
        
        Case AutoShapeLog.XlShapeTextLogOfElapsedTimeAndRowCountAppearance
        
            strText = "XlShapeTextLogOfElapsedTimeAndRowCountAppearance"
        
        Case AutoShapeLog.XlShapeTextLogOfInputSourceTableFilePath
        
            strText = "XlShapeTextLogOfInputSourceTableFilePath"
            
        Case AutoShapeLog.XlShapeTextLogOfGeneral
        
            strText = "XlShapeTextLogOfGeneral"
            
        Case AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
        
            strText = "XlShapeTextLogOfMetaDesignInformation"
    End Select

    GetAutoShapeLogTextFromEnm = strText
End Function

'''
'''
'''
Public Function GetAutoShapeMessageFillPatternTypeTextFromEnm(ByVal venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType) As String

    Dim strText As String
    
    strText = ""
    
    Select Case venmAutoShapeMessageFillPatternType
    
        Case AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault
        
            strText = "XlShapeTextLogInteriorFillDefault"
        
        Case AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
        
            strText = "XlShapeTextLogInteriorFillGradientYellow"
        
        Case AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillRedCornerGradient
        
            strText = "XlShapeTextLogInteriorFillRedCornerGradient"
        
        Case AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfWinRegistry
        
            strText = "XlShapeTextLogInteriorFillOfWinRegistry"
        
        Case AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfNearPureBlackForDataTableTexts
        
            strText = "XlShapeTextLogInteriorFillOfNearPureBlackForDataTableTexts"
    End Select

    GetAutoShapeMessageFillPatternTypeTextFromEnm = strText
End Function

'**---------------------------------------------
'** About AutoShape logging
'**---------------------------------------------
'''
'''
'''
Public Sub AddRectangleShapeWithText(ByRef robjShape As Excel.Shape, _
        ByVal vstrText As String, ByVal venmAutoShapeLog As AutoShapeLog, _
        Optional ByVal venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType = AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault, _
        Optional ByVal vsngFontSize As Single = 9.5, _
        Optional ByVal vstrAutoShapeObjectName As String = "")
    
    Dim objTextAutoShapeFontInterior As TextAutoShapeFontInterior
    
    robjShape.TextFrame2.TextRange.Characters.Text = vstrText
    
    Set objTextAutoShapeFontInterior = New TextAutoShapeFontInterior: SetDefaultTextAutoShapeInteriorFontByShapeLogType objTextAutoShapeFontInterior, venmAutoShapeLog
    
    With objTextAutoShapeFontInterior
    
        .FontSize = vsngFontSize
    End With
    
    DecorateAndSetDefaultShape robjShape, venmAutoShapeLog, venmAutoShapeMessageFillPatternType, objTextAutoShapeFontInterior, vstrAutoShapeObjectName
End Sub


'''
''' get most bottom shape position from existed work-sheet
'''
Public Function IsPreviousPositionCopiedToAnotherShape(ByVal vobjSheet As Excel.Worksheet, ByRef rudtCopyDestinationRegion As AutoShapeRegion) As Boolean

    Dim objShape As Excel.Shape, sngBottom As Single, sngPreviousBottom As Single, intCounter As Long
    Dim udtRegion As AutoShapeRegion, blnKeepRegion As Boolean, blnIsCopied As Boolean, blnDoProcess As Boolean
    
    blnIsCopied = False
    
    intCounter = 0
    
    For Each objShape In vobjSheet.Shapes
    
        With objShape
            
            blnDoProcess = False
            
            If objShape.Type = msoFormControl Then
            
                blnDoProcess = True ' includes Button control on the Worksheet
            End If
            
            Select Case objShape.AutoShapeType
            
                Case msoShapeRectangle, msoShapeFoldedCorner, msoShapeRoundedRectangle, msoShapeVerticalScroll
            
                    blnDoProcess = True
            End Select
            
            If blnDoProcess Then
            
                intCounter = intCounter + 1
                
                sngBottom = .Top + .Height
            
                blnKeepRegion = False
                
                If intCounter = 1 Then
                
                    blnKeepRegion = True
                Else
                    If sngBottom > sngPreviousBottom Then
                    
                        blnKeepRegion = True
                    End If
                End If
                
                If blnKeepRegion Then
                
                    With udtRegion
                    
                        .Top = objShape.Top: .Left = objShape.Left
                        
                        .Height = objShape.Height: .Width = objShape.Width
                    End With
                End If
                
                sngPreviousBottom = sngBottom
            End If
        End With
    Next

    If intCounter > 0 Then
    
        CopyAutoShapeRegion udtRegion, rudtCopyDestinationRegion
        
        blnIsCopied = True
    End If

    IsPreviousPositionCopiedToAnotherShape = blnIsCopied
End Function

'''
''' copy method of AutoShapeRegion
'''
Public Sub CopyAutoShapeRegion(ByRef robjFrom As AutoShapeRegion, ByRef robjTo As AutoShapeRegion)

    With robjTo
    
        .Left = robjFrom.Left: .Top = robjFrom.Top
        
        .Width = robjFrom.Width: .Height = robjFrom.Height
    End With
End Sub

'''
''' Set an Auto-shape Message box default size
'''
Public Sub DecideDefaultAutoShapeMessagePosition(ByRef rudtRegion As AutoShapeRegion, ByVal vobjSheet As Excel.Worksheet)

    Dim objBook As Excel.Workbook, objWindow As Excel.Window
    Dim blnBoundaryVertical As Boolean, blnBoundaryHorizontal As Boolean, sngUsedRight As Single, sngUsedBottom As Single


    Set objBook = vobjSheet.Parent
    
    Set objWindow = vobjSheet.Application.Windows.Item(objBook.Name)

    Select Case objWindow.WindowState
    
        Case XlWindowState.xlNormal, XlWindowState.xlMaximized
    
            With vobjSheet.UsedRange
            
                sngUsedRight = .Left + .Width
                
                sngUsedBottom = .Top + .Height
            End With
        
            blnBoundaryVertical = (sngUsedRight + 50) < (objWindow.Width * msngBoundaryRacioWidth)
            
            blnBoundaryHorizontal = (sngUsedBottom + 30) < (objWindow.Height * msngBoundaryRacioHeight)
            
            If (Not blnBoundaryHorizontal) And blnBoundaryVertical Then
            
                If (sngUsedRight + 50) < (objWindow.Width * msngBoundaryRacioWidth) Then
                
                    rudtRegion.Left = sngUsedRight + 20
                Else
                    rudtRegion.Left = objWindow.Width * msngBoundaryRacioWidth + 50
                End If
            
                rudtRegion.Top = 20
                
            ElseIf blnBoundaryHorizontal And (Not blnBoundaryVertical) Then
            
                rudtRegion.Left = 20
                
                If (sngUsedBottom + 50) < (objWindow.Height * msngBoundaryRacioHeight) Then
                
                    rudtRegion.Top = sngUsedBottom + 50
                Else
                    rudtRegion.Top = objWindow.Height * msngBoundaryRacioHeight + 50
                End If
            Else

                If (sngUsedRight + 50) < (objWindow.Width * msngBoundaryRacioWidth) Then
                
                    rudtRegion.Left = sngUsedRight + 50
                Else
                    rudtRegion.Left = objWindow.Width * msngBoundaryRacioWidth + 50
                End If
                
                If (sngUsedBottom + 50) < (objWindow.Height * msngBoundaryRacioHeight) Then
                    
                    rudtRegion.Top = sngUsedBottom + 50
                Else
                    rudtRegion.Top = objWindow.Height * msngBoundaryRacioHeight + 50
                End If
            End If
    End Select
End Sub


'''
'''
'''
Public Sub GetDefaultRectangleRegion(ByRef rudtRegion As AutoShapeRegion, ByVal venmAutoShapeLog As AutoShapeLog)

     ' About default size
    
    With rudtRegion
     
        Select Case venmAutoShapeLog
        
            Case AutoShapeLog.XlShapeTextLogOfUsedActualSQL
                
                .Width = 530: .Height = 60
                
            Case AutoShapeLog.XlShapeTextLogOfElapsedTimeAndRowCountAppearance
                
                .Width = 277: .Height = 138 ' 110 ' 94
                
            Case AutoShapeLog.XlShapeTextLogOfInputSourceTableFilePath
                
                .Width = 758: .Height = 24
                
            Case AutoShapeLog.XlShapeTextLogOfGeneral
                
                .Width = 435: .Height = 125
                
            Case AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
            
                .Width = 360: .Height = 150
        End Select
    End With
    
End Sub


'''
'''
'''
Private Function mfenmGetOfficeAutoShapeTypeFromAutoShapeLogType(ByVal venmAutoShapeLog As AutoShapeLog) As Office.MsoAutoShapeType

    Dim enmAutoShapeType As Office.MsoAutoShapeType

    Select Case venmAutoShapeLog
    
        Case AutoShapeLog.XlShapeTextLogOfUsedActualSQL
        
            enmAutoShapeType = Office.MsoAutoShapeType.msoShapeRectangle    ' Japanese as 矩形
        
        Case AutoShapeLog.XlShapeTextLogOfElapsedTimeAndRowCountAppearance
        
            enmAutoShapeType = Office.MsoAutoShapeType.msoShapeRoundedRectangle ' Japanese as 角丸矩形
        
        Case AutoShapeLog.XlShapeTextLogOfInputSourceTableFilePath
        
            enmAutoShapeType = Office.MsoAutoShapeType.msoShapeFoldedCorner ' Japanese as 右下角折り返し矩形
        
        Case AutoShapeLog.XlShapeTextLogOfGeneral
    
            enmAutoShapeType = Office.MsoAutoShapeType.msoShapeFoldedCorner ' Japanese as 右下角折り返し矩形
    
        Case AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
        
            enmAutoShapeType = msoShapeVerticalScroll   ' Japanese as 上下方向巻物
    End Select

    mfenmGetOfficeAutoShapeTypeFromAutoShapeLogType = enmAutoShapeType
End Function



'''
''' This is also a testing interface and includes GetAdjustedPositionXlAutoShapeByAutoShapeLogType
'''
Public Function GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics(ByVal robjSheet As Excel.Worksheet, _
        ByRef robjAutoShapeAdjuster As ASWindowPositionAdjuster, _
        ByVal venmAutoShapeLog As AutoShapeLog, _
        ByRef rstrDisplayText As String, _
        Optional ByVal venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType = AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault, _
        Optional ByVal vsngFontSize As Single = 9.5, _
        Optional ByVal vstrAutoShapeObjectName As String = "") As Excel.Shape


    Dim objShape As Excel.Shape

    Set objShape = GetAdjustedPositionXlAutoShapeByAutoShapeLogType(robjSheet, venmAutoShapeLog, robjAutoShapeAdjuster)

    AddRectangleShapeWithText objShape, rstrDisplayText, venmAutoShapeLog, venmAutoShapeMessageFillPatternType, vsngFontSize, vstrAutoShapeObjectName

    Set GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics = objShape
End Function

'''
''' This is also a testing interface
'''
Public Function GetAdjustedPositionXlAutoShapeByAutoShapeLogType(ByVal robjSheet As Excel.Worksheet, ByVal venmAutoShapeLog As AutoShapeLog, ByRef robjAutoShapeAdjuster As ASWindowPositionAdjuster) As Excel.Shape

    Dim objShape As Excel.Shape, objTextAutoShapePositionSize As TextAutoShapePositionSize, objAutoShapeAdjuster As ASWindowPositionAdjuster

    If robjAutoShapeAdjuster Is Nothing Then

        Set objAutoShapeAdjuster = New ASWindowPositionAdjuster
    Else
        Set objAutoShapeAdjuster = robjAutoShapeAdjuster
    End If

    Set objTextAutoShapePositionSize = New TextAutoShapePositionSize
    
    SetPositionAndSizeOfTextAutoShape objTextAutoShapePositionSize, objAutoShapeAdjuster.GetAdjustedPosition(robjSheet, venmAutoShapeLog)

    objTextAutoShapePositionSize.AutoShapeType = mfenmGetOfficeAutoShapeTypeFromAutoShapeLogType(venmAutoShapeLog)

    Set objShape = objTextAutoShapePositionSize.GetSpecifiedShape(robjSheet)

    Set GetAdjustedPositionXlAutoShapeByAutoShapeLogType = objShape
End Function



'''
'''
'''
Public Function AddAutoShapeMetaDesignInformation(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vstrMetaDesignInformation As String, _
        Optional ByVal vstrSubject As String = "", _
        Optional vobjExistedAutoShapeAdjuster As ASWindowPositionAdjuster = Nothing, _
        Optional ByVal vobjDecorationTagValueSettings As Collection = Nothing, _
        Optional ByVal vsngFontSize As Single = 9.5, _
        Optional ByVal vstrAutoShapeObjectName As String = "") As Excel.Shape
        
    Dim objAutoShapeAdjuster As ASWindowPositionAdjuster
    Dim objShape As Excel.Shape, objDecorationTargetTextSettings As Collection, strMessage As String


    If vobjExistedAutoShapeAdjuster Is Nothing Then
    
        Set objAutoShapeAdjuster = New ASWindowPositionAdjuster
        
        objAutoShapeAdjuster.GetPreviousPosition vobjSheet
    Else
        Set objAutoShapeAdjuster = vobjExistedAutoShapeAdjuster
    End If
    
    If vstrSubject <> "" Then
    
        strMessage = vstrSubject & vbNewLine & vstrMetaDesignInformation
    Else
        strMessage = vstrMetaDesignInformation
    End If
    

    Set objShape = GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics(vobjSheet, objAutoShapeAdjuster, XlShapeTextLogOfMetaDesignInformation, strMessage, XlShapeTextLogInteriorFillDefault, vsngFontSize, vstrAutoShapeObjectName)
    
    If vstrSubject <> "" Then
    
        Set objDecorationTargetTextSettings = New Collection
        
        With objDecorationTargetTextSettings
        
            .Add vstrSubject & ";16;trRGBDarkSepiaFontColor"
        End With
    End If
    
    msubDecorateAutoShapeSpecifiedTextsWithComplementingAutomaticParams objShape, vobjDecorationTagValueSettings, objDecorationTargetTextSettings
    
    Set AddAutoShapeMetaDesignInformation = objShape
End Function


'''
'''
'''
Public Function AddAutoShapeGeneralMessage(ByVal vobjSheet As Excel.Worksheet, ByVal vstrMessage As String, _
        Optional venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType = AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault, _
        Optional vobjExistedAutoShapeAdjuster As ASWindowPositionAdjuster = Nothing, _
        Optional ByVal vobjDecorationTagValueSettings As Collection = Nothing, _
        Optional ByVal vobjDecorationTargetTextSettings As Collection = Nothing, _
        Optional ByVal vsngFontSize As Single = 9.5, _
        Optional ByVal vstrAutoShapeObjectName As String = "") As Excel.Shape


    Dim objAutoShapeAdjuster As ASWindowPositionAdjuster, objShape As Excel.Shape
    
    
    If vobjExistedAutoShapeAdjuster Is Nothing Then
    
        Set objAutoShapeAdjuster = New ASWindowPositionAdjuster
        
        objAutoShapeAdjuster.GetPreviousPosition vobjSheet
    Else
        Set objAutoShapeAdjuster = vobjExistedAutoShapeAdjuster
    End If


    Set objShape = GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics(vobjSheet, objAutoShapeAdjuster, XlShapeTextLogOfGeneral, vstrMessage, venmAutoShapeMessageFillPatternType, vsngFontSize, vstrAutoShapeObjectName)
    
    
    msubDecorateAutoShapeSpecifiedTextsWithComplementingAutomaticParams objShape, vobjDecorationTagValueSettings, vobjDecorationTargetTextSettings
    
    Set AddAutoShapeGeneralMessage = objShape
End Function


'''
'''
'''
Public Sub DecorateAndSetDefaultShape(ByRef robjShape As Excel.Shape, ByVal venmAutoShapeLog As AutoShapeLog, ByVal venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType, ByRef robjTextAutoShapeFontInterior As TextAutoShapeFontInterior, Optional ByVal vstrAutoShapeObjectName As String = "")

    msubSetDefaultNameAndAdjustingSizeOption robjShape, venmAutoShapeLog, vstrAutoShapeObjectName

    msubSetCommonFontNameAndDefaultSize robjShape, robjTextAutoShapeFontInterior
    
    DecorateFontForeColorAndInterior robjShape, venmAutoShapeLog, venmAutoShapeMessageFillPatternType
End Sub


'''
''' This is also a testing interface.
'''
Public Sub DecorateFontForeColorAndInterior(ByRef robjShape As Excel.Shape, ByRef renmAutoShapeLog As AutoShapeLog, Optional ByVal venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType = AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault)

    msubSetAdjustmentsDefault robjShape, renmAutoShapeLog

    msubDecorateFontColorOfShape robjShape, renmAutoShapeLog
    
    msubDecorateFillPatternOfShape robjShape, renmAutoShapeLog, venmAutoShapeMessageFillPatternType
    
    msubDecorateContourLineOfShape robjShape, renmAutoShapeLog
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubDecorateContourLineOfShape(ByRef robjShape As Excel.Shape, ByVal venmAutoShapeLog As AutoShapeLog)

    With robjShape
    
        Select Case venmAutoShapeLog
        
            Case AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
            
                With .Line
                   
                    .ForeColor.RGB = RGB(70, 56, 38)
                    
                    .Transparency = 0.4
                    
                    .Weight = 0.25
                End With
            Case Else
            
                With .Line
                    
                    With .ForeColor
                    
                        .ObjectThemeColor = msoThemeColorText1
                        
                        .TintAndShade = 0
                        
                        .Brightness = 0
                    End With
                    
                    .Transparency = 0
                    
                    .Weight = 0.75
                End With
        End Select
    End With
End Sub

'''
'''
'''
Private Sub msubDecorateFillPatternOfShape(ByRef robjShape As Excel.Shape, ByVal venmAutoShapeLog As AutoShapeLog, ByVal venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType)

    With robjShape
   
        Select Case venmAutoShapeLog
        
            Case AutoShapeLog.XlShapeTextLogOfUsedActualSQL
                
                DecorateXlShapeInteriorForSQLAppearance robjShape
                
            Case AutoShapeLog.XlShapeTextLogOfElapsedTimeAndRowCountAppearance
                
                DecorateXlShapeInteriorForElapsedTimeAndRowCountAppearance robjShape
                
            Case AutoShapeLog.XlShapeTextLogOfInputSourceTableFilePath
            
                DecorateXlShapeInteriorForTableSheetBookPathAppearance robjShape
                
             Case AutoShapeLog.XlShapeTextLogOfGeneral
             
                Select Case venmAutoShapeMessageFillPatternType
                
                    Case AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault
                    
                        DecorateXlShapeInteriorForGeneralLogOfDefaultFill robjShape
                    
                    Case AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
                    
                        DecorateXlShapeInteriorForGeneralLogOfGradientYellow robjShape
                    
                    Case AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillRedCornerGradient
                    
                        DecorateXlShapeInteriorForGeneralLogOfRedCornerGradient robjShape
                    
                    Case AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfWinRegistry
                    
                        DecorateXlShapeInteriorForGeneralLogOfWinRegistryLog robjShape
                    
                    Case AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfNearPureBlackForDataTableTexts

                        DecorateXlShapeInteriorForGeneralLogOfNearPureBlackForDataTableTexts robjShape

                End Select
            
            Case AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
            
                DecorateXlShapeInteriorForMetaDesignInformation robjShape
        End Select
    End With
End Sub


'''
'''
'''
Private Sub msubDecorateFontColorOfShape(ByRef robjShape As Excel.Shape, ByVal venmAutoShapeLog As AutoShapeLog)

    With robjShape
    
        With .TextFrame2.TextRange.Characters
    
            Select Case venmAutoShapeLog
            
                Case AutoShapeLog.XlShapeTextLogOfUsedActualSQL
                    
                    DecorateXlShapeFontsForGeneralLogOfSQLAppearance robjShape
                     
                Case AutoShapeLog.XlShapeTextLogOfElapsedTimeAndRowCountAppearance
                
                    DecorateXlShapeFontsForGeneralLogOfElapsedTimeAndRowCountAppearance robjShape
                    
                Case AutoShapeLog.XlShapeTextLogOfInputSourceTableFilePath
                    
                    DecorateXlShapeFontsForGeneralLogOfTableSheetBookPathAppearance robjShape
'
                Case AutoShapeLog.XlShapeTextLogOfGeneral
                    
                    DecorateXlShapeFontsForGeneralLogOfGeneral robjShape
                    
                Case AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
                    
                    DecorateXlShapeFontsForGeneralLogOfMetaDesignInformation robjShape
                    
            End Select
        End With
    End With
End Sub

'''
'''
'''
Private Sub msubSetCommonFontNameAndDefaultSize(ByRef robjShape As Excel.Shape, ByRef robjTextAutoShapeFontInterior As TextAutoShapeFontInterior)

    ' Probably, this is supported after Excel 2007

    With robjShape.TextFrame2
        
        .MarginLeft = 2.83
        
        .MarginRight = 2.83
    
        With .TextRange
            
             With .Font
                
                .BaselineOffset = 0
                
                .NameComplexScript = "+mn-cs"
                
                If robjTextAutoShapeFontInterior.FontNameFarEast <> "" Then
                
                    .NameFarEast = robjTextAutoShapeFontInterior.FontNameFarEast
                Else
                    .NameFarEast = "+mn-ea"
                End If
                
                If robjTextAutoShapeFontInterior.FontSize > 0 Then
                
                    .Size = robjTextAutoShapeFontInterior.FontSize
                Else
                    .Size = 11
                End If
                
                If robjTextAutoShapeFontInterior.FontName <> "" Then
                
                    .Name = robjTextAutoShapeFontInterior.FontName
                Else
                    .Name = "+mn-lt"
                End If
            End With
            
            With .Characters.ParagraphFormat
                    
                .FirstLineIndent = 0
                
                .Alignment = msoAlignLeft
            End With
        End With
    End With
End Sub

'''
'''
'''
Private Sub msubSetAdjustmentsDefault(ByRef robjShape As Excel.Shape, ByVal venmAutoShapeLog As AutoShapeLog)

    With robjShape
    
        Select Case venmAutoShapeLog
        
            Case msoShapeRoundedRectangle   ' Japanese -> 角マル矩形
            
                .Item(1) = 0.098
                
            Case msoShapeRoundedRectangularCallout ' Japanese -> 吹き出し付角丸矩形
            
                .Item(1) = -0.53
                
            Case msoShapeFoldedCorner   ' Japanese -> 右下端折り矩形
            
                Select Case venmAutoShapeLog
                
                    Case AutoShapeLog.XlShapeTextLogOfGeneral
                        .Item(1) = 0.18
                    Case Else
                        .Item(1) = 0.3
                End Select
            Case msoShapeVerticalScroll ' Japanese -> 巻物
            
                '.Item(1) = 0.12
                .Item(1) = 0.09
                '.Item(1) = 0.05637
                '.Item(1) = 0.03
        End Select
    End With

End Sub

'''
'''
'''
Private Sub msubSetDefaultNameAndAdjustingSizeOption(ByRef robjShape As Excel.Shape, ByVal venmAutoShapeLog As AutoShapeLog, Optional ByVal vstrAutoShapeObjectName As String = "")

    Dim strObjectName As String
    
    If vstrAutoShapeObjectName <> "" Then
                
        strObjectName = vstrAutoShapeObjectName
    Else
        strObjectName = mfstrGetAutoShapeDefaultNameByLogType(venmAutoShapeLog)
    End If

    On Error Resume Next
    
    With robjShape
    
        ' set default name
        
        Select Case venmAutoShapeLog
        
            Case AutoShapeLog.XlShapeTextLogOfUsedActualSQL
            
                .Name = strObjectName
                
                If .TextFrame2.TextRange.Characters.Count <= 800 Then
                
                    .TextFrame2.AutoSize = msoAutoSizeShapeToFitText ' * Japanese as テキストに合わせて図形のサイズを調整する
                Else
                    .TextFrame2.AutoSize = msoAutoSizeTextToFitShape ' * Japanese as テキストを図形からはみ出して表示する
                    
                    '.TextFrame.HorizontalOverflow = xlOartHorizontalOverflowOverflow
                    .TextFrame.VerticalOverflow = xlOartVerticalOverflowOverflow
                End If
                
            Case AutoShapeLog.XlShapeTextLogOfElapsedTimeAndRowCountAppearance
            
                .Name = strObjectName
                
            Case AutoShapeLog.XlShapeTextLogOfInputSourceTableFilePath
                
                .Name = strObjectName
                
                .TextFrame2.AutoSize = msoAutoSizeShapeToFitText ' * Japanese as テキストに合わせて図形のサイズを調整する
                
            Case AutoShapeLog.XlShapeTextLogOfGeneral
            
                .Name = strObjectName
            
                If .TextFrame2.TextRange.Characters.Count <= 1200 Then
                
                    .TextFrame2.AutoSize = msoAutoSizeShapeToFitText ' * Japanese as テキストに合わせて図形のサイズを調整する
                Else
                    .TextFrame2.AutoSize = msoAutoSizeTextToFitShape ' * Japanese as テキストを図形からはみ出して表示する
                    
                    '.TextFrame.HorizontalOverflow = xlOartHorizontalOverflowOverflow
                    .TextFrame.VerticalOverflow = xlOartVerticalOverflowOverflow
                End If
                
            Case AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
            
                .Name = strObjectName
                
                .TextFrame2.AutoSize = msoAutoSizeShapeToFitText    ' * Japanese as テキストに合わせて図形のサイズを調整する
        End Select
    End With
     
    On Error GoTo 0
End Sub

'''
'''
'''
Private Function mfstrGetAutoShapeDefaultNameByLogType(ByRef renmAutoShapeLog As AutoShapeLog) As String

    Dim strName As String
    
    strName = ""
    
    Select Case renmAutoShapeLog
    
        Case AutoShapeLog.XlShapeTextLogOfUsedActualSQL
        
            strName = "RectangleLogSQL"
    
        Case AutoShapeLog.XlShapeTextLogOfInputSourceTableFilePath
    
            strName = "RectangleLogTableSheetBookPath"
    
        Case AutoShapeLog.XlShapeTextLogOfElapsedTimeAndRowCountAppearance
    
            strName = "RectangleLogElapsedTimeAndRowCount"
    
        Case AutoShapeLog.XlShapeTextLogOfGeneral
        
            strName = "RectangleGeneralBlueMessage"

        Case AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
        
            strName = "VerticalScrollMetaDesignInfo"

    End Select

    mfstrGetAutoShapeDefaultNameByLogType = strName
End Function



'''
'''
'''
Private Sub msubDecorateAutoShapeSpecifiedTextsWithComplementingAutomaticParams(ByVal vobjShape As Excel.Shape, _
        ByVal vobjDecorationTagValueSettings As Collection, ByVal vobjDecorationTargetTextSettings As Collection)


    Dim objDecorationTagValueSettings As Collection, objDecorationTargetTextSettings As Collection
    Dim objKeyValueDic As Scripting.Dictionary, strFirstRowLine As String, varKey As Variant, strKey As String, strValue As String
    Dim blnIsAutomaticTagValueSettingNeeded As Boolean, blnIsAutomaticTargetTextSettingNeeded As Boolean
    Dim strRightTerminator As String
    Dim blnIsLightColorRecommended As Boolean, strTrFontColorName As String, blnIsFontColorInitialized As Boolean
    
    blnIsAutomaticTagValueSettingNeeded = False: blnIsAutomaticTargetTextSettingNeeded = False
    
    blnIsFontColorInitialized = False
    
    If Not vobjDecorationTagValueSettings Is Nothing Then
    
        Set objDecorationTagValueSettings = vobjDecorationTagValueSettings
    Else
        blnIsAutomaticTagValueSettingNeeded = True
    End If

    If Not vobjDecorationTargetTextSettings Is Nothing Then
    
        Set objDecorationTargetTextSettings = vobjDecorationTargetTextSettings
    Else
        blnIsAutomaticTargetTextSettingNeeded = True
    End If


    If blnIsAutomaticTagValueSettingNeeded Or blnIsAutomaticTargetTextSettingNeeded Then
    
        Set objKeyValueDic = New Scripting.Dictionary
    
        AddKeyValueToDicInFromText objKeyValueDic, strFirstRowLine, vobjShape.TextFrame2.TextRange.Characters.Text, ": ", True
    
'        Debug.Print "Messaging AutoShape.Fill.BackColor: " & GetTextFromRGBAndHSB(vobjShape.Fill.BackColor)
'
'        Debug.Print "Messaging AutoShape.Fill.ForeColor: " & GetTextFromRGBAndHSB(vobjShape.Fill.ForeColor)
    
        If blnIsAutomaticTargetTextSettingNeeded Then
        
            If Not IsEmpty(strFirstRowLine) Then
        
                If Not blnIsFontColorInitialized Then
                
                    If vobjShape.Fill.ForeColor <> RGB(255, 255, 255) Then
                
                        blnIsLightColorRecommended = IsWhiteFontColorRecommendedWithUsingCache(vobjShape.Fill.ForeColor)
                    Else
                        blnIsLightColorRecommended = IsWhiteFontColorRecommendedWithUsingCache(vobjShape.Fill.BackColor)
                    End If
                    
                    blnIsFontColorInitialized = True
                End If
        
                Set objDecorationTargetTextSettings = New Collection
                
                With objDecorationTargetTextSettings
                
                    ' temporary disabling....
                    'strTrFontColorName = "trRGBPaleYellowFontColor"
                
                    If blnIsLightColorRecommended Then

                        strTrFontColorName = "trRGBPaleYellowFontColor"
                    Else
                        strTrFontColorName = "trRGBDarkSepiaFontColor"
                    End If
                
                    .Add strFirstRowLine & ";13;" & strTrFontColorName    ' trRGBWhiteFontColor
                End With
            End If
        End If
    
        If blnIsAutomaticTagValueSettingNeeded And Not objKeyValueDic Is Nothing Then
        
            If objKeyValueDic.Count > 0 Then
            
                If Not blnIsFontColorInitialized Then
                
                    If vobjShape.Fill.ForeColor <> RGB(255, 255, 255) Then
                
                        blnIsLightColorRecommended = IsWhiteFontColorRecommendedWithUsingCache(vobjShape.Fill.ForeColor)
                    Else
                        blnIsLightColorRecommended = IsWhiteFontColorRecommendedWithUsingCache(vobjShape.Fill.BackColor)
                    End If
                
                    'blnIsLightColorRecommended = IsWhiteFontColorRecommendedWithUsingCache(vobjShape.Fill.ForeColor)
                    
                    blnIsFontColorInitialized = True
                End If
            
                Set objDecorationTagValueSettings = New Collection
            
                With objKeyValueDic
                
                    For Each varKey In .Keys
                    
                        strKey = varKey
                        
                        strValue = .Item(varKey)
                    
                        strRightTerminator = vbNewLine
                    
                        If InStr(1, LCase(strKey), "time") > 0 And InStr(1, strValue, "[s]") > 0 Then
                        
                            strRightTerminator = "[s]"
                        End If
                        
                        
                        ' temporary disabling....
                        'strTrFontColorName = "trRGBLightGrayFontColor"
                        
                        If blnIsLightColorRecommended Then

                            strTrFontColorName = "trRGBLightGrayFontColor"
                        Else
                            strTrFontColorName = "trRGBBlackFontColor"
                        End If
                        
                        objDecorationTagValueSettings.Add strKey & ":;" & strRightTerminator & ";12;" & strTrFontColorName & "; "
                    Next
                End With
            End If
        End If
    End If

    msubDecorateAutoShapeSpecifiedTexts vobjShape, objDecorationTagValueSettings, objDecorationTargetTextSettings
End Sub




'''
'''
'''
Private Sub msubDecorateAutoShapeSpecifiedTexts(ByVal vobjShape As Excel.Shape, ByVal vobjDecorationTagValueSettings As Collection, ByVal vobjDecorationTargetTextSettings As Collection)

    If Not vobjDecorationTagValueSettings Is Nothing Then
    
        If vobjDecorationTagValueSettings.Count > 0 Then
        
            ChangeAutoShapeTagValueFonts vobjShape, vobjDecorationTagValueSettings
        End If
    End If
    
    If Not vobjDecorationTargetTextSettings Is Nothing Then
    
        If vobjDecorationTargetTextSettings.Count > 0 Then
        
            ChangeAutoShapeTargetTextFonts vobjShape, vobjDecorationTargetTextSettings
        End If
    End If
End Sub



'**---------------------------------------------
'** Key-Value cache preparation for AddAutoLogShapes
'**---------------------------------------------
'''
''' get string Value from STR_KEY_ADD_AUTO_LOG_SHAPES_SQL_EXECUTION_ELAPSED_TIME
'''
''' automatically-added for AddAutoLogShapes string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAddAutoLogShapesSqlExecutionElapsedTime() As String

    If Not mblnIsStrKeyValueAddAutoLogShapesDicInitialized Then

        msubInitializeTextForAddAutoLogShapes
    End If

    GetTextOfStrKeyAddAutoLogShapesSqlExecutionElapsedTime = mobjStrKeyValueAddAutoLogShapesDic.Item("STR_KEY_ADD_AUTO_LOG_SHAPES_SQL_EXECUTION_ELAPSED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADD_AUTO_LOG_SHAPES_EXCEL_SHEET_PROCESSING_ELAPSED_TIME
'''
''' automatically-added for AddAutoLogShapes string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAddAutoLogShapesExcelSheetProcessingElapsedTime() As String

    If Not mblnIsStrKeyValueAddAutoLogShapesDicInitialized Then

        msubInitializeTextForAddAutoLogShapes
    End If

    GetTextOfStrKeyAddAutoLogShapesExcelSheetProcessingElapsedTime = mobjStrKeyValueAddAutoLogShapesDic.Item("STR_KEY_ADD_AUTO_LOG_SHAPES_EXCEL_SHEET_PROCESSING_ELAPSED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADD_AUTO_LOG_SHAPES_TABLE_RECORD_COUNT
'''
''' automatically-added for AddAutoLogShapes string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAddAutoLogShapesTableRecordCount() As String

    If Not mblnIsStrKeyValueAddAutoLogShapesDicInitialized Then

        msubInitializeTextForAddAutoLogShapes
    End If

    GetTextOfStrKeyAddAutoLogShapesTableRecordCount = mobjStrKeyValueAddAutoLogShapesDic.Item("STR_KEY_ADD_AUTO_LOG_SHAPES_TABLE_RECORD_COUNT")
End Function

'''
''' get string Value from STR_KEY_ADD_AUTO_LOG_SHAPES_TABLE_FIELD_COUNT
'''
''' automatically-added for AddAutoLogShapes string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAddAutoLogShapesTableFieldCount() As String

    If Not mblnIsStrKeyValueAddAutoLogShapesDicInitialized Then

        msubInitializeTextForAddAutoLogShapes
    End If

    GetTextOfStrKeyAddAutoLogShapesTableFieldCount = mobjStrKeyValueAddAutoLogShapesDic.Item("STR_KEY_ADD_AUTO_LOG_SHAPES_TABLE_FIELD_COUNT")
End Function

'''
''' get string Value from STR_KEY_ADD_AUTO_LOG_SHAPES_SQL_EXECUTED_TIME
'''
''' automatically-added for AddAutoLogShapes string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAddAutoLogShapesSqlExecutedTime() As String

    If Not mblnIsStrKeyValueAddAutoLogShapesDicInitialized Then

        msubInitializeTextForAddAutoLogShapes
    End If

    GetTextOfStrKeyAddAutoLogShapesSqlExecutedTime = mobjStrKeyValueAddAutoLogShapesDic.Item("STR_KEY_ADD_AUTO_LOG_SHAPES_SQL_EXECUTED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADD_AUTO_LOG_SHAPES_UNIT_SECOND - English -> [s] or Japanese -> [秒]
'''
''' automatically-added for AddAutoLogShapes string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAddAutoLogShapesUnitSecond() As String

    If Not mblnIsStrKeyValueAddAutoLogShapesDicInitialized Then

        msubInitializeTextForAddAutoLogShapes
    End If

    GetTextOfStrKeyAddAutoLogShapesUnitSecond = mobjStrKeyValueAddAutoLogShapesDic.Item("STR_KEY_ADD_AUTO_LOG_SHAPES_UNIT_SECOND")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for AddAutoLogShapes string-key-value management for standard module and class module
Private Sub msubInitializeTextForAddAutoLogShapes()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueAddAutoLogShapesDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForAddAutoLogShapesByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForAddAutoLogShapesByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueAddAutoLogShapesDicInitialized = True
    End If

    Set mobjStrKeyValueAddAutoLogShapesDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for AddAutoLogShapes key-values cache
'''
''' automatically-added for AddAutoLogShapes string-key-value management for standard module and class module
Private Sub AddStringKeyValueForAddAutoLogShapesByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_SQL_EXECUTION_ELAPSED_TIME", "SQL execution elapsed time"
        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_EXCEL_SHEET_PROCESSING_ELAPSED_TIME", "Excel sheet processing elapsed time"
        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_TABLE_RECORD_COUNT", "record count"
        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_TABLE_FIELD_COUNT", "field count"
        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_SQL_EXECUTED_TIME", "SQL executed time"
        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_UNIT_SECOND", "[s]"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for AddAutoLogShapes key-values cache
'''
''' automatically-added for AddAutoLogShapes string-key-value management for standard module and class module
Private Sub AddStringKeyValueForAddAutoLogShapesByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_SQL_EXECUTION_ELAPSED_TIME", "SQL実行経過時間"
        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_EXCEL_SHEET_PROCESSING_ELAPSED_TIME", "Excelシート描画経過時間"
        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_TABLE_RECORD_COUNT", "レコード数"
        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_TABLE_FIELD_COUNT", "列数"
        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_SQL_EXECUTED_TIME", "SQL実行の日付時刻"
        .Add "STR_KEY_ADD_AUTO_LOG_SHAPES_UNIT_SECOND", "[秒]"
    End With
End Sub

'''
''' Remove Keys for AddAutoLogShapes key-values cache
'''
''' automatically-added for AddAutoLogShapes string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueAddAutoLogShapes(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_ADD_AUTO_LOG_SHAPES_SQL_EXECUTION_ELAPSED_TIME") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_ADD_AUTO_LOG_SHAPES_SQL_EXECUTION_ELAPSED_TIME"
            .Remove "STR_KEY_ADD_AUTO_LOG_SHAPES_EXCEL_SHEET_PROCESSING_ELAPSED_TIME"
            .Remove "STR_KEY_ADD_AUTO_LOG_SHAPES_TABLE_RECORD_COUNT"
            .Remove "STR_KEY_ADD_AUTO_LOG_SHAPES_TABLE_FIELD_COUNT"
            .Remove "STR_KEY_ADD_AUTO_LOG_SHAPES_SQL_EXECUTED_TIME"
            .Remove "STR_KEY_ADD_AUTO_LOG_SHAPES_UNIT_SECOND"
        End If
    End With
End Sub


