VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ASWindowPositionAdjuster"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is dependent on Excel refercence
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 12/Jan/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mudtDefaultRegion As AutoShapeRegion

Private mudtPreviousRegion As AutoShapeRegion

Private msngVerticalSpace As Single

Private mintSetPositionTimesCounter As Integer

'///////////////////////////////////////////////
'/// Class Initialize
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mintSetPositionTimesCounter = 0

    With mudtDefaultRegion
    
        .Left = 400: .Top = 350
        
        .Width = 300: .Height = 250
    End With

    msngVerticalSpace = 12
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Get VerticalSpace() As Single

    VerticalSpace = msngVerticalSpace
End Property
Public Property Let VerticalSpace(ByVal vsngVerticalSpace As Single)

    msngVerticalSpace = vsngVerticalSpace
End Property

'///////////////////////////////////////////////
'/// Operation
'///////////////////////////////////////////////
'''
''' get adjusted auto-shape position and size
'''
Public Function GetAdjustedPosition(ByVal vobjSheet As Excel.Worksheet, Optional ByVal venmType As AutoShapeLog = AutoShapeLog.XlShapeTextLogOfGeneral) As AutoShapeRegion

    Dim udtRegion As AutoShapeRegion

    CopyAutoShapeRegion mudtDefaultRegion, udtRegion
    
    GetDefaultRectangleRegion udtRegion, venmType
    
    ' About default position
    If mintSetPositionTimesCounter = 0 Then
        
        DecideDefaultAutoShapeMessagePosition udtRegion, vobjSheet
    Else
        Me.GetPreviousPosition vobjSheet    ' a size of shaped is automated adjusted when the shape specifiy msoAutoSizeShapeToFitText of TextFrame2.AutoSized property
        
        With udtRegion
        
            .Left = mudtPreviousRegion.Left
        
            .Top = mudtPreviousRegion.Top + mudtPreviousRegion.Height + msngVerticalSpace
        End With
    End If
    
    IncrementPositionTimesCounter
    
    CopyAutoShapeRegion udtRegion, mudtPreviousRegion

    GetAdjustedPosition = udtRegion
End Function

'''
''' get most bottom shape position from existed work-sheet
'''
Public Sub GetPreviousPosition(ByVal vobjSheet As Excel.Worksheet)

    If IsPreviousPositionCopiedToAnotherShape(vobjSheet, mudtPreviousRegion) Then
    
        IncrementPositionTimesCounter
    End If
End Sub


Public Sub IncrementPositionTimesCounter()
    
    mintSetPositionTimesCounter = mintSetPositionTimesCounter + 1
End Sub

