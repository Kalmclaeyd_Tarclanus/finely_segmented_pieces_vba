Attribute VB_Name = "UTfFindDiffFromXlSheet"
'
'   sanity test to find differences from two Excel.Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on UTfDecorationSetterToXlSheet.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 20/Jul/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** creating table-sheet for detect differences
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToPrepareTestSheets()

    msubPrepareTestSheets 5, 8, ""
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubPrepareTestSheets(Optional ByVal vintRowMax As Long = 1, _
        Optional ByVal vintColumnMax As Long = 1, _
        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "")


    Dim strTmpDir As String, strBookName As String, strBookPath As String

    Dim objBook As Excel.Workbook, objDTSheetFormatter As DataTableSheetFormatter
    
    Dim objDTCol As Collection, objFieldTitlesCol As Collection
    
    Dim objSheet01 As Excel.Worksheet, objSheet02 As Excel.Worksheet, objSheet03 As Excel.Worksheet, objSheet04 As Excel.Worksheet

    Dim intLengthOfChars As Long, intCountOfRandomSampleValuePatternType As Long

    Dim objReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic As Scripting.Dictionary


    strTmpDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\VariousSampleDataForXlSheetDiff"
    
    If Not DirectoryExistsByVbaDir(strTmpDir) Then
    
        ForceToCreateDirectoryByVbaDir strTmpDir
    End If
    
    intLengthOfChars = 5
    
    intCountOfRandomSampleValuePatternType = 7
    
    
    strBookName = "ThreePartSamples.xlsx"
    
    strBookPath = strTmpDir & "\" & strBookName
    
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    
    Set objSheet01 = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet01.Name = "IntegerAndString"
    
    SetupPadSimpleIntegerAndStringWithNullColumnsForRCTableColAndFieldTitles objDTCol, objFieldTitlesCol, vintRowMax, vintColumnMax, intLengthOfChars, vstrNullColumnIndexDelimitedComma
    
    Set objDTSheetFormatter = New DataTableSheetFormatter

    objDTSheetFormatter.RecordBordersType = RecordCellsGrayAll

    OutputColToSheet objSheet01, objDTCol, objFieldTitlesCol, objDTSheetFormatter
    
    
    
    Set objDTCol = Nothing: Set objFieldTitlesCol = Nothing
    
    Set objSheet02 = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    objSheet02.Name = "AllRandomValues"

    SetupPadSampleRandomSomeValueWithNullColumnsOnRCTableAndFieldTitles objDTCol, objFieldTitlesCol, vintRowMax, vintColumnMax, intCountOfRandomSampleValuePatternType, vstrNullColumnIndexDelimitedComma, False, True


    Set objDTSheetFormatter = New DataTableSheetFormatter

    objDTSheetFormatter.RecordBordersType = RecordCellsGrayAll

    OutputColToSheet objSheet02, objDTCol, objFieldTitlesCol, objDTSheetFormatter



    Set objSheet03 = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    objSheet03.Name = "IntAndStringWithRandom"

    Set objDTCol = Nothing: Set objFieldTitlesCol = Nothing

    SetupPadSimpleIntegerAndStringWithSampleRandomValueAtSpecifiedIndexesAndNullColumnsForRCTableColAndFieldTitles objDTCol, _
        objFieldTitlesCol, _
        vintRowMax, _
        vintColumnMax, _
        intLengthOfChars, _
        vstrNullColumnIndexDelimitedComma, _
        mfobjGetReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic(vintRowMax, vintColumnMax, ReplaceSampleRandomForAllFourCornerOfTable, "(2, 3)"), _
        intCountOfRandomSampleValuePatternType, _
        True

    Set objDTSheetFormatter = New DataTableSheetFormatter

    objDTSheetFormatter.RecordBordersType = RecordCellsGrayAll

    OutputColToSheet objSheet03, objDTCol, objFieldTitlesCol, objDTSheetFormatter



    Set objSheet04 = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    objSheet04.Name = "IntAndStringWithRandom02"

    Set objDTCol = Nothing: Set objFieldTitlesCol = Nothing

    SetupPadSimpleIntegerAndStringWithSampleRandomValueAtSpecifiedIndexesAndNullColumnsForRCTableColAndFieldTitles objDTCol, _
        objFieldTitlesCol, _
        vintRowMax, _
        vintColumnMax, _
        intLengthOfChars, _
        vstrNullColumnIndexDelimitedComma, _
        mfobjGetReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic(vintRowMax, vintColumnMax, ReplaceSampleRandomForLeftBottomOfTable Or ReplaceSampleRandomForRightTopOfTable, "(4, 6), (4, 5)"), _
        intCountOfRandomSampleValuePatternType, _
        True

    Set objDTSheetFormatter = New DataTableSheetFormatter

    objDTSheetFormatter.RecordBordersType = RecordCellsGrayAll

    OutputColToSheet objSheet04, objDTCol, objFieldTitlesCol, objDTSheetFormatter



    DeleteDummySheetWhenItExists objBook
End Sub

'''
'''
'''
Private Function mfobjGetReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic(Optional ByVal vintRowMax As Long = 1, _
        Optional ByVal vintColumnMax As Long = 1, _
        Optional ByVal venmReplacingSampleRandomValueOptionFlag As ReplacingSampleRandomValueOptionFlag, _
        Optional ByVal vstrParensNumbersDelimitedComma As String = "(2, 2)") As Scripting.Dictionary

    Dim objNestedDic As Scripting.Dictionary
    
    Dim varRowCol As Variant, objRowCol As Collection

    AddIndexesOfReplacingSampleRandomeValueByReplacingSampleRandomValueOptionFlagToNestedDic venmReplacingSampleRandomValueOptionFlag, vintRowMax, vintColumnMax, objNestedDic

    If vstrParensNumbersDelimitedComma <> "" Then
    
        For Each varRowCol In GetNumberDTColByParsingSimply(vstrParensNumbersDelimitedComma)
        
            Set objRowCol = varRowCol
        
            With objRowCol
            
                AddIndexesOfReplacingSampleRandomeValueToNestedDic .Item(1), .Item(2), objNestedDic
            End With
        Next
    End If

    Set mfobjGetReplacingRandomValueSpecifiedByRowColumnIndexesNestedDic = objNestedDic
End Function


'''
'''
'''
Private Sub msubSanityTestToAddIndexesOfReplacingSampleRandomeValueByReplacingSampleRandomValueOptionFlagToNestedDic()

    Dim objNestedDic As Scripting.Dictionary

    AddIndexesOfReplacingSampleRandomeValueByReplacingSampleRandomValueOptionFlagToNestedDic ReplaceSampleRandomForAllFourCornerOfTable, 5, 7, objNestedDic

    AddIndexesOfReplacingSampleRandomeValueToNestedDic 2, 2, objNestedDic
    
    
    DebugDic objNestedDic
End Sub











