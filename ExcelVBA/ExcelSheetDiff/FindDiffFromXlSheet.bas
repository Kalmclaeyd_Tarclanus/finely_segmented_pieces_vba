Attribute VB_Name = "FindDiffFromXlSheet"
'
'   find differences from two Excel.Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 20/Jul/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToFindDiffFromXlSheet()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UTfFindDiffFromXlSheet,DataTableCompressing,DataTableCompressingToXlSheet,ADOExSheetOut"
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** output diff result to another Excel.Worksheet
'**---------------------------------------------
'''
'''
'''
Public Sub OutputResultsOfGettingDiffRCValuesAndCurrentRegionsFromTwoSheetRangesToXlSheet(ByVal vstrOutputDir As String, _
        ByRef rstrBookPath01 As String, _
        ByRef rstrSheetName01 As String, _
        ByRef rstrRangeAddress01 As String, _
        ByRef rstrBookPath02 As String, _
        ByRef rstrSheetName02 As String, _
        ByRef rstrRangeAddress02 As String)


    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    Dim varDiffValues As Variant, objCurrentRegion01 As Excel.Range, objCurrentRegion02 As Excel.Range
    
    
    If Not DirectoryExistsByVbaDir(vstrOutputDir) Then
    
        ForceToCreateDirectoryByVbaDir vstrOutputDir
    End If

    strOutputBookPath = vstrOutputDir & "\DiffResultFromTwoExcelRanges.xlsx"

    Set objBook = GetWorkbook(strOutputBookPath, True)

    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet.Name = "DiffFromTwoRanges"

    DeleteDummySheetWhenItExists objSheet



    GetDiffRCValuesAndCurrentRegionsFromTwoSheetRanges varDiffValues, _
            objCurrentRegion01, _
            objCurrentRegion02, _
            rstrBookPath01, _
            rstrSheetName01, _
            rstrRangeAddress01, _
            rstrBookPath02, _
            rstrSheetName02, _
            rstrRangeAddress02


    objSheet.Cells(1, 1).Resize(UBound(varDiffValues, 1), UBound(varDiffValues, 2)).Value = varDiffValues

End Sub


'**---------------------------------------------
'** getting diff information as Variant array
'**---------------------------------------------
'''
'''
'''
''' <Argument>rvarDiffValues: Output</Argument>
''' <Argument>robjCurrentRegion01: Output</Argument>
''' <Argument>robjCurrentRegion02: Output</Argument>
''' <Argument>rstrBookPath01: Input</Argument>
''' <Argument>rstrSheetName01: Input</Argument>
''' <Argument>rstrRangeAddress01: Input</Argument>
''' <Argument>rstrBookPath02: Input</Argument>
''' <Argument>rstrSheetName02: Input</Argument>
''' <Argument>rstrRangeAddress02: Input</Argument>
Public Sub GetDiffRCValuesAndCurrentRegionsFromTwoSheetRanges(ByRef rvarDiffValues As Variant, _
        ByRef robjCurrentRegion01 As Excel.Range, _
        ByRef robjCurrentRegion02 As Excel.Range, _
        ByRef rstrBookPath01 As String, _
        ByRef rstrSheetName01 As String, _
        ByRef rstrRangeAddress01 As String, _
        ByRef rstrBookPath02 As String, _
        ByRef rstrSheetName02 As String, _
        ByRef rstrRangeAddress02 As String)
        
    
    Dim varValues01 As Variant, varValues02 As Variant
    
    
    msubGetVariantRCValuesAndCurrentRegionFromBookPathAndSheetNameAndCellAddress varValues01, robjCurrentRegion01, rstrBookPath01, rstrSheetName01, rstrRangeAddress01
    
    msubGetVariantRCValuesAndCurrentRegionFromBookPathAndSheetNameAndCellAddress varValues02, robjCurrentRegion02, rstrBookPath02, rstrSheetName02, rstrRangeAddress02
        
    
    msubGetDiffRCValuesFromTwoSourceRCValues rvarDiffValues, varValues01, varValues02
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////

'''
'''
'''
Private Sub msubGetDiffRCValuesFromTwoSourceRCValues(ByRef rvarDiffValues As Variant, _
        ByRef rvarSrcValues01 As Variant, _
        ByRef rvarSrcValues02 As Variant)
        
    Dim i As Long, j As Long, blnHasDiff As Boolean
    
    
    ReDim rvarDiffValues(1 To UBound(rvarSrcValues01, 1), 1 To UBound(rvarSrcValues01, 2))
    
    For i = 1 To UBound(rvarSrcValues01, 1)
    
        For j = 1 To UBound(rvarSrcValues01, 2)
    
            blnHasDiff = False
            
            Select Case True
            
                Case IsNull(rvarSrcValues01(i, j)) And IsNull(rvarSrcValues02(i, j))
            
                    ' Nothing to do
                Case IsNull(rvarSrcValues01(i, j)) And Not IsNull(rvarSrcValues02(i, j))
            
                    If IsEmpty(rvarSrcValues02(i, j)) Then
                    
                        ' Nothing to do
                        
                    ElseIf TypeName(rvarSrcValues02(i, j)) = "String" And rvarSrcValues02(i, j) = "" Then
                    
                        ' Nothing to do
                    Else
                        blnHasDiff = True
                    End If
                    
                Case Not IsNull(rvarSrcValues01(i, j)) And IsNull(rvarSrcValues02(i, j))
            
                    If IsEmpty(rvarSrcValues01(i, j)) Then
                    
                        ' Nothing to do
                        
                    ElseIf TypeName(rvarSrcValues01(i, j)) = "String" And rvarSrcValues01(i, j) = "" Then
                    
                        ' Nothing to do
                    Else
                        blnHasDiff = True
                    End If
                    
                Case Else
                
                    If TypeName(rvarSrcValues01(i, j)) = TypeName(rvarSrcValues02(i, j)) Then
                    
                        If rvarSrcValues01(i, j) <> rvarSrcValues02(i, j) Then
                        
                            blnHasDiff = True
                        End If
                    Else
                        If TypeName(rvarSrcValues01(i, j)) = "Date" Or TypeName(rvarSrcValues02(i, j)) = "Date" Then
                        
                            blnHasDiff = True
                        Else
                    
                            On Error Resume Next
                            
                            blnHasDiff = rvarSrcValues01(i, j) <> rvarSrcValues02(i, j)
                            
                            If Err.Number <> 0 Then
                            
                                blnHasDiff = True
                            End If
                            
                            On Error GoTo 0
                        End If
                    End If
            End Select
            
            
            If blnHasDiff Then
            
                rvarDiffValues(i, j) = True
            End If
        Next
    Next
End Sub


'''
'''
'''
''' <Argument>rvarValues: Output</Argument>
''' <Argument>robjCurrentRegion: Output</Argument>
''' <Argument>vstrBookPath: Input</Argument>
''' <Argument>vstrSheetName: Input</Argument>
''' <Argument>vstrCellAddress: Input</Argument>
Private Sub msubGetVariantRCValuesAndCurrentRegionFromBookPathAndSheetNameAndCellAddress(ByRef rvarValues As Variant, _
        ByRef robjCurrentRegion As Excel.Range, _
        ByVal vstrBookPath As String, _
        ByVal vstrSheetName As String, _
        ByVal vstrCellAddress As String)

    Set robjCurrentRegion = mfobjGetCurrentRegionFromBookPathAndSheetNameAndCellAddress(vstrBookPath, vstrSheetName, vstrCellAddress)

    msubGetVariantRCValuesFromRange rvarValues, robjCurrentRegion
End Sub


'''
'''
'''
''' <Argument>rvarValues: Output</Argument>
''' <Argument>robjRange: Input</Argument>
Private Sub msubGetVariantRCValuesFromRange(ByRef rvarValues As Variant, _
        ByRef robjRange As Excel.Range)

    If Not robjRange Is Nothing Then
    
        With robjRange
    
            ReDim rvarValues(1 To .Rows.Count, 1 To .Columns.Count)
    
            rvarValues = .Value
        End With
    End If
End Sub

'''
''' get Excel.Range.CurrentRegion from a Range object
'''
''' <Argument>vstrBookPath: Input</Argument>
''' <Argument>vstrSheetName: Input</Argument>
''' <Argument>vstrCellAddress: Input</Argument>
''' <Return>Excel.Range</Return>
Private Function mfobjGetCurrentRegionFromBookPathAndSheetNameAndCellAddress(ByVal vstrBookPath As String, _
        ByVal vstrSheetName As String, _
        ByVal vstrCellAddress As String) As Excel.Range

    Dim objRange As Excel.Range, objCurrentRegion As Excel.Range

    Set objCurrentRegion = Nothing
    
    Set objRange = mfobjGetRangeFromBookPathAndSheetNameAndCellAddress(vstrBookPath, vstrSheetName, vstrCellAddress)
    
    If Not objRange Is Nothing Then
    
        Set objCurrentRegion = objRange.CurrentRegion
    End If

    Set mfobjGetCurrentRegionFromBookPathAndSheetNameAndCellAddress = objCurrentRegion
End Function

'''
'''
'''
''' <Argument>vstrBookPath: Input</Argument>
''' <Argument>vstrSheetName: Input</Argument>
''' <Argument>vstrCellAddress: Input</Argument>
''' <Return>Excel.Range</Return>
Private Function mfobjGetRangeFromBookPathAndSheetNameAndCellAddress(ByVal vstrBookPath As String, _
        ByVal vstrSheetName As String, _
        ByVal vstrCellAddress As String) As Excel.Range
        
    Dim objBook As Excel.Workbook, objSheet As Excel.Worksheet, objRange As Excel.Range
    
    
    Set objRange = Nothing
    
    Set objBook = GetWorkbook(vstrBookPath, False)
    
    On Error Resume Next
    
    Set objSheet = Nothing
    
    Set objSheet = objBook.Worksheets.Item(vstrSheetName)
    
    If Not objSheet Is Nothing Then
    
        Set objRange = objSheet.Range(vstrCellAddress)
    End If
    
    On Error GoTo 0
        
    Set mfobjGetRangeFromBookPathAndSheetNameAndCellAddress = objRange
End Function







