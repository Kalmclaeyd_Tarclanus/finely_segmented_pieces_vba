Attribute VB_Name = "SqlUTfCsvAdoSheetExpander"
'
'   Sanity test to output SQL result into a Excel sheet for CSV files in a directory using ADO OLE DB
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
'       which should have been the installed when the Microsoft Office Excel software has been installed.
'       Dependent on UTfCreateDataTable.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Redesigned
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrTestCSVFileNamePrefix As String = "TestGenerated"


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** CsvAdoSheetExpander test
'**---------------------------------------------
'''
''' Test-connecting
'''
Private Sub ConnectTestToCSVFileByADO()

    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
    
    
    blnCSVGenerated = False
    
    strPath = mfstrGetSimpleTestCSVFilePath
    
    With New Scripting.FileSystemObject
    
        If Not .FileExists(strPath) Then

            msubGenerateTestingCSVFile

            blnCSVGenerated = True
        End If
    End With
    

    With New CsvAdoSheetExpander
    
        .SetInputCsvFileConnection strPath
        
        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strPath)
    
    
        With .DataTableSheetFormattingInterface
        
            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("col3,13")
        
            Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
            
            With .ColumnsNumberFormatLocal
            
                .SetNumberFormatLocalAndApplyFieldTitles "ddd, yyyy/m/d", GetColFromLineDelimitedChar("col3")
            End With
        End With

    
        .OutputInExistedBookByUnitTestMode strSQL, "Load_sample_csv"
        
        
        If blnCSVGenerated Then
        
            AddAutoShapeGeneralMessage .CurrentSheet, CreateObject("Scripting.FileSystemObject").GetFileName(strPath) & " is created now."
        End If
    End With
End Sub

'**---------------------------------------------
'** About SheetExpander comprehensive parameters
'**---------------------------------------------
'''
''' About CsvAdoSheetExpander
'''
Private Sub msubComprehensiveTestToCsvAdoSheetExpander()

    Dim varRowCol As Variant, objRowCol As Collection, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet, strSheetName As String, i As Long
    Dim strSQL As String
    
    Dim varAllowToShowFieldTitle As Variant
    Dim varAllowToRecordSQLLog As Variant, blnAfterAllowToRecordSQLLog As Boolean
    Dim objAdditionalLogDic As Scripting.Dictionary
    
    Dim strInputCSVPath As String
    
    Dim varExecutedAdoSqlQueryLogPositionType As Variant, enmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType
    
    
    strOutputBookPath = mfstrGetSheetDecorationTestBookDir() & "\CsvAdoSheetExpanderOutputComprensiveTest.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
    
    i = 1
    
    strInputCSVPath = GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(BasicSampleDT, 20)
    
    With New CsvAdoSheetExpander


        .SetInputCsvFileConnection strInputCSVPath

        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strInputCSVPath)
 
        For Each varAllowToRecordSQLLog In GetBooleanColFromLineDelimitedChar("True,False")

            For Each varExecutedAdoSqlQueryLogPositionType In GetAllExecutedAdoSqlQueryLogPositionTypesCol()
    
                enmExecutedAdoSqlQueryLogPositionType = varExecutedAdoSqlQueryLogPositionType
    
                For Each varAllowToShowFieldTitle In GetBooleanColFromLineDelimitedChar("True,False")
        
                    For Each varRowCol In GetDataTableSheetCreateTestConditionOnlyTopLeftDTCol(5, 7)
                    
                        Set objRowCol = varRowCol
                    
                        GetDataTableTopLeftWhenCreateTableOnSheet intTopLeftRowIndex, intTopLeftColumnIndex, objRowCol
                    
                        
                        .TopLeftRowIndex = intTopLeftRowIndex
                        
                        .TopLeftColumnIndex = intTopLeftColumnIndex
                        
                        .AllowToShowFieldTitle = varAllowToShowFieldTitle
                        
                        .AllowToRecordSQLLog = varAllowToRecordSQLLog
                        
                        .RecordBordersType = RecordCellsGrayAll
                        
                        If .AllowToShowFieldTitle Then

                            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FixedString,12,RowNumber,13,FlowerTypeSample,18,RandomInteger,17")
                        Else
                            Set .FieldTitleOrderToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Col1,12,Col2,13,Col3,18,Col4,17")
                        End If
                        
                        
                        Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
                        
                        strSheetName = "CsvSQLTest_" & Format(i, "00")
                        
                        objSheet.Name = strSheetName
                        
                        .OutputToSheetFrom strSQL, objSheet, enmExecutedAdoSqlQueryLogPositionType
                        
                        
                        Set objAdditionalLogDic = New Scripting.Dictionary
                        
                        With objAdditionalLogDic
                        
                            .Add "Input AllowToRecordSQLLog", varAllowToRecordSQLLog
'
'                            .Add "After-SQL-executed AllowToRecordSQLLog", blnAfterAllowToRecordSQLLog
                        
                            .Add "ExecutedAdoSqlQueryLogPositionType", GetExecutedAdoSqlQueryLogPositionTypeTextFromEnm(enmExecutedAdoSqlQueryLogPositionType)
                            
                            .Add "AllowToShowFieldTitle", varAllowToShowFieldTitle
                            
                            .Add "TopLeftRowIndex", intTopLeftRowIndex
                            
                            .Add "TopLeftColumnIndex", intTopLeftColumnIndex
                        End With
                        
                        OutputDicToAutoShapeLogTextOnSheet objSheet, objAdditionalLogDic, "CSV connecting ADOSheetFormatter some parameters test"
                        
                        i = i + 1
                    Next
                Next
            Next
        Next
    End With

    DeleteDummySheetWhenItExists objBook
End Sub

'''
'''
'''
Private Function mfstrGetSheetDecorationTestBookDir() As String

    Dim strDir As String

    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\ADOSheetFormatterTest"

    ForceToCreateDirectory strDir
    
    mfstrGetSheetDecorationTestBookDir = strDir
End Function


'**---------------------------------------------
'** CsvAdoConnector connecting by generated sample CSV
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTest01ToConnectCSVTestAfterCSVCreate()

    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate BasicSampleDT, 100, False
End Sub


Private Sub msubSanityTest02ToConnectCSVTestAfterCSVCreate()

    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate IncludeFictionalProductionNumberSampleDT, 10, True
End Sub


Private Sub msubSanityTest02ToConnectCSVTestAfterCSVSampleMasterTableOfFishType()

    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate FishTypeSampleMasterDT, vblnIncludesSQLLogToSheet:=True
End Sub

Private Sub msubSanityTest02ToConnectCSVTestAfterCSVSampleMasterTableOfFlowerType()

    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate FlowerTypeSampleMasterDT, vblnIncludesSQLLogToSheet:=True
End Sub


'''
'''
'''
Private Sub msubSanityTestToConectCSVWithJOINSQL()

    Dim strInputCsvPath01 As String, strInputCsvPath02 As String
    Dim strTableName01 As String, strTableName02 As String, strDir As String
    Dim strSQL As String
    
    strInputCsvPath01 = GetCSVPathAfterCreateSampleDataTable(IncludeFictionalProductionNumberSampleDT, 1000)

    strInputCsvPath02 = GetCSVPathAfterCreateSampleDataTable(FishTypeSampleMasterDT)

    With New Scripting.FileSystemObject

        strDir = .GetParentFolderName(strInputCsvPath01)
        
        strTableName01 = "[" & .GetFileName(strInputCsvPath01) & "]"
        
        strTableName02 = "[" & .GetFileName(strInputCsvPath02) & "]"

    End With

    With New CsvAdoSheetExpander
    
        .SetInputCsvDirectoryConnection strDir
        
        .RecordBordersType = RecordCellsGrayAll
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("RowNumber,12,RealNumber,12,FishType,11,FictionalProductionSerialNumber,30,FishTypeJpn,11,RandAlpha,11")
    
    
        strSQL = "SELECT * FROM " & strTableName01
        
        .OutputInExistedBookByUnitTestMode strSQL, "1stTable"
        
        strSQL = "SELECT * FROM " & strTableName02
        
        .OutputInExistedBookByUnitTestMode strSQL, "2ndTable"
        
        'strSQL = "SELECT A.RowNumber, A.RealNumber, A.FishType FROM " & strTableName01 & " AS A"
        
        strSQL = "SELECT A.RowNumber, A.RealNumber, A.FishType, B.FishTypeJpn, B.RandAlpha FROM " & strTableName01 & " AS A LEFT JOIN " & strTableName02 & " AS B ON A.FishType = B.FishTypeEng"
        
    
        .OutputInExistedBookByUnitTestMode strSQL, "SQLResult"
    
        AddAutoShapeGeneralMessage .CurrentSheet, "The SQL 'JOIN' can be also used by connecting plural CSV files in a conntected directory"
    End With
End Sub


'**---------------------------------------------
'** CSV directory connection tests
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToTryToConnectCSVDirectory()

    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
    Dim objCsvADOConnectStrGenerator As ADOConStrOfAceOleDbCsv
    
    blnCSVGenerated = False
    
    msubDeleteTestingCSVFile    ' If the directory path exists, the ADO connection is to be succeeded even though no CSV file exist.
    
    strPath = mfstrGetSimpleTestCSVFilePath
    
    Set objCsvADOConnectStrGenerator = New ADOConStrOfAceOleDbCsv
    
    objCsvADOConnectStrGenerator.SetCsvFileConnection strPath
    
    ConnectToCsvFileByAdo objCsvADOConnectStrGenerator.GetConnectionString

    ' Since no CSV file exist, the SQL will be failed.
End Sub

'''
''' Confirm that an error occurs
'''
Private Sub msubSanityTestToTryToConnectCSVDirectoryWhichDoesNotExist()

    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
    Dim objCsvADOConnectStrGenerator As ADOConStrOfAceOleDbCsv
    
    blnCSVGenerated = False
    
    strPath = mfstrGetTempCSVDirectoryPath("TemporaryCSF02", False)
    
    Set objCsvADOConnectStrGenerator = New ADOConStrOfAceOleDbCsv
    
    objCsvADOConnectStrGenerator.SetCsvDirectoryConnection strPath
    
    ConnectToCsvFileByAdo objCsvADOConnectStrGenerator.GetConnectionString
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>venmUnitTestPreparedSampleDataTable: Input</Argument>
''' <Argument>vintCountOfRows: Input</Argument>
''' <Argument>vblnIncludesSQLLogToSheet: Input</Argument>
Public Function GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
        Optional ByVal vintCountOfRows As Long = 100, _
        Optional ByVal vblnIncludesSQLLogToSheet As Boolean = True) As String


    Dim strInputCSVPath As String, strSQL As String, strLog As String
    
    Dim strOutputBookPath As String
    
    strInputCSVPath = GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(venmUnitTestPreparedSampleDataTable, vintCountOfRows)

    With New CsvAdoSheetExpander
    
        .SetInputCsvFileConnection strInputCSVPath
    
        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
    
        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strInputCSVPath)
        
        .OutputInExistedBookByUnitTestMode strSQL, "Load_sample_csv", vblnIncludesSQLLogToSheet
        
        
        strLog = GetLogTextOfPreparedSampleDataTableBaseName(venmUnitTestPreparedSampleDataTable)
        
        
        AddAutoShapeGeneralMessage .CurrentSheet, strLog
    
        strOutputBookPath = .CurrentBook.FullName
    
        .CloseAll
    End With
    
    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate = strOutputBookPath
End Function


'''
''' CSV directory connection test
'''
Public Sub ConnectToCsvFileByAdo(ByVal vstrConnectionString As String)

    Dim objCon As ADODB.Connection

    Set objCon = New ADODB.Connection
    
    With objCon
    
        .ConnectionString = vstrConnectionString
    
        On Error GoTo ErrHandler
    
        .Open   ' If the directory path exists, the ADO connection is to be succeeded even though no CSV file exist.
        
        .Close
        
ErrHandler:
        If Err.Number <> 0 Then
        
            Debug.Print "Failed to connect directory of CSV files: " & vstrConnectionString
            
            MsgBox "Failed to connect directory of CSV files: " & vbNewLine & vstrConnectionString, vbCritical Or vbOKOnly
        Else
            Debug.Print "Connected to the directory of CSV files"
        End If
        
        On Error GoTo 0
    End With
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** test CSV file generation
'**---------------------------------------------
'''
'''
'''
Private Sub msubGenerateTestingCSVFile()

    Dim strPath As String
    
    strPath = mfstrGetSimpleTestCSVFilePath
    
    With New Scripting.FileSystemObject
    
        With .CreateTextFile(strPath, True)
        
            ' field title
            .WriteLine "co1,col2,col3"
        
            ' data
            .WriteLine "abc,123," & Format(DateAdd("d", -1, Now()), "yyyy/m/d") '  2021/6/28"
            
            .WriteLine "def,456," & Format(DateAdd("d", -2, Now()), "yyyy/m/d") '  2021/6/29"
            
            .WriteLine "ghi,789," & Format(DateAdd("d", -3, Now()), "yyyy/m/d") '  2021/6/30"
            
            .Close
        End With
    End With
End Sub

'''
''' delete a generated sample CSV file
'''
Private Sub msubDeleteTestingCSVFile()

    Dim strPath As String
    
    strPath = mfstrGetSimpleTestCSVFilePath
    
    With New Scripting.FileSystemObject

        If .FileExists(strPath) Then
        
            .DeleteFile strPath, True
        End If
    End With
End Sub

'''
'''
'''
Private Function mfstrGetSimpleTestCSVFilePath()
    
    Dim strDir As String, strFileName As String, strPath As String
    
    
    strDir = mfstrGetTempCSVDirectoryPath
    
    strFileName = mstrTestCSVFileNamePrefix & ".csv"
    
    strPath = strDir & "\" & strFileName

    mfstrGetSimpleTestCSVFilePath = strPath
End Function


'''
'''
'''
Private Function mfstrGetTempCSVDirectoryPath(Optional ByVal vstrChildDirectoryName As String = "TemporaryCSVFiles", Optional ByVal vblnAllowToCreateDirectory As Boolean = True) As String
    Dim strDir As String
    
    With New Scripting.FileSystemObject
    
        strDir = GetCurrentOfficeFileObject().Path & "\" & vstrChildDirectoryName
    
        If vblnAllowToCreateDirectory Then
        
            ForceToCreateDirectory strDir
        End If
    End With

    mfstrGetTempCSVDirectoryPath = strDir
End Function






