Attribute VB_Name = "SqlUTfXlSheetExpanderByPassword"
'
'   Sanity test to connect Excel sheet with a locked password for opening
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Excel and ADO
'       Dependent on UTfDataTableSecurity.bas, UTfDecorationSetterToXlSheet.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 29/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrSheetName As String = "TestSheet"


'**---------------------------------------------
'** XlAdoConnector tests for password locked books
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToXlAdoConnectorForPasswordLockedBook()

    Dim strBookPath As String, strSQL As String

    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"

    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
    
    With New XlAdoConnector
    
        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
    
        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
    
        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub

'''
'''
'''
Private Sub msubSanityTestToXlAdoExpanderForPasswordLockedBook()

    Dim strBookPath As String, strSQL As String

    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"

    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
    
    With New XlAdoSheetExpander
    
        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
    
        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
    
        .OutputInExistedBookByUnitTestMode strSQL, "ATest", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
        
        DeleteDummySheetWhenItExists .CurrentBook
        
        .CloseAll
    End With
End Sub


'**---------------------------------------------
'** ADO connection tests
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToAdoConnectionToNoPasswordBook()

    Dim strBookPath As String, objBook As Excel.Workbook
    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
    
    
    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SampleBookWithNoLocked.xlsx"

    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, ""
    
    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
    
    With objADOConStrOfAceOledbXl
    
        .SetExcelBookConnection strBookPath, NoUseIMEXMode
    End With

    Set objConnection = New ADODB.Connection
    
    With objConnection
        
        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString

        On Error Resume Next

        .Open

        If Err.Number = 0 Then
        
            Debug.Print "Expected result to be connected"
            
            .Close
        End If

        On Error GoTo 0
    End With

End Sub

'''
'''
'''
Private Sub msubSanityTestToAdoConnectionToPasswordLockedBook()

    Dim strBookPath As String, objBook As Excel.Workbook
    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
    
    
    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"

    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
    
    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
    
    With objADOConStrOfAceOledbXl
    
        .SetExcelBookConnection strBookPath, NoUseIMEXMode
    End With

    Set objConnection = New ADODB.Connection
    
    With objConnection
        
        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString

        On Error Resume Next

        .Open

        If Err.Number <> 0 Then
        
            Debug.Print "Expected result to be failed connecting"
        Else
            .Close
        End If

        On Error GoTo 0
    End With
End Sub


'''
'''
'''
Private Sub msubSanityTestToAdoConnectionToPasswordLockedBookWithOpening()

    Dim strBookPath As String, objBook As Excel.Workbook
    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
    
    
    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"

    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
    
    Set objBook = GetWorkbook(strBookPath, False, vstrPassword:="1234")
    
    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
    
    With objADOConStrOfAceOledbXl
    
        .SetExcelBookConnection strBookPath, NoUseIMEXMode
    End With

    Set objConnection = New ADODB.Connection
    
    With objConnection
        
        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString

        On Error Resume Next

        .Open
        
        If Err.Number = 0 Then
        
            Debug.Print "Expected result to be connected, when the password locked Book is opened in this Excel"
            
            .Close
        End If

        On Error GoTo 0
    End With
End Sub

