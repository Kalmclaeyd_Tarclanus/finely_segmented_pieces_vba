Attribute VB_Name = "UTfLoadAndModifyModule"
'
'   Sanity tests for LoadAndModifyModule
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** IsFileOpened unit test
'**---------------------------------------------
Private Sub msubSanityTestOfIsFileOpened()

    Debug.Print "It should have been true: " & IsFileOpened(GetCurrentOfficeFileObject().FullName)

End Sub

Private Sub msubSanityTest02OfIsFileOpened()

    Debug.Print "It should have been false: " & IsFileOpened(GetCurrentOfficeFileObject().FullName & "a")

End Sub

Private Sub msubSanityTest03OfIsFileOpened()

    Dim strDir As String
    
    With New Scripting.FileSystemObject
    
        strDir = .GetParentFolderName(GetCurrentOfficeFileObject().FullName)
    End With
    
    Debug.Print "It should have been false: " & IsFileOpened(strDir)
End Sub
