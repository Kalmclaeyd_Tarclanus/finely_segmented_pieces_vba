Attribute VB_Name = "SqlUTfXlSheetExpander"
'
'   Sanity test to output SQL result into an output Excel sheet for a source Excel file sheets in a directory using ADO OLE DB
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
'       which should have been the installed when the Microsoft Office Excel software has been installed.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 20/Jan/2023    Kalmclaeyd Tarclanus    Added the multi-language caption function and tested
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrOutputBaseName As String = "SQLResultsOfUTfXlAdoSheetExpander"


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** XlAdoSheetExpander
'**---------------------------------------------
'''
''' Create an in-line table from the only SELECT sql inside
'''
Private Sub msubSanityTestToConnectByVirtualTableXlConnector()
    
    Dim strSQL As String

    With New XlAdoSheetExpander

        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()

        .AllowToRecordSQLLog = True

        'strSQL = "SELECT 'ABC', 'DEF' UNION SELECT 'GHI', 'JKL'" ' Error occurs

        'strSQL = "SELECT 1"

        'strSQL = "SELECT 'ABC', 'DEF'"

        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"

        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
        
        
        .CloseCacheOutputBookOfOpenByPresetModeProcess
    End With
End Sub

'''
''' Create an in-line table from the only SELECT sql inside
'''
Private Sub msubSanityTestToConnectForExpandingOperations()
    
    Dim objSheet As Excel.Worksheet, strOutputBookPath As String, objBook As Excel.Workbook
    Dim strSQL As String

    ForceToCreateDirectory GetTemporaryDevelopmentVBARootDir()
    
    strOutputBookPath = GetTemporaryDevelopmentVBARootDir() & "\SanityTestToOutputOperationsOfAdoRecordsetSheetExpander.xlsx"

    Set objBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)

    With New XlAdoSheetExpander

        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()

        .AllowToRecordSQLLog = True



        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"


        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
        
        .OutputInAlreadyExistedBook strSQL, "VirtualTable02"
        
        Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
        
        objSheet.Name = "VirtualTable03"
        
        .OutputToSheetFrom strSQL, objSheet
        
        .OutputInExistedBook strSQL, "VirtualTable04"
        
        .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath, "VirtualTable05"
        
        .OutputToSpecifiedBook strSQL, strOutputBookPath, "VirtualTable06"
        
        
        .CloseCacheOutputBookOfOpenByPresetModeProcess
        
        DeleteDummySheetWhenItExists .CurrentSheet
    End With
End Sub

'''
''' SQL logs will be appended on a specified sheet.
'''
Private Sub msubSanityTestToConnectByVirtualTableXlConnectorForSQLLogAppending()
    
    Dim strSQL As String

    With New XlAdoSheetExpander

        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()

        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"

        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
        
        strSQL = "SELECT 'EFG' AS C_Column, 'HIJ' AS D_Column"
        
        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable02", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
        
        strSQL = "SELECT 'KLM' AS E_Column, 'OPQ' AS F_Column"
        
        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable03", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
        
        
        DeleteDummySheetWhenItExists .CurrentBook
        
        .CloseCacheOutputBookOfOpenByPresetModeProcess
    End With
End Sub

'''
''' Confirm the auto shape log of SQL select
'''
Private Sub msubSanityTestToConnectByVirtualTableXlConnectorWithAutoShapeLog()
    
    Dim strSQL As String

    With New XlAdoSheetExpander

        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()

        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"

        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
        
        .CloseCacheOutputBookOfOpenByPresetModeProcess
    End With
End Sub

'''
''' show UErrorADOSQLForm tests
'''
Private Sub msubSanityTestToShowErrorTrapFormByIllegalSQL()

    Dim strSQL As String, objRSet As ADODB.Recordset

    With New XlAdoSheetExpander

        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()

        strSQL = "(SELECT 'ABC', 'DEF') UNION ALL (SELECT 'GHI', 'JKL')" ' This causes a error. It seems that UNION ALL is not applicable....

        'strSQL = "(SELECT 'ABC' AS A_Column, 'DEF' AS B_Column) UNION ALL (SELECT 'GHI', 'JKL')" ' Error occurs

        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
    End With
End Sub



'''
'''
'''
Private Sub msubSanityTest01ToConnectExcelBookTestAfterCreateBook()

    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook BasicSampleDT, 100, False
End Sub

'''
'''
'''
Private Sub msubSanityTest02ToConnectExcelBookTestAfterCreateBook()

    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook IncludeFictionalProductionNumberSampleDT, 1000, True
End Sub


'''
'''
'''
Private Sub msubSanityTestToSQLJoinWithConnectingExcelBookAfterItIsCreated()

    Dim strInputBookPath As String, strSheetName01 As String, strSheetName02 As String, strSQL As String
    
    strInputBookPath = GetOutputBookPathAfterCreateALeastTestSheetSet()

    With New XlAdoSheetExpander
    
        .SetInputExcelBookPath strInputBookPath
    
        strSheetName01 = "[" & GetSheetNameOfPreparedSampleDataTableExcelBookForTest(IncludeFictionalProductionNumberSampleDT) & "$]"
        
        strSheetName02 = "[" & GetSheetNameOfPreparedSampleDataTableExcelBookForTest(FishTypeSampleMasterDT) & "$]"
    
        strSQL = "SELECT * FROM " & strSheetName01
    
        .OutputInExistedBookByUnitTestMode strSQL, "CreatedTable01"
        
        strSQL = "SELECT * FROM " & strSheetName02
        
        .OutputInExistedBookByUnitTestMode strSQL, "SampleMasterTable02"
    
        strSQL = "SELECT A.RowNumber, A.RealNumber, B.FishTypeJpn, B.RandAlpha FROM " & strSheetName01 & " AS A LEFT JOIN " & strSheetName02 & " AS B ON A.FishType = B.FishTypeEng"
    
        .OutputInExistedBookByUnitTestMode strSQL, "SQLResult"
    End With

End Sub



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' This is also referenced from UTfReadFromShapeOnXlSheet.bas
'''
Public Function GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
        Optional ByVal vintCountOfRows As Long = 100, Optional _
        ByVal vblnIncludesSQLLogToSheet As Boolean = True) As String

    Dim strInputBookPath As String, strSQL As String, strLog As String, strSheetName As String
    
    Dim strOutputBookPath As String
    
    strInputBookPath = GetExcelBookPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(strSheetName, venmUnitTestPreparedSampleDataTable, vintCountOfRows)

    With New XlAdoSheetExpander
    
        .SetInputExcelBookPath strInputBookPath
    
        With .DataTableSheetFormattingInterface
    
            Set .FieldTitleOrderToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Col1,12,Col2,15")
        
            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FlowerTypeSample,19,RandomInteger,16")
            
            .RecordBordersType = RecordCellsGrayAll
        End With
    
        strSQL = "SELECT * FROM [" & strSheetName & "$]"
        
        .OutputInExistedBookByUnitTestMode strSQL, "Load_" & strSheetName, vblnIncludesSQLLogToSheet
        
        
        strLog = GetLogTextOfPreparedSampleDataTableBaseName(venmUnitTestPreparedSampleDataTable)
        
        
        AddAutoShapeGeneralMessage .CurrentSheet, strLog
        
        strOutputBookPath = .CurrentBook.FullName
        
        DeleteDummySheetWhenItExists .CurrentBook
    End With

    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook = strOutputBookPath
End Function



'''
''' XlAdoSheetExpander unit test
'''
Public Sub SanityTestAboutXlSheetExpander()
    
    Dim strInputPath As String, strOutputPath As String, strSheetName As String
    
    Dim strSQL As String, strSQLTableName As String, enmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable
    
    
    enmUnitTestPreparedSampleDataTable = BasicSampleDT
    
    
    strInputPath = GetExcelBookPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(strSheetName, enmUnitTestPreparedSampleDataTable, 50)
    
    
    strOutputPath = GetCurrentBookOutputDir() & mstrOutputBaseName & ".xlsx"
    
    With New XlAdoSheetExpander
    
        .SetInputExcelBookPath strInputPath
    
        .AllowToRecordSQLLog = True
        
        Set .FieldTitleToColumnWidthDic = GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest(enmUnitTestPreparedSampleDataTable)
        
        
        strSQLTableName = "[" & strSheetName & "$]"
        
        strSQL = "SELECT * FROM " & strSQLTableName

        .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputPath, "QueryTest", True
    End With
End Sub





