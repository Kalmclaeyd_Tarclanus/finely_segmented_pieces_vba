Attribute VB_Name = "EnumerateChildWindowsToXlSheet"
'
'   Output hWnd list on Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and Excel
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 13/Jul/2024    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' For all Top-level HWnd information
'''
Public Sub OutputAllTopLevelHwndClassNameAndWindowTitleTextOnSheet()

    msubOutputTopLevelHwndClassNameAndWindowTitleTextOnSheet
End Sub

'''
''' About Excel and VBIDE
'''
Public Sub OutputAllTopLevelHwndClassNameAndWindowTitleTextOf_Excel_OnSheet()

    msubOutputTopLevelHwndClassNameAndWindowTitleTextOnSheet "XLMAIN|wndclass_desked_gsk", "", "", SortInfoByTopLevelHWndWindowText
End Sub

'''
''' About Word
'''
Public Sub OutputAllTopLevelHwndClassNameAndWindowTitleTextOf_Word_OnSheet()

    msubOutputTopLevelHwndClassNameAndWindowTitleTextOnSheet "OpusApp", "", "", SortInfoByTopLevelHWndWindowText
End Sub

'''
''' About Wordpad
'''
Public Sub OutputAllTopLevelHwndClassNameAndWindowTitleTextOf_Wordpad_OnSheet()

    msubOutputTopLevelHwndClassNameAndWindowTitleTextOnSheet "WordPadClass", "", "", SortInfoByTopLevelHWndWindowText
End Sub


'''
''' About Notepad
'''
Public Sub OutputAllTopLevelHwndClassNameAndWindowTitleTextOf_Notepad_OnSheet()

    msubOutputTopLevelHwndClassNameAndWindowTitleTextOnSheet "Notepad", "", "Notepad.exe", SortInfoByTopLevelHWndWindowText
End Sub

Public Sub OutputChildHwndAndSomeInfoOnSheetOf_Notepad_OnSheetWithoutHWndWhenNoWindowText()

    'msubOutputChildHwndAndSomeInfoOnSheet , "Chrome_WidgetWin_1", "", SortInfoByTopLevelHWndWindowText, True
    
    msubOutputChildHwndAndSomeInfoOnSheet , "Notepad", "", "", SortInfoByTopLevelHWndWindowText, False
End Sub


'''
''' About SakuraEditor
'''
Public Sub OutputAllTopLevelHwndClassNameAndWindowTitleTextOf_SakuraEditor_OnSheet()

    msubOutputTopLevelHwndClassNameAndWindowTitleTextOnSheet "TextEditorWindowW166", "", "sakura.exe", SortInfoByTopLevelHWndWindowText
End Sub

'''
''' About Visual Studio Code
'''
Public Sub OutputAllTopLevelHwndClassNameAndWindowTitleTextOf_VisualStudioCode_OnSheet()

    'msubOutputTopLevelHwndClassNameAndWindowTitleTextOnSheet "Chrome_WidgetWin_1", "Visual Studio Code", "", SortInfoByTopLevelHWndWindowText
    
    msubOutputTopLevelHwndClassNameAndWindowTitleTextOnSheet "Chrome_WidgetWin_1", "", "Code.exe", SortInfoByTopLevelHWndWindowText
End Sub

Public Sub OutputChildHwndAndSomeInfoOnSheetOf_VisualStudioCode_OnSheetWithoutHWndWhenNoWindowText()

    'msubOutputChildHwndAndSomeInfoOnSheet , "Chrome_WidgetWin_1", "", SortInfoByTopLevelHWndWindowText, True
    
    msubOutputChildHwndAndSomeInfoOnSheet , "Chrome_WidgetWin_1", "", "Code.exe", SortInfoByTopLevelHWndWindowText, False
End Sub
'''
'''
'''
Public Sub OutputChildHwndAndSomeInfoOnSheetOf_Excel_OnSheet()

    msubOutputChildHwndAndSomeInfoOnSheet , "XLMAIN|wndclass_desked_gsk", "", "", SortInfoByTopLevelHWndWindowText
End Sub
Public Sub OutputChildHwndAndSomeInfoOnSheetOf_Excel_OnSheetWithoutHWndWhenNoWindowText()

    msubOutputChildHwndAndSomeInfoOnSheet , "XLMAIN|wndclass_desked_gsk", "", "", SortInfoByTopLevelHWndWindowText, True
End Sub

'''
'''
'''
Public Sub msubOutputChildHwndAndSomeInfoOnSheet(Optional ByVal venmHWndInfoAddingOptionType As HWndInfoAddingOptionType = HWndInfoAddingOptionType.AddBothWindowTextAndClassNameInfoFromHWnd, _
        Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal venmTopLevelWindowHWndInfoSortType As TopLevelWindowHWndInfoSortType = TopLevelWindowHWndInfoSortType.SortInfoByTopLevelHWndClassName, _
        Optional ByVal vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively As Boolean = False)


    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet

    Dim objDTCol As Collection
    
    Dim objDTSheetFormatter As DataTableSheetFormatter


    Set objDTCol = GetChildHWndAndSomeInfoRecursively(venmHWndInfoAddingOptionType, vstrClassNameRegExpPattern, vstrWindowTitleRegExpPattern, vstrProcessFullPathRegExpPattern, venmTopLevelWindowHWndInfoSortType, vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively)

    strBookPath = mfstrGetTempWindowHWndInfoBooksDir() & "\" & "ChildHWndAndSomeInfo.xlsx"
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    objSheet.Name = "ChildHWndInfo"

    DeleteDummySheetWhenItExists objBook

    Set objDTSheetFormatter = mfobjGetDTSheetFormatterFromChildHwndAndSomeInfo(objDTCol, venmHWndInfoAddingOptionType)
    
    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, objDTCol, objDTSheetFormatter
End Sub

'''
'''
'''
Private Function mfobjGetDTSheetFormatterFromChildHwndAndSomeInfo(ByVal vobjDTCol As Collection, _
        ByVal venmHWndInfoAddingOptionType As HWndInfoAddingOptionType) As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter, varFieldTitle As Variant
    
    Set objDTSheetFormatter = New DataTableSheetFormatter

    With objDTSheetFormatter

        .FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo
    
        .RecordBordersType = RecordCellsGrayAll
    
        Set .FieldTitleToColumnWidthDic = mfobjGetFieldTitlesToColumnWidthDicFromChildHWndAndSomeInfoDTDicAndAddingOption(vobjDTCol, venmHWndInfoAddingOptionType)


        .AllowToMergeCellsByContinuousSameValues = True
        
        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        Set .FieldTitlesToMergeCells = New Collection

        For Each varFieldTitle In .FieldTitleToColumnWidthDic
        
            With .FieldTitlesToMergeCells
        
                .Add varFieldTitle
            End With
        Next
    End With

    Set mfobjGetDTSheetFormatterFromChildHwndAndSomeInfo = objDTSheetFormatter
End Function


'''
'''
'''
Private Function mfobjGetFieldTitlesToColumnWidthDicFromChildHWndAndSomeInfoDTDicAndAddingOption(ByVal vobjDTCol As Collection, _
        ByVal venmHWndInfoAddingOptionType As HWndInfoAddingOptionType) As Scripting.Dictionary


    Dim intColumnsCount As Long, objDic As Scripting.Dictionary
    
    Dim blnNeedToGetWindowText As Boolean, blnNeedToGetClassName As Boolean, intMaxHierarchicalOrder As Long, i As Long
    
    
    Set objDic = New Scripting.Dictionary
    
    blnNeedToGetWindowText = (venmHWndInfoAddingOptionType And AddWindowTextInfoFromHWnd) = HWndInfoAddingOptionType.AddWindowTextInfoFromHWnd
    
    blnNeedToGetClassName = (venmHWndInfoAddingOptionType And AddClassNameInfoFromHWnd) = HWndInfoAddingOptionType.AddClassNameInfoFromHWnd
    
    If vobjDTCol.Count = 0 Then
    
        With objDic
        
            .Add "HWnd", 11
        
            If blnNeedToGetWindowText Then .Add "WindowText", 20
        
            If blnNeedToGetClassName Then .Add "ClassName", 18
        End With
    Else
    
        intColumnsCount = vobjDTCol.Item(1).Count

        intMaxHierarchicalOrder = 1
        
        If blnNeedToGetWindowText Then intMaxHierarchicalOrder = intMaxHierarchicalOrder + 1

        If blnNeedToGetClassName Then intMaxHierarchicalOrder = intMaxHierarchicalOrder + 1

        intColumnsCount = intColumnsCount / intMaxHierarchicalOrder

        For i = 1 To intColumnsCount

            With objDic
            
                If i = 1 Then
            
                    .Add "HWnd", 9
                
                    If blnNeedToGetWindowText Then .Add "WindowText", 20
                
                    If blnNeedToGetClassName Then .Add "ClassName", 14
                Else
                    .Add "HWnd" & Format(i, "00"), 8
                
                    If blnNeedToGetWindowText Then .Add "WindowText" & Format(i, "00"), 13
                
                    If blnNeedToGetClassName Then .Add "ClassName" & Format(i, "00"), 11
                End If
            End With
        Next
    End If

    Set mfobjGetFieldTitlesToColumnWidthDicFromChildHWndAndSomeInfoDTDicAndAddingOption = objDic
End Function



'''
'''
'''
Public Sub msubOutputTopLevelHwndClassNameAndWindowTitleTextOnSheet(Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal venmTopLevelWindowHWndInfoSortType As TopLevelWindowHWndInfoSortType = TopLevelWindowHWndInfoSortType.SortInfoByTopLevelHWndClassName)

    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet

    Dim objDTCol As Collection, objFieldTitlesCol As Collection, objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTCol = GetTopHWndWindowTextAndClassNameAndItOfParentIfExistsDTCol(vstrClassNameRegExpPattern, vstrWindowTitleRegExpPattern, vstrProcessFullPathRegExpPattern, True, venmTopLevelWindowHWndInfoSortType)

    
    strBookPath = mfstrGetTempWindowHWndInfoBooksDir() & "\" & "TopTitleAndClassNameAndParentOfTopIfExists.xlsx"

    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    objSheet.Name = "TopLevelHWndInfo"

    DeleteDummySheetWhenItExists objBook
    
    'Set objFieldTitlesCol = GetColFromLineDelimitedChar("")
    
    Set objDTSheetFormatter = mfobjGetDataTableSheetFormatterOfTopHwndWindowTextAndClassNameAndItOfParentIfExists(True)
    
    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, objDTCol, objDTSheetFormatter
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrGetTempWindowHWndInfoBooksDir() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\WinOsHWndInfo"

    ForceToCreateDirectory strDir
    
    mfstrGetTempWindowHWndInfoBooksDir = strDir
End Function


'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterOfTopHwndWindowTextAndClassNameAndItOfParentIfExists( _
        Optional ByVal vblnAllowToAddHWndValueAndProcessIDAndPathToRowData As Boolean = False) As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo
    
        .RecordBordersType = RecordCellsGrayAll
        
        
        If vblnAllowToAddHWndValueAndProcessIDAndPathToRowData Then
        
            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("TopHwnd,10,ProcessID,10,TopWindowText,36,TopClassName,32,ProcessModulePath,77,ParentOfTopHwnd,12,ParentOfTopWindowText,22,ParentOfTopClassName,20")
            
            
            .AllowToMergeCellsByContinuousSameValues = True
            
            .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
            
            Set .FieldTitlesToMergeCells = GetColAsDateFromLineDelimitedChar("ProcessID,TopWindowText,TopClassName,ProcessModulePath,ParentOfTopWindowText,ParentOfTopClassName")
            
        Else
            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("TopWindowText,36,TopClassName,32,ParentOfTopWindowText,22,ParentOfTopClassName,20")
            
            .AllowToMergeCellsByContinuousSameValues = True
            
            .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
            
            Set .FieldTitlesToMergeCells = GetColAsDateFromLineDelimitedChar("TopWindowText,TopClassName,ParentOfTopWindowText,ParentOfTopClassName")
            
        End If
    End With

    Set mfobjGetDataTableSheetFormatterOfTopHwndWindowTextAndClassNameAndItOfParentIfExists = objDTSheetFormatter
End Function
