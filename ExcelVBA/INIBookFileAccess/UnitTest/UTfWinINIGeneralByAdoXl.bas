Attribute VB_Name = "UTfWinINIGeneralByAdoXl"
'
'   Sanity tests for editing Windows INI file by Windows API
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and Excel
'       Dependent on ADODB
'       Dependent on InterfaceCall.bas and WinAPIMessageWithTimeOut
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun, 18/Aug/2024    Kalmclaeyd Tarclanus    Refactoring
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetSectionToKeyValueDicWithoutOpenBookFromSheet01()

    Dim objSectionToKeyValueDic As Scripting.Dictionary, strInputBookPath As String, strIniBaseName As String
    
    Dim strBookBaseName As String
    
    
    strBookBaseName = "PermittedCharactersINIAll"
    
    strInputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\" & strBookBaseName & ".xlsx"
    
    With New Scripting.FileSystemObject
    
        If Not .FileExists(strInputBookPath) Then
        
            GetAllIniBookPathAfterCreateSpecialTestingCharacters strBookBaseName, "CharacterLimitationINITest"
        End If
    End With

    ' using DataTableSheetIn.bas
    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(strInputBookPath)

    DebugDic objSectionToKeyValueDic
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetSectionToKeyValueDicWithoutOpenBookFromSheet02()

    Dim objSectionToKeyValueDic As Scripting.Dictionary, strInputBookPath As String, strIniBaseName As String
    Dim strBookBaseName As String
    
    strBookBaseName = "OrdinalCharactersINIAll"
    
    strInputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\" & strBookBaseName & ".xlsx"
    
    With New Scripting.FileSystemObject
    
        If Not .FileExists(strInputBookPath) Then
        
            GetAllIniBookPathAfterCreateOrdinalSampleCharacters strBookBaseName, "OrdinalCharactersINITest"
        End If
    End With
    
    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(strInputBookPath)

    DebugDic objSectionToKeyValueDic
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetSectionToKeyValueDicFromSheet01()

    Dim objSectionToKeyValueDic As Scripting.Dictionary, strInputBookPath As String, strIniBaseName As String
    Dim strBookBaseName As String
    
    strBookBaseName = "PermittedCharactersINIAll"
    
    strInputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\" & strBookBaseName & ".xlsx"
    
    With New Scripting.FileSystemObject
    
        If Not .FileExists(strInputBookPath) Then
        
            GetAllIniBookPathAfterCreateSpecialTestingCharacters strBookBaseName, "CharacterLimitationINITest"
        End If
    End With
    
    ' using ADO
    'Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQL(strInputBookPath)

    ' using DataTableSheetIn.bas
    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBook(strInputBookPath)

    DebugDic objSectionToKeyValueDic
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetSectionToKeyValueDicFromSheet02()

    Dim objSectionToKeyValueDic As Scripting.Dictionary, strInputBookPath As String, strIniBaseName As String
    Dim strBookBaseName As String
    
    strBookBaseName = "OrdinalCharactersINIAll"
    
    strInputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\" & strBookBaseName & ".xlsx"
    
    With New Scripting.FileSystemObject
    
        If Not .FileExists(strInputBookPath) Then
        
            GetAllIniBookPathAfterCreateOrdinalSampleCharacters strBookBaseName, "OrdinalCharactersINITest"
        End If
    End With
    
    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQL(strInputBookPath)
    
    'Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBook(strInputBookPath)

    DebugDic objSectionToKeyValueDic
End Sub

