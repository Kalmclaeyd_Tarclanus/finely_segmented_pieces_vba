Attribute VB_Name = "WinINIGeneralByAdoXl"
'
'   With ADODB connection, output read INI file results to Excel worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and Excel
'       Dependent on ADODB
'       Dependent on WinINIGeneralForXl.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun, 18/Aug/2024    Kalmclaeyd Tarclanus    Separated from WinINIGeneralForXl.bas
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About Excel three columns sheet compatible INI file data structure
'**---------------------------------------------
'''
''' load test
'''
Public Function OutputIniCompatibleExcelSheetAllKeyValueToBook(ByVal vstrInputINICompatibleBookPath As String, _
        ByVal vstrInputINICompatibleSheetName As String, _
        ByVal vstrOutputBookPath As String, _
        Optional ByVal vstrOutputSheetName As String = "INI_All", _
        Optional ByVal vstrFieldTitleToColumnWidthDelimitedByComma As String = "Section,15,Key,15,Value,15") As Excel.Worksheet


    Dim objSectionToKeyValueDic As Scripting.Dictionary, objSheet As Excel.Worksheet
    
    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(vstrInputINICompatibleBookPath, vstrInputINICompatibleSheetName)

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, vstrOutputSheetName)

    OutputOneNestedDicToSheetRestrictedByDataTableSheetFormatter objSheet, _
            objSectionToKeyValueDic, _
            GetDataTableSheetFormatterForIniSectionKeyValueDic(vstrFieldTitleToColumnWidthDelimitedByComma, True)
    
    AddAutoShapeGeneralMessage objSheet, GetAutoShapeTextLogOfIniAllKeyValue(objSheet, vstrInputINICompatibleBookPath, True)
    
    Set OutputIniCompatibleExcelSheetAllKeyValueToBook = objSheet
End Function


'**---------------------------------------------
'** About INI file
'**---------------------------------------------
'''
''' An improvement point - this doesn't need to open Excel book before get a nested Dictionary object
'''
''' <Argument>vstrInputBookPath: Input</Argument>
''' <Argument>vstrInputSheetName: Input</Argument>
''' <Argument>vblnAllowToCloseBookAfterDetectData: Input</Argument>
''' <Return>Dictionary(Of String[Section], Dictionary(Of String[Key], Variant[Variant]))</Return>
Public Function GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(ByVal vstrInputBookPath As String, _
        Optional ByVal vstrInputSheetName As String = "INI_All", _
        Optional ByVal vblnAllowToCloseBookAfterDetectData As Boolean = True) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, strSQL As String
    Dim objSectionToKeyValueDic As Scripting.Dictionary, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    ' Probably, Avoiding ADO causes faster getting results
    
    With New XlAdoConnector
    
        .SetInputExcelBookPath vstrInputBookPath
    
        ' Key is a SQL reserved word, then ADO (SQL92) needs brackets, such as '[Key]'
    
        strSQL = "SELECT [Section], [Key], [Value] FROM [" & vstrInputSheetName & "$] WHERE [Key] IS NOT NULL AND [Value] IS NOT NULL"
    
        Set objDic = GetTableOneNestedDictionaryFromRSetWithSpecialConversion(.GetRecordset(strSQL))
    End With
    
    Set GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion = objDic
End Function


'''
''' A defect point - this needs to open Excel book before get a nested Dictionary object
'''
''' <Argument>vstrInputBookPath: Input</Argument>
''' <Argument>vstrInputSheetName: Input</Argument>
''' <Argument>vblnAllowToCloseBookAfterDetectData: Input</Argument>
''' <Return>Dictionary(Of String[Section], Dictionary(Of String[Key], Variant[Variant]))</Return>
Public Function GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQL(ByVal vstrInputBookPath As String, _
        Optional ByVal vstrInputSheetName As String = "INI_All", _
        Optional ByVal vblnAllowToCloseBookAfterDetectData As Boolean = True) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, strSQL As String
    Dim objSectionToKeyValueDic As Scripting.Dictionary, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    
    Set objBook = GetWorkbook(vstrInputBookPath, False)

    Set objSheet = objBook.Worksheets.Item(vstrInputSheetName)

    If IsAtLeastOneMergedCellIncluded(objSheet, GetColFromLineDelimitedChar("Section")) Then

        RemoveMergedCellsToTable objSheet, GetColFromLineDelimitedChar("Section")

        ForceToSaveBookWithoutDisplayAlert objBook
    End If

    If vblnAllowToCloseBookAfterDetectData Then
    
        ' Need to save for the next SQL query
    
        If Not objBook.Saved Then
        
            ForceToSaveBookWithoutDisplayAlert objBook
        End If
        
        CloseBookWithoutDisplayAlerts objBook
    End If
    
    ' Probably, Avoiding ADO causes faster getting results
    
    With New XlAdoConnector
    
        .SetInputExcelBookPath vstrInputBookPath
    
    
        ' Key is a SQL reserved word, then ADO (SQL92) needs brackes, such as '[Key]'
    
    
        strSQL = "SELECT [Section], [Key], [Value] FROM [" & vstrInputSheetName & "$] WHERE [Section] IS NOT NULL AND [Key] IS NOT NULL AND [Value] IS NOT NULL"
        
        
        'strSQL = "SELECT [Section], [Key], [Value] FROM [" & vstrInputSheetName & "$]"
        
        'strSQL = "SELECT * FROM [" & vstrInputSheetName & "$]"
        
    
        Set objDic = GetTableOneNestedDictionaryFromRSet(.GetRecordset(strSQL))
    End With
    
    Set GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQL = objDic
End Function

