Attribute VB_Name = "SQLGeneralFromXlSheet"
'
'   generate SQL part with dependent on Excel
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Indpendent on ADO, this expects the PgSQL script creation.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  7/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetSQLWhereFromNotNullConditionFromWorkbookPathAndSheetName(ByVal vstrBookPath As String, _
        ByVal vstrSheetName As String, _
        ByVal vintFieldTitlesRowIndex As Long, _
        ByVal vintTopLeftColumnIndex As Long, _
        Optional ByVal vobjColumnsExceptionIndexList As Scripting.Dictionary = Nothing, _
        Optional ByVal vobjColumnsFieldTitleNameExceptionList As Scripting.Dictionary = Nothing) As String

    
    Dim objFieldTitlesCol As Collection, varFieldTitle As Variant, strFieldTitle As String, intColumnsCount As Long
    Dim strSQLPart As String, i As Long
    
    
    Set objFieldTitlesCol = GetFieldTitlesColFromWorkbookPathAndSheetName(vstrBookPath, vstrSheetName, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjColumnsExceptionIndexList, vobjColumnsFieldTitleNameExceptionList)

    intColumnsCount = objFieldTitlesCol.Count
    
    strSQLPart = ""
    
    i = 1

    For Each varFieldTitle In objFieldTitlesCol
    
        strFieldTitle = varFieldTitle
    
        If intColumnsCount = 1 Then
        
            strSQLPart = strSQLPart & strFieldTitle & " IS NOT NULL"
        Else
        
            strSQLPart = strSQLPart & strFieldTitle & " IS NULL"
        End If
    
        If i < objFieldTitlesCol.Count Then
        
            strSQLPart = strSQLPart & " AND "
        End If
        
        i = i + 1
    Next

    If intColumnsCount > 1 Then
    
        strSQLPart = "NOT (" & strSQLPart & ")"
    End If

    GetSQLWhereFromNotNullConditionFromWorkbookPathAndSheetName = strSQLPart
End Function

