Attribute VB_Name = "InterfaceCallForXl"
'
'   Interface call for implementing polymorphism (Call-back function)
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel, which also uses ThisWorkbook object name
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 26/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const THIS_VBE_IS_FROM_EXCEL = True


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Excel dependent call back solutions with using ThisWorkbook keyword
'**---------------------------------------------
'''
''' call back for Scripting.Dictionary
'''
Public Function GetDictionaryFromCallBackInterfaceForXl(ByVal vstrOperationInterface As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary

    Set objDic = Nothing
    
    On Error Resume Next

#If THIS_VBE_IS_FROM_EXCEL Then

    Set objDic = ThisWorkbook.Application.Run(vstrOperationInterface)
#Else
    Set objDic = GetCurrentOfficeFileObject().Application.Run(vstrOperationInterface)
#End If

    On Error GoTo 0

    Set GetDictionaryFromCallBackInterfaceForXl = objDic
End Function

'''
''' call back for Collection
'''
Public Function GetCollectionFromCallBackInterfaceForXl(ByVal vstrOperationInterface As String) As Collection

    Dim objCol As Collection

    Set objCol = Nothing
    
    On Error Resume Next

#If THIS_VBE_IS_FROM_EXCEL Then

    Set objCol = ThisWorkbook.Application.Run(vstrOperationInterface)
#Else
    Set objCol = GetCurrentOfficeFileObject().Application.Run(vstrOperationInterface)
#End If
    On Error GoTo 0

    Set GetCollectionFromCallBackInterfaceForXl = objCol
End Function

'''
''' call back for String with only one argument
'''
Public Function GetStringFromCallBackInterfaceAndOneStringArgumentForXl(ByVal vstrOperationInterface As String, ByRef rstrArgument As String) As String

    Dim strValue As String
    
    strValue = ""
    
    On Error Resume Next

#If THIS_VBE_IS_FROM_EXCEL Then

    strValue = ThisWorkbook.Application.Run(vstrOperationInterface, rstrArgument)
#Else
    strValue = GetCurrentOfficeFileObject().Application.Run(vstrOperationInterface, rstrArgument)
#End If
    On Error GoTo 0

    GetStringFromCallBackInterfaceAndOneStringArgumentForXl = strValue
End Function


'''
''' call back for String
'''
Public Function GetStringFromCallBackInterfaceForXl(ByVal vstrOperationInterface As String) As String

    Dim strValue As String
    
    strValue = ""
    
    On Error Resume Next

#If THIS_VBE_IS_FROM_EXCEL Then

    strValue = ThisWorkbook.Application.Run(vstrOperationInterface)
#Else
    strValue = GetCurrentOfficeFileObject().Application.Run(vstrOperationInterface)
#End If

    On Error GoTo 0

    GetStringFromCallBackInterfaceForXl = strValue
End Function

