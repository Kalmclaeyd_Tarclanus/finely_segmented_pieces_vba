Attribute VB_Name = "UTfCreateDataTableForXl"
'
'   output created fictional data-tables into Excel.Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  8/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Create plural data-table sheets on a book
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCreateAllTypeTestSheetSet()

    Dim objDic As Scripting.Dictionary, strBookPath As String
    
    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add UnitTestPreparedSampleDataTable.BasicSampleDT, 50
        
        .Add UnitTestPreparedSampleDataTable.IncludeFictionalProductionNumberSampleDT, 1000
        
        .Add UnitTestPreparedSampleDataTable.FishTypeSampleMasterDT, 1
    
        .Add UnitTestPreparedSampleDataTable.FlowerTypeSampleMasterDT, 1
    End With
    
    strBookPath = GetBookPathAfterCreatePluralSampleDataTablesToSheet(objDic, "GeneratedAllTypeSampleDataTables")
End Sub


'''
'''
'''
Private Sub msubOpenSampleDataTableWorkbookForSanityTest()

    OpenSampleDataTableWorkbookForSanityTest IncludeFictionalProductionNumberSampleDT
End Sub


Private Sub msubSanityTestOfCreateSampleDataTable01ToSheet()

    Dim varDataTable As Variant, objFieldTitles As Collection, objDoubleStopWatch As DoubleStopWatch

    Set objDoubleStopWatch = New DoubleStopWatch
    
    objDoubleStopWatch.MeasureStart

    CreateTestingSimpleDataTable01 varDataTable, objFieldTitles, 100

    objDoubleStopWatch.MeasureInterval
    
    OutputCreatedSampleDataTableToSheet BasicSampleDT, varDataTable, objFieldTitles, mfobjGetDataTableFormatterForSample01(), objDoubleStopWatch.ElapsedSecondTime
End Sub


Private Sub msubSanityTestOfCreateSampleDataTable02ToSheet()

    Dim varDataTable As Variant, objFieldTitles As Collection, objDoubleStopWatch As DoubleStopWatch

    Set objDoubleStopWatch = New DoubleStopWatch
    
    objDoubleStopWatch.MeasureStart

    CreateTestingSimpleDataTable02 varDataTable, objFieldTitles, 1000

    objDoubleStopWatch.MeasureInterval
    
    OutputCreatedSampleDataTableToSheet IncludeFictionalProductionNumberSampleDT, varDataTable, objFieldTitles, mfobjGetDataTableFormatterForSample02(), objDoubleStopWatch.ElapsedSecondTime
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>rstrSheetName: Output</Argument>
''' <Argument>venmUnitTestPreparedSampleDataTable: Input</Argument>
''' <Argument>vintCountOfRows</Argument>
''' <Return>String</Return>
Public Function GetExcelBookPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(ByRef rstrSheetName As String, _
        ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
        Optional ByVal vintCountOfRows As Long = 100) As String

    Dim strBookPath As String
    
    strBookPath = GetPreparedSampleDataTableExcelBookPathForTest(venmUnitTestPreparedSampleDataTable)
    
    rstrSheetName = GetSheetNameOfPreparedSampleDataTableExcelBookForTest(venmUnitTestPreparedSampleDataTable)
    
    With New Scripting.FileSystemObject
    
        If Not .FileExists(strBookPath) Then
        
            ' create
        
            GetBookPathAfterCreateSampleDataTableSheet venmUnitTestPreparedSampleDataTable, vintCountOfRows, True
        End If
    End With

    GetExcelBookPathAfterCheckFileExistenceOrCreateWhenItDoesntExist = strBookPath
End Function


'''
'''
'''
''' <Argument>venmUnitTestPreparedSampleDataTable: Input</Argument>
''' <Argument>vintCountOfRows: Input</Argument>
''' <Argument>vblnAllowToCloseWorkbookAfterFinishedCreating: Input</Argument>
''' <Return>String: Workbook path</Return>
Private Function GetBookPathAfterCreateSampleDataTableSheet(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
        Optional ByVal vintCountOfRows As Long = 100, _
        Optional ByVal vblnAllowToCloseWorkbookAfterFinishedCreating As Boolean = False) As String


    Dim varDataTable As Variant, objFieldTitles As Collection, objDoubleStopWatch As DoubleStopWatch
    
    Dim strBookPath As String, objSheet As Excel.Worksheet, objBook As Excel.Workbook

    Set objDoubleStopWatch = New DoubleStopWatch
    
    With objDoubleStopWatch
    
        .MeasureStart

        CreateSimpleRandomDataTable varDataTable, objFieldTitles, vintCountOfRows, venmUnitTestPreparedSampleDataTable
    
        .MeasureInterval
    End With
    
    
    strBookPath = GetPreparedSampleDataTableExcelBookPathForTest(venmUnitTestPreparedSampleDataTable)
    
    Set objSheet = mfobjOutputExcelBookFromSampleDataTable(varDataTable, objFieldTitles, venmUnitTestPreparedSampleDataTable)
    
    If vblnAllowToCloseWorkbookAfterFinishedCreating Then
    
        Set objBook = objSheet.Parent
        
        ForceToSaveBookWithoutDisplayAlert objBook
        
        objBook.Close
    End If
    
    GetBookPathAfterCreateSampleDataTableSheet = strBookPath
End Function


'''
'''
'''
Private Function GetBookPathAfterCreatePluralSampleDataTablesToSheet(ByVal vobjUnitTestPreparedSampleDataTableToParamsDic As Scripting.Dictionary, _
        Optional ByVal vstrBookBaseName As String = "GeneratedSampleDataTablesForTests", _
        Optional ByVal vblnAllowToCloseWorkbookAfterFinishedCreating As Boolean = False) As String


    Dim varUnitTestPreparedSampleDataTable As Variant, enmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable
    Dim intCountOfRows As Long
    Dim varDataTable As Variant, objFieldTitles As Collection, objDoubleStopWatch As DoubleStopWatch
    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet, objCurrentSheet As Excel.Worksheet
    

    strBookPath = GetPreparedPluralSampleDataTablesExcelBookPath(vstrBookBaseName)
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    Set objDoubleStopWatch = New DoubleStopWatch
    
    With objDoubleStopWatch
    
        .MeasureStart
        
        With vobjUnitTestPreparedSampleDataTableToParamsDic
        
            For Each varUnitTestPreparedSampleDataTable In .Keys
            
                enmUnitTestPreparedSampleDataTable = varUnitTestPreparedSampleDataTable
            
                intCountOfRows = .Item(varUnitTestPreparedSampleDataTable)
            
                CreateSimpleRandomDataTable varDataTable, objFieldTitles, intCountOfRows, enmUnitTestPreparedSampleDataTable
            
                Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
            
                objSheet.Name = GetSheetNameOfPreparedSampleDataTableExcelBookForTest(enmUnitTestPreparedSampleDataTable)
            
                OutputVarTableToSheet objSheet, varDataTable, objFieldTitles, mfobjGetDataTableSheetFormatterOfPreparedSampleType(enmUnitTestPreparedSampleDataTable)
                
                AddAutoShapeGeneralMessage objSheet, GetLogTextOfPreparedSampleDataTableBaseName(enmUnitTestPreparedSampleDataTable)
                
                Set objCurrentSheet = objSheet
            Next
        
        End With
    
        .MeasureInterval
    End With
    
    AddAutoShapeGeneralMessage objCurrentSheet, "All processes elapsed time: " & objDoubleStopWatch.ElapsedTimeByString, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillRedCornerGradient
    
    DeleteDummySheetWhenItExists objBook

    GetBookPathAfterCreatePluralSampleDataTablesToSheet = strBookPath
End Function


'''
''' This is used at SqlUTfXlSheetExpander
'''
Public Function GetOutputBookPathAfterCreateALeastTestSheetSet() As String

    Dim objDic As Scripting.Dictionary, strBookPath As String
    
    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add UnitTestPreparedSampleDataTable.IncludeFictionalProductionNumberSampleDT, 1000
        
        .Add UnitTestPreparedSampleDataTable.FishTypeSampleMasterDT, 1
    
    End With
    
    strBookPath = GetBookPathAfterCreatePluralSampleDataTablesToSheet(objDic)
    
    GetOutputBookPathAfterCreateALeastTestSheetSet = strBookPath
End Function




Public Sub OpenSampleDataTableWorkbookForSanityTest(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable)

    GetWorkbookIfItExists GetPreparedSampleDataTableBookPathForSanityTest(venmSanityTestPreparedSampleDataTable), True
End Sub


'''
'''
'''
Public Sub GetBookPathAndSheetNameAndFieldTitleToColumnWidthDicFromSanityTestPreparedSampleDataTable(ByRef rstrBookPath As String, ByRef rstrSheetName As String, ByRef robjFieldTitleToColumnWidthDic As Scripting.Dictionary, ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable)

    PrepareSampleDataTableWorkbookForSanityTest venmSanityTestPreparedSampleDataTable
    
    rstrBookPath = GetPreparedSampleDataTableBookPathForSanityTest(venmSanityTestPreparedSampleDataTable)
    
    rstrSheetName = GetPreparedSampleDataTableSheetNameForSanityTest(venmSanityTestPreparedSampleDataTable)
    
    Set robjFieldTitleToColumnWidthDic = GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest(venmSanityTestPreparedSampleDataTable)
End Sub


'''
'''
'''
Public Sub PrepareSampleDataTableWorkbookForSanityTest(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable)

    With New Scripting.FileSystemObject
    
        Select Case venmSanityTestPreparedSampleDataTable
            
            Case UnitTestPreparedSampleDataTable.BasicSampleDT
            
                If Not .FileExists(GetPreparedSampleDataTableBookPathForSanityTest(BasicSampleDT)) Then
            
                    msubSanityTestOfCreateSampleDataTable01ToSheet
                
                End If
            
            Case UnitTestPreparedSampleDataTable.IncludeFictionalProductionNumberSampleDT
            
                If Not .FileExists(GetPreparedSampleDataTableBookPathForSanityTest(IncludeFictionalProductionNumberSampleDT)) Then
                
                    msubSanityTestOfCreateSampleDataTable02ToSheet
                End If
        End Select
    End With
End Sub


'**---------------------------------------------
'** solute Sample book path and name
'**---------------------------------------------
'''
'''
'''
Public Function GetPreparedSampleDataTableSheetNameForSanityTest(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable) As String
    
    Dim strSheetName As String

    strSheetName = ""

    Select Case venmSanityTestPreparedSampleDataTable
    
        Case UnitTestPreparedSampleDataTable.BasicSampleDT
    
            strSheetName = "SampleTest01"
            
        Case UnitTestPreparedSampleDataTable.IncludeFictionalProductionNumberSampleDT
        
            strSheetName = "SampleTest02"
    End Select
    
    GetPreparedSampleDataTableSheetNameForSanityTest = strSheetName
End Function


Public Function GetPreparedSampleDataTableBookPathForSanityTest(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable) As String

    GetPreparedSampleDataTableBookPathForSanityTest = GetCurrentBookOutputDir() & "\AutoGeneratedSamples\" & GetPreparedSampleDataTableBaseNameForUnitTest(venmSanityTestPreparedSampleDataTable) & ".xlsx"
End Function

'''
'''
'''
Public Function GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable) As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary

    Set objDic = Nothing
    
    Select Case venmSanityTestPreparedSampleDataTable
        
        Case UnitTestPreparedSampleDataTable.BasicSampleDT
        
            Set objDic = GetTextToRealNumberDicFromLineDelimitedChar("FixedString,12.5,RowNumber,13,FlowerTypeSample,14.5,RandomInteger,16")

        Case UnitTestPreparedSampleDataTable.IncludeFictionalProductionNumberSampleDT
        
            Set objDic = GetTextToRealNumberDicFromLineDelimitedChar("RowNumber,13,RealNumber,16,FishType,14.5,FictionalProductionSerialNumber,15")
            
        Case UnitTestPreparedSampleDataTable.FishTypeSampleMasterDT
        
            Set objDic = GetTextToRealNumberDicFromLineDelimitedChar("Index,10,FishTypeEng,12,FishTypeJpn,12,RandAlpha,12")
        
        Case UnitTestPreparedSampleDataTable.FlowerTypeSampleMasterDT
        
            Set objDic = GetTextToRealNumberDicFromLineDelimitedChar("Index,10,FlowerTypeEng,12,FlowerTypeJpn,12,RandAlpha,12")
    End Select

    Set GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest = objDic
End Function



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** craete a Excel book
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToOpenExcelBookAfterCreateSampleDataTable01()

    mfstrGetBookPathAfterCreateSampleDataTableSheet01
End Sub

'''
'''
'''
Private Sub msubSanityTestToOpenExcelBookAfterCreateSampleDataTable02()

    mfstrGetBookPathAfterCreateSampleDataTableSheet02
End Sub


'**---------------------------------------------
'** output Excel.Worksheet as sample
'**---------------------------------------------
'''
'''
'''
''' <Argument>venmSanityTestPreparedSampleDataTable: Input</Argument>
''' <Argument>vvarRCTable: Input</Argument>
''' <Argument>vobjFieldTitlesCol: Input</Argument>
''' <Argument>vobjDataTableSheetFormatter: Input</Argument>
''' <Argument>vdblElapsedSecondTime: Input</Argument>
Private Sub OutputCreatedSampleDataTableToSheet(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
        ByVal vvarRCTable As Variant, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal vobjDataTableSheetFormatter As DataTableSheetFormatter, _
        Optional ByVal vdblElapsedSecondTime As Double = 0#)
    
    
    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    
    strBookPath = GetPreparedSampleDataTableBookPathForSanityTest(venmSanityTestPreparedSampleDataTable)

    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet.Name = GetPreparedSampleDataTableSheetNameForSanityTest(venmSanityTestPreparedSampleDataTable)

    OutputVarTableToSheet objSheet, vvarRCTable, vobjFieldTitlesCol, vobjDataTableSheetFormatter
    
    
    msubAddCreatingSapmleDataTableLog objSheet, vdblElapsedSecondTime
    
    DeleteDummySheetWhenItExists objBook
    
    ' save
    ForceToSaveBookWithoutDisplayAlert objBook
End Sub


'**---------------------------------------------
'** About Excel book files
'**---------------------------------------------
'''
'''
'''
Private Function mfstrGetBookPathAfterCreateSampleDataTableSheet01(Optional ByVal vintCountOfRows As Long = 100, _
        Optional ByVal vblnAllowToCloseWorkbookAfterFinishedCreating As Boolean = False) As String

    mfstrGetBookPathAfterCreateSampleDataTableSheet01 = GetBookPathAfterCreateSampleDataTableSheet(BasicSampleDT, vintCountOfRows, vblnAllowToCloseWorkbookAfterFinishedCreating)
End Function

'''
'''
'''
Private Function mfstrGetBookPathAfterCreateSampleDataTableSheet02(Optional ByVal vintCountOfRows As Long = 1000, _
        Optional ByVal vblnAllowToCloseWorkbookAfterFinishedCreating As Boolean = False) As String

    mfstrGetBookPathAfterCreateSampleDataTableSheet02 = GetBookPathAfterCreateSampleDataTableSheet(IncludeFictionalProductionNumberSampleDT, _
            vintCountOfRows, _
            vblnAllowToCloseWorkbookAfterFinishedCreating)
End Function



'''
'''
'''
Private Function mfobjOutputExcelBookFromSampleDataTable(ByRef rvarDataTable As Variant, ByRef robjFieldTitles As Collection, ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable) As Excel.Worksheet

    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    
    strBookPath = GetPreparedSampleDataTableExcelBookPathForTest(venmUnitTestPreparedSampleDataTable)

    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)

    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    objSheet.Name = GetSheetNameOfPreparedSampleDataTableExcelBookForTest(venmUnitTestPreparedSampleDataTable)
    
    OutputVarTableToSheet objSheet, rvarDataTable, robjFieldTitles, mfobjGetDataTableSheetFormatterOfPreparedSampleType(venmUnitTestPreparedSampleDataTable)
    
    DeleteDummySheetWhenItExists objBook
    
    
    Set mfobjOutputExcelBookFromSampleDataTable = objSheet
End Function


'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterOfPreparedSampleType(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable) As DataTableSheetFormatter

    Dim objDataTableSheetFormatter As DataTableSheetFormatter
    
    Set objDataTableSheetFormatter = New DataTableSheetFormatter
    
    With objDataTableSheetFormatter
    
        Set .FieldTitleToColumnWidthDic = GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest(venmUnitTestPreparedSampleDataTable)
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        
        .RecordBordersType = RecordCellsGrayAll
    End With
    
    Set mfobjGetDataTableSheetFormatterOfPreparedSampleType = objDataTableSheetFormatter
End Function



'**---------------------------------------------
'** About DataTableSheetFormatter for sample data-tables
'**---------------------------------------------
Private Function mfobjGetDataTableFormatterForSample01() As DataTableSheetFormatter

    Dim objDataTableFormatter As DataTableSheetFormatter
    
    Set objDataTableFormatter = New DataTableSheetFormatter

    With objDataTableFormatter
    
        Set .FieldTitleToColumnWidthDic = GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest(BasicSampleDT)
    End With

    Set mfobjGetDataTableFormatterForSample01 = objDataTableFormatter
End Function

Private Function mfobjGetDataTableFormatterForSample02() As DataTableSheetFormatter

    Dim objDataTableFormatter As DataTableSheetFormatter
    
    Set objDataTableFormatter = New DataTableSheetFormatter

    With objDataTableFormatter
    
        Set .FieldTitleToColumnWidthDic = GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest(IncludeFictionalProductionNumberSampleDT)
    End With

    Set mfobjGetDataTableFormatterForSample02 = objDataTableFormatter
End Function

'**---------------------------------------------
'** About addtional logging onto Excel.Worksheet
'**---------------------------------------------
'''
'''
'''
Private Sub msubAddCreatingSapmleDataTableLog(ByVal vobjSheet As Worksheet, _
        Optional ByVal vdblElapsedSecondTime As Double = 0#)


    Dim intRowsCount As Long, intColumnsCount As Long, strLoggedTimeStamp As String
    
    Dim strLog As String


    GetCountsOfRowsColumnsFromSheet vobjSheet, intRowsCount, intColumnsCount, strLoggedTimeStamp

    strLog = "Created sample fictional data-table log" & vbNewLine
    
    strLog = strLog & "Created time: " & strLoggedTimeStamp & vbNewLine
    
    If vdblElapsedSecondTime > 0# Then
        
        strLog = strLog & "Creation elapsed time: " & Format(vdblElapsedSecondTime, "0.000") & " [s]" & vbNewLine
    End If
    
    strLog = strLog & "Rows Count: " & intRowsCount & vbNewLine
    
    strLog = strLog & "Columns Count: " & intColumnsCount
    
    AddAutoShapeGeneralMessage vobjSheet, strLog, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault
End Sub
