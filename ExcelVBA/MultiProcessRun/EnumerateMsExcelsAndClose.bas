Attribute VB_Name = "EnumerateMsExcelsAndClose"
'
'   Close Excel book which includes the outside process of Excel.Application
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Excel and Windows OS
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  1/Oct/2019    Mr. excel-chunchun      A part of the idea has been disclosed at https://www.excel-chunchun.com/entry/enumwindows-excel-vba
'       Wed, 31/May/2023    Kalmclaeyd Tarclanus    Modified
'

Option Explicit


'///////////////////////////////////////////////
'/// Windows API Declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function EnumWindows Lib "user32.dll" (ByVal lpEnumFunc As LongPtr, ByVal lParam As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function EnumChildWindows Lib "user32.dll" (ByVal hWndParent As LongPtr, ByVal lpEnumFunc As LongPtr, ByVal lParam As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As LongPtr, ByVal Msg As LongPtr, ByVal wParam As LongPtr, lParam As Any) As LongPtr
    Private Declare PtrSafe Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As LongPtr
    Private Declare PtrSafe Function ObjectFromLresult Lib "oleacc" (ByVal lResult As LongPtr, riid As Any, ByVal wParam As LongPtr, ppvObject As Any) As LongPtr
    Private Declare PtrSafe Function IsWindow Lib "user32" (ByVal hwnd As LongPtr) As LongPtr
#Else
    Private Declare Function EnumWindows Lib "user32.dll" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
    
    Private Declare Function EnumChildWindows Lib "user32.dll" (ByVal hWndParent As Long, ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
    
    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, lParam As Any) As Long
    Private Declare Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As Long
    Private Declare Function ObjectFromLresult Lib "oleacc" (ByVal lResult As Long, riid As Any, ByVal wParam As Long, ppvObject As Any) As Long
    Private Declare Function IsWindow Lib "user32" (ByVal hWnd As Long) As Long
#End If

Private Const OBJID_NATIVEOM = &HFFFFFFF0
Private Const OBJID_CLIENT = &HFFFFFFFC

Private Const IID_IMdcList = "{8BD21D23-EC42-11CE-9E0D-00AA006002F3}"
Private Const IID_IUnknown = "{00000000-0000-0000-C000-000000000046}"
Private Const IID_IDispatch = "{00020400-0000-0000-C000-000000000046}"


Private Const WM_GETOBJECT = &H3D&

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjWindowHandleToBooksDic As Scripting.Dictionary  ' Dictionary(Of LongPtr[hWnd], Of Excel.Workbook)


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' use VBA.Dir
'''
Public Sub DeleteExcelBookIfItInThisProcessOrOtherProcessesExistsWithClosing(ByVal vstrBookPath As String)

    Dim strFileName As String
        
    If FileExistsByVbaDir(vstrBookPath) Then
    
        strFileName = GetFileNameFromPathByVbaDir(vstrBookPath)
    
        CloseExcelForAllProcessesByBookName strFileName
        
        VBA.Kill vstrBookPath
    End If
End Sub


'''
'''
'''
Public Sub CloseExcelForAllProcessesByBookName(ByVal vstrTargetBookNameTip As String)
    
#If VBA7 Then
    Dim intReturn As LongPtr
#Else
    Dim intReturn As Long
#End If
    
    Dim varHWnd As Variant, objBooksCol As Collection, objBook As Excel.Workbook
    Dim i As Long
  
    On Error Resume Next
  
    Set mobjWindowHandleToBooksDic = Nothing
    
    
    On Error GoTo 0
    
    ' get Window handle of Excel workbooks
    intReturn = EnumWindows(AddressOf EnumWindowsProc, ByVal 0&)
    
    If Not mobjWindowHandleToBooksDic Is Nothing Then
    
        With mobjWindowHandleToBooksDic
        
            For Each varHWnd In .Keys
            
                Set objBook = .Item(varHWnd)
            
                With objBook
                
                    If InStr(1, .Name, vstrTargetBookNameTip) > 0 Then
                    
                        Debug.Print "To close Excel: " & .Name
                        
                        .Saved = True
                        
                        .Close
                    End If
                End With
            Next
        End With
    End If
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
''' call back function
'''
#If VBA7 Then
Private Function EnumWindowsProc(ByVal vintHWnd As LongPtr, ByVal vintlParam As LongPtr) As Long

    Dim intReturn       As LongPtr
#Else
Private Function EnumWindowsProc(ByVal vintHWnd As Long, ByVal vintlParam As Long) As Long

    Dim intReturn       As Long
#End If
    
    Dim strClass        As String
    
    strClass = GetClassNameVBAString(vintHWnd)
    
    If strClass = "XLMAIN" Then
    
        ' enumerate the child windows
        
        intReturn = EnumChildWindows(vintHWnd, AddressOf EnumChildSubProc, vintlParam)
    End If
    
    ' continue to enumerate
EnumPass:

    EnumWindowsProc = True
End Function

'''
''' call back function: enumerate the child windows
'''
#If VBA7 Then
Private Function EnumChildSubProc(ByVal vintHwndChild As LongPtr, ByVal vintlParam As LongPtr) As Long
#Else
Private Function EnumChildSubProc(ByVal vintHwndChild As Long, ByVal vintlParam As Long) As Long
#End If

    Dim strClass As String, strWindowTitle  As String
    Dim objBook As Excel.Workbook, strHWnd As String
    
    strClass = GetClassNameVBAString(vintHwndChild)
    
    If strClass = "EXCEL7" Then
        
        strWindowTitle = GetWindowTextVBAString(vintHwndChild)
  
        If InStr(1, LCase(strWindowTitle), ".xla") = 0 Then   ' ignore Excel Adin Workbook
        
            If mobjWindowHandleToBooksDic Is Nothing Then Set mobjWindowHandleToBooksDic = New Scripting.Dictionary
            
            Set objBook = GetExcelBookFromWindowHandle(vintHwndChild)
            
            strHWnd = CStr(vintHwndChild)
            
            mobjWindowHandleToBooksDic.Add strHWnd, objBook
        End If
    End If
    
    ' continue to enumerate
EnumChildPass:

    EnumChildSubProc = True
End Function



#If VBA7 Then
Private Function GetExcelBookFromWindowHandle(ByVal vintHWnd As LongPtr) As Excel.Workbook

    Dim intExcelHWnd As LongPtr, intReturn As LongPtr, intMessageResult As LongPtr
#Else
Private Function GetExcelBookFromWindowHandle(ByVal vintHWnd As Long) As Excel.Workbook
    
    Dim intExcelHWnd As Long, intReturn As Long, intMessageResult As Long
#End If

    Dim IID(0 To 3) As Long, bytID() As Byte
    Dim objWindow As Excel.Window, objBook As Excel.Workbook
  
    intExcelHWnd = vintHWnd
  
    If IsWindow(intExcelHWnd) = 0 Then Exit Function
    
    intMessageResult = SendMessage(intExcelHWnd, WM_GETOBJECT, 0, ByVal OBJID_NATIVEOM)
    
    If intMessageResult Then
        
        bytID = IID_IDispatch & vbNullChar
        
        IIDFromString bytID(0), IID(0)
        
        intReturn = ObjectFromLresult(intMessageResult, IID(0), 0, objWindow)
        
        If Not objWindow Is Nothing Then
        
            Set objBook = objWindow.Parent
        End If
    End If

    Set GetExcelBookFromWindowHandle = objBook
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToCloseXlWorkbooksForAllExcelProcesses()

    ' For all Excel application process, close the Excel workbooks which file-name include [Book].
    
    CloseExcelForAllProcessesByBookName "Book"
End Sub

'''
''' this can close all out-process 'ANewMacroBook" name Excel books
'''
Private Sub msubSanityTestToCloseSpecifiedNameBookForAllExcelProcesses()

    ' For all Excel application process, close the Excel workbooks which file-name include [Book].
    
    CloseExcelForAllProcessesByBookName "ANewMacroBook"
End Sub
