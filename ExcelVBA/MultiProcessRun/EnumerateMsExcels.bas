Attribute VB_Name = "EnumerateMsExcels"
'
'   Detect all Excel.Workbook which includes the outside process of Excel.Application
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Excel and Windows OS
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  1/Oct/2019    Mr. excel-chunchun      The original idea has been disclosed at https://www.excel-chunchun.com/entry/enumwindows-excel-vba
'       Jan, 19/Jan/2023    Kalmclaeyd Tarclanus    Modified
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = True

'///////////////////////////////////////////////
'/// Windows API Declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As LongPtr, ByVal Msg As LongPtr, ByVal wParam As LongPtr, lParam As Any) As LongPtr

    Private Declare PtrSafe Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As LongPtr

    Private Declare PtrSafe Function ObjectFromLresult Lib "oleacc" (ByVal lResult As LongPtr, riid As Any, ByVal wParam As LongPtr, ppvObject As Any) As LongPtr

    Private Declare PtrSafe Function IsWindow Lib "user32" (ByVal hwnd As LongPtr) As LongPtr
#Else
    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, lParam As Any) As Long

    Private Declare Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As Long

    Private Declare Function ObjectFromLresult Lib "oleacc" (ByVal lResult As Long, riid As Any, ByVal wParam As Long, ppvObject As Any) As Long

    Private Declare Function IsWindow Lib "user32" (ByVal hWnd As Long) As Long
#End If


Private Const OBJID_NATIVEOM = &HFFFFFFF0
Private Const OBJID_CLIENT = &HFFFFFFFC

' Interface IDentification
Private Const IID_IMdcList As String = "{8BD21D23-EC42-11CE-9E0D-00AA006002F3}"
Private Const IID_IUnknown As String = "{00000000-0000-0000-C000-000000000046}"
Private Const IID_IDispatch As String = "{00020400-0000-0000-C000-000000000046}"

Private Const WM_GETOBJECT = &H3D&

'-----
' class name of Windows programs
Private Const WINDOW_CLASS_EXCEL = "XLMAIN" ' Excel Application
Private Const WINDOW_CLASS_VBE = "wndclass_desked_gsk"  ' Visual Basic Editor
Private Const WINDOW_CALSS_EXPLORER = "CabinetWClass"

Private Const WINDOW_CLASS_MS_WORD = "OpusApp" ' Word Application

Private Const CHILD_CLASS_EXCELWINDOW = "EXCEL7"
Private Const CHILD_CLASS_VBE = "VbaWindow"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
#If HAS_REF Then
    Private mobjFoundParentHWndToLParamDic As Scripting.Dictionary
    
    Private mobjFoundChildHWndToParentHWndDic As Scripting.Dictionary
#Else
    Private mobjFoundParentHWndToLParamDic As Object  'Dictionary
    
    Private mobjFoundChildHWndToParentHWndDic As Object   'Dictionary
#End If

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Excel.Application state control for outside this VBIDE process
'**---------------------------------------------
'''
''' This can be also used from either Word application or PowerPoint application
'''
Public Sub CloseAllProcessExcelBooksExceptMySelfApplication()

    Dim strExceptHwnd As String, varHWnd As Variant, objApplication As Excel.Application, objBook As Excel.Workbook

    strExceptHwnd = ""

    If Trim(Replace(LCase(Application.Name), "microsoft", "")) = "excel" Then
    
        ' The reason why use the VBA.CallByName() is that this statement can avoid a VBA syntax error in both Word or PowerPoint
        
        strExceptHwnd = "" & VBA.CallByName(Application, "Hwnd", VbGet)
    End If
    
    With ExecutingHwndToExcelApplicationsDic
    
        For Each varHWnd In .Keys
        
            If varHWnd <> strExceptHwnd Then
        
                Set objApplication = .Item(varHWnd)
                
                For Each objBook In objApplication.Workbooks
                
                    objBook.Saved = True
                    
                    objBook.Close
                Next
                
                objApplication.Quit
            End If
        Next
    End With
End Sub

'''
''' This can be also used from either Word application or PowerPoint application
'''
Public Sub ChangeVisibleAllProcessExcelBooksExceptMySelfApplication()

    Dim strExceptHwnd As String, varHWnd As Variant, objApplication As Excel.Application

    strExceptHwnd = ""

    If Trim(Replace(LCase(Application.Name), "microsoft", "")) = "excel" Then
    
        ' The reason why use the VBA.CallByName() is that this statement can avoid a VBA syntax error in both Word or PowerPoint
        
        strExceptHwnd = "" & VBA.CallByName(Application, "Hwnd", VbGet)
    End If
    
    With ExecutingHwndToExcelApplicationsDic
    
        For Each varHWnd In .Keys
        
            If varHWnd <> strExceptHwnd Then
        
                Set objApplication = .Item(varHWnd)
                
                With objApplication
                
                    If Not .Visible Then
                    
                        .Visible = True
                    End If
                End With
            End If
        Next
    End With
End Sub


'**---------------------------------------------
'** Pick outside Excel processes up tools by Windows API
'**---------------------------------------------
'''
''' get all Excel application window object for all Windows processes in execution
'''
''' <Return>Excel.Window(0 To #) - Array of the Excel window objects</Return>
'''
''' <Attention>
'''  Remove some Excel processes of the different execution autorities, which cannot be got. However if this Excel process is executed with the Administrator authority, it can be got.
'''  Remove the protected view Excel processes even though this Excel process is also executed with the Adiministrator authority.
''' </Attention>
Private Function ExecExcelWindows() As Variant

    'get objects as if this enumerate ExcelProcess().excelWindow().hWnd
    Dim varHWndArray As Variant, varSafeExcelWindowArray As Variant
    
    varHWndArray = EnumFindChildWindows(vstrParentClassName:=WINDOW_CLASS_EXCEL, vstrChildClassName:=CHILD_CLASS_EXCELWINDOW)
    
    If LBound(varHWndArray) <= UBound(varHWndArray) Then
    
        ReDim varSafeExcelWindowArray(LBound(varHWndArray) To UBound(varHWndArray))
        
        Dim i As Long, j As Long: j = LBound(varHWndArray) - 1
        
        For i = LBound(varHWndArray) To UBound(varHWndArray)
            
            j = j + 1
            
            Set varSafeExcelWindowArray(j) = GetExcelWindowByHWnd(varHWndArray(i))
            
            ' To be failed to get it, because of the lack ot the authority
            If varSafeExcelWindowArray(j) Is Nothing Then j = j - 1
            
            ' Disabled to operate this because of the protected view of the Excel window. If you try to operate it, then the Excel application process will be aborted.
            If TypeName(varSafeExcelWindowArray(j)) = "ProtectedViewWindow" Then j = j - 1
            
            ' Disabled to operate this because of the protected view of the Excel window. If you try to operate it, then the Excel application process will be aborted.
            If varSafeExcelWindowArray(j).Application.hwnd = 0 Then j = j - 1
        Next
        
        If j < LBound(varHWndArray) Then
        
            ExecExcelWindows = Array()
        Else
            ReDim Preserve varSafeExcelWindowArray(LBound(varHWndArray) To j)
            
            ExecExcelWindows = varSafeExcelWindowArray
        End If
    Else
        ' Only when this function is executed from either Word or PowerPoint and there are no Excel process, then the following should have been executed.
    
        ExecExcelWindows = Array()
    End If
End Function

'''
''' get the array of the Excel.Application all objects which includes other Excel application processes
'''
''' <Return>Excel.Application(0 To #) - array of objects</Return>
'''
''' <Attention>
'''  The protected view Excel window returns 0 value
'''  If it have been tried to access, this Excel process might be aborted.
''' </Attention>
Public Function ExecExcelApps() As Variant
    
    ' Delete duplicated Application.hWnd
    
    Dim objHWndToExcelApplicationDic As Scripting.Dictionary: Set objHWndToExcelApplicationDic = CreateObject("Scripting.Dictionary")
    
    Dim varWindow  As Variant, objWindow As Excel.Window
    
    Dim strHWnd As String
    
    
    For Each varWindow In ExecExcelWindows()
    
        Set objWindow = varWindow
    
        strHWnd = "" & objWindow.Application.hwnd
        
        If strHWnd = "0" Then
'            Debug.Print "This may be a protected view of the Excel application"
        Else
            With objHWndToExcelApplicationDic
                
                If Not .Exists(strHWnd) Then
                    
                    .Add strHWnd, objWindow.Application
                    
    '                Debug.Print hWnd, Win.Application.Caption
                End If
            End With
        End If
    Next
    
    ExecExcelApps = objHWndToExcelApplicationDic.Items  ' Return array of Excel.Application object references
End Function

'''
''' get the array of the Excel.Workbook all objects which includes other Excel application processes
'''
''' <Return>Excel.Workbook(0 To #) - array of the Workbook objects</Return>
'''
''' <Attention>
'''  The protected view Excel window returns 0 value
'''  If it have been tried to access, this Excel process might be aborted.
''' </Attention>
Public Function ExecExcelWorkbooks() As Variant
    
    'Remove the duplicated Application.hWnd
    Dim objBookNameToBookInstanceDic As Scripting.Dictionary: Set objBookNameToBookInstanceDic = CreateObject("Scripting.Dictionary")
    
    Dim varWindow As Variant
    Dim objWindow As Excel.Window, objBook As Excel.Workbook
    
    Dim strHWnd As String
    
    For Each varWindow In ExecExcelWindows()
    
        Set objWindow = varWindow
        
        strHWnd = "" & objWindow.Application.hwnd
        
        If strHWnd = "0" Then
        
'            Debug.Print "This may be a protected view of the Excel application"
        Else
            Set objBook = objWindow.Parent
            
            With objBookNameToBookInstanceDic
                
                If Not .Exists(objBook.Name) Then
                    
                    .Add objBook.Name, objBook
                    
        '            Debug.Print Win.Parent.Name, Win.Parent.Name
                End If
            End With
        End If
    Next
    
    ExecExcelWorkbooks = objBookNameToBookInstanceDic.Items ' return Excel.Workbook(0 To #)
End Function

'''
'''
'''
Public Function ExecutingHwndToExcelApplicationsDic() As Scripting.Dictionary

    Dim objHwndToExcelApplicationsDic As Scripting.Dictionary
    Dim varWindow As Variant, objWindow As Excel.Window, strHWnd As String
    
    
    Set objHwndToExcelApplicationsDic = New Scripting.Dictionary
    
    For Each varWindow In ExecExcelWindows()
    
        Set objWindow = varWindow
        
        strHWnd = "" & objWindow.Application.hwnd
        
        If strHWnd = "0" Then
        
'            Debug.Print "This may be a protected view of the Excel application"
        Else
            With objHwndToExcelApplicationsDic
            
                If Not .Exists(strHWnd) Then
                
                    .Add strHWnd, objWindow.Application
                End If
            End With
        End If
    Next
    
    Set ExecutingHwndToExcelApplicationsDic = objHwndToExcelApplicationsDic
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
''' get Excel.Window instances from hWnd window-handle
'''
''' <Argument>hWnd : window handle</Argument>
''' <Return>Excel.Window</Return>
'''
''' <Attention>
'''  If a Excel process is executed by the other user authority,
'''  the object may be failed to get, and this returns to Nothing.
''' </Attention>
Private Function GetExcelWindowByHWnd(ByRef rvarHWnd As Variant) As Excel.Window

    Dim intID(0 To 3) As Long
    Dim bytID()     As Byte
    
#If VBA7 Then
    Dim intMessageResult As LongPtr, intReturn  As LongPtr
#Else
    Dim intMessageResult As Long, intReturn  As Long
#End If
    
    Dim objExcelWindow As Excel.Window

    If IsWindow(rvarHWnd) = 0 Then Exit Function
    
    intMessageResult = SendMessage(rvarHWnd, WM_GETOBJECT, 0, ByVal OBJID_NATIVEOM)
    
    If intMessageResult Then
    
        bytID = IID_IDispatch & vbNullChar
        
        IIDFromString bytID(0), intID(0)
        
        intReturn = ObjectFromLresult(intMessageResult, intID(0), 0, objExcelWindow)
    End If
    
    Set GetExcelWindowByHWnd = objExcelWindow
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' Enumerate all Excel.exe which includes other processes
'''
''' The protected view book also allow to display the App.Caption
'''
Private Sub msubSanityTestToExecExcelApps()

    Dim varApp As Variant
    
    Debug.Print "WindowHandle", "CountOfBooks", "ApplicationName"
    
    For Each varApp In ExecExcelApps()
    
        Debug.Print varApp.hwnd, varApp.Workbooks.Count, varApp.Caption
    Next
End Sub

'''
''' Enumerate all Excel Work books which includes other processes
'''
Private Sub msubSanityTestToExecExcelWorkbooks()
    
    Dim varWorkbook As Variant
    
    Debug.Print "WindowHandle", "CountOfWindows", "BookName"
    
    For Each varWorkbook In ExecExcelWorkbooks()
    
        Debug.Print varWorkbook.Application.hwnd, varWorkbook.Windows.Count, varWorkbook.Name
    Next
End Sub

'''
''' Enumerate all Excel Window objects which includes other processes
'''
Private Sub msubSanityTestToExecExcelWindows()

    Dim varWindow As Variant, objWindow As Excel.Window
    'Dim hWnd As String
    
    Debug.Print "WindowHandle", "Zero", "WindowTitleBarCaption"
    
    For Each varWindow In ExecExcelWindows()
        
        If varWindow Is Nothing Then
'            Debug.Print "err"
        ElseIf TypeName(varWindow) = "ProtectedViewWindow" Then
'            Debug.Print "err"
        ElseIf varWindow.Application.hwnd = 0 Then
'            Debug.Print "This may be a protected window"
        Else
            Set objWindow = varWindow
            
            Debug.Print objWindow.Application.hwnd, 0, objWindow.Caption
        End If
    Next
End Sub


