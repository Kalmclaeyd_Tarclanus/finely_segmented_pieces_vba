Attribute VB_Name = "EnumerateAppWindowsForXl"
'
'   Sanity tests to list up Windows at the current Windows OS
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 11/Aug/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const WINDOW_CLASS_EXCEL = "XLMAIN" ' Excel Application

Private Const WINDOW_CLASS_MS_WORD = "OpusApp" ' Word Application



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToOutputCurrentWinProcessAndChildWindowsInfoToSheetAboutMsWord()

    Dim strBookPath As String
    
    strBookPath = mfstrGetTempWindowHWndInfoBooksDir() & "\CurrentMsWordHWndInfo.xlsx"

    OutputCurrentWinProcessAndChildWindowsInfoToSheet strBookPath, WINDOW_CLASS_MS_WORD
End Sub

'''
'''
'''
Private Sub msubSanityTestToOutputCurrentWinProcessAndChildWindowsInfoToSheetAboutMsExcel()

    Dim strBookPath As String
    
    strBookPath = mfstrGetTempWindowHWndInfoBooksDir() & "\CurrentMsExcelHWndInfo.xlsx"

    OutputCurrentWinProcessAndChildWindowsInfoToSheet strBookPath, WINDOW_CLASS_EXCEL
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub OutputCurrentWinProcessAndChildWindowsInfoToSheet(ByVal vstrOutputBookPath As String, _
        ByVal vstrParentClassName As String)

    Dim objTopLevelHWndToWindowTextDic As Scripting.Dictionary, objChildWindowClassNameInfoCol As Collection
    Dim objBook As Excel.Workbook, objTopLevelWindowInfoSheet As Excel.Worksheet, objChildWindowInfoSheet As Excel.Worksheet
    

    FindCurrentProcessAndChildWindowsInfo objTopLevelHWndToWindowTextDic, objChildWindowClassNameInfoCol, vstrParentClassName

    Set objBook = GetWorkbookAndPrepareForUnitTest(vstrOutputBookPath)

    Set objTopLevelWindowInfoSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objTopLevelWindowInfoSheet.Name = "ParentHWndInfo"

    Set objChildWindowInfoSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)

    objChildWindowInfoSheet.Name = "ChildHWndInfo"

    msubOutputTopLevelWindowHWndInfoToSheet objTopLevelWindowInfoSheet, objTopLevelHWndToWindowTextDic
    
    msubOutputChildWindowHWndInfoToSheet objChildWindowInfoSheet, objChildWindowClassNameInfoCol

    DeleteDummySheetWhenItExists objBook

End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrGetTempWindowHWndInfoBooksDir() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\WinOsHWndInfo"

    ForceToCreateDirectory strDir
    
    mfstrGetTempWindowHWndInfoBooksDir = strDir
End Function


'''
'''
'''
Private Sub msubOutputTopLevelWindowHWndInfoToSheet(ByRef robjSheet As Excel.Worksheet, ByRef robjTopLevelHWndToWindowTextDic As Scripting.Dictionary)

    OutputDicToSheet robjSheet, robjTopLevelHWndToWindowTextDic, GetColFromLineDelimitedChar("ParentHWnd,WindowText"), mfobjGetDataTableSheetFormatterOfHWndInfo()
End Sub

'''
'''
'''
Private Sub msubOutputChildWindowHWndInfoToSheet(ByRef robjSheet As Excel.Worksheet, ByRef robjChildWindowClassNameInfoCol As Collection)

    OutputColToSheet robjSheet, robjChildWindowClassNameInfoCol, GetColFromLineDelimitedChar("WindowText,ChildClassName,ChildHWnd"), mfobjGetDataTableSheetFormatterOfHWndInfo()
End Sub


'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterOfHWndInfo() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo
    
        .RecordBordersType = RecordCellsGrayAll
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("ParentHWnd,12.5,WindowText,22,ChildClassName,15,ChildHWnd,12.5")
        
    End With

    Set mfobjGetDataTableSheetFormatterOfHWndInfo = objDTSheetFormatter
End Function



