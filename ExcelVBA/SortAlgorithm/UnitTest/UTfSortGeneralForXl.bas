Attribute VB_Name = "UTfSortGeneralForXl"
'
'   Sanity tests of traditional sort implementation
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  3/Aug/2023    Kalmclaeyd Tarclanus    Separated from UTfSortGeneral.bas
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrQuickSortSampleTestBaseName As String = "UnitTestsForQuickSortSimply"

'**---------------------------------------------
'** local logging keys
'**---------------------------------------------
Private Const mstrElapsedSortingTimeBySecond As String = "ElapsedSortingTime"

Private Const mstrElapsedCreatingRandomShequenceTimeBySecond As String = "ElapsedCreatingRandomSequenceTime"

Private Const mstrElapsedOutputtingExcelSheetSecondTimeBySecond As String = "ElapsedOutputtingExcelSheetTime"

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToQuickSortArray()
    
    Dim intCountOfArray As Long, objLogs As Collection

    intCountOfArray = 30

    msubOutputSortedResultsToSheetAfterSortingTestingRandomArray objLogs, intCountOfArray, Decending, RandomCharacterForTest
End Sub


'///////////////////////////////////////////////
'/// Performance tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubPerformanceTestOfComprehensiveSortTypeWithoutOutputtingSheet()

    Dim objDTCol As Collection, varCountOfArray As Variant, intCountOfArray As Long
    Dim objSheet As Excel.Worksheet, objDoubleStopWatch As DoubleStopWatch
    
    Set objDTCol = New Collection
    
    ' GetIntegerColFromLineDelimitedChar("30,100,500,1500")
    
    Set objDoubleStopWatch = New DoubleStopWatch
    
    With objDoubleStopWatch
    
        .MeasureStart
    
        For Each varCountOfArray In GetIntegerColFromLineDelimitedChar("30,100,500,2000,6000")   ' GetIntegerColFromLineDelimitedChar("30,100,500,1500")
        
            intCountOfArray = varCountOfArray
        
            GetDTColForComprehensiveQuickSortPerformanceTests objDTCol, intCountOfArray
        Next

        .MeasureInterval
    End With

    Set objSheet = OutputQuickSortConditionAndElapsedTimeToSheet(objDTCol)
    
    msubAddLogMessageForSortingPerformanceTest objSheet, objDoubleStopWatch
End Sub

'''
'''
'''
Private Sub msubAddLogMessageForSortingPerformanceTest(ByRef robjSheet As Excel.Worksheet, ByRef robjDoubleStopWatch As DoubleStopWatch)

    Dim intRowsCount As Long, intColumnsCount As Long, strLoggedTimeStamp As String
    Dim strLog As String

    GetCountsOfRowsColumnsFromSheet robjSheet, intRowsCount, intColumnsCount, strLoggedTimeStamp
    
    
    strLog = "Traditional quick-sort algorithm performance test" & vbNewLine
    
    strLog = strLog & "Elapsed all processing time: " & robjDoubleStopWatch.ElapsedTimeByString & vbNewLine
    
    strLog = strLog & "Count of test cases: " & CStr(intRowsCount) & vbNewLine
    
    strLog = strLog & "Performance all tests finished time: " & strLoggedTimeStamp
    
    AddAutoShapeGeneralMessage robjSheet, strLog, XlShapeTextLogInteriorFillRedCornerGradient
End Sub




'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Output result to sheet
'**---------------------------------------------
'''
'''
'''
Public Function GetResultSheetAfterOutputSortedTestingRandomInfoTableToExcelBook(ByRef robjLogs As Collection, ByRef rvarInputArray As Variant, ByRef rvarOutputArray As Variant, ByVal vstrSheetName As String) As Excel.Worksheet
    
    Dim varDataTable() As Variant, objFieldTitles As Collection
    Dim objSheet As Excel.Worksheet, objDoubleStopWatch As DoubleStopWatch

    Set objDoubleStopWatch = New DoubleStopWatch
    
    With objDoubleStopWatch
    
        .MeasureStart
    
        JoinTwoArrays varDataTable, rvarInputArray, rvarOutputArray
        
        Set objFieldTitles = GetColFromLineDelimitedChar("RandomValues,SortedValues")
        
        Set objSheet = OutputSortedResultsToSheet(vstrSheetName, varDataTable, objFieldTitles, mfobjGetSoretedResultDataTableFormatter())
    
        .MeasureInterval
    End With
    
    If robjLogs Is Nothing Then Set robjLogs = New Collection
    
    robjLogs.Add objDoubleStopWatch.ElapsedSecondTime, key:=mstrElapsedOutputtingExcelSheetSecondTimeBySecond

    Set GetResultSheetAfterOutputSortedTestingRandomInfoTableToExcelBook = objSheet
End Function



'**---------------------------------------------
'** Testing interfaces
'**---------------------------------------------
'''
'''
'''
Private Sub msubOutputSortedResultsToSheetAfterSortingTestingRandomArray(ByRef robjLogs As Collection, ByVal vintCountOfArray As Long, Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending, Optional ByVal venmTestingRandomSequenceType As SortTestingRandomSequenceType = SortTestingRandomSequenceType.RandomNaturalIntegerForTest)

    msubOutputSortedTestingRandomArrayResultsToSheet robjLogs, vintCountOfArray, venmSortOrder, venmTestingRandomSequenceType
End Sub



'''
''' test plural sorting alogorithms
'''
Private Sub msubOutputSortedTestingRandomArrayResultsToSheet(ByRef robjLogs As Collection, ByVal vintCountOfArray As Long, _
        Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending, _
        Optional ByVal venmTestingRandomSequenceType As SortTestingRandomSequenceType = SortTestingRandomSequenceType.RandomNaturalIntegerForTest)
    
    Dim varInputArray() As Variant, varOutputArray() As Variant, objSheet As Excel.Worksheet
    
    
    GetLogsAndIOArrayAfterSortTestingRandomArray robjLogs, varInputArray, varOutputArray, vintCountOfArray, venmSortOrder, venmTestingRandomSequenceType
    
    Set objSheet = GetResultSheetAfterOutputSortedTestingRandomInfoTableToExcelBook(robjLogs, varInputArray, varOutputArray, "SampleSort")
    
    AddAutoShapeGeneralMessage objSheet, mfstrGetOperationLogForTraditionalSort(vintCountOfArray, venmSortOrder, venmTestingRandomSequenceType, robjLogs.Item(mstrElapsedSortingTimeBySecond), robjLogs.Item(mstrElapsedOutputtingExcelSheetSecondTimeBySecond))
End Sub


'''
'''
'''
Private Function mfstrGetOperationLogForTraditionalSort(ByVal vintCountOfArray As Long, ByVal venmSortOrder As SortOrder, ByVal venmTestingRandomSequenceType As SortTestingRandomSequenceType, ByVal vdblElapsedTimeAboutSorting As Double, ByVal vdblElapsedTimeAboutOutputExcelSheet As Double) As String

    Dim strLog As String
    
    
    ' English
    strLog = strLog & "Display the sorted sequence at the SortedValues column after sorting "
    
    strLog = strLog & GetTestingRandomSequenceDataTypeString(venmTestingRandomSequenceType) & " at the RandomValues column" & vbNewLine

    
    'strLog = strLog & "Using sort function: " & GetSortFunctionNameFromTestingImplimentedSortFunction(venmTestingImplimentedSortFunction) & vbNewLine

    strLog = strLog & "Length of random sequence: " & CStr(vintCountOfArray) & vbNewLine & "Elapsed time about sort: " & Format(vdblElapsedTimeAboutSorting, "0.000") & " [s]" & vbNewLine & vbNewLine
    
    strLog = strLog & "Elapsed Worksheet output time: " & Format(vdblElapsedTimeAboutOutputExcelSheet, "0.000") & " [s]"
    
    mfstrGetOperationLogForTraditionalSort = strLog
End Function


'**---------------------------------------------
'** About outputting result to sheet
'**---------------------------------------------
'''
''' Output results to Excel book
'''
Private Function OutputSortedResultsToSheet(ByVal vstrSheetName As String, ByRef rvarDataTable As Variant, ByVal vobjFieldTitles As Collection, ByVal vobjDataTableFormatter As DataTableSheetFormatter) As Excel.Worksheet
    
    Dim objSheet As Excel.Worksheet

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(GetSimplyQuickSortUnitTestOutputBookPath(), vstrSheetName)
    
    OutputVarTableToSheetRestrictedByDataTableSheetFormatter objSheet, rvarDataTable, vobjDataTableFormatter

    Set OutputSortedResultsToSheet = objSheet
End Function


'''
''' Simple data-table formatter
'''
Private Function mfobjGetSoretedResultDataTableFormatter() As DataTableSheetFormatter
    
    Dim objDataTableFormatter As DataTableSheetFormatter
    
    Set objDataTableFormatter = New DataTableSheetFormatter
    
    With objDataTableFormatter
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("RandomValues,21,SortedValues,21")
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
    
        .RecordBordersType = RecordCellsGrayAll
    End With

    Set mfobjGetSoretedResultDataTableFormatter = objDataTableFormatter
End Function

'''
''' Output results to Excel book
'''
Private Function OutputQuickSortConditionAndElapsedTimeToSheet(ByRef robjDTCol As Collection) As Excel.Worksheet
    
    Dim objSheet As Excel.Worksheet

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(GetSortConditionAndElapsedTimeResultsOutputBookPath(), "PerformanceResults")
    
    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, robjDTCol, mfobjGetDTSheetFormatterForSortConditionAndElapsedTimeResults()
    
    Set OutputQuickSortConditionAndElapsedTimeToSheet = objSheet
End Function

'''
'''
'''
Private Function mfobjGetDTSheetFormatterForSortConditionAndElapsedTimeResults() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("CountOfRandomizedArray,11,SortingElapsedTime,12,RandomSequenceCreatedTime,12,RandomSequenceTypeName,19,SortOrder,11")
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
    
        .RecordBordersType = RecordCellsGrayAll
        
        
        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
        
        With .ColumnsNumberFormatLocal
        
            .SetNumberFormatLocalAndApplyFieldTitles "0.000 ""[s]""", GetColFromLineDelimitedChar("SortingElapsedTime,RandomSequenceCreatedTime")
        End With
        
        .AllowToMergeCellsByContinuousSameValues = True
        
        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("CountOfRandomizedArray,SortingElapsedTime,RandomSequenceCreatedTime,RandomSequenceTypeName")
    End With

    Set mfobjGetDTSheetFormatterForSortConditionAndElapsedTimeResults = objDTSheetFormatter
End Function



'''
'''
'''
Public Function GetSimplyQuickSortUnitTestOutputBookPath() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
    
    ForceToCreateDirectory strDir

    GetSimplyQuickSortUnitTestOutputBookPath = strDir & "\" & mstrQuickSortSampleTestBaseName & ".xlsx"
End Function

'''
'''
'''
Public Function GetSortConditionAndElapsedTimeResultsOutputBookPath() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
    
    ForceToCreateDirectory strDir

    GetSortConditionAndElapsedTimeResultsOutputBookPath = strDir & "\PerformanceTestResultsOfSortedConditionAndElapsedTime.xlsx"
End Function

