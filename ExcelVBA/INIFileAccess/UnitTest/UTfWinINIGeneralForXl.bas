Attribute VB_Name = "UTfWinINIGeneralForXl"
'
'   Sanity tests for editing Windows INI file by Windows API
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and Excel
'       Dependent on InterfaceCall.bas and WinAPIMessageWithTimeOut
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  1/Sep/2016    Atsushi Oomura          A part of the idea has been disclosed at https://www.shuwasystem.co.jp/support/7980html/4734.html
'       Thu, 16/Jun/2022    Kalmclaeyd Tarclanus    Refactoring
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vstrOutputBookBaseName: Input</Argument>
''' <Argument>vstrIniBaseName: Input</Argument>
''' <Return>String: book-path</Return>
Public Function GetAllIniBookPathAfterCreateSpecialTestingCharacters(ByVal vstrOutputBookBaseName As String, _
        ByVal vstrIniBaseName As String) As String

    Dim strIniPath As String, strOuputBookPath As String

    ' About ; ;
    strIniPath = GetIniPathAfterCreateIniFileFromDic("A;B:C", "D;E:F", "G;H:I", vstrIniBaseName)

    ' About + -
    strIniPath = GetIniPathAfterCreateIniFileFromDic("E - + C", "D;E:F - C", "G;H:I # J", vstrIniBaseName)
    
    strIniPath = GetIniPathAfterCreateIniFileFromDic("E - + C", "Key02", "Value:03", vstrIniBaseName)

    ' About #
    strIniPath = GetIniPathAfterCreateIniFileFromDic("XA # 21", "6; -4", "# AK", vstrIniBaseName)


    strOuputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\" & vstrOutputBookBaseName & ".xlsx"

    OutputIniAllKeyValueToBook strIniPath, strOuputBookPath

    GetAllIniBookPathAfterCreateSpecialTestingCharacters = strOuputBookPath
End Function

'''
'''
'''
''' <Argument>vstrOutputBookBaseName: Input</Argument>
''' <Argument>vstrIniBaseName: Input</Argument>
''' <Return>String: book-path</Return>
Public Function GetAllIniBookPathAfterCreateOrdinalSampleCharacters(ByVal vstrOutputBookBaseName As String, _
        ByVal vstrIniBaseName As String) As String

    Dim strIniPath As String, strOuputBookPath As String


    strIniPath = GetIniPathAfterCreateIniFileFromDic("ABC", "DEF", "GHI", vstrIniBaseName)
    
    strIniPath = GetIniPathAfterCreateIniFileFromDic("L_D", "D K", "UI2", vstrIniBaseName)

    strIniPath = GetIniPathAfterCreateIniFileFromDic("XA_21", "TestKey", "3210", vstrIniBaseName)
    
    strIniPath = GetIniPathAfterCreateIniFileFromDic("XA_21", "KeyA", "51.21", vstrIniBaseName)


    strOuputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\" & vstrOutputBookBaseName & ".xlsx"

    OutputIniAllKeyValueToBook strIniPath, strOuputBookPath

    GetAllIniBookPathAfterCreateOrdinalSampleCharacters = strOuputBookPath
End Function

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Conversion test among INI file, Dictionary object, and Excel file
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToPermittedCharacters01()

    GetAllIniBookPathAfterCreateSpecialTestingCharacters "PermittedCharactersINIAll", "CharacterLimitationINITest"
End Sub


'''
'''
'''
Private Sub msubSanityTestToOrdinalCharacters02()

    GetAllIniBookPathAfterCreateOrdinalSampleCharacters "OrdinalCharactersINIAll", "OrdinalCharactersINITest"
End Sub


