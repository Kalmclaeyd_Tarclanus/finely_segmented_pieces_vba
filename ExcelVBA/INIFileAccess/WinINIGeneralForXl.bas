Attribute VB_Name = "WinINIGeneralForXl"
'
'   Output read INI file results to Excel worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 20/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About INI file
'**---------------------------------------------
'''
'''
'''
Public Function OutputIniSpecifiedSectionKeyValueToBook(ByVal vstrIniPath As String, ByVal vstrSection As String, ByVal vstrOutputBookPath As String) As Excel.Worksheet

    Dim objDic As Scripting.Dictionary
    Dim objSheet As Excel.Worksheet, strLog As String

    Set objDic = GetKeyValueDicFromIniASection(vstrIniPath, vstrSection)

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "INI_" & Left(vstrSection, 28))

    OutputDicToSheetRestrictedByDataTableSheetFormatter objSheet, objDic, mfobjGetDataTableSheetFormatterForIniKeyValueDicInASection()
    
    AddAutoShapeGeneralMessage objSheet, mfstrGetLogOfIniSpecifiedSectionKeyValue(vstrIniPath, vstrSection, objDic)
    
    Set OutputIniSpecifiedSectionKeyValueToBook = objSheet
End Function

'''
'''
'''
Private Function mfstrGetLogOfIniSpecifiedSectionKeyValue(ByRef rstrIniPath As String, ByRef rstrSection As String, ByRef robjDic As Scripting.Dictionary) As String

    Dim strLog As String
    
    strLog = "INI file path: " & rstrIniPath & vbNewLine
    
    strLog = strLog & "INI section: " & rstrSection & vbNewLine
    
    strLog = strLog & "Count of key-values: " & CStr(robjDic.Count) & vbNewLine
    
    strLog = strLog & "Read INI time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
    
    mfstrGetLogOfIniSpecifiedSectionKeyValue = strLog
End Function


'''
'''
'''
Public Function OutputIniAllKeyValueToBook(ByVal vstrIniPath As String, _
        ByVal vstrOutputBookPath As String, _
        Optional ByVal vstrOutputSheetName As String = "INI_All", _
        Optional ByVal vstrFieldTitleToColumnWidthDelimitedByComma As String = "Section,15,Key,15,Value,15") As Excel.Worksheet


    Dim objSectionToKeyValueDic As Scripting.Dictionary, objSheet As Excel.Worksheet, strLog As String
    
    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromIni(vstrIniPath)

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, vstrOutputSheetName)

    OutputOneNestedDicToSheetRestrictedByDataTableSheetFormatter objSheet, objSectionToKeyValueDic, GetDataTableSheetFormatterForIniSectionKeyValueDic(vstrFieldTitleToColumnWidthDelimitedByComma)
    
    AddAutoShapeGeneralMessage objSheet, GetAutoShapeTextLogOfIniAllKeyValue(objSheet, vstrIniPath)
    
    Set OutputIniAllKeyValueToBook = objSheet
End Function

'''
'''
'''
''' <Argument>robjSheet: Input-Output</Argument>
''' <Argument>rstrIniPath: Input</Argument>
''' <Argument>vblnUseIniCompatibleExcelBookPath: Input</Argument>
''' <Return>String: log-text</Return>
Public Function GetAutoShapeTextLogOfIniAllKeyValue(ByRef robjSheet As Excel.Worksheet, _
        ByRef rstrIniPath As String, _
        Optional ByVal vblnUseIniCompatibleExcelBookPath As Boolean = False) As String


    Dim intRowsCount As Long, intColumnsCount As Long, strLoggedTimeStamp As String, strLog As String


    GetCountsOfRowsColumnsFromSheet robjSheet, intRowsCount, intColumnsCount, strLoggedTimeStamp
    
    strLog = "Count of key-values: " & CStr(intRowsCount) & vbNewLine
    
    If vblnUseIniCompatibleExcelBookPath Then
    
        strLog = strLog & "INI compatible Excel book path: " & rstrIniPath & vbNewLine
    Else
        strLog = strLog & "INI file path: " & rstrIniPath & vbNewLine
    End If
    
    strLog = strLog & "Read INI time: " & strLoggedTimeStamp

    GetAutoShapeTextLogOfIniAllKeyValue = strLog
End Function

'''
''' A defect point - this needs to open Excel book before get a nested Dictionary object
'''
''' <Argument>vstrInputBookPath: Input</Argument>
''' <Argument>vstrInputSheetName: Input</Argument>
''' <Argument>vblnAllowToCloseBookAfterDetectData: Input</Argument>
''' <Return>Dictionary(Of String[Section], Dictionary(Of String[Key], Variant[Variant]))</Return>
Public Function GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBook(ByVal vstrInputBookPath As String, _
        Optional ByVal vstrInputSheetName As String = "INI_All", _
        Optional ByVal vblnAllowToCloseBookAfterDetectData As Boolean = True) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, strSQL As String
    Dim objSectionToKeyValueDic As Scripting.Dictionary, objBook As Excel.Workbook, objSheet As Excel.Worksheet
    
    
    Set objBook = GetWorkbook(vstrInputBookPath, False)

    Set objSheet = objBook.Worksheets.Item(vstrInputSheetName)

    If IsAtLeastOneMergedCellIncluded(objSheet, GetColFromLineDelimitedChar("Section")) Then

        RemoveMergedCellsToTable objSheet, GetColFromLineDelimitedChar("Section")
    End If

    Set objDic = GetSimpleOneNestedDictionaryFromWorksheet(objSheet, "Section", "Key", "Value")

    If vblnAllowToCloseBookAfterDetectData Then
    
        ' No necessity to save
    
        CloseBookWithoutDisplayAlerts objBook
    End If
    
    Set GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBook = objDic
End Function

'''
'''
'''
''' <Argument>vstrFieldTitleToColumnWidthDelimitedByComma: Input</Argument>
''' <Argument>vblnAllowToUseINIComatibleExcelSheet: Input</Argument>
''' <Return>DataTableSheetFormatter</Return>
Public Function GetDataTableSheetFormatterForIniSectionKeyValueDic(Optional ByVal vstrFieldTitleToColumnWidthDelimitedByComma As String = "Section,15,Key,15,Value,15", _
        Optional ByVal vblnAllowToUseINIComatibleExcelSheet As Boolean = False) As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = GetOneNestedKeyValueDicDataTableSheetFormatter(vstrFieldTitleToColumnWidthDelimitedByComma)
    
    With objDTSheetFormatter
    
        If vblnAllowToUseINIComatibleExcelSheet Then
        
            .FieldTitleInteriorType = ColumnNameInteriorOfExtractedTableFromSheet
        Else
            .FieldTitleInteriorType = ColumnNameInteriorOfLoadedINIFileAsTable
        End If
        
        .RecordBordersType = RecordCellsGrayAll
    End With

    Set GetDataTableSheetFormatterForIniSectionKeyValueDic = objDTSheetFormatter
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////


'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterForIniKeyValueDicInASection() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = GetKeyValueDataTableSheetFormatter()
    
    With objDTSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfLoadedINIFileAsTable
        
        .RecordBordersType = RecordCellsGrayAll
    End With
    
    Set mfobjGetDataTableSheetFormatterForIniKeyValueDicInASection = objDTSheetFormatter
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToOutputIniSpecifiedSectionKeyValueToBook()

    Dim strInputIniPath As String, strOuputBookPath As String
    
    strInputIniPath = GetIniTestFilePathAfterCheckExistenceOrCreateWhenItDoesntExist()

    strOuputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\SanityTestOfLoadedINIInASection.xlsx"

    OutputIniSpecifiedSectionKeyValueToBook strInputIniPath, "Section2", strOuputBookPath
End Sub


'''
'''
'''
Private Sub msubSanityTestToOutputIniAllKeyValueToBook()

    Dim strInputIniPath As String, strOuputBookPath As String, objSectionToKeyValueDic As Scripting.Dictionary
    
    strInputIniPath = GetIniTestFilePathAfterCheckExistenceOrCreateWhenItDoesntExist()

    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromIni(strInputIniPath)

    strOuputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\SanityTestOfLoadedINIAll.xlsx"

    OutputIniAllKeyValueToBook strInputIniPath, strOuputBookPath
End Sub


'''
'''
'''
Public Function GetOutputExcelBookPathAfterCreateBookOfLoadedIniAllData() As String

    Dim strInputIniPath As String, strOuputBookPath As String, objSectionToKeyValueDic As Scripting.Dictionary

    strInputIniPath = GetIniTestFilePathAfterCheckExistenceOrCreateWhenItDoesntExist()

    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromIni(strInputIniPath)

    strOuputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\SanityTestOfLoadedINIAll.xlsx"

    OutputIniAllKeyValueToBook strInputIniPath, strOuputBookPath

    GetOutputExcelBookPathAfterCreateBookOfLoadedIniAllData = strOuputBookPath
End Function

