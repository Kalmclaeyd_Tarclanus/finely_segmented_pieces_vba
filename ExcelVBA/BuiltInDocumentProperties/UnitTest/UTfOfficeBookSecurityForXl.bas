Attribute VB_Name = "UTfOfficeBookSecurityForXl"
'
'   Sanity tests to output office book built-in properties to a Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat,  1/Sep/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToUTfOfficeBookSecurityForXl()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in this project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "OfficeBookSecurity,OfficeBookSecurityForXl,OfficeFileSecurity"
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToOutputBookBuiltInPropertiesToSheet()

    Dim objFilePaths As Collection
    Dim strInvestigatingBuitInPropertiesDir As String, strLogBookPath As String
    
    strInvestigatingBuitInPropertiesDir = GetCurrentOfficeFileObject().Path & "\LocalizeByKeyValues"
    
    Set objFilePaths = GetFilePathsUsingFSO(strInvestigatingBuitInPropertiesDir, "*.xlsx")

    'DebugCol objFilePaths
    
    strLogBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\BookBuiltInPropertiesLog"

    OutputBookBuiltInPropertiesToSheet objFilePaths, strLogBookPath
End Sub


'''
''' These are to be failed...
'''
''' It is seems that the Office.DocumentProperty object can not be get beyond the individual Windows process.
'''
Private Sub msubSanityTestToFailingOutputBookBuiltInPropertiesToSheet()

    Dim objFilePaths As Collection
    Dim strInvestigatingBuitInPropertiesDir As String, strLogBookPath As String
    
    strInvestigatingBuitInPropertiesDir = GetCurrentOfficeFileObject().Path & "\LocalizeByKeyValues"
    
    Set objFilePaths = GetFilePathsUsingFSO(strInvestigatingBuitInPropertiesDir, "*.xlsx")

    strLogBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\BookBuiltInPropertiesLog"

    OutputBookBuiltInPropertiesToSheet objFilePaths, strLogBookPath, True
End Sub

