Attribute VB_Name = "OfficeBookSecurityForXl"
'
'   Output office book built-in properties to a Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat,  1/Sep/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub OutputBookBuiltInPropertiesToSheet(ByRef robjBookPaths As Collection, ByVal vstrOutputLogBookPath As String, Optional ByVal vblnAllowToUseOutsideProcess As Boolean = False)

    Dim objDTCol As Collection, objFieldTitlesCol As Collection, objLogSheet As Excel.Worksheet, objDoubleStopWatch As DoubleStopWatch


    Set objDoubleStopWatch = New DoubleStopWatch
    
    With objDoubleStopWatch

        .MeasureStart

        If vblnAllowToUseOutsideProcess Then
        
            Set objDTCol = GetBuiltinDocumentPropertiesDTColFromWorkbooksWithoutPasswordByOutsideProcessApplication(robjBookPaths)
        Else
            Set objDTCol = GetBuiltinDocumentPropertiesDTColFromWorkbooksWithoutPasswordByThisProcessApplication(robjBookPaths)
        End If
        
        .MeasureInterval
    End With


    Set objFieldTitlesCol = GetColFromLineDelimitedChar("FileName,Author,LastAuthor,Company")

    
    Set objLogSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputLogBookPath, "BuiltInProperites")
    
    OutputColToSheet objLogSheet, objDTCol, objFieldTitlesCol, mfobjGetDTSheetFormatterForBookBuiltInProperties()

    AddAutoShapeGeneralMessage objLogSheet, "Book BuiltIn properties" & vbNewLine & "Collecting relative data elapsed time: " & objDoubleStopWatch.ElapsedTimeByString
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetDTSheetFormatterForBookBuiltInProperties() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter

    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FileName,38,Author,19,LastAuthor,19,Company,16,Manager,16")
        
        .RecordBordersType = RecordCellsGrayAll
    End With
    
    Set mfobjGetDTSheetFormatterForBookBuiltInProperties = objDTSheetFormatter
End Function

