Attribute VB_Name = "OfficeBookSecurity"
'
'   Serve office book built-in property security
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat,  1/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Change Excel.Workbook BuiltinDocumentProperties
'**---------------------------------------------
'''
'''
'''
Public Sub ChangeBooksBuiltInProperties(ByRef robjBookPaths As Collection, _
        ByVal vstrAuthorToOverwrite As String, _
        ByVal vstrLastAuthorToOverwrite As String, _
        Optional ByVal vstrCompanyToOverwrite As String = "")
        
            
    Dim varPath As Variant, objBook As Excel.Workbook
    
    
    For Each varPath In robjBookPaths
    
        Set objBook = GetWorkbook(varPath, False)
        
        If Not objBook Is Nothing Then
    
            ChangeBuiltInProperties objBook, vstrAuthorToOverwrite, vstrLastAuthorToOverwrite, vstrCompanyToOverwrite
            
            objBook.Close
        End If
    Next
End Sub

'**---------------------------------------------
'** Get data-table collection from Excel.Workbook BuiltinDocumentProperties
'**---------------------------------------------
'''
'''
'''
Public Function GetBuiltinDocumentPropertiesDTColFromWorkbooksWithoutPasswordByThisProcessApplication(ByRef robjBookPaths As Collection) As Collection

    Dim objExcelApplication As Excel.Application

    Set objExcelApplication = GetOfficeApplicationObject("Excel")

    Set GetBuiltinDocumentPropertiesDTColFromWorkbooksWithoutPasswordByThisProcessApplication = GetBuiltinDocumentPropertiesDTColFromWorkbooksWithoutPassword(robjBookPaths, objExcelApplication, New Scripting.FileSystemObject)
End Function

'''
''' This is failed...
'''
Public Function GetBuiltinDocumentPropertiesDTColFromWorkbooksWithoutPasswordByOutsideProcessApplication(ByRef robjBookPaths As Collection) As Collection

    Dim objApplication As Excel.Application, objDTCol As Collection
    
    ' It is seems that the Office.DocumentProperty can not be get beyond the individual process.
    
    ' These are to be failed all...
    
    Set objApplication = CreateObject("Excel.Application")

    With objApplication
    
        '.Visible = False
        
        .Visible = True
    
        Set objDTCol = GetBuiltinDocumentPropertiesDTColFromWorkbooksWithoutPassword(robjBookPaths, objApplication)
    
        .Quit
    End With

    Set GetBuiltinDocumentPropertiesDTColFromWorkbooksWithoutPasswordByOutsideProcessApplication = objDTCol
End Function

'''
'''
'''
Public Function GetBuiltinDocumentPropertiesDTColFromWorkbooksWithoutPassword(ByRef robjBookPaths As Collection, _
        ByRef robjApplication As Excel.Application, _
        Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing) As Collection
        
        
    Dim objBookPathToPasswordDic As Scripting.Dictionary, varPath As Variant
    
    Set objBookPathToPasswordDic = New Scripting.Dictionary

    For Each varPath In robjBookPaths
    
        objBookPathToPasswordDic.Add varPath, ""
    Next
    
    Set GetBuiltinDocumentPropertiesDTColFromWorkbooksWithoutPassword = GetBuiltinDocumentPropertiesDTColFromWorkbooks(objBookPathToPasswordDic, robjApplication, vobjFS)
End Function

'''
'''
'''
Public Function GetBuiltinDocumentPropertiesDTColFromWorkbooks(ByRef robjBookPathToPasswordDic As Scripting.Dictionary, _
        ByRef robjApplication As Excel.Application, _
        Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing) As Collection


    Dim objFS As Scripting.FileSystemObject, objDTCol As Collection, objRowCol As Collection, varPath As Variant, strPath As String, strPassword As String
    Dim objProperty As Office.DocumentProperty
    Dim objBook As Excel.Workbook, strAuthor As String, strLastAuthor As String, strCompany As String
    
    
    If vobjFS Is Nothing Then
    
        Set objFS = New Scripting.FileSystemObject
    Else
        Set objFS = vobjFS
    End If
    
    Set objDTCol = New Collection
    
    With robjBookPathToPasswordDic
    
        For Each varPath In .Keys

            strPath = varPath
            
            If objFS.FileExists(strPath) Then
            
                strPassword = .Item(varPath)
                
                On Error Resume Next
                
                If strPassword = "" Then
                
                    Set objBook = robjApplication.Workbooks.Open(strPath)
                Else
                    Set objBook = robjApplication.Workbooks.Open(strPath, Password:=strPassword)
                End If
                
                Set objRowCol = New Collection
                
                With objRowCol
                
                    .Add objFS.GetFileName(strPath)
                
                    Set objProperty = objBook.BuiltinDocumentProperties.Item("Author")
                
                    .Add objProperty.Value
                    
                    Set objProperty = objBook.BuiltinDocumentProperties.Item("Last Author")
                    
                    .Add objProperty.Value
                
                    Set objProperty = objBook.BuiltinDocumentProperties.Item("Company")
                
                    .Add objProperty.Value
                    
                    Set objProperty = objBook.BuiltinDocumentProperties.Item("Manager")
                    
                    .Add objProperty.Value
                End With
                
                objDTCol.Add objRowCol
                
                If Err.Number <> 0 Then
                
                    'If Not robjApplication.Visible Then robjApplication.Visible = True
                End If
                
                On Error GoTo 0
                
                If Not objBook Is Nothing Then
                
                    On Error Resume Next
                    
                    objBook.Close
                    
                    On Error GoTo 0
                End If
            End If
        Next
    End With

    Set GetBuiltinDocumentPropertiesDTColFromWorkbooks = objDTCol
End Function


'**---------------------------------------------
'** Change Excel.Workbook ReadOnlyRecommended
'**---------------------------------------------
'''
'''
'''
Public Sub RemoveReadOnlyAttribute(ByVal vstrPath As String)
    
    With New Scripting.FileSystemObject
    
        If .FileExists(vstrPath) Then
        
            With .GetFile(vstrPath)
                
                If (.Attributes And vbReadOnly) > 0 Then
                
                    .Attributes = .Attributes And (Not vbReadOnly)
                End If
            End With
        End If
    End With
End Sub


'''
'''
'''
Public Sub RemoveReadOnlyRecommendedAndReadOnlyAttribute(ByVal vstrBookPath As String)
    
    Dim objApplication As Excel.Application, blnOldDisplayAlerts As Boolean, blnOldScreenUpdating As Boolean
    Dim objBook As Excel.Workbook
    
    
    Set objApplication = GetCurrentOfficeFileObject().Application
    
    With objApplication
    
        blnOldDisplayAlerts = .DisplayAlerts
        
        blnOldScreenUpdating = .ScreenUpdating
        
        .DisplayAlerts = False
        
        .ScreenUpdating = False
    End With

    On Error Resume Next
    
    With New Scripting.FileSystemObject
    
        If .FileExists(vstrBookPath) Then
        
            Set objBook = objApplication.Workbooks.Open(vstrBookPath, IgnoreReadOnlyRecommended:=True)
            
            If Not objBook Is Nothing Then
            
                If objBook.ReadOnlyRecommended Then
                
                    If StrComp(LCase(.GetExtensionName(vstrBookPath)), "xlsm") = 0 Then
                    
                        objBook.SaveAs vstrBookPath, FileFormat:=xlOpenXMLWorkbookMacroEnabled, ReadOnlyRecommended:=False
                    Else
                        objBook.SaveAs vstrBookPath, ReadOnlyRecommended:=False
                    End If
                End If
                
                objBook.Close
            End If
            
            Set objBook = Nothing
        End If
    End With
    
    On Error GoTo 0
    
    With objApplication
    
        .DisplayAlerts = blnOldDisplayAlerts
        
        .ScreenUpdating = blnOldScreenUpdating
    End With

    RemoveReadOnlyAttribute vstrBookPath
End Sub




