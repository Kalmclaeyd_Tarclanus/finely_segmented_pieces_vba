Attribute VB_Name = "SqlUTfPgSqlOdbcSheetExpander"
'
'   Sanity test to output SQL results into Excel sheets for a PostgreSQL database using ADO ODBC
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'       Dependent on both PsqlOdbc driver at this Windows and an PostgreSQL database server in either a network somewhere or the local-host
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrConStrInfoSettingKeyNameOfDSN As String = "TestPgSqlDSNKey"

Private Const mstrConStrInfoSettingKeyNameOfDSNless As String = "TestPgSqlDSNlessKey"



#If Win64 Then

    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
#Else
    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
#End If

Private Const mintPostgreSQLDefaultPortNumber As Long = 5432

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToSqlUTfPgSqlOdbcSheetExpander()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", _
            "SqlUTfPgSqlOdbcSheetExpander,SqlAdoConnectOdbcUTfPgSql,SqlAdoConnectOdbcPTfPgSql,PgSqlOdbcConnector,PgSqlOdbcSheetExpander,PgSqlCommonPath,PgSqlDSNlessTest"
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** PgSqlOdbcSheetExpander
'**---------------------------------------------
'''
''' Create an in-line table from the only SELECT sql inside
'''
Private Sub msubSanityTestToConnectByVirtualTablePgSqlOdbcSheetExpander()
    
    Dim strSQL As String

    With New PgSqlOdbcSheetExpander

        ' for default connection setting soon after the PostgreSQL typcal configuration is installed in this local computer.

        .SetODBCParametersWithoutDSN mstrDriverName, "LocalHost", "postgres", "postgres", "postgres"

        .AllowToRecordSQLLog = True

        strSQL = "select 'ABC' as A_Column, 'DEF' as B_Column"

        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
        
        .CloseAll
    End With
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Use IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm interface for ADO connecting
'**---------------------------------------------
'''
''' get PostgreSQL version by SQL
'''
Private Sub msubSanityTestToOutputSqlVersionOnSheetAfterSqlVersionOfPostgreSQL()
    
    Dim strSQL As String
    
    With New PgSqlOdbcSheetExpander

        ' for default connection setting soon after the PostgreSQL typcal configuration is installed in this local computer.

        If .IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(mstrConStrInfoSettingKeyNameOfDSNless, , "LocalHost", "postgres", "postgres") Then

            .AllowToRecordSQLLog = True
    
            strSQL = "select * from version()"
    
            .OutputInExistedBookByUnitTestMode strSQL, "PgSQL_version"
            
            .CloseCacheOutputBookOfOpenByPresetModeProcess
        Else
            Debug.Print "The ADO connection information inputs have canceled."
        End If
    End With
End Sub

'**---------------------------------------------
'** Use presetting object for ADO connecting
'**---------------------------------------------
'''
''' SQL insert test with output Excel sheet log
'''
Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLInsertExecution()

    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
    
    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()

    If Not objAdoSheetExpander Is Nothing Then
    
        With objAdoSheetExpander
    
            strSQL = "insert into Test_Table values (13, 15, 16, 'Type3')"
        
            .OutputCommandLogToSpecifiedBook strSQL, GetPgSqlConnectingPrearrangedLogBookPath()
        
            .ADOConnectorInterface.CommitTransaction
            
            strSQL = "select * from Test_Table"
            
            .OutputToSpecifiedBook strSQL, GetPgSqlConnectingPrearrangedLogBookPath(), "SqlResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
            
            .CloseAll
        End With
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If
End Sub

'''
''' SQL delete test with output Excel sheet log
'''
Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLDeleteExecution()

    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander, strOutputBookPath As String, strDir As String
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
    
    strDir = GetCurrentBookOutputDir() & "\SQLTests\FromPgSqlConnect"
    
    ForceToCreateDirectory strDir
    
    strOutputBookPath = strDir & "\AdoConnectToPgSqlTest01.xlsx"
    
    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()

    If Not objAdoSheetExpander Is Nothing Then
    
        With objAdoSheetExpander
    
            strSQL = "delete from Test_Table where ColText = 'Type2'"
        
            .OutputCommandLogToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath
        
            .ADOConnectorInterface.CommitTransaction
            
            strSQL = "select * from Test_Table"
            
            .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath, "SqlResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
            
            .CloseAll
        End With
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If
End Sub

'''
''' SQL update test with output Excel sheet log
'''
Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLUpdateExecution()

    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
    
    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()

    If Not objAdoSheetExpander Is Nothing Then
    
        With objAdoSheetExpander
    
            strSQL = "update Test_Table set Col1 = 51, Col2 = 52 where Col3 = 9 AND ColText = 'Type2'"
        
        
            .OutputCommandLogInExistedBookByUnitTestMode strSQL
        
            .ADOConnectorInterface.CommitTransaction
            
            strSQL = "select * from Test_Table"
            
            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
            
            .CloseAll
        End With
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If
End Sub

'''
''' SQL insert test with output Excel sheet log
'''
Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLPluralInsertExecution()

    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
    
    Dim i As Long
    Dim int01 As Long, int02 As Long, int03 As Long, intType As Long
    
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
    
    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()

    If Not objAdoSheetExpander Is Nothing Then
    
        With objAdoSheetExpander
    
            For i = 1 To 15
    
                ' sample values
    
                int01 = 3 * (i + 4)
                
                int02 = int01 + 1
                
                int03 = int02 + 1
                
                intType = i + 5
    
                strSQL = "insert into Test_Table values (" & CStr(int01) & ", " & CStr(int02) & ", " & CStr(int03) & ", 'Type" & CStr(intType) & "')"
            
                .OutputCommandLogInExistedBookByUnitTestMode strSQL
            Next
        
            .ADOConnectorInterface.CommitTransaction
            
            strSQL = "select * from Test_Table"
            
            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
            
            .CloseAll
        End With
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If
End Sub


'''
''' SQL insert test with output Excel sheet log
'''
Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLPluralInsertExecutionWithOtherLoggingOption()

    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
    
    Dim i As Long
    Dim int01 As Long, int02 As Long, int03 As Long, intType As Long
    
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
    
    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()

    If Not objAdoSheetExpander Is Nothing Then
    
        With objAdoSheetExpander
    
            For i = 1 To 15
    
                ' sample values
    
                int01 = 3 * (i + 4)
                
                int02 = int01 + 1
                
                int03 = int02 + 1
                
                intType = i + 5
    
                strSQL = "insert into Test_Table values (" & CStr(int01) & ", " & CStr(int02) & ", " & CStr(int03) & ", 'Type" & CStr(intType) & "')"
            
                .OutputCommandLogInExistedBookByUnitTestMode strSQL, NoControlToShowUpAdoSqlErrorFlag, "TestingSqlCommandLog"
            Next
        
            .ADOConnectorInterface.CommitTransaction
            
            strSQL = "select * from Test_Table"
            
            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
            
            .CloseCacheOutputBookOfOpenByPresetModeProcess
        End With
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests() As PgSqlOdbcSheetExpander

    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
    
    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo()
    
    If Not objAdoSheetExpander Is Nothing Then
    
        PrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests objAdoSheetExpander.ADOConnectorInterface
        
        objAdoSheetExpander.DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function hasn't been implemented yet."
    End If

    Set mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests = objAdoSheetExpander
End Function


'''
'''
'''
Private Function mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo() As PgSqlOdbcSheetExpander

    Dim objPgSqlOdbcSheetExpander As PgSqlOdbcSheetExpander
    
    On Error Resume Next
    
    Set objPgSqlOdbcSheetExpander = Application.Run("GetCurrentThisSytemTestingPgSqlOdbcSheetExpander")

    On Error GoTo 0

    Set mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo = objPgSqlOdbcSheetExpander
End Function

