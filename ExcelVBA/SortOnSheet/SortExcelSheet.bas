Attribute VB_Name = "SortExcelSheet"
'
'   Sort by Excel.Sheet methods
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Some algorithms should have looked like either uncompleted or naive, if you are a IT architect, a computer scientist, or a mathematician.
'       It is not a open-source project purpose that the completed algorithms are adopted in all individual processes.
'       After I have confirmed both the users' needs and my sufficient funds, I will investigate to improve the concerned algorithms.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 18/Apr/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Excel sheet sort tools
'**---------------------------------------------
'''
'''
'''
Public Sub SortXlSheetBasically(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vstrSortConditionStringFromLineDelimitedComma As String, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)


    Dim objFieldTitleToSortOrderDic As Scripting.Dictionary, objFieldTitleToPositionIndexDic As Scripting.Dictionary
    
    Dim objTopLeftRange As Excel.Range, objTableRange As Excel.Range
    
    Dim objKeyRange As Excel.Range
    
    Dim varFieldTitle As Variant, strFieldTitle As String, enmOrder As SortOrder, enmXlSortOrder As XlSortOrder, strColumnLetter As String
    
    
    Set objFieldTitleToSortOrderDic = GetTextToSortOrderDicFromLineDelimitedChar(vstrSortConditionStringFromLineDelimitedComma)

    Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(GetEnumeratorKeysColFromDic(objFieldTitleToSortOrderDic), vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
    With vobjSheet
    
        Set objTopLeftRange = .Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
    
        Set objTableRange = objTopLeftRange.CurrentRegion
    
        With .Sort
        
            .SortFields.Clear
            
            For Each varFieldTitle In objFieldTitleToSortOrderDic.Keys
            
                If objFieldTitleToPositionIndexDic.Exists(varFieldTitle) Then
            
                    strFieldTitle = varFieldTitle
                
                    enmOrder = objFieldTitleToSortOrderDic.Item(varFieldTitle)
                
                    Select Case enmOrder
                    
                        Case SortOrder.Ascending
                        
                            enmXlSortOrder = xlAscending
                            
                        Case SortOrder.Decending
                        
                            enmXlSortOrder = xlDescending
                    End Select
                
                    strColumnLetter = ConvertXlColumnIndexToLetter(objFieldTitleToPositionIndexDic.Item(strFieldTitle))
                
                    Set objKeyRange = vobjSheet.Range(strColumnLetter & "2:" & strColumnLetter & CStr(objTableRange.Count))
                
                    .SortFields.Add objKeyRange, _
                            xlSortOnValues, _
                            enmXlSortOrder, _
                            DataOption:=xlSortNormal
                End If
            Next
        
            .SetRange objTableRange
            
            .Header = xlYes
                
            .MatchCase = False
        
'            .Orientation = xlTopToBottom
'            .SortMethod = xlPinYin
            
            .Apply
        End With
    End With
End Sub



'**---------------------------------------------
'** About sorting on a Excel sheet by Excel.SortFields property
'**---------------------------------------------
'''
'''
'''
Public Sub SortSheetFromSortFieldsProperty(ByVal vobjSheet As Excel.Worksheet, _
        ByVal vobjColumnNameToSortingParametersDic As Scripting.Dictionary, _
        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
        Optional ByVal vintTopLeftColumnIndex As Long = 1)


    Dim objTopLeftRange As Excel.Range, objCurrentRegion As Excel.Range
    Dim intStartRowIndex As Long, intEndRowIndex As Long, objFieldTitleToPositionIndexDic As Scripting.Dictionary
    Dim strRange As String, strColumnLetter, objArgumentNameToParameterDic As Scripting.Dictionary, varColumnName As Variant
    
    Dim enmSortOn As Excel.XlSortOn, enmSortOrder As Excel.XlSortOrder, enmSortDataOption As Excel.XlSortDataOption, strCustomOrder As String
    
    With vobjSheet
    
        Set objTopLeftRange = .Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex + 1))
        
        Set objCurrentRegion = objTopLeftRange.CurrentRegion
    
        With objCurrentRegion
        
            intStartRowIndex = .Row + 1
            
            intEndRowIndex = .Rows.Count + .Row - 1
        End With
    
        Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(GetColFromRange(GetFieldTitlesRangeFromCurrentRegionWithPriorFieldTitleIdxThanTopLeftCell(vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)), vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
    
        With .Sort
            
            With .SortFields
            
                .Clear
                
                For Each varColumnName In vobjColumnNameToSortingParametersDic.Keys
                
                    Set objArgumentNameToParameterDic = vobjColumnNameToSortingParametersDic.Item(varColumnName)
                
                    strColumnLetter = ConvertXlColumnIndexToLetter(objFieldTitleToPositionIndexDic.Item(varColumnName))
                    
                    strRange = strColumnLetter & CInt(intStartRowIndex) & ":" & strColumnLetter & CInt(intEndRowIndex)
                
                    msubGetSortFieldsArgumentParametersFromDic enmSortOn, enmSortOrder, enmSortDataOption, strCustomOrder, objArgumentNameToParameterDic
                
                    If strCustomOrder <> "" Then
                    
                        '.Add2 Key:=vobjSheet.Range(strRange), SortOn:=enmSortOn, Order:=enmSortOrder, CustomOrder:=strCustomOrder, DataOption:=enmSortDataOption   ' This will occur error, this is probably Microsoft Excel bug
                        
                        .Add2 key:=vobjSheet.Range(strRange), _
                                SortOn:=enmSortOn, _
                                Order:=enmSortOrder, _
                                CustomOrder:="""," & strCustomOrder & ",""", _
                                DataOption:=enmSortDataOption
                    Else
                    
                        .Add2 key:=vobjSheet.Range(strRange), _
                                SortOn:=enmSortOn, _
                                Order:=enmSortOrder, _
                                DataOption:=enmSortDataOption
                    End If
                Next
            End With
        
            .SetRange objCurrentRegion
            
            .Header = xlYes
            
            .MatchCase = False
            
            .Orientation = xlTopToBottom
            
            .SortMethod = xlPinYin
            
            .Apply
        End With
    End With
End Sub


'''
'''
'''
Public Sub SetColumnNameToExcelSheetSortingParametersDic(ByRef robjColumnNameToSortingParametersDic As Scripting.Dictionary, _
        ByVal vstrColumnName As String, _
        Optional ByVal venmSortOn As Excel.XlSortOn = Excel.XlSortOn.xlSortOnValues, _
        Optional ByVal venmSortOrder As Excel.XlSortOrder = Excel.XlSortOrder.xlAscending, _
        Optional ByVal venmDataOption As Excel.XlSortDataOption = Excel.XlSortDataOption.xlSortNormal, _
        Optional ByVal vstrCustomOrder As String = "")


    Dim objArgumentNameToParameterDic As Scripting.Dictionary
    
    
    If robjColumnNameToSortingParametersDic Is Nothing Then Set robjColumnNameToSortingParametersDic = New Scripting.Dictionary
    
    With robjColumnNameToSortingParametersDic
    
        If Not .Exists(vstrColumnName) Then
        
            Set objArgumentNameToParameterDic = New Scripting.Dictionary
        
            msubSetSortFieldsArgumentParametersToDic objArgumentNameToParameterDic, _
                    venmSortOn, _
                    venmSortOrder, _
                    venmDataOption, _
                    vstrCustomOrder
            
            .Add vstrColumnName, objArgumentNameToParameterDic
        End If
    End With
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>renmSortOn: Output</Argument>
''' <Argument>renmSortOrder: Output</Argument>
''' <Argument>renmDataOption: Output</Argument>
''' <Argument>rstrCustomOrder: Output</Argument>
''' <Argument>vobjArgumentNameToParameterDic: Input</Argument>
Private Sub msubGetSortFieldsArgumentParametersFromDic(ByRef renmSortOn As Excel.XlSortOn, _
        ByRef renmSortOrder As Excel.XlSortOrder, _
        ByRef renmDataOption As Excel.XlSortDataOption, _
        ByRef rstrCustomOrder As String, _
        ByVal vobjArgumentNameToParameterDic As Scripting.Dictionary)

    With vobjArgumentNameToParameterDic
    
        If .Exists("SortOn") Then
        
            renmSortOn = .Item("SortOn")
        Else
            renmSortOn = xlSortOnValues ' Default value
        End If
    
        If .Exists("Order") Then
        
            renmSortOrder = .Item("Order")
        Else
            renmSortOrder = xlAscending ' Default value
        End If
    
        If .Exists("DataOption") Then
        
            renmDataOption = .Item("DataOption")
        Else
            renmDataOption = xlSortNormal ' Default value
        End If
    
        If .Exists("CustomOrder") Then
        
            rstrCustomOrder = .Item("CustomOrder")
        Else
            rstrCustomOrder = ""
        End If
    End With
End Sub

'''
'''
'''
''' <Argument>vobjArgumentNameToParameterDic: Output</Argument>
''' <Argument>venmSortOn: Input</Argument>
''' <Argument>venmSortOrder: Input</Argument>
''' <Argument>venmDataOption: Input</Argument>
''' <Argument>vstrCustomOrder: Input</Argument>
Private Sub msubSetSortFieldsArgumentParametersToDic(ByVal vobjArgumentNameToParameterDic As Scripting.Dictionary, _
        Optional ByVal venmSortOn As Excel.XlSortOn = Excel.XlSortOn.xlSortOnValues, _
        Optional ByVal venmSortOrder As Excel.XlSortOrder = Excel.XlSortOrder.xlAscending, _
        Optional ByVal venmDataOption As Excel.XlSortDataOption = Excel.XlSortDataOption.xlSortNormal, _
        Optional ByVal vstrCustomOrder As String = "")

    With vobjArgumentNameToParameterDic
    
        If venmSortOn <> xlSortOnValues Then
            
            .Add "SortOn", venmSortOn
        End If
    
        If venmSortOrder <> xlAscending Then
        
            .Add "Order", venmSortOrder
        End If
    
        If venmDataOption <> xlSortNormal Then
        
            .Add "DataOption", venmDataOption
        End If
    
        If vstrCustomOrder <> "" Then
        
            .Add "CustomOrder", vstrCustomOrder
        End If
    End With
End Sub



