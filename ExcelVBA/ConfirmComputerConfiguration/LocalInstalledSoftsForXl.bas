Attribute VB_Name = "LocalInstalledSoftsForXl"
'
'   Output the checked this computer local installed softwares result to Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both WMI and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 13/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub OutputInstalledApplicationInfoOfWmiConnectedThisComputerToSheet()

    Dim strOutputBookPath As String
    
    strOutputBookPath = mfstrGetInstalledSoftwaresInfoDirectoryPath() & "\InstalledSoftwareByWmi.xlsx"
    
    OutputInstalledApplicationInfoOfWmiConnectedComputerToSheet strOutputBookPath
End Sub


'''
''' This needs the Administrator authority, which WMI use Win32_InstalledWin32Program
'''
Public Sub OutputInstalledApplicationInfoOfThisComputerToSheet()

    Dim strOutputBookPath As String
    
    strOutputBookPath = mfstrGetInstalledSoftwaresInfoDirectoryPath() & "\InstalledSoftwareOfThisComputerByWmi.xlsx"
     
    OutputInstalledApplicationInfoOfWmiThisComputerToSheet strOutputBookPath
End Sub



'''
'''
'''
Public Sub OutputInstalledApplicationInfoOfWmiConnectedComputerToSheet(ByVal vstrOutputBookPath As String, _
        Optional ByVal vstrHostName As String = "localhost", _
        Optional ByVal vstrUserName As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vstrDelimitedChar As String = ",")

    Dim objDTCol As Collection, objFieldTitlesCol As Collection, objSheet As Excel.Worksheet, objLogDic As Scripting.Dictionary

    With New DoubleStopWatch

        .MeasureStart
        
        GetInstalledApplicationInfoForWmiConnectedComputer objDTCol, objFieldTitlesCol, vstrHostName, vstrUserName, vstrPassword, vstrDelimitedChar

        Set objLogDic = New Scripting.Dictionary
        
        With objLogDic
        
            .Add "Count of detected installed softwares", objDTCol.Count
        
            .Add "Connected host name", vstrHostName
        
            .Add "User name in the connected host", vstrUserName
        End With

        .MeasureInterval
        
        objLogDic.Add "Getting list elapsed time", .ElapsedTimeByString
    End With

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "InstalledSoftListByWMI")

    OutputColToSheet objSheet, objDTCol, objFieldTitlesCol, mfobjGetDTSheetFormatterOfInstalledApplicationInfoOfWmiConnectedComputer()

    OutputDicToAutoShapeLogTextOnSheet objSheet, objLogDic, "WMI connected computer installed softwares"
End Sub



'''
''' This needs the Administrator authority, which WMI use Win32_InstalledWin32Program
'''
Public Sub OutputInstalledApplicationInfoOfWmiThisComputerToSheet(ByVal vstrOutputBookPath As String)

    Dim objDTCol As Collection, objFieldTitlesCol As Collection, objSheet As Excel.Worksheet, objLogDic As Scripting.Dictionary

    With New DoubleStopWatch

        .MeasureStart
        
        GetInstalledWin32ProgramInformationInThisComputer objDTCol, objFieldTitlesCol, ";"

        Set objLogDic = New Scripting.Dictionary
        
        With objLogDic
        
            .Add "Count of detected installed softwares", objDTCol.Count
        End With

        .MeasureInterval
        
        objLogDic.Add "Getting list elapsed time", .ElapsedTimeByString
    End With

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "InstalledSoftListByWMI")

    OutputColToSheet objSheet, objDTCol, objFieldTitlesCol, mfobjGetDTSheetFormatterOfInstalledApplicationInfoOfWmiThisComputer()

    OutputDicToAutoShapeLogTextOnSheet objSheet, objLogDic, "This computer installed softwares"
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetDTSheetFormatterOfInstalledApplicationInfoOfWmiConnectedComputer() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = New DataTableSheetFormatter

    With objDTSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Name,45,Vendor,42,HOST,12,InstallDate,16,InstallLocation,31,Version,15,IdentifyingNumber,37")
    
        .RecordBordersType = RecordCellsGrayAll
        
        
        .AllowToConvertDataForSpecifiedColumns = True
        
        Set .FieldTitlesToColumnDataConvertTypeDic = GetTextToColumnDataConvertTypeDicFromLineDelimitedChar("InstallDate,Digits8IntegersToDate")
        
        
        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
        
        With .ColumnsNumberFormatLocal
        
            .SetNumberFormatLocalAndApplyFieldTitles "ddd, yyyy/m/d", GetColFromLineDelimitedChar("InstallDate")
        End With
        
        
        .AllowToMergeCellsByContinuousSameValues = True
        
        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("Vendor,InstallDate")
    End With

    Set mfobjGetDTSheetFormatterOfInstalledApplicationInfoOfWmiConnectedComputer = objDTSheetFormatter
End Function


'''
'''
'''
Private Function mfobjGetDTSheetFormatterOfInstalledApplicationInfoOfWmiThisComputer() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = New DataTableSheetFormatter

    With objDTSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Name,45,Vendor,29,Version,15,ProgramID,43")
    
        .RecordBordersType = RecordCellsGrayAll
        

        .AllowToMergeCellsByContinuousSameValues = True

        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True

        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("Vendor,Version")
    End With

    Set mfobjGetDTSheetFormatterOfInstalledApplicationInfoOfWmiThisComputer = objDTSheetFormatter
End Function

'''
'''
'''
Private Function mfstrGetInstalledSoftwaresInfoDirectoryPath() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\InstalledSoftwaresInfo"
    
    ForceToCreateDirectory strDir
    
    mfstrGetInstalledSoftwaresInfoDirectoryPath = strDir
End Function




