Attribute VB_Name = "WinRegViewForXl"
'
'   Output the read Windows 32 bit registries data-table, such as Collection or Scripting.Dictionary ,to Excel-sheet.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 31/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
'**---------------------------------------------
'** Solute file-system path by the Windows registry
'**---------------------------------------------
Private Const mstrModuleKey As String = "WinRegViewForXl"   ' 1st part of registry sub-key

Private Const mstrSubjectNetworkDriveRootDirKey As String = "UTfNetworkDriveRootDir"   ' 2nd part of registry sub-key, recommend network drive

Private Const mstrSubjectWinRegReadOutputBookPathKey As String = "WinRegReadOutputBookPath"


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToWinRegViewForXl()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "WinRegView,WinRegManagementDeclarations,UTfWinAPIRegEnumForXl,UTfWinAPIRegistryRW"
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About read Windows-registory key-value Dictionary
'**---------------------------------------------
'''
'''
'''
Public Function OutputWinRegValuesOneNestedDicInSpecifiedWinRegistryKeyToBook(ByVal vstrFullRegKey As String, _
        ByVal vobjReadOneNestedDic As Scripting.Dictionary, _
        ByVal vstrOutputBookPath As String, _
        Optional ByVal vstrFieldTitleToColumnWidthDelimitedByComma As String = "LastChildSubKeyName,17,Key,15,Value,25") As Excel.Worksheet


    Dim objSheet As Excel.Worksheet

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "WinReg_" & Left(GetLastChildRegKeyName(vstrFullRegKey), 24))

    OutputOneNestedDicToSheetRestrictedByDataTableSheetFormatter objSheet, vobjReadOneNestedDic, mfobjGetDataTableSheetFormatterForOneNestedWinRegKeyValueDic(vstrFieldTitleToColumnWidthDelimitedByComma)

    AddAutoShapeGeneralMessage objSheet, mfstrGetLogOfWinRegOneNestedKeysAndValues(objSheet, vstrFullRegKey)
    
    Set OutputWinRegValuesOneNestedDicInSpecifiedWinRegistryKeyToBook = objSheet
End Function

'''
'''
'''
Public Function OutputWinRegValuesDicInSpecifiedWinRegistryKeyToBook(ByVal vstrFullRegKey As String, _
        ByVal vobjReadDic As Scripting.Dictionary, _
        ByVal vstrOutputBookPath As String) As Excel.Worksheet


    Dim objSheet As Excel.Worksheet

    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "WinReg_" & Left(GetLastChildRegKeyName(vstrFullRegKey), 24))

    OutputDicToSheetRestrictedByDataTableSheetFormatter objSheet, vobjReadDic, mfobjGetDataTableSheetFormatterForWinRegKeyValuesDicInAKey()

    AddAutoShapeGeneralMessage objSheet, mfstrGetLogOfWinRegKeyValue(vstrFullRegKey, vobjReadDic)
    
    Set OutputWinRegValuesDicInSpecifiedWinRegistryKeyToBook = objSheet
End Function


'**---------------------------------------------
'** Output results to Excel book
'**---------------------------------------------
'''
''' using GetWinRegistoriesInfomationBookPath
'''
Public Sub OutputRegValuesWithChildSubKeysToSheet(ByVal vstrFullSubKey As String, _
        Optional ByVal venmRegCollectTargetFlag As RegCollectTargetFlag = (RegCollectTargetFlag.CollectWinRegSubKeys Or RegCollectTargetFlag.CollectWinRegValues), _
        Optional ByVal vblnNoDeletePreviousSheets As Boolean = False, _
        Optional ByVal vstrSubKeysSheetName As String = "SubKeys", _
        Optional ByVal vstrRegValuesSheetName As String = "RegValues", _
        Optional ByVal vstrAdditionalLogMessage As String = "")

    Dim strOutputBookPath As String
    
    strOutputBookPath = GetWinRegistoriesInfomationBookPath()

    GetRegValuesWithChildSubKeysAndOutExcelSheet vstrFullSubKey, strOutputBookPath, venmRegCollectTargetFlag, vblnNoDeletePreviousSheets, vstrSubKeysSheetName, vstrRegValuesSheetName, vstrAdditionalLogMessage
End Sub


'''
''' list sub-keys up on Excel sheet
'''
Public Sub OutputWinRegSubKeysToSheet(ByVal vstrFullSubKey As String, _
        Optional ByVal vblnNoDeletePreviousSheets As Boolean = False, _
        Optional ByVal vstrSubKeysShName As String = "SubKeys", _
        Optional ByVal vstrAddLogMessage As String = "")

    ' only subkeys
    OutputRegValuesWithChildSubKeysToSheet vstrFullSubKey, _
            CollectWinRegSubKeys, _
            vblnNoDeletePreviousSheets, _
            vstrSubKeysSheetName:=vstrSubKeysShName, _
            vstrAdditionalLogMessage:=vstrAddLogMessage
End Sub

'''
''' list registry values up on Excel sheet
'''
Public Sub OutputWinRegValuesToSheet(ByVal vstrFullSubKey As String, _
        Optional ByVal vblnNoDeletePreviousSheets As Boolean = False, _
        Optional ByVal vstrRegValuesShName As String = "RegValues", _
        Optional ByVal vstrAddLogMessage As String = "")

    ' only registry values
    OutputRegValuesWithChildSubKeysToSheet vstrFullSubKey, _
            CollectWinRegValues, _
            vblnNoDeletePreviousSheets, _
            vstrRegValuesSheetName:=vstrRegValuesShName, _
            vstrAdditionalLogMessage:=vstrAddLogMessage
End Sub


'**---------------------------------------------
'** Output Excel sheet by loading Windows Regstries
'**---------------------------------------------
'''
''' get registry values and sub-keys and output Excel specified test sheet
'''
Public Sub GetRegValuesWithChildSubKeysAndOutExcelSheet(ByVal vstrFullSubKey As String, _
        ByVal vstrOutputBookPath As String, _
        Optional venmRegCollectTargetFlag As RegCollectTargetFlag = (RegCollectTargetFlag.CollectWinRegSubKeys Or RegCollectTargetFlag.CollectWinRegValues), _
        Optional vblnNoDeletePreviousSheets As Boolean = False, _
        Optional vstrSubKeysSheetName As String = "SubKeys", _
        Optional vstrRegValuesSheetName As String = "RegValues", _
        Optional vstrAdditionalLogMessage As String = "")
    
    
    Dim intRootKey As Long, strSubKey As String, objSubKeys As Collection, objRegValues As Collection
    
    Dim objRowItems As Collection, intHierarchicalOrder As Long, objBook As Excel.Workbook
    
    Dim objSheet As Excel.Worksheet
    
    
    Dim objDoubleStopWatch As DoubleStopWatch
    
    
    If vblnNoDeletePreviousSheets Then
    
        Set objBook = GetWorkbook(vstrOutputBookPath, False)
    Else
        Set objBook = GetWorkbookAndPrepareForUnitTest(vstrOutputBookPath)
    End If
    
    SeparateRegistryRootKeyAndSubKey vstrFullSubKey, intRootKey, strSubKey
    
    intHierarchicalOrder = UBound(Split(strSubKey, "\")) + 1
    
    If (venmRegCollectTargetFlag And CollectWinRegSubKeys) > 0 Then
        ' set first subkey item
        Set objSubKeys = New Collection
    
        If IsWinFullRegKeyExisted(vstrFullSubKey) Then
    
            Set objRowItems = New Collection
            
            With objRowItems
                
                .Add intHierarchicalOrder
                
                .Add vstrFullSubKey
            End With
            
            objSubKeys.Add objRowItems
        Else
            Debug.Print "Not exist, searching registry key base: " & vstrFullSubKey
        End If
    End If
    
    If (venmRegCollectTargetFlag And CollectWinRegValues) > 0 Then
    
        Set objRegValues = New Collection
    End If

    Set objDoubleStopWatch = New DoubleStopWatch

    With objDoubleStopWatch
    
        .MeasureStart
      
        GetRegValuesWithChildSubKeys intHierarchicalOrder + 1, intRootKey, strSubKey, objSubKeys, objRegValues, True, venmRegCollectTargetFlag

        .MeasureInterval
    End With
    
    If (venmRegCollectTargetFlag And CollectWinRegSubKeys) > 0 Then
        
        Set objSheet = mfobjExtractSubKeysColToSheet(objSubKeys, GetColFromLineDelimitedChar("HierarchicalOrder,KeyFullPath"), objBook, vstrSubKeysSheetName)
        
        msubAddRegistrySearchLogOfSubKeys objSheet, objDoubleStopWatch.ElapsedSecondTime
    End If
    
    If (venmRegCollectTargetFlag And CollectWinRegValues) > 0 Then
        
        Set objSheet = mfobjExtractRegValuesColToSheet(objRegValues, GetColFromLineDelimitedChar("HierarchicalOrder,RegName,RegValue,RegType,KeyFullPath"), objBook, vstrRegValuesSheetName)
        
        msubAddRegistrySearchLogOfRegValues objSheet, objDoubleStopWatch.ElapsedSecondTime
    End If
    
    If vstrAdditionalLogMessage <> "" And Not objSheet Is Nothing Then
    
        AddAutoShapeGeneralMessage objSheet, vstrAdditionalLogMessage
    End If
    
    DeleteDummySheetWhenItExists objBook
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** View this Excel VBA user's registry-key and values
'**---------------------------------------------
Private Sub msubViewCurrentRegUNCPathOfAll()

    ' lists of subkeys and registry values
    OutputRegValuesWithChildSubKeysToSheet strRegKeySecureExcelVBAUNCPathPos
End Sub



'''
'''
'''
Private Function mfstrGetLogOfWinRegOneNestedKeysAndValues(ByRef robjSheet As Excel.Worksheet, _
        ByRef rstrFullRegKey As String) As String


    Dim intRowsCount As Long, intColumnsCount As Long, strLoggedTimeStamp As String, strLog As String

    GetCountsOfRowsColumnsFromSheet robjSheet, intRowsCount, intColumnsCount, strLoggedTimeStamp
    
    strLog = "An one-nested Dictionary by reading Windows-registry sub-keys and values" & vbNewLine
    
    strLog = strLog & "Full Windows-registry key: " & rstrFullRegKey & vbNewLine
    
    strLog = strLog & "Count of key-values: " & CStr(intRowsCount) & vbNewLine
    
    strLog = strLog & "Read Windows-registry time: " & strLoggedTimeStamp

    mfstrGetLogOfWinRegOneNestedKeysAndValues = strLog
End Function

'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterForOneNestedWinRegKeyValueDic(Optional ByVal vstrFieldTitleToColumnWidthDelimitedByComma As String = "LastChildSubKeyName,17,Key,15,Value,25") As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = GetOneNestedKeyValueDicDataTableSheetFormatter(vstrFieldTitleToColumnWidthDelimitedByComma)
    
    With objDTSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfLoadedWinRegistryAsTable
        
        .RecordBordersType = RecordCellsGrayAll
    End With

    Set mfobjGetDataTableSheetFormatterForOneNestedWinRegKeyValueDic = objDTSheetFormatter
End Function


'''
'''
'''
Private Function mfstrGetLogOfWinRegKeyValue(ByRef rstrFullRegKey As String, ByRef robjDic As Scripting.Dictionary) As String

    Dim strLog As String
    
    strLog = "A dictionary by reading a specified Windows-registry key" & vbNewLine
    
    strLog = strLog & "Full Windows-registry key: " & rstrFullRegKey & vbNewLine
    
    strLog = strLog & "Count of key-values: " & CStr(robjDic.Count) & vbNewLine
    
    strLog = strLog & "Read Windows-registry time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
    
    mfstrGetLogOfWinRegKeyValue = strLog
End Function

'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterForWinRegKeyValuesDicInAKey() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = GetKeyValueDataTableSheetFormatter
    
    With objDTSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfLoadedWinRegistryAsTable
    
        .RecordBordersType = RecordCellsGrayAll
    End With

    Set mfobjGetDataTableSheetFormatterForWinRegKeyValuesDicInAKey = objDTSheetFormatter
End Function


'''
'''
'''
''' <Return>Worksheet</Return>
Private Function mfobjExtractSubKeysColToSheet(ByVal vobjDTCol As Collection, ByVal vobjFieldTitlesCol As Collection, _
        ByVal vobjLogBook As Excel.Workbook, _
        ByVal vstrSheetName As String) As Excel.Worksheet

    Dim objSheet As Excel.Worksheet
    Dim objDataTableSheetFormatter As DataTableSheetFormatter


    Set objDataTableSheetFormatter = New DataTableSheetFormatter
    
    With objDataTableSheetFormatter
        
        .FieldTitleInteriorType = ColumnNameInteriorOfLoadedWinRegistryAsTable
        
        .RecordBordersType = RecordCellsGrayAll
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("HierarchicalOrder,6,KeyFullPath,105")
    End With

    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(vobjLogBook)
    
    objSheet.Name = vstrSheetName

    OutputColToSheet objSheet, vobjDTCol, vobjFieldTitlesCol, objDataTableSheetFormatter

    Set mfobjExtractSubKeysColToSheet = objSheet
End Function


Private Sub msubAddRegistrySearchLogOfSubKeys(ByVal vobjSheet As Worksheet, ByVal vdblElapsedTime As Double)
    
    Dim intRowsCount As Long, intColumnsCount As Long, strLoggedTimeStamp As String
    Dim strSearchLog As String, objDecorationSettings As Collection


    GetCountsOfRowsColumnsFromSheet vobjSheet, intRowsCount, intColumnsCount, strLoggedTimeStamp


    strSearchLog = "Sub-keys searching of Windows Registry" & vbNewLine
    
    strSearchLog = strSearchLog & "Searched time: " & strLoggedTimeStamp & vbNewLine
    
    strSearchLog = strSearchLog & "Seaching elapsed time: " & Format(CDbl(vdblElapsedTime), "0.000") & " [s]" & vbNewLine
    
    strSearchLog = strSearchLog & "Rows Count: " & intRowsCount & vbNewLine
    
    strSearchLog = strSearchLog & "Columns Count: " & intColumnsCount
    
    Set objDecorationSettings = mfobjGetDecorationSettingsForRegistrySearchResults()
    
    AddAutoShapeGeneralMessage vobjSheet, strSearchLog, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfWinRegistry, vobjDecorationTagValueSettings:=objDecorationSettings
End Sub


'''
'''
'''
''' <Return>Excel.Worksheet</Return>
Private Function mfobjExtractRegValuesColToSheet(ByVal vobjDTCol As Collection, ByVal vobjFieldTitlesCol As Collection, ByVal vobjLogBook As Excel.Workbook, ByVal vstrSheetName As String) As Excel.Worksheet

    Dim objSheet As Excel.Worksheet
    Dim objDataTableSheetFormatter As DataTableSheetFormatter

    Set objDataTableSheetFormatter = New DataTableSheetFormatter
    
    With objDataTableSheetFormatter
        
        .FieldTitleInteriorType = ColumnNameInteriorOfLoadedWinRegistryAsTable
        
        .RecordBordersType = RecordCellsGrayAll
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("HierarchicalOrder,6,RegName,22,RegValue,82,RegType,12.5,KeyFullPath,105")
    End With

    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(vobjLogBook)
    
    objSheet.Name = vstrSheetName

    OutputColToSheet objSheet, vobjDTCol, vobjFieldTitlesCol, objDataTableSheetFormatter

    Set mfobjExtractRegValuesColToSheet = objSheet
End Function

Private Sub msubAddRegistrySearchLogOfRegValues(ByVal vobjSheet As Excel.Worksheet, ByVal vdblElapsedTime As Long)
    
    Dim intRowsCount As Long, intColumnsCount As Long, strLoggedTimeStamp As String
    Dim strSearchLog As String


    GetCountsOfRowsColumnsFromSheet vobjSheet, intRowsCount, intColumnsCount, strLoggedTimeStamp

    strSearchLog = "Windows Registry values searching" & vbNewLine
    
    strSearchLog = strSearchLog & "Searched time: " & strLoggedTimeStamp & vbNewLine
    
    strSearchLog = strSearchLog & "Seaching elapsed time: " & Format(CDbl(vdblElapsedTime), "0.000") & " [s]" & vbNewLine
    
    strSearchLog = strSearchLog & "Rows Count: " & intRowsCount & vbNewLine
    
    strSearchLog = strSearchLog & "Columns Count: " & intColumnsCount
    
    AddAutoShapeGeneralMessage vobjSheet, strSearchLog, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfWinRegistry, vobjDecorationTagValueSettings:=mfobjGetDecorationSettingsForRegistrySearchResults()
End Sub

'''
'''
'''
Private Function mfobjGetDecorationSettingsForRegistrySearchResults() As Collection

    Dim objDecorationSettings As Collection
    
    Set objDecorationSettings = New Collection
    
    With objDecorationSettings

        .Add "Searched time:" & ";" & vbNewLine & ";10;trRGBWhiteFontColor; "
        
        .Add "Seaching elapsed time:" & ";" & "[s]" & ";14;trRGBPaleBlueFontColor; "
        
        .Add "Rows Count:" & ";" & vbLf & ";14;trRGBWhiteFontColor; "
        
        .Add "Columns Count:" & ";" & vbLf & ";14;trRGBWhiteFontColor; "
    End With

    Set mfobjGetDecorationSettingsForRegistrySearchResults = objDecorationSettings
End Function

'///////////////////////////////////////////////
'/// Operations - a UNC path portability solution
'///////////////////////////////////////////////
'**---------------------------------------------
'** set user-specified path
'**---------------------------------------------
'''
''' Individual-Set-Registy
'''
Public Sub ISRInUTfWinAPIRegEnumOfTestOutputBookPath(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectWinRegReadOutputBookPathKey, vstrTargetPath
End Sub
Public Sub IDelRInUTfWinAPIRegEnumOfTestOutputBookPath()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectWinRegReadOutputBookPathKey
End Sub


'**---------------------------------------------
'** solute path generaly
'**---------------------------------------------
'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetWinRegistoriesInfomationBookPath() As String
    
    Dim strTargetPath As String, strModuleNameAndSubjectKey As String
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectWinRegReadOutputBookPathKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If strTargetPath = "" Then
    
        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForWinRegistoriesInfomationBookPath())
    End If
    
    GetWinRegistoriesInfomationBookPath = strTargetPath
End Function

'**---------------------------------------------
'** generate RegParamOfStorageUNCPath object
'**---------------------------------------------
'''
''' get RegParamOfStorageUNCPath object of GetWinRegistoriesInfomationBookPath
'''
Private Function mfobjGetRegParamOfStorageUNCPathColForWinRegistoriesInfomationBookPath() As Collection
    
    Dim objCol As Collection, strSubDirectoryPath As String
    
    Set objCol = Nothing
    
    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForWinRegistoriesInfomationBookPath")
    
    If objCol Is Nothing Then   ' If interface cannot be soluted
        
        With New WshNetwork
            
            strSubDirectoryPath = "VBAWorks\" & .UserName & "\WinRegistryLoaded"
        End With
    
        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCFilePath("ReadRegistriesInfo.xlsx", StoragePathSearchingFlag.NetWorkOrLocalAsFile, strSubDirectoryPath, strSubDirectoryPath))
    End If

    Set mfobjGetRegParamOfStorageUNCPathColForWinRegistoriesInfomationBookPath = objCol
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfGetWinRegistoriesInfomationBookPath()

    Debug.Print GetWinRegistoriesInfomationBookPath()
End Sub

'''
'''
'''
Private Sub msubSanityTestOfDeleteRegWinRegistoriesInfomationBookPath()

    ClearCacheOfModuleNameAndSubjectKeyToCurrentPathDic

    IDelRInUTfWinAPIRegEnumOfTestOutputBookPath
End Sub



