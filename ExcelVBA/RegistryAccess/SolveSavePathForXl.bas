Attribute VB_Name = "SolveSavePathForXl"
'
'   utilities to solute this VB project system common paths for Excel objects.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'       Dependent on WinRegStorageUNCPath.bas, InitRegParamOfStorageUNCPath.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat,  5/Aug/2023    Kalmclaeyd Tarclanus    Separated from SolveSavePathGeneral.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Considering removable-disk
'**---------------------------------------------
'''
'''
'''
Public Function GetTemporaryOutputBookDirWithConsideringRemovableDiskFromBook(ByVal vobjAnotherBook As Excel.Workbook) As String

    GetTemporaryOutputBookDirWithConsideringRemovableDiskFromBook = GetTemporaryOutputBookDirWithConsideringRemovableDisk(vobjAnotherBook.FullName)
End Function

'''
'''
'''
Public Function GetTemporaryOutputBookDirWithConsideringRemovableDisk(ByVal vstrAnotherInputBookPath As String) As String

    Dim objDrive As Scripting.Drive, strOutputTemporaryBookDir As String
    
    With New Scripting.FileSystemObject
    
        strOutputTemporaryBookDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
    
        If .GetParentFolderName(vstrAnotherInputBookPath) <> "" Then
    
            Set objDrive = .GetDrive(.GetDriveName(vstrAnotherInputBookPath))
            
            If objDrive.DriveType = Removable Then
            
                strOutputTemporaryBookDir = Replace(strOutputTemporaryBookDir, .GetDriveName(strOutputTemporaryBookDir), .GetDriveName(vstrAnotherInputBookPath))
            End If
        End If
    End With

    GetTemporaryOutputBookDirWithConsideringRemovableDisk = strOutputTemporaryBookDir
End Function



'**---------------------------------------------
'** common path solutions
'**---------------------------------------------
'''
''' solute path
'''
Public Sub GetInstaneouslyDecidedTemporaryBookAndPath(ByRef robjBook As Excel.Workbook, ByRef rstrPath As String, ByVal vstrTemporaryDirectoryName As String, ByVal vstrFileBaseName As String, ByVal vstrRootDir As String)
    
    Dim strTemporaryDir As String

    strTemporaryDir = vstrRootDir & "\" & vstrTemporaryDirectoryName
    
    ForceToCreateDirectory strTemporaryDir
    
    rstrPath = strTemporaryDir & "\" & vstrFileBaseName & ".xlsx"

    Set robjBook = GetWorkbookAndPrepareForUnitTest(rstrPath)
End Sub

'''
'''
'''
Public Sub GetInstaneouslyDecidedLogBookAndPathWithDate(ByRef robjBook As Excel.Workbook, ByRef rstrPath As String, ByVal vstrTemporaryDirectoryName As String, ByVal vstrFileBaseName As String, ByVal vudtDate As Date, ByVal vstrRootDir As String)
    
    Dim strTemporaryDir As String

    strTemporaryDir = vstrRootDir & "\" & vstrTemporaryDirectoryName
    
    ForceToCreateDirectory strTemporaryDir
    
    rstrPath = strTemporaryDir & "\" & vstrFileBaseName & "_" & Format(vudtDate, "mmdd") & ".xlsx"

    Set robjBook = GetWorkbookAndPrepareForUnitTest(rstrPath)
End Sub

'''
'''
'''
Public Sub GetInstaneouslyDecidedLoggingBookAndPath(ByRef robjBook As Workbook, _
        ByRef rstrPath As String, _
        ByVal vstrLoggingDirectoryName As String, _
        ByVal vstrFileBaseName As String, _
        ByVal vstrRootDir As String)
    
    Dim strLoggingDir As String

    strLoggingDir = vstrRootDir & "\" & Format(Now(), "yyyymmdd") & "\" & vstrLoggingDirectoryName
    
    ForceToCreateDirectory strLoggingDir
    
    rstrPath = strLoggingDir & "\" & vstrFileBaseName & "_" & Format(Now(), "yyyymmdd") & ".xlsx"

    Set robjBook = GetWorkbookAndPrepareForUnitTest(rstrPath)
End Sub


Public Sub SaveAsInstaneouslyDecidedLoggingBookAndPath(ByVal vobjBook As Workbook, ByVal vstrLoggingDirectoryName As String, ByVal vstrFileBaseName As String, ByVal vstrRootDir As String)

    Dim strLoggingDir As String, strPath As String

    strLoggingDir = vstrRootDir & "\" & Format(Now(), "yyyymmdd") & "\" & vstrLoggingDirectoryName
    
    ForceToCreateDirectory strLoggingDir
    
    strPath = FindNewNameWhenAlreadyExist(strLoggingDir & "\" & vstrFileBaseName & "_" & Format(Now(), "yyyymmdd") & ".xlsx")
    
    ForceToSaveAsBookWithoutDisplayAlert vobjBook, strPath
    
    CloseBookWithoutDisplayAlerts vobjBook
    
    GetWorkbookIfItExists strPath
End Sub


