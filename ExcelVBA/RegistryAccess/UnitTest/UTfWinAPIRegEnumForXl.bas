Attribute VB_Name = "UTfWinAPIRegEnumForXl"
'
'   Sanity tests about WinRegViewForXl
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 31/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleKey As String = "UTfWinAPIRegEnum"   ' 1st part of registry sub-key


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' sanity test of GetRegSubKeys
'''
Private Sub msubSanityTestOfGetRegSubKeysOfCurrentVersion()
    
    Dim strFullSubKey As String
    
    strFullSubKey = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion"

    ' only subkeys list
    OutputWinRegSubKeysToSheet strFullSubKey
End Sub

'''
''' sanity test of GetRegValues
'''
Private Sub msubSanityTestToGetRegValuesOfCurrentVersion()
    
    Dim strFullSubKey As String
    
    strFullSubKey = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion"

    ' only registry values list
    OutputWinRegValuesToSheet strFullSubKey
End Sub


'''
''' about "HKEY_CURRENT_USER\Software\VB and VBA Program Settings"
'''
Private Sub msubSanityTestToGetRegSubKeysOfVBandVBAProgramSettings()
    
    Dim strFullSubKey As String
    
    strFullSubKey = "HKEY_CURRENT_USER\Software\VB and VBA Program Settings"

    ' only subkeys list
    OutputWinRegSubKeysToSheet strFullSubKey
End Sub



'''
''' reg-values about VB and VBA Program Settings
'''
Private Sub msubSanityTestToGetRegValuesOfVBandVBAProgramSettings()

    Dim strFullSubKey As String
    
    strFullSubKey = "HKEY_CURRENT_USER\Software\VB and VBA Program Settings"

    ' only registry values list
    OutputWinRegValuesToSheet strFullSubKey
End Sub



'''
''' sub-keys and reg-values about VB and VBA Program Settings
'''
Private Sub msubSanityTestToGetSubKeysAndRegValuesOfVBandVBAProgramSettings()

    Dim strFullSubKey As String
    
    strFullSubKey = "HKEY_CURRENT_USER\Software\VB and VBA Program Settings"

    ' lists of subkeys and registry values
    OutputRegValuesWithChildSubKeysToSheet strFullSubKey
End Sub


Private Sub msubSanityTestToTwoSubkeys()

    Dim strFullSubKey As String
    
    strFullSubKey = "HKEY_CURRENT_USER\Software\Mozilla\NativeMessagingHosts"

    ' only subkeys list
    OutputWinRegSubKeysToSheet strFullSubKey, False, "SubKeys01A", "Test additional message for SubKeys search"

    strFullSubKey = "HKEY_CURRENT_USER\Software\Realtek"
    
    ' only registry values list
    OutputWinRegValuesToSheet strFullSubKey, True, "RegValues02B", "Test additional message for registry values search"
End Sub

