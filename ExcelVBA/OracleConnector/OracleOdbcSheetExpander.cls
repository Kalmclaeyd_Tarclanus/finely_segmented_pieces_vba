VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "OracleOdbcSheetExpander"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   output SQL results into Excel worksheets after connecting a Oracle database by ADO ODBC interface
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'       Dependent on both Oracle ODBC driver at this Windows and an Oracle database server in a network somewhere
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   Modification History:
'       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IOutputBookPath

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
#If Win64 Then

    Private Const mstrDriverName As String = "{Oracle in OraClient11g_home1}"
#Else
    Private Const mstrDriverName As String = "{Oracle in OraClient10g_home1}"
#End If

Private Const mintOracleDefaultPortNumber As Long = 1521

Private Const mintPostgreSQLDefaultPortNumber As Long = 5432

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjConnector As OracleOdbcConnector

Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjConnector = New OracleOdbcConnector
    
    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
    
    With mobjAdoRecordsetSheetExpander
    
        Set .ADOConnectorInterface = mobjConnector
    
        Set .OutputBookPathInterface = Me
        
        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQL
    End With
End Sub

'''
'''
'''
Private Sub Class_Terminate()

    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess

    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing

    Me.CloseConnection

    Set mobjAdoRecordsetSheetExpander = Nothing
    
    Set mobjConnector = Nothing
End Sub


'///////////////////////////////////////////////
'/// Properties - connoted ADO connector
'///////////////////////////////////////////////
Public Property Get SQLExecutionResult() As SQLResult

    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
End Property

Public Property Get IsSqlExecutionFailed() As Boolean

    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
End Property

Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)

    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
End Property
Public Property Get AllowToRecordSQLLog() As Boolean

    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
End Property


Public Property Get CurrentSheet() As Excel.Worksheet

    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
End Property


Public Property Get CurrentBook() As Excel.Workbook

    If Not Me.CurrentSheet Is Nothing Then
    
        Set CurrentBook = Me.CurrentSheet.Parent
    Else
        Set CurrentBook = Nothing
    End If
End Property


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter

    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
End Property


'///////////////////////////////////////////////
'/// Properties - connoted ADOConnectionSetting
'///////////////////////////////////////////////
Public Property Get ConnectSetting() As ADOConnectionSetting

    Set ConnectSetting = mobjConnector.ConnectSetting
End Property

'''
''' unit is second. [s]
'''
Public Property Let CommandTimeout(ByVal vintTimeout As Long)

    mobjConnector.CommandTimeout = vintTimeout
End Property
Public Property Get CommandTimeout() As Long

    CommandTimeout = mobjConnector.CommandTimeout
End Property

'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
'''
'''
'''
Private Function IOutputBookPath_GetPreservedOutputBookPath() As String

    IOutputBookPath_GetPreservedOutputBookPath = GetOracleConnectingPrearrangedLogBookPath()
End Function

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** set ADO connection string all parameters directly
'**---------------------------------------------
'''
''' set ODBC connection parameters by a registered Data Source Name (DSN)
'''
Public Sub SetODBCParametersByDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)

    mobjConnector.SetODBCParametersByDSN vstrDSN, vstrUID, vstrPWD
End Sub

'''
''' set ODBC connection parameters without Data Source Name (DSN) setting
'''
Public Sub SetODBCParametersWithoutDSN(ByVal vstrDriverName As String, ByVal vstrServerHostName As String, ByVal vstrNetworkServiceName As String, ByVal vstrUserid As String, ByVal vstrPassword As String, Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber)

    mobjConnector.SetODBCParametersWithoutDSN vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber
End Sub


'**---------------------------------------------
'** set ADO connection string parameters by a User-form interface
'**---------------------------------------------
'''
''' Using a User-form, set DSN connection
'''
''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
''' <Argument>vstrDSN: Input</Argument>
''' <Argument>vstrUID: Input</Argument>
''' <Argument>vstrPWD: Input</Argument>
Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDSN As String = "", _
        Optional ByVal vstrUID As String = "", _
        Optional ByVal vstrPWD As String = "") As Boolean


    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN, vstrUID, vstrPWD)
End Function

'''
''' Using a User-form, set DSN connection
'''
''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrNetworkServiceName: Input</Argument>
''' <Argument>vstrUserid: Input</Argument>
''' <Argument>vstrPassword: Input</Argument>
''' <Argument>vintPortNumber: Input</Argument>
Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDriverName As String = mstrDriverName, _
        Optional ByVal vstrServerHostName As String = "", _
        Optional ByVal vstrNetworkServiceName As String = "", _
        Optional ByVal vstrUserid As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber) As Boolean


    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber)
End Function

'''
''' Close ADO connection
'''
Public Sub CloseConnection()
    
    mobjConnector.CloseConnection
End Sub


'''
'''
'''
Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()

    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
End Sub

'''
'''
'''
Public Sub CloseAll()

    CloseConnection

    CloseCacheOutputBookOfOpenByPresetModeProcess
End Sub

'**---------------------------------------------
'** Output Recordset and logs to cells on sheet
'**---------------------------------------------
'''
''' query SQL and output result to Excel sheet
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>vobjOutputSheet: Input-Output</Argument>
''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
        ByVal vobjOutputSheet As Excel.Worksheet, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
     
     
    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, _
            vobjOutputSheet, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the Existed Excel sheet
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>vstrNewSheetName: Input</Argument>
''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, _
            vstrNewSheetName, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
'''
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>vstrNewSheetName: Input</Argument>
''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)


    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, _
            vstrNewSheetName, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
'''
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>vstrNewSheetName: Input</Argument>
''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)

    
    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, _
            vstrNewSheetName, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the specified Excel sheet
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>vstrOutputBookPath: Input</Argument>
''' <Argument>vstrNewSheetName: Input</Argument>
''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, _
            vstrOutputBookPath, _
            vstrNewSheetName, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
'''
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>vstrOutputBookPath: Input</Argument>
''' <Argument>vstrNewSheetName: Input</Argument>
''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)


    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, _
            vstrOutputBookPath, _
            vstrNewSheetName, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
''' query SQL and output result to the already specified Excel sheet
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>vstrNewSheetName: Input</Argument>
''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrNewSheetName As String, _
        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, _
            vstrNewSheetName, _
            venmExecutedAdoSqlQueryLogPositionType, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub


'''
'''
'''
''' <Argument>vstrOutputBookPath: Input</Argument>
''' <Argument>venmOutputRSetExcelBookOpenModeProcessFlag: Input</Argument>
Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)

    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, _
            venmOutputRSetExcelBookOpenModeProcessFlag
End Sub

'**---------------------------------------------
'** Excecute SQL command (UPDATE, INSERT, DELETE)
'**---------------------------------------------
'''
''' command SQL and output result-log to Excel sheet
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>vobjOutputSqlCommandLogSheet: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom _
            vstrSQL, _
            vobjOutputSqlCommandLogSheet, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>vstrOutputBookPath: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, _
            vstrOutputBookPath, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vstrNewSqlCommandLogSheetName, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vstrNewSqlCommandLogSheetName, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vstrNewSqlCommandLogSheetName, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the specified Excel sheet
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>vstrOutputBookPath</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
        ByVal vstrOutputBookPath As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, _
            vstrOutputBookPath, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vstrNewSqlCommandLogSheetName, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub

'''
''' query SQL and output result to the prepared Excel sheet
'''
''' <Argument>vstrSQL: Input</Argument>
''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
    
    
    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, _
            venmShowUpAdoErrorOptionFlag, _
            Nothing, _
            vstrNewSqlCommandLogSheetName, _
            vblnChangeVisibleExcelApplicationIfItIsImvisible
End Sub





