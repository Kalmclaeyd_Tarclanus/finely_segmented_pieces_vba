Attribute VB_Name = "UsingClipboardForXl"
'
'   utility for Windows Clip-board from Excel.Worksheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and Excel
'       Dependent on UsingClipboard.bas, CurrentSelectedXlCells.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 25/May/2023    Kalmclaeyd Tarclanus    Separated from UsingClipboard.bas
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests or F5 key immediate executions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub SanityTestToGetTextFromSelectedRangeOfActiveSheet()

    ' Included NULL(Empty) value
    PutInClipboardFromSelectedExcelSheetRangeText
End Sub

Private Sub SanityTestToGetTextFromSelectedRangeOfActiveSheetWithoutWasteSpaceChar()

    ' Included NULL(Empty) value without waste space char
    PutInClipboardFromSelectedExcelSheetRangeText "", False, "", "", False
End Sub
'''
'''
'''
Public Sub SanityTestToGetTextFromSelectedRangeOfActiveSheetExceptForNullValues()


    PutInClipboardFromSelectedExcelSheetRangeText "", True
End Sub

'''
''' Use Japanese-style quatation marks
'''
Public Sub SanityTestToGetTextFromSelectedRangeWithJapaneseStyleQuatationMarks()

    PutInClipboardFromSelectedExcelSheetRangeTextWithJapaneseStyleQuatationMarks "", False
End Sub

'''
''' Use single-quatation marks
'''
Public Sub SanityTestToGetTextFromSelectedRangeWithSingleQuatationMarks()

    PutInClipboardFromSelectedExcelSheetRangeTextWithSingleQuotation "", False
End Sub


Public Sub SanityTestPutInClipboardText()

    'PutInClipboardText "ClipBoardCopyTest"

    PutInClipboardTextByWshShell "By Shell, ClipBoardCopyTest"
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub PutInClipboardFromGettingColumnWidthTextDelimitedByCommaInSelectedCells()

    Dim strText As String
    
    strText = GetColumnWidthTextDelimitedByCommaInSelectedCells()
    
    ' for using "GetTextToRealNumberDicFromLineDelimitedChar" function
    
    Debug.Print "ClipBoard copied: " & strText
    
    PutInClipboardText strText
End Sub

'''
'''
'''
Public Sub PutInClipboardFromTextOfValueAndColumnWidthDelimitedCommaFromSelectedRange()

    PutInClipboardText GetTextOfValueAndColumnWidthDelimitedCommaFromSelectedRange()
End Sub

'''
'''
'''
Public Sub PutInClipboardFromSelectedExcelSheetRangeTextWithSingleQuotation(Optional ByVal vstrEachItemPrefix As String = "", _
    Optional ByVal vblnExceptForNullValues As Boolean = False)

    PutInClipboardFromSelectedExcelSheetRangeText vstrEachItemPrefix, vblnExceptForNullValues, "'", "'"
End Sub

'''
'''
'''
Public Sub PutInClipboardFromSelectedExcelSheetRangeTextWithBracket(Optional ByVal vstrEachItemPrefix As String = "", _
    Optional ByVal vblnExceptForNullValues As Boolean = False)

    PutInClipboardFromSelectedExcelSheetRangeText vstrEachItemPrefix, vblnExceptForNullValues, "[", "]"
End Sub

'''
'''
'''
Public Sub PutInClipboardFromSelectedExcelSheetRangeTextWithJapaneseStyleQuatationMarks(Optional ByVal vstrEachItemPrefix As String = "", _
    Optional ByVal vblnExceptForNullValues As Boolean = False)

    PutInClipboardFromSelectedExcelSheetRangeText vstrEachItemPrefix, vblnExceptForNullValues, "�u", "�v"
End Sub

'''
''' copy selected-range text to Clipboard
'''
Public Sub PutInClipboardFromSelectedExcelSheetRangeText(Optional ByVal vstrEachItemPrefix As String = "", _
    Optional ByVal vblnExceptForNullValues As Boolean = False, _
    Optional ByVal vstrLeftBracketChar As String = "", _
    Optional ByVal vstrRightBracketChar As String = "", _
    Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True)

    PutInClipboardText GetTextFromSelectedRange(vstrEachItemPrefix, vblnExceptForNullValues, vstrLeftBracketChar, vstrRightBracketChar, vblnIncludeSpaceAfterDelimitedChar)
End Sub

