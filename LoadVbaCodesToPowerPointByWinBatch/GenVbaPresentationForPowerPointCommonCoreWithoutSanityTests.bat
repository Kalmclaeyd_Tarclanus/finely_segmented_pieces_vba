@ECHO OFF
rem
rem Set Argument [PowerPointCommonCoreWithoutSanityTests] for executing LoadVBAToOfficeFile.wsf
rem
rem Dependency Abstract:
rem     This calls LoadVBAToOfficeFile.wsf with PowerPointCommonCoreWithoutSanityTests
rem
rem Author:
rem     Kalmclaeyd M. Tarclanus
rem
rem License disclosure:
rem     Copyright (c) 2024 Kalmclaeyd M. Tarclanus
rem     Released under the MIT license
rem     https://opensource.org/licenses/mit-license.php
rem
rem Modification History:
rem     Wed, 28/Aug/2024    Tarclanus-generator     Generated at 08:25 AM.
rem
@ECHO ON

rem Set the current drive
%~d0

rem Set the current directory
cd %~dp0

cd ../

@echo Please confirm that the 'Trust access to the VBA project object model' check box in the current log-in user is enabled now.

cscript LoadVBAToOfficeFile.wsf PowerPointCommonCoreWithoutSanityTests GeneratedVbaPresentation

cd %~dp0