# Finely Segmented Pieces VBA

This project mainly proposes VBA codes to serve various table format data.

## Project purpose

This project may look like a proposal of a VBA framework. This project users can serve it as a framework. However, the author intends that users will simply serve it as VBA code parts for individual VBA coding purposes.

## Developer

Solo.

## Recommended users for this project

The following persons may be suitable for using this VBA project.
- The time of activated Visual Basic Editor is longer than the time of activated the Excel spread-sheet window, when the Microsoft Excel is used.
- The Excel VBA application is often started from either the PowerShell or Cmd, when the Microsoft Excel is used. 
- There are some local area networks (LAN) isolated from the Internet in the users' organization, and the organization has plural Windows OS edge machines with installed Excel, and the users have to administrate these.
- There have been some local area networks (LAN) isolated from the Internet in the user's organization, and users have been operating any spread-sheet application with including VBA compatible Macro system, then the application might be compatible with only part of this VBA codes set in this project.

## Usage

The two procedures are prepared.

A) Download this VBA project (git clone), and load these with manual procedures on the Visual Basic Editor. But the procedures will trouble users because this project has too many VBA code files...

B) Use prepared macro office file creating Batch file or VBScript (wsf)

1. Download this VBA project (git clone)
2. Please confirm that the 'Trust access to the VBA project object model' check box in the current log-in user is enabled now.
3. Excecute the downloaded .bat or .wsf file at either this project root,  "LoadVbaCodesToExcelByWinBatch", "LoadVbaCodesToPowerPointByWinBatch", or "LoadVbaCodesToWordByWinBatch".

Then, the user gets new Excel (, Word, or PowerPoint) macro file at the current directory.

## Test environment

- Windows 11 Pro 22H2 - 23H2
- Microsoft Office 2021 (Excel, Word, PowerPoint, Outlook)
- Microsoft Access 2019
- PostgreSQL 12.14
- SQLite 3.32.3

***

# Characteristics


## Strong points

- Quickness log visual checks on each generated spread-sheet, which is hard to misunderstanding for human beings, by a few VBA code lines dependent on this VBA project codes
- Readable codes, (but this project adopts a special system-Hungarian notation style)


## General merits


- The function implementations have been segmented into VBA code module pieces for each function dependencies. This allows to users can partially use any parts of code files of this VBA project.
- This project prepares many sanity-tests VBA code files. The VBA modules of sanity-tests can be easily removed.
  - The sanity-tests VBA codes files have almost prefix 'UTf'.
- This VBA project includes the user-interface language localization codes, which supports both Japanese and English at default. The language localization can be extended by each Excel spread-sheet.
- This VBA codes has a simple solving algorithm for deciding logging (Excel book) file path without both user VBA coding or file-dialog. This is easily understandable for all-users by executing some sanity-tests.
- When the "Trust access to the VBA project object model" check box is enabled in each Microsoft application Trust Center, this project prepares a user-form application so that users select code file from current opened codes set by some list-controls

## General demerits

- The VBA project has too many modules and classes because they are segmented for each function dependency. So, the relationships among codes are difficult to grasp on the default Visual Basic Editor Project Explorer.
- The algorithm of solving logging file path may be a little hard to understand.
- This project development process is not an absolute test-driven-development (TDD), but TDD is applied for parts of codes.
- The author adopts a special system-Hungarian notation for all VBA codes. The reason is explained for all code files
- No operating Excel-graph codes
- No support of M programming language at Microsoft Excel.

## Expected reciprocity

These opinions may look like too idealistic...

- It is a heavy work load for persons except software developers to use multiple programming languages properly. If a little of VBA supporting work domain expands, the work load of other language should have decreased slightly.
- In Japan, Microsoft Excel is still widely used in such public agencies, education institutions, and small business offices for various reasons. For example, the scale of processing data size have been modest yet. So, if they can use high quality spread-sheet application codes, then the qualities of whole their works are possible to increase with quickness.
- If high quality spread-sheet application codes have widely used, then the limitation of the code's automation become clearly understandable for all interested persons. It may lead to increase a few of general IT business chances for using other programming languages.


***

# An idea for AI system coordination with this project

This VBA project has a huge scale, so the whole VBA architecture may be a little hard to understand for human developers.

If the quality of VBA codes of this project has been accepted widely, the author can propose the following method.

If users request the understanding of this project for a trusted AI system and receive only difference VBA codes dependent on this VBA codes,
then the AI generated VBA codes should have been shortened with keeping high qualities.

Furthermore, the practical shortened codes will decrease each network communication cost between each user and the AI system.

Any Hungarian notation style codes have no problem when an AI system understands it.

***

# About author


## Real name

The author's real name is Masahiro OYAMADA, male, Japanese.

The Kalmclaeyd M. Tarclanus is an alias name.

## Reason for using an alias name

Everyone can distinct the author from people having same first (Masahiro) and last (OYAMADA) name by using the alias name. it prevents some information confusions.

## License
For this open-source project, the MIT license is applied.

Since any users can accept only one VBA code file in this project, the author includes the license terms, which is indicated by URL, for all VBA code files.

