Attribute VB_Name = "OutlookHyperLink"
'
'   get some informations, such as hyper-links, e-mail addresses, and UNC paths of the body text of a Outlook.MailItem
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Outlook
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   Modification History:
'       Mon,  8/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////

#Const HAS_WORD_REF = False

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetHyperLinksOfSelectedMail()

    Dim strMailFolderPath As String, objFraudMailStoredFolder As Outlook.Folder
    Dim objMailItem As Outlook.MailItem, objSelectedItem As Object
    Dim objDTCol As Collection
    
    Dim objAddresses As Collection
    
    
    Set objDTCol = New Collection
    
    ClearRegExpsOfHyperlinks
    
    With New Outlook.Application
    
        Set objMailItem = GetSelectedOneMailItems(.ActiveExplorer)
        
        Debug.Print objMailItem.Subject
        
        DebugCol GetHyperLinksColFromMailItem(objMailItem)
        
        Set objAddresses = GetAddressesColFromMailItem(objMailItem)
        
        If objAddresses.Count > 0 Then
        
            Debug.Print "E-mail addresses of body-text:"
            
            DebugCol objAddresses
        End If
    End With

End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** get data-table collection
'**---------------------------------------------
'''
'''
'''
Public Sub GetMailBodyHyperLinksAndSomeInfoDTColFromMail(ByRef robjDTCol As Collection, _
        ByRef robjMailItem As Outlook.MailItem, _
        Optional ByVal vblnUseWordDocumentConversion As Boolean = False)


    Dim objInspector As Outlook.Inspector, objDocument As Word.Document, objHyperlink As Word.Hyperlink
    
    Dim objHyperLinks As Collection, varHyperLink As Variant
    
    Dim strHyperLink As String, strAddress As String, objRowCol As Collection, objBaseMailInfoRowCol As Collection
        

    If robjDTCol Is Nothing Then Set robjDTCol = New Collection

    With robjMailItem
    
        Set objHyperLinks = GetHyperLinksColFromMailItem(robjMailItem, vblnUseWordDocumentConversion)
    
        If objHyperLinks.Count > 0 Then
        
            Set objBaseMailInfoRowCol = New Collection
            
            AddMailItemSomeTypesLogToRowCol objBaseMailInfoRowCol, robjMailItem
        
            For Each varHyperLink In objHyperLinks
            
                Set objRowCol = New Collection
                
                objRowCol.Add varHyperLink
                
                UnionDoubleCollectionsToSingle objRowCol, objBaseMailInfoRowCol
                
                robjDTCol.Add objRowCol
            Next
        End If
    End With
End Sub

'''
'''
'''
Public Sub GetOneRowMultiLinesHyperLinksAndEmailAddressesAndUNCPathsAndSomeInfoDTColFromMail(ByRef robjDTCol As Collection, _
        ByRef robjMailItem As Outlook.MailItem)

    Dim objHyperLinks As Collection, objEMailAddresses As Collection, objUNCPaths As Collection
    
    Dim strMultiLineHyperLinks As String, strMultiLineEMailAddresses As String, strMultiLineUNCPaths As String
    
    Dim objRowCol As Collection
    
    If robjDTCol Is Nothing Then Set robjDTCol = New Collection
    
    
    With robjMailItem
    
        Set objHyperLinks = GetHyperLinksColFromMailItem(robjMailItem)
        
        strMultiLineHyperLinks = GetMultiLineTextForGridCellFromCol(objHyperLinks, "<", ">")
        
        Set objEMailAddresses = GetAddressesColFromMailItem(robjMailItem)
        
        strMultiLineEMailAddresses = GetMultiLineTextForGridCellFromCol(objEMailAddresses, "", "", ";" & vbLf)
        
        Set objUNCPaths = GetUNCPathsColFromMailItem(robjMailItem)
        
        strMultiLineUNCPaths = GetMultiLineTextForGridCellFromCol(objUNCPaths, "<", ">")
    End With

    Set objRowCol = New Collection
        
    With objRowCol
    
        .Add strMultiLineHyperLinks
        
        .Add strMultiLineEMailAddresses
        
        .Add strMultiLineUNCPaths
    End With
    
    AddMailItemSomeTypesLogToRowCol objRowCol, robjMailItem
    
    robjDTCol.Add objRowCol
End Sub

'**---------------------------------------------
'** get hyperlinks from a Mail-Item
'**---------------------------------------------
'''
'''
'''
Public Function GetHyperLinksColFromMailItem(ByRef robjMailItem As Outlook.MailItem, _
        Optional ByVal vblnUseWordDocumentConversion As Boolean = False) As Collection

    Dim objHyperLinks As Collection

    If robjMailItem.BodyFormat = olFormatPlain Then
    
        Set objHyperLinks = GetHyperLinksColFromText(robjMailItem.Body)
    Else
        If vblnUseWordDocumentConversion Then
        
            Set objHyperLinks = GetHyperLinksColFromMailItemByUsingWordDocument(robjMailItem)
        Else
            Set objHyperLinks = GetHyperLinksColFromText(robjMailItem.HTMLBody)
        End If
    End If

    Set GetHyperLinksColFromMailItem = objHyperLinks
End Function


'''
'''
'''
Public Function GetHyperLinksColFromMailItemByUsingWordDocument(ByRef robjMailItem As Outlook.MailItem) As Collection

    Dim objHyperLinkDic As Scripting.Dictionary, objHyperLinks As Collection
    
    Dim objInspector As Outlook.Inspector, strHyperLink As String


#If HAS_WORD_REF Then

    Dim objDocument As Word.Document, objHyperlink As Word.Hyperlink
#Else
    Dim objDocument As Object, objHyperlink As Object
#End If
    
    Set objInspector = robjMailItem.GetInspector
                
    Set objDocument = objInspector.WordEditor
    
    Set objHyperLinkDic = New Scripting.Dictionary
    
    For Each objHyperlink In objDocument.Hyperlinks
    
        'Debug.Print objHyperlink.Address
        
        strHyperLink = objHyperlink.Address
              
        With objHyperLinkDic
        
            If Not .Exists(strHyperLink) Then
            
                .Add strHyperLink, 0
            End If
        End With
    Next
    
    Set GetHyperLinksColFromMailItemByUsingWordDocument = GetEnumeratorKeysColFromDic(objHyperLinkDic)
End Function



'**---------------------------------------------
'** get some information from a body text of a Mail-Item
'**---------------------------------------------
'''
''' get E-mail addresses from a body text of a Mail-Item
'''
Public Function GetAddressesColFromMailItem(ByRef robjMailItem As Outlook.MailItem) As Collection

    Dim objAddresses As Collection

    If robjMailItem.BodyFormat = olFormatPlain Then
    
        Set objAddresses = GetEmailAddressesColFromText(robjMailItem.Body)
    Else
        Set objAddresses = GetEmailAddressesColFromText(robjMailItem.HTMLBody)
    End If

    Set GetAddressesColFromMailItem = objAddresses
End Function

'''
''' get UNC pathes from a body text of a Mail-Item
'''
Public Function GetUNCPathsColFromMailItem(ByRef robjMailItem As Outlook.MailItem) As Collection

    Dim objUNCPaths As Collection

    If robjMailItem.BodyFormat = olFormatPlain Then
    
        Set objUNCPaths = GetUNCPathsColFromText(robjMailItem.Body)
    Else
        Set objUNCPaths = GetUNCPathsColFromText(robjMailItem.HTMLBody)
    End If

    Set GetUNCPathsColFromMailItem = objUNCPaths
End Function
