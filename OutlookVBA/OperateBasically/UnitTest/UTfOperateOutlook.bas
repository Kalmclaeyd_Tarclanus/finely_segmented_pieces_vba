Attribute VB_Name = "UTfOperateOutlook"
'
'   Sanity test to operate Microsoft Outlook
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 14/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////

#Const HAS_REF = True


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Public Sub msubSanityTestToConfirmCurrentOutlookDefaultAccountUser()

#If HAS_REF Then

    Dim objNamespace As Outlook.Namespace, objFolder As Outlook.Folder, objOutlook As Outlook.Application
#Else
    Dim objNamespace As Object, objFolder As Object objOutlook As Object
#End If
    
    Set objOutlook = Nothing
    
    On Error Resume Next
    
    Set objOutlook = GetObject(, "Outlook.Application")
    
    On Error GoTo 0
    
    If objOutlook Is Nothing Then
    
        Set objOutlook = CreateObject("Outlook.Application")
    End If
    
    With objOutlook
    
        Set objNamespace = .GetNamespace("MAPI")
        
        Debug.Print objNamespace.CurrentUser.Address
        
        Debug.Print objNamespace.CurrentUser.AddressEntry
    End With
End Sub

'''
'''
'''
Private Sub msubSanityTestToLogOnOutlook01()

#If HAS_REF Then

    With New Outlook.Application
#Else
    With CreateObject("Outlook.Application")
#End If
        With .Session
        
            .Logon "Outlook", ""
            
            .SendAndReceive True    ' All e-mails send and receive
        
        End With
        
        .Quit
    End With
End Sub


'''
'''
'''
Private Sub msubSanityTestToDisplayInBox()

    ' Display the Outlook Inbox
    
    msubDisplayOutlookDefaultMailFolder olFolderInbox
End Sub

'''
'''
''''
Private Sub msubSanityTestToDisplayJunk()

    ' Display the Outlook Junk - 迷惑フォルダ
    
    msubDisplayOutlookDefaultMailFolder Outlook.OlDefaultFolders.olFolderJunk
End Sub

'''
'''
''''
Private Sub msubSanityTestToDisplaySent()

    ' Display the Outlook sent mails
    
    msubDisplayOutlookDefaultMailFolder Outlook.OlDefaultFolders.olFolderSentMail
End Sub

'''
'''
'''
Private Sub msubDisplayOutlookDefaultMailFolder(Optional ByVal venmDefaultFolders As Outlook.OlDefaultFolders = Outlook.OlDefaultFolders.olFolderInbox)

#If HAS_REF Then

    Dim objNamespace As Outlook.Namespace, objFolder As Outlook.Folder
#Else
    Dim objNamespace As Object, objFolder As Object
#End If
    
    With CreateObject("Outlook.Application") ' New Outlook.Application
    
        Set objNamespace = .GetNamespace("MAPI")
        
        Set objFolder = objNamespace.GetDefaultFolder(venmDefaultFolders)   ' Inbox
        
        objFolder.Display ' display the Outlook Inbox
    End With
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetFolderNames()

#If HAS_REF Then

    Dim objNamespace As Outlook.Namespace, objFolder As Outlook.Folder
#Else
    Dim objNamespace As Object, objFolder As Object
#End If

    With CreateObject("Outlook.Application") ' New Outlook.Application
    
        Set objNamespace = .GetNamespace("MAPI")
        
        For Each objFolder In objNamespace.Folders
            
            Debug.Print objFolder.Name
        Next
    End With

End Sub


'''
'''
'''
Private Sub msubSanityTestToGetFolderNamesWithSubFolders()

#If HAS_REF Then

    Dim objFolder As Outlook.Folder
#Else
    Dim objFolder As Object
#End If
    
    Dim intHierarchyOrder As Long
    
    intHierarchyOrder = 1
    
    With CreateObject("Outlook.Application") ' New Outlook.Application
    
        'For Each objFolder In .Session.Folders
        
        'For Each objFolder In .Session.Folders("OutlookPersonalMails20220713").Folders
        
        For Each objFolder In .GetNamespace("MAPI").Folders
        
            Debug.Print CStr(intHierarchyOrder) & ", " & objFolder.Name
            
            msubLoadMailSubFolder objFolder, intHierarchyOrder + 1
        Next
    
    End With
    
    ' .Session.Folders("OutlookPersonalMails20220713")
    
End Sub

'''
'''
'''
Private Sub msubLoadMailSubFolder(ByVal vobjFolder As Outlook.Folder, ByVal vintHierarchyOrder As Long)
    
    Dim objChildFolder As Outlook.Folder
    
    For Each objChildFolder In vobjFolder.Folders
        
        Debug.Print CStr(vintHierarchyOrder) & ", " & objChildFolder.Name
        
        msubLoadMailSubFolder objChildFolder, vintHierarchyOrder + 1
    Next
End Sub


'''
'''
'''
Private Sub msubSanityTestToListUpOutlookMails()

    Dim objFolder As Outlook.Folder
    Dim objMailItem As Outlook.MailItem
    
    Dim strLog As String
    
    Dim j As Long
    
    ' Set objFolder = objOutlookApplication.Session.Folders("Outlook データ ファイル").Folders("受信保存").Folders("×××")
    
    With New Outlook.Application
    
        Set objFolder = .Session.Folders("prowmiheneth@brown.plala.or.jp").Folders("受信トレイ")
        
        For Each objMailItem In objFolder.Items
        
            strLog = ""
        
            With objMailItem
            
                'strLog = .Subject & ", " & .SenderName & ", " & Format(.ReceivedTime, "yyyy/m/d - aaa - hh:mm:ss") & ", " & .Body
            
                strLog = .Subject & ", " & .SenderName & ", " & .SenderEmailAddress & ", " & Format(.ReceivedTime, "yyyy/m/d - aaa - hh:mm:ss")
            End With
        
            Debug.Print strLog
            
    '        For j = 1 To objMailItem.Attachments.Count
    '
    '            Cells(i, j + 4) = objMailItem.Attachments.Item(j).fileName
    '
    '            ' If you need to save the attachment files
    '
    '            'objMailItem.Attachments.Item(j).SaveAsFile Path:="フルパス"
    '        Next
            
        Next

    End With

End Sub


'''
'''
'''
Private Sub msubSanityTestToMoveOutlookMails()

    Dim objSrcFolder As Outlook.Folder, objDstFolder As Outlook.Folder
    Dim objMailItem As Outlook.MailItem
    
    Dim strLog As String
    
    Dim blnDoMove As Boolean

    With New Outlook.Application
    
        Set objSrcFolder = .Session.Folders("prowmiheneth@brown.plala.or.jp").Folders("受信トレイ")
        
        Set objDstFolder = .Session.Folders("OutlookPersonalMails20220713").Folders("事業").Folders("転職活動").Folders("リクナビNEXT")
    
        For Each objMailItem In objSrcFolder.Items
        
            blnDoMove = False
        
            With objMailItem
            
                'strLog = .Subject & ", " & .SenderName & ", " & Format(.ReceivedTime, "yyyy/m/d - aaa - hh:mm:ss") & ", " & .Body
            
                strLog = .Subject & ", " & .SenderName & ", " & .SenderEmailAddress & ", " & Format(.ReceivedTime, "yyyy/m/d - aaa - hh:mm:ss")
                
                Debug.Print strLog
                
                If InStr(1, .SenderName, "リクナビＮＥＸＴ") > 0 And InStr(1, .SenderEmailAddress, "next.rikunabi.com") > 0 Then
                
                    blnDoMove = True
                End If
                
                
            End With
        
            If blnDoMove Then
            
                objMailItem.Move objDstFolder
            
            End If
            
        Next
    
    End With

End Sub


'''
''' About Outlook.Store object
'''
Private Sub msubSanityTestToListUpOutlookStore()

    Dim objStore As Outlook.Store
    
    With New Outlook.Application
    
        For Each objStore In .Session.Stores
        
            With objStore
            
                'Debug.Print .StoreID & ", " & .IsDataFileStore & ", " & .GetRootFolder.FolderPath
            
                'Debug.Print .Session.CurrentUser
            
                'Debug.Print .DisplayName
            
                'Debug.Print .ExchangeStoreType
            
                'Debug.Print .FilePath
            
                Debug.Print .DisplayName & ", " & .IsDataFileStore & ", " & .IsCachedExchange & ", " & .FilePath & ", " & .GetRootFolder.FolderPath
            
            
            End With
        Next
    
    End With

End Sub

