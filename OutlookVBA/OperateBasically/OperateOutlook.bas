Attribute VB_Name = "OperateOutlook"
'
'   Tools to operate Microsoft Outlook
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Outlook
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 15/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' olm; microsoft OutLook-Mails
'''
Public Enum OutlookMailItemPropertyType

    olmSenderEmailAddress = 0   ' Text
    
    olmNameOfSenderObject = 1   ' Text
    
    olmSubject = 2  ' Text
    
    olmBody = 3     ' Text
    
    olmUnRead = 4   ' Boolean
End Enum


'''
''' UnRead state, olm; microsoft OutLook-Mails
'''
Public Enum OutlookMailUnReadState

    olmNoChecksMailUnRead = 0
    
    olmMailIsUnRead = 1 ' Unread Is True
    
    olmMailHasAlreadyRead = 2   ' Unread Is False
End Enum

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToOperateOutlook()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", _
            "OperateOutlookToClassify,OperateOutlookForXl,OperateOutlookToClassifyForXl,EMailClassifyingCondition,EMailMovedPersonalBoxLogs,OutlookHyperLink,UTfOperateOutlook,UTfOperateOutlookForXl,FindKeywords,FindURLAndEmailAddresses"
End Sub

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetMailItemInformationsAboutDefaultInbox()

    Dim objCol As Collection
    
    Set objCol = GetDefaultInboxMailItemInfoCol()

    DebugCol objCol
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetAllFolderDetailInformations()

    Dim objPaths As Collection
    
    Set objPaths = GetMailFolderPathsWithSubFolders(True)

    DebugCol objPaths
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetAllFolderNames()

    Dim objPaths As Collection
    
    Set objPaths = GetMailFolderPathsWithSubFolders()

    DebugCol objPaths
End Sub



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Get the current selected object on the current Outlook process
'**---------------------------------------------
'''
'''
'''
Public Function GetSelectedOneMailItems(ByRef robjExplorer As Outlook.Explorer) As Outlook.MailItem

    Dim objSelectedItem As Object, objMailItem As Outlook.MailItem
    
    For Each objSelectedItem In robjExplorer.Selection
    
        If TypeOf objSelectedItem Is Outlook.MailItem Then
        
            Set objMailItem = objSelectedItem
            
            Exit For
        End If
    Next

    Set GetSelectedOneMailItems = objMailItem
End Function

'''
'''
'''
Public Function GetSelectedMailFolder(ByRef robjExplorer As Outlook.Explorer) As Outlook.Folder

    Dim objFolder As Outlook.Folder
    
    Set objFolder = GetSelectedOneMailItems(robjExplorer).Parent

    Set GetSelectedMailFolder = objFolder
End Function


'**---------------------------------------------
'** Serve classifying rule enuemrations
'**---------------------------------------------
'''
'''
'''
Public Function GetEmailClassifyingConditionFromKeyText(ByVal vstrKeyText As String, Optional ByVal vstrCondition2ndDelimiter As String = "-") As EMailClassifyingCondition

    Dim objEMailClassifyingCondition As EMailClassifyingCondition
    Dim varMatchingCondition As Variant, strMatchingCondition As String
    
    
    Set objEMailClassifyingCondition = New EMailClassifyingCondition

    With objEMailClassifyingCondition
    
        For Each varMatchingCondition In Split(vstrKeyText, vstrCondition2ndDelimiter)
        
            strMatchingCondition = CStr(varMatchingCondition)
        
            Select Case strMatchingCondition
            
                Case "Subject", "SenderEmailAddress", "NameOfSenderObject", "Body", "UnRead"
                
                    .SearchingMailItemPropertyType = GetMailItemPropertyTypeFromText(strMatchingCondition)
            
                Case "AndOp", "OrOp", "NotAll"
            
                    .SearchingMatchingTextBoolCondition = GetMatchingTextBoolConditionTypeFromText(strMatchingCondition)
            
                Case "LCase", "Lcase"
            
                    .NeedToUseLowerCase = True
            
                Case "ExactMatch"
            
                    .NeedToExactMatch = True
                
            End Select
        Next
    End With

    Set GetEmailClassifyingConditionFromKeyText = objEMailClassifyingCondition
End Function


'''
'''
'''
Public Function GetMailItemPropertyTypeFromText(ByVal vstrMailPropertyName As String) As OutlookMailItemPropertyType

    Dim enmMailItemPropertyType As OutlookMailItemPropertyType
    
    Select Case vstrMailPropertyName
    
        Case "SenderEmailAddress"
    
            enmMailItemPropertyType = OutlookMailItemPropertyType.olmSenderEmailAddress
            
        Case "NameOfSenderObject"
        
            enmMailItemPropertyType = OutlookMailItemPropertyType.olmNameOfSenderObject
        
        Case "Subject"
    
            enmMailItemPropertyType = OutlookMailItemPropertyType.olmSubject
            
        Case "Body"
            
            enmMailItemPropertyType = OutlookMailItemPropertyType.olmBody
            
        Case "UnRead"
        
            enmMailItemPropertyType = OutlookMailItemPropertyType.olmUnRead
    End Select

    GetMailItemPropertyTypeFromText = enmMailItemPropertyType
End Function

'''
''' convert a String data delimited some character into Collection object
'''
Public Function GetMailItemPropertyTypeColFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Collection
    
    Dim objCol As Collection, i As Long, strArray() As String

    Set objCol = New Collection
    
    If vstrLineDelimited <> "" Then
        
        strArray = Split(vstrLineDelimited, vstrDelimiter)
        
        For i = LBound(strArray) To UBound(strArray)
        
            objCol.Add GetMailItemPropertyTypeFromText(Trim(strArray(i)))
        Next
    End If
    
    Set GetMailItemPropertyTypeColFromLineDelimitedChar = objCol
End Function

'**---------------------------------------------
'** Initialize EMailMovedPersonalBoxLogs object
'**---------------------------------------------
'''
'''
'''
Public Sub SetupEMailMovedPersonalBoxLogs(ByRef robjEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs, _
        ByVal vblnAllowToGetMovingBasicMailLogAsTable As Boolean, _
        ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean)

    If robjEMailMovedPersonalBoxLogs Is Nothing Then Set robjEMailMovedPersonalBoxLogs = New EMailMovedPersonalBoxLogs

    With robjEMailMovedPersonalBoxLogs
    
        .AllowToGetMovingBasicMailLogAsTable = vblnAllowToGetMovingBasicMailLogAsTable
        
        .AllowToCollectMovedMailBodyHyperlinksLogAsTable = vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable
    End With
End Sub


'''
'''
'''
Public Sub OutputEMailMovedPersonalBoxLogsToImmediateWindow(ByRef robjEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs)

    With robjEMailMovedPersonalBoxLogs
    
        Debug.Print GetTwoDimentionalTextFromDic(.MainMovingProcessLogDic, "", ",", True, True)
        
        If .AllowToGetMovingBasicMailLogAsTable Then
        
            Debug.Print GetTwoDimentionalTextFromCol(.MovingBasicMailLogDTCol)
        End If
    End With
End Sub


'**---------------------------------------------
'** Move Outlook mails to Outlook personal folder
'**---------------------------------------------
'''
'''
'''
Public Sub MoveMailsToSpecifiedFolderFromDestinationCondition(ByRef robjEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs, _
        ByVal vstrSourceMailFolderPath As String, _
        ByVal vobjMailAddressConditionToSomeInfoDic As Scripting.Dictionary, _
        ByVal vobjConditionKeyToDescriptionDic As Scripting.Dictionary, _
        ByVal vstrConditionFieldTitlesDelimitedByComma As String, _
        ByVal vstrPersonalOutlookBoxName As String, _
        ByVal vobjDestinationPathPartCol As Collection, _
        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)


    Dim objNamespace As Outlook.Namespace
    Dim objSourceFolder As Outlook.Folder, objDestinationFolder As Outlook.Folder, objMailItem As Outlook.MailItem
    Dim objPathToFolderObjectDic As Scripting.Dictionary

    Dim blnDoMove As Boolean
    Dim objPlusConditions As Collection, strNameOfSenderObject As String, strDestinationPathPart As String, objFilteringConditions As Collection
    Dim strDescription As String
    Dim objDoubleStopWatch As DoubleStopWatch, i As Long
    
    Dim objMovingMailLogDTCol As Collection, intNotMovedCount As Long, intMovedCount As Long
    Dim objExtractedMovedMailBodySomeInfoLogDTCol As Collection
    
    Dim objReportItem As Outlook.ReportItem
    
    Set objDoubleStopWatch = New DoubleStopWatch


    intNotMovedCount = 0: intMovedCount = 0

    With objDoubleStopWatch
    
        .MeasureStart

        With New Outlook.Application
        
            Set objNamespace = .GetNamespace("MAPI")
            
            Set objPathToFolderObjectDic = mfobjGetMailFolderPathPartToFolderObjectDic(vstrPersonalOutlookBoxName, _
                    vobjDestinationPathPartCol, _
                    objNamespace)
        
            Set objFilteringConditions = GetMailItemPropertyTypeColFromLineDelimitedChar(vstrConditionFieldTitlesDelimitedByComma)
        
            Set objSourceFolder = GetMailFolder(vstrSourceMailFolderPath, objNamespace.Folders)
        
            For i = objSourceFolder.Items.Count To 1 Step -1
        
                Select Case TypeName(objSourceFolder.Items.Item(i))
        
                    Case "MailItem"
        
                        Set objMailItem = objSourceFolder.Items.Item(i)
                
                        blnDoMove = False
                    
                        With objMailItem
                        
                            ' If you need to include Outlook.MailItem.UnRead property branching, I recommend you to use the MoveMailsBySpecifiedDetailConditions
                            
                            msubGetDestinationFolderWhenMailItemHasToBeMoved blnDoMove, _
                                    strDestinationPathPart, _
                                    strDescription, _
                                    objMailItem, _
                                    vobjMailAddressConditionToSomeInfoDic, _
                                    vobjConditionKeyToDescriptionDic, _
                                    objFilteringConditions
                        
                            If blnDoMove Then
                        
                                MoveMailWithLogging intMovedCount, _
                                        intNotMovedCount, _
                                        objMovingMailLogDTCol, _
                                        strDestinationPathPart, _
                                        strDescription, _
                                        objMailItem, _
                                        objPathToFolderObjectDic, _
                                        robjEMailMovedPersonalBoxLogs.AllowToGetMovingBasicMailLogAsTable, _
                                        vblnPreventExecutingToMoveMail
                                
                                If robjEMailMovedPersonalBoxLogs.AllowToCollectMovedMailBodyHyperlinksLogAsTable Then
                                
                                    ' confirm strings all of hyperlink and E-mail addresses and UNC pathes in mail body-texts
                                
                                    GetOneRowMultiLinesHyperLinksAndEmailAddressesAndUNCPathsAndSomeInfoDTColFromMail objExtractedMovedMailBodySomeInfoLogDTCol, _
                                            objMailItem
                                End If
                            Else
                                intNotMovedCount = intNotMovedCount + 1
                            End If
                        End With
                    
                    Case "ReportItem"
                
                        ' Yet implementation
                        
                        Set objReportItem = objSourceFolder.Items.Item(i)
                        
                End Select
            Next
        End With
    
        .MeasureInterval
    End With


    With robjEMailMovedPersonalBoxLogs
    
        Set .MainMovingProcessLogDic = GetBasicLogDicOfMovingEmailsProcessing(vobjMailAddressConditionToSomeInfoDic.Count, _
                intNotMovedCount, _
                intMovedCount, _
                objDoubleStopWatch.ElapsedTimeByString)
        
        If .AllowToGetMovingBasicMailLogAsTable Then
        
            Set .MovingBasicMailLogDTCol = objMovingMailLogDTCol
        End If
        
        If .AllowToCollectMovedMailBodyHyperlinksLogAsTable Then
        
            Set .ExtractedMovedMailBodySomeInfoLogDTCol = objExtractedMovedMailBodySomeInfoLogDTCol
        End If
    End With
End Sub

'''
'''
'''
Public Function GetBasicLogDicOfMovingEmailsProcessing(ByRef rintCountOfMovingEMailConditions As Long, _
        ByRef rintNotMovedCount As Long, _
        ByRef rintMovedCount As Long, _
        ByVal vstrMovingElapsedTimeByString As String)
        
    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add "Count of moving e-mail conditions", rintCountOfMovingEMailConditions
    
        .Add "Not moved count", rintNotMovedCount
        
        .Add "Moved count", rintMovedCount
    
        .Add "Moving elapsed time", vstrMovingElapsedTimeByString
        
        .Add "Logging date-time", Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
    End With
        
    Set GetBasicLogDicOfMovingEmailsProcessing = objDic
End Function
        

'''
'''
'''
Public Sub MoveMailWithLogging(ByRef rintMovedCount As Long, _
        ByRef rintNotMovedCount As Long, _
        ByRef robjMovingMailLogDTCol As Collection, _
        ByRef rstrDestinationPathPart As String, _
        ByRef rstrDescription As String, _
        ByRef robjMailItem As Outlook.MailItem, _
        ByRef robjPathToFolderObjectDic As Scripting.Dictionary, _
        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)
        

    Dim objDestinationFolder As Outlook.Folder


    Set objDestinationFolder = robjPathToFolderObjectDic.Item(rstrDestinationPathPart)


    If vblnAllowToGetMovingMailLogAsTable Then
    
        AddMovingMailLog robjMovingMailLogDTCol, rstrDestinationPathPart, rstrDescription, robjMailItem
    End If
    
    If Not vblnPreventExecutingToMoveMail Then
    
        On Error Resume Next
    
        robjMailItem.Move objDestinationFolder
        
        If Err.Number = 0 Then
        
            rintMovedCount = rintMovedCount + 1
        Else
            rintNotMovedCount = rintNotMovedCount + 1
        End If
        
        On Error GoTo 0
    Else
        rintNotMovedCount = rintNotMovedCount + 1
    End If
End Sub


'''
'''
'''
Public Sub AddMovingMailLog(ByRef robjMovingMailLogDTCol As Collection, _
        ByRef rstrDestinationPathPart As String, _
        ByRef rstrDescription As String, _
        ByRef robjMailItem As Outlook.MailItem)


    Dim objRowCol As Collection


    If robjMovingMailLogDTCol Is Nothing Then Set robjMovingMailLogDTCol = New Collection
    
    Set objRowCol = New Collection
    
    With objRowCol
    
        .Add rstrDestinationPathPart
        
        .Add rstrDescription
        
        AddMailItemSomeTypesLogToRowCol objRowCol, robjMailItem
    End With
    
    robjMovingMailLogDTCol.Add objRowCol
End Sub


'''
''' common logging
'''
Public Sub AddMailItemSomeTypesLogToRowCol(ByRef robjRowCol As Collection, ByRef robjMailItem As Outlook.MailItem)

    With robjRowCol
        
        .Add robjMailItem.SenderEmailAddress
        
        .Add robjMailItem.Sender.Name
        
        .Add robjMailItem.UnRead
        
        .Add robjMailItem.ReceivedTime
        
        .Add Left(robjMailItem.Subject, 256)
        
        .Add Left(robjMailItem.Body, 256)
    End With
End Sub


'''
'''
'''
Private Sub msubGetDestinationFolderWhenMailItemHasToBeMoved(ByRef rblnIsASatisfiedConditionSetFound As Boolean, _
        ByRef rstrDestinationPathPart As String, _
        ByRef rstrDescription As String, _
        ByRef robjMailItem As Outlook.MailItem, _
        ByRef robjMailAddressConditionToSomeInfoDic As Scripting.Dictionary, _
        ByRef vobjConditionKeyToDescriptionDic As Scripting.Dictionary, _
        ByRef robjFilteringConditions As Collection)


    Dim objDestinationFolder As Outlook.Folder, strConditionTypeName As String
    
    Dim varFirstCondition As Variant, varCondition As Variant, objSecondLaterConditions As Collection, i As Long
    
    Dim enmMailItemPropertyType As OutlookMailItemPropertyType, strDistinctKey As String ' , strDestinationPathPart As String, strDescription As String


    Set objDestinationFolder = Nothing
    
    With robjMailAddressConditionToSomeInfoDic
    
        rblnIsASatisfiedConditionSetFound = False
    
        For Each varFirstCondition In .Keys
        
            strDistinctKey = ""
            
            ' In this case, varFirstCondition is SenderEmailAddress
        
            enmMailItemPropertyType = robjFilteringConditions.Item(1)
        
            If mfblnIsAMailFilteringConditionsSatisfied(robjMailItem, varFirstCondition, enmMailItemPropertyType) Then
        
                strDistinctKey = strDistinctKey & varFirstCondition
        
                Set objSecondLaterConditions = .Item(varFirstCondition)
            
                i = 1
                
                For Each varCondition In objSecondLaterConditions
                
                    If i < objSecondLaterConditions.Count Then
                    
                        ' In this case (1st), varCondition is NameOfSenderObject
                    
                        enmMailItemPropertyType = robjFilteringConditions.Item(i + 1)
                    
                        If Not mfblnIsAMailFilteringConditionsSatisfied(robjMailItem, varCondition, enmMailItemPropertyType) Then
                        
                            Exit For
                        Else
                            strDistinctKey = strDistinctKey & "," & varCondition
                        End If
                    Else
                        ' In this case (2nd), varCondition is DestinationPathPart
                    
                        ' Use strDistinctKey then, get strDescription
                        
                        rstrDescription = vobjConditionKeyToDescriptionDic.Item(strDistinctKey)
                    
                        rstrDestinationPathPart = varCondition
                    
                        rblnIsASatisfiedConditionSetFound = True
                        
                        Exit For
                    End If
                    
                    i = i + 1
                Next
                
                If rblnIsASatisfiedConditionSetFound Then
                
                    Exit For
                End If
            End If
        Next
    End With
End Sub


'''
'''
'''
Private Function mfblnIsAMailFilteringConditionsSatisfied(ByRef robjMailItem As Outlook.MailItem, _
        ByVal vstrConditions As String, _
        ByRef renmMailItemPropertyType As OutlookMailItemPropertyType, _
        Optional ByVal vstrDelimiter As String = ";") As Boolean


    Dim blnIsSatisfied As Boolean, strConditions() As String, i As Long

    blnIsSatisfied = False

    strConditions = Split(vstrConditions, vstrDelimiter)

    For i = LBound(strConditions) To UBound(strConditions)
    
        If mfblnIsAMailAnFilteringConditionSatisfied(robjMailItem, strConditions(i), renmMailItemPropertyType) Then
        
            blnIsSatisfied = True
            
            Exit For
        End If
    Next

    mfblnIsAMailFilteringConditionsSatisfied = blnIsSatisfied
End Function


'''
''' This supports only partial matches for searching words
'''
Private Function mfblnIsAMailAnFilteringConditionSatisfied(ByRef robjMailItem As Outlook.MailItem, _
        ByVal vstrAnCondition As String, _
        ByRef renmMailItemPropertyType As OutlookMailItemPropertyType) As Boolean


    Dim blnIsSatisfied As Boolean

    blnIsSatisfied = False

    With robjMailItem

        Select Case renmMailItemPropertyType
        
            Case OutlookMailItemPropertyType.olmSenderEmailAddress
            
                blnIsSatisfied = (InStr(1, .SenderEmailAddress, vstrAnCondition) > 0)
            
            Case OutlookMailItemPropertyType.olmNameOfSenderObject
            
                blnIsSatisfied = (InStr(1, .Sender.Name, vstrAnCondition) > 0)
            
            Case OutlookMailItemPropertyType.olmSubject
            
                blnIsSatisfied = (InStr(1, .Subject, vstrAnCondition) > 0)
                
            Case OutlookMailItemPropertyType.olmBody
        
                blnIsSatisfied = (InStr(1, .Body, vstrAnCondition) > 0)
        End Select

    End With

    mfblnIsAMailAnFilteringConditionSatisfied = blnIsSatisfied
End Function


'''
''' get dictionary of Key DestinationFolderPathPart string and Value DestinationFolder object based on the vobjDestinationPathPartCol
'''
Private Function mfobjGetMailFolderPathPartToFolderObjectDic(ByVal vstrPersonalOutlookBoxName As String, _
        ByVal vobjDestinationPathPartCol As Collection, _
        ByVal vobjNamespace As Outlook.Namespace) As Scripting.Dictionary


    Dim varDestinationPathPart As Variant, strDestinationFolderPath As String, objPathToFolderObjectDic As Scripting.Dictionary
    
    Dim objDestinationFolder As Outlook.Folder


    Set objPathToFolderObjectDic = New Scripting.Dictionary

    For Each varDestinationPathPart In vobjDestinationPathPartCol
        
        strDestinationFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & varDestinationPathPart
    
        Set objDestinationFolder = GetMailFolder(strDestinationFolderPath, vobjNamespace.Folders)
        
        If Not objDestinationFolder Is Nothing Then
        
            objPathToFolderObjectDic.Add varDestinationPathPart, objDestinationFolder
        Else
            MsgBox "Probably, the Mail folder hasn't been created yet;" & vbNewLine & strDestinationFolderPath
        End If
    Next

    Set mfobjGetMailFolderPathPartToFolderObjectDic = objPathToFolderObjectDic
End Function



'**---------------------------------------------
'** Get the current Outlook mail information
'**---------------------------------------------
'''
'''
'''
Public Function GetDefaultInboxMailItemInfoCol() As Collection

    Dim objNamespace As Outlook.Namespace, objFolder As Outlook.Folder
    Dim objCol As Collection

    With New Outlook.Application
    
        Set objNamespace = .GetNamespace("MAPI")
        
        Set objFolder = objNamespace.GetDefaultFolder(olFolderInbox)
    
        Set objCol = GetMailItemInfoColInAFolder(objFolder)
    End With

    Set GetDefaultInboxMailItemInfoCol = objCol
End Function


'''
'''
'''
Public Function GetMailItemInfoColInAFolder(ByVal vobjFolder As Outlook.Folder) As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    CollectMailItemInfoColInAFolder objCol, vobjFolder

    Set GetMailItemInfoColInAFolder = objCol
End Function


'''
'''
'''
Public Function CollectMailItemInfoColInAFolder(ByRef robjCol As Collection, _
        ByVal vobjFolder As Outlook.Folder) As Collection

    Dim objRowCol As Collection
    
    Dim objMailItem As Outlook.MailItem
    
    
    For Each objMailItem In vobjFolder.Items

        Set objRowCol = New Collection
        
        With objRowCol
            
            .Add objMailItem.Subject
            
            .Add objMailItem.SenderName
            
            .Add objMailItem.Sender.Name
            
            .Add objMailItem.SenderEmailAddress
            
            .Add objMailItem.ReceivedTime
            
            .Add objMailItem.Attachments.Count
            
            .Add objMailItem.BodyFormat
            
            .Add objMailItem.Size
            
            .Add objMailItem.UnRead
            
        End With
        
        robjCol.Add objRowCol
    Next
End Function


'''
'''
'''
Public Function GetSpecifiedMailFolderDetailInformationCol(ByVal vobjMailFolderPaths As Collection) As Collection

    Dim varMailFolderPath As Variant, strMailFolderPath As String
    Dim objFolder As Outlook.Folder, objNamespace As Outlook.Namespace
    Dim objCol As Collection
    
    
    Set objCol = New Collection
    
    With New Outlook.Application
    
        Set objNamespace = .GetNamespace("MAPI")
    
        For Each varMailFolderPath In vobjMailFolderPaths
        
            strMailFolderPath = varMailFolderPath
            
            Set objFolder = GetMailFolder(strMailFolderPath, objNamespace.Folders)
            
            If Not objFolder Is Nothing Then
            
                msubCollectMailFolderDetails objCol, objFolder
            End If
        Next
    End With

    Set GetSpecifiedMailFolderDetailInformationCol = objCol
End Function



'''
'''
'''
Public Function GetMailFolderPathsWithSubFolders(Optional ByVal vblnCollectMailDetailInfo As Boolean = False, _
        Optional ByVal vblnFilterOfIncludingAtLeastOneMail As Boolean = False) As Collection


    Dim objFolder As Outlook.Folder, strRootMailFolderPath As String
    
    Dim intHierarchyOrder As Long
    
    Dim objMailFolderInfos As Collection, blnAddInfo As Boolean
    
    
    intHierarchyOrder = 1
    
    
    Set objMailFolderInfos = New Collection
    
    With New Outlook.Application
    
        'For Each objFolder In .Session.Folders
        
        For Each objFolder In .GetNamespace("MAPI").Folders
        
            'Debug.Print CStr(intHierarchyOrder) & ", " & objFolder.Name
            
            strRootMailFolderPath = "\\" & objFolder.Name
            
            blnAddInfo = True
            
            If vblnFilterOfIncludingAtLeastOneMail Then
            
                If objFolder.Items.Count = 0 Then
                
                    blnAddInfo = False
                End If
            End If
            
            If blnAddInfo Then
            
                If vblnCollectMailDetailInfo Then
                
                    msubCollectMailFolderDetails objMailFolderInfos, objFolder
                Else
                    objMailFolderInfos.Add strRootMailFolderPath
                End If
            End If
            
            
            If objFolder.Folders.Count > 0 Then
            
                msubCollectSubMailFolder objMailFolderInfos, _
                        strRootMailFolderPath, _
                        objFolder, _
                        intHierarchyOrder + 1, _
                        vblnCollectMailDetailInfo, _
                        vblnFilterOfIncludingAtLeastOneMail
            End If
        Next
    
    End With
    
    Set GetMailFolderPathsWithSubFolders = objMailFolderInfos
End Function


'''
'''
'''
Public Function GetMailFolderDetailInformationCol(ByVal vstrPersonalOutlookBoxName As String, _
        ByVal vobjDestinationPathPartCol As Collection) As Collection


    Dim objNamespace As Outlook.Namespace, varDestinationPathPart As Variant
    
    Dim strMailFolderPath As String, objFolder As Outlook.Folder
    
    Dim objMailFolderInfos As Collection, objRowCol As Collection
    
    
    Set objMailFolderInfos = New Collection

    With New Outlook.Application

        Set objNamespace = .GetNamespace("MAPI")

        For Each varDestinationPathPart In vobjDestinationPathPartCol
        
            strMailFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & varDestinationPathPart
        
            Set objFolder = GetMailFolder(strMailFolderPath, objNamespace.Folders)
        
            If Not objFolder Is Nothing Then
            
                msubCollectMailFolderDetails objMailFolderInfos, objFolder
            End If
        Next
    End With

    Set GetMailFolderDetailInformationCol = objMailFolderInfos
End Function


'''
'''
'''
Public Function CreateMailFolderPathsFromDestinationPathPartCol(ByVal vstrPersonalOutlookBoxName As String, _
        ByVal vobjDestinationPathPartCol As Collection) As Collection


    Dim objNamespace As Outlook.Namespace, varDestinationPathPart As Variant
    
    Dim strMailFolderPath As String, objFolder As Outlook.Folder
    
    Dim objCol As Collection
    
    
    Set objCol = New Collection

    With New Outlook.Application

        Set objNamespace = .GetNamespace("MAPI")

        For Each varDestinationPathPart In vobjDestinationPathPartCol
        
            strMailFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & varDestinationPathPart
        
            Set objFolder = GetMailFolder(strMailFolderPath, objNamespace.Folders)
        
            If objFolder Is Nothing Then
            
                Set objFolder = CreateMailFolder(strMailFolderPath, objNamespace.Folders)
                
                If Not objFolder Is Nothing Then
                
                    objCol.Add strMailFolderPath
                End If
            End If
        Next
    End With

    Set CreateMailFolderPathsFromDestinationPathPartCol = objCol
End Function


'''
'''
'''
Public Sub CreateMailFoldersFromMailFolderPathDicIfTheUserNeedsIts(ByRef robjNotExistedMailFolderPathDic As Scripting.Dictionary, _
        Optional ByVal vobjOutlookApplication As Outlook.Application = Nothing)


    Dim objOutlookApplication As Outlook.Application, objNamespace As Outlook.Namespace
    
    Dim enmMsgResult As VbMsgBoxResult, varMailFolderPath As Variant, strMailFolderPath As String, objFolder As Outlook.Folder
    
    Dim intCountOfCreate As Long
    
    
    If Not robjNotExistedMailFolderPathDic Is Nothing Then
        
        If robjNotExistedMailFolderPathDic.Count > 0 Then
        
            enmMsgResult = MsgBox(mfstrGetMessagePromptAboutCreateOutlookSubFolders(robjNotExistedMailFolderPathDic), _
                    vbYesNo Or vbInformation, _
                    "Confirm you want whether to create Outlook sub-folders in a personal boxes or not")
        
            If enmMsgResult = vbYes Then
            
                If Not vobjOutlookApplication Is Nothing Then
                
                    Set objOutlookApplication = vobjOutlookApplication
                Else
                    Set objOutlookApplication = New Outlook.Application
                End If
                
                intCountOfCreate = 0
                
                With objOutlookApplication
                
                    Set objNamespace = .GetNamespace("MAPI")
            
                    For Each varMailFolderPath In GetEnumeratorKeysColFromDic(robjNotExistedMailFolderPathDic)
            
                        strMailFolderPath = varMailFolderPath
                    
                        Set objFolder = CreateMailFolder(strMailFolderPath, objNamespace.Folders)
                        
                        If Not objFolder Is Nothing Then
                        
                            intCountOfCreate = intCountOfCreate + 1
                        End If
                    Next
                End With
            
                If intCountOfCreate = robjNotExistedMailFolderPathDic.Count Then
                
                    robjNotExistedMailFolderPathDic.RemoveAll
                End If
            End If
        End If
    End If
End Sub


'''
'''
'''
Private Function mfstrGetMessagePromptAboutCreateOutlookSubFolders(ByRef robjNotExistedMailFolderPathDic As Scripting.Dictionary) As String

    Dim varMailFolderPath As Variant, strPrompt As String
    
    strPrompt = "Do you allow to create the following Outlook sub-folders:" & vbNewLine

    For Each varMailFolderPath In GetEnumeratorKeysColFromDic(robjNotExistedMailFolderPathDic)

        strPrompt = strPrompt & vbNewLine & CStr(varMailFolderPath)
    Next

    mfstrGetMessagePromptAboutCreateOutlookSubFolders = strPrompt
End Function


'''
'''
'''
Public Function GetMailFolderPathToExistsTableCol(ByVal vstrPersonalOutlookBoxName As String, _
        ByVal vobjDestinationPathPartCol As Collection) As Collection


    Dim objNamespace As Outlook.Namespace, varDestinationPathPart As Variant, strMailFolderPath As String
    
    Dim objFolder As Outlook.Folder
    
    Dim objCol As Collection, objRowCol As Collection
    
    
    Set objCol = New Collection

    With New Outlook.Application

        Set objNamespace = .GetNamespace("MAPI")

        For Each varDestinationPathPart In vobjDestinationPathPartCol
        
            strMailFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & varDestinationPathPart
        
            Set objFolder = GetMailFolder(strMailFolderPath, objNamespace.Folders)
        
            Set objRowCol = New Collection
        
            With objRowCol
            
                .Add strMailFolderPath
            
                If Not objFolder Is Nothing Then
                
                    .Add True
                    
                    .Add objFolder.Items.Count
                Else
                    .Add False
                    
                    .Add Empty
                End If
        
                objCol.Add objRowCol
            End With
        Next

    End With

    Set GetMailFolderPathToExistsTableCol = objCol
End Function


'''
'''
'''
Public Function GetMailFolderWithPersonalOutlookBoxName(ByRef rstrPersonalOutlookBoxName As String, _
        ByRef rstrDestinationPathPart As String, _
        ByVal vobjMAPIFolders As Outlook.Folders) As Outlook.Folder


    Dim strMailFolderPath As String
    
    
    strMailFolderPath = "\\" & rstrPersonalOutlookBoxName & "\" & rstrDestinationPathPart
    
    Set GetMailFolderWithPersonalOutlookBoxName = GetMailFolder(strMailFolderPath, vobjMAPIFolders)
End Function


'''
'''
'''
Public Function GetMailFolder(ByRef rstrMailFolderFullPath As String, ByVal vobjMAPIFolders As Outlook.Folders) As Outlook.Folder

    Set GetMailFolder = GetMailFolderWithCreating(rstrMailFolderFullPath, vobjMAPIFolders, False)
End Function

Public Function CreateMailFolder(ByVal vstrMailFolderFullPath As String, ByVal vobjMAPIFolders As Outlook.Folders) As Outlook.Folder

    Set CreateMailFolder = GetMailFolderWithCreating(vstrMailFolderFullPath, vobjMAPIFolders, True)
End Function


'''
'''
'''
Private Function GetMailFolderWithCreating(ByRef rstrMailFolderFullPath As String, ByVal vobjMAPIFolders As Outlook.Folders, _
        Optional ByVal vblnCreateFolder As Boolean = True) As Outlook.Folder


    Dim strDelimitedFolderNames() As String, i As Long, intCounter As Long, strPath As String
    
    Dim objFolder As Outlook.Folder, objParentFolders As Outlook.Folders
    
    
    If InStr(1, rstrMailFolderFullPath, "\\") = 1 Then
    
        strPath = Right(rstrMailFolderFullPath, Len(rstrMailFolderFullPath) - 2)
    Else
        strPath = rstrMailFolderFullPath
    End If
    
    If InStrRev(strPath, "\") = Len(strPath) Then
    
        strPath = Left(strPath, Len(strPath) - 1)
    End If
    
    strDelimitedFolderNames = Split(strPath, "\")
    
    intCounter = 0
    
    For i = LBound(strDelimitedFolderNames) To UBound(strDelimitedFolderNames)
    
        If intCounter = 0 Then
        
            Set objParentFolders = vobjMAPIFolders
        Else
            Set objParentFolders = objFolder.Folders
        End If
    

        Set objFolder = Nothing
        
        On Error Resume Next
        
        Set objFolder = objParentFolders.Item(strDelimitedFolderNames(i))
        
        On Error GoTo 0
        
        If objFolder Is Nothing Then
        
            If vblnCreateFolder Then
            
                Set objFolder = objParentFolders.Add(strDelimitedFolderNames(i))
            Else
            
                Exit For
            End If
        End If
        
        intCounter = intCounter + 1
    Next

    Set GetMailFolderWithCreating = objFolder
End Function



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubCollectSubMailFolder(ByRef robjMailFolderInfos As Collection, _
        ByVal vstrParentFolderPath As String, _
        ByVal vobjFolder As Outlook.Folder, _
        ByVal vintHierarchyOrder As Long, _
        Optional ByVal vblnCollectMailDetailInfo As Boolean = False, _
        Optional ByVal vblnFilterOfIncludingAtLeastOneMail As Boolean = False)
    
    
    Dim objChildFolder As Outlook.Folder, strCurrentFolderPath As String
    
    Dim blnAddInfo As Boolean
    
    
    For Each objChildFolder In vobjFolder.Folders
        
        'Debug.Print CStr(vintHierarchyOrder) & ", " & objChildFolder.Name
        
        strCurrentFolderPath = mfstrGetMailFolderPathString(vstrParentFolderPath, objChildFolder)
        
        blnAddInfo = True
        
        If vblnFilterOfIncludingAtLeastOneMail Then
        
            If objChildFolder.Items.Count = 0 Then
            
                blnAddInfo = False
            End If
        End If
        
        
        If blnAddInfo Then
        
            If vblnCollectMailDetailInfo Then
                
                msubCollectMailFolderDetails robjMailFolderInfos, objChildFolder
            Else
                
                robjMailFolderInfos.Add strCurrentFolderPath
            End If
        End If
        
        If objChildFolder.Folders.Count > 0 Then
        
            msubCollectSubMailFolder robjMailFolderInfos, _
                    strCurrentFolderPath, _
                    objChildFolder, _
                    vintHierarchyOrder + 1, _
                    vblnCollectMailDetailInfo, _
                    vblnFilterOfIncludingAtLeastOneMail
        End If
    Next
End Sub


'''
'''
'''
Private Sub msubCollectMailFolderDetails(ByRef robjCol As Collection, ByRef robjFolder As Outlook.Folder)

    Dim objRowCol As Collection
    
    Set objRowCol = New Collection
    
    
    With robjFolder
    
        objRowCol.Add .Name
        
        objRowCol.Add .Items.Count
        
        objRowCol.Add .UnReadItemCount
    
        objRowCol.Add .FolderPath
        
        objRowCol.Add .EntryID
        
        objRowCol.Add .Description
        
        objRowCol.Add .DefaultItemType
    End With

    robjCol.Add objRowCol
End Sub

'''
'''
'''
Private Function mfstrGetMailFolderPathString(ByRef rstrParentFolderPath As String, _
        ByRef robjFolder As Outlook.Folder) As String

    Dim strCurrentFolderPath As String

    If rstrParentFolderPath <> "" Then
    
        strCurrentFolderPath = rstrParentFolderPath & "\" & robjFolder.Name
    Else
        strCurrentFolderPath = "\\" & robjFolder.Name
    End If

    mfstrGetMailFolderPathString = strCurrentFolderPath
End Function



