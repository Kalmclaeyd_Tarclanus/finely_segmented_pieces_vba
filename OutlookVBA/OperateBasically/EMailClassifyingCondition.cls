VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "EMailClassifyingCondition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   A data class of the outlook e-mail classification, when no use of RegExp patterns
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Outlook
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat,  6/May/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private menmMailItemPropertyType As OutlookMailItemPropertyType

Private menmMatchingTextBoolCondition As MatchingTextBoolCondition

Private mblnNeedToUseLowerCase As Boolean

Private mblnNeedToExactMatch As Boolean

'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub Class_Initialize()

    menmMailItemPropertyType = olmSubject
    
    menmMatchingTextBoolCondition = MatchingTextBoolOrOp
    
    mblnNeedToUseLowerCase = False
    
    mblnNeedToExactMatch = False
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////


Public Property Get SearchingMailItemPropertyType() As OutlookMailItemPropertyType

    SearchingMailItemPropertyType = menmMailItemPropertyType
End Property
Public Property Let SearchingMailItemPropertyType(ByVal venmMailItemPropertyType As OutlookMailItemPropertyType)

    menmMailItemPropertyType = venmMailItemPropertyType
End Property


Public Property Get SearchingMatchingTextBoolCondition() As MatchingTextBoolCondition

    SearchingMatchingTextBoolCondition = menmMatchingTextBoolCondition
End Property
Public Property Let SearchingMatchingTextBoolCondition(ByVal venmMatchingTextBoolCondition As MatchingTextBoolCondition)

    menmMatchingTextBoolCondition = venmMatchingTextBoolCondition
End Property


Public Property Get NeedToUseLowerCase() As Boolean

    NeedToUseLowerCase = mblnNeedToUseLowerCase
End Property
Public Property Let NeedToUseLowerCase(ByVal vblnNeedToUseLowerCase As Boolean)

    mblnNeedToUseLowerCase = vblnNeedToUseLowerCase
End Property


Public Property Get NeedToExactMatch() As Boolean

    NeedToExactMatch = mblnNeedToExactMatch
End Property
Public Property Let NeedToExactMatch(ByVal vblnNeedToExactMatch As Boolean)

    mblnNeedToExactMatch = vblnNeedToExactMatch
End Property

