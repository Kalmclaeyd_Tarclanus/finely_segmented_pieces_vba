Attribute VB_Name = "OperateOutlookToClassify"
'
'   Tools to operate Microsoft Outlook using Windows API, which serves Windows INI files
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Outlook and Windows API(serving INI files)
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 28/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
'       Wed, 17/Jul/2024    Kalmclaeyd Tarclanus    Supported the moving the receiver E-mail address misrepresentation string type
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrReceiverMailAddressMisrepresentationKey As String = "\ReceiverMailAddressMisrepresentation"

Private Const mstrFirstPartOfReceiverMailAddressMisrepresentation As String = mstrReceiverMailAddressMisrepresentationKey & ":=FirstPart"

Private Const mstrAllOfReceiverMailAddressMisrepresentation As String = mstrReceiverMailAddressMisrepresentationKey & ":=All"

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Move mail-items by detailed conditions
'**---------------------------------------------
'''
'''
'''
Public Sub MoveMailsBySpecifiedDetailConditionsFromIniFile(ByVal vstrSourceMailFolderPath As String, _
        ByVal vstrPersonalOutlookBoxName As String, _
        ByVal vstrIniPath As String, _
        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
        Optional ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean = False, _
        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)


    Dim objSectionToKeyValueDic As Scripting.Dictionary, objEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs


    ' !Attention - Since the Windows ini file is a Shift_JIS, the Chinese Kanji characters should have been lost in Japanese locale Windows OS.

    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromIni(vstrIniPath)


    SetupEMailMovedPersonalBoxLogs objEMailMovedPersonalBoxLogs, _
            vblnAllowToGetMovingMailLogAsTable, _
            vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable

    MoveMailsBySpecifiedDetailConditions objEMailMovedPersonalBoxLogs, _
            vstrSourceMailFolderPath, _
            vstrPersonalOutlookBoxName, _
            objSectionToKeyValueDic, _
            vblnPreventExecutingToMoveMail
    
    ' output log to the Immediate-window
    
    OutputEMailMovedPersonalBoxLogsToImmediateWindow objEMailMovedPersonalBoxLogs
End Sub


'''
'''
'''
Public Sub MoveMailsBySpecifiedDetailConditions(ByRef robjEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs, _
        ByVal vstrSourceMailFolderPath As String, _
        ByVal vstrPersonalOutlookBoxName As String, _
        ByRef robjSectionToKeyValueDic As Scripting.Dictionary, _
        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)



    Dim objNamespace As Outlook.Namespace, objSourceFolder As Outlook.Folder, objMailItem As Outlook.MailItem
    Dim i As Long
    
    Dim objPathToFolderObjectDic As Scripting.Dictionary
    
    Dim varConditionSet As Variant, objConditionSet As Collection, varCondition1stKey As Variant
    Dim objEMailClassifyingCondition As EMailClassifyingCondition, objFindingKeyWords As Collection
    Dim objSecondLaterConditions As Collection
    Dim strDescription As String
    
    Dim objCondition1stKeyToConditionSetDic As Scripting.Dictionary             ' Dictionary(Of Key, Collection(Of SomeObjects))
    Dim objCondition1stKeyToSecondLaterConditionsDic As Scripting.Dictionary    ' Dictionary(Of Key, Collection(Of Collection(Of SomeObjects)))
    Dim objCondition1stKeyToDestinationPathPartDic As Scripting.Dictionary      ' Dictionary(Of Key, String)
    Dim objCondition1stKeyToDescriptionDic As Scripting.Dictionary              ' Dictionary(Of Key, String)
    Dim blnAre2ndConditionsSatisfied As Boolean
    
    
    
    Dim blnNeedToMove As Boolean, strDestinationPathPart As String, strDestinationFolderPath As String
    Dim objDestinationFolder As Outlook.Folder
    Dim objMovingMailLogDTCol As Collection, intNotMovedCount As Long, intMovedCount As Long
    Dim objExtractedMovedMailBodySomeInfoLogDTCol As Collection
    Dim objDoubleStopWatch As DoubleStopWatch
    
    
    Dim objOutlook As Outlook.Application
    
    
    Dim objReportItem As Outlook.ReportItem
    
    intNotMovedCount = 0: intMovedCount = 0
    
    
    Set objOutlook = New Outlook.Application
    
    Set objNamespace = objOutlook.GetNamespace("MAPI")
    
    msubInitializeCondition1stKeyToConditionSetDic _
            objCondition1stKeyToConditionSetDic, _
            objCondition1stKeyToSecondLaterConditionsDic, _
            objCondition1stKeyToDestinationPathPartDic, _
            objCondition1stKeyToDescriptionDic, _
            robjSectionToKeyValueDic, _
            objNamespace

    Set objDoubleStopWatch = New DoubleStopWatch
    
    With objDoubleStopWatch

        .MeasureStart

        With objOutlook
            
            Set objNamespace = .GetNamespace("MAPI")
            
            Set objPathToFolderObjectDic = mfobjGetMailFolderPathPartToFolderObjectDicFromMailTrasportingDetailSectionToKeyValueDic(vstrPersonalOutlookBoxName, _
                    robjSectionToKeyValueDic, _
                    objNamespace)
            
            
            Set objSourceFolder = GetMailFolder(vstrSourceMailFolderPath, objNamespace.Folders)
        
            For i = objSourceFolder.Items.Count To 1 Step -1
    
                Select Case TypeName(objSourceFolder.Items.Item(i))
    
                    Case "MailItem"
    
                        Set objMailItem = objSourceFolder.Items.Item(i)
            
                        blnNeedToMove = False
            
                        With objCondition1stKeyToConditionSetDic
            
            
                            msubFindFirstSatisfyingDetailConditionSet blnNeedToMove, _
                                    strDestinationPathPart, _
                                    strDescription, _
                                    objMailItem, _
                                    objCondition1stKeyToConditionSetDic, _
                                    objCondition1stKeyToSecondLaterConditionsDic, _
                                    objCondition1stKeyToDestinationPathPartDic, _
                                    objCondition1stKeyToDescriptionDic
            
                            
                            If blnNeedToMove Then
                                
                                MoveMailWithLogging intMovedCount, _
                                        intNotMovedCount, _
                                        objMovingMailLogDTCol, _
                                        strDestinationPathPart, _
                                        strDescription, _
                                        objMailItem, _
                                        objPathToFolderObjectDic, _
                                        robjEMailMovedPersonalBoxLogs.AllowToGetMovingBasicMailLogAsTable, _
                                        vblnPreventExecutingToMoveMail
                                
                                
                                If robjEMailMovedPersonalBoxLogs.AllowToCollectMovedMailBodyHyperlinksLogAsTable Then
                                
                                    ' confirm strings all of hyperlink and E-mail addresses and UNC pathes in mail body-texts
                                
                                    GetOneRowMultiLinesHyperLinksAndEmailAddressesAndUNCPathsAndSomeInfoDTColFromMail objExtractedMovedMailBodySomeInfoLogDTCol, _
                                            objMailItem
                                End If
                            Else
                                intNotMovedCount = intNotMovedCount + 1
                            End If
                        End With
                
                    Case "ReportItem"
                
                        Set objReportItem = objSourceFolder.Items.Item(i)
                
                        Debug.Print "ReportItem received: " & objReportItem.Subject
                
                    Case Else
                
                        Debug.Print "Not supported - " & TypeName(objSourceFolder.Items.Item(i))
                
                End Select
                
            Next
        End With
    
        .MeasureInterval
    End With


    With robjEMailMovedPersonalBoxLogs
    
        Set .MainMovingProcessLogDic = GetBasicLogDicOfMovingEmailsProcessing(objCondition1stKeyToConditionSetDic.Count, _
                intNotMovedCount, _
                intMovedCount, _
                objDoubleStopWatch.ElapsedTimeByString)
        
        
        If .AllowToGetMovingBasicMailLogAsTable Then
        
            Set .MovingBasicMailLogDTCol = objMovingMailLogDTCol
        End If
        
        If .AllowToCollectMovedMailBodyHyperlinksLogAsTable Then
        
            Set .ExtractedMovedMailBodySomeInfoLogDTCol = objExtractedMovedMailBodySomeInfoLogDTCol
        End If
    End With
End Sub


'''
''' get dictionary of Key DestinationFolderPathPart string and Value DestinationFolder object based on the robjSectionToKeyValueDic detail e-mail transport rules dictionary
'''
Private Function mfobjGetMailFolderPathPartToFolderObjectDicFromMailTrasportingDetailSectionToKeyValueDic(ByVal vstrPersonalOutlookBoxName As String, _
        ByRef robjSectionToKeyValueDic As Scripting.Dictionary, _
        ByVal vobjNamespace As Outlook.Namespace) As Scripting.Dictionary


    Dim varCondition1stKey As Variant, objChildConditionsDic As Scripting.Dictionary, objPathToFolderObjectDic As Scripting.Dictionary
    
    Dim strDestinationPathPart As String, strDestinationFolderPath As String
    
    Dim objDestinationFolder As Outlook.Folder
        
    Const strTransportDestinationKey As String = "TransportDestination"


    Set objPathToFolderObjectDic = New Scripting.Dictionary

    With robjSectionToKeyValueDic
    
        For Each varCondition1stKey In .Keys
        
            Set objChildConditionsDic = .Item(varCondition1stKey)
        
            With objChildConditionsDic
            
                If .Exists(strTransportDestinationKey) Then
            
                    strDestinationPathPart = .Item(strTransportDestinationKey)
            
                    With objPathToFolderObjectDic
            
                        If Not .Exists(strDestinationPathPart) Then
            
                            strDestinationFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & strDestinationPathPart
                            
                            Set objDestinationFolder = GetMailFolder(strDestinationFolderPath, vobjNamespace.Folders)
                            
                            If Not objDestinationFolder Is Nothing Then
                            
                                objPathToFolderObjectDic.Add strDestinationPathPart, objDestinationFolder
                            Else
                                MsgBox "Probably, the Mail folder hasn't been created yet;" & vbNewLine & strDestinationFolderPath
                            End If
                        End If
                    End With
                Else
                    MsgBox "No TransportDestination key-value setting in " & CStr(varCondition1stKey), vbExclamation Or vbOKOnly
                End If
            End With
        Next
    End With

    Set mfobjGetMailFolderPathPartToFolderObjectDicFromMailTrasportingDetailSectionToKeyValueDic = objPathToFolderObjectDic
End Function


'''
'''
'''
Private Sub msubFindFirstSatisfyingDetailConditionSet(ByRef rblnIsASatisfiedConditionSetFound As Boolean, _
        ByRef rstrDestinationPathPart As String, _
        ByRef rstrDescription As String, _
        ByRef robjMailItem As Outlook.MailItem, _
        ByRef robjCondition1stKeyToConditionSetDic As Scripting.Dictionary, _
        ByRef robjCondition1stKeyToSecondLaterConditionsDic As Scripting.Dictionary, _
        ByRef robjCondition1stKeyToDestinationPathPartDic As Scripting.Dictionary, _
        ByRef robjCondition1stKeyToDescriptionDic As Scripting.Dictionary)


    Dim varCondition1stKey As Variant
    Dim objEMailClassifyingCondition As EMailClassifyingCondition, objFindingKeyWords As Collection
    Dim objSecondLaterConditions As Collection, objConditionSet  As Collection, varConditionSet As Variant
    Dim blnAre2ndConditionsSatisfied As Boolean

    Dim strTmpDescription As String

    With robjCondition1stKeyToConditionSetDic

        For Each varCondition1stKey In .Keys
        
            msubGetClassifyingMatchConditionSet objEMailClassifyingCondition, objFindingKeyWords, .Item(varCondition1stKey)
        
            ' Whether the first condition is satisfied or not
            
            If mfblnIsDetailAConditionSatisfied(robjMailItem, objEMailClassifyingCondition, objFindingKeyWords) Then
            
                blnAre2ndConditionsSatisfied = True
            
'                strTmpDescription = robjCondition1stKeyToDescriptionDic.Item(varCondition1stKey)
'
'                If InStr(1, strTmpDescription, "<Description>") > 0 Then
'
'                    Debug.Print "Mail sorting description - breaking point"
'                End If
            
                Set objSecondLaterConditions = robjCondition1stKeyToSecondLaterConditionsDic.Item(varCondition1stKey)
            
                If objSecondLaterConditions.Count > 0 Then
                
                    For Each varConditionSet In objSecondLaterConditions
                    
                        Set objConditionSet = varConditionSet
                        
                        msubGetClassifyingMatchConditionSet objEMailClassifyingCondition, objFindingKeyWords, objConditionSet
                        
                        If Not mfblnIsDetailAConditionSatisfied(robjMailItem, objEMailClassifyingCondition, objFindingKeyWords) Then
                        
                            blnAre2ndConditionsSatisfied = False
                        
                            Exit For
                        End If
                    Next
                End If
            
                If blnAre2ndConditionsSatisfied Then
                
                    ' Need to move
                
                    rstrDescription = robjCondition1stKeyToDescriptionDic.Item(varCondition1stKey)
                
                    rstrDestinationPathPart = robjCondition1stKeyToDestinationPathPartDic.Item(varCondition1stKey)
                
                    Debug.Print "! Need to move - [" & robjMailItem.Subject & "] for " & rstrDescription
                
                    rblnIsASatisfiedConditionSetFound = True ' Need To Move
                
                    Exit For
                End If
            End If
        Next
    End With
End Sub



'''
''' a finding base
'''
Private Function mfblnIsDetailAConditionSatisfied(ByRef robjMailItem As Outlook.MailItem, _
        ByRef robjEMailClassifyingCondition As EMailClassifyingCondition, _
        ByRef robjFindingKeyWords As Collection) As Boolean


    Dim enmMatchingTextBoolCondition As MatchingTextBoolCondition, i As Long
    
    Dim varFindingKeyWord As Variant, strFindingKeyWord As String
    
    Dim blnIsOneKeywordMatched As Boolean
    
    Dim blnAllConditionsSatisfied As Boolean
    
    
    blnAllConditionsSatisfied = True

    With robjEMailClassifyingCondition
    
        enmMatchingTextBoolCondition = .SearchingMatchingTextBoolCondition
    
        i = 1
    
        For Each varFindingKeyWord In robjFindingKeyWords
    
            strFindingKeyWord = varFindingKeyWord
    
            blnIsOneKeywordMatched = False
    
            Select Case .SearchingMailItemPropertyType
            
                Case OutlookMailItemPropertyType.olmSenderEmailAddress
                
                    blnIsOneKeywordMatched = IsOneKeywordMatchedAboutATextString(robjMailItem.SenderEmailAddress, _
                            strFindingKeyWord, _
                            .NeedToUseLowerCase, _
                            .NeedToExactMatch)
                
                Case OutlookMailItemPropertyType.olmNameOfSenderObject
                
                    If Not robjMailItem.Sender Is Nothing Then
                
                        blnIsOneKeywordMatched = IsOneKeywordMatchedAboutATextString(robjMailItem.Sender.Name, _
                                strFindingKeyWord, _
                                .NeedToUseLowerCase, _
                                .NeedToExactMatch)
                    End If
                    
                Case OutlookMailItemPropertyType.olmSubject
                
                    blnIsOneKeywordMatched = IsOneKeywordMatchedAboutATextString(robjMailItem.Subject, _
                            strFindingKeyWord, _
                            .NeedToUseLowerCase, _
                            .NeedToExactMatch)
                
                Case OutlookMailItemPropertyType.olmBody
            
                    blnIsOneKeywordMatched = IsOneKeywordMatchedAboutATextString(robjMailItem.Body, _
                            strFindingKeyWord, _
                            .NeedToUseLowerCase, _
                            .NeedToExactMatch)
                    
                Case OutlookMailItemPropertyType.olmUnRead
                
                    If robjMailItem.UnRead = CBool(varFindingKeyWord) Then
                    
                        blnIsOneKeywordMatched = True
                    End If
                
            End Select
        
            AreAllMatchingTextConditionsSatisfied blnAllConditionsSatisfied, _
                    enmMatchingTextBoolCondition, _
                    blnIsOneKeywordMatched, _
                    i, _
                    robjFindingKeyWords.Count
            
            If enmMatchingTextBoolCondition = MatchingTextBoolOrOp And blnIsOneKeywordMatched Then
            
                ' blnAllConditionsSatisfied Is True
            
                Exit For
            End If
            
            If Not blnAllConditionsSatisfied Then
            
                Exit For
            End If
            
            i = i + 1
        Next
    End With


    mfblnIsDetailAConditionSatisfied = blnAllConditionsSatisfied
End Function




'''
'''
'''
''' <Argument>robjCondition1stKeyToConditionSetDic: Output - Nested dictionary - Dictionary(Of Key[Section], Of Dictionary(Of Key[KeyInSection], Value))</Argument>
''' <Argument>robjCondition1stKeyToSecondLaterConditionsDic: Output</Argument>
''' <Argument>robjCondition1stKeyToDestinationPathPartDic: Output</Argument>
''' <Argument>robjCondition1stKeyToDescriptionDic: Output</Argument>
''' <Argument>robjSectionToKeyValueDic: Input</Argument>
''' <Argument>robjNameSpace: Input</Argument>
''' <Argument>vstrConditionDelimiter: Output</Argument>
''' <Argument>vstrCondition2ndDelimiter: Output</Argument>
Private Sub msubInitializeCondition1stKeyToConditionSetDic(ByRef robjCondition1stKeyToConditionSetDic As Scripting.Dictionary, _
        ByRef robjCondition1stKeyToSecondLaterConditionsDic As Scripting.Dictionary, _
        ByRef robjCondition1stKeyToDestinationPathPartDic As Scripting.Dictionary, _
        ByRef robjCondition1stKeyToDescriptionDic As Scripting.Dictionary, _
        ByRef robjSectionToKeyValueDic As Scripting.Dictionary, _
        ByRef robjNameSpace As Outlook.Namespace, _
        Optional ByVal vstrConditionDelimiter As String = ";", _
        Optional ByVal vstrCondition2ndDelimiter As String = "-")


    Dim varCondition1stKey As Variant, strCondition1st As String, str1stConditions() As String
    
    Dim i As Long, j As Long
    
    Dim varMatchingCondition As Variant, strMatchingCondition As String
    
    Dim objChildKeyValueDic As Scripting.Dictionary, varKey As Variant, strKey As String, varFindingKeyWord As Variant

    Dim objEMailClassifyingCondition As EMailClassifyingCondition
    
    Dim objFindingKeyWords As Collection

    Dim objConditionSet As Collection, objSecondLaterConditions As Collection
    
    Dim strDestinationPathPart As String, strDescription As String

    
    
    If robjCondition1stKeyToConditionSetDic Is Nothing Then Set robjCondition1stKeyToConditionSetDic = New Scripting.Dictionary
    
    If robjCondition1stKeyToSecondLaterConditionsDic Is Nothing Then Set robjCondition1stKeyToSecondLaterConditionsDic = New Scripting.Dictionary
    
    If robjCondition1stKeyToDestinationPathPartDic Is Nothing Then Set robjCondition1stKeyToDestinationPathPartDic = New Scripting.Dictionary

    If robjCondition1stKeyToDescriptionDic Is Nothing Then Set robjCondition1stKeyToDescriptionDic = New Scripting.Dictionary
    
    

    With robjSectionToKeyValueDic
    
        For Each varCondition1stKey In .Keys
        
            msubSetClassifyingMatchConditionSetFrom1stStringConditionKey objConditionSet, _
                    varCondition1stKey, _
                    robjNameSpace, _
                    vstrConditionDelimiter, _
                    vstrCondition2ndDelimiter
        
            robjCondition1stKeyToConditionSetDic.Add varCondition1stKey, _
                    objConditionSet
        
        
            Set objChildKeyValueDic = .Item(varCondition1stKey)
            
            msubSetClassifyingMatchConditionSetFrom2ndChildKeyValueDic objSecondLaterConditions, _
                    strDestinationPathPart, _
                    strDescription, _
                    objChildKeyValueDic, _
                    robjNameSpace, _
                    vstrConditionDelimiter, _
                    vstrCondition2ndDelimiter
            
            robjCondition1stKeyToSecondLaterConditionsDic.Add varCondition1stKey, _
                    objSecondLaterConditions
            
            robjCondition1stKeyToDestinationPathPartDic.Add varCondition1stKey, _
                    strDestinationPathPart
            
            robjCondition1stKeyToDescriptionDic.Add varCondition1stKey, _
                    strDescription
        Next
    End With
End Sub


'''
'''
'''
''' <Argument>robjSecondLaterConditions: Output</Argument>
''' <Argument>rstrDestinationPathPart: Output</Argument>
''' <Argument>rstrDescription: Output</Argument>
''' <Argument>vobjChildKeyValueDic: Input</Argument>
''' <Argument>robjNameSpace: Input</Argument>
''' <Argument>vstrConditionDelimiter: Input</Argument>
''' <Argument>vstrCondition2ndDelimiter: Input</Argument>
Private Sub msubSetClassifyingMatchConditionSetFrom2ndChildKeyValueDic(ByRef robjSecondLaterConditions As Collection, _
        ByRef rstrDestinationPathPart As String, _
        ByRef rstrDescription As String, _
        ByVal vobjChildKeyValueDic As Scripting.Dictionary, _
        ByRef robjNameSpace As Outlook.Namespace, _
        Optional ByVal vstrConditionDelimiter As String = ";", _
        Optional ByVal vstrCondition2ndDelimiter As String = "-")
    
    
    Dim objEMailClassifyingCondition As EMailClassifyingCondition, objFindingKeyWords As Collection
    Dim varKey As Variant, strKey As String, objConditionSet As Collection
    Dim varFindingKeyWord As Variant, strFindingKeyWord As String
    
    
    Set robjSecondLaterConditions = New Collection
    
    With vobjChildKeyValueDic
            
        For Each varKey In .Keys
        
            strKey = varKey
            
            Set objEMailClassifyingCondition = Nothing: Set objFindingKeyWords = Nothing
        
            Select Case True
            
                Case StrComp("TransportDestination", strKey) = 0
            
                    rstrDestinationPathPart = .Item(varKey)
                    
                Case StrComp("Description", strKey) = 0
            
                    rstrDescription = .Item(varKey)
                
                Case Else
                
                    Set objEMailClassifyingCondition = GetEmailClassifyingConditionFromKeyText(strKey, vstrCondition2ndDelimiter)
            
                    For Each varFindingKeyWord In Split(CStr(.Item(varKey)), vstrConditionDelimiter)
            
                        If objFindingKeyWords Is Nothing Then Set objFindingKeyWords = New Collection
                        
                        ' ReceiverMailAddressMisrepresentation
                        
                        strFindingKeyWord = mfstrGetRawKeyWordOrConvertedKeyWord(CStr(varFindingKeyWord), robjNameSpace)
                        
                        objFindingKeyWords.Add strFindingKeyWord
                    Next
                    
                    msubSetClassifyingMatchConditionSet objConditionSet, _
                            objEMailClassifyingCondition, _
                            objFindingKeyWords
                    
                    robjSecondLaterConditions.Add objConditionSet
            End Select
        Next
    End With
End Sub

'''
'''
'''
''' <Argument>vstrFindingKeyWord: Input</Argument>
''' <Argument>robjNameSpace: Input</Argument>
''' <Return>String: raw or converted</Return>
Private Function mfstrGetRawKeyWordOrConvertedKeyWord(ByVal vstrFindingKeyWord As String, _
        ByRef robjNameSpace As Outlook.Namespace) As String

    Dim strFindingWord As String

    Dim strCurrentUserEMailAddress As String
    
    Dim strCurrentUserFirstPartOfEMailAddress As String
    
    
    strFindingWord = vstrFindingKeyWord
    
    Select Case UCase(vstrFindingKeyWord)

        Case UCase(mstrFirstPartOfReceiverMailAddressMisrepresentation)
        
            strFindingWord = Left(robjNameSpace.CurrentUser.Address, InStr(1, robjNameSpace.CurrentUser.Address, "@") - 1)
        
        Case UCase(mstrAllOfReceiverMailAddressMisrepresentation)

            strFindingWord = robjNameSpace.CurrentUser.Address

        Case UCase(mstrReceiverMailAddressMisrepresentationKey)
        
            strFindingWord = Left(robjNameSpace.CurrentUser.Address, InStr(1, robjNameSpace.CurrentUser.Address, "@") - 1)
    End Select

    mfstrGetRawKeyWordOrConvertedKeyWord = strFindingWord
End Function


'''
''' for 1st Section-key
'''
''' <Argument>robjConditionSet: Output - collection including both robjEMailClassifyingCondition and robjFindingKeyWords</Argument>
''' <Argument>vstrCondition1stKey: Input</Argument>
''' <Argument>robjNameSpace: Input</Argument>
''' <Argument>vstrConditionDelimiter: Input</Argument>
''' <Argument>vstrCondition2ndDelimiter: Input</Argument>
Private Sub msubSetClassifyingMatchConditionSetFrom1stStringConditionKey(ByRef robjConditionSet As Collection, _
        ByVal vstrCondition1stKey As String, _
        ByRef robjNameSpace As Outlook.Namespace, _
        Optional ByVal vstrConditionDelimiter As String = ";", _
        Optional ByVal vstrCondition2ndDelimiter As String = "-")

    Dim str1stConditions() As String, i As Long
    
    Dim objFindingKeyWords As Collection, objEMailClassifyingCondition As EMailClassifyingCondition
    
    Dim strFindingKeyWord As String
     
     
    str1stConditions = Split(vstrCondition1stKey, vstrConditionDelimiter)
            
    Set objFindingKeyWords = Nothing
    
    For i = LBound(str1stConditions) To UBound(str1stConditions)
    
        If i = LBound(str1stConditions) Then
        
            Set objEMailClassifyingCondition = GetEmailClassifyingConditionFromKeyText(str1stConditions(i), vstrCondition2ndDelimiter)
        Else
        
            If objFindingKeyWords Is Nothing Then Set objFindingKeyWords = New Collection
            
            ' ReceiverMailAddressMisrepresentation
            
            strFindingKeyWord = mfstrGetRawKeyWordOrConvertedKeyWord(str1stConditions(i), robjNameSpace)
            
            objFindingKeyWords.Add strFindingKeyWord
        End If
    Next
    
    msubSetClassifyingMatchConditionSet robjConditionSet, objEMailClassifyingCondition, objFindingKeyWords
End Sub

'''
''' get both robjEMailClassifyingCondition and robjFindingKeyWords
'''
''' <Argument>robjEMailClassifyingCondition: Output</Argument>
''' <Argument>robjFindingKeyWords: Output</Argument>
''' <Argument>robjConditionSet: Input</Argument>
Private Sub msubGetClassifyingMatchConditionSet(ByRef robjEMailClassifyingCondition As EMailClassifyingCondition, _
        ByRef robjFindingKeyWords As Collection, _
        ByRef robjConditionSet As Collection)

    With robjConditionSet
    
        Set robjEMailClassifyingCondition = .Item("EMailClassifyingCondition")
        
        Set robjFindingKeyWords = .Item("FindingKeyWords")
    End With
End Sub

'''
''' get robjConditionSet
'''
''' <Argument>robjConditionSet: Output - collection including both robjEMailClassifyingCondition and robjFindingKeyWords</Argument>
''' <Argument>robjEMailClassifyingCondition: Input</Argument>
''' <Argument>robjFindingKeyWords: Input</Argument>
Private Sub msubSetClassifyingMatchConditionSet(ByRef robjConditionSet As Collection, _
        ByRef robjEMailClassifyingCondition As EMailClassifyingCondition, _
        ByRef robjFindingKeyWords As Collection)

    Set robjConditionSet = New Collection

    With robjConditionSet
    
        .Add robjEMailClassifyingCondition, "EMailClassifyingCondition"
        
        .Add robjFindingKeyWords, "FindingKeyWords"
    End With
End Sub


'**---------------------------------------------
'** Visualize INI file state with Outlook Personal box
'**---------------------------------------------
'''
'''
'''
Public Function GetDetailConditionInformationOfMailTransPortINIFileWithoutPersonalOutlookBoxCurrentInfo(ByVal vstrvstrDetailConditionsINIFilePath As String) As Collection

    Dim objDummyDic As Scripting.Dictionary

    Set GetDetailConditionInformationOfMailTransPortINIFileWithoutPersonalOutlookBoxCurrentInfo = _
            GetDetailConditionInformationOfMailTransportingByINIFile(objDummyDic, vstrvstrDetailConditionsINIFilePath)
End Function



'''
'''
'''
Public Function GetDetailConditionInformationOfMailTransportingByINIFile(ByRef robjNotExistedMailFolderPathDic As Scripting.Dictionary, _
        ByVal vstrDetailConditionsINIFilePath As String, Optional ByVal vstrPersonalOutlookBoxName As String = "") As Collection

    Dim objSectionToKeyValueDic As Scripting.Dictionary
    
    With New Scripting.FileSystemObject
    
        If .FileExists(vstrDetailConditionsINIFilePath) Then
        
            Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromIni(vstrDetailConditionsINIFilePath)
        End If
    End With
    
    Set GetDetailConditionInformationOfMailTransportingByINIFile = GetDetailConditionInformationOfMailTransporting(robjNotExistedMailFolderPathDic, _
            objSectionToKeyValueDic, _
            vstrPersonalOutlookBoxName)
End Function

'''
'''
'''
Public Function GetDetailConditionInformationOfMailTransporting(ByRef robjNotExistedMailFolderPathDic As Scripting.Dictionary, _
        ByRef robjSectionToKeyValueDic As Scripting.Dictionary, _
        Optional ByVal vstrPersonalOutlookBoxName As String = "") As Collection


    Dim objDTCol As Collection, var1stKey As Variant, var2ndKey As Variant, objChildDic As Scripting.Dictionary, intCountOfConditions As Long
    
    Dim objRowCol As Collection
    
    Dim strDestinationPathPart As String, strClassifyingRuleName As String
    
    Dim blnAddToOutlookPersonalOutlookBoxState As Boolean, objOutlookApplication As Outlook.Application, objNamespace As Outlook.Namespace
    
    Dim objFolder As Outlook.Folder, strMailFolderPath As String
    
    Dim i As Long
    
    
    Set objDTCol = New Collection

    With robjSectionToKeyValueDic
    
        If .Count > 0 Then
        
            blnAddToOutlookPersonalOutlookBoxState = False
            
            If vstrPersonalOutlookBoxName <> "" Then
            
                blnAddToOutlookPersonalOutlookBoxState = True
                        
                Set objOutlookApplication = New Outlook.Application
                
                Set objNamespace = objOutlookApplication.GetNamespace("MAPI")
                
                If robjNotExistedMailFolderPathDic Is Nothing Then
                
                    Set robjNotExistedMailFolderPathDic = New Scripting.Dictionary
                End If
            End If
        End If
    
        For Each var1stKey In .Keys
        
            intCountOfConditions = 1
        
            Set objChildDic = .Item(var1stKey)
        
            With objChildDic
            
                For Each var2ndKey In .Keys
                
                    Select Case var2ndKey
                    
                        Case "TransportDestination"
                    
                            strDestinationPathPart = .Item(var2ndKey)
                    
                        Case "Description"
                    
                            strClassifyingRuleName = .Item(var2ndKey)
                        Case Else
                    
                            intCountOfConditions = intCountOfConditions + 1
                    End Select
                Next
            End With
            
            Set objRowCol = New Collection
            
            With objRowCol
            
                .Add strClassifyingRuleName
             
                .Add strDestinationPathPart
                
                .Add intCountOfConditions
            
                If blnAddToOutlookPersonalOutlookBoxState Then
                
                    strMailFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & strDestinationPathPart
                
                    Set objFolder = GetMailFolder(strMailFolderPath, objNamespace.Folders)
                
                    If Not objFolder Is Nothing Then
                    
                        .Add True
                        
                        .Add objFolder.Items.Count
                    Else
                    
                        .Add False
                        
                        .Add Empty
                        
                        With robjNotExistedMailFolderPathDic
                        
                            If Not .Exists(strMailFolderPath) Then
                        
                                .Add strMailFolderPath, vstrPersonalOutlookBoxName
                            End If
                        End With
                    End If
                End If
            End With
            
            objDTCol.Add objRowCol
        Next
    End With
    
    
    If Not robjNotExistedMailFolderPathDic Is Nothing Then
    
        If robjNotExistedMailFolderPathDic.Count > 0 Then
        
            ' The user message prompt will be shown. if the user want it, the sub-folders are to be created.
        
            CreateMailFoldersFromMailFolderPathDicIfTheUserNeedsIts robjNotExistedMailFolderPathDic, objOutlookApplication
        End If
    End If
    
    
    Set objOutlookApplication = Nothing

    Set GetDetailConditionInformationOfMailTransporting = objDTCol
End Function

