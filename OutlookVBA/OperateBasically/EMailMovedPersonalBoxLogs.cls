VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "EMailMovedPersonalBoxLogs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   A data class of the outlook e-mail moved logs
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Outlook
'       Independent from Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 10/May/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjMainMovingProcessLogDic As Scripting.Dictionary ' get always on each E-mails moving


Private mblnAllowToGetMovingBasicMailLogAsTable As Boolean

Private mobjMovingBasicMailLogDTCol As Collection


Private mblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean

Private mobjExtractedMovedMailBodySomeInfoLogDTCol As Collection



'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub Class_Initialize()

    mblnAllowToGetMovingBasicMailLogAsTable = True
    
    mblnAllowToCollectMovedMailBodyHyperlinksLogAsTable = False
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get MainMovingProcessLogDic() As Scripting.Dictionary

    Set MainMovingProcessLogDic = mobjMainMovingProcessLogDic
End Property
Public Property Set MainMovingProcessLogDic(ByVal vobjMainMovingProcessLogDic As Scripting.Dictionary)

    Set mobjMainMovingProcessLogDic = vobjMainMovingProcessLogDic
End Property


'''
'''
'''
Public Property Get AllowToGetMovingBasicMailLogAsTable() As Boolean

    AllowToGetMovingBasicMailLogAsTable = mblnAllowToGetMovingBasicMailLogAsTable
End Property
Public Property Let AllowToGetMovingBasicMailLogAsTable(ByVal vblnAllowToGetMovingBasicMailLogAsTable As Boolean)

    mblnAllowToGetMovingBasicMailLogAsTable = vblnAllowToGetMovingBasicMailLogAsTable
End Property

'''
'''
'''
Public Property Get MovingBasicMailLogDTCol() As Collection

    Set MovingBasicMailLogDTCol = mobjMovingBasicMailLogDTCol
End Property
Public Property Set MovingBasicMailLogDTCol(ByVal vobjMovingBasicMailLogDTCol As Collection)

    Set mobjMovingBasicMailLogDTCol = vobjMovingBasicMailLogDTCol
End Property


'''
'''
'''
Public Property Get AllowToCollectMovedMailBodyHyperlinksLogAsTable() As Boolean

    AllowToCollectMovedMailBodyHyperlinksLogAsTable = mblnAllowToCollectMovedMailBodyHyperlinksLogAsTable
End Property
Public Property Let AllowToCollectMovedMailBodyHyperlinksLogAsTable(ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean)

    mblnAllowToCollectMovedMailBodyHyperlinksLogAsTable = vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable
End Property


'''
'''
'''
Public Property Get ExtractedMovedMailBodySomeInfoLogDTCol() As Collection

    Set ExtractedMovedMailBodySomeInfoLogDTCol = mobjExtractedMovedMailBodySomeInfoLogDTCol
End Property
Public Property Set ExtractedMovedMailBodySomeInfoLogDTCol(ByVal vobjExtractedMovedMailBodySomeInfoLogDTCol As Collection)

    Set mobjExtractedMovedMailBodySomeInfoLogDTCol = vobjExtractedMovedMailBodySomeInfoLogDTCol
End Property
