Attribute VB_Name = "UTfOperateOutlookForXl"
'
'   Output operated Microsoft Outlook results into Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 15/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToOutputOutlookMailItemsInbox()

    Dim objNamespace As Outlook.Namespace
    Dim objFolder As Outlook.Folder

    With New Outlook.Application
    
        Set objNamespace = .GetNamespace("MAPI")
        
        Set objFolder = objNamespace.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox)    ' Inbox
        
        msubOutputOutlookMailItemsInfoToTemporaryBook objFolder
    End With

End Sub
'''
'''
'''
Private Sub msubSanityTestToOutputOutlookMailFolderPaths()

    msubOutputOutlookMailFolderInfoToBook False
End Sub

'''
'''
'''
Private Sub msubSanityTestToOutputOutlookMailFolderPathsWithDetails()

    msubOutputOutlookMailFolderInfoToBook True
End Sub

'''
'''
'''
Private Sub msubSanityTestToOutputOutlookMailFolderPathsWithDetailsWhichIncludesAtLeastOneMail()

    msubOutputOutlookMailFolderInfoToBook True, True
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** Output Outlook collected information to Excel book
'**---------------------------------------------
'''
'''
'''
Private Sub msubOutputOutlookMailItemsInfoToTemporaryBook(ByVal vobjFolder As Outlook.Folder)

    Dim strBookPath As String
    
    strBookPath = mfstrGetTemporaryOutputBookDir() & "\CurrentOutlookMailItemsInformation.xlsx"

    msubOutputOutlookMailItemsInfoToBook vobjFolder, strBookPath
End Sub



'''
'''
'''
Private Sub msubOutputOutlookMailItemsInfoToBook(ByVal vobjFolder As Outlook.Folder, ByVal vstrBookPath As String)

    Dim objSheet As Excel.Worksheet, objBook As Excel.Workbook, strBookPath As String
    Dim objMailInfoCol As Collection, objCollectingDataStopWatch As DoubleStopWatch, objOutputSheetStopWatch As DoubleStopWatch
    
    
    Set objCollectingDataStopWatch = New DoubleStopWatch
    Set objOutputSheetStopWatch = New DoubleStopWatch
    
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(vstrBookPath)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    objSheet.Name = "MailItems"


    objCollectingDataStopWatch.MeasureStart
    
    Set objMailInfoCol = GetMailItemInfoColInAFolder(vobjFolder)

    objCollectingDataStopWatch.MeasureInterval
    
    
    objOutputSheetStopWatch.MeasureStart

    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, objMailInfoCol, mfobjGetDataTableSheetFormatterForOutlookMailItems()
    
    
    SortXlSheetBasically objSheet, "SenderName,Ascending,SenderEmailAddress,Ascending,ReceivedTime,Decending"

    SetMergedCellsToTable objSheet, GetColFromLineDelimitedChar("SenderName,SenderEmailAddress,NameOfSenderObject"), 1, 1, True

    objOutputSheetStopWatch.MeasureInterval
    

    AddAutoShapeGeneralMessage objSheet, mfstrGetLogOfOutlookMailFolder(vobjFolder, objCollectingDataStopWatch, objOutputSheetStopWatch)
    
    DeleteDummySheetWhenItExists objBook
End Sub


'''
'''
'''
Private Function mfstrGetLogOfOutlookMailFolder(ByVal vobjFolder As Outlook.Folder, ByVal vobjCollectingDataStopWatch As DoubleStopWatch, ByVal vobjOutputSheetStopWatch As DoubleStopWatch) As String

    Dim strLog As String
    
    strLog = "Logged time: " & Format(Now(), "ddd, yyyy/mm/dd hh:mm:ss") & vbNewLine
    
    strLog = strLog & "Collecting data elasped time: " & vobjCollectingDataStopWatch.ElapsedTimeByString & vbNewLine
    
    strLog = strLog & "Output Excel sheet elasped time: " & vobjOutputSheetStopWatch.ElapsedTimeByString & vbNewLine
    
    strLog = strLog & "User name: " & vobjFolder.Session.CurrentUser & vbNewLine
    
    strLog = strLog & "Folder path: " & vobjFolder.FolderPath & vbNewLine
    
    strLog = strLog & "Count of mail-items: " & vobjFolder.Items.Count & vbNewLine
    
    strLog = strLog & "Count of unread mail-items: " & vobjFolder.UnReadItemCount

    mfstrGetLogOfOutlookMailFolder = strLog
End Function



'''
'''
'''
Private Sub msubOutputOutlookMailFolderInfoToBook(Optional ByVal vblnCollectMailDetailInfo As Boolean = False, Optional ByVal vblnFilterOfIncludingAtLeastOneMail As Boolean = False)

    Dim objSheet As Excel.Worksheet, objBook As Excel.Workbook, strBookPath As String
    Dim objMailFolderInfoCol As Collection
    
    If vblnCollectMailDetailInfo Then
    
        strBookPath = mfstrGetTemporaryOutputBookDir() & "\CurrentOutlookMailFolderPathsWithDetails.xlsx"
    Else
        strBookPath = mfstrGetTemporaryOutputBookDir() & "\CurrentOutlookMailFolderPaths.xlsx"
    End If
    
    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
    
    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
    
    If vblnCollectMailDetailInfo Then
    
        objSheet.Name = "MailFolderDetails"
    Else
        objSheet.Name = "MailFolderPaths"
    End If
    
    Set objMailFolderInfoCol = GetMailFolderPathsWithSubFolders(vblnCollectMailDetailInfo, vblnFilterOfIncludingAtLeastOneMail)


    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, objMailFolderInfoCol, GetDataTableSheetFormatterForOutlookMailFolderPath(vblnCollectMailDetailInfo)

    If vblnFilterOfIncludingAtLeastOneMail Then
    
        SortXlSheetBasically objSheet, "Name,Ascending"
    
        AddAutoShapeGeneralMessage objSheet, "Filtered mail-folders by including at least one mail", AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
    End If

    DeleteDummySheetWhenItExists objBook
End Sub


'''
'''
'''
Private Function mfstrGetTemporaryOutputBookDir() As String

    Dim strPath As String
    
    strPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"

    ForceToCreateDirectory strPath

    mfstrGetTemporaryOutputBookDir = strPath
End Function


'''
'''
'''
Private Function mfobjGetDataTableSheetFormatterForOutlookMailItems() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Subject,40,SenderName,30,NameOfSenderObject,15,SenderEmailAddress,25,ReceivedTime,18,CountOfAttachments,8,BodyFormat,8,Size,8,UnRead,8")
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
    
        .RecordBordersType = RecordCellsGrayAll
        
        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
        
        With .ColumnsNumberFormatLocal
        
            .SetNumberFormatLocalAndApplyFieldTitles "ddd, yyyy/m/d hh:mm", GetColFromLineDelimitedChar("ReceivedTime")
        
        End With
        
'        .AllowToMergeCellsByContinuousSameValues = True
'        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
'        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("SenderName,SenderEmailAddress")
        
    End With
    
    Set mfobjGetDataTableSheetFormatterForOutlookMailItems = objDTSheetFormatter
End Function



