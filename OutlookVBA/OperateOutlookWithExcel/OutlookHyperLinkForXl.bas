Attribute VB_Name = "OutlookHyperLinkForXl"
'
'   Output results of get some informations from the body text of a Outlook.MailItem to Excel sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Outlook and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  9/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' About hyperlinks on an Outlook.MailItem object
'''
Public Sub msubSanityTestToOutputMailBodyHyperLinksOfASelectedMailToSheet()

    Dim objMailItem As Outlook.MailItem, objDTCol As Collection
    
    With New Outlook.Application
    
        Set objMailItem = GetSelectedOneMailItems(.ActiveExplorer)
        
        GetMailBodyHyperLinksAndSomeInfoDTColFromMail objDTCol, objMailItem
    End With

    OutputMailBodyHyperLinksAndSomeInfoDTColToSheet objDTCol, mfstrGetTemporaryMailBodyHyperLinksAndSomeInfoBookPath()
End Sub

'''
''' About hyperlinks on plural Outlook.MailItem objects of a folder
'''
Public Sub msubSanityTestToOutputMailBodyHyperLinksOfASelectedFolderToSheet()

    Dim objMailItem As Outlook.MailItem, objDTCol As Collection
    
    With New Outlook.Application
    
        For Each objMailItem In GetSelectedMailFolder(.ActiveExplorer).Items
                
            GetMailBodyHyperLinksAndSomeInfoDTColFromMail objDTCol, objMailItem
        Next
    End With

    OutputMailBodyHyperLinksAndSomeInfoDTColToSheet objDTCol, mfstrGetTemporaryMailBodyHyperLinksAndSomeInfoBookPath()
End Sub

'''
''' About hyperlinks, e-mail addresses, and UNC pathes on the mail-body of an Outlook.MailItem object
'''
Public Sub msubSanityTestToOutputExtractedMailBodySomeInfosOfASelectedMailToSheet()

    Dim objMailItem As Outlook.MailItem, objDTCol As Collection
    
    With New Outlook.Application
    
        Set objMailItem = GetSelectedOneMailItems(.ActiveExplorer)
        
        GetOneRowMultiLinesHyperLinksAndEmailAddressesAndUNCPathsAndSomeInfoDTColFromMail objDTCol, objMailItem
    End With

    OutputExtractedMailBodyInfosAndSomeInfoDTColToSheetWithPreparingBook objDTCol, mfstrGetTemporaryExtractedMailBodySomeInfosAndSomeInfoBookPath()
End Sub


'''
''' About hyperlinks, e-mail addresses, and UNC pathes on the mail-body of an Outlook.MailItem object
'''
Public Sub msubSanityTestToOutputExtractedMailBodySomeInfosOfASelectedFolderToSheet()

    Dim objMailItem As Outlook.MailItem, objDTCol As Collection
    
    With New Outlook.Application
    
        For Each objMailItem In GetSelectedMailFolder(.ActiveExplorer).Items
                
            GetOneRowMultiLinesHyperLinksAndEmailAddressesAndUNCPathsAndSomeInfoDTColFromMail objDTCol, objMailItem
        Next
    End With

    OutputExtractedMailBodyInfosAndSomeInfoDTColToSheetWithPreparingBook objDTCol, mfstrGetTemporaryExtractedMailBodySomeInfosAndSomeInfoBookPath()
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub OutputMailBodyHyperLinksAndSomeInfoDTColToSheet(ByRef robjDTCol As Collection, ByVal vstrOutputBookPath As String)

    Dim objSheet As Excel.Worksheet
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "MailBodyHyperlinks")
    
    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, robjDTCol, mfobjGetDTFormatterForMailBodyHyperLinksAndSomeInfo()
End Sub

'''
'''
'''
Public Sub OutputExtractedMailBodyInfosAndSomeInfoDTColToSheetWithPreparingBook(ByRef robjDTCol As Collection, ByVal vstrOutputBookPath As String)

    Dim objSheet As Excel.Worksheet
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "ExtractedMailBodyInfos")
    
    OutputExtractedMailBodyInfosAndSomeInfoDTColToSheet objSheet, robjDTCol
End Sub

'''
'''
'''
Public Sub OutputExtractedMailBodyInfosAndSomeInfoDTColToSheet(ByRef robjSheet As Excel.Worksheet, ByRef robjDTCol As Collection)

    OutputColToSheetRestrictedByDataTableSheetFormatter robjSheet, robjDTCol, mfobjGetDTFormatterForExtractedMailBodyInfosAndSomeInfo()
    
    AdjustRowHeightForIndividualCellLineFeedsCountOfSpecifiedColumns robjSheet, GetColFromLineDelimitedChar("MailBodyHyperlink")
End Sub

'''
'''
'''
Private Function mfstrGetTemporaryMailBodyHyperLinksAndSomeInfoBookPath()

    Dim strDir As String, strPath As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
    
    ForceToCreateDirectory strDir
    
    strPath = strDir & "\TempMailBodyHyperLinksAndSomeInfo.xlsx"

    mfstrGetTemporaryMailBodyHyperLinksAndSomeInfoBookPath = strPath
End Function

'''
'''
'''
Private Function mfstrGetTemporaryExtractedMailBodySomeInfosAndSomeInfoBookPath()

    Dim strDir As String, strPath As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
    
    ForceToCreateDirectory strDir
    
    strPath = strDir & "\TempMailBodyExtractedInfosAndSomeInfo.xlsx"

    mfstrGetTemporaryExtractedMailBodySomeInfosAndSomeInfoBookPath = strPath
End Function

'''
''' about hyperlinks
'''
Private Function mfobjGetDTFormatterForMailBodyHyperLinksAndSomeInfo() As DataTableSheetFormatter

    Dim objDTFormatter As DataTableSheetFormatter
    
    Set objDTFormatter = New DataTableSheetFormatter
    
    With objDTFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("MailBodyHyperlink,55," & GetFieldTitleToColumnWidthStringForMailTypicalProperties())
    
        .RecordBordersType = RecordCellsGrayAll
        
        
        .AllowToMergeCellsByContinuousSameValues = True
        
        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        Set .FieldTitlesToMergeCells = GetEnumeratorKeysColFromDic(GetTextToRealNumberDicFromLineDelimitedChar(GetFieldTitleToColumnWidthStringForMailTypicalProperties()))
        
    
        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
        
        With .ColumnsNumberFormatLocal
        
            .SetNumberFormatLocalAndApplyFieldTitles "ddd, yyyy/m/d hh:mm:ss", GetColFromLineDelimitedChar("ReceivedTime")
        End With
        
    End With
    
    Set mfobjGetDTFormatterForMailBodyHyperLinksAndSomeInfo = objDTFormatter
End Function

'''
''' about one-cell multi-lines of hyperlinks, e-mail addresses, and UNC pathes
'''
Private Function mfobjGetDTFormatterForExtractedMailBodyInfosAndSomeInfo() As DataTableSheetFormatter

    Dim objDTFormatter As DataTableSheetFormatter
    
    Set objDTFormatter = New DataTableSheetFormatter
    
    With objDTFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
    
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("MailBodyHyperlink,55,MailBodyEmailAddresses,35,MailBodyUNCPaths,40," & GetFieldTitleToColumnWidthStringForMailTypicalProperties())
    
        .RecordBordersType = RecordCellsGrayAll
        
        
        .AllowToMergeCellsByContinuousSameValues = True
        
        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        Set .FieldTitlesToMergeCells = GetEnumeratorKeysColFromDic(GetTextToRealNumberDicFromLineDelimitedChar(GetFieldTitleToColumnWidthStringForMailTypicalProperties()))
        
        
        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
        
        With .ColumnsNumberFormatLocal
        
            .SetNumberFormatLocalAndApplyFieldTitles "ddd, yyyy/m/d hh:mm:ss", GetColFromLineDelimitedChar("ReceivedTime")
        End With
        
        '.AllowToAutoFitRowsHeight = True
    End With
    
    Set mfobjGetDTFormatterForExtractedMailBodyInfosAndSomeInfo = objDTFormatter
End Function

