Attribute VB_Name = "OperateOutlookToClassifyForXl"
'
'   Tools to operate Microsoft Outlook using both Excel and Windows API, which serves Windows INI files
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on all of Outlook, Excel and Windows API(serving INI files)
'       Dependent on ADODB
'       Dependent on XlAdoConnector.cls
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat,  6/May/2023    Kalmclaeyd Tarclanus    Separated from OperateOutlookToClassify.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Align and decorate the section-key-value table of Excel book, which is compatible INI file data
'**---------------------------------------------
'''
'''
'''
Public Sub PadSameAboveValuesOnEmptyCellsForEachColumnOnWorksheetAboutSectionColumnAtActiveCell()

    Dim objRange As Excel.Range, objSheet As Excel.Worksheet

    Set objRange = ActiveCell

    With objRange
    
        PadSameAboveValuesOnEmptyCellsForEachColumnOnWorksheetAboutSectionColumn .Worksheet, .CurrentRegion.Row, .CurrentRegion.Column, True, True
    End With
    
    Set objSheet = objRange.Worksheet
    
    DecorateGridSimply objSheet, ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgMeiryo
End Sub

'''
'''
'''
Public Sub PadSameAboveValuesOnEmptyCellsForEachColumnOnWorksheetAboutSectionColumn(ByVal vobjSheet As Excel.Worksheet, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1, Optional ByVal vblnAllowToMergeCellsAfterPadding As Boolean = False, Optional ByVal vblnAllowToSetVerticalAlignmentTop As Boolean = False)

    PadSameAboveValuesOnEmptyCellsForEachColumnOnWorksheet vobjSheet, GetColAsDateFromLineDelimitedChar("Section"), vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vblnAllowToMergeCellsAfterPadding, vblnAllowToSetVerticalAlignmentTop
End Sub


'**---------------------------------------------
'** Ini file interface
'**---------------------------------------------
'''
'''
'''
Public Sub MoveMailsBySpecifiedDetailConditionsFromIniFileWithOutputtingResultLogToSheet(ByVal vstrSourceMailFolderPath As String, _
        ByVal vstrPersonalOutlookBoxName As String, _
        ByVal vstrIniPath As String, _
        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)

    Dim objSectionToKeyValueDic As Scripting.Dictionary

    ' !Attention - The Chinese Kanji characters should have been lost in Japanese locale Windows OS.

    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromIni(vstrIniPath)

    MoveMailsBySpecifiedDetailConditionsWithOutputtingResultLogToSheet vstrSourceMailFolderPath, vstrPersonalOutlookBoxName, objSectionToKeyValueDic, vblnAllowToGetMovingMailLogAsTable, vblnPreventExecutingToMoveMail
End Sub


'**---------------------------------------------
'** Move mail-items by detailed conditions
'**---------------------------------------------
'''
'''
'''
Public Sub MoveMailsBySpecifiedDetailConditionsFromExcelBookWithOutputtingResultLogToSheet(ByVal vstrSourceMailFolderPath As String, _
        ByVal vstrPersonalOutlookBoxName As String, _
        ByVal vstrBookPath As String, _
        Optional ByVal vstrSheetName As String = "DetailConditions", _
        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
        Optional ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean = False, _
        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)

    Dim objSectionToKeyValueDic As Scripting.Dictionary


    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(vstrBookPath, vstrSheetName)

    MoveMailsBySpecifiedDetailConditionsWithOutputtingResultLogToSheet vstrSourceMailFolderPath, vstrPersonalOutlookBoxName, objSectionToKeyValueDic, vblnAllowToGetMovingMailLogAsTable, vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable, vblnPreventExecutingToMoveMail
End Sub

'''
'''
'''
Public Sub MoveMailsBySpecifiedDetailConditionsFromExcelBook(ByVal vstrSourceMailFolderPath As String, _
        ByVal vstrPersonalOutlookBoxName As String, _
        ByVal vstrBookPath As String, _
        Optional ByVal vstrSheetName As String = "DetailConditions", _
        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
        Optional ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean = False, _
        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)

    Dim objSectionToKeyValueDic As Scripting.Dictionary, objEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs
    

    'Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBook(vstrBookPath, vstrSheetName)
    
    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(vstrBookPath, vstrSheetName)


    SetupEMailMovedPersonalBoxLogs objEMailMovedPersonalBoxLogs, vblnAllowToGetMovingMailLogAsTable, vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable

    MoveMailsBySpecifiedDetailConditions objEMailMovedPersonalBoxLogs, vstrSourceMailFolderPath, vstrPersonalOutlookBoxName, objSectionToKeyValueDic, vblnPreventExecutingToMoveMail
    
    
    ' output log to the Immediate-window
    
    OutputEMailMovedPersonalBoxLogsToImmediateWindow objEMailMovedPersonalBoxLogs
End Sub


'''
'''
'''
Public Sub MoveMailsBySpecifiedDetailConditionsWithOutputtingResultLogToSheet(ByVal vstrSourceMailFolderPath As String, _
        ByVal vstrPersonalOutlookBoxName As String, _
        ByRef robjSectionToKeyValueDic As Scripting.Dictionary, _
        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
        Optional ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean = False, _
        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)
        
    
    Dim strOutputBookPath As String, objEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs
    
    
    SetupEMailMovedPersonalBoxLogs objEMailMovedPersonalBoxLogs, vblnAllowToGetMovingMailLogAsTable, vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable
    
    MoveMailsBySpecifiedDetailConditions objEMailMovedPersonalBoxLogs, vstrSourceMailFolderPath, vstrPersonalOutlookBoxName, robjSectionToKeyValueDic, vblnPreventExecutingToMoveMail
    
    ' Output logs to a sheet
    
    strOutputBookPath = GetLoggingBookPathForMovingOutlookEMails(vblnPreventExecutingToMoveMail)

    OutputEMailMovedPersonalBoxLogsToSheet objEMailMovedPersonalBoxLogs, strOutputBookPath, vblnPreventExecutingToMoveMail, "Moving E-mails logs with detail conditions"
End Sub




'**---------------------------------------------
'** Visualize Excel sheet state, which has INI file structure, with Outlook Personal box
'**---------------------------------------------
'''
'''
'''
Public Function OutputMailTransportDetailINICompatibleExcelSheetConditionsToBook(ByVal vstrOutputBookPath As String, ByVal vstrDetailConditionsINICompatibleBookPath As String, Optional ByVal vstrINICompatibleSheetName As String = "DetailConditions", Optional ByVal vstrPersonalOutlookBoxName As String = "") As Excel.Worksheet

    Dim objSheet As Excel.Worksheet, objDTCol As Collection, objNotExistedMailFolderPathDic As Scripting.Dictionary
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "DetailTransportINI")
    
    Set objDTCol = GetDetailConditionInformationOfMailTransportingByExcelBook(objNotExistedMailFolderPathDic, vstrDetailConditionsINICompatibleBookPath, vstrINICompatibleSheetName, vstrPersonalOutlookBoxName)

    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, objDTCol, GetDTSheetFormatterForMailTransportDetailConditionInformation(vstrPersonalOutlookBoxName, True)

    Set OutputMailTransportDetailINICompatibleExcelSheetConditionsToBook = objSheet
End Function

'''
'''
'''
Public Function GetDetailConditionInformationOfMailTransportingByExcelBook(ByRef robjNotExistedMailFolderPathDic As Scripting.Dictionary, ByVal vstrDetailConditionsExcelBookPath As String, Optional ByVal vstrSheetName As String = "DetailConditions", Optional ByVal vstrPersonalOutlookBoxName As String = "") As Collection

    Dim objSectionToKeyValueDic As Scripting.Dictionary
    
    With New Scripting.FileSystemObject
    
        If .FileExists(vstrDetailConditionsExcelBookPath) Then
        
            'Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBook(vstrDetailConditionsExcelBookPath, vstrSheetName)
            
            Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(vstrDetailConditionsExcelBookPath, vstrSheetName)
        End If
    End With
    
    Set GetDetailConditionInformationOfMailTransportingByExcelBook = GetDetailConditionInformationOfMailTransporting(robjNotExistedMailFolderPathDic, objSectionToKeyValueDic, vstrPersonalOutlookBoxName)
End Function


'**---------------------------------------------
'** output INI file state information to Excel book
'**---------------------------------------------
'''
'''
'''
Public Function OutputMailTransportDetailINIConditionsToBook(ByVal vstrOutputBookPath As String, ByVal vstrDetailConditionsINIFilePath As String, Optional ByVal vstrPersonalOutlookBoxName As String = "") As Excel.Worksheet

    Dim objSheet As Excel.Worksheet, objDTCol As Collection, objNotExistedMailFolderPathDic As Scripting.Dictionary
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "DetailTransportINI")
    
    Set objDTCol = GetDetailConditionInformationOfMailTransportingByINIFile(objNotExistedMailFolderPathDic, vstrDetailConditionsINIFilePath, vstrPersonalOutlookBoxName)

    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, objDTCol, GetDTSheetFormatterForMailTransportDetailConditionInformation(vstrPersonalOutlookBoxName)

    Set OutputMailTransportDetailINIConditionsToBook = objSheet
End Function

'''
'''
'''
Public Function GetDTSheetFormatterForMailTransportDetailConditionInformation(Optional ByVal vstrPersonalOutlookBoxName As String = "", Optional ByVal vblnAllowToUseINIComatibleExcelSheet As Boolean = False) As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Dim objFormatConditionExpander As FormatConditionExpander
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        If vblnAllowToUseINIComatibleExcelSheet Then
        
            .FieldTitleInteriorType = ColumnNameInteriorOfExtractedTableFromSheet
        Else
            .FieldTitleInteriorType = ColumnNameInteriorOfLoadedINIFileAsTable
        End If
    
        .RecordBordersType = RecordCellsGrayAll
    
        If vstrPersonalOutlookBoxName <> "" Then
        
        
            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("ClassifyingRuleName,41,DestinationPathPart,40,CountOfConditions,12,IsFolderExisted,9,CountOfMails,9")
            
            
            Set .ColumnsFormatCondition = New ColumnsFormatConditionParam
            
            With .ColumnsFormatCondition
            
                Set objFormatConditionExpander = New FormatConditionExpander
            
                With objFormatConditionExpander
                
                    .AddTextContainCondition "False", FontDeepRedBgLightRed
                End With
            
                .FieldTitleToCellFormatCondition.Add "IsFolderExisted", objFormatConditionExpander
            End With
        Else
            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("ClassifyingRuleName,41,DestinationPathPart,40,CountOfConditions,12")
        End If
    
        .AllowToMergeCellsByContinuousSameValues = True
        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
        
        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("DestinationPathPart,CountOfConditions,IsFolderExisted,CountOfMails")
    
    End With

    Set GetDTSheetFormatterForMailTransportDetailConditionInformation = objDTSheetFormatter
End Function


'**---------------------------------------------
'** Prepare a detail condition INI file from Excel book
'**---------------------------------------------
'''
'''
'''
Public Function GetIniPathAfterOutputMailTransportDetailConditionsINIFileFromSettingExcelBook(ByVal vstrInputSectionToKeyValueBookPath As String, Optional ByVal vstrInputSheetName As String = "DetailConditions") As String

    Dim strIniPath As String, objSectionToKeyValueDic As Scripting.Dictionary
    
    With New Scripting.FileSystemObject
    
        strIniPath = .GetParentFolderName(vstrInputSectionToKeyValueBookPath) & "\" & .GetBaseName(vstrInputSectionToKeyValueBookPath) & ".ini"
        
        If .FileExists(strIniPath) Then
        
            .DeleteFile strIniPath, True
        End If
    End With


    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(vstrInputSectionToKeyValueBookPath, vstrInputSheetName)

    WriteIniFromSectionToKeyValueDic strIniPath, objSectionToKeyValueDic
    
    GetIniPathAfterOutputMailTransportDetailConditionsINIFileFromSettingExcelBook = strIniPath
End Function

