Attribute VB_Name = "OperateOutlookForXl"
'
'   Tools to operate Microsoft Outlook and output results to Excel work-sheet
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Outlook and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 15/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Moving outlook e-mails logs
'**---------------------------------------------
'''
''' simple e-mail sorting rule and output some results to a Excel.Worksheet object
'''
Public Sub MoveMailsToSpecifiedFolderFromDestinationConditionWithOutputtingResultLogToSheet(ByVal vstrSourceMailFolderPath As String, _
        ByVal vobjMailAddressConditionToSomeInfoDic As Scripting.Dictionary, _
        ByVal vobjConditionKeyToDescriptionDic As Scripting.Dictionary, _
        ByVal vstrConditionFieldTitlesDelimitedByComma As String, _
        ByVal vstrPersonalOutlookBoxName As String, _
        ByVal vobjDestinationPathPartCol As Collection, _
        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
        Optional ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean = False, _
        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)


    Dim objEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs, strOutputBookPath As String


    SetupEMailMovedPersonalBoxLogs objEMailMovedPersonalBoxLogs, vblnAllowToGetMovingMailLogAsTable, vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable

    MoveMailsToSpecifiedFolderFromDestinationCondition objEMailMovedPersonalBoxLogs, vstrSourceMailFolderPath, vobjMailAddressConditionToSomeInfoDic, vobjConditionKeyToDescriptionDic, vstrConditionFieldTitlesDelimitedByComma, vstrPersonalOutlookBoxName, vobjDestinationPathPartCol, vblnPreventExecutingToMoveMail

    ' Output logs to a sheet
    
    strOutputBookPath = GetLoggingBookPathForMovingOutlookEMails(vblnPreventExecutingToMoveMail)

    OutputEMailMovedPersonalBoxLogsToSheet objEMailMovedPersonalBoxLogs, strOutputBookPath, vblnPreventExecutingToMoveMail
End Sub

'''
'''
'''
Public Sub OutputEMailMovedPersonalBoxLogsToSheet(ByRef robjEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs, ByVal vstrOutputBookPath As String, Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False, Optional ByVal vstrTitleTextOfMainMovingProcessLog As String = "Moving E-mails logs")

    Dim objSheet As Excel.Worksheet, objExtractedHyperlinksSheet As Excel.Worksheet

    With robjEMailMovedPersonalBoxLogs
    
        If .AllowToGetMovingBasicMailLogAsTable Then

            Set objSheet = OutputMovingMailLogDTToSheet(.MovingBasicMailLogDTCol, vstrOutputBookPath)
            
            OutputDicToAutoShapeLogTextOnSheet objSheet, .MainMovingProcessLogDic, vstrTitleTextOfMainMovingProcessLog
            
            If vblnPreventExecutingToMoveMail Then
            
                AddAutoShapeGeneralMessage objSheet, "Now, their mails haven't been moved yet.", XlShapeTextLogInteriorFillGradientYellow
            End If
            
            If .AllowToCollectMovedMailBodyHyperlinksLogAsTable Then
            
                Set objExtractedHyperlinksSheet = GetNewWorksheetAfterSpecifiedSheets(objSheet, "ExtractedMailBodyTextInfos")
                
                OutputExtractedMailBodyInfosAndSomeInfoDTColToSheet objExtractedHyperlinksSheet, .ExtractedMovedMailBodySomeInfoLogDTCol
            End If
        Else

            OutputEMailMovedPersonalBoxLogsToImmediateWindow robjEMailMovedPersonalBoxLogs
        End If
    End With
End Sub



'''
'''
'''
Public Function OutputMovingMailLogDTToSheet(ByVal vobjMovingMailLogDTCol As Collection, ByVal vstrOutputBookPath As String) As Excel.Worksheet

    Dim objSheet As Excel.Worksheet
    
    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "MovingEMails")
    
    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, vobjMovingMailLogDTCol, mfobjGetDTSheetFormatterForMovingMailLogDT()

    SortXlSheetBasically objSheet, "SenderEmailAddress,Ascending,ReceivedTime,Decending"

    SetMergedCellsToTable objSheet, GetColFromLineDelimitedChar("DestinationPathPart,Description,SenderEmailAddress,SenderOfName,UnRead,Subject"), 1, 1, True

    Set OutputMovingMailLogDTToSheet = objSheet
End Function

'''
'''
'''
''' <Argument>vblnPreventExecutingToMoveMail: If the e-mail matching tests have been executed, then this will be True</Argument>
Public Function GetLoggingBookPathForMovingOutlookEMails(Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False) As String

    Dim strDir As String, strBookName As String, strPath As String
    
    strDir = GetCurrentBookOutputDir() & "\OutlookLogs"
    
    If vblnPreventExecutingToMoveMail Then
    
        strDir = strDir & "\CheckToMailDestinations"
        
        strBookName = "ChecksMailDestinations_" & Format(Now(), "yyyymmdd_hhmmss") & ".xlsx"
    Else
        strDir = strDir & "\MovedMailsLogs"
        
        strBookName = "MovedMails_" & Format(Now(), "yyyymmdd_hhmmss") & ".xlsx"
    End If
    
    ForceToCreateDirectory strDir
    
    strPath = strDir & "\" & strBookName
    
    GetLoggingBookPathForMovingOutlookEMails = strPath
End Function


'''
'''
'''
Public Function GetFieldTitleToColumnWidthStringForMailTypicalProperties() As String

    GetFieldTitleToColumnWidthStringForMailTypicalProperties = "SenderEmailAddress,41,SenderOfName,16,UnRead,10,ReceivedTime,20,Subject,20,BodyLeftPart,20"
End Function


'''
'''
'''
Private Function mfobjGetDTSheetFormatterForMovingMailLogDT() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter, objFormatConditionExpander As FormatConditionExpander
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("DestinationPathPart,39,Description,41," & GetFieldTitleToColumnWidthStringForMailTypicalProperties())
    
    
        
        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
        
        With .ColumnsNumberFormatLocal
        
            .SetNumberFormatLocalAndApplyFieldTitles "yyyy/m/d ""(""aaa"")"" hh:mm:ss", GetColFromLineDelimitedChar("ReceivedTime")
        End With
                
        Set .ColumnsFormatCondition = New ColumnsFormatConditionParam
        
        With .ColumnsFormatCondition
        
            Set objFormatConditionExpander = New FormatConditionExpander
            
            With objFormatConditionExpander
            
                .AddTextContainCondition "False", FontDeepGreenBgLightGreen
            End With
        
            .FieldTitleToCellFormatCondition.Add "UnRead", objFormatConditionExpander
        End With
        
        
'        .AllowToMergeCellsByContinuousSameValues = True
'        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
'
'        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("DestinationPathPart,Description,SenderEmailAddress,SenderOfName,UnRead,Subject")
    
        .RecordBordersType = RecordCellsGrayAll
        
    End With
    

    Set mfobjGetDTSheetFormatterForMovingMailLogDT = objDTSheetFormatter
End Function

'**---------------------------------------------
'** About outlook mail folders data-table
'**---------------------------------------------
'''
''' for Outlook mail Folder detail information
'''
Public Function GetDataTableSheetFormatterForOutlookMailFolderPath(Optional ByVal vblnCollectMailDetailInfo As Boolean = False) As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter
    
    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        If vblnCollectMailDetailInfo Then
        
            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Name,36,MailsCount,8,UnReadItemCount,8,FolderPath,86,EntryID,10,Description,10,DefaultItemType,10")
        Else
    
            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("MailFolderPaths,86")
        End If
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
    
        .RecordBordersType = RecordCellsGrayAll
    
    End With
    
    Set GetDataTableSheetFormatterForOutlookMailFolderPath = objDTSheetFormatter
End Function



'''
''' for Outlook mail Folder existence checks
'''
Public Function GetDataTableSheetFormatterForOutlookMailFolderExistence() As DataTableSheetFormatter

    Dim objDTSheetFormatter As DataTableSheetFormatter, objFormatConditionExpander As FormatConditionExpander

    Set objDTSheetFormatter = New DataTableSheetFormatter
    
    With objDTSheetFormatter
    
        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
        
        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("OutlookMailFolderPath,65,FolderExists,10,CountOfMails,11")
    
        
        Set .ColumnsFormatCondition = New ColumnsFormatConditionParam
        
        With .ColumnsFormatCondition
        
            Set objFormatConditionExpander = New FormatConditionExpander
            
            With objFormatConditionExpander
                
                .CondtionText = "False"
                
                .CellFormatConditionType = CellBackground.FormatConditionTextContain
                
                .CellColorArrangementType = FontDeepRedBgLightRed
            End With
        
            .FieldTitleToCellFormatCondition.Add "FolderExists", objFormatConditionExpander
        
        End With
        
        .RecordBordersType = RecordCellsGrayAll
    End With

    Set GetDataTableSheetFormatterForOutlookMailFolderExistence = objDTSheetFormatter
End Function


