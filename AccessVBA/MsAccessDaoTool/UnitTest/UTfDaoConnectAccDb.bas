Attribute VB_Name = "UTfDaoConnectAccDb"
'
'   Serving Microsoft Access db serving tests by DAO
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both DAO and Microsoft Access
'       Dependent on UTfAdoConnectAccDb.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 25/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** USetAdoOleDbConStrForAccdb form tests
'**---------------------------------------------
'''
''' delete testing SettingKeyName from the registory
'''
Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToAccessOleDbWithAccessDb()

    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToAccessDbKey"
    
    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToAccessDbWithPasswordKey"
End Sub

'''
''' Access Db, which is not locked by any password.
'''
Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToAccessDbForm()

    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess
    Dim strSettingKeyName As String, strDbPath As String
    
    
    strSettingKeyName = "TestAccOleDbToAccessDbKey"
    
    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess

    strDbPath = mfstrPrepareAccessDbPathAfterCreateSampleAccDbIfItDoesntExist()

    SetOleDbConnectionParametersByAccessOleDbToMsAccessDbForm blnIsRequestedToCancelAdoConnection, objADOConStrOfOleDbMsAccess, strSettingKeyName, strDbPath
End Sub

'''
''' Connect a password locked Access db
'''
Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToAccessDbFormWithPasswordLock()

    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess
    Dim strSettingKeyName As String, strDbPath As String
    
    
    strSettingKeyName = "TestAccOleDbToAccessDbWithPasswordKey"
    
    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess

    strDbPath = mfstrPreparePasswordLockedAccessDbPathAfterCreateSampleAccDbIfItDoesntExist("1234")

    SetOleDbConnectionParametersByAccessOleDbToMsAccessDbForm blnIsRequestedToCancelAdoConnection, objADOConStrOfOleDbMsAccess, strSettingKeyName, strDbPath, vstrOldTypeDbPassword:="1234"
End Sub

'''
''' Connect a password locked Access db
'''
Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToAccessDbFormWithPasswordLockByOpeningForm()

    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess
    Dim strSettingKeyName As String, strDbPath As String
    
    
    strSettingKeyName = "TestAccOleDbToAccessDbWithPasswordKey"
    
    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess

    strDbPath = mfstrPreparePasswordLockedAccessDbPathAfterCreateSampleAccDbIfItDoesntExist("1234")

    SetOleDbConnectionParametersByAccessOleDbToMsAccessDbForm blnIsRequestedToCancelAdoConnection, objADOConStrOfOleDbMsAccess, strSettingKeyName, strDbPath
End Sub


'''
'''
'''
Private Sub msubSanityTestToCreateAccessDB()

    Dim strDbPath As String

    strDbPath = mfstrPreparePasswordLockedAccessDbPathAfterCreateSampleAccDbIfItDoesntExist("1234")
End Sub

'**---------------------------------------------
'** About DAO SQL
'**---------------------------------------------
'''
''' Get the Microsoft Access Table list
'''
Private Sub msubSanityTestToConnectToAccDbAndSqlByDao01()

    Dim strDbPath As String, strSQL As String
    Dim objRSet As DAO.Recordset, objTableDef As DAO.TableDef
    
    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\TestDb01OfAccDbAdoConnector.accdb"

    With New DAO.DBEngine
        
        With .OpenDatabase(strDbPath, False)
    
            strSQL = "SELECT MSysObjects.Type, MSysObjects.Name, MSysObjects.Flags From MSysObjects WHERE MSysObjects.Type = 1 AND MSysObjects.Flags = 0 ORDER BY MSysObjects.Type, MSysObjects.Name;"
            
            ' The following occurs an error of the access authority of MSysObjects
            
            On Error Resume Next
            
            Set objRSet = .OpenRecordset(strSQL)
            
            If Err.Number <> 0 Then
                
                Debug.Print Err.Description
            End If
            
            On Error GoTo 0
            
            ' However, DAT.TableDef is able to use from Microsoft Excel VBA
            
            For Each objTableDef In .TableDefs
            
                Debug.Print objTableDef.Name
            Next
            
            .Close
        End With
    End With
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrPreparePasswordLockedAccessDbPathAfterCreateSampleAccDbIfItDoesntExist(ByRef rstrPassword As String) As String

    Dim strDbPath As String

    strDbPath = PrepareAccessDbAfterCreateSampleAccDbWithLockingPasswordIfItDoesntExist("TestFromVba02WithPassword.accdb", rstrPassword)
    
    mfstrPreparePasswordLockedAccessDbPathAfterCreateSampleAccDbIfItDoesntExist = strDbPath
End Function

'''
'''
'''
Private Function mfstrPrepareAccessDbPathAfterCreateSampleAccDbIfItDoesntExist() As String

    mfstrPrepareAccessDbPathAfterCreateSampleAccDbIfItDoesntExist = PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist("TestFromVba01.accdb")
End Function


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function PrepareAccessDbAfterCreateSampleAccDbWithLockingPasswordIfItDoesntExist(ByVal vstrDbName As String, ByVal vstrPassword As String) As String

    Dim strDbPath As String

    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\" & vstrDbName

    If Not FileExistsByVbaDir(strDbPath) Then
    
        GetPathAfterCreateSampleAccDb vstrDbName
        
        SetNewAccessPasswordUsingDao strDbPath, vstrPassword
    End If

    PrepareAccessDbAfterCreateSampleAccDbWithLockingPasswordIfItDoesntExist = strDbPath
End Function

'''
''' Dependent on DAO
'''
Private Sub SetNewAccessPasswordUsingDao(ByRef rstrDbPath As String, ByRef rstrPassword As String)

    With New DAO.DBEngine
        
        With .OpenDatabase(rstrDbPath, True)
    
            .NewPassword "", rstrPassword
        
            .Close
        End With
    End With
End Sub

'''
'''
'''
Private Sub msubSanityTestToCreateDaoDBEngine()

    Dim objDBEngine As DAO.DBEngine
    
    On Error Resume Next
    
    Set objDBEngine = CreateObject("DAO.DBEngine")
    
    If Err.Number <> 0 Then
    
        ' DAO.DBEngine is not supported at CreateObject
    
        Debug.Print Err.Description
    Else
        Debug.Print TypeName(objDBEngine)
        
        Debug.Assert False
    End If
End Sub



