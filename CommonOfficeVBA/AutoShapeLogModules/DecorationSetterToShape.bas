Attribute VB_Name = "DecorationSetterToShape"
'
'   Microsoft Office common library Shape tools
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Common functions among Excel, PowerPoint, Word
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 12/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToDecorationSetterToShape()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "DecorationSetterToShapeForXl"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////

'**---------------------------------------------
'** About general color enumeration types
'**---------------------------------------------
Public Function GetThemeColorIndexFromText(ByVal vstrThemeColorIndex As String) As Office.MsoThemeColorIndex

    Dim enmMsoThemeColorIndex As Office.MsoThemeColorIndex
    
    enmMsoThemeColorIndex = msoNotThemeColor

    Select Case vstrThemeColorIndex
    
        Case "ThemeColorAccent1"
        
            enmMsoThemeColorIndex = msoThemeColorAccent1
        
        Case "ThemeColorAccent2"
        
            enmMsoThemeColorIndex = msoThemeColorAccent2
        
        Case "ThemeColorAccent3"
        
            enmMsoThemeColorIndex = msoThemeColorAccent3
        
        Case "ThemeColorAccent4"
        
            enmMsoThemeColorIndex = msoThemeColorAccent4
        
        Case "ThemeColorAccent5"
        
            enmMsoThemeColorIndex = msoThemeColorAccent5
        
        Case "ThemeColorAccent6"
        
            enmMsoThemeColorIndex = msoThemeColorAccent6
        
        Case "ThemeColorDark1"
        
            enmMsoThemeColorIndex = msoThemeColorDark1
        
        Case "ThemeColorDark2"
        
            enmMsoThemeColorIndex = msoThemeColorDark2
        
        Case "ThemeColorLight1"
        
            enmMsoThemeColorIndex = msoThemeColorLight1
        
        Case "ThemeColorLight2"
        
            enmMsoThemeColorIndex = msoThemeColorLight2
            
    End Select
    

    GetThemeColorIndexFromText = enmMsoThemeColorIndex
End Function



'**---------------------------------------------
'** About auto-shape various enumeration types
'**---------------------------------------------
'''
'''
'''
Public Function GetOfficeShapeTypeName(ByVal venmShapeType As Office.MsoShapeType) As String

    Dim strTypeName As String

    strTypeName = ""

    Select Case venmShapeType
    
        Case Office.MsoShapeType.msoShapeTypeMixed  '  -2 (&HFFFFFFFE)
    
            strTypeName = "ShapeTypeMixed (-2)"
    
        Case Office.MsoShapeType.msoAutoShape   ' 1
    
            strTypeName = "AutoShape"
            
        Case Office.MsoShapeType.msoCallout ' 2
            
            strTypeName = "Callout"
            
        Case Office.MsoShapeType.msoFormControl ' 8
        
            strTypeName = "FormControl"
    
        Case Office.MsoShapeType.msoLine    ' 9
        
            strTypeName = "Line"
    
        Case Office.MsoShapeType.msoChart
    
            strTypeName = "Chart"
    
        Case Office.MsoShapeType.msoDiagram
        
            strTypeName = "Diagram"
        
        Case Office.MsoShapeType.msoTextBox
        
            strTypeName = "TextBox"
        
        Case Office.MsoShapeType.msoSmartArt
        
            strTypeName = "SmartArt"
    
        Case Else
        
            strTypeName = CStr(venmShapeType)
    
    End Select

    GetOfficeShapeTypeName = strTypeName
End Function


'''
'''
'''
Public Function GetOfficeAutoShapeTypeName(ByVal venmAutoShapeType As Office.MsoAutoShapeType) As String

    Dim strTypeName As String

    strTypeName = ""
    
    Select Case venmAutoShapeType
    
        Case Office.MsoAutoShapeType.msoShapeMixed    ' -2 (&HFFFFFFFE)
        
            strTypeName = "ShapeMixed (-2)"
    
        Case Office.MsoAutoShapeType.msoShapeNotPrimitive   ' &H8A
        
            strTypeName = "ShapeNotPrimitive"
    
        Case Office.MsoAutoShapeType.msoShapeRectangle  ' 1
    
            strTypeName = "ShapeRectangle"
            
        Case Office.MsoAutoShapeType.msoShapeParallelogram  ' 2
        
            strTypeName = "Parallelogram"
    
        Case Office.MsoAutoShapeType.msoShapeDiamond  ' 4
        
            strTypeName = "ShapeDiamond"
    
        Case Office.MsoAutoShapeType.msoShapeRoundedRectangle   '5
        
            strTypeName = "ShapeRoundedRectangle"
    
        Case Office.MsoAutoShapeType.msoShapeOctagon  ' 6
        
            strTypeName = "ShapeOctagon"
    
        Case Office.MsoAutoShapeType.msoShapeIsoscelesTriangle  ' 7
        
            strTypeName = "ShapeIsoscelesTriangle"
    
        Case Office.MsoAutoShapeType.msoShapeRightTriangle  ' 8
    
            strTypeName = "ShapeRightTriangle"
    
        Case Office.MsoAutoShapeType.msoShapeOval   ' 9 - Oval �ȉ~
        
            strTypeName = "ShapeOval"
    
        Case Office.MsoAutoShapeType.msoShapeHexagon    ' 10
    
            strTypeName = "ShapeHexagon"
        
        Case Office.MsoAutoShapeType.msoShapeCross  ' 11
        
            strTypeName = "ShapeCross"
            
        Case Office.MsoAutoShapeType.msoShapeRegularPentagon    ' 12
        
            strTypeName = "ShapeRegularPentagon"
            
        Case Office.MsoAutoShapeType.msoShapeCube   ' 14
        
            strTypeName = "ShapeCube"
    
        Case Office.MsoAutoShapeType.msoShapeFoldedCorner    ' 16 - Noting form
    
            strTypeName = "ShapeFoldedCorner"
    
        Case Office.MsoAutoShapeType.msoShapeActionButtonCustom
    
            strTypeName = "ShapeActionButtonCustom"
    
        Case Office.MsoAutoShapeType.msoShapeChartX
        
            strTypeName = "ShapeChartX"
    
        Case Office.MsoAutoShapeType.msoShapeVerticalScroll     ' 101 (&H65) - Vertical scroll

            strTypeName = "ShapeVerticalScroll"
    
        Case Office.MsoAutoShapeType.msoShapeRectangularCallout ' 105 (&H69)
        
            strTypeName = "ShapeRectangularCallout"
            
        Case Else
        
            strTypeName = CStr(venmAutoShapeType)
            
    End Select

    GetOfficeAutoShapeTypeName = strTypeName
End Function



Public Function GetOfficeShapeStyleIndexTypeName(ByVal venmShapeStyleIndex As Office.MsoShapeStyleIndex) As String

    Dim strTypeName As String

    strTypeName = ""
    
    Select Case venmShapeStyleIndex

        Case Office.MsoShapeStyleIndex.msoShapeStyleMixed   ' -2 (&HFFFFFFFE)

            strTypeName = "ShapeStyleMixed (-2)"

        Case Office.MsoShapeStyleIndex.msoShapeStyleNotAPreset  ' 0
        
            strTypeName = "ShapeStyleNotAPreset (0)"

        
        Case Office.MsoShapeStyleIndex.msoShapeStylePreset1 ' 1
        
            strTypeName = "ShapeStylePreset1"
            
        Case Office.MsoShapeStyleIndex.msoShapeStylePreset2 ' 2
        
            strTypeName = "ShapeStylePreset2"
            
        Case Office.MsoShapeStyleIndex.msoShapeStylePreset9 ' 9
        
            strTypeName = "ShapeStylePreset9"
            

        Case Office.MsoShapeStyleIndex.msoLineStylePreset1 ' 10001 (&H2711)
        
            strTypeName = "LineStylePreset1"
        
        Case Office.MsoShapeStyleIndex.msoLineStylePreset2 ' 10002 (&H2712)

            strTypeName = "LineStylePreset2"
            
        Case Else

            strTypeName = CStr(venmShapeStyleIndex)

    End Select
    
    GetOfficeShapeStyleIndexTypeName = strTypeName
End Function

