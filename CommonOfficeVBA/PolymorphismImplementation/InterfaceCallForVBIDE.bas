Attribute VB_Name = "InterfaceCallForVBIDE"
'
'   Interface VBIDE call for implementing polymorphism (Call-back function)
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on VBIDE
'       Dependent on InterfaceCall.bas
'       Dependent on IVBComponentIdentifier.cls interfaces
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon,  5/Jun/2023    Kalmclaeyd Tarclanus    Separated from InterfaceCall.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = True

#Const HAS_EXCEL_REF = False

#Const HAS_WORD_REF = False

#Const HAS_ACCESS_REF = False

#Const HAS_POWERPOINT_REF = False

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** VBProject menu-debug compile
'**---------------------------------------------
'''
'''
'''
''' <Argument>vobjOfficeFile: Excel.Workbook object or Word.Document object, PowerPoint.Presentation object</Argument>
Public Sub TryToCompieForOfficeObject(ByVal vobjOfficeFile As Object)

#If HAS_REF Then

    Dim objVBE As VBIDE.VBE
#Else
    Dim objVBE As Object
#End If
    With vobjOfficeFile

        AppActivate .Name
    
        Set objVBE = .Application.VBE
        
        With objVBE
        
            With .MainWindow
            
                If Not .Visible Then
                
                    ' Open VBIDE (Visual Basic Editor)
                
                    .Visible = True
                End If
            
                .SetFocus
            End With
        End With
        
        AppActivate .Name
    
        Set objVBE.ActiveVBProject = .VBProject
    
        On Error Resume Next
    
        Debug.Print "Try to compile VBA [" & vobjOfficeFile.VBProject.Name & "] of " & vobjOfficeFile.Name
    
        With New DoubleStopWatch
        
            .MeasureStart
        
            objVBE.CommandBars.FindControl(, 578).Execute
        
            .MeasureInterval
            
            Debug.Print "VBA [" & vobjOfficeFile.VBProject.Name & "] Compile elasped time: " & .ElapsedTimeByString
        End With
        
        If Err.Number <> 0 Then
        
            Debug.Print "Compile error occurd: " & Err.Description & ", Error number: " & CStr(Err.Number)
        End If
        
        On Error GoTo 0
    End With
End Sub

'**---------------------------------------------
'** VBProject object selecting operations
'**---------------------------------------------
'''
'''
'''
#If HAS_REF Then
Public Function GetThisOfficeFileVBProject() As VBIDE.VBProject

#Else
Public Function GetThisOfficeFileVBProject() As Object

#End If
    Set GetThisOfficeFileVBProject = GetVBProjectFromFilePath(GetCurrentOfficeFileObject().FullName)
End Function



'''
''' If VBProject object model is not trusted from each user, (The "Trust access to the VBA project object model" check box hasn't been enabled), the following occur error.
''' VBIDE doesn't support Outlook.
'''
#If HAS_REF Then

Public Function GetVBProjectFromFilePath(ByVal vstrExecutingApplicationPath As String, _
        Optional ByVal vblnReadOnly As Boolean = False, _
        Optional ByVal vblnOpenWithPreventingEvents As Boolean = False, _
        Optional ByVal vblnAddToMru As Boolean = False) As VBIDE.VBProject


    Dim objFS As Scripting.FileSystemObject, strExtension As String
    Dim objVBProject As VBIDE.VBProject
#Else

Public Function GetVBProjectFromFilePath(ByVal vstrExecutingApplicationPath As String, _
        Optional ByVal vblnReadOnly As Boolean = False, _
        Optional ByVal vblnOpenWithPreventingEvents As Boolean = False, _
        Optional ByVal vblnAddToMru As Boolean = False) As Object

    Dim objFS As Object, strExtension As String
    Dim objVBProject As Object
#End If

#If HAS_EXCEL_REF Then

    Dim objExcelBook As Excel.Workbook
#Else
    Dim objExcelBook As Object
#End If
#If HAS_WORD_REF Then

    Dim objWordDoc As Word.Document
#Else
    Dim objWordDoc As Object
#End If
#If HAS_ACCESS_REF Then

    Dim objAccessProject As Access.CurrentProject
#Else
    Dim objAccessProject As Object
#End If
#If HAS_POWERPOINT_REF Then

    Dim objPresentation As PowerPoint.Presentation
#Else
    Dim objPresentation As Object
#End If
    
    Dim objApplicationSomeInstance As Object, objApplication As Object
    
    
    Set objVBProject = Nothing
    
    With CreateObject("Scripting.FileSystemObject")
    
        Select Case LCase(.GetExtensionName(vstrExecutingApplicationPath))
        
            Case "xlsm", "xls", "xlsb", "xlt", "xltm", "xltm", "xlsx"
                ' Excel.Application
                
                Set objExcelBook = GetMsExcelWorkbookByInterface(vstrExecutingApplicationPath, _
                        vblnReadOnly, _
                        "", _
                        vblnOpenWithPreventingEvents, _
                        vblnAddToMru)
                
                Set objVBProject = objExcelBook.VBProject
                
            Case "docm", "doc", "docb", "dot", "dotm", "docx"
                ' Word.Application
    
                Set objWordDoc = GetMsWordDocument(vstrExecutingApplicationPath)
                
                Set objVBProject = objWordDoc.VBProject
                
            Case "pptm", "ppt", "pptb", "pot", "potm"
                ' PowerPoint.Application
                
                Set objPresentation = GetMsPowerPointPresentation(vstrExecutingApplicationPath)
                
                Set objVBProject = objPresentation.VBProject
            
            Case "mdb", "accdb"
                ' Access Database
                On Error Resume Next
                
                Set objApplication = Nothing
                
                Set objApplication = GetObject(vstrExecutingApplicationPath)
                
                On Error GoTo 0
                
                ' The following is not supported in Access 2019 and before
                Set objVBProject = objApplication.VBE.VBProjects.ActiveVBProject
                
            Case Else
                ' Try to GetObject
                
                On Error Resume Next
                
                Set objApplicationSomeInstance = Nothing
                
                Set objApplicationSomeInstance = GetObject(vstrExecutingApplicationPath)
                
                On Error GoTo 0
                
                If Not objApplicationSomeInstance Is Nothing Then
                
                    Set objApplication = objApplicationSomeInstance.Application
                    
                    If Not objApplication Is Nothing Then
                    
                        Set objVBProject = objApplication.VBProject
                    End If
                    
                End If
        End Select
    End With

    Set GetVBProjectFromFilePath = objVBProject
End Function


'''
'''
'''
''' <Argument>vobjOfficeFilePathCol: file path either Excel, Word, or PowerPoint</Argument>
Public Function GetVBProjectColFromOfficeFilePathCol(ByVal vobjOfficeFilePathCol As Collection) As Collection
    
    Dim objCol As Collection, varFilePath As Variant, strFilePath As String
    Dim objVBProject As VBIDE.VBProject
    
    Set objCol = New Collection
    
    For Each varFilePath In vobjOfficeFilePathCol
    
        strFilePath = varFilePath
    
        Set objVBProject = GetVBProjectFromFilePath(strFilePath)
    
        If Not objVBProject Is Nothing Then
        
            objCol.Add objVBProject
        End If
    Next

    Set GetVBProjectColFromOfficeFilePathCol = objCol
End Function


'''
'''
'''
''' <Argument>rstrFileName: Output</Argument>
''' <Argument>robjVBComponent: Output</Argument>
''' <Argument>vstrModuleName: Input</Argument>
Public Sub GetFileNameAndVBComponentFromCurrentOfficeFileModuleName(ByRef rstrFileName As String, _
        ByRef robjVBComponent As VBIDE.VBComponent, _
        ByVal vstrModuleName As String)


    Dim objVBProject As VBIDE.VBProject
    
    With GetCurrentOfficeFileObject()
    
        rstrFileName = .Name
        
        Set objVBProject = .VBProject
    End With
    
    Set robjVBComponent = objVBProject.VBComponents.Item(vstrModuleName)
End Sub


'''
''' This can also support the no saved new macro Office file
'''
Public Function GetVBProjectFileNameToVBProjectDic() As Scripting.Dictionary

    Dim strLowerApplicationName  As String, objDic As Scripting.Dictionary
    Dim objApplication As Object ' Excel.Application, Word.Application, or PowerPoint.Application

#If HAS_EXCEL_REF Then
    Dim objBook As Excel.Workbook
#Else
    Dim objBook As Object
#End If
#If HAS_WORD_REF Then
    Dim objDocument As Word.Document
#Else
    Dim objDocument As Object
#End If
#If HAS_POWERPOINT_REF Then
    Dim objPresentation As PowerPoint.Presentation
#Else
    Dim objPresentation As Object
#End If
#If HAS_ACCESS_REF Then
    Dim objAccessProject As Access.CurrentProject
#Else
    Dim objAccessProject As Object
#End If

    Set objApplication = GetCurrentOfficeFileObject().Application
    
    strLowerApplicationName = LCase(Trim(Replace(objApplication.Name, "Microsoft", "")))
    
    Set objDic = New Scripting.Dictionary
    
    Select Case strLowerApplicationName
    
        Case "excel"
    
            For Each objBook In objApplication.Workbooks
            
                'Debug.Print objBook.Name
                
                objDic.Add objBook.Name, objBook.VBProject
            Next
    
        Case "word"
        
            For Each objDocument In objApplication.Documents
        
                objDic.Add objDocument.Name, objDocument.VBProject
            Next
            
        Case "powerpoint"
    
            For Each objPresentation In objApplication.Presentations
            
                objDic.Add objPresentation.Name, objPresentation.VBProject
            Next
    
        Case "access"
        
            ' Need to test...
            
            Set objAccessProject = GetCurrentOfficeFileObject()
        
            objDic.Add objAccessProject.Name, objAccessProject.VBE.VBProjects.Item(1)
    End Select
    
    Set GetVBProjectFileNameToVBProjectDic = objDic
End Function


'''
''' Inverse GetVBProjectFileNameToVBProjectDic
'''
Public Function GetVBProjectToVBProjectFileNameDic() As Scripting.Dictionary

    Dim varFileName As Variant, strFileName As String, objVBProject As VBIDE.VBProject
    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary

    With GetVBProjectFileNameToVBProjectDic
    
        For Each varFileName In .Keys
        
            strFileName = varFileName
            
            Set objVBProject = .Item(varFileName)
            
            objDic.Add objVBProject, strFileName
        Next
    
    End With

    Set GetVBProjectToVBProjectFileNameDic = objDic
End Function

'**---------------------------------------------
'** Tools for IVBComponentIdentifier
'**---------------------------------------------
'''
'''
'''
Public Function GetVBComponentWithOfficeFileName(ByRef ritfVBComponentIdentifier As IVBComponentIdentifier) As VBIDE.VBComponent

    Dim objVBProject As VBIDE.VBProject, objVBComponent As VBIDE.VBComponent

    With GetVBProjectFileNameToVBProjectDic()
    
        Set objVBProject = .Item(ritfVBComponentIdentifier.OfficeFileName)
    End With
    
    'Debug.Print ritfVBComponentIdentifier.ModuleName
    
    Set objVBComponent = objVBProject.VBComponents.Item(ritfVBComponentIdentifier.ModuleName)
    
    Set GetVBComponentWithOfficeFileName = objVBComponent
End Function

'''
'''
'''
Public Function GetExtensionOfVBComponentWithOfficeFileName(ByRef ritfVBComponentIdentifier As IVBComponentIdentifier) As String

    GetExtensionOfVBComponentWithOfficeFileName = GetDotExtensionFromVBAComponentType(GetVBComponentWithOfficeFileName(ritfVBComponentIdentifier).Type)
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetVBProjectFileNameToVBProjectDic()

    Dim objDic As Scripting.Dictionary
    
    Set objDic = GetVBProjectFileNameToVBProjectDic()
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetAccessCurrentProjectForExistedFile()

    ' Acculately, execute test before generate an Access DB (.accdb) file
#If HAS_ACCESS_REF Then

    Dim objAccessProject As Access.CurrentProject
#Else
    Dim objAccessProject As Object
#End If

    Dim strAccessDBPath As String, objVBProject As VBIDE.VBProject
    
    With GetCurrentOfficeFileObject()
    
        strAccessDBPath = .Path & "\ContinuousDevelopDB.accdb"
    End With

    Set objAccessProject = GetMsAccessCurrentProject(strAccessDBPath)

    Debug.Print objAccessProject.Name

    Set objVBProject = objAccessProject.Application.VBE.ActiveVBProject

    Debug.Print objVBProject.fileName
End Sub



'''
''' Test code , which is dependent on X: drive
'''
Private Sub msubSanityTestToGetPoworPointPresentationForExistedFile()

    ' Acculately, execute test before generate a PowerPoint presentation (.pptx) file
    
#If HAS_POWERPOINT_REF Then

    Dim objPresentation As PowerPoint.Presentation
#Else
    Dim objPresentation As Object
#End If

    Dim strPresentationPath As String, objVBProject As VBIDE.VBProject
    
    With GetCurrentOfficeFileObject()
    
        strPresentationPath = .Path & "\ContinuousDevelopPresentation.pptm"
    End With

    Set objPresentation = GetMsPowerPointPresentation(strPresentationPath)

    Debug.Print objPresentation.Name

    Set objVBProject = objPresentation.VBProject

    Debug.Print objVBProject.fileName
End Sub



'''
'''
'''
Private Sub msubSanityTestToGetWordDocumentForExistedFile()

    ' Acculately, execute test before generate a Word document (.docx) file
#If HAS_WORD_REF Then

    Dim objDoc As Word.Document
#Else
    Dim objDoc As Object
#End If

    Dim strDocPath As String, objVBProject As VBIDE.VBProject
    
    With GetCurrentOfficeFileObject()
    
        strDocPath = .Path & "\ContinuousDevelopDoc.docm"
    End With

    Set objDoc = GetMsWordDocument(strDocPath)

    Debug.Print objDoc.Name

    Set objVBProject = objDoc.VBProject

    Debug.Print objVBProject.fileName
End Sub


