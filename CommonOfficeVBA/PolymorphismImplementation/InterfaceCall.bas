Attribute VB_Name = "InterfaceCall"
'
'   Interface call for implementing polymorphism (Call-back function)
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 26/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = True   ' Except for Microsoft Office objects

#Const HAS_EXCEL_REF = False

#Const HAS_WORD_REF = False

#Const HAS_ACCESS_REF = False

#Const HAS_POWERPOINT_REF = False


'///////////////////////////////////////////////
'/// Windows API Declarations
'///////////////////////////////////////////////

#If VBA7 Then
    Private Declare PtrSafe Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As LongPtr
#Else
    Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
#End If
    

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
'**---------------------------------------------
'** Solute file-system path by the Windows registry
'**---------------------------------------------
Private Const mstrModuleKey As String = "InterfaceCall"   ' 1st part of registry sub-key

Private Const mstrSubjectOfficeTemplatesRootDirKey As String = "OfficeTemplatesRootDir"   ' 2nd part of registry sub-key, recommend network drive

Private Const mstrSubjectWhenShowedFileDialogInitialDirKey As String = "FileDialogInitialDir"   ' 2nd part of registry sub-key, recommend network drive

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjCurrentOfficeFileObject As Object    ' Excel.Workbook, Word.Document, Access.Database, PowerPoint.Presentation, OutlookSession


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' save location test enumerated values, after the existed macro enabled office file have been edited
'''
Public Enum ExistedMacroBookSaveDestinationPath
    
    SaveSameLocation
    
    SaveSubDirectoryLocationOfTheBook
End Enum



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' This supports not only Excel.Application, but supports the execution on Word, Access, PowerPoint, and Outlook
'''
Public Function GetCurrentOfficeFileObject() As Object

    Dim objOfficeObject As Object
        
    If Not mobjCurrentOfficeFileObject Is Nothing Then
    
        Set GetCurrentOfficeFileObject = mobjCurrentOfficeFileObject
    Else
    
        Set objOfficeObject = Nothing
        
        Set objOfficeObject = mfobjGetOfficeFileObjectFromApplicationKeyword()
    
        If Not objOfficeObject Is Nothing Then
        
            Set mobjCurrentOfficeFileObject = objOfficeObject
        End If
        
        Set GetCurrentOfficeFileObject = mobjCurrentOfficeFileObject
    End If
End Function

'''
'''
'''
Public Function GetParentDirectoryPathOfCurrentOfficeFile() As String

    GetParentDirectoryPathOfCurrentOfficeFile = GetCurrentOfficeFileObject().Path
End Function

'''
'''
'''
Public Function GetFileNameOfCurrentOfficeFile() As String

    GetFileNameOfCurrentOfficeFile = GetCurrentOfficeFileObject().Name
End Function


'''
''' Get Excel.Application, Word.Application, Access.Application, PowerPoint.Application, Outlook.Application
'''
''' If more than one Excel application started by multi-processes on Windows, The single 'Application' keyword may often causes some unexpected errors.
'''
Public Function GetApplicationFromFilePath(ByVal vstrExecutingApplicationPath As String) As Object

#If HAS_EXCEL_REF Then
    Dim objExcelBook As Excel.Workbook
#Else
    Dim objExcelBook As Object
#End If
#If HAS_WORD_REF Then
    Dim objWordDoc As Word.Document
#Else
    Dim objWordDoc As Object
#End If
#If HAS_ACCESS_REF Then
    Dim objAccessProject As Access.CurrentProject
#Else
    Dim objAccessProject As Object
#End If
#If HAS_POWERPOINT_REF Then
    Dim objPresentation As PowerPoint.Presentation
#Else
    Dim objPresentation As Object
#End If
    
    Dim objApplicationSomeInstance As Object, strExtension As String
    Dim objApplication As Object
    
    
    Set GetApplicationFromFilePath = Nothing
    
    With CreateObject("Scripting.FileSystemObject")
    
        Select Case LCase(.GetExtensionName(vstrExecutingApplicationPath))
            
            Case "xlsm", "xls", "xlsb", "xlt", "xltm", "xltm", "xlsx"
                ' Excel.Application
                
                Set objExcelBook = GetMsExcelWorkbookByInterface(vstrExecutingApplicationPath)
                
                Set objApplication = objExcelBook.Application
                
            Case "docm", "doc", "docb", "dot", "dotm", "docx"
                ' Word.Application
    
                Set objWordDoc = GetMsWordDocument(vstrExecutingApplicationPath)
                
                Set objApplication = objWordDoc.Application
                
            Case "pptm", "ppt", "pptb", "pot", "potm"
                ' PowerPoint.Application
                
                Set objPresentation = GetMsPowerPointPresentation(vstrExecutingApplicationPath)
                
                Set objApplication = objPresentation.Application
            
            Case "mdb", "accdb"
                ' Access Database
                On Error Resume Next
                
                Set objApplication = Nothing
                
                Set objApplication = GetObject(vstrExecutingApplicationPath)
                
                On Error GoTo 0
                
            Case Else
                ' Try to GetObject
                
                On Error Resume Next
                
                Set objApplicationSomeInstance = Nothing
                
                Set objApplicationSomeInstance = GetObject(vstrExecutingApplicationPath)
                
                On Error GoTo 0
                
                If Not objApplicationSomeInstance Is Nothing Then
                
                    Set objApplication = objApplicationSomeInstance.Application
                End If
        End Select
    End With

    Set GetApplicationFromFilePath = objApplication
End Function

'''
'''
'''
Public Function GetOfficeFileFromPath(ByVal vstrPath As String) As Object

#If HAS_EXCEL_REF Then
    Dim objExcelBook As Excel.Workbook
#Else
    Dim objExcelBook As Object
#End If
#If HAS_WORD_REF Then
    Dim objWordDoc As Word.Document
#Else
    Dim objWordDoc As Object
#End If
#If HAS_ACCESS_REF Then
    Dim objAccessProject As Access.CurrentProject
#Else
    Dim objAccessProject As Object
#End If
#If HAS_POWERPOINT_REF Then
    Dim objPresentation As PowerPoint.Presentation
#Else
    Dim objPresentation As Object
#End If

    Dim objOfficeFileObject As Object

    Set objOfficeFileObject = Nothing

    Select Case LCase(GetExtensionNameByVbaDir(vstrPath))
            
        Case "xlsm", "xls", "xlsb", "xlt", "xltm", "xltm", "xlsx"
            ' Excel.Application
            
            Set objExcelBook = GetMsExcelWorkbookByInterface(vstrPath)
            
            Set objOfficeFileObject = objExcelBook
            
        Case "docm", "doc", "docb", "dot", "dotm", "docx"
            ' Word.Application

            Set objWordDoc = GetMsWordDocument(vstrPath)
            
            Set objOfficeFileObject = objWordDoc
            
        Case "pptm", "ppt", "pptb", "pot", "potm"
            ' PowerPoint.Application
            
            Set objPresentation = GetMsPowerPointPresentation(vstrPath)
        
            Set objOfficeFileObject = objPresentation
            
        Case "mdb", "accdb"
            ' Access Database
            On Error Resume Next
            
            
            
            On Error GoTo 0
            
        Case Else
            ' Try to GetObject
            
            On Error Resume Next
            
            Set objOfficeFileObject = GetObject(vstrPath)
            
            On Error GoTo 0
    End Select

    Set GetOfficeFileFromPath = objOfficeFileObject
End Function



'**---------------------------------------------
'** Serve AddIn
'**---------------------------------------------
'''
'''
'''
Public Sub SaveOfficeFileWithSaveDestinationOptions(ByVal vobjExistedOfficeFile As Object, _
        Optional ByVal venmExistedMacroBookSaveDestinationPath As ExistedMacroBookSaveDestinationPath = ExistedMacroBookSaveDestinationPath.SaveSubDirectoryLocationOfTheBook)

    Dim strPath As String, objApplication As Object  ' Excel.Application
    Dim blnDisplayAlert As Boolean
    Dim objFS As Scripting.FileSystemObject

    If venmExistedMacroBookSaveDestinationPath = SaveSameLocation Then
    
        On Error Resume Next
    
        vobjExistedOfficeFile.Save
        
        On Error GoTo 0
    Else
        Set objFS = New Scripting.FileSystemObject

        ' When some reference dendency exists between the controling macro-book and the code operated macro-book,
        ' The macro-book changed codes (or AddIn file) will be failed to save
        strPath = GetSaveMacroFilePathFromOfficeFile(vobjExistedOfficeFile, objFS, venmExistedMacroBookSaveDestinationPath)
    
        Set objApplication = vobjExistedOfficeFile.Application
        
        With vobjExistedOfficeFile
                
            blnDisplayAlert = objApplication.DisplayAlerts
                    
            objApplication.DisplayAlerts = False
            
            On Error GoTo ErrHandler:
            
            Select Case LCase(objFS.GetExtensionName(vobjExistedOfficeFile.Name))
            
                Case "xlam"
            
                    .SaveAs strPath, XlFileFormat.xlOpenXMLAddIn
            
                Case "xla"
            
                    .SaveAs strPath, XlFileFormat.xlAddIn
                    
                Case Else
                    .SaveAs strPath
            End Select
ErrHandler:
            objApplication.DisplayAlerts = blnDisplayAlert
        End With
    End If
End Sub


'''
'''
'''
Private Function GetSaveMacroFilePathFromOfficeFile(ByVal vobjOfficeFile As Object, ByVal vobjFS As Scripting.FileSystemObject, _
        ByVal venmExistedMacroBookSaveDestinationPath As ExistedMacroBookSaveDestinationPath) As String

    Dim strExistedPath As String, strSavePath As String
    Dim strExistedDir As String, strDir As String
    Dim strExtension As String, strSubDir As String
    
    With vobjOfficeFile
        
        strExistedPath = .Path & "\" & .Name
        
        strExistedDir = .Path
        
        strExtension = LCase(vobjFS.GetExtensionName(.Name))
    End With

    Select Case strExtension
    
        Case "xlam", "xla"
        
            strSubDir = "ChangedAddInBooks"
        
        Case "xlsm", "xls"
    
            strSubDir = "ChangedMacroBooks"
    
        Case Else
        
            strSubDir = "ChangedMacroOfficeFiles"
    End Select

    
    strSavePath = ""
    
    Select Case venmExistedMacroBookSaveDestinationPath
    
        Case ExistedMacroBookSaveDestinationPath.SaveSubDirectoryLocationOfTheBook
    
            strDir = strExistedDir & "\" & strSubDir
    
            If Not vobjFS.FolderExists(strDir) Then
            
                vobjFS.CreateFolder strDir
            End If
    
            strSavePath = strDir & "\" & vobjOfficeFile.Name
    
        Case ExistedMacroBookSaveDestinationPath.SaveSameLocation
    
            strSavePath = strExistedPath
    End Select
    
    GetSaveMacroFilePathFromOfficeFile = strSavePath
End Function


'**---------------------------------------------
'** File dialog interface
'**---------------------------------------------
'''
'''
'''
Public Function GetFilePathByApplicationFileDialog(ByVal vstrTitle As String, _
        ByVal vstrInitialPath As String, _
        Optional ByVal vstrExtensionFilterInfoString As String = "", _
        Optional ByVal vstrExecutingApplicationPath As String = "", _
        Optional ByVal vblnPriorToUsePreviousSelectedItemDirectory As Boolean = False, _
        Optional ByVal vstrAdditionalSaveKey As String = "") As String
    
    
    Dim strSelectedFilePath As String, objSelectedItems As Collection
    
    Set objSelectedItems = mfobjGetFilePathsByApplicationFileDialog(False, _
            vstrTitle, _
            vstrInitialPath, _
            vstrExtensionFilterInfoString, _
            vstrExecutingApplicationPath, _
            vblnPriorToUsePreviousSelectedItemDirectory, _
            vstrAdditionalSaveKey)
    
    strSelectedFilePath = ""
    
    If Not objSelectedItems Is Nothing Then
    
        strSelectedFilePath = objSelectedItems.Item(1)
    End If
    
    GetFilePathByApplicationFileDialog = strSelectedFilePath
End Function

'''
'''
''
Public Function GetPluralFilePathsByApplicationFileDialog(ByVal vstrTitle As String, _
        ByVal vstrInitialDirectoryPath As String, _
        Optional ByVal vstrExtensionFilterInfoString As String = "", _
        Optional ByVal vstrExecutingApplicationPath As String = "", _
        Optional ByVal vblnPriorToUsePreviousSelectedItemDirectory As Boolean = False, _
        Optional ByVal vstrAdditionalSaveKey As String = "") As Collection
    
    
    Set GetPluralFilePathsByApplicationFileDialog = mfobjGetFilePathsByApplicationFileDialog(True, _
            vstrTitle, _
            vstrInitialDirectoryPath, _
            vstrExtensionFilterInfoString, _
            vstrExecutingApplicationPath, _
            vblnPriorToUsePreviousSelectedItemDirectory, _
            vstrAdditionalSaveKey)
End Function


'''
''' open FileDialog
'''
Public Function GetFilePathByOpeningFileDialogAndCheck(ByVal vstrCheckFileName As String, ByVal vstrDialogTitle As String, ByVal vstrFileFilter As String) As String
    
    Dim strPath As String
    
    strPath = ""
    
    strPath = GetFilePathByOpeningFileDialog(vstrDialogTitle, vstrFileFilter)

    If strPath <> "" Then
    
        With New Scripting.FileSystemObject
        
            If StrComp(UCase(vstrCheckFileName), UCase(.GetFileName(strPath))) <> 0 Then
            
                strPath = ""
            End If
        End With
    End If

    GetFilePathByOpeningFileDialogAndCheck = strPath
End Function

'''
'''
'''
Public Function GetFilePathByOpeningFileDialog(ByVal vstrDialogTitle As String, ByVal vstrFileFilter As String) As String
    
    Dim objApplication As Object ' Excel.Application, Word.Application, or PowerPoint.Application
    Dim strPath As String
    
    strPath = ""
    
    ' If this has been called from the back ground Excel application process, This 'Application' keyword should have displayed the active window-top-level FileDialog window.
    
    ' the active Excel.Application object will be selected, even though this Workbook VBA is executed by the back-ground other process
    
    Set objApplication = Application
    
    strPath = objApplication.GetOpenFilename(FileFilter:=vstrFileFilter, Title:=vstrDialogTitle)

    GetFilePathByOpeningFileDialog = strPath
End Function

'''
''' open FolderDialog
'''
Public Function GetDirectoryPathByOpeningFolderDialogAndCheck(ByVal vstrDialogTitle As String, ByVal vstrNecessaryPathStringPart As String) As String
    
    Dim strPath As String
    
    strPath = GetDirectoryPathByOpeningFolderDialog(vstrDialogTitle)
    
    If InStr(1, strPath, vstrNecessaryPathStringPart) = 0 Then
        
        strPath = ""
    End If
    
    GetDirectoryPathByOpeningFolderDialogAndCheck = strPath
End Function


'''
'''
'''
Public Function GetDirectoryPathByOpeningFolderDialog(ByVal vstrDialogTitle As String, _
        Optional ByVal vstrRootDirectoryPath As String = "") As String
    
    Dim objFolder As Shell32.Folder3, strPath As String
    
    strPath = ""
    
#If HAS_REF Then

    With New Shell32.Shell
#Else
    With CreateObject("Shell.Application")
#End If
        ' &H1 := Select only folders (cannot select [Network])
        ' &H100 := Display the user operation hints
        ' &H200 := Hide the [create folder] button
        ' &H4000 := Select also files
    
        If vstrRootDirectoryPath <> "" Then
        
            Set objFolder = .BrowseForFolder(CLng(GetThisApplicationHWnd()), _
                    vstrDialogTitle, _
                    &H1 + &H100 + &H200, _
                    vstrRootDirectoryPath)
        Else
            Set objFolder = .BrowseForFolder(CLng(GetThisApplicationHWnd()), _
                    vstrDialogTitle, _
                    &H1 + &H100 + &H200)
        End If
    
        If Not objFolder Is Nothing Then
        
            strPath = objFolder.Self.Path
        End If
    End With

    GetDirectoryPathByOpeningFolderDialog = strPath
End Function


'**---------------------------------------------
'** Call back solutions
'**---------------------------------------------
'''
'''
'''
Public Function GetStringFromCallBackInterface(ByVal vstrOperationInterface As String, _
        Optional ByVal vstrExecutingApplicationPath As String = "") As String
    
    Dim objApplication As Object, strValue As String
    
    strValue = ""
    
    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)
    
    On Error Resume Next

    strValue = objApplication.Run(vstrOperationInterface)

    On Error GoTo 0

    GetStringFromCallBackInterface = strValue
End Function

'''
'''
'''
Public Function GetStringFromCallBackInterfaceAndOneArgument(ByVal vstrOperationInterface As String, _
        ByRef rvarArgument As Variant, _
        Optional ByVal vstrExecutingApplicationPath As String = "") As String
    
    
    Dim objApplication As Object, strValue As String, varValue As Variant
    
    strValue = ""
    
    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)
    
    On Error Resume Next

    strValue = objApplication.Run(vstrOperationInterface, rvarArgument)

    If Err.Number <> 0 Then
    
        Debug.Print "Call back error: " & Err.Description & ", - Error Number: " & CStr(Err.Number)
    End If

    On Error GoTo 0

    GetStringFromCallBackInterfaceAndOneArgument = strValue
End Function


'''
'''
'''
Public Function GetStringFromCallBackInterfaceAndOneStringArgument(ByVal vstrOperationInterface As String, ByRef rstrArg As String, Optional ByVal vstrExecutingApplicationPath As String = "") As String
    
    Dim objApplication As Object, strValue As String, varValue As Variant
    
    strValue = ""
    
    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)
    
    On Error Resume Next

    strValue = objApplication.Run(vstrOperationInterface, rstrArg)

    If Err.Number <> 0 Then
    
        Debug.Print "Call back error: " & Err.Description & ", - Error Number: " & CStr(Err.Number)
    End If

    On Error GoTo 0

    GetStringFromCallBackInterfaceAndOneStringArgument = strValue
End Function


'''
''' call back for Collection
'''
Public Function GetCollectionFromCallBackInterface(ByVal vstrOperationInterface As String, Optional ByVal vstrExecutingApplicationPath As String = "") As Collection
    
    Dim objApplication As Object, objCol As Collection

    Set objCol = Nothing
    
    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)
    
    On Error Resume Next

    Set objCol = objApplication.Run(vstrOperationInterface)

    On Error GoTo 0

    Set GetCollectionFromCallBackInterface = objCol
End Function

'''
''' call back for Scripting.Dictionary
'''
Public Function GetDictionaryFromCallBackInterface(ByVal vstrOperationInterface As String, Optional ByVal vstrExecutingApplicationPath As String = "") As Scripting.Dictionary
    
    Dim objApplication As Object, objDic As Scripting.Dictionary

    Set objDic = Nothing
    
    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)
    
    On Error Resume Next

    Set objDic = objApplication.Run(vstrOperationInterface)

    On Error GoTo 0

    Set GetDictionaryFromCallBackInterface = objDic
End Function

'''
'''
'''
Public Function GetVariantValueFromCallBackInterface(ByVal vstrOperationInterface As String, Optional ByVal vstrExecutingApplicationPath As String = "") As Variant

    Dim varValue As Variant, objApplication As Object
    
    varValue = 0
    
    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)
    
    On Error Resume Next
    
    varValue = objApplication.Run(vstrOperationInterface)

    On Error GoTo 0
    
    GetVariantValueFromCallBackInterface = varValue
End Function


'''
''' for single, testing interface.
'''
''' If more than one Excel application started by multi-processes on Windows, The single 'Application' keyword may often causes some unexpected errors.
'''
Public Sub CallBackInterfaceBySingleArguments(ByVal vstrOperationInterface As String, ByRef rvarArgument As Variant, Optional ByVal vstrExecutingApplicationPath As String = "")

    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''

    On Error Resume Next

    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)

    If Not objApplication Is Nothing Then
    
        objApplication.Run vstrOperationInterface, rvarArgument
    End If

    On Error GoTo 0
End Sub

'''
''' for two, testing interface.
'''
''' If more than one Excel application started by multi-processes on Windows, The single 'Application' keyword may often causes some unexpected errors.
'''
Public Sub CallBackInterfaceByTwoArguments(ByVal vstrOperationInterface As String, _
        ByRef rvarArgument1 As Variant, _
        ByRef rvarArgument2 As Variant, _
        Optional ByVal vstrExecutingApplicationPath As String = "")

    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''

    On Error Resume Next

    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)

    If Not objApplication Is Nothing Then
    
        objApplication.Run vstrOperationInterface, rvarArgument1, rvarArgument2
        
        If Err.Number <> 0 Then
        
            Debug.Print "Call back error: " & Err.Description
        End If
    End If

    On Error GoTo 0
End Sub

'''
''' for two, testing interface.
'''
''' If more than one Excel application started by multi-processes on Windows, The single 'Application' keyword may often causes some unexpected errors.
'''
Public Sub CallBackInterfaceByTwoStringArguments(ByVal vstrOperationInterface As String, _
        ByRef rstrArg1 As String, _
        ByRef rstrArg2 As String, _
        Optional ByVal vstrExecutingApplicationPath As String = "")

    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''

    On Error Resume Next

    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)

    If Not objApplication Is Nothing Then
    
        objApplication.Run vstrOperationInterface, rstrArg1, rstrArg2
        
        If Err.Number <> 0 Then
        
            Debug.Print "Call back error: " & Err.Description
        End If
    End If

    On Error GoTo 0
End Sub

'''
''' for three, testing interface.
'''
''' If more than one Excel application started by multi-processes on Windows, The single 'Application' keyword may often causes some unexpected errors.
'''
Public Sub CallBackInterfaceByThreeArguments(ByVal vstrOperationInterface As String, _
        ByRef rvarArgument1 As Variant, _
        ByRef rvarArgument2 As Variant, _
        ByRef rvarArgument3 As Variant, _
        Optional ByVal vstrExecutingApplicationPath As String = "")

    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''

    On Error Resume Next

    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)

    If Not objApplication Is Nothing Then
    
        objApplication.Run vstrOperationInterface, rvarArgument1, rvarArgument2, rvarArgument3
    End If

    On Error GoTo 0
End Sub

'''
''' for four, testing interface
'''
''' If more than one Excel application started by multi-processes on Windows, The single 'Application' keyword may often causes some unexpected errors.
'''
Public Sub CallBackInterfaceByFourArguments(ByVal vstrOperationInterface As String, ByRef rvarArgument1 As Variant, ByRef rvarArgument2 As Variant, ByRef rvarArgument3 As Variant, ByRef rvarArgument4 As Variant, Optional ByVal vstrExecutingApplicationPath As String = "")

    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''

    On Error Resume Next

    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)

    If Not objApplication Is Nothing Then
    
        objApplication.Run vstrOperationInterface, rvarArgument1, rvarArgument2, rvarArgument3, rvarArgument4
    End If

    On Error GoTo 0
End Sub

'''
''' for five, testing interface.
'''
''' If more than one Excel application started by multi-processes on Windows, The single 'Application' keyword may often causes some unexpected errors.
'''
Public Sub CallBackInterfaceByFiveArguments(ByVal vstrOperationInterface As String, ByRef rvarArgument1 As Variant, ByRef rvarArgument2 As Variant, ByRef rvarArgument3 As Variant, ByRef rvarArgument4 As Variant, ByRef rvarArgument5 As Variant, Optional ByVal vstrExecutingApplicationPath As String = "")

    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''

    On Error Resume Next

    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)

    If Not objApplication Is Nothing Then
    
        objApplication.Run vstrOperationInterface, rvarArgument1, rvarArgument2, rvarArgument3, rvarArgument4, rvarArgument5
    End If

    On Error GoTo 0
End Sub

'''
''' for Six
'''
Public Sub CallBackInterfaceBySixArguments(ByVal vstrOperationInterface As String, ByRef rvarArgument1 As Variant, ByRef rvarArgument2 As Variant, ByRef rvarArgument3 As Variant, ByRef rvarArgument4 As Variant, ByRef rvarArgument5 As Variant, ByRef rvarArgument6 As Variant, Optional ByVal vstrExecutingApplicationPath As String = "")

    Dim objApplication As Object

    On Error Resume Next

    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)

    If Not objApplication Is Nothing Then
    
        objApplication.Run vstrOperationInterface, rvarArgument1, rvarArgument2, rvarArgument3, rvarArgument4, rvarArgument5, rvarArgument6
    End If

    On Error GoTo 0
End Sub

'''
''' for Six
'''
Public Function GetObjectFromCallBackSixArgumentsInterface(ByVal vstrOperationInterface As String, ByRef rvarArgument1 As Variant, ByRef rvarArgument2 As Variant, ByRef rvarArgument3 As Variant, ByRef rvarArgument4 As Variant, ByRef rvarArgument5 As Variant, ByRef rvarArgument6 As Variant, Optional ByVal vstrExecutingApplicationPath As String = "") As Object

    Dim objApplication As Object, objReturnObject As Object

    Set objReturnObject = Nothing
    
    On Error Resume Next

    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)

    If Not objApplication Is Nothing Then
    
        Set objReturnObject = objApplication.Run(vstrOperationInterface, rvarArgument1, rvarArgument2, rvarArgument3, rvarArgument4, rvarArgument5, rvarArgument6)
    End If

    On Error GoTo 0

    Set GetObjectFromCallBackSixArgumentsInterface = objReturnObject
End Function

'''
''' for Seven
'''
Public Sub CallBackInterfaceBySevenArguments(ByVal vstrOperationInterface As String, ByRef rvarArgument1 As Variant, ByRef rvarArgument2 As Variant, ByRef rvarArgument3 As Variant, ByRef rvarArgument4 As Variant, ByRef rvarArgument5 As Variant, ByRef rvarArgument6 As Variant, ByRef rvarArgument7 As Variant, Optional ByVal vstrExecutingApplicationPath As String = "")

    Dim objApplication As Object

    On Error Resume Next

    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)

    If Not objApplication Is Nothing Then
    
        objApplication.Run vstrOperationInterface, rvarArgument1, rvarArgument2, rvarArgument3, rvarArgument4, rvarArgument5, rvarArgument6, rvarArgument7
    End If

    On Error GoTo 0
End Sub

'**---------------------------------------------
'** Solute Office Application object
'**---------------------------------------------
'''
'''
'''
Public Function GetOfficeApplicationObject(Optional ByVal vstrComIdentifierApplicationName As String = "Excel", _
        Optional ByVal vblnStartByAnotherProcess As Boolean = False) As Object

    Dim objApplication As Object
    
    Set objApplication = Nothing

    Select Case vstrComIdentifierApplicationName

        Case "Excel", "Word", "PowerPoint", "Access", "Outlook"

            Set objApplication = mfobjGetOfficeApplicationObjectByExactName(vstrComIdentifierApplicationName, _
                    vblnStartByAnotherProcess)
    End Select

    Set GetOfficeApplicationObject = objApplication
End Function

'''
'''
'''
Public Function GetOfficeApplicationObjectByTwoMethods(Optional ByVal vstrExecutingApplicationPath As String = "") As Object
    
    Dim objApplication As Object

    If vstrExecutingApplicationPath <> "" Then
    
        Set objApplication = GetApplicationFromFilePath(vstrExecutingApplicationPath)
    Else
        Set objApplication = GetCurrentOfficeFileObject().Application
    End If

    Set GetOfficeApplicationObjectByTwoMethods = objApplication
End Function

'''
'''
'''
Public Sub QuitApplicationIfItIsNotCurrentOfficeApplication(Optional ByVal vstrComIdentifierApplicationName As String = "Excel")

    Dim objApplication As Object
    
    Set objApplication = Nothing

    Select Case vstrComIdentifierApplicationName

        Case "Excel", "Word", "PowerPoint", "Access", "Outlook"

            If GetOfficeApplicationLowerName() <> LCase(vstrComIdentifierApplicationName) Then

                Set objApplication = mfobjGetExistedProcessOfficeApplicationObjectByExactName(vstrComIdentifierApplicationName)
                
                If Not objApplication Is Nothing Then
                
                    objApplication.Quit
                End If
            End If
    End Select
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetOfficeApplicationObjectByExactName(ByVal vstrComIdentifierApplicationName As String, _
        Optional ByVal vblnStartByAnotherProcess As Boolean = False) As Object
    
    Dim objApplication As Object

    If GetOfficeApplicationLowerName() = LCase(vstrComIdentifierApplicationName) Then

        If Not vblnStartByAnotherProcess Then
        
            Set objApplication = GetCurrentOfficeFileObject().Application
        Else
            Set objApplication = CreateObject(vstrComIdentifierApplicationName & ".Application")
        End If
    Else
        On Error Resume Next
    
        If Not vblnStartByAnotherProcess Then
    
            Set objApplication = mfobjGetExistedProcessOfficeApplicationObjectByExactName(vstrComIdentifierApplicationName)
        End If
    
        On Error GoTo 0
    
        If objApplication Is Nothing Then
    
            Set objApplication = CreateObject(vstrComIdentifierApplicationName & ".Application")
        End If
        
        If Not objApplication.Visible Then
        
            objApplication.Visible = True
        End If
    End If

    Set mfobjGetOfficeApplicationObjectByExactName = objApplication
End Function

'''
'''
'''
Private Function mfobjGetExistedProcessOfficeApplicationObjectByExactName(ByRef rstrComIdentifierApplicationName As String) As Object
    
    Dim objApplication As Object

    Set objApplication = Nothing
    
    On Error Resume Next

    Set objApplication = GetObject(, rstrComIdentifierApplicationName & ".Application")

    On Error GoTo 0

    Set mfobjGetExistedProcessOfficeApplicationObjectByExactName = objApplication
End Function
'**---------------------------------------------
'** File dialog interface
'**---------------------------------------------
'''
'''
'''
Private Function mfobjGetFilePathsByApplicationFileDialog(ByVal vblnAllowMultiSelect As String, _
        ByVal vstrTitle As String, _
        ByVal vstrInitialPath As String, _
        Optional ByVal vstrExtensionFilterInfoString As String = "", _
        Optional ByVal vstrExecutingApplicationPath As String = "", _
        Optional ByVal vblnPriorToUsePreviousSelectedItemDirectory As Boolean = False, _
        Optional ByVal vstrAdditionalSaveKey As String = "") As Collection


    Dim objApplication As Object
    
    Dim objSelectedItems As Collection, varSelectedItem As Variant, strDecidedInitialDirectoryPath As String
    
#If HAS_REF Then
    Dim objFileDialog As Office.FileDialog
#Else
    Dim objFileDialog As Object
#End If
    
    Set objSelectedItems = Nothing
    
    Set objApplication = GetOfficeApplicationObjectByTwoMethods(vstrExecutingApplicationPath)
    
    ' use FileDialog
    Set objFileDialog = objApplication.FileDialog(msoFileDialogFilePicker)
    
    With objFileDialog

        .Title = vstrTitle
        
        .AllowMultiSelect = vblnAllowMultiSelect
        
        If FileExistsByVbaDir(vstrInitialPath) Then
        
            .InitialFileName = vstrInitialPath
        Else
            strDecidedInitialDirectoryPath = mfstrGetFileDialogInitialDirectoryPathFromRegistry(vstrInitialPath, _
                    vblnPriorToUsePreviousSelectedItemDirectory, _
                    vstrAdditionalSaveKey)
            
            If strDecidedInitialDirectoryPath <> "" Then
            
                .InitialFileName = strDecidedInitialDirectoryPath & "\"
            End If
        End If

        If vstrExtensionFilterInfoString <> "" Then
        
            msubSetupFileDialogFiltersFromSettingString .Filters, vstrExtensionFilterInfoString
        End If

        

        If .Show() Then
        
            For Each varSelectedItem In .SelectedItems
            
                If objSelectedItems Is Nothing Then Set objSelectedItems = New Collection
            
                objSelectedItems.Add varSelectedItem
            Next
            
            If vblnPriorToUsePreviousSelectedItemDirectory Then
            
                msubSetFileDialogInitialDirectoryPathToRegistry objSelectedItems, strDecidedInitialDirectoryPath, vstrAdditionalSaveKey
            End If
        End If
    End With

    Set mfobjGetFilePathsByApplicationFileDialog = objSelectedItems
End Function

'''
''' decide the initial-directory-path when the file-dialog window is opened
'''
Private Function mfstrGetFileDialogInitialDirectoryPathFromRegistry(ByVal vstrInitialDirectoryPath As String, _
        Optional ByVal vblnPriorToUsePreviousSelectedItemDirectory As Boolean = False, _
        Optional ByVal vstrAdditionalSaveKey As String = "") As String
    
    
    Dim strDirectoryPath As String

    strDirectoryPath = ""
    
    If vblnPriorToUsePreviousSelectedItemDirectory Then
    
        strDirectoryPath = GetUNCPathToCurUserReg(mfstrGetModuleNameAndKeyForFileDialog(vstrAdditionalSaveKey))
    End If

    If strDirectoryPath = "" Then
    
        If Not IsEmpty(vstrInitialDirectoryPath) Then
        
            If vstrInitialDirectoryPath <> "" Then
            
                If DirectoryExistsByVbaDir(vstrInitialDirectoryPath) Then
                
                    strDirectoryPath = vstrInitialDirectoryPath
                End If
            End If
        End If
    End If

    mfstrGetFileDialogInitialDirectoryPathFromRegistry = strDirectoryPath
End Function

'''
'''
'''
Private Sub msubSetFileDialogInitialDirectoryPathToRegistry(ByVal vobjSelectedPaths As Collection, _
        ByVal vstrInitialDirectoryPath As String, _
        Optional ByVal vstrAdditionalSaveKey As String = "")


    Dim varSelectedPath As Variant, strSelectedPath As String, strFirstSelectedDirectoryPath As String
     
    With New Scripting.FileSystemObject
        
        For Each varSelectedPath In vobjSelectedPaths
        
            strSelectedPath = varSelectedPath
            
            If .FileExists(strSelectedPath) Then
            
                strFirstSelectedDirectoryPath = .GetParentFolderName(strSelectedPath)
            End If
            
            If .FolderExists(strSelectedPath) Then
            
                strFirstSelectedDirectoryPath = strSelectedPath
            End If
        
            Exit For
        Next
    End With

    If strFirstSelectedDirectoryPath <> "" And strFirstSelectedDirectoryPath <> vstrInitialDirectoryPath Then
    
        SetUNCPathToCurUserReg mfstrGetModuleNameAndKeyForFileDialog(vstrAdditionalSaveKey), strFirstSelectedDirectoryPath
    End If
End Sub


Private Function mfstrGetModuleNameAndKeyForFileDialog(Optional ByVal vstrAdditionalSaveKey As String = "")

    mfstrGetModuleNameAndKeyForFileDialog = mstrModuleKey & "\" & mstrSubjectWhenShowedFileDialogInitialDirKey & vstrAdditionalSaveKey
End Function

'''
'''
'''
Private Sub msubSetupFileDialogFiltersFromSettingString(ByVal vobjFilters As Office.FileDialogFilters, _
        ByVal vstrExtensionFilterInfoString As String)


    Dim strFiltersInfoArray() As String, i As Long
    
    Dim strFilterDescription As String, strFilterExtensionDeclaration As String
    
    
    strFiltersInfoArray = Split(vstrExtensionFilterInfoString, ",")
    
    With vobjFilters
    
        .Clear
    
        For i = LBound(strFiltersInfoArray) To UBound(strFiltersInfoArray) Step 2
        
            strFilterDescription = strFiltersInfoArray(i)
            
            strFilterExtensionDeclaration = strFiltersInfoArray(i + 1)
            
            .Add strFilterDescription, strFilterExtensionDeclaration
        Next
    End With
End Sub


'**---------------------------------------------
'** Internal functions for Call back solutions
'**---------------------------------------------
'''
'''
'''
Private Function mfobjGetOfficeFileObjectFromApplicationKeyword() As Object
    
    Dim objOfficeObject As Object
    
    Select Case GetOfficeApplicationLowerName()
        
        Case "excel"
            
            On Error Resume Next
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisWorkbook", VbGet) ' Application.ThisWorkbook
            
            On Error GoTo 0
            
            If objOfficeObject Is Nothing Then
            
                Set objOfficeObject = VBA.CallByName(Application, "ActiveWorkbook", VbGet)  ' Excel.Workbook
            End If
            
        Case "word"
    
            On Error Resume Next
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisDocument", VbGet)
            
            On Error GoTo 0
            
            If objOfficeObject Is Nothing Then
            
                Set objOfficeObject = VBA.CallByName(Application, "ActiveDocument", VbGet)  ' Word.Document
            End If
        Case "access"
    
            'Set objOfficeObject = Application.CurrentProject
            
            Set objOfficeObject = VBA.CallByName(Application, "CurrentProject", VbGet)  ' Access.CurrentProject
            
        Case "powerpoint"
    
            'Set objOfficeObject = Application.ActivePresentation
            
            Set objOfficeObject = VBA.CallByName(Application, "ActivePresentation", VbGet)  ' PowerPoint.Presentation
            
        Case "outlook"
        
            'Set objOfficeObject = Application.ThisOutlookSession
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisOutlookSession", VbGet)  ' Outlook.OutlookSession
    End Select

    Set mfobjGetOfficeFileObjectFromApplicationKeyword = objOfficeObject
End Function

'''
'''
'''
#If VBA7 Then
Public Function GetThisApplicationHWnd() As LongPtr

    Dim intHWnd As LongPtr
#Else
Public Function GetThisApplicationHWnd() As Long

    Dim intHWnd As Long
#End If

    Dim objOfficeObject As Object

    Select Case mfstrGetLowerCaseWithTrimmingLogo(Application.Name)
    
        Case "excel"
    
            intHWnd = VBA.CallByName(Application, "Hwnd", VbGet)
    
        Case "word"
    
            On Error Resume Next
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisDocument", VbGet)
            
            On Error GoTo 0
            
            If objOfficeObject Is Nothing Then
            
                Set objOfficeObject = VBA.CallByName(Application, "ActiveDocument", VbGet)  ' Word.Document
            End If
    
            intHWnd = VBA.CallByName(Application.Windows.Item(objOfficeObject.Name), "hwnd", VbGet)
    
        Case "powerpoint"

            intHWnd = FindWindow("PPTFrameClass", vbNullString)
    End Select

    GetThisApplicationHWnd = intHWnd
End Function



'''
'''
'''
''' <Argument>objApplication: Input</Argument>
''' <Argument>vstrOfficeApplicationName: Input</Argument>
''' <Return>Object: Either Excel.Workbook , Word.Document, or PowerPoint.Application</Return>
Public Function GetNewMsOfficeFileObjectByInterface(ByVal objApplication As Object, _
        Optional ByVal vstrOfficeApplicationName As String = "Excel") As Object

    Dim objOfficeFileObject As Object   ' Excel.Workbook, Word.Document, or PowerPoint.Presentation

    Select Case vstrOfficeApplicationName
    
        Case "Excel"    ' The type of this objApplication is Excel.Application
    
            If mfstrGetLowerCaseWithTrimmingLogo(objApplication.Name) = LCase(vstrOfficeApplicationName) Then
    
                Set objOfficeFileObject = objApplication.Workbooks.Add()
            Else
                Set objOfficeFileObject = GetOfficeApplicationObject("Excel").Workbooks.Add()   ' Return new Excel.Workbook object
            End If
            
        Case "Word"     ' The type of this objApplication is Word.Application
    
            If mfstrGetLowerCaseWithTrimmingLogo(objApplication.Name) = LCase(vstrOfficeApplicationName) Then
    
                Set objOfficeFileObject = objApplication.Documents.Add()
            Else
                Set objOfficeFileObject = GetOfficeApplicationObject("Word").Documents.Add()    ' Return new Word.Document object
            End If
            
        Case "PowerPoint"   ' The type of this objApplication is PowerPoint.Application
    
            If mfstrGetLowerCaseWithTrimmingLogo(objApplication.Name) = LCase(vstrOfficeApplicationName) Then
            
                Set objOfficeFileObject = objApplication.Presentations.Add()
            Else
                Set objOfficeFileObject = GetOfficeApplicationObject("PowerPoint").Presentations.Add()    ' Return new PowerPoint.Presentation
            End If
    End Select

    Set GetNewMsOfficeFileObjectByInterface = objOfficeFileObject
End Function


'''
'''
'''
#If HAS_ACCESS_REF Then
Public Function GetTestingAccessCurrentProjectObject(ByVal objApplication As Access.Application) As Object
#Else
Public Function GetTestingAccessCurrentProjectObject(ByVal objApplication As Object) As Object
#End If
    
    Dim strDBDir As String, strDbPath As String

#If HAS_ACCESS_REF Then
    Dim objProject As Access.CurrentProject
#Else
    Dim objProject As Object
#End If

    Set objProject = Nothing
    
    On Error Resume Next
    
    Set objProject = objApplication.CurrentProject

    On Error GoTo 0
    
    If objProject Is Nothing Then
    
        strDBDir = GetParentDirectoryPathOfCurrentOfficeFile() & "\TemporaryAccessDB"
                
        ForceToCreateDirectory strDBDir
        
        strDbPath = strDBDir & "\" & GetTestingOfficeFilePath("Access")
        
        With New Scripting.FileSystemObject
        
            If .FileExists(strDbPath) Then
            
                objApplication.OpenCurrentDatabase strDbPath
            Else
                objApplication.NewCurrentDatabase strDbPath
            End If
        End With
        
        Set objProject = objApplication.CurrentProject
    End If

    Set GetTestingAccessCurrentProjectObject = objProject  ' Access.CurrentProject object
End Function


'''
'''
'''
Private Function GetTestingOfficeFilePath(Optional ByVal vstrOfficeApplicationName As String = "Excel") As String

    Dim strFileName As String
    
    strFileName = ""
    
    Select Case vstrOfficeApplicationName
    
        Case "Excel"
    
            strFileName = "TestMacroBook.xlsm"
            
        Case "Word"
    
            strFileName = "TestMacroDoc.docm"
            
        Case "PowerPoint"
        
            strFileName = "TestMacroPresentation.pptm"
            
        Case "Access"
        
            strFileName = "TestDB01.accdb"
            
        Case "Outlook"
        
    End Select

    GetTestingOfficeFilePath = strFileName
End Function


'''
''' Interface implementation. This is also executed from Word.Application and PowerPoint.Application. When this starts from the Excel VBE, it returns 'excel'
'''
Public Function GetOfficeApplicationLowerName(Optional ByVal vobjApplication As Object = Nothing) As String
    
    Dim strLowerName As String
    
    strLowerName = ""

    If Not vobjApplication Is Nothing Then
    
        If StrComp(TypeName(vobjApplication), "Application") = 0 Then
        
            ' This vobjApplication can be other Office application object
            
            ' This VB project should be belongs to Excel, when this code has been executed from Excel.Workbook,
            ' the vobjApplication may have been the Word.Application object, or the PowerPoint.Application object
        
            strLowerName = mfstrGetLowerCaseWithTrimmingLogo(vobjApplication.Name)
        End If
    End If


    If strLowerName = "" Then
    
        ' This Application instance stands for this VB project Application
        ' This code is blongs to Excel macro-book, this instance type of Application keyword is sure to be the Excel.Application
    
        strLowerName = mfstrGetLowerCaseWithTrimmingLogo(Application.Name)
    End If

    GetOfficeApplicationLowerName = strLowerName
End Function

'''
'''
'''
Private Function mfstrGetLowerCaseWithTrimmingLogo(ByVal vstrApplicationName As String) As String

    mfstrGetLowerCaseWithTrimmingLogo = Trim(Replace(LCase(vstrApplicationName), "microsoft", ""))
End Function


'''
'''
'''
Private Function IsCurrentApplicationExcel() As Boolean

    IsCurrentApplicationExcel = (StrComp(GetOfficeApplicationLowerName(), "excel") = 0)
End Function

'''
''' Using GetObject function
'''
''' The ThisWorkbook object module doesn't exist in the application either Word, Access, PowerPoint, or Outlook
'''
Public Function GetMsExcelWorkbookByInterface(ByVal vstrBookPath As String, _
        Optional ByVal vblnReadOnly As Boolean = False, _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vblnOpenWithPreventingEvents As Boolean = False, _
        Optional ByVal vblnAddToMru As Boolean = False) As Object

    Dim objBook As Object
    
    On Error Resume Next
    
    Set objBook = Nothing
    
    If IsCurrentApplicationExcel() Then
        ' use Excel.Workbooks.Open method
    
        Set objBook = mfobjGetExcelWorkbookByExcelInterface(vstrBookPath, _
                vblnReadOnly, _
                vstrPassword, _
                vblnOpenWithPreventingEvents, _
                vblnAddToMru)
    End If
    
    If objBook Is Nothing Then
    
        ' If the GetObject() is used, the file is opened at the same Windows-process. But the options of Open() cannot be selected.
        ' If the following is executed from either Word.Application, PowerPoint.Application, or Outlook.Application, the new Excel windows-porcess is started.
    
        Set objBook = GetObject(vstrBookPath)
    End If
    
    On Error GoTo 0

    Set GetMsExcelWorkbookByInterface = objBook
End Function


'''
'''
'''
Private Function mfobjGetExcelWorkbookByExcelInterface(ByVal vstrBookPath As String, _
        Optional ByVal vblnReadOnly As Boolean = False, _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vblnOpenWithPreventingEvents As Boolean = False, _
        Optional ByVal vblnAddToMru As Boolean = False) As Object

    Dim blnFound As Boolean, blnOldEnableEvents As Boolean, blnOldDisplayAlerts As Boolean

#If HAS_EXCEL_REF Then

    Dim objApplication As Excel.Application, objBook As Excel.Workbook, objOpenedBook As Excel.Workbook
#Else
    Dim objApplication As Object, objBook As Object, objOpenedBook As Object
#End If

#If HAS_REF Then

    Dim objFS As Scripting.FileSystemObject
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objFS As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If

    Set objBook = Nothing
    
    Set objApplication = GetOfficeApplicationObject("Excel")
    
    blnFound = False
    
    With objFS
    
        For Each objOpenedBook In objApplication.Workbooks
        
            Select Case True
        
                Case StrComp(LCase(objOpenedBook.Name), LCase(.GetFileName(vstrBookPath))) = 0, _
                        StrComp(LCase(objOpenedBook.Name), LCase(.GetBaseName(vstrBookPath))) = 0
                    
                    Set objBook = objOpenedBook
                
                    blnFound = True
            End Select
        Next
    End With

    If Not blnFound Then
    
        blnOldDisplayAlerts = objApplication.DisplayAlerts
        
        objApplication.DisplayAlerts = False
    
        If vblnOpenWithPreventingEvents Then
            
            blnOldEnableEvents = objApplication.EnableEvents
            
            objApplication.EnableEvents = False
        End If
    
        On Error Resume Next
    
        If vstrPassword = "" Then
    
            Set objBook = objApplication.Workbooks.Open(vstrBookPath, _
                    ReadOnly:=vblnReadOnly, _
                    AddToMru:=vblnAddToMru)
        Else
            Set objBook = objApplication.Workbooks.Open(vstrBookPath, _
                    ReadOnly:=vblnReadOnly, _
                    Password:=vstrPassword, _
                    AddToMru:=vblnAddToMru)
        End If
    
        On Error GoTo 0
        
        objApplication.DisplayAlerts = blnOldDisplayAlerts
        
        If vblnOpenWithPreventingEvents Then
        
            objApplication.EnableEvents = blnOldEnableEvents
        End If
    End If

    Set mfobjGetExcelWorkbookByExcelInterface = objBook
End Function


'''
''' get Word Document object
'''
Public Function GetMsWordDocument(ByVal vstrDocPath As String) As Object

    Dim objDoc As Object

    On Error Resume Next
    
    Set objDoc = Nothing
    
    Set objDoc = GetObject(vstrDocPath)
    
    If objDoc Is Nothing Then
    
        Set objDoc = mfobjGetMsWordDocumentFromPath(vstrDocPath)
    End If
    
    On Error GoTo 0

    Set GetMsWordDocument = objDoc
End Function

'''
'''
'''
Private Function mfobjGetMsWordDocumentFromPath(ByRef rstrPath As String, _
        Optional ByVal vblnForceToCreate As Boolean = False) As Object

#If HAS_WORD_REF Then

    Dim objApplication As Word.Application, objDocument As Word.Document
#Else
    Dim objApplication As Object, objDocument As Object
#End If

    Set objApplication = GetOfficeApplicationObject("Word")

    If FileExistsByVbaDir(rstrPath) Then
    
        Set objDocument = objApplication.Documents.Open(rstrPath)
    Else
        If vblnForceToCreate Then
        
            Set objDocument = GetNewWordDocumentWithProcessingProtectedView(objApplication)
        End If
    End If

    Set mfobjGetMsWordDocumentFromPath = objDocument
End Function

'''
'''
'''
#If HAS_WORD_REF Then
Public Function GetNewWordDocumentWithProcessingProtectedView(ByVal vobjWordApplication As Word.Application, _
        Optional ByVal vstrTemplateDocumentFilePath As String = "") As Word.Document

    Dim objDocument As Word.Document, objProtectedViewWindow As Word.ProtectedViewWindow
#Else

Public Function GetNewWordDocumentWithProcessingProtectedView(ByVal vobjWordApplication As Object, _
        Optional ByVal vstrTemplateDocumentFilePath As String = "") As Object

    Dim objDocument As Object, objProtectedViewWindow As Object
#End If

    On Error Resume Next
    
    Set objDocument = Nothing
    
    If vstrTemplateDocumentFilePath <> "" Then
    
        Set objDocument = vobjWordApplication.Documents.Add(vstrTemplateDocumentFilePath, False, 0)       ' WdNewDocumentType.wdNewBlankDocument = 0
    Else
        Set objDocument = vobjWordApplication.Documents.Add()
    End If
    
    If Err.Number <> 0 Then
    
        Set objProtectedViewWindow = Nothing
        
        Set objProtectedViewWindow = vobjWordApplication.ProtectedViewWindows.Item(1)
        
        If Not objProtectedViewWindow Is Nothing Then
        
            Set objDocument = objProtectedViewWindow.Edit()
            
            If Not objDocument Is Nothing Then
            
                Set objProtectedViewWindow = Nothing
            End If
        End If
    End If
    
    On Error GoTo 0

    Set GetNewWordDocumentWithProcessingProtectedView = objDocument
End Function


'''
''' get PowerPoint Presentation object
'''
Public Function GetMsPowerPointPresentation(ByVal vstrPresentationPath As String) As Object

    Dim objPresentation As Object

    On Error Resume Next
    
    Set objPresentation = Nothing
    
    Set objPresentation = GetObject(vstrPresentationPath)
    
    If objPresentation Is Nothing Then
    
        Set objPresentation = mfobjGetMsPowerPointPresentationFromPath(vstrPresentationPath)
    End If
    
    On Error GoTo 0

    Set GetMsPowerPointPresentation = objPresentation
End Function

'''
'''
'''
Private Function mfobjGetMsPowerPointPresentationFromPath(ByRef rstrPath As String, Optional ByVal vblnForceToCreate As Boolean = False) As Object

#If HAS_POWERPOINT_REF Then

    Dim objApplication As PowerPoint.Application, objPresentation As PowerPoint.Presentation
#Else
    Dim objApplication As Object, objPresentation As Object
#End If

    Set objApplication = GetOfficeApplicationObject("PowerPoint")

    If FileExistsByVbaDir(rstrPath) Then
    
        Set objPresentation = objApplication.Presentations.Open(rstrPath)
    Else
        If vblnForceToCreate Then
        
            Set objPresentation = objApplication.Presentations.Add()
        End If
    End If

    Set mfobjGetMsPowerPointPresentationFromPath = objPresentation
End Function


'''
''' get Access.CurrentProject object from (*.accdb) or (*.mdb)
'''
Public Function GetMsAccessCurrentProject(ByVal vstrPresentationPath As String) As Object

    Dim objAccessApplication As Object
    
    On Error Resume Next
    
    Set objAccessApplication = Nothing
    
    Set objAccessApplication = GetObject(vstrPresentationPath)
    
    On Error GoTo 0

    Set GetMsAccessCurrentProject = objAccessApplication.CurrentProject
End Function


'///////////////////////////////////////////////
'/// Operations - a UNC path portability solution
'///////////////////////////////////////////////
'**---------------------------------------------
'** set user-specified path
'**---------------------------------------------
'''
''' Individual-Set-Registy
'''
Public Sub ISRInInterfaceCallOfOfficeTemplatesRootDir(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectOfficeTemplatesRootDirKey, vstrTargetPath
End Sub
Public Sub IDelRInInterfaceCallOfOfficeTemplatesRootDir()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectOfficeTemplatesRootDirKey
End Sub


'**---------------------------------------------
'** solute path generaly
'**---------------------------------------------
'''
''' solute directory path from registry CURRENT_USER
''' this is also refered by CodeAnalysisVBAAllPaths.bas and CodeExternalBookOut.bas
'''
Public Function GetOfficeTemplatesRootDir() As String
    
    Dim strModuleNameAndSubjectKey As String, strTargetPath As String
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectOfficeTemplatesRootDirKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If strTargetPath = "" Then
    
        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForOfficeTemplatesRoot())
    End If
    
    GetOfficeTemplatesRootDir = strTargetPath
End Function


Private Function mfobjGetRegParamOfStorageUNCPathColForOfficeTemplatesRoot() As Collection
    
    Dim objCol As Collection, strSubDirectoryPath As String
    
    Set objCol = Nothing
    
    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForOfficeTemplatesRoot")
    
    If objCol Is Nothing Then   ' If interface cannot be soluted
        
#If HAS_REF Then
        With New WshNetwork
#Else
        With CreateObject("WScript.Network")
#End If
            strSubDirectoryPath = "VBAWorks\" & .UserName & "\OfficeTemplates"
        End With
    
        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCDirectoryPath(StoragePathSearchingFlag.NetWorkOrLocalAsDirectory, strSubDirectoryPath, strSubDirectoryPath))
    End If
    
    Set mfobjGetRegParamOfStorageUNCPathColForOfficeTemplatesRoot = objCol
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' The following can be executed in either Excel, Word, or PowerPoint
'''
Private Sub msubSanityTestToGetOfficeFileObjectFromApplicationKeyword()

    Dim objOfficeObject As Object
    
    Set objOfficeObject = mfobjGetOfficeFileObjectFromApplicationKeyword()

    Debug.Print TypeName(objOfficeObject)
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetCurrentOfficeFileObject()

    Debug.Print TypeName(GetCurrentOfficeFileObject())
End Sub


Private Sub msubSanityTestToGetOfficeTemplatesRootDir()

    Debug.Print GetOfficeTemplatesRootDir()
End Sub

Private Sub msubSanityTestToDeleteOfficeTemplatesRootDir()

    IDelRInInterfaceCallOfOfficeTemplatesRootDir
End Sub


