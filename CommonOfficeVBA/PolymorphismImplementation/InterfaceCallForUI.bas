Attribute VB_Name = "InterfaceCallForUI"
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Office application user interface
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 13/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "InterfaceCallForUI"

'///////////////////////////////////////////////
'/// Declrations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Key-Value cache preparation for InterfaceCallForUI
'**---------------------------------------------
Private mobjStrKeyValueInterfaceCallForUIDic As Scripting.Dictionary

Private mblnIsStrKeyValueInterfaceCallForUIDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About [Office].Application.FileDialog, this can be used in Excel, Word, PowerPoint
'**---------------------------------------------
'''
'''
'''
Public Function GetMacroEnabledFilePathByApplicationFileDialog(ByVal vstrTitle As String, _
        Optional ByVal vstrExtensionFilterInfoString As String = "", _
        Optional ByVal vstrAdditionalSaveKey As String = "")

    Dim strSelectedPath As String

    With GetCurrentOfficeFileObject()

        strSelectedPath = GetFilePathByApplicationFileDialog(vstrTitle, .Path, vstrExtensionFilterInfoString, .FullName, True, vstrAdditionalSaveKey)
    End With

    GetMacroEnabledFilePathByApplicationFileDialog = strSelectedPath
End Function

'**---------------------------------------------
'** About VBE UI interface
'**---------------------------------------------
'''
'''
'''
Public Sub DeleteCommandBarIfItExists(ByVal vobjApplication As Object, vstrCommandBarName As String)

    Dim objCommandBar As Office.CommandBar

    With vobjApplication
    
        On Error Resume Next
    
        Set objCommandBar = .CommandBars.Item(vstrCommandBarName)
    
        On Error GoTo 0
    
        If Not objCommandBar Is Nothing Then
        
            objCommandBar.Delete
        End If
    End With
End Sub


'**---------------------------------------------
'** Key-Value cache preparation for InterfaceCallForUI
'**---------------------------------------------
'''
''' get string Value from STR_KEY_INTERFACE_CALL_FOR_UI_TEXT_MACRO_ENABLED_FILE
'''
''' automatically-added for InterfaceCallForUI string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInterfaceCallForUiTextMacroEnabledFile() As String

    If Not mblnIsStrKeyValueInterfaceCallForUIDicInitialized Then

        msubInitializeTextForInterfaceCallForUI
    End If

    GetTextOfStrKeyInterfaceCallForUiTextMacroEnabledFile = mobjStrKeyValueInterfaceCallForUIDic.Item("STR_KEY_INTERFACE_CALL_FOR_UI_TEXT_MACRO_ENABLED_FILE")
End Function

'''
''' get string Value from STR_KEY_INTERFACE_CALL_FOR_UI_TEXT_MACRO_BOOK_FILE
'''
''' automatically-added for InterfaceCallForUI string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInterfaceCallForUiTextMacroBookFile() As String

    If Not mblnIsStrKeyValueInterfaceCallForUIDicInitialized Then

        msubInitializeTextForInterfaceCallForUI
    End If

    GetTextOfStrKeyInterfaceCallForUiTextMacroBookFile = mobjStrKeyValueInterfaceCallForUIDic.Item("STR_KEY_INTERFACE_CALL_FOR_UI_TEXT_MACRO_BOOK_FILE")
End Function

'''
''' get string Value from STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_BOOK_FILE
'''
''' automatically-added for InterfaceCallForUI string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInterfaceCallForUiFileDialogTitleSelectMacroBookFile() As String

    If Not mblnIsStrKeyValueInterfaceCallForUIDicInitialized Then

        msubInitializeTextForInterfaceCallForUI
    End If

    GetTextOfStrKeyInterfaceCallForUiFileDialogTitleSelectMacroBookFile = mobjStrKeyValueInterfaceCallForUIDic.Item("STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_BOOK_FILE")
End Function

'''
''' get string Value from STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_COMPARE_VBPROJECT_LINES
'''
''' automatically-added for InterfaceCallForUI string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInterfaceCallForUiFileDialogTitleSelectMacroOfficeFileForCompareVbprojectLines() As String

    If Not mblnIsStrKeyValueInterfaceCallForUIDicInitialized Then

        msubInitializeTextForInterfaceCallForUI
    End If

    GetTextOfStrKeyInterfaceCallForUiFileDialogTitleSelectMacroOfficeFileForCompareVbprojectLines = mobjStrKeyValueInterfaceCallForUIDic.Item("STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_COMPARE_VBPROJECT_LINES")
End Function

'''
''' get string Value from STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_COMPARE_VBPROJECT_ALL_TEXTS
'''
''' automatically-added for InterfaceCallForUI string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInterfaceCallForUiFileDialogTitleSelectMacroOfficeFileForCompareVbprojectAllTexts() As String

    If Not mblnIsStrKeyValueInterfaceCallForUIDicInitialized Then

        msubInitializeTextForInterfaceCallForUI
    End If

    GetTextOfStrKeyInterfaceCallForUiFileDialogTitleSelectMacroOfficeFileForCompareVbprojectAllTexts = mobjStrKeyValueInterfaceCallForUIDic.Item("STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_COMPARE_VBPROJECT_ALL_TEXTS")
End Function

'''
''' get string Value from STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_LIST_UP_FUNCTIONS
'''
''' automatically-added for InterfaceCallForUI string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInterfaceCallForUiFileDialogTitleSelectMacroOfficeFileForListUpFunctions() As String

    If Not mblnIsStrKeyValueInterfaceCallForUIDicInitialized Then

        msubInitializeTextForInterfaceCallForUI
    End If

    GetTextOfStrKeyInterfaceCallForUiFileDialogTitleSelectMacroOfficeFileForListUpFunctions = mobjStrKeyValueInterfaceCallForUIDic.Item("STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_LIST_UP_FUNCTIONS")
End Function

'''
''' get string Value from STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_LIST_UP_ALL_COM_REFERENCES
'''
''' automatically-added for InterfaceCallForUI string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInterfaceCallForUiFileDialogTitleSelectMacroOfficeFileForListUpAllComReferences() As String

    If Not mblnIsStrKeyValueInterfaceCallForUIDicInitialized Then

        msubInitializeTextForInterfaceCallForUI
    End If

    GetTextOfStrKeyInterfaceCallForUiFileDialogTitleSelectMacroOfficeFileForListUpAllComReferences = mobjStrKeyValueInterfaceCallForUIDic.Item("STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_LIST_UP_ALL_COM_REFERENCES")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for InterfaceCallForUI string-key-value management for standard module and class module
Private Sub msubInitializeTextForInterfaceCallForUI()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueInterfaceCallForUIDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForInterfaceCallForUIByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForInterfaceCallForUIByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueInterfaceCallForUIDicInitialized = True
    End If

    Set mobjStrKeyValueInterfaceCallForUIDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for InterfaceCallForUI key-values cache
'''
''' automatically-added for InterfaceCallForUI string-key-value management for standard module and class module
Private Sub AddStringKeyValueForInterfaceCallForUIByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_TEXT_MACRO_ENABLED_FILE", "Macro enabled file"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_TEXT_MACRO_BOOK_FILE", "Macro enabled Excel-book"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_BOOK_FILE", "Select macro-enabled Excel-book"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_COMPARE_VBPROJECT_LINES", "Select a macro enable file, which you want to compare count of lines of each common module name"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_COMPARE_VBPROJECT_ALL_TEXTS", "Select a macro enable file, which you want to compare all-texts of each common module name by exporting its"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_LIST_UP_FUNCTIONS", "Select a macro enable file, which you want to investigate VB macro procedures or properties"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_LIST_UP_ALL_COM_REFERENCES", "Select a macro enable file, which you want to list up VB project COM references"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for InterfaceCallForUI key-values cache
'''
''' automatically-added for InterfaceCallForUI string-key-value management for standard module and class module
Private Sub AddStringKeyValueForInterfaceCallForUIByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_TEXT_MACRO_ENABLED_FILE", "マクロ有効ファイル"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_TEXT_MACRO_BOOK_FILE", "マクロ有効ブック"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_BOOK_FILE", "マクロブック(.xlsm)を選択"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_COMPARE_VBPROJECT_LINES", "コードモジュール名と行数を比較するため、マクロ有効ファイルを選択"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_COMPARE_VBPROJECT_ALL_TEXTS", "コードモジュールの全コードを比較するため、マクロ有効ファイルを選択"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_LIST_UP_FUNCTIONS", "コードモジュールの全プロシージャ・関数をリスト表示するため、マクロ有効ファイルを選択"
        .Add "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_LIST_UP_ALL_COM_REFERENCES", "VBプロジェクトのCOM参照をリスト表示するため、マクロ有効ファイルを選択"
    End With
End Sub

'''
''' Remove Keys for InterfaceCallForUI key-values cache
'''
''' automatically-added for InterfaceCallForUI string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueInterfaceCallForUI(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_INTERFACE_CALL_FOR_UI_TEXT_MACRO_ENABLED_FILE") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_INTERFACE_CALL_FOR_UI_TEXT_MACRO_ENABLED_FILE"
            .Remove "STR_KEY_INTERFACE_CALL_FOR_UI_TEXT_MACRO_BOOK_FILE"
            .Remove "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_BOOK_FILE"
            .Remove "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_COMPARE_VBPROJECT_LINES"
            .Remove "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_COMPARE_VBPROJECT_ALL_TEXTS"
            .Remove "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_LIST_UP_FUNCTIONS"
            .Remove "STR_KEY_INTERFACE_CALL_FOR_UI_FILE_DIALOG_TITLE_SELECT_MACRO_OFFICE_FILE_FOR_LIST_UP_ALL_COM_REFERENCES"
        End If
    End With
End Sub



