VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DbConOracle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Connecting Oracle least parameters for some scripting-shells
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 28/Nov/2023    Kalmclaeyd Tarclanus    Initial Creation
'

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////


Private mstrServerHostName As String    ' Database server hostname or IP address


Private mstrNetworkServiceName As String  ' Service name, for Oracle server

Private mintPortNo As Long  ' connection TCP/IP port number, it is ordinary omitted, for Oracle 1521

Private mstrUID As String   ' User ID

Private mstrPWD As String   ' Password


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
''' read-only User Name - ODBC
'''
Public Property Get UserName() As String

    UserName = mstrUID
End Property

'**---------------------------------------------
'** ODBC DSNless connection
'**---------------------------------------------
'''
''' read-only Driver Name for ODBC
'''
Public Property Get DriverName() As String

    DriverName = mstrDriverName
End Property

'''
''' read-only RDB server host name or IP address
'''
Public Property Get ServerHostName() As String

    ServerHostName = mstrServerHostName
End Property

'''
''' read-only Oracle network-service name
'''
Public Property Get NetworkServiceName() As String

    NetworkServiceName = mstrNetworkServiceName
End Property


'''
''' read-only Port number
'''
Public Property Get PortNo() As Long

    PortNo = mintPortNo
End Property

'''
''' read-only password
'''
Public Property Get PWD() As String

    PWD = mstrPWD
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' using Oracle connection string, which is able to be used for SQL*Plus connection in a shell
'''
Public Sub SetOracleConnectionParameters(ByVal vstrServerHostName As String, _
        ByVal vstrNetworkServiceName As String, _
        ByVal vstrUserid As String, _
        ByVal vstrPassword As String, _
        Optional ByVal vintPortNumber As Long = 1521)
    
    
    mstrServerHostName = vstrServerHostName
    
    mstrNetworkServiceName = vstrNetworkServiceName
    
    mstrUID = vstrUserid   ' User ID
    
    mstrPWD = vstrPassword   ' Password
    
    mintPortNo = vintPortNumber
End Sub



