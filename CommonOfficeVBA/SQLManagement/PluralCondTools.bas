Attribute VB_Name = "PluralCondTools"
'
'   SQL plural condition object tools
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri,  5/Aug/2023    Kalmclaeyd Tarclanus    Separated from ADOSheetOut.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** simple PluralCondition generators
'**---------------------------------------------
Public Function GetSQLInPluralCondition(ByVal vstrFieldTitle As String, _
        ByVal vobjConditionItemCol As Collection, _
        Optional ByVal vblnForceToServeAsCharacters As Boolean = False) As PluralCondition


    Set GetSQLInPluralCondition = mfobjGetSQLInPluralCondition(vstrFieldTitle, vobjConditionItemCol, vblnForceToServeAsCharacters)
End Function

'''
'''
'''
Public Function GetSQLInPluralCondition_PgSQL(ByVal vstrFieldTitle As String, _
        ByVal vobjConditionItemCol As Collection, _
        Optional ByVal vblnForceToServeAsCharacters As Boolean = False) As PluralCondition


    Set GetSQLInPluralCondition_PgSQL = mfobjGetSQLInPluralCondition(vstrFieldTitle, vobjConditionItemCol, vblnForceToServeAsCharacters, RdbPostgreSqlDialectType)
End Function

'''
'''
'''
Private Function mfobjGetSQLInPluralCondition(ByVal vstrFieldTitle As String, _
        ByVal vobjConditionItemCol As Collection, _
        Optional ByVal vblnForceToServeAsCharacters As Boolean = False, _
        Optional ByVal venmSQLInstanceType As RdbSqlDialectType = RdbSqlDialectType.RdbOracleSqlDialectType) As PluralCondition
    
    Dim objCond As PluralCondition

    Set objCond = New PluralCondition
    
    With objCond
        
        .FieldName = vstrFieldTitle
    
        Set .Col = vobjConditionItemCol
        
        If vblnForceToServeAsCharacters Then
        
            .ForceToServeChar = True
        End If
        
        .SQLGeneratingInstanceType = venmSQLInstanceType
    End With

    Set mfobjGetSQLInPluralCondition = objCond
End Function


Public Function GetSQLLikePluralCondition(ByVal vstrFieldTitle As String, _
        ByVal vobjConditionItemCol As Collection, _
        Optional ByVal venmPluralStringLikeMatchType As RdbSqlPluralStringLikeMatchType = (RdbSqlPluralStringLikeMatchType.RdbSqlOrCond Or RdbSqlPluralStringLikeMatchType.RdbSqlLikePrefixSearch)) As PluralCondition
    
    Dim objCond As PluralCondition

    Set objCond = New PluralCondition
    
    With objCond
        
        .FieldName = vstrFieldTitle
    
        Set .Col = vobjConditionItemCol
        
        .PluralStringLikeMatch = venmPluralStringLikeMatchType
    End With

    Set GetSQLLikePluralCondition = objCond
End Function


Public Function GetDate8DigitsIntegerPeriodPluralCondition(ByVal vstrFieldTitle As String, _
        ByVal vudtFromDate As Date, _
        ByVal vudtToDate As Date) As PluralCondition
    
    Dim objDateCond As PluralCondition
    
    Set objDateCond = New PluralCondition
    
    With objDateCond
        
        Set .Col = New Collection
        
        With .Col
            
            .Add ConvertDateTo8DigitsDateLong(vudtFromDate): .Add ConvertDateTo8DigitsDateLong(vudtToDate)
        End With
        
        .ForceToBetweenCondition = True
        
        .FieldName = vstrFieldTitle
    End With

    Set GetDate8DigitsIntegerPeriodPluralCondition = objDateCond
End Function

Public Function GetDate8DigitsStringPeriodPluralCondition(ByVal vstrFieldTitle As String, _
        ByVal vudtFromDate As Date, _
        ByVal vudtToDate As Date) As PluralCondition
    
    Dim objDateCond As PluralCondition
    
    Set objDateCond = New PluralCondition
    
    With objDateCond
        
        Set .Col = New Collection
        
        With .Col
            
            .Add ConvertDateTo8DigitsDateString(vudtFromDate): .Add ConvertDateTo8DigitsDateString(vudtToDate)
        End With
        
        .ForceToBetweenCondition = True
        
        .ForceToServeChar = True
        
        .FieldName = vstrFieldTitle
    End With

    Set GetDate8DigitsStringPeriodPluralCondition = objDateCond
End Function

'''
'''
'''
Public Function GetDate14DigitsStringPeriodPluralCondition(ByVal vstrFieldTitle As String, _
        ByVal vudtFromDate As Date, _
        ByVal vudtToDate As Date) As PluralCondition

    Dim objDateCond As PluralCondition
    
    Set objDateCond = New PluralCondition
    
    With objDateCond
        
        Set .Col = New Collection
        
        With .Col
            
            .Add ConvertDateTo14DigitsDateString(vudtFromDate): .Add ConvertDateTo14DigitsDateString(vudtToDate)
        End With
        
        .ForceToBetweenCondition = True
        
        .ForceToServeChar = True
        
        .FieldName = vstrFieldTitle
    End With

    Set GetDate14DigitsStringPeriodPluralCondition = objDateCond
End Function

'''
'''
'''
Public Function GetSQLBetweenDatePeriodPluralCondition(ByVal vstrFieldTitle As String, _
        ByVal vudtFromDate As Date, _
        ByVal vudtToDate As Date) As PluralCondition

    Set GetSQLBetweenDatePeriodPluralCondition = mfobjGetSQLBetweenDatePeriodPluralCondition(vstrFieldTitle, vudtFromDate, vudtToDate)
End Function


Public Function GetSQLBetweenDatePeriodPluralCondition_PgSQL(ByVal vstrFieldTitle As String, _
        ByVal vudtFromDate As Date, _
        ByVal vudtToDate As Date) As PluralCondition

    Set GetSQLBetweenDatePeriodPluralCondition_PgSQL = mfobjGetSQLBetweenDatePeriodPluralCondition(vstrFieldTitle, vudtFromDate, vudtToDate, RdbPostgreSqlDialectType)
End Function



'''
'''
'''
Private Function mfobjGetSQLBetweenDatePeriodPluralCondition(ByVal vstrFieldTitle As String, _
        ByVal vudtFromDate As Date, _
        ByVal vudtToDate As Date, _
        Optional ByVal venmSQLInstanceType As RdbSqlDialectType = RdbSqlDialectType.RdbOracleSqlDialectType) As PluralCondition
    
    Dim objDateCond As PluralCondition

    Set objDateCond = New PluralCondition
    
    With objDateCond
    
        Set .Col = New Collection
        
        With .Col
        
            .Add vudtFromDate: .Add vudtToDate
        End With
        
        .ForceToBetweenCondition = True
        
        .SQLGeneratingInstanceType = venmSQLInstanceType
        
        .FieldName = vstrFieldTitle
    End With

    Set mfobjGetSQLBetweenDatePeriodPluralCondition = objDateCond
End Function


'''
'''
'''
Public Function GetDatePeriodPluralConditionFromCenterDate(ByVal vstrFieldTitle As String, _
        ByVal vobjCenterDate As Date, _
        ByVal vintPreviousMonth As Long, _
        ByVal vintPrecedingMonth As Long) As PluralCondition
    
    Dim objDateCond As PluralCondition, udtDate As Date, udtFromDate As Date, udtToDate As Date
    
    udtFromDate = DateAdd("m", -vintPreviousMonth, vobjCenterDate)  ' privous monthes
    
    udtToDate = DateAdd("m", vintPrecedingMonth, vobjCenterDate)    ' preceding monthes

    Set objDateCond = New PluralCondition
    
    With objDateCond
        
        Set .Col = New Collection
        
        With .Col
            
            .Add udtFromDate: .Add udtToDate
        End With
        
        .ForceToBetweenCondition = True
        
        .FieldName = vstrFieldTitle
    End With

    Set GetDatePeriodPluralConditionFromCenterDate = objDateCond
End Function

'''
'''
'''
Public Function GetNearlyDatePeriodCondition(ByVal vstrFieldTitle As String) As PluralCondition
    
    Dim objDateCond As PluralCondition
    Dim udtDate As Date, udtFromDate As Date, udtToDate As Date

    udtDate = Now
    
    udtFromDate = DateAdd("m", -3, udtDate) ' privous three monthes
    
    udtToDate = DateAdd("m", 1, udtDate)    ' foward to one month

    Set objDateCond = New PluralCondition
    
    With objDateCond
    
        Set .Col = New Collection
        
        With .Col
        
            .Add udtFromDate: .Add udtToDate
        End With
        
        .ForceToBetweenCondition = True
        
        .FieldName = vstrFieldTitle
    End With

    Set GetNearlyDatePeriodCondition = objDateCond
End Function


