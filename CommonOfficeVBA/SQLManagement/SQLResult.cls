VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "SQLResult"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO SQL result simple data class
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on CurrentUserDomainUtility.bas
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  1/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Variables as properties
'**---------------------------------------------
Public SQLExeElapsedTime As Long  ' mili-second,  SheetOut.bls, OracleOdbcConnector, XlAdoSheetExpander

Public TableRecordCount As Long

Public TableFieldCount As Long

Public SQLExeTime As Date

Public ExcelSheetOutputElapsedTime As Long  ' mili-second

'**---------------------------------------------
'** Private variables
'**---------------------------------------------
Private mintRecordsAffected As Long  ' this is used when the SQL is UPDATE, DELETE, INSERT

Private mstrSQL As String

Private menmSqlOptionalLogFlagOfWorksheet As SqlOptionalLogFlagOfWorksheet

Private mobjLoadedADOConnectionSetting As LoadedADOConnectionSetting

Private mobjCurrentUserDomain As CurrentUserDomain

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mintRecordsAffected = -1

    menmSqlOptionalLogFlagOfWorksheet = 0
End Sub



'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////

Public Property Let SQL(ByVal vstrSQL As String)

    mstrSQL = vstrSQL
End Property
Public Property Get SQL() As String

    SQL = mstrSQL
End Property


Public Property Let RecordsAffected(ByVal vintRecordsAffected As Long)

    If vintRecordsAffected >= 0 Then
    
        mintRecordsAffected = vintRecordsAffected
    End If
End Property
Public Property Get RecordsAffected() As Long

    RecordsAffected = mintRecordsAffected
End Property

'''
''' read-only SqlOptionalLogFlagOfWorksheet
'''
Public Property Get SQLOptionalLog() As SqlOptionalLogFlagOfWorksheet

    SQLOptionalLog = menmSqlOptionalLogFlagOfWorksheet
End Property

'''
''' read-only LoadedADOConnectionSetting
'''
Public Property Get LoadedADOConnectSetting() As LoadedADOConnectionSetting

    Set LoadedADOConnectSetting = mobjLoadedADOConnectionSetting
End Property

'''
''' read-only CurrentUserDomain
'''
Public Property Get CurrentUserInfo() As CurrentUserDomain

    Set CurrentUserInfo = mobjCurrentUserDomain
End Property

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub SetSQLOptionalLog(ByVal vobjLoadedADOConnectionSetting As LoadedADOConnectionSetting)

    Set mobjLoadedADOConnectionSetting = vobjLoadedADOConnectionSetting

    menmSqlOptionalLogFlagOfWorksheet = menmSqlOptionalLogFlagOfWorksheet Or SqlOptionalLogFlagOfWorksheet.LogOfAdoConnectSettingOnSheet
End Sub

'''
'''
'''
Public Sub SetCurrentUserLog(ByVal vobjCurrentUserDomain As CurrentUserDomain)

    Set mobjCurrentUserDomain = vobjCurrentUserDomain

    ' insert exception code
    With mobjCurrentUserDomain
    
        If mblnFEATURE_FLAG_PREVENT_SPECIAL_CLIENT_USER_LOG Then
        
            Exit Sub
        End If
    End With

    menmSqlOptionalLogFlagOfWorksheet = menmSqlOptionalLogFlagOfWorksheet Or SqlOptionalLogFlagOfWorksheet.LogOfCurrentUserComputerDomainOnSheet
End Sub
