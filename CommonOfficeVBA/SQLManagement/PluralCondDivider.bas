Attribute VB_Name = "PluralCondDivider"
'
'   PluralCondition divider, for avoiding SQL 'IN' maximum item restriction in some real relational databases
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  1/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetDividedPluralConditions(ByVal vobjCond As PluralCondition, ByVal vintLimitMax As Long) As Collection
    
    Dim objCols As Collection, objValueCols As Collection, objValueCol As Collection
    Dim varItem As Variant, objChildCond As PluralCondition
    
    If vobjCond.Col.Count <= vintLimitMax Then
    
        Set objCols = New Collection
        
        objCols.Add vobjCond
    Else
        Set objValueCols = GetDividedColsFromLimitMax(vobjCond.Col, vintLimitMax)
        
        Set objCols = New Collection
        
        For Each varItem In objValueCols
        
            Set objValueCol = varItem
            
            Set objChildCond = New PluralCondition
            
            With objChildCond
            
                .FieldName = vobjCond.FieldName
                
                .ForceToBetweenCondition = vobjCond.ForceToBetweenCondition
                
                .ForceToServeChar = vobjCond.ForceToServeChar
                
                .AddNotCondition = vobjCond.AddNotCondition
                
                Set .Col = objValueCol
            End With
            
            objCols.Add objChildCond
        Next
    End If

    Set GetDividedPluralConditions = objCols
End Function


'''
'''
'''
Public Function GetDividedCols(ByVal vobjCol As Collection, ByVal vintDenominator As Long) As Collection
    
    Dim objCols As Collection, intDividedSize As Long
    Dim intColIndex As Long, intElementIndex As Long
    Dim objDividedCol As Collection, varItem As Variant
    
    
    Set GetDividedCols = Nothing
    
    If Not vobjCol Is Nothing Then
    
        If vobjCol.Count > 0 Then

            intDividedSize = Round(CDbl(vobjCol.Count) / CDbl(vintDenominator), 0)
    
            intColIndex = 1
            
            Set objDividedCol = New Collection
            
            intElementIndex = 0
            
            For Each varItem In vobjCol
            
                objDividedCol.Add varItem
                
                intElementIndex = intElementIndex + 1
                
                If intElementIndex = intDividedSize Then
                
                    intElementIndex = 0
                
                    If intColIndex < vintDenominator Then
                    
                        If objCols Is Nothing Then Set objCols = New Collection
                    
                        objCols.Add objDividedCol
                        
                        Set objDividedCol = New Collection
                    End If
                    
                    intColIndex = intColIndex + 1
                End If
            Next
            
            If objCols Is Nothing Then Set objCols = New Collection
            
            objCols.Add objDividedCol
            
            Set GetDividedCols = objCols
        End If
    End If
End Function


'''
'''
'''
Public Function GetDividedColsFromLimitMax(ByVal vobjCol As Collection, ByVal vintLimitMax As Long) As Collection
    
    Dim dblDividedVal As Double
    Dim intDenominator As Long
    
    Set GetDividedColsFromLimitMax = Nothing

    
    If Not vobjCol Is Nothing Then
    
        If vobjCol.Count > 0 Then

            dblDividedVal = CDbl(vobjCol.Count) / CDbl(vintLimitMax)
            
            If dblDividedVal > Fix(dblDividedVal) Then
            
                intDenominator = Fix(dblDividedVal) + 1
            Else
                intDenominator = Fix(dblDividedVal)
            End If
            
            intDenominator = Fix(CDbl(vobjCol.Count) / CDbl(vintLimitMax)) + 1
            
            Set GetDividedColsFromLimitMax = GetDividedCols(vobjCol, intDenominator)
        End If
    End If
End Function

