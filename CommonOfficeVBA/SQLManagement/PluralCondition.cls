VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "PluralCondition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   SQL WHERE condition simple generator
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on VBA data-type
'       This is independent from ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  1/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'***--------------------------------------------
'*** Data Type Constants
'***--------------------------------------------
Private Const mintDateOnlyDate As Integer = 1

Private Const mintDateOnlyTime As Integer = 2

Private Const mintDateGeneral As Integer = 3


Private Const mintReal As Integer = 12

Private Const mintInteger As Integer = 4

Private Const mintString As Integer = 32

Private Const mintBoolean As Integer = 16


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Variables as properties
'**---------------------------------------------
Public Col As Collection

Public FieldName As String

Public ForceToServeChar As Boolean

Public ForceToBetweenCondition As Boolean

Public AddNotCondition As Boolean

'**---------------------------------------------
'** Private variables
'**---------------------------------------------
' classify the data type from the only first element of the Col
Private mintDataType As Integer

Private mblnIsDataTypeDetermined As Boolean


Private menmPluralStringLikeMatch As RdbSqlPluralStringLikeMatchType

Private menmSQLInstanceType As RdbSqlDialectType

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()
    
    mintDataType = mintString
    
    ForceToServeChar = False
    
    ForceToBetweenCondition = False
    
    mblnIsDataTypeDetermined = False
    
    AddNotCondition = False
    
    menmPluralStringLikeMatch = RdbSqlPluralStringLikeMatchType.RdbSqlOrCond
    
    menmSQLInstanceType = RdbOracleSqlDialectType
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////

Public Property Let PluralStringLikeMatch(ByVal venmPluralStringLikeMatch As RdbSqlPluralStringLikeMatchType)

    menmPluralStringLikeMatch = venmPluralStringLikeMatch
End Property
Public Property Get PluralStringLikeMatch() As RdbSqlPluralStringLikeMatchType

    PluralStringLikeMatch = menmPluralStringLikeMatch
End Property

'''
''' Oracle, PostgreSQL , or ADO-Access
'''
Public Property Get SQLGeneratingInstanceType() As RdbSqlDialectType

    SQLGeneratingInstanceType = menmSQLInstanceType
End Property
Public Property Let SQLGeneratingInstanceType(ByVal venmSQLInstanceType As RdbSqlDialectType)

    menmSQLInstanceType = venmSQLInstanceType
End Property


'''
''' get SQL for more than one item condition (for Oracle RDB)
''' when the values are string, this stands for plural conditions of exact match
'''
Public Property Get SQLIncludingAllSection() As String

    Dim strSQL As String, strInSQL As String, strInSQLTip As String
    Dim i As Long, varItem As Variant
    
    
    If mfblnTypeDetermined Then
    
        If Col.Count = 1 Then
        
            SQLIncludingAllSection = SQLSection(1)
        Else
            If ForceToBetweenCondition Then
            
                SQLIncludingAllSection = ""
            
                Select Case menmSQLInstanceType
                
                    Case RdbSqlDialectType.RdbOracleSqlDialectType
                    
                        Select Case mintDataType
                        
                            Case mintDateGeneral
                            
                                SQLIncludingAllSection = " BETWEEN TO_DATE('" & Format(CDate(Col.Item(1)), "yyyy/m/d Hh:Nn:Ss") & "','yyyy/mm/dd hh24:mi:ss') AND TO_DATE('" & Format(CDate(Col.Item(2)), "yyyy/m/d Hh:Nn:Ss") & "','yyyy/mm/dd hh24:mi:ss')"
                                
                            Case mintDateOnlyDate
                                
                                SQLIncludingAllSection = " BETWEEN TO_DATE('" & Format(CDate(Col.Item(1)), "yyyy/m/d") & "','yyyy/mm/dd') AND TO_DATE('" & Format(CDate(Col.Item(2)), "yyyy/m/d") & "','yyyy/mm/dd')"
                            
                            Case mintDateOnlyTime
                                
                                SQLIncludingAllSection = " BETWEEN TO_DATE('" & Format(CDate(Col.Item(1)), "Hh:Nn:Ss") & "','hh24:mi:ss') AND TO_DATE('" & Format(CDate(Col.Item(2)), "Hh:Nn:Ss") & "','hh24:mi:ss')"
                            
                            Case mintReal, mintInteger
                                
                                SQLIncludingAllSection = " BETWEEN " & CStr(Col.Item(1)) & " AND " & CStr(Col.Item(2))
                            
                            Case mintString
                                
                                SQLIncludingAllSection = " BETWEEN '" & CStr(Col.Item(1)) & "' AND '" & CStr(Col.Item(2)) & "'"
                        End Select
                        
                    Case RdbSqlDialectType.RdbPostgreSqlDialectType
                    
                         Select Case mintDataType
                         
                            Case mintDateGeneral
                            
                                SQLIncludingAllSection = " BETWEEN '" & Format(CDate(Col.Item(1)), "yyyy/mm/dd hh:mm:ss") & "' AND '" & Format(CDate(Col.Item(2)), "yyyy/mm/dd hh:mm:ss") & "'"
                            
                            Case mintDateOnlyDate
                                
                                SQLIncludingAllSection = " BETWEEN '" & Format(CDate(Col.Item(1)), "yyyy/mm/dd") & "' AND '" & Format(CDate(Col.Item(2)), "yyyy/mm/dd") & "'" ' TO_DATE('" & Format(CDate(Col.Item(1)), "yyyy/m/d") & "','yyyy/mm/dd') AND TO_DATE('" & Format(CDate(Col.Item(2)), "yyyy/m/d") & "','yyyy/mm/dd')"
                            
                            Case mintDateOnlyTime
                                
                                SQLIncludingAllSection = " BETWEEN '" & Format(CDate(Col.Item(1)), "hh:mm:ss") & "' AND '" & Format(CDate(Col.Item(2)), "hh:mm:ss") & "'" ' TO_DATE('" & Format(CDate(Col.Item(1)), "Hh:Nn:Ss") & "','hh24:mi:ss') AND TO_DATE('" & Format(CDate(Col.Item(2)), "Hh:Nn:Ss") & "','hh24:mi:ss')"
                            
                            Case mintReal, mintInteger
                                
                                SQLIncludingAllSection = " BETWEEN " & CStr(Col.Item(1)) & " AND " & CStr(Col.Item(2))
                            
                            Case mintString
                                
                                SQLIncludingAllSection = " BETWEEN '" & CStr(Col.Item(1)) & "' AND '" & CStr(Col.Item(2)) & "'"
                        End Select
                
                    Case RdbSqlDialectType.RdbAdoAccessSqlDialectType
                
                            ' for example, #2023/05/04 15:34:22#,...
                            
                            ' need to implement...
                End Select
                
                If AddNotCondition Then
                
                    SQLIncludingAllSection = "NOT " & FieldName & SQLIncludingAllSection
                Else
                    SQLIncludingAllSection = FieldName & SQLIncludingAllSection
                End If
            Else
                
                i = 1
                
                strInSQL = ""
                
                For Each varItem In Col
                
                    Select Case menmSQLInstanceType
                    
                        Case RdbSqlDialectType.RdbOracleSqlDialectType
                        
                            Select Case mintDataType
                            
                                Case mintDateGeneral
                                
                                    strInSQLTip = "TO_DATE('" & Format(CDate(varItem), "yyyy/m/d Hh:Nn:Ss") & "','yyyy/mm/dd Hh:Nn:Ss')"
                                    
                                Case mintDateOnlyDate
                                
                                    strInSQLTip = "TO_DATE('" & Format(CDate(varItem), "yyyy/m/d") & "','yyyy/mm/dd')"
                                
                                Case mintDateOnlyTime
                                    
                                    strInSQLTip = "TO_DATE('" & Format(CDate(varItem), "Hh:Nn:Ss") & "','Hh:Nn:Ss')"
                                
                                Case mintReal, mintInteger
                                    
                                    strInSQLTip = CStr(varItem)
                                    
                                Case mintString
                                    
                                    strInSQLTip = "'" & CStr(varItem) & "'"
                            End Select
                    
                        Case RdbSqlDialectType.RdbPostgreSqlDialectType
                        
                            Select Case mintDataType
                                
                                Case mintDateGeneral
                                    
                                    strInSQLTip = "'" & Format(CDate(varItem), "yyyy/mm/dd hh:mm:ss") & "'"
                                
                                Case mintDateOnlyDate
                                    
                                    strInSQLTip = "'" & Format(CDate(varItem), "yyyy/mm/dd") & "'"
                                
                                Case mintDateOnlyTime
                                    
                                    strInSQLTip = "'" & Format(CDate(varItem), "hh:mm:ss") & "'"
                                
                                Case mintReal, mintInteger
                                    
                                    strInSQLTip = CStr(varItem)
                                
                                Case mintString
                                    
                                    strInSQLTip = "'" & CStr(varItem) & "'"
                            End Select
                        
                        Case RdbSqlDialectType.RdbAdoAccessSqlDialectType
                
                            ' for example, #2021/06/24 15:34:22#,...
                            
                            ' need to implement...
                    End Select
                
                    strInSQL = strInSQL & strInSQLTip
                    
                    If i < Col.Count Then
                    
                        strInSQL = strInSQL & ", "
                    End If
                
                    i = i + 1
                Next
                
                If AddNotCondition Then
                
                    SQLIncludingAllSection = FieldName & " NOT IN (" & strInSQL & ")"
                Else
                    SQLIncludingAllSection = FieldName & " IN (" & strInSQL & ")"
                End If
            End If
        End If

    End If
End Property

'''
''' get SQL for one item condition (for Oracle RDB)
'''
Public Property Get SQLSection(ByVal Index As Long) As String
    
    If mfblnTypeDetermined Then
    
        Select Case menmSQLInstanceType
            
            Case RdbSqlDialectType.RdbOracleSqlDialectType
                
                Select Case mintDataType
                    
                    Case mintDateGeneral
                        
                        SQLSection = FieldName & " = TO_DATE('" & Format(CDate(Col.Item(Index)), "yyyy/m/d Hh:Nn:Ss") & "','yyyy/mm/dd hh24:mi:ss')"
                    
                    Case mintDateOnlyDate
                        
                        SQLSection = FieldName & " = TO_DATE('" & Format(CDate(Col.Item(Index)), "yyyy/m/d") & "','yyyy/mm/dd')"
                    
                    Case mintDateOnlyTime
                        
                        SQLSection = FieldName & " = TO_DATE('" & Format(CDate(Col.Item(Index)), "Hh:Nn:Ss") & "','hh24:mi:ss')"
                    
                    Case mintReal, mintInteger
                        
                        SQLSection = FieldName & " = " & CStr(Col.Item(Index))
                    
                    Case mintString
                        
                        SQLSection = FieldName & " = '" & CStr(Col.Item(Index)) & "'"
                End Select
                
            Case RdbSqlDialectType.RdbPostgreSqlDialectType
                
                Select Case mintDataType
                    
                    Case mintDateGeneral
                        
                        SQLSection = FieldName & " = '" & Format(CDate(Col.Item(Index)), "yyyy/mm/dd hh:mm:ss") & "'"  ' " = TO_DATE('" & Format(CDate(Col.Item(Index)), "yyyy/m/d Hh:Nn:Ss") & "','yyyy/mm/dd hh24:mi:ss')"
                    
                    Case mintDateOnlyDate
                        
                        SQLSection = FieldName & " = '" & Format(CDate(Col.Item(Index)), "yyyy/mm/dd") & "'"  ' " = TO_DATE('" & Format(CDate(Col.Item(Index)), "yyyy/m/d") & "','yyyy/mm/dd')"
                    
                    Case mintDateOnlyTime
                        
                        SQLSection = FieldName & " = '" & Format(CDate(Col.Item(Index)), "hh:mm:ss") & "'"  ' " = TO_DATE('" & Format(CDate(Col.Item(Index)), "Hh:Nn:Ss") & "','hh24:mi:ss')"
                    
                    Case mintReal, mintInteger
                        
                        SQLSection = FieldName & " = " & CStr(Col.Item(Index))
                    
                    Case mintString
                        
                        SQLSection = FieldName & " = '" & CStr(Col.Item(Index)) & "'"
                End Select
            
            Case RdbSqlDialectType.RdbAdoAccessSqlDialectType
                
                ' for example, #2023/05/04 15:34:22#,...
                
                ' need to implement...
            
        End Select
        
        If AddNotCondition Then
        
            SQLSection = "NOT " & SQLSection
        End If
    End If
End Property

'''
'''
'''
Public Property Get SQLAllLikeSection() As String

    Dim strSQLConnect As String ' AND, OR
    Dim enmLikeType As RdbSqlPluralStringLikeMatchType, varItem As Variant, strLikeCond As String
    Dim strSQL As String, i As Long
    
    If mfblnTypeDetermined Then
        
        If (menmPluralStringLikeMatch And RdbSqlPluralStringLikeMatchType.RdbSqlAndCond) = RdbSqlPluralStringLikeMatchType.RdbSqlAndCond Then
        
            strSQLConnect = " AND "
        Else
            strSQLConnect = " OR "
        End If
    
        If (menmPluralStringLikeMatch And RdbSqlPluralStringLikeMatchType.RdbSqlLikePartialSearch) = RdbSqlPluralStringLikeMatchType.RdbSqlLikePartialSearch Then
            
            enmLikeType = RdbSqlLikePartialSearch
        
        ElseIf (menmPluralStringLikeMatch And RdbSqlPluralStringLikeMatchType.RdbSqlLikePrefixSearch) = RdbSqlPluralStringLikeMatchType.RdbSqlLikePrefixSearch Then
            
            enmLikeType = RdbSqlLikePrefixSearch
        
        ElseIf (menmPluralStringLikeMatch And RdbSqlPluralStringLikeMatchType.RdbSqlLikeSuffixSearch) = RdbSqlPluralStringLikeMatchType.RdbSqlLikeSuffixSearch Then
            
            enmLikeType = RdbSqlLikeSuffixSearch
        End If
        
        
        strSQL = ""
        
        i = 1
        
        For Each varItem In Col
        
            Select Case enmLikeType
                
                Case RdbSqlPluralStringLikeMatchType.RdbSqlLikePrefixSearch
                    
                    strLikeCond = FieldName & " LIKE '" & varItem & "%'"
                
                Case RdbSqlPluralStringLikeMatchType.RdbSqlLikeSuffixSearch
                    
                    strLikeCond = FieldName & " LIKE '%" & varItem & "'"
                
                Case RdbSqlPluralStringLikeMatchType.RdbSqlLikePartialSearch
                    
                    strLikeCond = FieldName & " LIKE '%" & varItem & "%'"
            End Select
        
            strSQL = strSQL & strLikeCond
            
            If i < Col.Count Then
                
                strSQL = strSQL & strSQLConnect
            End If
        
            i = i + 1
        Next
        
        If Col.Count > 1 Then strSQL = "(" & strSQL & ")"
        
        SQLAllLikeSection = strSQL
    End If
End Property



'''
''' get SQL for a only one column inline table by UNION phrase
'''
Public Property Get SQLInlineTableInSELECT_ByUnion() As String
   
    Dim strSQL As String, i As Long
            
    If mfblnTypeDetermined Then
        
        If Col.Count > 0 Then

            strSQL = ""
            
            For i = 1 To Col.Count
                
                strSQL = strSQL & "SELECT "
                
                Select Case menmSQLInstanceType
                
                    Case RdbSqlDialectType.RdbOracleSqlDialectType
                        
                        Select Case mintDataType
                            
                            Case mintDateGeneral
                                
                                strSQL = strSQL & "TO_DATE('" & Format(CDate(Col.Item(i)), "yyyy/m/d Hh:Nn:Ss") & "','yyyy/mm/dd hh24:mi:ss')"
                            
                            Case mintDateOnlyDate
                                
                                strSQL = strSQL & "TO_DATE('" & Format(CDate(Col.Item(i)), "yyyy/m/d") & "','yyyy/mm/dd')"
                            
                            Case mintDateOnlyTime
                                
                                strSQL = strSQL & "TO_DATE('" & Format(CDate(Col.Item(i)), "Hh:Nn:Ss") & "','hh24:mi:ss')"
                            
                            Case mintReal, mintInteger
                                
                                strSQL = strSQL & "" & CStr(Col.Item(i))
                            
                            Case mintString
                                
                                strSQL = strSQL & "'" & CStr(Col.Item(i)) & "'"
                        End Select
                
                    Case RdbSqlDialectType.RdbPostgreSqlDialectType
                        
                        Select Case mintDataType
                            
                            Case mintDateGeneral
                                
                                strSQL = strSQL & "'" & Format(CDate(Col.Item(i)), "yyyy/mm/dd hh:mm:ss") & "'"
                            
                            Case mintDateOnlyDate
                                
                                strSQL = strSQL & "'" & Format(CDate(Col.Item(i)), "yyyy/mm/dd") & "'"
                            
                            Case mintDateOnlyTime
                                
                                strSQL = strSQL & "'" & Format(CDate(Col.Item(i)), "hh:mm:ss") & "'"
                            
                            Case mintReal, mintInteger
                                
                                strSQL = strSQL & "" & CStr(Col.Item(i))
                            
                            Case mintString
                                
                                strSQL = strSQL & "'" & CStr(Col.Item(i)) & "'"
                        End Select
                
                    Case RdbSqlDialectType.RdbAdoAccessSqlDialectType
                
                        ' need to implement...
                
                End Select
                
            
                If i = 1 Then
                
                    strSQL = strSQL & " " & FieldName
                End If
                
                If i < Col.Count Then
                
                    Select Case menmSQLInstanceType
                        
                        Case RdbSqlDialectType.RdbOracleSqlDialectType
                
                            strSQL = strSQL & " FROM dual UNION ALL "   ' for speed up
                        
                        Case RdbSqlDialectType.RdbPostgreSqlDialectType
                        
                            strSQL = strSQL & " UNION ALL "   ' for speed up
                    End Select
                End If
            Next
            
            Select Case menmSQLInstanceType
            
                Case RdbSqlDialectType.RdbOracleSqlDialectType
            
                    strSQL = strSQL & " FROM dual"
        
                Case RdbSqlDialectType.RdbPostgreSqlDialectType
                
                    ' Need to implement...
                    
            End Select
        
            SQLInlineTableInSELECT_ByUnion = strSQL
        End If
    End If
End Property

'''
''' get SQL for a only one column inline table by PIVOT phrase (for ORACLE RDB)
'''
Public Property Get SQLInlineTableInSELECT_ByPivot() As String

    Dim strSQL As String
    Dim strNameInlineTable1st As String, strNameInlineTable2nd As String
    Dim strFormatExpress As String
    Dim i As Long
    
    
    If mfblnTypeDetermined Then
        
        If Col.Count > 0 Then
            
            strNameInlineTable1st = "Tmp" & FieldName & "X"
            
            strNameInlineTable2nd = "Tmp" & FieldName & "Y"
            
            If Col.Count < 10 Then
            
                strFormatExpress = "0"
            
            ElseIf Col.Count < 100 Then
                
                strFormatExpress = "00"
            
            ElseIf Col.Count < 1000 Then
                
                strFormatExpress = "000"
            
            ElseIf Col.Count < 10000 Then
                
                strFormatExpress = "0000"
            
            ElseIf Col.Count < 10000 Then
                
                strFormatExpress = "00000"
            Else
                strFormatExpress = "000000"
            End If
            
            strSQL = "SELECT " & FieldName & " FROM (WITH " & strNameInlineTable1st & "(col1, "
            
            For i = 1 To Col.Count
            
                strSQL = strSQL & "c" & Format(i, strFormatExpress)
                
                If i < Col.Count Then
                    
                    strSQL = strSQL & ", "
                End If
            Next
            
            strSQL = strSQL & ") AS (SELECT '" & FieldName & "', "
            
            For i = 1 To Col.Count
                
                Select Case menmSQLInstanceType
                
                    Case RdbSqlDialectType.RdbOracleSqlDialectType
                        
                        Select Case mintDataType
                            
                            Case mintDateGeneral
                                
                                strSQL = strSQL & "TO_DATE('" & Format(CDate(Col.Item(i)), "yyyy/m/d Hh:Nn:Ss") & "','yyyy/mm/dd hh24:mi:ss')"
                            
                            Case mintDateOnlyDate
                                
                                strSQL = strSQL & "TO_DATE('" & Format(CDate(Col.Item(i)), "yyyy/m/d") & "','yyyy/mm/dd')"
                            
                            Case mintDateOnlyTime
                                
                                strSQL = strSQL & "TO_DATE('" & Format(CDate(Col.Item(i)), "Hh:Nn:Ss") & "','hh24:mi:ss')"
                            
                            Case mintReal, mintInteger
                                
                                strSQL = strSQL & "" & CStr(Col.Item(i))
                            
                            Case mintString
                                
                                strSQL = strSQL & "'" & CStr(Col.Item(i)) & "'"
                        End Select
                
                    Case RdbSqlDialectType.RdbPostgreSqlDialectType
                        
                        Select Case mintDataType
                            
                            Case mintDateGeneral
                                
                                strSQL = strSQL & "'" & Format(CDate(Col.Item(i)), "yyyy/mm/dd hh:mm:ss") & "'"
                            
                            Case mintDateOnlyDate
                                
                                strSQL = strSQL & "'" & Format(CDate(Col.Item(i)), "yyyy/mm/dd") & "'"
                            
                            Case mintDateOnlyTime
                                
                                strSQL = strSQL & "'" & Format(CDate(Col.Item(i)), "hh:mm:ss") & "'"
                            
                            Case mintReal, mintInteger
                                
                                strSQL = strSQL & "" & CStr(Col.Item(i))
                            
                            Case mintString
                                
                                strSQL = strSQL & "'" & CStr(Col.Item(i)) & "'"
                        End Select
                
                    Case RdbSqlDialectType.RdbAdoAccessSqlDialectType
                
                        ' need to implement...
                        
                End Select
            
                If i < Col.Count Then
                
                    strSQL = strSQL & ", "
                End If
            Next
            
            strSQL = strSQL & " FROM dual) SELECT * FROM " & strNameInlineTable1st & " UNPIVOT(col_vals FOR col_names IN ("
            
            For i = 1 To Col.Count
            
                strSQL = strSQL & "c" & Format(i, strFormatExpress)
                
                If i < Col.Count Then
                
                    strSQL = strSQL & ", "
                End If
            Next
            
            strSQL = strSQL & ")) PIVOT(MAX(col_vals) FOR col1 IN ('" & FieldName & "' AS " & FieldName & ")) ORDER BY col_names) " & strNameInlineTable2nd
            
            SQLInlineTableInSELECT_ByPivot = strSQL
        End If
    End If
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub LoadFirstItemSplitedByCommaFromTextFile(ByVal vstrPath As String)

    Dim strLine As String, strValues() As String, strValue As String
    
    
    If Col Is Nothing Then
    
        Set Col = New Collection
    End If
    
    With New Scripting.FileSystemObject
    
        With .OpenTextFile(vstrPath)
        
            Do While Not .AtEndOfStream
            
                strLine = .ReadLine
            
                If strLine <> "" Then
                
                    strValues = Split(strLine, ";") ' old: ',' => ';'
                    
                    strValue = Trim(strValues(0))
                
                    If strValue <> "" Then
                    
                        Col.Add strValue
                    End If
                End If
            Loop
        
            .Close
        End With
    End With
    
End Sub



'///////////////////////////////////////////////
'/// Local Routines
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrTmpColumnName(ByVal vintColumnNumber As Long) As String

    If Col.Count < 10 Then
        
        mfstrTmpColumnName = "c" & CStr(vintColumnNumber)
    
    ElseIf Col.Count < 100 Then
        
        mfstrTmpColumnName = "c" & Format(vintColumnNumber, "00")
    
    ElseIf Col.Count < 1000 Then
        
        mfstrTmpColumnName = "c" & Format(vintColumnNumber, "000")
    
    ElseIf Col.Count < 10000 Then
        
        mfstrTmpColumnName = "c" & Format(vintColumnNumber, "0000")
    
    ElseIf Col.Count < 10000 Then
        
        mfstrTmpColumnName = "c" & Format(vintColumnNumber, "00000")
    Else
        mfstrTmpColumnName = "c" & Format(vintColumnNumber, "000000")
    End If
End Function


'''
''' check data type from the first element of Col
''' If the type of the first element is a object, this arises some errors
'''
Private Function mfblnTypeDetermined() As Boolean

    Dim varValue As Variant, dblValue As Double
    Dim strValue As String, strTipOne As String, strTipTwo As String
    
    
    mfblnTypeDetermined = mblnIsDataTypeDetermined
    
    If Not mblnIsDataTypeDetermined Then
        
        ' default type is String
        
        varValue = Col.Item(1)  ' If Col is nothing or Col has no item, arise a error.
        
        If StrComp("Date", TypeName(varValue)) = 0 Then    ' Old code: If IsDate(varValue) Then , but 'IsDate function also matches '19/01/11' string, but CDbl function doesn't matches '19/01/11'
            
            dblValue = CDbl(varValue)
            
            If dblValue < 1# Then
                
                mintDataType = mintDateOnlyTime
            Else
                If Abs(CDbl(Int(dblValue)) - dblValue) < 0.00000000001 Then
                    
                    mintDataType = mintDateOnlyDate
                Else
                    mintDataType = mintDateGeneral
                End If
            End If
        Else
            If IsNumeric(varValue) Then
                
                If ForceToServeChar Then
                    
                    mintDataType = mintString
                Else
                
                    strValue = CStr(varValue)
                    
                    strTipOne = Mid$(strValue, 1, 1)
                    
                    strTipTwo = Mid$(strValue, 1, 2)
                    
                    If StrComp(strTipOne, "0") = 0 And StrComp(strTipTwo, "0.") <> 0 Then
                        
                        mintDataType = mintString
                    Else
                        dblValue = CDbl(varValue)
                        
                        If Abs(CDbl(Int(dblValue)) - dblValue) < 0.00000000001 Then
                            
                            mintDataType = mintInteger
                        Else
                            mintDataType = mintReal
                        End If
                    End If
                End If
            Else
'                If IsLogical(varValue) Then
'                    mintDataType = mintBoolean
'                End If
            
            End If
        End If
    
        mblnIsDataTypeDetermined = True
    End If
    
    mfblnTypeDetermined = mblnIsDataTypeDetermined
End Function
