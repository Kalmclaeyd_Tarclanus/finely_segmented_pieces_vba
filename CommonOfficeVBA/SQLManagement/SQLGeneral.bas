Attribute VB_Name = "SQLGeneral"
'
'   SQL generation parameters
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Indpendent on ADO, this expects the PgSQL script creation.
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  8/Aug/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' CQRS classification, CQRS stands for the command and query responsibility segregation
'''
Public Enum SqlRdbCQRSClassification

    RdbSqlQueryType = 1       ' SELECT sentence, which means that the table-data is included in the return of the SQL results, If the ADO library is used, then the ADODB.Recordset object table-data is included in the SQL execution results.
    
    RdbSqlCommandType = 2     ' UPDATE, INSERT, DELETE sentence (DML, DDL)
End Enum

'''
'''
'''
Public Enum RdbSqlClassification

    RdbSqlSelectType = 1    ' SQL query (DML)
    
    RdbSqlUpdateType = 2    ' SQL command   (DML)
    
    RdbSqlInsertType = 3    ' SQL command   (DML)
    
    RdbSqlDeleteType = 4    ' SQL command   (DML)
    
    RdbSqlSelectIntoType = 5    ' SQL command   (DML)
    
    RdbSqlCreateTableType = 31  ' SQL command   (DDL)
End Enum

'''
''' when a SQL system refuses some regular expression WHERE condtion, such REGEXP_LIKE()
'''
Public Enum RdbSqlPluralStringLikeMatchType

    RdbSqlOrCond = 0      ' join Or operator
    
    RdbSqlAndCond = 1     ' join And operator
    
    
    RdbSqlLikePrefixSearch = 4    ' LIKE <StringItem>%
    
    RdbSqlLikeSuffixSearch = 8    ' LIKE %<StringItem>
    
    RdbSqlLikePartialSearch = 12  ' LIKE %<StringItem>%
End Enum

'''
''' used in PluralCondition class
'''
Public Enum RdbSqlDialectType

    RdbAdoAccessSqlDialectType = 0  ' SQL for Microsoft Access
    
    RdbOracleSqlDialectType = 4 ' SQL for Oracle
    
    RdbPostgreSqlDialectType = 1    ' SQL for PostgreSQL
    
    RdbSqLiteDialectType = 5    ' SQL for SQLite3
End Enum

'**---------------------------------------------
'** General data base connection information
'**---------------------------------------------
'''
'''
'''
Public Enum RdbConnectionInformationFlag

    NoInformationToConnectRdbFlag = 0

    UseDatabaseMsAdoObjectToConnectRdbFlag = &H1
    
    UseDatabaseMsDaoObjectToConnectRdbFlag = &H2  ' Connection destination is the only Microsoft Access

    
    UseShellAndRdbIndividualCommandLineToolToConnectRdbFlag = &H100 ' psql.exe for PostgreSQL database, sqlite3.exe for SQLite database
    
    UseMsOdbcDriverToConnectRdbFlag = &H200
    
    UseMsOleDbProviderToConnectRdbFlag = &H400
    
    
    ConnectToPgSqlRdbFlag = &H1000

    ConnectToOracleRdbFlag = &H2000
    
    ConnectToSqLiteRdbFlag = &H4000
    
    ConnectToMsAccessRdbFlag = &H8000
    
    ConnectToMsExcelSheetInBookAsDatabaseFlag = &H10000
    
    ConnectToCsvFileAsDatabaseFlag = &H20000
    
    
    AdoOdbcToSqLiteRdbFlag = UseDatabaseMsAdoObjectToConnectRdbFlag Or UseMsOdbcDriverToConnectRdbFlag Or ConnectToSqLiteRdbFlag
    
    AdoOdbcToPgSqlRdbFlag = UseDatabaseMsAdoObjectToConnectRdbFlag Or UseMsOdbcDriverToConnectRdbFlag Or ConnectToPgSqlRdbFlag
    
    AdoOdbcToOracleRdbFlag = UseDatabaseMsAdoObjectToConnectRdbFlag Or UseMsOdbcDriverToConnectRdbFlag Or ConnectToOracleRdbFlag
    
    AdoOleDbToOracleRdbFlag = UseDatabaseMsAdoObjectToConnectRdbFlag Or UseMsOleDbProviderToConnectRdbFlag Or ConnectToOracleRdbFlag
    
    AdoOleDbToMsAccessRdbFlag = UseDatabaseMsAdoObjectToConnectRdbFlag Or UseMsOleDbProviderToConnectRdbFlag Or ConnectToMsAccessRdbFlag
    
    AdoOleDbToMsExcelBookAsRdbFlag = UseDatabaseMsAdoObjectToConnectRdbFlag Or UseMsOleDbProviderToConnectRdbFlag Or ConnectToMsExcelSheetInBookAsDatabaseFlag
    
    AdoOleDbToCsvFileAsRdbFlag = UseDatabaseMsAdoObjectToConnectRdbFlag Or UseMsOleDbProviderToConnectRdbFlag Or ConnectToCsvFileAsDatabaseFlag
    
    DaoToMsAccessRdbFlag = UseDatabaseMsDaoObjectToConnectRdbFlag Or ConnectToMsAccessRdbFlag
End Enum


'**---------------------------------------------
'** logging option when SQL exection
'**---------------------------------------------
'''
''' SQLResult optional flag
'''
Public Enum SqlOptionalLogFlagOfWorksheet
    
    LogOfAdoConnectSettingOnSheet = &H1 ' ADO CursorLocation, ExecutionOption, CursorType, LockType
    
    LogOfCurrentUserComputerDomainOnSheet = &H2    ' Current Windows User, Computer Name, IPv4 Address
End Enum

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** classify SQL
'**---------------------------------------------
'''
'''
'''
Public Function GetSQLSyntaxType(ByVal vstrSQL As String) As RdbSqlClassification
    
    Dim enmType As RdbSqlClassification, strLowerCaseSQL As String, intP1 As Long
    
    strLowerCaseSQL = LCase(vstrSQL)
    
    If InStr(1, strLowerCaseSQL, "delete") > 0 Then
    
        enmType = RdbSqlClassification.RdbSqlDeleteType
        
    ElseIf InStr(1, strLowerCaseSQL, "update") > 0 Then
    
        enmType = RdbSqlClassification.RdbSqlUpdateType
        
    ElseIf InStr(1, strLowerCaseSQL, "insert") > 0 Then
    
        enmType = RdbSqlClassification.RdbSqlInsertType
        
    ElseIf InStr(1, strLowerCaseSQL, "select") > 0 Then
    
        If InStr(InStr(1, strLowerCaseSQL, "select") + 6, strLowerCaseSQL, "into") > 0 Then
        
            enmType = RdbSqlClassification.RdbSqlSelectIntoType ' SQL command
        Else
            enmType = RdbSqlClassification.RdbSqlSelectType ' SQL query
        End If
    
    ElseIf InStr(1, strLowerCaseSQL, "create table") > 0 Then
    
        enmType = RdbSqlCreateTableType
    End If

    GetSQLSyntaxType = enmType
End Function

'**---------------------------------------------
'** SQL generation
'**---------------------------------------------
'''
'''
'''
Public Function GetSQLWherePartOfDateBetweenInAMonth(ByVal vstrColumnTitle As String, _
        ByVal vintYear As Long, _
        ByVal vintMonth As Long, _
        Optional ByVal venmSQLInstanceType As RdbSqlDialectType = RdbSqlDialectType.RdbAdoAccessSqlDialectType) As String


    Dim udtDate As Date, udtEndOfMonthDate As Date, strSQLWherePart As String
    
    udtDate = DateSerial(vintYear, vintMonth, 1)

    udtEndOfMonthDate = DateAdd("m", 1, udtDate)
    
    udtEndOfMonthDate = DateAdd("d", -1, udtEndOfMonthDate)

    'Debug.Print Format(udtEndOfMonthDate, "yyyy/m/d")

    Select Case venmSQLInstanceType
    
        Case RdbSqlDialectType.RdbAdoAccessSqlDialectType
    
            strSQLWherePart = vstrColumnTitle & " BETWEEN #" & Format(udtDate, "yyyy/m/d") & "# AND #" & Format(udtEndOfMonthDate, "yyyy/m/d") & "#"
        
        Case RdbSqlDialectType.RdbPostgreSqlDialectType
    
            strSQLWherePart = vstrColumnTitle & " between cast('" & Format(udtDate, "yyyymmdd") & "' as date) and cast('" & Format(udtEndOfMonthDate, "yyyymmdd") & "' as date)"

        Case RdbSqlDialectType.RdbOracleSqlDialectType
        
            strSQLWherePart = vstrColumnTitle & " BETWEEN TO_DATE('" & Format(udtDate, "yyyymmdd") & "', 'YYYYMMDD') AND TO_DATE('" & Format(udtEndOfMonthDate, "yyyymmmd") & "', 'YYYYMMDD')"
        
    End Select

    GetSQLWherePartOfDateBetweenInAMonth = strSQLWherePart
End Function



'''
'''
'''
Public Function GetSQLSelectFields(ByVal vobjFieldNames As Collection, Optional ByVal vstrAliasTableName As String = "") As String
    
    Dim strSQLSelect As String, varFieldTitle As Variant, strFieldTitle As String, blnAddAliasTableName As Boolean
    Dim i As Long
    
    blnAddAliasTableName = False
    
    If vstrAliasTableName <> "" Then
    
        blnAddAliasTableName = True
    End If
    
    i = 1: strSQLSelect = ""
    
    For Each varFieldTitle In vobjFieldNames
    
        strFieldTitle = varFieldTitle
    
        If blnAddAliasTableName Then
        
            strSQLSelect = strSQLSelect & vstrAliasTableName & "." & strFieldTitle
        Else
            strSQLSelect = strSQLSelect & strFieldTitle
        End If
    
        If i < vobjFieldNames.Count Then
        
            strSQLSelect = strSQLSelect & ", "
        End If
        
        i = i + 1
    Next

    GetSQLSelectFields = strSQLSelect
End Function

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetSQLWherePartOfDateBetweenInAMonth()

    Debug.Print GetSQLWherePartOfDateBetweenInAMonth("�N����", 2023, 5)
End Sub
