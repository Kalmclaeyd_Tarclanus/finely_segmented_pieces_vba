Attribute VB_Name = "FormTopMostEnabledUtilities"
'
'   Utility for FormTopMostEnabledCtlHdr class
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on MSForms
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 10/Aug/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub InitializeFormTopMostEnabledControlHandler(ByRef robjFormTopMostEnabledCtlHdr As FormTopMostEnabledCtlHdr, ByRef robjForm As MSForms.UserForm, Optional ByVal vblnAllowToSetTopMost As Boolean = False)

    If robjFormTopMostEnabledCtlHdr Is Nothing Then Set robjFormTopMostEnabledCtlHdr = New FormTopMostEnabledCtlHdr
    
    With robjFormTopMostEnabledCtlHdr
    
        Set .ParentUserForm = robjForm
        
        If vblnAllowToSetTopMost Then
        
            .SetFormToTopMost   ' this is enabled when the Form is shown by modeless
        End If
    End With
End Sub



