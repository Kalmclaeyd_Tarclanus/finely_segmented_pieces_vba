VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "FormTopMostEnabledCtlHdr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Aggregate the control codes of changing window TopMost state, but the Form control class
'
'   Dependency Abstract:
'       Dependent on both MSForms and Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  9/Feb/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If Win64 Then
    Private Declare PtrSafe Function GetWindowLongPtr Lib "user32" Alias "GetWindowLongPtrA" (ByVal hwnd As LongPtr, ByVal nIndex As Long) As LongPtr
    Private Declare PtrSafe Function SetWindowLongPtr Lib "user32" Alias "SetWindowLongPtrA" (ByVal hwnd As LongPtr, ByVal nIndex As Long, ByVal dwNewLong As LongPtr) As LongPtr
    Private Declare PtrSafe Function GetActiveWindow Lib "user32" () As LongPtr
#ElseIf VBA7 Then
    Private Declare PtrSafe Function GetWindowLongPtr Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As LongPtr, ByVal nIndex As Long) As LongPtr
    Private Declare PtrSafe Function SetWindowLongPtr Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As LongPtr, ByVal nIndex As Long, ByVal dwNewLong As LongPtr) As LongPtr
    Private Declare PtrSafe Function GetActiveWindow Lib "user32" () As LongPtr
#Else
    Private Declare PtrSafe Function GetWindowLongPtr Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
    Private Declare PtrSafe Function SetWindowLongPtr Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
    Private Declare PtrSafe Function GetActiveWindow Lib "user32" () As Long
#End If


#If VBA7 Then
    Private Declare PtrSafe Function SetWindowPos Lib "user32" (ByVal hwnd As LongPtr, ByVal hWndInsertAfter As LongPtr, ByVal x As LongPtr, ByVal y As LongPtr, ByVal cx As LongPtr, ByVal cy As LongPtr, ByVal uFlags As LongPtr) As LongPtr
    Private Declare PtrSafe Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As LongPtr
    
    Private Declare PtrSafe Function GetWindowInfo Lib "user32" (ByVal hwnd As LongPtr, ByRef rstdWindowInfo As WindowInfoType) As Boolean
#Else
    Private Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
    Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
    
    Private Declare Function GetWindowInfo Lib "user32" (ByVal hWnd As Long, ByRef rstdWindowInfo As WindowInfoType) As Boolean
#End If

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type WindowInfoType
    cbSize As Long              ' The size of the structure, in bytes. The caller must set this member to sizeof(WINDOWINFO)
    rcWindow As RECT            ' The coordinates of the window.
    rcClient As RECT            ' The coordinates of the client area.
    dwStyle As Long             ' The window styles. For a table of window styles, see Window Styles.
    dwExStyle As Long           ' The extended window styles. For a table of extended window styles, see Extended Window
    dwWindowStatus As Long      ' The window status. If this member is WS_ACTIVECAPTION (0x0001), the window is active. Otherwise, this member is zero.
    cxWindowBorders As Long     ' The width of the window border, in pixels.
    cyWindowBorders As Long     ' The height of the window border, in pixels.
    atomWindowType As Integer   ' The window class atom (see RegisterClass).
    wCreatorVersion As Integer  ' The Windows version of the application that created the window.
End Type

'''
''' The following are the window styles. After the window has been created, these styles cannot be modified, except as noted.
'''
Private Enum WINDOWS_STYLES
    WS_BORDER = &H800000        ' The window has a thin-line border.
    WS_CAPTION = &HC00000       ' Allow to include the window title bar (includes the WS_BORDER style).
    WS_CHILD = &H40000000       ' The window is a child window. A window with this style cannot have a menu bar. This style cannot be used with the WS_POPUP style.
    WS_CLIPCHILDREN = &H2000000 ' Same as the WS_CHILD style.
    WS_CLIPSIBLINGS = &H4000000 ' Excludes the area occupied by child windows when drawing occurs within the parent window. This style is used when creating the parent window.
    WS_DISABLED = &H8000000     ' The window is initially disabled. A disabled window cannot receive input from the user. To change this after a window has been created, use the EnableWindow function.
    WS_DLGFRAME = &H400000      ' The window has a border of a style typically used with dialog boxes. A window with this style cannot have a title bar.
    WS_HSCROLL = &H100000       ' The window has a horizontal scroll bar.
    WS_MINIMIZE = &H20000000    ' The window is initially minimized. Same as the WS_ICONIC style.
    WS_MAXIMIZE = &H1000000     ' The window is initially maximized.
    WS_MAXIMIZEBOX = &H10000    ' At the right-top of the window, allow to include the maximize button, which is combined with the WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be included.
    WS_MINIMIZEBOX = &H20000    ' At the right-top of the window, allow to include the minimize button. Cannot be combined with the WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be specified.
    WS_OVERLAPPED = &H0         ' The window is an overlapped window. An overlapped window has a title bar and a border. Same as the WS_TILED style.
    WS_POPUP = &H80000000       ' The window is a pop-up window. This style cannot be used with the WS_CHILD style.
    WS_SIZEBOX = &H40000        ' The window has a sizing border. Same as the WS_THICKFRAME style.
    WS_SYSMENU = &H80000        ' Create the control menu boxes with the window style. The window has a window menu on its title bar. The WS_CAPTION style must also be specified.
    WS_VISIBLE = &H10000000     ' The window is initially visible. This style can be turned on and off by using the ShowWindow or SetWindowPos function.
    WS_VSCROLL = &H200000       ' The window has a vertical scroll bar.
End Enum

'''
''' he following are the extended window styles.
'''
Private Enum EXTENDED_WINDOWS_STYLES
    WS_EX_CLIENTEDGE = &H200    ' The window has a border with a sunken edge.
    WS_EX_CONTEXTHELP = &H400   ' The title bar of the window includes a question mark. When the user clicks the question mark, the cursor changes to a question mark with a pointer. If the user then clicks a child window, the child receives a WM_HELP message. The child window should pass the message to the parent window procedure, which should call the WinHelp function using the HELP_WM_HELP command. The Help application displays a pop-up window that typically contains help for the child window.
    WS_EX_DLGMODALFRAME = &H1   ' Add a double border with the extended windows style (Dialog modal frame). It is necessary to add the title-bar. The window has a double border; the window can, optionally, be created with a title bar by specifying the WS_CAPTION style in the dwStyle parameter.
    WS_EX_NOACTIVATE = &H8000000 ' A top-level window created with this style does not become the foreground window when the user clicks it. The system does not bring this window to the foreground when the user minimizes or closes the foreground window. The window should not be activated through programmatic access or via keyboard navigation by accessible technology, such as Narrator. To activate the window, use the SetActiveWindow or SetForegroundWindow function. The window does not appear on the taskbar by default. To force the window to appear on the taskbar, use the WS_EX_APPWINDOW style.
    WS_EX_TOPMOST = &H8         ' The window should be placed above all non-topmost windows and should stay above them, even when the window is deactivated. To add or remove this style, use the SetWindowPos function.
    WS_EX_TRANSPARENT = &H20    ' The window should not be painted until siblings beneath the window (that were created by the same thread) have been painted. The window appears transparent because the bits of underlying sibling windows have already been painted. To achieve transparency without these restrictions, use the SetWindowRgn function.
End Enum

'///////////////////////////////////////////////
'/// Windows API Constants
'///////////////////////////////////////////////
Private Const GWL_EXSTYLE = (-20)               ' Sets/Gets a new extended window style.
Private Const GWL_STYLE As Integer = (-16)      ' Sets/Gets a new window style.

'Private Const WS_EX_DLGMODALFRAME As Long = &H1 ' The window has a double border; the window can, optionally, be created with a title bar by specifying the WS_CAPTION style in the dwStyle parameter
'Private Const WS_MAXIMIZEBOX As Long = &H10000  ' The window has a maximize button. Cannot be combined with the WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be specified.
'Private Const WS_MINIMIZEBOX As Long = &H20000  ' The window has a minimize button. Cannot be combined with the WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be specified.
Private Const WS_THICKFRAME As Long = &H40000   ' The window has a sizing border. Same as the WS_SIZEBOX style.
'Private Const WS_SYSMENU As String = &H80000    ' The window has a window menu on its title bar. The WS_CAPTION style must also be specified.
'Private Const WS_CAPTION As Long = &HC00000     ' The window has a title bar (includes the WS_BORDER style).

Private Const SWP_VISIBLE = &H40 ' SWP_SHOWWINDOW - Displays the window.
Private Const SWP_NOMOVE = &H2  ' Retains the current position (ignores X and Y parameters).
Private Const SWP_NOSIZE = &H1  ' Retains the current size (ignores the cx and cy parameters).

Private Const HWND_TOP = 0  ' Places the window at the top of the Z order.
Private Const HWND_BOTTOM = 1   ' Places the window at the bottom of the Z order.
Private Const HWND_TopMost = -1 ' Places the window above all non-topmost windows.
Private Const HWND_RemoveTopMost = -2   ' HWND_NOTOPMOST - Places the window above all non-topmost windows (that is, behind all topmost windows).

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "FormTopMostEnabledCtlHdr"


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private WithEvents mobjUserForm As MSForms.UserForm
Attribute mobjUserForm.VB_VarHelpID = -1

Private mobjUserDefinedForm As Object

Private WithEvents mobjWindowTopMostPlacingEnablingButton As ToggleButton
Attribute mobjWindowTopMostPlacingEnablingButton.VB_VarHelpID = -1

Private mintInitializing As Long

'**---------------------------------------------
'** Key-Value cache preparation for FormTopMostEnabledCtlHdr
'**---------------------------------------------
Private mobjStrKeyValueFormTopMostEnabledCtlHdrDic As Scripting.Dictionary

Private mblnIsStrKeyValueFormTopMostEnabledCtlHdrDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Set ParentUserForm(ByVal vobjUserForm As Object)

    Set mobjUserDefinedForm = vobjUserForm
    
    Set mobjUserForm = vobjUserForm
End Property

Public Property Set WindowTopMostPlacingEnablingButton(ByVal vobjToggleButton As ToggleButton)

    Set mobjWindowTopMostPlacingEnablingButton = vobjToggleButton
End Property


'''
''' Change the TopMost window state. However it is disabled when the window is invisible
'''
Public Property Get IsWindowTopMostPlacingEnabled() As Boolean

    Dim blnIsWindowTopMostPlacingEnabled As Boolean
    Dim stdWindowsInfo As WindowInfoType
    
#If VBA7 Then
    Dim intWindowHandle As LongPtr
#Else
    Dim intWindowHandle As Long
#End If

    blnIsWindowTopMostPlacingEnabled = False

    ' If Window is not shown, FindWindow API will be failed.
    intWindowHandle = FindWindow(vbNullString, mobjUserDefinedForm.Caption)
    
    If intWindowHandle <> 0 Then
    
        If GetWindowInfo(intWindowHandle, stdWindowsInfo) Then
            
            If (stdWindowsInfo.dwExStyle And EXTENDED_WINDOWS_STYLES.WS_EX_TOPMOST) > 0 Then
            
                blnIsWindowTopMostPlacingEnabled = True
            Else
                blnIsWindowTopMostPlacingEnabled = False
            End If
        End If
    End If

    IsWindowTopMostPlacingEnabled = blnIsWindowTopMostPlacingEnabled
End Property
Public Property Let IsWindowTopMostPlacingEnabled(ByVal vblnIsWindowTopMostPlacingEnabled As Boolean)
    
    #If VBA7 Then
        Dim intWindowHandle As LongPtr
    #Else
        Dim intWindowHandle As Long
    #End If

    intWindowHandle = FindWindow(vbNullString, mobjUserDefinedForm.Caption)
    
    If intWindowHandle <> 0 Then
    
        If vblnIsWindowTopMostPlacingEnabled Then
            
            SetWindowPos intWindowHandle, HWND_TopMost, 0, 0, 0, 0, SWP_VISIBLE Or SWP_NOMOVE Or SWP_NOSIZE
        Else
            SetWindowPos intWindowHandle, HWND_RemoveTopMost, 0, 0, 0, 0, SWP_VISIBLE Or SWP_NOMOVE Or SWP_NOSIZE
        End If
    End If
End Property


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()
    
    mintInitializing = 0
    
End Sub

'''
''' TopMost change
'''
Private Sub mobjWindowTopMostPlacingEnablingButton_Click()

    If mintInitializing <= 0 Then

        If Me.IsWindowTopMostPlacingEnabled() Then
        
            Me.IsWindowTopMostPlacingEnabled = False
        Else
        
            Me.IsWindowTopMostPlacingEnabled = True
        End If
    End If
    
    RefreshWindowTopMostPlacingEnablingToggleButton
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' If you need TopMost setting, you should execute in the UserControl_Initialize of the parent UserForm
'''
Public Sub SetFormToTopMost()

    Me.IsWindowTopMostPlacingEnabled = True
End Sub

'''
''' you should execute in the UserControl_Initialize of the parent UserForm
'''
Public Sub RefreshWindowTopMostPlacingEnablingToggleButton()

    If Not mobjWindowTopMostPlacingEnablingButton Is Nothing And mintInitializing <= 0 Then
        
        mintInitializing = mintInitializing + 1
    
        On Error GoTo ErrHandler:
    
        With mobjWindowTopMostPlacingEnablingButton
            
            If Me.IsWindowTopMostPlacingEnabled() Then
                
                .Value = True
                .Caption = GetTextOfStrKeyFormTopMostEnabledCtlHdrDisabling()
                
            Else
                .Value = False
                .Caption = GetTextOfStrKeyFormTopMostEnabledCtlHdrEnabling()
            End If
        End With
    
ErrHandler:

        mintInitializing = mintInitializing - 1
    End If
End Sub


'**---------------------------------------------
'** Key-Value cache preparation for FormTopMostEnabledCtlHdr
'**---------------------------------------------
'''
''' get string Value from STR_KEY_FORM_TOP_MOST_ENABLED_CTL_HDR_DISABLING
'''
''' automatically-added for FormTopMostEnabledCtlHdr string-key-value management for standard module and class module
Private Function GetTextOfStrKeyFormTopMostEnabledCtlHdrDisabling() As String

    If Not mblnIsStrKeyValueFormTopMostEnabledCtlHdrDicInitialized Then

        msubInitializeTextForFormTopMostEnabledCtlHdr
    End If

    GetTextOfStrKeyFormTopMostEnabledCtlHdrDisabling = mobjStrKeyValueFormTopMostEnabledCtlHdrDic.Item("STR_KEY_FORM_TOP_MOST_ENABLED_CTL_HDR_DISABLING")
End Function

'''
''' get string Value from STR_KEY_FORM_TOP_MOST_ENABLED_CTL_HDR_ENABLING
'''
''' automatically-added for FormTopMostEnabledCtlHdr string-key-value management for standard module and class module
Private Function GetTextOfStrKeyFormTopMostEnabledCtlHdrEnabling() As String

    If Not mblnIsStrKeyValueFormTopMostEnabledCtlHdrDicInitialized Then

        msubInitializeTextForFormTopMostEnabledCtlHdr
    End If

    GetTextOfStrKeyFormTopMostEnabledCtlHdrEnabling = mobjStrKeyValueFormTopMostEnabledCtlHdrDic.Item("STR_KEY_FORM_TOP_MOST_ENABLED_CTL_HDR_ENABLING")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for FormTopMostEnabledCtlHdr string-key-value management for standard module and class module
Private Sub msubInitializeTextForFormTopMostEnabledCtlHdr()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueFormTopMostEnabledCtlHdrDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForFormTopMostEnabledCtlHdrByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForFormTopMostEnabledCtlHdrByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueFormTopMostEnabledCtlHdrDicInitialized = True
    End If

    Set mobjStrKeyValueFormTopMostEnabledCtlHdrDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for FormTopMostEnabledCtlHdr key-values cache
'''
''' automatically-added for FormTopMostEnabledCtlHdr string-key-value management for standard module and class module
Private Sub AddStringKeyValueForFormTopMostEnabledCtlHdrByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_FORM_TOP_MOST_ENABLED_CTL_HDR_DISABLING", "disabling to place window of the top-most"
        .Add "STR_KEY_FORM_TOP_MOST_ENABLED_CTL_HDR_ENABLING", "Enabling to place window of the top-most"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for FormTopMostEnabledCtlHdr key-values cache
'''
''' automatically-added for FormTopMostEnabledCtlHdr string-key-value management for standard module and class module
Private Sub AddStringKeyValueForFormTopMostEnabledCtlHdrByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_FORM_TOP_MOST_ENABLED_CTL_HDR_DISABLING", "最前面表示解除"
        .Add "STR_KEY_FORM_TOP_MOST_ENABLED_CTL_HDR_ENABLING", "最前面表示固定"
    End With
End Sub

'''
''' Remove Keys for FormTopMostEnabledCtlHdr key-values cache
'''
''' automatically-added for FormTopMostEnabledCtlHdr string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueFormTopMostEnabledCtlHdr(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_FORM_TOP_MOST_ENABLED_CTL_HDR_DISABLING") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_FORM_TOP_MOST_ENABLED_CTL_HDR_DISABLING"
            .Remove "STR_KEY_FORM_TOP_MOST_ENABLED_CTL_HDR_ENABLING"
        End If
    End With
End Sub


