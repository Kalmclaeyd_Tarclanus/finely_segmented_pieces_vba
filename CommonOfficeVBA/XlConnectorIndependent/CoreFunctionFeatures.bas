Attribute VB_Name = "CoreFunctionFeatures"
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Feature flag contrl in order to automize the unit tests.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 17/Jan/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

' Uninitialized Boolean value is False in the Visual Basic for Application(7.0) language specification.

'**---------------------------------------------
'** About installed editors
'**---------------------------------------------
Public FeatureOfSakuraEditorDisabling As Boolean    ' If it is True, this system emulates behaviors as if the Sakura editor (sakura.exe) hasn't been installed.

Public FeatureOfVSCodeDisabling As Boolean  ' If it is True, this system emulates behaviors as if the Visual Studio Code hasn't been installed.

'**---------------------------------------------
'** About using Windows system shells
'**---------------------------------------------
Public FeatureOfWshShellRunDisabling As Boolean     ' It it is True, this system emulates an environment which WshShell.Run is sure to be failed for reasons such as ActiveDirectory domain service limitations.

'**---------------------------------------------
'** About forcing locale
'**---------------------------------------------
Public FeatureToForceLocaleToUseJapanese As Boolean

Public FeatureToForceLocaleToUseEnglish As Boolean


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About this machine locale
'**---------------------------------------------
'''
'''
'''
Public Sub ExchangeFeatureToForceLocaleToUseEnglishState()

    msubChangeFeatureEnablingStaticVariable FeatureToForceLocaleToUseEnglish, "FeatureToForceLocaleToUseEnglish"
End Sub


'**---------------------------------------------
'** About installed editors
'**---------------------------------------------
'''
'''
'''
Public Sub ExchangeSakuraEditorFeatureDisablingState()

    msubChangeFeatureEnablingStaticVariable FeatureOfSakuraEditorDisabling, "FeatureOfSakuraEditorDisabling"
End Sub

'''
'''
'''
Public Sub ExchangeVSCodeEditorFeatureDisablingState()

    msubChangeFeatureEnablingStaticVariable FeatureOfVSCodeDisabling, "FeatureOfVSCodeDisabling"
End Sub

'''
'''
'''
Private Sub DisablingAllExternalEditors()

    FeatureOfSakuraEditorDisabling = True
    
    FeatureOfVSCodeDisabling = True
End Sub

'**---------------------------------------------
'** About permitted Windows shells
'**---------------------------------------------
'''
'''
'''
Public Sub ChangeWshShellRunDisablingFeatureDisabledWithMsgBox()

    ChangeWshShellRunDisablingFeatureDisabled
End Sub
'''
'''
'''
Public Sub ChangeWshShellRunDisablingFeatureDisabled(Optional ByVal vblnAllowToShowMsgBox As Boolean = True)

    msubChangeFeatureEnablingStaticVariable FeatureOfWshShellRunDisabling, "FeatureOfWshShellRunDisabling", vblnAllowToShowMsgBox
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubChangeFeatureEnablingStaticVariable(ByRef rblnFeature As Boolean, ByVal vstrFeatureName As String, Optional ByVal vblnAllowToShowMsgBox As Boolean = True, Optional ByVal vblnAllowToUseWinAPI As Boolean = True)

    Dim enmMsgBoxStyle As VbMsgBoxStyle
    
    rblnFeature = Not rblnFeature
    
    If vblnAllowToShowMsgBox Then
    
        If rblnFeature Then
            
            enmMsgBoxStyle = vbOKOnly Or vbExclamation
        Else
            enmMsgBoxStyle = vbOKOnly Or vbInformation
        End If
    
        If vblnAllowToUseWinAPI Then
        
            MessageBoxWithTimeoutBySecondUnit "Current " & vstrFeatureName & " is " & rblnFeature, 3, GetCurrentOfficeFileObject().Application.Name & " System feature enabled state", enmMsgBoxStyle
        Else
            MsgBox "Current " & vstrFeatureName & " is " & rblnFeature, enmMsgBoxStyle
        End If
    End If

End Sub

