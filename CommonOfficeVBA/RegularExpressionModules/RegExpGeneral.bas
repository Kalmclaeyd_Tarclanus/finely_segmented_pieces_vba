Attribute VB_Name = "RegExpGeneral"
'
'   RegExp general processing
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon,  6/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrVbConstTabDelimiter As String = "\t"

Private Const mstrVbConstNewLineDelimiter As String = "\r\n"

Private Const mstrRegExpEscapeTypeDelimiters As String = "\(\)\[\]\{\}\.\,\;\:\*\/\+\-\=\\\$\&\|\'\""\#\<\>"

Private Const mstrRegExpDelimiters As String = " @" & mstrVbConstTabDelimiter & mstrVbConstNewLineDelimiter & mstrRegExpEscapeTypeDelimiters

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjConvertTabToPatternCharRegExp As VBScript_RegExp_55.RegExp

Private mobjConvertNewLineToPatternCharRegExp As VBScript_RegExp_55.RegExp

Private mobjConvertSpecialCharactersToEscapePatternCharRegExp As VBScript_RegExp_55.RegExp


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>rstrText: Input-Output</Argument>
''' <Argument>robjReplaceWordDic: Input</Argument>
''' <Argument>vblnIgnoreCase: Input</Argument>
Public Sub ReplaceUnitWordByRegExp(ByRef rstrText As String, _
        ByRef robjReplaceWordDic As Scripting.Dictionary, _
        Optional ByVal vblnIgnoreCase As Boolean = False)



    Dim objReplacingRegExp As VBScript_RegExp_55.RegExp, objUnitWordWithoutBothSideExpandedPatternRegExp As VBScript_RegExp_55.RegExp
    
    Dim objExpandedUnitWordRegExp As VBScript_RegExp_55.RegExp
    
    Dim objTestingExpandedUnitWordRegExp As VBScript_RegExp_55.RegExp
    
    Dim varKey As Variant, strBeforeWord As String, strAfterWord As String
    
    Dim objMatch As VBScript_RegExp_55.Match, objSubMatches As VBScript_RegExp_55.SubMatches
    
    Dim blnNeedToRegExpReplace As Boolean
    
    
    
    Set objReplacingRegExp = mfobjGetInitialUnitWordRegExp(vblnIgnoreCase)

    Set objUnitWordWithoutBothSideExpandedPatternRegExp = mfobjGetInitialUnitWordRegExp(vblnIgnoreCase)
    
    Set objTestingExpandedUnitWordRegExp = New VBScript_RegExp_55.RegExp
    
    With objTestingExpandedUnitWordRegExp
    
        .IgnoreCase = vblnIgnoreCase
    
        .Global = False
    End With
    
    
    With robjReplaceWordDic
    
        For Each varKey In .Keys
        
            strBeforeWord = varKey: strAfterWord = .Item(varKey)
        
            With objUnitWordWithoutBothSideExpandedPatternRegExp
            
                .Pattern = mfstrGetRegExpPatternOfUnitWordWithoutBothSideExpandedPattern(strBeforeWord)
            
                If .Test(rstrText) Then
                
                    blnNeedToRegExpReplace = False
                
                    For Each objMatch In .Execute(rstrText)
                    
                        Set objSubMatches = objMatch.SubMatches
                    
                        Select Case True
                        
                            Case objSubMatches.Item(0) <> ""    ' perfect match
                            
                                msubRegExpReplaceWithOptionsOfPrefixMatchOrSuffixMatch rstrText, objReplacingRegExp, strBeforeWord, strAfterWord, True, True
                                
                            Case objSubMatches.Item(1) <> ""    ' string suffix match
                            
                                msubRegExpReplaceWithOptionsOfPrefixMatchOrSuffixMatch rstrText, objReplacingRegExp, strBeforeWord, strAfterWord, False, True
                            
                            Case objSubMatches.Item(2) <> ""    ' string prefix match
                            
                                msubRegExpReplaceWithOptionsOfPrefixMatchOrSuffixMatch rstrText, objReplacingRegExp, strBeforeWord, strAfterWord, True, False
                        End Select
                    Next
                End If
            End With
            
            With objTestingExpandedUnitWordRegExp
            
                .Pattern = mfstrGetRegExpPatternOfExpandedUnitWord(strBeforeWord, True)
            
                If .Test(rstrText) Then
                
                    ' string match among the whole text
                
                    If objExpandedUnitWordRegExp Is Nothing Then
                    
                        Set objExpandedUnitWordRegExp = mfobjGetInitialUnitWordRegExp(vblnIgnoreCase)
                    End If
                
                    With objExpandedUnitWordRegExp
                    
                        .Pattern = mfstrGetRegExpPatternOfExpandedUnitWord(strBeforeWord)
                    
                        rstrText = .Replace(rstrText, "$1" & strAfterWord & "$2")
                    End With
                End If
            End With
        Next
    End With
End Sub


'''
'''
'''
''' <Argument>rstrTestString: Input</Argument>
''' <Argument>robjRegExp: Input</Argument>
''' <Return>String: log-text</Return>
Public Function GetLogTextOfRegExpHitTest(ByRef rstrTestString As String, _
        ByRef robjRegExp As VBScript_RegExp_55.RegExp) As String
    
    
    Dim strLog As String, objMatch As VBScript_RegExp_55.Match, i As Long, j As Long
    
    Dim objSubMatches As VBScript_RegExp_55.SubMatches, varSubMatch As Variant
    

    'strLog = "RegExp test - [" & rstrTestString & "] =>> "

    With robjRegExp
    
        If .Test(rstrTestString) Then
        
            'strLog = strLog & "Hit" & vbNewLine
            
            i = 1
            
            For Each objMatch In .Execute(rstrTestString)
            
                With objMatch
                
                    If strLog <> "" Then
                    
                        strLog = strLog & vbNewLine
                    End If
                    
                    strLog = strLog & "Hit " & CStr(i) & " : [" & CStr(.Value) & "]"
                    
                    If Not .SubMatches Is Nothing Then
                    
                        Set objSubMatches = .SubMatches
                    
                        If objSubMatches.Count > 0 Then
                    
                            j = 1
                            
                            For Each varSubMatch In objSubMatches
                    
                                strLog = strLog & ", SubMatch(" & CStr(j) & ") : [" & CStr(varSubMatch) & "]"
                            
                                j = j + 1
                            Next
                        End If
                    End If
                End With
                
                i = i + 1
            Next
        Else
            strLog = strLog & "No-hit : {" & rstrTestString & "}"
        End If
    End With

    GetLogTextOfRegExpHitTest = strLog
End Function

'''
'''
'''
''' <Argument>vobjMatches: Inputs</Argument>
''' <Return>String: log-text</Return>
Public Function GetConnectedMatchedString(ByVal vobjMatches As VBScript_RegExp_55.MatchCollection) As String

    Dim strItem As String, objMatch As VBScript_RegExp_55.Match
    
    strItem = ""
    
    For Each objMatch In vobjMatches
    
        strItem = strItem & objMatch.Value
    Next
    
    GetConnectedMatchedString = strItem
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToReplaceUnitWordByRegExp02()

    Dim varText As Variant, strText As String, strBeforeText As String
    

    For Each varText In mfobjGetTextsOfTest02()

        strText = varText: strBeforeText = strText

        ReplaceUnitWordByRegExp strText, mfobjGetSample02ReplaceWordDic()
        
        msubDebugOutForBeforeTextAndAfterText strBeforeText, strText
    Next
End Sub

'''
'''
'''
Private Sub msubSanityTestToReplaceSimplyByRegExp02()

    Dim varText As Variant, strText As String, strBeforeText As String
    

    For Each varText In mfobjGetTextsOfTest02()

        strText = varText: strBeforeText = strText

        msubReplaceSimplyByRegExp strText, mfobjGetSample02ReplaceWordDic()
        
        ' The following will be failed to replace by each individual word
        
        msubDebugOutForBeforeTextAndAfterText strBeforeText, strText
    Next
End Sub
    
'''
'''
'''
Private Sub msubSanityTestToReplaceUnitWordByRegExp01()

    Dim varText As Variant, strText As String
    
    Dim strBeforeText As String
    

    For Each varText In mfobjGetTextsOfTest01()

        strText = varText
        
        strBeforeText = strText

        ReplaceUnitWordByRegExp strText, mfobjGetSample01ReplaceWordDic()
        
        msubDebugOutForBeforeTextAndAfterText strBeforeText, strText
    Next
End Sub


'''
'''
'''
Private Sub msubSanityTestToReplaceUnitWordByRegExpWithCacheDic()

    Dim varText As Variant, strText As String
    
    Dim strBeforeText As String
    

    For Each varText In mfobjGetTextsOfTest01()

        strText = varText
        
        strBeforeText = strText

        msubReplaceUnitWordByRegExpWithCacheDic strText, mfobjGetSample01ReplaceWordDic(), True
        
        msubDebugOutForBeforeTextAndAfterText strBeforeText, strText
    Next
End Sub

'''
'''
'''
Private Sub msubSanityTestToReplaceSimplyByRegExp()

    Dim varText As Variant, strText As String
    
    Dim strBeforeText As String
    

    For Each varText In mfobjGetTextsOfTest01()

        strText = varText
        
        strBeforeText = strText

        msubReplaceSimplyByRegExp strText, mfobjGetSample01ReplaceWordDic()
        
        msubDebugOutForBeforeTextAndAfterText strBeforeText, strText
    Next
End Sub

'''
'''
'''
Private Function mfobjGetSample01ReplaceWordDic() As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add "For", "fOR"
    
    End With
    
    Set mfobjGetSample01ReplaceWordDic = objDic
End Function

'''
'''
'''
Private Sub msubDebugOutForBeforeTextAndAfterText(ByRef rstrBeforeText As String, ByRef rstrAfterText As String)

    If StrComp(rstrBeforeText, rstrAfterText) = 0 Then
    
        Debug.Print "Before: [" & rstrBeforeText & "], Equals to ... After: [" & rstrAfterText & "]"
    Else
        Debug.Print "Before: [" & rstrBeforeText & "], After: [" & rstrAfterText & "]"
    End If
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetHitWord()

    Dim objRegExp As VBScript_RegExp_55.RegExp
    
    Dim varText As Variant, strText As String, strHitLog As String
    
    
    Set objRegExp = New VBScript_RegExp_55.RegExp
    
    With objRegExp
    
        .Pattern = mfstrGetRegExpPatternOfUnitWord("For")
    
        .IgnoreCase = False
        
        .Global = True
    End With
    
    
    For Each varText In mfobjGetTextsOfTest01()
    
        strText = varText
        
        
        strHitLog = GetLogTextOfRegExpHitTest(strText, objRegExp)
    
        Debug.Print strHitLog
    Next
End Sub


'''
'''
'''
Private Function mfobjGetTextsOfTest02() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add " XYZ02 YZ02 YZ0252"
        
        .Add " XYZ025 YZ0252 AYZ02"
    
        .Add " X.YZ02 TX/YZ02*XYZ02"
    End With

    Set mfobjGetTextsOfTest02 = objCol
End Function

'''
'''
'''
Private Function mfobjGetSample02ReplaceWordDic() As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add "YZ02", "ToConverted01"
        
        .Add "YZ0252", "ToConverted0_toZ"
    
    End With
    
    Set mfobjGetSample02ReplaceWordDic = objDic
End Function

'''
'''
'''
Private Function mfobjGetTextsOfTest01() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add "For"
    
        .Add "For "
    
        .Add " For"
        
        .Add " For "
        
        .Add " ForEach "
        
        .Add " AnFor"
        
        .Add " @For"
    
        .Add " ""For"""
        
        .Add " -For="" "
        
        .Add "For InFor Loop After for AtFor"
        
        .Add "ForEach At 0For For2An FoR"
        
        .Add "ABC.For/Text*fOr ForEach LoopFor XTc:For"
        
        .Add " (For) ForIn <For:> XFor ForY"
    End With

    Set mfobjGetTextsOfTest01 = objCol
End Function


'**---------------------------------------------
'** sanity tests about special characters RegExp pattern strings
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToConvertTextForConvertingSpecialCharactersToEscapePatternChar()

    Dim strText As String, varText As Variant, strBeforeText As String
    
    
    For Each varText In mfobjGetTextWithSpecialCharactersTestTexts()
    
        strText = varText
    
        strBeforeText = strText
    
        msubConvertTextForConvertingSpecialCharactersToEscapePatternChar strText
    
        Debug.Print "Before : [" & strBeforeText & "], After : [" & strText & "]"
    Next
End Sub

'''
'''
'''
Private Function mfobjGetTextWithSpecialCharactersTestTexts() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add "<Text>-"
        
        .Add " (Text + 1) *"
        
        .Add " {""ext"", 'tye'} [a]"
    End With

    Set mfobjGetTextWithSpecialCharactersTestTexts = objCol
End Function


'''
'''
'''
Private Sub msubSanityTestToConvertTextForConvertingNewLineToPatternChar()

    Dim strText As String, varText As Variant
    
    For Each varText In mfobjGetNewLineTestTexts()
    
        strText = varText
    
        msubConvertTextForConvertingNewLineToPatternChar strText

        Debug.Print strText
    Next
End Sub

'''
'''
'''
Private Function mfobjGetNewLineTestTexts() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add "TNewLine1" & vbNewLine & "Text" & vbNewLine
        
        .Add "TCr2" & vbCr & "Text" & vbCr
        
        .Add "TLf3" & vbLf & "Text" & vbLf
    End With

    Set mfobjGetNewLineTestTexts = objCol
End Function



'''
'''
'''
Private Sub msubSanityTestToConvertTextForConvertingTabToPatternChar()

    Dim strText As String
    
    strText = "T1" & vbTab & "Text" & vbTab

    msubConvertTextForConvertingTabToPatternChar strText

    Debug.Print strText
End Sub

'**---------------------------------------------
'** carriage-return and line-feed character test
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToReturnCharactersRegExp()

    Dim objLfRegExp As VBScript_RegExp_55.RegExp, strText As String
    Dim objCrRegExp As VBScript_RegExp_55.RegExp
    
    
    Set objLfRegExp = New VBScript_RegExp_55.RegExp
    
    objLfRegExp.Global = True
    
    Set objCrRegExp = New VBScript_RegExp_55.RegExp
    
    objCrRegExp.Global = True
    
    
    
    strText = "TextA" & vbNewLine & "TextB" & vbNewLine & "TextC"
    
    
    objLfRegExp.Pattern = vbLf

    Debug.Print "Test case 2" & vbNewLine & objLfRegExp.Replace(strText, "ConvertedLf")

    objCrRegExp.Pattern = vbCr
    
    Debug.Print "Test case 1" & vbNewLine & Replace(strText, vbCr, "ConvertedCr")
    
    
    Debug.Print "Test case 3" & vbNewLine & objCrRegExp.Replace(objLfRegExp.Replace(strText, "ConvertedLf"), "ConvertedCr")
    
    
    objLfRegExp.Pattern = "\n"
    
    Debug.Print "Test case 4" & vbNewLine & objCrRegExp.Replace(objLfRegExp.Replace(strText, "ConvertedLf"), "ConvertedCr")
    
    objCrRegExp.Pattern = "\r"
    
    Debug.Print "Test case 5" & vbNewLine & objCrRegExp.Replace(strText, "ConvertedCr")
End Sub



'''
'''
'''
Private Sub msubSanityTestToThisSystemNewLineCharacters()

    Dim strText As String
    
    
    strText = "TextA" & vbNewLine & "TextB" & vbNewLine & "TextC"

    Debug.Print "Test case 1" & vbNewLine & Replace(strText, vbCr, "ConvertedCr")
    
    Debug.Print "Test case 2" & vbNewLine & Replace(strText, vbLf, "ConvertedLf")

    Debug.Print "Test case 3" & vbNewLine & Replace(Replace(strText, vbLf, "ConvertedLf"), vbCr, "ConvertedCr")
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
''' Clear cache
'''
Private Sub msubClearSpecialCharactersRegExp()

    Set mobjConvertTabToPatternCharRegExp = Nothing
    
    Set mobjConvertNewLineToPatternCharRegExp = Nothing
    
    Set mobjConvertSpecialCharactersToEscapePatternCharRegExp = Nothing
End Sub

'**---------------------------------------------
'** About unit word replacing for string
'**---------------------------------------------
'''
'''
'''
Private Sub msubReplaceUnitWordByRegExpWithCacheDic(ByRef rstrText As String, _
        ByRef robjReplaceWordDic As Scripting.Dictionary, _
        Optional ByVal vblnIgnoreCase As Boolean = False)


    Dim objReplacingRegExp As VBScript_RegExp_55.RegExp, objUnitWordRegExp As VBScript_RegExp_55.RegExp
    
    Dim varKey As Variant, strAfterWord As String
    Dim strBeforeWord As String, objMatch As VBScript_RegExp_55.Match, objSubMatches As VBScript_RegExp_55.SubMatches
    
    Dim objExpandedReplaceWordDic As Scripting.Dictionary
    
    
    Set objReplacingRegExp = mfobjGetInitialUnitWordRegExp(vblnIgnoreCase)

    Set objUnitWordRegExp = mfobjGetInitialUnitWordRegExp(vblnIgnoreCase)
    
    With robjReplaceWordDic
    
        For Each varKey In .Keys
        
            strBeforeWord = varKey: strAfterWord = .Item(varKey)
        
            With objUnitWordRegExp
            
                .Pattern = mfstrGetRegExpPatternOfUnitWord(strBeforeWord)
            
                If .Test(rstrText) Then
                
                    msubGetInstanceOrClearDictionary objExpandedReplaceWordDic
                
                    For Each objMatch In .Execute(rstrText)
                    
                        Set objSubMatches = objMatch.SubMatches
                    
                        Select Case True
                        
                            Case objSubMatches.Item(0) <> ""    ' perfect match
                            
                                msubRegExpReplaceWithOptionsOfPrefixMatchOrSuffixMatch rstrText, objReplacingRegExp, strBeforeWord, strAfterWord, True, True
                                
                            Case objSubMatches.Item(1) <> ""    ' string suffix match
                            
                                msubRegExpReplaceWithOptionsOfPrefixMatchOrSuffixMatch rstrText, objReplacingRegExp, strBeforeWord, strAfterWord, False, True
                            
                            Case objSubMatches.Item(2) <> ""    ' string prefix match
                            
                                msubRegExpReplaceWithOptionsOfPrefixMatchOrSuffixMatch rstrText, objReplacingRegExp, strBeforeWord, strAfterWord, True, False
                            
                            Case objSubMatches.Item(3) <> ""    ' string match among the whole text
                        
                                msubAddExpandedBeforeWordToFoundAfterWordToCacheDic objExpandedReplaceWordDic, objMatch, strAfterWord
                        End Select
                    Next
                    
                    If objExpandedReplaceWordDic.Count > 0 Then
                    
                        msubReplaceWordsByExpandedReplaceWordCacheDic rstrText, objExpandedReplaceWordDic
                    End If
                End If
            End With
        Next
    End With
End Sub

'''
'''
'''
Private Sub msubGetInstanceOrClearDictionary(ByRef robjDic As Scripting.Dictionary)

    If Not robjDic Is Nothing Then

        robjDic.RemoveAll
    Else
        Set robjDic = New Scripting.Dictionary
    End If
End Sub

'''
'''
'''
Private Sub msubReplaceWordsByExpandedReplaceWordCacheDic(ByRef rstrText As String, ByRef robjExpandedReplaceWordDic As Scripting.Dictionary)

    Dim varFoundBeforeWord As Variant

    With robjExpandedReplaceWordDic

        For Each varFoundBeforeWord In .Keys
        
            rstrText = VBA.Replace(rstrText, CStr(varFoundBeforeWord), CStr(.Item(varFoundBeforeWord)))
        Next
    End With
End Sub

'''
'''
'''
Private Sub msubAddExpandedBeforeWordToFoundAfterWordToCacheDic(ByRef robjExpandedReplaceWordDic As Scripting.Dictionary, _
        ByRef robjMatch As VBScript_RegExp_55.Match, _
        ByRef rstrAfterWord As String)

    Dim strHitText As String, strFoundBeforeWord As String, strFoundAfterWord As String

    With robjMatch
    
        strHitText = .SubMatches.Item(3)
    
        strFoundBeforeWord = .Value
    End With
    
    With robjExpandedReplaceWordDic
    
        If Not .Exists(strFoundBeforeWord) Then
    
            strFoundAfterWord = Replace(strFoundBeforeWord, strHitText, rstrAfterWord)
    
            .Add strFoundBeforeWord, strFoundAfterWord
        End If
    End With
End Sub


'''
'''
'''
Private Sub msubRegExpReplaceWithOptionsOfPrefixMatchOrSuffixMatch(ByRef rstrText As String, _
        ByRef robjRegExp As VBScript_RegExp_55.RegExp, _
        ByRef rstrBeforeWord As String, _
        ByRef rstrAfterWord As String, _
        Optional ByVal vblnUsePrefixMatch As Boolean = False, _
        Optional ByVal vblnUseSuffixMatch As Boolean = False)
        
    With robjRegExp

        If vblnUsePrefixMatch And vblnUseSuffixMatch Then
        
            .Pattern = "^" & rstrBeforeWord & "$"
        
        ElseIf vblnUsePrefixMatch And Not vblnUseSuffixMatch Then
        
            .Pattern = "^" & rstrBeforeWord
        
        ElseIf Not vblnUsePrefixMatch And vblnUseSuffixMatch Then
            
            .Pattern = rstrBeforeWord & "$"
        Else
            .Pattern = rstrBeforeWord
        End If

        If .Test(rstrText) Then

            rstrText = .Replace(rstrText, rstrAfterWord)
        End If
    End With
End Sub


'''
'''
'''
Private Sub msubReplaceSimplyByRegExp(ByRef rstrText As String, _
        ByRef robjReplaceWordDic As Scripting.Dictionary, _
        Optional ByVal vblnIgnoreCase As Boolean = False)

    Dim objRegExp As VBScript_RegExp_55.RegExp, varKey As Variant, strAfterWord As String
    
    
    Set objRegExp = mfobjGetInitialUnitWordRegExp(vblnIgnoreCase)

    With robjReplaceWordDic
    
        For Each varKey In .Keys
        
            strAfterWord = .Item(varKey)
        
            With objRegExp
            
                .Pattern = GetRegExpPatternSpecialCharactersFromSearchingWords(CStr(varKey))
        
                If .Test(rstrText) Then
        
                    rstrText = .Replace(rstrText, strAfterWord)
                End If
            End With
        Next
    End With
End Sub

'''
'''
'''
Private Function mfobjGetInitialUnitWordRegExp(Optional ByVal vblnIgnoreCase As Boolean = False) As VBScript_RegExp_55.RegExp

    Dim objRegExp As VBScript_RegExp_55.RegExp
    
    Set objRegExp = New VBScript_RegExp_55.RegExp
    
    With objRegExp
    
        .Global = True
        
        .IgnoreCase = vblnIgnoreCase
    End With
    
    Set mfobjGetInitialUnitWordRegExp = objRegExp
End Function

'''
'''
'''
Private Function mfstrGetRegExpPatternOfUnitWord(ByRef rstrUnit As String, _
        Optional ByVal vblnWithBothSideExpandedPattern As Boolean = True) As String

    Dim strPattern As String, strConvertedUnit As String
    
    
    strConvertedUnit = GetRegExpPatternSpecialCharactersFromSearchingWords(rstrUnit)
    
    strPattern = "^(" & strConvertedUnit & ")$|[" & mstrRegExpDelimiters & "]{1}(" & strConvertedUnit & ")$|^(" & strConvertedUnit & ")[" & mstrRegExpDelimiters & "]{1}"
    
    If vblnWithBothSideExpandedPattern Then
    
        strPattern = strPattern & "|[" & mstrRegExpDelimiters & "]{1}(" & strConvertedUnit & ")[" & mstrRegExpDelimiters & "]{1}"
    End If
    
    mfstrGetRegExpPatternOfUnitWord = strPattern
End Function

'''
'''
'''
Private Function mfstrGetRegExpPatternOfUnitWordWithoutBothSideExpandedPattern(ByRef rstrUnit As String) As String

    mfstrGetRegExpPatternOfUnitWordWithoutBothSideExpandedPattern = mfstrGetRegExpPatternOfUnitWord(rstrUnit, False)
End Function


'''
'''
'''
Private Function mfstrGetRegExpPatternOfExpandedUnitWord(ByRef rstrUnit As String, Optional ByVal vblnDisablingSubMatches As Boolean = False) As String

    Dim strPattern As String, strConvertedUnit As String
    
    strConvertedUnit = GetRegExpPatternSpecialCharactersFromSearchingWords(rstrUnit)
    
    If vblnDisablingSubMatches Then
    
        strPattern = "[" & mstrRegExpDelimiters & "]{1}" & strConvertedUnit & "[" & mstrRegExpDelimiters & "]{1}"
    Else
        strPattern = "([" & mstrRegExpDelimiters & "]{1})" & strConvertedUnit & "([" & mstrRegExpDelimiters & "]{1})"
    End If
    
    mfstrGetRegExpPatternOfExpandedUnitWord = strPattern
End Function





'**---------------------------------------------
'** About special characters RegExp pattern strings
'**---------------------------------------------
'''
'''
'''
Public Function GetRegExpPatternSpecialCharactersFromSearchingWords(ByRef rstrSearchingWords As String) As String

    Dim strConvertedWords As String
    
    strConvertedWords = rstrSearchingWords

    msubConvertTextForAllEscapePatternChars strConvertedWords
     
    GetRegExpPatternSpecialCharactersFromSearchingWords = strConvertedWords
End Function

'''
'''
'''
Private Sub msubConvertTextForAllEscapePatternChars(ByRef rstrText As String)

    msubConvertTextForConvertingSpecialCharactersToEscapePatternChar rstrText

    msubConvertTextForConvertingNewLineToPatternChar rstrText

    msubConvertTextForConvertingTabToPatternChar rstrText
End Sub


'''
'''
'''
Private Sub msubConvertTextForConvertingSpecialCharactersToEscapePatternChar(ByRef rstrText As String)

    With mfobjGetConvertSpecialCharactersToEscapePatternCharRegExp()
    
        rstrText = .Replace(rstrText, "\$1")
    End With
End Sub

'''
'''
'''
Private Sub msubConvertTextForConvertingNewLineToPatternChar(ByRef rstrText As String)

    With mfobjGetConvertNewLineToPatternCharRegExp()
    
        rstrText = VBA.Replace(VBA.Replace(.Replace(rstrText, "$1"), vbCr, "\r"), vbLf, "\n")
    End With
End Sub

'''
'''
'''
Private Sub msubConvertTextForConvertingTabToPatternChar(ByRef rstrText As String)

    With mfobjGetConvertTabToPatternCharRegExp()
    
        rstrText = .Replace(rstrText, mstrVbConstTabDelimiter)
    End With
End Sub



'''
'''
'''
Private Function mfobjGetConvertSpecialCharactersToEscapePatternCharRegExp() As VBScript_RegExp_55.RegExp

    If mobjConvertSpecialCharactersToEscapePatternCharRegExp Is Nothing Then
    
        Set mobjConvertSpecialCharactersToEscapePatternCharRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjConvertSpecialCharactersToEscapePatternCharRegExp
        
            .Pattern = "([" & mstrRegExpEscapeTypeDelimiters & "]{1})"
        
            .IgnoreCase = False
            
            .Global = True
        End With
    End If

    Set mfobjGetConvertSpecialCharactersToEscapePatternCharRegExp = mobjConvertSpecialCharactersToEscapePatternCharRegExp
End Function


'''
'''
'''
Private Function mfobjGetConvertNewLineToPatternCharRegExp() As VBScript_RegExp_55.RegExp

    If mobjConvertNewLineToPatternCharRegExp Is Nothing Then
    
        Set mobjConvertNewLineToPatternCharRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjConvertNewLineToPatternCharRegExp
        
            .Pattern = "([" & mstrVbConstNewLineDelimiter & "]{1})"
        
            .IgnoreCase = False
            
            .Global = True
        End With
    End If

    Set mfobjGetConvertNewLineToPatternCharRegExp = mobjConvertNewLineToPatternCharRegExp
End Function

'''
'''
'''
Private Function mfobjGetConvertTabToPatternCharRegExp() As VBScript_RegExp_55.RegExp

    If mobjConvertTabToPatternCharRegExp Is Nothing Then
    
        Set mobjConvertTabToPatternCharRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjConvertTabToPatternCharRegExp
        
            .Pattern = "[" & mstrVbConstTabDelimiter & "]{1}"
        
            .IgnoreCase = False
            
            .Global = True
        End With
    End If

    Set mfobjGetConvertTabToPatternCharRegExp = mobjConvertTabToPatternCharRegExp
End Function

