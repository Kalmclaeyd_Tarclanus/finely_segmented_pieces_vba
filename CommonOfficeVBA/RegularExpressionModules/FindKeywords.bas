Attribute VB_Name = "FindKeywords"
'
'   find keywords from a text string
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on VBScript.RegExp
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 10/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjGeneralCachePatternKeyToRegExpDic As Scripting.Dictionary

Private mobjCurrentGeneralRegExp As VBScript_RegExp_55.RegExp

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' MatchingTextBoolCondition, olm; microsoft OutLook-Mails
'''
Public Enum MatchingTextBoolCondition

    MatchingTextBoolOrOp = 0 ' boolean Or operation
    
    MatchingTextBoolAndOp = 1    ' boolean And operation
    
    MatchingTextBoolNotAllOp = 2   ' boolean No matching items operation
End Enum



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Get Variant value with both RegExp and classfying
'**---------------------------------------------
'''
'''
'''
Public Function ExtractVariantValueFromTextByRegExpPattern(ByRef rstrText As String, ByRef rstrRegExpPattern As String) As Variant

    Dim varFoundValue As Variant, objMatch As VBScript_RegExp_55.Match, strMatchedValue As String

    varFoundValue = Empty

    With GetRegExpObjectFromGeneralCacheAfterSetting(rstrRegExpPattern)
    
        If .Test(rstrText) Then
        
            Set objMatch = .Execute(rstrText).Item(0)
        
            strMatchedValue = objMatch.SubMatches.Item(0)
        
            varFoundValue = GetVariantValueFromGeneralStringInformation(strMatchedValue)
        End If
    End With

    ExtractVariantValueFromTextByRegExpPattern = varFoundValue
End Function

'**---------------------------------------------
'** Serve classifying rule enuemrations
'**---------------------------------------------
'''
'''
'''
Public Function GetMatchingTextBoolConditionTypeFromText(ByVal vstrText As String) As MatchingTextBoolCondition

    Dim enmType As MatchingTextBoolCondition
    
    enmType = MatchingTextBoolOrOp
    
    Select Case vstrText
    
        Case "OrOp"
        
            enmType = MatchingTextBoolOrOp
            
        Case "AndOp"
        
            enmType = MatchingTextBoolAndOp
        
        Case "NotAll"
    
            enmType = MatchingTextBoolNotAllOp
    
    End Select
    
    GetMatchingTextBoolConditionTypeFromText = enmType
End Function



'''
''' matching boolean operation for a finding-words collection object
'''
''' <Argument>rblnAllConditionsSatisfied: Output</Argument>
Public Sub AreAllMatchingTextConditionsSatisfied(ByRef rblnAllConditionsSatisfied As Boolean, _
        ByRef renmMatchingTextBoolCondition As MatchingTextBoolCondition, _
        ByRef rblnIsOneKeywordMatched As Boolean, _
        ByRef rintIndexOfFindingKeyWords As Long, _
        ByRef rintCountOfFindingKeyWords As Long)


    Select Case renmMatchingTextBoolCondition
            
        Case MatchingTextBoolCondition.MatchingTextBoolNotAllOp
    
            If rblnIsOneKeywordMatched Then
            
                rblnAllConditionsSatisfied = False
            End If
    
        Case MatchingTextBoolCondition.MatchingTextBoolOrOp
        
            If rblnIsOneKeywordMatched Then
            
                ' rblnAllConditionsSatisfied Is True
            Else
                If rintIndexOfFindingKeyWords = rintCountOfFindingKeyWords Then
                
                    rblnAllConditionsSatisfied = False
                End If
            End If
            
        
        Case MatchingTextBoolCondition.MatchingTextBoolAndOp
    
            If Not rblnIsOneKeywordMatched Then
            
                rblnAllConditionsSatisfied = False
            End If
        
        Case Else
        
            If rblnIsOneKeywordMatched Then
            
                ' blnAllConditionsSatisfied Is True
            Else
                If rintIndexOfFindingKeyWords = rintCountOfFindingKeyWords Then
                
                    rblnAllConditionsSatisfied = False
                End If
            End If
    End Select
End Sub



'**---------------------------------------------
'** searching tools
'**---------------------------------------------
'''
''' basic searching
'''
Public Function IsOneKeywordMatchedAboutATextString(ByRef rstrText As String, _
        ByRef rstrFindingKeyWord As String, _
        ByRef rblnNeedToUseLowerCase As String, _
        ByRef rblnNeedToExactMatch As String) As Boolean

    Dim blnIsOneKeywordMatched As Boolean

    blnIsOneKeywordMatched = False
    
    
    If rblnNeedToUseLowerCase Then
                    
        If rblnNeedToExactMatch Then
    
            If StrComp(LCase(rstrText), rstrFindingKeyWord) = 0 Then
            
                blnIsOneKeywordMatched = True
            End If
        Else
            If InStr(1, LCase(rstrText), rstrFindingKeyWord) > 0 Then
            
                blnIsOneKeywordMatched = True
            End If
        End If
    Else
        If rblnNeedToExactMatch Then
        
            If StrComp(rstrText, rstrFindingKeyWord) = 0 Then
            
                blnIsOneKeywordMatched = True
            End If
        Else
            If InStr(1, rstrText, rstrFindingKeyWord) > 0 Then
            
                blnIsOneKeywordMatched = True
            End If
        End If
    End If

    IsOneKeywordMatchedAboutATextString = blnIsOneKeywordMatched
End Function

'**---------------------------------------------
'** searching with RegExp
'**---------------------------------------------
'''
'''
'''
Public Sub ClearFindWordsRegExpGeneralCache()

    If Not mobjGeneralCachePatternKeyToRegExpDic Is Nothing Then
    
        mobjGeneralCachePatternKeyToRegExpDic.RemoveAll
    End If
    
    Set mobjGeneralCachePatternKeyToRegExpDic = Nothing
End Sub

'''
''' RegExp search
'''
Public Function IsRegExpPatternMatchedAboutATextString(ByRef rstrText As String, _
        ByRef rstrRegExpPattern As String, _
        Optional ByVal vblnIgnoreCase As Boolean = False, _
        Optional ByVal vblnGlobal As Boolean = False) As Boolean
        
    Dim blnIsAtLeastOneMatched As Boolean
    
    blnIsAtLeastOneMatched = False
    
    With mfobjGetRegExpFromFindWordsRegExpGeneralCache(rstrRegExpPattern, vblnIgnoreCase, vblnGlobal)
    
        blnIsAtLeastOneMatched = .Test(rstrText)
    End With
        
    IsRegExpPatternMatchedAboutATextString = blnIsAtLeastOneMatched
End Function

'''
''' get RegExp from cache
'''
Public Function GetRegExpObjectFromGeneralCacheAfterSetting(ByRef rstrRegExpPattern As String, _
        Optional ByVal vblnIgnoreCase As Boolean = False, _
        Optional ByVal vblnGlobal As Boolean = False) As VBScript_RegExp_55.RegExp

    Set GetRegExpObjectFromGeneralCacheAfterSetting = mfobjGetRegExpFromFindWordsRegExpGeneralCache(rstrRegExpPattern, vblnIgnoreCase, vblnGlobal)
End Function

'''
'''
'''
Private Function mfobjGetRegExpFromFindWordsRegExpGeneralCache(ByRef rstrRegExpPattern As String, _
        Optional ByVal vblnIgnoreCase As Boolean = False, _
        Optional ByVal vblnGlobal As Boolean = False) As VBScript_RegExp_55.RegExp


    Dim objRegExp As VBScript_RegExp_55.RegExp, strKey As String


    strKey = rstrRegExpPattern & CStr(CInt(vblnIgnoreCase)) & CStr(CInt(vblnGlobal))

    If mfblnIsCurrentGeneralRegExpKeyEqualsToNeededKey(strKey) Then

        Set objRegExp = mobjCurrentGeneralRegExp
        
        'Debug.Print "Pattern string and conditions are same as previous item"
        
    Else
        If mobjGeneralCachePatternKeyToRegExpDic Is Nothing Then Set mobjGeneralCachePatternKeyToRegExpDic = New Scripting.Dictionary
    
        With mobjGeneralCachePatternKeyToRegExpDic
        
            If .Exists(strKey) Then
            
                Set objRegExp = .Item(strKey)
                
                'Debug.Print "RegExp cache are used"
            Else
                Set objRegExp = New VBScript_RegExp_55.RegExp
                
                With objRegExp
                
                    .IgnoreCase = vblnIgnoreCase
                    
                    .Global = vblnGlobal
                    
                    .Pattern = rstrRegExpPattern
                End With
                
                .Add strKey, objRegExp
            End If
        
            Set mobjCurrentGeneralRegExp = objRegExp
        End With
    End If

    Set mfobjGetRegExpFromFindWordsRegExpGeneralCache = objRegExp
End Function

'''
'''
'''
Private Function mfblnIsCurrentGeneralRegExpKeyEqualsToNeededKey(ByRef rstrNeededKey As String) As Boolean

    Dim strCacheRegExpKey As String, blnIsEqual As Boolean
    
    If mobjCurrentGeneralRegExp Is Nothing Then
    
        blnIsEqual = False
    Else
        msubGetKeyOfGeneralCacheFromRegExp strCacheRegExpKey, mobjCurrentGeneralRegExp
    
        blnIsEqual = (StrComp(strCacheRegExpKey, rstrNeededKey) = 0)
    End If
    
    mfblnIsCurrentGeneralRegExpKeyEqualsToNeededKey = blnIsEqual
End Function


'''
'''
'''
Private Sub msubGetKeyOfGeneralCacheFromRegExp(ByRef rstrOutputKey As String, ByRef robjRegExp As VBScript_RegExp_55.RegExp)

    With robjRegExp
    
        rstrOutputKey = .Pattern & CStr(CInt(.IgnoreCase)) & CStr(CInt(.Global))
    End With
End Sub


'**---------------------------------------------
'** counting characters tools
'**---------------------------------------------
'''
'''
'''
Public Function CountOfCellLineFeedChar(ByRef rstrText As String) As Long

    Const vstrCellLineFeedChar As String = vbLf ' Line-feed character of each Excel sheet cell string

    CountOfCellLineFeedChar = CountOfSpecifiedChar(rstrText, vstrCellLineFeedChar)
End Function


'''
'''
'''
Public Function CountOfSpecifiedChar(ByRef rstrText As String, ByRef rstrSpecifiedChar As String) As Long

    Dim intCount As Long, intPointOfChar As Long
    
    intCount = 0
    
    intPointOfChar = InStr(1, rstrText, rstrSpecifiedChar)
    
    If intPointOfChar > 0 Then
    
        Do
            intCount = intCount + 1
    
            intPointOfChar = InStr(intPointOfChar + 1, rstrText, rstrSpecifiedChar)
    
        Loop While intPointOfChar > 0
    End If
    
    CountOfSpecifiedChar = intCount
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToIsRegExpPatternMatchedAboutATextString()

    Dim strText As String, strPattern As String
    
    ' Whether the cache RegExp objects are used properly or not.
    
    
    ClearFindWordsRegExpGeneralCache
    
    strText = "Text-matching"

    strPattern = "Text\-matching"


    Debug.Print IsRegExpPatternMatchedAboutATextString(strText, strPattern)
    
    strPattern = "Text-matching"
    
    Debug.Print IsRegExpPatternMatchedAboutATextString(strText, strPattern)

    strText = "T-matching"

    strPattern = "Text-matching"

    Debug.Print IsRegExpPatternMatchedAboutATextString(strText, strPattern)
    
    
    strText = "T-matching"
    
    strPattern = "Text\-matching"

    Debug.Print IsRegExpPatternMatchedAboutATextString(strText, strPattern)

End Sub


