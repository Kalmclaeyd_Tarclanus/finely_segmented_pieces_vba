Attribute VB_Name = "FindURLAndEmailAddresses"
'
'   get hyperlinks of the body text of a Outlook.MailItem using RegExp objects
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on VBScript.RegExp
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  9/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Contants
'///////////////////////////////////////////////
Private Const mstrJapaneseCharPattern As String = "ぁ-んァ-ヶｱ-ﾝﾞﾟ一-龠・ー"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjURLRegExp As VBScript_RegExp_55.RegExp

Private mobjURLTagRegExp As VBScript_RegExp_55.RegExp

Private mobjEMailAddressRegExp As VBScript_RegExp_55.RegExp

Private mobjUNCPathRegExp As VBScript_RegExp_55.RegExp


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetEMailAddressRegExp()

    Dim strText As String, objMatch As VBScript_RegExp_55.Match
    
    
    ClearRegExpsOfHyperlinks
    
    strText = "abcde@fghklmn.jp"

    With GetEMailAddressRegExp()
    
        If .Test(strText) Then
        
            For Each objMatch In .Execute(strText)
            
                Debug.Print objMatch.Value
            Next
        End If
    End With
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetUNCPathRegExp()

    Dim strText As String, objMatch As VBScript_RegExp_55.Match
    
    ClearRegExpsOfHyperlinks
    
    'strText = "\\192.168.11.1\test_directory"
    
    'strText = "c:\"
    
    strText = "c:\日本語フォルダ"

    With GetUNCPathRegExp()
    
        If .Test(strText) Then
        
            For Each objMatch In .Execute(strText)
            
                Debug.Print objMatch.Value
            Next
        End If
    End With
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** getting URL or e-mail adresses from texts by RegExp
'**---------------------------------------------
'''
'''
'''
Public Function GetHyperLinksColFromText(ByRef rstrText As String) As Collection

    Dim objHyperLinkDic As Scripting.Dictionary, objMatch As VBScript_RegExp_55.Match, strHyperLink As String
    Dim objCol As Collection
    

    Set objHyperLinkDic = New Scripting.Dictionary

    With GetURLRegExp
    
        If .Test(rstrText) Then
        
            For Each objMatch In .Execute(rstrText)
        
                strHyperLink = objMatch.Value
              
                With objHyperLinkDic
                
                    If Not .Exists(strHyperLink) Then
                    
                        .Add strHyperLink, 0
                    End If
                End With
            Next
            
            SortDictionaryAboutKeys objHyperLinkDic
            
            Set objCol = GetEnumeratorKeysColFromDic(objHyperLinkDic)
        Else
            Set objCol = New Collection
        End If
    End With

    Set GetHyperLinksColFromText = objCol
End Function


'''
'''
'''
Public Function GetEmailAddressesColFromText(ByRef rstrText As String) As Collection

    Dim objEMailAddressesDic As Scripting.Dictionary, objMatch As VBScript_RegExp_55.Match
    Dim strAddress As String, objCol As Collection


    Set objEMailAddressesDic = New Scripting.Dictionary

    With GetEMailAddressRegExp()
    
        If .Test(rstrText) Then
        
            For Each objMatch In .Execute(rstrText)
        
                strAddress = objMatch.Value
              
                With objEMailAddressesDic
                
                    If Not .Exists(strAddress) Then
                    
                        .Add strAddress, 0
                    End If
                End With
            Next
            
            SortDictionaryAboutKeys objEMailAddressesDic
            
            Set objCol = GetEnumeratorKeysColFromDic(objEMailAddressesDic)
        Else
            Set objCol = New Collection
        End If
    End With

    Set GetEmailAddressesColFromText = objCol
End Function

'''
'''
'''
Public Function GetUNCPathsColFromText(ByRef rstrText As String) As Collection

    Dim objUNCPathsDic As Scripting.Dictionary, objMatch As VBScript_RegExp_55.Match
    Dim strUNCPath As String, objCol As Collection


    Set objUNCPathsDic = New Scripting.Dictionary

    With GetUNCPathRegExp()
    
        If .Test(rstrText) Then
        
            For Each objMatch In .Execute(rstrText)
        
                strUNCPath = objMatch.Value
              
                With objUNCPathsDic
                
                    If Not .Exists(strUNCPath) Then
                    
                        .Add strUNCPath, 0
                    End If
                End With
            Next
            
            SortDictionaryAboutKeys objUNCPathsDic
            
            Set objCol = GetEnumeratorKeysColFromDic(objUNCPathsDic)
        Else
            Set objCol = New Collection
        End If
    End With

    Set GetUNCPathsColFromText = objCol
End Function


'**---------------------------------------------
'** getting URL and e-mail RegExp
'**---------------------------------------------
'''
'''
'''
Public Function GetURLRegExp() As VBScript_RegExp_55.RegExp

    If mobjURLRegExp Is Nothing Then
    
        Set mobjURLRegExp = New VBScript_RegExp_55.RegExp
    
        With mobjURLRegExp
        
            .Pattern = "http(|s)\:\/\/[@0-9a-zA-Z\.\?\-\/\=]{1,}"
        
            .Global = True
        End With
    End If

    Set GetURLRegExp = mobjURLRegExp
End Function

'''
'''
'''
Public Function GetEMailAddressRegExp() As VBScript_RegExp_55.RegExp

    If mobjEMailAddressRegExp Is Nothing Then
    
        Set mobjEMailAddressRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjEMailAddressRegExp
        
            '.Pattern = "[0-9a-zA-Z_\.\-]{1,}\@"
            
            .Pattern = "[0-9a-zA-Z_\.\-]{1,}\@[0-9a-zA-Z_\-]{1,}\.[0-9a-zA-Z_\.\-]{1,}"
    
            .Global = True
        End With
    End If

    Set GetEMailAddressRegExp = mobjEMailAddressRegExp
End Function

'''
'''
'''
Public Function GetUNCPathRegExp() As VBScript_RegExp_55.RegExp

    If mobjUNCPathRegExp Is Nothing Then
    
        Set mobjUNCPathRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjUNCPathRegExp
        
            .Pattern = "(\\\\([0-9]{1,}\.[0-9]{1,}\.[0-9]{1,}\.[0-9]{1,}|[a-zA-Z0-9_\-]{1,})|[a-zA-Z]{1}\:)\\[a-zA-Z0-9_\-" & mstrJapaneseCharPattern & "]{0,}(|\\)[\\a-zA-Z0-9_\-" & mstrJapaneseCharPattern & "]{0,}"
            
            .Global = True
        End With
    End If

    Set GetUNCPathRegExp = mobjUNCPathRegExp
End Function

'''
'''
'''
Public Sub ClearRegExpsOfHyperlinks()

    Set mobjURLRegExp = Nothing
    
    Set mobjURLTagRegExp = Nothing

    Set mobjEMailAddressRegExp = Nothing
    
    Set mobjUNCPathRegExp = Nothing
End Sub


