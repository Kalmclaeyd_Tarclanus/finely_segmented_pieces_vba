Attribute VB_Name = "DataTableCompressing"
'
'   remove all-null column from any data-table Collection
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 20/Jul/2024    Kalmclaeyd Tarclanus    Separated from ADOExSheetOut.bas
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetNullColumnsDelimitedCommaFromSqlResultDataTableCol(ByVal robjDTCol As Collection) As String

    Dim strNullColumnsDelimitedComma As String, objNullColumnsDic As Scripting.Dictionary
    Dim varValuesCol As Variant, objValuesCol As Collection
    Dim i As Long, varItem As Variant, varIndex As Variant
    
    Set objNullColumnsDic = New Scripting.Dictionary
    
    For Each varValuesCol In robjDTCol
    
        Set objValuesCol = varValuesCol
    
        i = 1
        
        For Each varItem In objValuesCol
        
            objNullColumnsDic.Add i, Null
        
            i = i + 1
        Next
        
        Exit For
    Next
    
    
    For Each varValuesCol In robjDTCol
    
        Set objValuesCol = varValuesCol
        
        i = 1
        
        For Each varItem In objValuesCol
    
            If objNullColumnsDic.Exists(i) Then
    
                Select Case True
                
                    Case IsNull(varItem), IsEmpty(varItem)
                    
                        ' Nothing to do
                        
                    Case TypeName(varItem) = "String"
                
                        If varItem <> "" Then
                        
                            objNullColumnsDic.Remove i
                        End If
                    Case Else
                    
                        objNullColumnsDic.Remove i
                End Select
            End If
    
            If objNullColumnsDic.Count = 0 Then
            
                Exit For
            End If
    
            i = i + 1
        Next
    Next

    If objNullColumnsDic.Count > 0 Then
    
        For Each varIndex In objNullColumnsDic.Keys
        
            If strNullColumnsDelimitedComma <> "" Then
            
                strNullColumnsDelimitedComma = strNullColumnsDelimitedComma & ","
            End If
        
            strNullColumnsDelimitedComma = strNullColumnsDelimitedComma & CStr(varIndex)
        Next
    Else
        strNullColumnsDelimitedComma = ""
    End If

    GetNullColumnsDelimitedCommaFromSqlResultDataTableCol = strNullColumnsDelimitedComma
End Function


