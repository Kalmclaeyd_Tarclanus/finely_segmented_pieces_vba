Attribute VB_Name = "RemoveAllVBACodes"
'
'   remove this VBA project all source codes without this
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Use some duplicated codes from outside VBA code modules for keeping this module independency.
'
'   Dependency Abstract:
'       Dependent on VBIDE
'       This is an independent code from all other modules
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 26/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'       Mon, 13/Feb/2023    Kalmclaeyd Tarclanus    Supported the caption localization for both English and Japanese.
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = False   ' If you want to the code-snipet support, you need to change HAS_REF into True

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrModuleName As String = "RemoveAllVBACodes"   ' 1st part of registry sub-key

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Key-Value cache preparation for RemoveAllVBACodes
'**---------------------------------------------
Private mobjStrKeyValueRemoveAllVBACodesDic As Object

Private mblnIsStrKeyValueRemoveAllVBACodesDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' For example, you can remove all PowerPoint VBA codes.
''' But, this prevents deleting this current VBA project codes from some users wrong operation
'''
''' Note: Outlook has no VBIDE.VBProject interface, so the method Import and Export and Remove cannot be used in Outlook.
'''
Public Sub RemoveAllVBACodesWhenThisIsNotActiveApplication(ByVal vobjOfficeFile As Object)

    Dim strFilePath As String
    
    Select Case TypeName(vobjOfficeFile)
    
        Case "Workbook", "Document", "Presentation"
            
            If Not mfobjGetCurrentFile() Is vobjOfficeFile Then
            
                msubRemoveAllVBACodesWithoutThisModule vobjOfficeFile
                
            End If
            
        Case "DataBase"
        
            ' The VBProject of the Access data-base is not supported to operate
    End Select
End Sub


'''
''' Attention! use duplicated codes for keeping this module independency
'''
Private Function mfobjGetCurrentFile() As Object

    Dim objOfficeObject As Object
    
    Select Case LCase(Trim(Replace(Application.Name, "Microsoft", "")))
    
        Case "excel"

            On Error Resume Next
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisWorkbook", VbGet) ' Application.ThisWorkbook
            
            On Error GoTo 0
            
            If objOfficeObject Is Nothing Then
            
                Set objOfficeObject = VBA.CallByName(Application, "ActiveWorkbook", VbGet)  ' Excel.Workbook
            End If
            
        Case "word"

            On Error Resume Next
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisDocument", VbGet)
            
            On Error GoTo 0
            
            If objOfficeObject Is Nothing Then
            
                Set objOfficeObject = VBA.CallByName(Application, "ActiveDocument", VbGet)  ' Word.Document
            End If
            
        Case "powerpoint"

            Set objOfficeObject = VBA.CallByName(Application, "ActivePresentation", VbGet)  ' PowerPoint.Presentation
    End Select
    
    Set mfobjGetCurrentFile = objOfficeObject
End Function



'///////////////////////////////////////////////
'/// Internal Functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubRemoveAllVBACodesWithoutThisModule(ByVal vobjOfficeFile As Object)

    Dim objRemovedFilesCol As Collection, strToRemoveFileName As String, strLogPath As String
    
#If HAS_REF Then
    Dim objVBComponent As VBIDE.VBComponent, objVBProject As VBIDE.VBProject

    Set objVBProject = vobjOfficeFile.VBProject
#Else
    Dim objVBComponent As Object, objVBProject As Object
    
    Set objVBProject = vobjOfficeFile.VBProject
#End If

    If Not objVBProject Is Nothing Then
    
        With objVBProject
    
            For Each objVBComponent In .VBComponents
            
                If objVBComponent.Type <> vbext_ct_Document Then
                
                    strToRemoveFileName = objVBComponent.Name & mfstrGetExtensionFromComponentType(objVBComponent.Type)
                
                    .VBComponents.Remove objVBComponent
                    
                    If objRemovedFilesCol Is Nothing Then Set objRemovedFilesCol = New Collection
                    
                    objRemovedFilesCol.Add strToRemoveFileName
                End If
            Next
        
        End With
    End If
    
    
    If Not objRemovedFilesCol Is Nothing Then
    
        msubWriteLogAboutRemovedFiles vobjOfficeFile, objRemovedFilesCol
        
        strLogPath = vobjOfficeFile.Path & "\RomovedVBACodesLog.txt"
        
        msubOpenLogFileByNotepad strLogPath
    End If
End Sub


'''
''' Attention! use duplicated codes for keeping this module independency
'''
Private Sub msubOpenLogFileByNotepad(ByVal vstrLogFilePath As String, Optional ByVal vblnAllowToOpenDirectoryByExplorer As Boolean = True)

    Dim strNotepadPath As String, strCmd As String

#If HAS_REF Then
    Dim objWshWhell As IWshRuntimeLibrary.WshShell, objShell As Shell32.Shell
    
    Set objWshWhell = New IWshRuntimeLibrary.WshShell
#Else
    Dim objWshWhell As Object, objShell As Object

    Set objWshWhell = CreateObject("Wscript.Shell")
#End If

    strNotepadPath = "%SystemRoot%\System32\notepad.exe"

    strCmd = strNotepadPath & " """ & vstrLogFilePath & """"
                
    With objWshWhell

        .Run strCmd, 1
    End With
    
    If vblnAllowToOpenDirectoryByExplorer Then
    
#If HAS_REF Then

        Set objShell = New Shell32.Shell
#Else
        Set objShell = CreateObject("Shell.Application")
#End If
    
        With objShell  ' At VBScript, then With CreateObject("Shell.Application")
        
            .Explore CreateObject("Scripting.FileSystemObject").GetParentFolderName(vstrLogFilePath)
        End With
    End If
End Sub


'''
'''
'''
Private Sub msubWriteLogAboutRemovedFiles(ByVal vobjOfficeFile As Object, ByVal vobjRemovedFilesCol As Collection)
    
    Dim strLogPath As String

#If HAS_REF Then
    Dim objFS As Scripting.FileSystemObject
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objFS As Object

    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If

    strLogPath = vobjOfficeFile.Path & "\RomovedVBACodesLog.txt"

    With objFS
        With .CreateTextFile(strLogPath, True)
    
            .WriteLine mfstrGetLogOfRemovingVBACodes(vobjOfficeFile, vobjRemovedFilesCol)
    
            .Close
        End With
    End With
End Sub


'''
'''
'''

Private Function mfstrGetLogOfRemovingVBACodes(ByVal vobjOfficeFile As Object, ByVal vobjRemovedFilesCol As Collection) As String
    
    Dim strLog As String, varFileName As Variant
    
    
    strLog = GetTextOfStrKeyRemoveAllVbaCodesLogRemovingFunctionModuleFileName() & ": " & mstrModuleName & vbNewLine & vbNewLine
    
    strLog = strLog & GetTextOfStrKeyRemoveAllVbaCodesLogProcessedDateTime() & ": " & Format(Now(), "yyyy/mm/dd hh:mm:ss") & vbNewLine
    
    With CreateObject("WScript.Network")
        
        strLog = strLog & GetTextOfStrKeyRemoveAllVbaCodesLogCurrentUserName() & ": " & .UserName & vbNewLine
        strLog = strLog & GetTextOfStrKeyRemoveAllVbaCodesLogCurrentComputerName() & ": " & .ComputerName & vbNewLine
    
    End With
    
    strLog = strLog & vbNewLine
    
    strLog = strLog & GetTextOfStrKeyRemoveAllVbaCodesLogCountOfRemovedFiles() & ": " & CStr(vobjRemovedFilesCol.Count) & vbNewLine
    
    strLog = strLog & vbNewLine
    
    strLog = strLog & GetTextOfStrKeyRemoveAllVbaCodesLogRemovedList() & ":" & vbNewLine
    
    For Each varFileName In vobjRemovedFilesCol
    
        strLog = strLog & vbTab & CStr(varFileName) & vbNewLine
    Next
        

    mfstrGetLogOfRemovingVBACodes = strLog
End Function


'''
''' get extension name
''' <Duplicated function for keeping this module independency>
'''
#If HAS_REF Then
Private Function mfstrGetExtensionFromComponentType(ByVal venmComponentType As VBIDE.vbext_ComponentType) As String
#Else
Private Function mfstrGetExtensionFromComponentType(ByVal venmComponentType As Variant) As String
#End If
    Dim strExtension As String

    strExtension = ""

    Select Case venmComponentType
    
        Case vbext_ct_StdModule     ' 1
        
            strExtension = ".bas"
        Case vbext_ct_ClassModule   ' 2
        
            strExtension = ".cls"
        Case vbext_ct_MSForm        ' 3
        
            strExtension = ".frm"
        Case vbext_ct_ActiveXDesigner   ' 11
        
            strExtension = ".cls"
        Case vbext_ct_Document      ' 100
        
            strExtension = ".cls"   ' Worksheet object class module, Workbook object class module
    End Select

    mfstrGetExtensionFromComponentType = strExtension
End Function



'**---------------------------------------------
'** Key-Value cache preparation for RemoveAllVBACodes
'**---------------------------------------------
'''
''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME
'''
''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyRemoveAllVbaCodesLogRemovingFunctionModuleFileName() As String

    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then

        msubInitializeTextForRemoveAllVBACodes
    End If

    GetTextOfStrKeyRemoveAllVbaCodesLogRemovingFunctionModuleFileName = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME")
End Function

'''
''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME
'''
''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyRemoveAllVbaCodesLogProcessedDateTime() As String

    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then

        msubInitializeTextForRemoveAllVBACodes
    End If

    GetTextOfStrKeyRemoveAllVbaCodesLogProcessedDateTime = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME")
End Function

'''
''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_COUNT_OF_REMOVED_FILES
'''
''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyRemoveAllVbaCodesLogCountOfRemovedFiles() As String

    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then

        msubInitializeTextForRemoveAllVBACodes
    End If

    GetTextOfStrKeyRemoveAllVbaCodesLogCountOfRemovedFiles = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_COUNT_OF_REMOVED_FILES")
End Function

'''
''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVED_LIST
'''
''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyRemoveAllVbaCodesLogRemovedList() As String

    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then

        msubInitializeTextForRemoveAllVBACodes
    End If

    GetTextOfStrKeyRemoveAllVbaCodesLogRemovedList = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVED_LIST")
End Function

'''
''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_USER_NAME
'''
''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyRemoveAllVbaCodesLogCurrentUserName() As String

    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then

        msubInitializeTextForRemoveAllVBACodes
    End If

    GetTextOfStrKeyRemoveAllVbaCodesLogCurrentUserName = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_USER_NAME")
End Function

'''
''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME
'''
''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyRemoveAllVbaCodesLogCurrentComputerName() As String

    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then

        msubInitializeTextForRemoveAllVBACodes
    End If

    GetTextOfStrKeyRemoveAllVbaCodesLogCurrentComputerName = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME")
End Function

'''
''' Initialize Key-Values which values. Avoiding to use LocalizationADOConnector instance.
'''
''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
Private Sub msubInitializeTextForRemoveAllVBACodes()

    Dim objKeyToTextDic As Object ' Scripting.Dictionary

    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then

        Set objKeyToTextDic = CreateObject("Scripting.Dictionary") ' Scripting.Dictionary

        Select Case Application.LanguageSettings.LanguageID(msoLanguageIDUI)

            Case 1041 ' Japanese environment

                AddStringKeyValueForRemoveAllVBACodesByJapaneseValues objKeyToTextDic
            Case Else

                AddStringKeyValueForRemoveAllVBACodesByEnglishValues objKeyToTextDic
        End Select
        mblnIsStrKeyValueRemoveAllVBACodesDicInitialized = True
    End If

    Set mobjStrKeyValueRemoveAllVBACodesDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for RemoveAllVBACodes key-values cache
'''
''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
Private Sub AddStringKeyValueForRemoveAllVBACodesByEnglishValues(ByRef robjDic As Object)

    With robjDic

        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME", "This Removing Function Module File Name"
        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "Processed date-time"
        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_COUNT_OF_REMOVED_FILES", "Count of removed files"
        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVED_LIST", "Removed module list"
        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "Current user name"
        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "Current computer name"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for RemoveAllVBACodes key-values cache
'''
''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
Private Sub AddStringKeyValueForRemoveAllVBACodesByJapaneseValues(ByRef robjDic As Object)

    With robjDic

        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME", "VBA削除機能のモジュール名"
        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "削除日時"
        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_COUNT_OF_REMOVED_FILES", "削除したモジュール数"
        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVED_LIST", "削除したモジュールリスト"
        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "ユーザー名"
        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "コンピューター名"
    End With
End Sub

'''
''' Remove Keys for RemoveAllVBACodes key-values cache
'''
''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueRemoveAllVBACodes(ByRef robjDic As Object)

    With robjDic

        If .Exists("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME"
            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME"
            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_COUNT_OF_REMOVED_FILES"
            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVED_LIST"
            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_USER_NAME"
            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME"
        End If
    End With
End Sub



