Attribute VB_Name = "ImportAllVBACodes"
'
'   read this VBA project all source codes from the specified directory
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Use some duplicated codes from outside VBA code modules for keeping this module independency.
'
'   Dependency Abstract:
'       Dependent on VBIDE
'       This is an independent code from all other modules
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 23/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'       Mon, 13/Feb/2023    Kalmclaeyd Tarclanus    Supported the caption localization for both English and Japanese.
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = False   ' If you get the code-snipet support, you need to change HAS_REF to True


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrModuleName As String = "ImportAllVBACodes"   ' 1st part of registry sub-key

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Key-Value cache preparation for ImportAllVBACodes
'**---------------------------------------------
Private mobjStrKeyValueImportAllVBACodesDic As Object

Private mblnIsStrKeyValueImportAllVBACodesDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToLoadAllVBACodesFromDirectoryWithUserSelected()

    Dim objApplicationObjects As Object

    Select Case Trim(Replace(LCase(Application.Name), "microsoft", ""))

        Case "excel"
    
            Dim objBook As Object
            
            Set objApplicationObjects = VBA.CallByName(Application, "Workbooks", VbGet)
            
            Set objBook = VBA.CallByName(objApplicationObjects, "Add", VbMethod)
            
            LoadAllVBACodesFromDirectoryWithUserSelected objBook, True
        
        Case "word"
        
            Dim objDocument As Object
            
            Set objApplicationObjects = VBA.CallByName(Application, "Documents", VbGet)
            
            Set objDocument = VBA.CallByName(objApplicationObjects, "Add", VbMethod)
            
            LoadAllVBACodesFromDirectoryWithUserSelected objDocument, True
        
        Case "powerpoint"
        
            Dim objPresentation As Object
            
            Set objApplicationObjects = VBA.CallByName(Application, "Presentations", VbGet)
            
            Set objPresentation = VBA.CallByName(objApplicationObjects, "Add", VbMethod)
            
            LoadAllVBACodesFromDirectoryWithUserSelected objPresentation, True
    End Select
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub LoadAllVBACodesFromDirectory(ByVal vstrVBACodesDirectoryPath As String, ByVal vobjOfficeFile As Object, Optional ByVal vblnForceToImport As Boolean = False, Optional ByVal vblnAllowToImportFromSubDirectory As Boolean = True, Optional ByVal vblnAllowToCreateLog As Boolean = True)

    Dim intImportedCount As Long, objImportedFilesCol As Collection
    
#If HAS_REF Then
    Dim objFS As Scripting.FileSystemObject
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objFS As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If
    
    
    msubImportAllModulesFromDirectory intImportedCount, objImportedFilesCol, vstrVBACodesDirectoryPath, vobjOfficeFile.VBProject, objFS, vblnForceToImport
    
    msubCheckImportedResultAndLogging intImportedCount, objImportedFilesCol, vstrVBACodesDirectoryPath, vobjOfficeFile, vblnAllowToCreateLog
End Sub


'''
''' using folder-dialog, load all VBA source files
'''
Public Sub LoadAllVBACodesFromDirectoryWithUserSelected(ByVal vobjOfficeFile As Object, Optional ByVal vblnForceToImport As Boolean = False)

    Dim strDirPath As String

    With Application.FileDialog(msoFileDialogFolderPicker)
    
        .Title = GetTextOfStrKeyImportAllVbaCodesSelectDirectoryThatIncludesVbaSource()
        
        .AllowMultiSelect = False
    
        If .Show = -1 Then
        
            strDirPath = .SelectedItems(1)
        
            LoadAllVBACodesFromDirectory strDirPath, vobjOfficeFile, vblnForceToImport
        Else
        
            Debug.Print GetTextOfStrKeyImportAllVbaCodesCanceledSelectingDirectory() & "..."
        End If
    End With
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
''' This is a recursive call function.
'''
#If HAS_REF Then
Private Sub msubImportAllModulesFromDirectory(ByRef rintImportedCount As Long, ByRef robjImportedFilesCol As Collection, ByVal vstrVBACodesDirectoryPath As String, ByVal vobjVBProject As VBIDE.VBProject, ByVal vobjFS As Scripting.FileSystemObject, Optional ByVal vblnForceToImport As Boolean = False, Optional ByVal vblnAllowToImportFromSubDirectory As Boolean = True)
#Else
Private Sub msubImportAllModulesFromDirectory(ByRef rintImportedCount As Long, ByRef robjImportedFilesCol As Collection, ByVal vstrVBACodesDirectoryPath As String, ByVal vobjVBProject As Object, ByVal vobjFS As Object, Optional ByVal vblnForceToImport As Boolean = False, Optional ByVal vblnAllowToImportFromSubDirectory As Boolean = True)
#End If
    Dim strFileName As String, strSourcePath As String, strSourceFileBaseName As String

#If HAS_REF Then
    Dim objDir As Scripting.Folder, objFile As Scripting.File
    Dim objSubDir As Scripting.Folder
#Else
    Dim objDir As Object, objFile As Object
    Dim objComponent As Object, objSubDir As Object
#End If
    
    With vobjFS
    
        Set objDir = .GetFolder(vstrVBACodesDirectoryPath)

        If Not objDir Is Nothing Then
        
            For Each objFile In objDir.Files
            
                Select Case LCase(.GetExtensionName(objFile.Name))
                    Case "bas", "cls", "frm"
                
                        rintImportedCount = rintImportedCount + 1
                
                        strSourcePath = objFile.Path
                
                        msubImportVBACodeWithLogging robjImportedFilesCol, vobjVBProject, vobjFS, objFile.Path, vblnForceToImport
                End Select
            Next
        
            If vblnAllowToImportFromSubDirectory Then
                
                For Each objSubDir In objDir.SubFolders
                    
                    ' recursive call
                    msubImportAllModulesFromDirectory rintImportedCount, robjImportedFilesCol, objSubDir.Path, vobjVBProject, vobjFS, vblnForceToImport, vblnAllowToImportFromSubDirectory
                Next
            End If
        End If
    End With
End Sub


'''
'''
'''
#If HAS_REF Then
Private Sub msubImportVBACodeWithLogging(ByRef robjImportedFilesCol As Collection, ByVal vobjVBProject As VBIDE.VBProject, ByVal vobjFS As Scripting.FileSystemObject, ByRef rstrSourcePath As String, ByVal vblnForceToImport As Boolean)

    Dim objComponent As VBIDE.VBComponent
#Else
Private Sub msubImportVBACodeWithLogging(ByRef robjImportedFilesCol As Collection, ByVal vobjVBProject As Object, ByVal vobjFS As Object, ByRef rstrSourcePath As String, ByVal vblnForceToImport As Boolean)

    Dim objComponent As Object
#End If

    With vobjVBProject.VBComponents
                
        On Error Resume Next

        Set objComponent = Nothing

        Set objComponent = .Item(vobjFS.GetBaseName(rstrSourcePath))

        On Error GoTo 0
        
        If objComponent Is Nothing Then
        
            .Import rstrSourcePath
            
            If robjImportedFilesCol Is Nothing Then Set robjImportedFilesCol = New Collection
            
            robjImportedFilesCol.Add vobjFS.GetFileName(rstrSourcePath)
        Else
            Debug.Print vobjVBProject.Name & " has already have " & vobjFS.GetBaseName(rstrSourcePath)
        
            If vblnForceToImport Then
            
                .Remove objComponent
                
                Set objComponent = Nothing
                
                .Import rstrSourcePath
                
                If robjImportedFilesCol Is Nothing Then Set robjImportedFilesCol = New Collection
            
                robjImportedFilesCol.Add vobjFS.GetFileName(rstrSourcePath)
            End If
        End If
    End With
End Sub



'''
'''
'''
Private Sub msubCheckImportedResultAndLogging(ByVal vintImportedCount As Long, _
        ByVal vobjImportedFilesCol As Collection, _
        ByVal vstrVBACodesDirectoryPath As String, _
        ByVal vobjOfficeFile As Object, _
        Optional ByVal vblnAllowToCreateLog As Boolean = True)

    Dim strMsgBoxTitle As String
    
#If HAS_REF Then
    Dim objFS As Scripting.FileSystemObject
#Else
    Dim objFS As Object
#End If

    If vintImportedCount = 0 Then

        Set objFS = CreateObject("Scripting.FileSystemObject")
        
        strMsgBoxTitle = GetTextOfStrKeyImportAllVbaCodesImportSourceFilesInto() & " " & vobjOfficeFile.Name

        MsgBox "Path: " & vstrVBACodesDirectoryPath & vbNewLine & GetTextOfStrKeyImportAllVbaCodesNoVbaSourceCode(), vbInformation + vbOKOnly, strMsgBoxTitle
    Else
        If vobjImportedFilesCol Is Nothing Then
        
            Set objFS = CreateObject("Scripting.FileSystemObject")
        
            strMsgBoxTitle = GetTextOfStrKeyImportAllVbaCodesImportSourceFilesInto() & " " & vobjOfficeFile.Name
            
            MsgBox "Path: " & vstrVBACodesDirectoryPath & vbNewLine & GetTextOfStrKeyImportAllVbaCodesFailedToImportBecauseExisted(), vbExclamation + vbOKOnly, strMsgBoxTitle
        Else
            If vblnAllowToCreateLog Then
            
                msubWriteOfficeFileBuiltInPropertyLog vstrVBACodesDirectoryPath, vobjOfficeFile, vobjImportedFilesCol
            End If
        End If
    End If
End Sub


'''
'''
'''
Private Sub msubWriteOfficeFileBuiltInPropertyLog(ByVal vstrDirectoryPath As String, _
        ByVal vobjOfficeFile As Object, _
        ByVal vobjImportedFilesCol As Collection)

#If HAS_REF Then
    Dim objNetwork As WshNetwork
#Else
    Dim objNetwork As Object
#End If
    
    With vobjOfficeFile.BuiltinDocumentProperties
    
        .Item("Subject").Value = "Automatic VBA Importing"
                    
        Set objNetwork = CreateObject("WScript.Network")
    
        .Item("Author").Value = "Auto-imported by " & objNetwork.UserName
    
        .Item("Comments").Value = mfstrGetLogOfImportingVBACodes(vstrDirectoryPath, vobjOfficeFile, vobjImportedFilesCol)
        
'        .Item("Company").Value = Empty  ' Company
'        .Item("Manager").Value = Empty  ' Manager
    End With
End Sub


'''
'''
'''
Private Function mfstrGetLogOfImportingVBACodes(ByVal vstrVBACodesDirectoryPath As String, _
        ByVal vobjOfficeFile As Object, _
        ByVal vobjImportedFilesCol As Collection) As String

    Dim strLog As String, varFileName As Variant
    
    
    strLog = GetTextOfStrKeyImportAllVbaCodesLogImportingFunctionModuleFileName() & ": " & mstrModuleName & vbNewLine & vbNewLine
    
    'strLog = strLog & "Office file name: " & vobjOfficeFile.Name & vbNewLine & vbNewLine
    
    strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogProcessedDateTime() & ": " & Format(Now(), "yyyy/mm/dd hh:mm:ss") & vbNewLine
    
    With CreateObject("WScript.Network")
        
        strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogCurrentUserName() & ": " & .UserName & vbNewLine
        
        strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogCurrentComputerName() & ": " & .ComputerName & vbNewLine
    End With
    
    strLog = strLog & vbNewLine
    
    strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogImportingSourceDirectory() & ": " & vstrVBACodesDirectoryPath & vbNewLine
    
    strLog = strLog & vbNewLine
    
    strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogCountOfImportedFiles() & ": " & CStr(vobjImportedFilesCol.Count) & vbNewLine
    
    strLog = strLog & vbNewLine
    
    strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogImportedList() & ":" & vbNewLine
    
    For Each varFileName In vobjImportedFilesCol
    
        strLog = strLog & vbTab & CStr(varFileName) & vbNewLine
    Next
        
    mfstrGetLogOfImportingVBACodes = strLog
End Function

'**---------------------------------------------
'** Key-Value cache preparation for ImportAllVBACodes
'**---------------------------------------------
'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesSelectDirectoryThatIncludesVbaSource() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesSelectDirectoryThatIncludesVbaSource = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE")
End Function

'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_CANCELED_SELECTING_DIRECTORY
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesCanceledSelectingDirectory() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesCanceledSelectingDirectory = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_CANCELED_SELECTING_DIRECTORY")
End Function

'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_IMPORT_SOURCE_FILES_INTO
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesImportSourceFilesInto() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesImportSourceFilesInto = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_IMPORT_SOURCE_FILES_INTO")
End Function

'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesNoVbaSourceCode() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesNoVbaSourceCode = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE")
End Function

'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_FAILED_TO_IMPORT_BECAUSE_EXISTED
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesFailedToImportBecauseExisted() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesFailedToImportBecauseExisted = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_FAILED_TO_IMPORT_BECAUSE_EXISTED")
End Function

'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_FUNCTION_MODULE_FILE_NAME
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesLogImportingFunctionModuleFileName() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesLogImportingFunctionModuleFileName = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_FUNCTION_MODULE_FILE_NAME")
End Function

'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesLogProcessedDateTime() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesLogProcessedDateTime = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME")
End Function

'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_SOURCE_DIRECTORY
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesLogImportingSourceDirectory() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesLogImportingSourceDirectory = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_SOURCE_DIRECTORY")
End Function


'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_COUNT_OF_IMPORTED_FILES
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesLogCountOfImportedFiles() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesLogCountOfImportedFiles = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_COUNT_OF_IMPORTED_FILES")
End Function

'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTED_LIST
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesLogImportedList() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesLogImportedList = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTED_LIST")
End Function

'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesLogCurrentUserName() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesLogCurrentUserName = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME")
End Function

'''
''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyImportAllVbaCodesLogCurrentComputerName() As String

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        msubInitializeTextForImportAllVBACodes
    End If

    GetTextOfStrKeyImportAllVbaCodesLogCurrentComputerName = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME")
End Function

'''
''' Initialize Key-Values which values. Avoiding to use LocalizationADOConnector instance.
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Sub msubInitializeTextForImportAllVBACodes()

    Dim objKeyToTextDic As Object ' Scripting.Dictionary

    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then

        Set objKeyToTextDic = CreateObject("Scripting.Dictionary") ' Scripting.Dictionary

        Select Case Application.LanguageSettings.LanguageID(msoLanguageIDUI)

            Case 1041 ' Japanese environment

                AddStringKeyValueForImportAllVBACodesByJapaneseValues objKeyToTextDic
            Case Else

                AddStringKeyValueForImportAllVBACodesByEnglishValues objKeyToTextDic
        End Select
        mblnIsStrKeyValueImportAllVBACodesDicInitialized = True
    End If

    Set mobjStrKeyValueImportAllVBACodesDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for ImportAllVBACodes key-values cache
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Sub AddStringKeyValueForImportAllVBACodesByEnglishValues(ByRef robjDic As Object)

    With robjDic

        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE", "Select a directory which includes VBA source files"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_CANCELED_SELECTING_DIRECTORY", "Canceled selecting directory"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_IMPORT_SOURCE_FILES_INTO", "Import source files into"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE", "No VBA source codes in specified directory"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_FAILED_TO_IMPORT_BECAUSE_EXISTED", "Failed to import, because the VBA souce module files have already existed in the file"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_FUNCTION_MODULE_FILE_NAME", "This Importing Function Module File Name"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "Processed date-time"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_SOURCE_DIRECTORY", "Importing source directory path"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_COUNT_OF_IMPORTED_FILES", "Count of imported VBA module files"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTED_LIST", "Imported List"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "Current user name"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "Current computer name"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for ImportAllVBACodes key-values cache
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Sub AddStringKeyValueForImportAllVBACodesByJapaneseValues(ByRef robjDic As Object)

    With robjDic

        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE", "VBAコードが含まれるディレクトリを選択してください"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_CANCELED_SELECTING_DIRECTORY", "ディレクトリの選択はキャンセルされました"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_IMPORT_SOURCE_FILES_INTO", "VBAソースファイルの読み込み -"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE", "読込対象のモジュール(ソースコード)がありません"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_FAILED_TO_IMPORT_BECAUSE_EXISTED", "既にモジュール(ソースコード)ファイルがVBProjectに含まれていたため、読み込みされませんでした。"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_FUNCTION_MODULE_FILE_NAME", "読み込み機能のモジュール名"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "読み込み日時"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_SOURCE_DIRECTORY", "読み込み元ディレクトリパス"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_COUNT_OF_IMPORTED_FILES", "読み込んだファイル数"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTED_LIST", "読み込んだリスト"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "ユーザー名"
        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "コンピューター名"
    End With
End Sub

'''
''' Remove Keys for ImportAllVBACodes key-values cache
'''
''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueImportAllVBACodes(ByRef robjDic As Object)

    With robjDic

        If .Exists("STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE"
            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_CANCELED_SELECTING_DIRECTORY"
            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_IMPORT_SOURCE_FILES_INTO"
            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE"
            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_FAILED_TO_IMPORT_BECAUSE_EXISTED"
            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_FUNCTION_MODULE_FILE_NAME"
            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME"
            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_SOURCE_DIRECTORY"
            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_COUNT_OF_IMPORTED_FILES"
            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTED_LIST"
            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME"
            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME"
        End If
    End With
End Sub


