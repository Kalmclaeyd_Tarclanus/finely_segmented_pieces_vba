Attribute VB_Name = "ExportAllVBACodes"
'
'   save this VBA project all source codes into the specified directory
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       This keeps the independency from the other VBProject codes.
'       Since, use some duplicated codes for keeping this module independency.
'
'   Dependency Abstract:
'       Dependent on VBIDE, Scripting.Dictionary
'       This is an independent code from all other modules
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 23/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'       Mon, 13/Feb/2023    Kalmclaeyd Tarclanus    Supported the caption localization for both English and Japanese.
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////

#Const HAS_REF = False   ' If you want to get the code-snipet support, you need to change HAS_REF into True


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrModuleName As String = "ExportAllVBACodes"   ' 1st part of registry sub-key

Private Const mstrLogFileBaseName As String = "OutputVBACodesLog"


Private Const mstrCommonVBAGenerationSubDirectoryName As String = "GenerateVBACodes"

Private Const mstrTmpSubDirName As String = "TemporaryStoredCodes"

Private Const mstrCommonTemporarySourceExportedDirectoryName As String = "TemporaryExportedCodes"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Key-Value cache preparation for ExportAllVBACodes
'**---------------------------------------------
Private mobjStrKeyValueExportAllVBACodesDic As Object

Private mblnIsStrKeyValueExportAllVBACodesDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' local dpendent sanity test
'''
Private Sub msubSanityTestToSaveCodesOfOpenedListViewTest()

    SaveCodesOfOpenedOfficeFile "ListViewTest"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' Attention! use duplicated codes for keeping this module independency
'''
Public Sub SaveCodesOfSelectedOfficeFile()

    Dim strOfficeFilePath As String, strOutputDir As String

#If HAS_REF Then
    Dim objVBProject As VBIDE.VBProject
#Else
    Dim objVBProject As Object
#End If

    strOfficeFilePath = GetMacroEnableOfficeFilePathFromFileDialog()

    On Error Resume Next

    Set objVBProject = GetObject(strOfficeFilePath).VBProject

    On Error GoTo 0

    If Not objVBProject Is Nothing Then
        'Debug.Print objVBProject.Name
     
        strOutputDir = GetOutputCodesDirPath(objVBProject)
    
        SaveAllVBAProjectCodesWithoutObjectModule strOutputDir, objVBProject, True
    End If
End Sub


'''
'''
'''
Public Sub SaveCodesOfOpenedOfficeFile(ByVal vstrVBProjectName As String)

#If HAS_REF Then
    Dim objVBProject As VBIDE.VBProject
#Else
    Dim objVBProject As Object
#End If

    Dim strOutputDir As String
    
    Set objVBProject = FindVBProjectFromVBE(vstrVBProjectName)
    
    If Not objVBProject Is Nothing Then
    
        strOutputDir = GetOutputCodesDirPath(objVBProject)
    
        SaveAllVBAProjectCodesWithoutObjectModule strOutputDir, objVBProject, True
    End If
End Sub

'''
'''
'''
#If HAS_REF Then
Private Function FindVBProjectFromVBE(ByVal vstrVBProjectName As String) As VBIDE.VBProject

    Dim objVBProject As VBIDE.VBProject, objFoundVBProject As VBIDE.VBProject
#Else
Private Function FindVBProjectFromVBE(ByVal vstrVBProjectName As String) As Object

    Dim objVBProject As Object, objFoundVBProject As Object

#End If
    Set objFoundVBProject = Nothing
    
    For Each objVBProject In Application.VBE.VBProjects
    
        If StrComp(objVBProject.Name, vstrVBProjectName) = 0 Then
        
            Set objFoundVBProject = objVBProject
            
            Exit For
        End If
    Next

    Set FindVBProjectFromVBE = objFoundVBProject
End Function


'''
'''
'''
#If HAS_REF Then

Private Function GetOutputCodesDirPath(ByVal vobjVBProject As VBIDE.VBProject) As String

#Else
Private Function GetOutputCodesDirPath(ByVal vobjVBProject As Object) As String
#End If

    Dim strDir As String
    
    With New Scripting.FileSystemObject
    
        strDir = mfstrGetCurrentOfficeFileDirectoryPath() & "\GenerateVBACodes"

        If Not .FolderExists(strDir) Then
        
            .CreateFolder strDir
        End If
        
        strDir = strDir & "\Exported" & vobjVBProject.Name
        
        If Not .FolderExists(strDir) Then
        
            .CreateFolder strDir
        End If
    End With
    
    GetOutputCodesDirPath = strDir
End Function




'''
''' Attention! use duplicated codes for keeping this module independency
'''
Private Function GetMacroEnableOfficeFilePathFromFileDialog() As String

    Dim strPath As String
    
    strPath = ""

    With Application.FileDialog(msoFileDialogFilePicker)
    
        With .Filters
        
            .Clear
            
            .Add GetTextOfStrKeyExportAllVbaCodesFileOfExcelWordPowerpoint(), "*.xlsm;*.xls;*.docm;*.doc;*.pptm;*.ppt"
        End With
    
        .InitialFileName = mfstrGetCurrentOfficeFileDirectoryPath()
    
        .Title = GetTextOfStrKeyExportAllVbaCodesSelectOfficeFileThatIncludesVba()
    
        .AllowMultiSelect = False
    
        If .Show() Then
        
            strPath = .SelectedItems(1)
        End If
    End With

    GetMacroEnableOfficeFilePathFromFileDialog = strPath
End Function


'''
''' Attention! use duplicated codes for keeping this module independency
'''
Private Function mfstrGetCurrentOfficeFileDirectoryPath() As String

    mfstrGetCurrentOfficeFileDirectoryPath = mfobjGetCurrentOfficeFile().Path
End Function

'''
''' Attention! use duplicated codes for keeping this module independency
'''
Private Function mfobjGetCurrentOfficeFile() As Object

    Dim objOfficeObject As Object
    
    Set objOfficeObject = Nothing

    Select Case Trim(Replace(LCase(Application.Name), "microsoft", ""))
    
        Case "excel"
        
            On Error Resume Next
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisWorkbook", VbGet) ' Application.ThisWorkbook
            
            On Error GoTo 0
            
            If objOfficeObject Is Nothing Then
            
                Set objOfficeObject = VBA.CallByName(Application, "ActiveWorkbook", VbGet)  ' Excel.Workbook
            End If
            
        Case "word"
        
            On Error Resume Next
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisDocument", VbGet)
            
            On Error GoTo 0
            
            If objOfficeObject Is Nothing Then
            
                Set objOfficeObject = VBA.CallByName(Application, "ActiveDocument", VbGet)  ' Word.Document
            End If
            
        Case "powerpoint"
    
            Set objOfficeObject = VBA.CallByName(Application, "ActivePresentation", VbGet)  ' PowerPoint.Presentation
    End Select

    Set mfobjGetCurrentOfficeFile = objOfficeObject
End Function

'''
'''
'''
#If HAS_REF Then

Public Sub SaveAllVBAProjectCodesWithoutObjectModule(ByVal vstrDirectoryPath As String, _
        ByVal vobjVBProject As VBIDE.VBProject, _
        Optional ByVal vblnForceToExport As Boolean = False, _
        Optional ByVal vblnAllowToCreateLog As Boolean = True, _
        Optional ByVal vstrLogOutputDirectoryPath As String = "")

#Else

Public Sub SaveAllVBAProjectCodesWithoutObjectModule(ByVal vstrDirectoryPath As String, _
        ByVal vobjVBProject As Object, Optional ByVal vblnForceToExport As Boolean = False, _
        Optional ByVal vblnAllowToCreateLog As Boolean = True, _
        Optional ByVal vstrLogOutputDirectoryPath As String = "")
#End If

    Dim objExportedFilesCol As Collection, intExportedCount As Long
    
    msubExportAllModules intExportedCount, _
            objExportedFilesCol, _
            vstrDirectoryPath, _
            vobjVBProject, _
            vblnForceToExport
    
    msubCheckExportedResultAndLogging intExportedCount, _
            objExportedFilesCol, _
            vstrDirectoryPath, _
            vobjVBProject, _
            vblnAllowToCreateLog, _
            vstrLogOutputDirectoryPath
End Sub

'''
'''
'''
#If HAS_REF Then

Public Sub SaveAllVBAProjectCodes(ByVal vstrDirectoryPath As String, _
        ByVal vobjVBProject As VBIDE.VBProject, _
        Optional ByVal vblnForceToExport As Boolean = False, _
        Optional ByVal vblnAllowToCreateLog As Boolean = True, _
        Optional ByVal vstrLogOutputDirectoryPath As String = "")

#Else

Public Sub SaveAllVBAProjectCodes(ByVal vstrDirectoryPath As String, _
        ByVal vobjVBProject As Object, _
        Optional ByVal vblnForceToExport As Boolean = False, _
        Optional ByVal vblnAllowToCreateLog As Boolean = True, _
        Optional ByVal vstrLogOutputDirectoryPath As String = "")

#End If
    
    Dim objExportedFilesCol As Collection, intExportedCount As Long
    
    msubExportAllModules intExportedCount, _
            objExportedFilesCol, _
            vstrDirectoryPath, _
            vobjVBProject, _
            vblnForceToExport, _
            True
    
    msubCheckExportedResultAndLogging intExportedCount, _
            objExportedFilesCol, _
            vstrDirectoryPath, _
            vobjVBProject, _
            vblnAllowToCreateLog, _
            vstrLogOutputDirectoryPath
End Sub


#If HAS_REF Then

    Public Sub UpdateAllVBAProjectCodes(ByVal vstrDirectoryPath As String, _
            ByVal vobjVBProject As VBIDE.VBProject, _
            Optional ByVal vstrTemporaryOutputDirectoryPath As String = "", _
            Optional ByVal vblnForceToExport As Boolean = False, _
            Optional ByVal vblnAllowToCreateLog As Boolean = True, _
            Optional ByVal vstrLogOutputDirectoryPath As String = "")
    
    
        msubUpdateAllVBAProjectCodesWithVariousOptions vstrDirectoryPath, _
                vobjVBProject, _
                vstrTemporaryOutputDirectoryPath, _
                vblnForceToExport, _
                vblnAllowToCreateLog, _
                vstrLogOutputDirectoryPath, _
                True
    End Sub
    
    Public Sub UpdateAllVBAProjectCodesWithoutObjectModule(ByVal vstrDirectoryPath As String, _
            ByVal vobjVBProject As VBIDE.VBProject, _
            Optional ByVal vstrTemporaryOutputDirectoryPath As String = "", _
            Optional ByVal vblnForceToExport As Boolean = False, _
            Optional ByVal vblnAllowToCreateLog As Boolean = True, _
            Optional ByVal vstrLogOutputDirectoryPath As String = "")
    
    
        msubUpdateAllVBAProjectCodesWithVariousOptions _
                vstrDirectoryPath, _
                vobjVBProject, _
                vstrTemporaryOutputDirectoryPath, _
                vblnForceToExport, _
                vblnAllowToCreateLog, _
                vstrLogOutputDirectoryPath, False
    End Sub
#Else

    Public Sub UpdateAllVBAProjectCodes(ByVal vstrDirectoryPath As String, _
            ByVal vobjVBProject As Object, _
            Optional ByVal vstrTemporaryOutputDirectoryPath As String = "", _
            Optional ByVal vblnForceToExport As Boolean = False, _
            Optional ByVal vblnAllowToCreateLog As Boolean = True, _
            Optional ByVal vstrLogOutputDirectoryPath As String = "")
    
    
        msubUpdateAllVBAProjectCodesWithVariousOptions vstrDirectoryPath, _
                vobjVBProject, _
                vstrTemporaryOutputDirectoryPath, _
                vblnForceToExport, _
                vblnAllowToCreateLog, _
                vstrLogOutputDirectoryPath, _
                True
    End Sub
    
    Public Sub UpdateAllVBAProjectCodesWithoutObjectModule(ByVal vstrDirectoryPath As String, _
            ByVal vobjVBProject As Object, _
            Optional ByVal vstrTemporaryOutputDirectoryPath As String = "", _
            Optional ByVal vblnForceToExport As Boolean = False, _
            Optional ByVal vblnAllowToCreateLog As Boolean = True, _
            Optional ByVal vstrLogOutputDirectoryPath As String = "")
    
    
        msubUpdateAllVBAProjectCodesWithVariousOptions vstrDirectoryPath, _
                vobjVBProject, _
                vstrTemporaryOutputDirectoryPath, _
                vblnForceToExport, _
                vblnAllowToCreateLog, _
                vstrLogOutputDirectoryPath, _
                False
    End Sub
#End If

'''
'''
'''
#If HAS_REF Then

Private Sub msubUpdateAllVBAProjectCodesWithVariousOptions(ByVal vstrDirectoryPath As String, _
        ByVal vobjVBProject As VBIDE.VBProject, _
        Optional ByVal vstrTemporaryOutputDirectoryPath As String = "", _
        Optional ByVal vblnForceToExport As Boolean = False, _
        Optional ByVal vblnAllowToCreateLog As Boolean = True, _
        Optional ByVal vstrLogOutputDirectoryPath As String = "", _
        Optional ByVal vblnIncludesAlsoObjectModuleCodes As Boolean = False, _
        Optional ByVal vblnAllowToDeleteVbaSourceFilesIfTheLatestVBProjectDoesntHaveIts As Boolean = True)

    Dim objFS As Scripting.FileSystemObject
    
    Dim objExistedFileNameToPathDic As Scripting.Dictionary, objTmpFileNameToPathDic As Scripting.Dictionary
    
    Set objFS = New Scripting.FileSystemObject
#Else

Private Sub msubUpdateAllVBAProjectCodesWithVariousOptions(ByVal vstrDirectoryPath As String, _
        ByVal vobjVBProject As Object, _
        Optional ByVal vstrTemporaryOutputDirectoryPath As String = "", _
        Optional ByVal vblnForceToExport As Boolean = False, _
        Optional ByVal vblnAllowToCreateLog As Boolean = True, _
        Optional ByVal vstrLogOutputDirectoryPath As String = "", _
        Optional ByVal vblnIncludesAlsoObjectModuleCodes As Boolean = False, _
        Optional ByVal vblnAllowToDeleteVbaSourceFilesIfTheLatestVBProjectDoesntHaveIts As Boolean = True)
    
    
    Dim objFS As Object

    Dim objExistedFileNameToPathDic As Object, objTmpFileNameToPathDic As Object

    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If
    
    
    Dim strTmpOutputDir As String, strSrcPath As String, strDstPath As String
    
    Dim intExportedCount As Long, objExportedFilesCol As VBA.Collection
    
    Dim varFileName As Variant
    
    
    If vstrTemporaryOutputDirectoryPath <> "" Then
    
        strTmpOutputDir = vstrTemporaryOutputDirectoryPath
    Else
        strTmpOutputDir = GetTemporaryStoredCodeDirectoryPath()
    End If
    
    If Not objFS.FolderExists(strTmpOutputDir) Then

        mfintForceToCreateDirectory strTmpOutputDir, objFS
    End If
    
    ClearAllFilesBeforeSaveAllVBAProjectCodes strTmpOutputDir

    msubExportAllModules intExportedCount, _
            objExportedFilesCol, _
            strTmpOutputDir, _
            vobjVBProject, _
            vblnForceToExport, _
            vblnIncludesAlsoObjectModuleCodes


    Set objTmpFileNameToPathDic = mfobjGetFileNameToFilePathDicAboutVbaSources(strTmpOutputDir, objFS)

    If vblnAllowToDeleteVbaSourceFilesIfTheLatestVBProjectDoesntHaveIts Then
    
        DeleteFilesInADirectoryForNotIncludedFileNamesDic vstrDirectoryPath, objTmpFileNameToPathDic, objFS
    End If


    Set objExistedFileNameToPathDic = mfobjGetFileNameToFilePathDicAboutVbaSources(vstrDirectoryPath, objFS)

    With objTmpFileNameToPathDic
    
        For Each varFileName In .Keys
        
            strSrcPath = .Item(varFileName)
        
            If objExistedFileNameToPathDic.Exists(varFileName) Then
            
                strDstPath = objExistedFileNameToPathDic.Item(varFileName)
                
                If mfblnDoTwoSourcesHaveSomeDifferences(strSrcPath, strDstPath, objFS) Then
                
                    VBA.FileCopy strSrcPath, strDstPath
                End If
            Else
                strDstPath = vstrDirectoryPath & "\" & varFileName
            
                VBA.FileCopy strSrcPath, strDstPath
            End If
        Next
    End With
End Sub

'''
''' Attention! use duplicated codes for keeping this module independency
'''
#If HAS_REF Then

Private Function mfblnDoTwoSourcesHaveSomeDifferences(ByRef rstrSrcPath As String, ByRef rstrDstPath As String, ByRef robjFS As Scripting.FileSystemObject) As Boolean

#Else
Private Function mfblnDoTwoSourcesHaveSomeDifferences(ByRef rstrSrcPath As String, ByRef rstrDstPath As String, ByRef robjFS As Object) As Boolean

#End If
    Dim strSrcAllText As String, strDstAllText As String

    With robjFS
    
        With .OpenTextFile(rstrSrcPath, Scripting.IOMode.ForReading)
        
            strSrcAllText = .ReadAll
        
            .Close
        End With
    
        With .OpenTextFile(rstrDstPath, Scripting.IOMode.ForReading)
        
            strDstAllText = .ReadAll
        
            .Close
        End With
    End With

    If InStr(strSrcAllText, strDstAllText) = 0 Or InStr(strDstAllText, strSrcAllText) = 0 Then
    
        mfblnDoTwoSourcesHaveSomeDifferences = True
    Else
        mfblnDoTwoSourcesHaveSomeDifferences = False
    End If
End Function


'''
'''
'''
#If HAS_REF Then

Private Function mfobjGetFileNameToFilePathDicAboutVbaSources(ByVal vstrDir As String, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, objFile As Scripting.File

    Set objDic = New Scripting.Dictionary
#Else
Private Function mfobjGetFileNameToFilePathDicAboutVbaSources(ByVal vstrDir As String, Optional ByVal vobjFS As Object = Nothing) As Object

    Dim objDic As Object, objFile As Object
    
    Set objDic = CreateObject("Scripting.Dictionary")
#End If

    With mfobjGetFileSystemObjectFromOptional(vobjFS)
    
        For Each objFile In .GetFolder(vstrDir).Files

            Select Case LCase(.GetExtensionName(objFile.Path))

                Case "bas", "cls", "frm"

                    With objFile
                        
                        objDic.Add .Name, .Path
                    End With
            End Select
        Next
    End With

    Set mfobjGetFileNameToFilePathDicAboutVbaSources = objDic
End Function

'''
'''
'''
#If HAS_REF Then

Public Sub DeleteFilesInADirectoryForNotIncludedFileNamesDic(ByRef rstrDir As String, ByRef robjFileNameToPathDic As Scripting.Dictionary, Optional ByVal vobjFS As Scripting.FileSystemObject)

    Dim objFile As Scripting.File, strExtension As String, strFrxFilePath As String
#Else
Public Sub DeleteFilesInADirectoryForNotIncludedFileNamesDic(ByRef rstrDir As String, ByRef robjFileNameToPathDic As Object, Optional ByVal vobjFS As Object)

    Dim objFile As Object, strExtension As String, strFrxFilePath As String
    
#End If

    With mfobjGetFileSystemObjectFromOptional(vobjFS)

        For Each objFile In .GetFolder(rstrDir).Files
        
            strExtension = LCase(.GetExtensionName(objFile.Path))
        
            Select Case strExtension
            
                Case "bas", "cls", "frm"
        
                    If Not robjFileNameToPathDic.Exists(objFile.Name) Then
                    
                        If StrComp(strExtension, "frm") = 0 Then
                        
                            strFrxFilePath = .GetParentFolderName(objFile.Path) & "\" & .GetBaseName(objFile.Path) & ".frx"
                        
                            If .FileExists(strFrxFilePath) Then
                            
                                .DeleteFile strFrxFilePath
                            End If
                        End If
                        
                        objFile.Delete
                    End If
            End Select
        Next
    End With
End Sub



'''
''' Attention! use duplicated codes for keeping this module independency
'''
Public Sub ClearAllFilesBeforeSaveAllVBAProjectCodes(ByVal vstrDirectoryPath As String)


#If HAS_REF Then
    Dim objDir As Scripting.Folder, objFile As Scripting.File, objFS As Scripting.FileSystemObject
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objDir As Object, objFile As Object, objFS As Object

    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If

    With objFS
    
        If .FolderExists(vstrDirectoryPath) Then
        
            Set objDir = .GetFolder(vstrDirectoryPath)
        
            For Each objFile In objDir.Files
            
                Select Case LCase(.GetExtensionName(objFile.Name))
                    
                    Case "bas", "cls", "frm", "frx"
            
                        .DeleteFile objFile.Path, True
                        
                    Case "txt"
                    
                        If StrComp(LCase(objFile.Name), LCase(mstrLogFileBaseName) & ".txt") = 0 Then
                        
                            .DeleteFile objFile.Path, True
                        End If
                End Select
            Next
        End If
    End With
End Sub

'''
''' Attention! use duplicated codes for keeping this module independency
'''
#If HAS_REF Then

    Private Function mfobjGetFileSystemObjectFromOptional(Optional ByVal vobjFS As Scripting.FileSystemObject) As Scripting.FileSystemObject
    
        If Not vobjFS Is Nothing Then
        
            Set mfobjGetFileSystemObjectFromOptional = vobjFS
        Else
            Set mfobjGetFileSystemObjectFromOptional = New Scripting.FileSystemObject
        End If
    End Function
#Else
    Private Function mfobjGetFileSystemObjectFromOptional(Optional ByVal vobjFS As Object) As Object
    
        If Not vobjFS Is Nothing Then
        
            Set mfobjGetFileSystemObjectFromOptional = vobjFS
        Else
            Set mfobjGetFileSystemObjectFromOptional = CreateObject("Scripting.FileSystemObject")
        End If
    End Function
#End If

'///////////////////////////////////////////////
'/// Internal Functions
'///////////////////////////////////////////////
'''
''' Attention! use duplicated codes for keeping this module independency
'''
#If HAS_REF Then

Public Sub msubExportVBComponentWithCreatingDirectory(ByRef robjVBComponent As VBIDE.VBComponent, _
        ByRef rstrOutputFilePath As String, _
        ByRef robjFS As Scripting.FileSystemObject)

#Else

Public Sub msubExportVBComponentWithCreatingDirectory(ByRef robjVBComponent As Object, _
        ByRef rstrOutputFilePath As String, _
        ByRef robjFS As Object)
#End If
    Dim strTmpDir As String

    strTmpDir = robjFS.GetParentFolderName(rstrOutputFilePath)
    
    mfintForceToCreateDirectory strTmpDir
    
    robjVBComponent.Export rstrOutputFilePath
End Sub


'''
''' Attention! use duplicated codes for keeping this module independency
'''
''' vstrDirPath has to be a directory path, This uses FSO
'''
#If HAS_REF Then

Private Function mfintForceToCreateDirectory(ByVal vstrDirPath As String, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing) As Long
#Else
Private Function mfintForceToCreateDirectory(ByVal vstrDirPath As String, Optional ByVal vobjFS As Object = Nothing) As Long
#End If

    Dim intRet As Long, intP1 As Long, strDrive As String, strTargetDirPath As String
    
    intRet = 0
    
    On Error GoTo ErrHandler
    
    With mfobjGetFileSystemObjectFromOptional(vobjFS)
    
        If .FolderExists(vstrDirPath) Then
        
            intRet = 0
            
            mfintForceToCreateDirectory = intRet
            
            Exit Function
        End If

        strDrive = .GetDriveName(vstrDirPath)
        
        If Not .DriveExists(strDrive) Then
        
            intRet = -1
            
            mfintForceToCreateDirectory = intRet
            
            Exit Function
        End If
        
        intP1 = InStr(Len(strDrive) + 2, vstrDirPath, "\")
        
        If intP1 > 0 Then
        
            Do
                strTargetDirPath = Left(vstrDirPath, intP1 - 1)
                
                If Not .FolderExists(strTargetDirPath) Then
                
                    .CreateFolder strTargetDirPath
                End If
                
                intP1 = InStr(intP1 + 1, vstrDirPath, "\")
                            
            Loop While intP1 > 0
        
            If Not .FolderExists(vstrDirPath) Then
            
                .CreateFolder vstrDirPath
            End If
        Else
            .CreateFolder vstrDirPath
        End If
    End With

    mfintForceToCreateDirectory = intRet
    
    Exit Function
    
ErrHandler:

    Debug.Print Err.Description
    
    mfintForceToCreateDirectory = 2
End Function
'''
''' Attention! use duplicated codes for keeping this module independency
'''
Private Function GetTemporaryStoredCodeDirectoryPath() As String

    Dim objOfficeFile As Object, strTmpDir As String
    
    Set objOfficeFile = mfobjGetCurrentOfficeFile()
    
    strTmpDir = objOfficeFile.Path & "\" & mstrCommonVBAGenerationSubDirectoryName

#If HAS_REF Then

    With New Scripting.FileSystemObject
#Else
    With CreateObject("Scripting.FileSystemObject")
#End If
        If Not .FolderExists(strTmpDir) Then
        
            .CreateFolder strTmpDir
        End If
        
        strTmpDir = strTmpDir & "\" & mstrTmpSubDirName
        
        If Not .FolderExists(strTmpDir) Then
        
            .CreateFolder strTmpDir
        End If
    End With

    GetTemporaryStoredCodeDirectoryPath = strTmpDir
End Function

'''
''' Attention! use duplicated codes for keeping this module independency
'''
''' <Argument>rintExportedCount: Output</Argument>
''' <Argument>robjExportedFilesCol: Output</Argument>
''' <Argument>vstrDirectoryPath: Input - ouput-directory-path</Argument>
''' <Argument>vobjVBProject: Input</Argument>
''' <Argument>vblnForceToExport: Input</Argument>
''' <Argument>vblnIncludesAlsoObjectModuleCodes: Input</Argument>
#If HAS_REF Then

Private Sub msubExportAllModules(ByRef rintExportedCount As Long, _
        ByRef robjExportedFilesCol As Collection, _
        ByVal vstrDirectoryPath As String, _
        ByVal vobjVBProject As VBIDE.VBProject, _
        Optional ByVal vblnForceToExport As Boolean = False, _
        Optional ByVal vblnIncludesAlsoObjectModuleCodes As Boolean = False)


    Dim objDir As Scripting.Folder, objFile As Scripting.File
    Dim objComponent As VBIDE.VBComponent, objFS As Scripting.FileSystemObject
    
    Set objFS = New Scripting.FileSystemObject
#Else
    
Private Sub msubExportAllModules(ByRef rintExportedCount As Long, _
        ByRef robjExportedFilesCol As Collection, _
        ByVal vstrDirectoryPath As String, _
        ByVal vobjVBProject As Object, _
        Optional ByVal vblnForceToExport As Boolean = False, _
        Optional ByVal vblnIncludesAlsoObjectModuleCodes As Boolean = False)
    
    
    Dim objDir As Object, objFile As Object
    Dim objComponent As Object, objFS As Object

    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If
    
    Dim strFileName As String, strDestinationPath As String, strDestinationFileName As String
    Dim objExportedFilesCol As Collection, intExportedCount As Long, blnIsContinue As Boolean
    
    intExportedCount = 0
    
    With objFS
    
        Set objDir = .GetFolder(vstrDirectoryPath)

        If Not objDir Is Nothing Then
        
            For Each objComponent In vobjVBProject.VBComponents
            
                If vbext_ct_Document <> objComponent.Type Then
                
                    blnIsContinue = True
                Else
                    blnIsContinue = False
                    
                    If vblnIncludesAlsoObjectModuleCodes Then
                
                        If mfblnDoesVbaObjectModuleHaveEnabledCodes(objComponent) Then
                        
                            blnIsContinue = True
                        End If
                    End If
                End If
            
                If blnIsContinue Then  ' Exclude ThisWorkbook class module and Worksheet class module
            
                    intExportedCount = intExportedCount + 1
            
                    With objComponent
                    
                        strDestinationFileName = .Name & mfstrGetExtensionFromComponentType(.Type)
                    End With
                
                    strDestinationPath = vstrDirectoryPath & "\" & strDestinationFileName
                
                    If .FileExists(strDestinationPath) Then
                    
                        If vblnForceToExport Then
                        
                            .DeleteFile strDestinationPath, True
                            
                            objComponent.Export strDestinationPath
                            
                            If objExportedFilesCol Is Nothing Then Set objExportedFilesCol = New Collection
                            
                            objExportedFilesCol.Add strDestinationFileName
                        End If
                    Else
                        msubExportVBComponentWithCreatingDirectory objComponent, strDestinationPath, objFS
                        
                        If objExportedFilesCol Is Nothing Then Set objExportedFilesCol = New Collection
                        
                        objExportedFilesCol.Add strDestinationFileName
                    End If
                End If
            Next
        End If
    End With

    ' Outputs
    rintExportedCount = intExportedCount
    
    Set robjExportedFilesCol = objExportedFilesCol
End Sub

'''
'''
'''
#If HAS_REF Then

Private Function mfblnDoesVbaObjectModuleHaveEnabledCodes(ByRef robjComponent As VBIDE.VBComponent) As Boolean
#Else

Private Function mfblnDoesVbaObjectModuleHaveEnabledCodes(ByRef robjComponent As Object) As Boolean
#End If
    Dim blnSomeEnabledCodesExist As Boolean
    
    Const intBoundaryLine As Long = 3

    blnSomeEnabledCodesExist = False

    With robjComponent.CodeModule
    
        If .CountOfLines > intBoundaryLine Then
        
            blnSomeEnabledCodesExist = True
        Else
            If .CountOfLines > 0 And .CountOfLines <= intBoundaryLine Then
            
                If Trim(Replace(Replace(.Lines(1, .CountOfLines), "Option Explicit", ""), vbNewLine, "")) <> "" Then
                
                    blnSomeEnabledCodesExist = True
                End If
            End If
        End If
    End With

    mfblnDoesVbaObjectModuleHaveEnabledCodes = blnSomeEnabledCodesExist
End Function

'''
'''
'''
#If HAS_REF Then

Private Sub msubCheckExportedResultAndLogging(ByVal vintExportedCount As Long, _
        ByVal vobjExportedFilesCol As Collection, _
        ByVal vstrDirectoryPath As String, _
        ByVal vobjVBProject As VBIDE.VBProject, _
        Optional ByVal vblnAllowToCreateLog As Boolean = True, _
        Optional ByVal vstrLogOutputDirectoryPath As String = "")

#Else
Private Sub msubCheckExportedResultAndLogging(ByVal vintExportedCount As Long, _
        ByVal vobjExportedFilesCol As Collection, _
        ByVal vstrDirectoryPath As String, _
        ByVal vobjVBProject As Object, _
        Optional ByVal vblnAllowToCreateLog As Boolean = True, _
        Optional ByVal vstrLogOutputDirectoryPath As String = "")
#End If

    Dim strLogFilePath As String, strMsgBoxTitle As String, strLogDir As String
    
#If HAS_REF Then
    Dim objFS As Scripting.FileSystemObject
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objFS As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If

    If vintExportedCount = 0 Then

        strMsgBoxTitle = GetTextOfStrKeyExportAllVbaCodesExportSourceFilesFrom() & " " & objFS.GetFileName(vobjVBProject.fileName)

        MsgBox "Path: " & vobjVBProject.fileName & vbNewLine & GetTextOfStrKeyExportAllVbaCodesNoVbaSourceCode(), vbInformation Or vbOKOnly, strMsgBoxTitle
    Else
        If vobjExportedFilesCol Is Nothing Then
        
            strMsgBoxTitle = GetTextOfStrKeyExportAllVbaCodesExportSourceFilesFrom() & " " & objFS.GetFileName(vobjVBProject.fileName)
            
            MsgBox "Path: " & vobjVBProject.fileName & vbNewLine & GetTextOfStrKeyExportAllVbaCodesFailedToExportBecauseExisted(), vbExclamation Or vbOKOnly, strMsgBoxTitle
        Else
            If vblnAllowToCreateLog Then
            
                If vstrLogOutputDirectoryPath <> "" Then
                
                    strLogDir = vstrLogOutputDirectoryPath
                Else
                    strLogDir = vstrDirectoryPath
                End If
            
                msubExportLogByText strLogDir, vobjVBProject, vobjExportedFilesCol
                
                strLogFilePath = strLogDir & "\" & mstrLogFileBaseName & ".txt"
                
                If vstrLogOutputDirectoryPath <> "" Then
                
                    msubOpenLogFileBySomeTextEditor strLogFilePath, False
                    
                    ' open the Git repository root directory path
                    msubOpenWinExplorerOfDirectoryPath vstrDirectoryPath
                Else
                    ' logging with opening the exporting log output directory path
                    msubOpenLogFileBySomeTextEditor strLogFilePath
                End If
            End If
        End If
    End If
End Sub


'''
'''
'''
Private Sub msubOpenWinExplorerOfDirectoryPath(ByVal vstrDirectoryPath As String)

    Const strWinExploreOutsideOperationInThisVBProject As String = "ExploreFolderWhenOpenedThatIsNothing"

    On Error Resume Next

    ' If this project doesn't have "ExploreFolderWhenOpenedThatIsNothing", then execute a substitute method
    
    Application.Run strWinExploreOutsideOperationInThisVBProject, vstrDirectoryPath

    If Err.Number <> 0 Then
    
#If HAS_REF Then

        Dim objShell As Shell32.Shell

        Set objShell = New Shell32.Shell
#Else
        Dim objShell As Object
        
        Set objShell = CreateObject("Shell.Application")
#End If
        With objShell
        
            .Explore vstrDirectoryPath & "\"
        End With
    Else
        Debug.Print "Called " & strWinExploreOutsideOperationInThisVBProject & " without any errors."
    End If

    On Error GoTo 0
End Sub

'''
'''
'''
Private Sub msubOpenLogFileBySomeTextEditor(ByVal vstrLogFilePath As String, Optional ByVal vblnAllowToOpenDirectoryByExplorer As Boolean = True)

    Const strOpenSomeTextEditorOutsideOperationInThisVBProject As String = "OpenTextBySomeSelectedTextEditorFromWshShell"

    On Error Resume Next
    
    ' If this project doesn't have "OpenTextBySomeSelectedTextEditorFromWshShell", then execute a substitute method
    
    Application.Run strOpenSomeTextEditorOutsideOperationInThisVBProject, vstrLogFilePath, True

    If Err.Number <> 0 Then
    
        msubOpenLogFileByNotepad vstrLogFilePath, vblnAllowToOpenDirectoryByExplorer
    Else
        Debug.Print "Called " & strOpenSomeTextEditorOutsideOperationInThisVBProject & " without any errors."
        
        If vblnAllowToOpenDirectoryByExplorer Then
        
            msubOpenWinExplorerOfDirectoryPath CreateObject("Scripting.FileSystemObject").GetParentFolderName(vstrLogFilePath)
        End If
    End If

    On Error GoTo 0
End Sub


'''
''' Attention! use duplicated codes for keeping this module independency
'''
Private Sub msubOpenLogFileByNotepad(ByVal vstrLogFilePath As String, Optional ByVal vblnAllowToOpenDirectoryByExplorer As Boolean = True)

    Dim strNotepadPath As String, strCmd As String

#If HAS_REF Then
    Dim objWshWhell As IWshRuntimeLibrary.WshShell, objShell As Shell32.Shell
    
    Set objWshWhell = New IWshRuntimeLibrary.WshShell
#Else
    Dim objWshWhell As Object, objShell As Object

    Set objWshWhell = CreateObject("Wscript.Shell")
#End If

    strNotepadPath = "%SystemRoot%\System32\notepad.exe"

    strCmd = strNotepadPath & " """ & vstrLogFilePath & """"
                
    With objWshWhell

        .Run strCmd, 1
    End With
    
    If vblnAllowToOpenDirectoryByExplorer Then
    
        msubOpenWinExplorerOfDirectoryPath CreateObject("Scripting.FileSystemObject").GetParentFolderName(vstrLogFilePath)
    End If
End Sub



'''
'''
'''
#If HAS_REF Then
Private Sub msubExportLogByText(ByVal vstrDirectoryPath As String, ByVal vobjVBProject As VBIDE.VBProject, ByVal vobjExportedFilesCol As Collection)

#Else
Private Sub msubExportLogByText(ByVal vstrDirectoryPath As String, ByVal vobjVBProject As Object, ByVal vobjExportedFilesCol As Collection)
#End If
    Dim strLogPath As String
    
    strLogPath = vstrDirectoryPath & "\" & mstrLogFileBaseName & ".txt"

#If HAS_REF Then
    Dim objFS As Scripting.FileSystemObject
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objFS As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If

    With objFS
        
        With .CreateTextFile(strLogPath, True)
        
            .WriteLine mfstrGetLogOfExportingVBACodes(vstrDirectoryPath, vobjVBProject, vobjExportedFilesCol)
        
            .Close
        End With
    End With
End Sub

'''
'''
'''
#If HAS_REF Then

Private Function mfstrGetLogOfExportingVBACodes(ByVal vstrDirectoryPath As String, ByVal vobjVBProject As VBIDE.VBProject, ByVal vobjExportedFilesCol As Collection) As String

#Else
Private Function mfstrGetLogOfExportingVBACodes(ByVal vstrDirectoryPath As String, ByVal vobjVBProject As Object, ByVal vobjExportedFilesCol As Collection) As String
#End If
    Dim strLog As String, varFileName As Variant
    
    
    strLog = GetTextOfStrKeyExportAllVbaCodesLogExportingFunctionModuleFileName() & ": " & mstrModuleName & vbNewLine & vbNewLine
    
    strLog = strLog & GetTextOfStrKeyExportAllVbaCodesLogProcessedDateTime() & ": " & Format(Now(), "ddd, yyyy/mm/dd hh:mm:ss") & vbNewLine
    
    With CreateObject("WScript.Network")
        
        strLog = strLog & GetTextOfStrKeyExportAllVbaCodesLogCurrentUserName() & ": " & .UserName & vbNewLine
        
        strLog = strLog & GetTextOfStrKeyExportAllVbaCodesLogCurrentComputerName() & ": " & .ComputerName & vbNewLine
    End With
    
    strLog = strLog & vbNewLine
    
    strLog = strLog & GetTextOfStrKeyExportAllVbaCodesLogCountOfExportedFiles() & ": " & CStr(vobjExportedFilesCol.Count) & vbNewLine
    
    strLog = strLog & vbNewLine
    
    strLog = strLog & GetTextOfStrKeyExportAllVbaCodesLogExportedList() & ":" & vbNewLine
    
    For Each varFileName In vobjExportedFilesCol
    
        strLog = strLog & vbTab & CStr(varFileName) & vbNewLine
    Next

    mfstrGetLogOfExportingVBACodes = strLog
End Function



'''
''' get extension name
'''
''' Attention! use duplicated codes for keeping this module independency
'''
Private Function mfstrGetExtensionFromComponentType(ByVal venmComponentType As Variant) As String
    
    Dim strExtension As String

    strExtension = ""

    Select Case venmComponentType
    
        Case vbext_ct_StdModule     ' 1
        
            strExtension = ".bas"
        Case vbext_ct_ClassModule   ' 2
        
            strExtension = ".cls"
        Case vbext_ct_MSForm        ' 3
        
            strExtension = ".frm"
        Case vbext_ct_ActiveXDesigner   ' 11
        
            strExtension = ".cls"
        Case vbext_ct_Document      ' 100
        
            strExtension = ".cls"   ' Worksheet object class module, Workbook object class module
    End Select

    mfstrGetExtensionFromComponentType = strExtension
End Function


'**---------------------------------------------
'** Key-Value cache preparation for ExportAllVBACodes
'**---------------------------------------------
'''
''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyExportAllVbaCodesSelectOfficeFileThatIncludesVba() As String

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        msubInitializeTextForExportAllVBACodes
    End If

    GetTextOfStrKeyExportAllVbaCodesSelectOfficeFileThatIncludesVba = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA")
End Function

'''
''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_FILE_OF_EXCEL_WORD_POWERPOINT
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyExportAllVbaCodesFileOfExcelWordPowerpoint() As String

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        msubInitializeTextForExportAllVBACodes
    End If

    GetTextOfStrKeyExportAllVbaCodesFileOfExcelWordPowerpoint = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_FILE_OF_EXCEL_WORD_POWERPOINT")
End Function

'''
''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_EXPORT_SOURCE_FILES_FROM
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyExportAllVbaCodesExportSourceFilesFrom() As String

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        msubInitializeTextForExportAllVBACodes
    End If

    GetTextOfStrKeyExportAllVbaCodesExportSourceFilesFrom = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_EXPORT_SOURCE_FILES_FROM")
End Function

'''
''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyExportAllVbaCodesNoVbaSourceCode() As String

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        msubInitializeTextForExportAllVBACodes
    End If

    GetTextOfStrKeyExportAllVbaCodesNoVbaSourceCode = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE")
End Function

'''
''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_FAILED_TO_EXPORT_BECAUSE_EXISTED
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyExportAllVbaCodesFailedToExportBecauseExisted() As String

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        msubInitializeTextForExportAllVBACodes
    End If

    GetTextOfStrKeyExportAllVbaCodesFailedToExportBecauseExisted = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_FAILED_TO_EXPORT_BECAUSE_EXISTED")
End Function

'''
''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTING_FUNCTION_MODULE_FILE_NAME
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyExportAllVbaCodesLogExportingFunctionModuleFileName() As String

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        msubInitializeTextForExportAllVBACodes
    End If

    GetTextOfStrKeyExportAllVbaCodesLogExportingFunctionModuleFileName = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTING_FUNCTION_MODULE_FILE_NAME")
End Function

'''
''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyExportAllVbaCodesLogProcessedDateTime() As String

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        msubInitializeTextForExportAllVBACodes
    End If

    GetTextOfStrKeyExportAllVbaCodesLogProcessedDateTime = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME")
End Function

'''
''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_COUNT_OF_EXPORTED_FILES
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyExportAllVbaCodesLogCountOfExportedFiles() As String

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        msubInitializeTextForExportAllVBACodes
    End If

    GetTextOfStrKeyExportAllVbaCodesLogCountOfExportedFiles = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_COUNT_OF_EXPORTED_FILES")
End Function

'''
''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTED_LIST
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyExportAllVbaCodesLogExportedList() As String

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        msubInitializeTextForExportAllVBACodes
    End If

    GetTextOfStrKeyExportAllVbaCodesLogExportedList = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTED_LIST")
End Function

'''
''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyExportAllVbaCodesLogCurrentUserName() As String

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        msubInitializeTextForExportAllVBACodes
    End If

    GetTextOfStrKeyExportAllVbaCodesLogCurrentUserName = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME")
End Function

'''
''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyExportAllVbaCodesLogCurrentComputerName() As String

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        msubInitializeTextForExportAllVBACodes
    End If

    GetTextOfStrKeyExportAllVbaCodesLogCurrentComputerName = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME")
End Function

'''
''' Initialize Key-Values which values. Avoiding to use LocalizationADOConnector instance.
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Sub msubInitializeTextForExportAllVBACodes()

    Dim objKeyToTextDic As Object ' Scripting.Dictionary

    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then

        Set objKeyToTextDic = CreateObject("Scripting.Dictionary") ' Scripting.Dictionary

        Select Case Application.LanguageSettings.LanguageID(msoLanguageIDUI)

            Case 1041 ' Japanese environment

                AddStringKeyValueForExportAllVBACodesByJapaneseValues objKeyToTextDic
            Case Else

                AddStringKeyValueForExportAllVBACodesByEnglishValues objKeyToTextDic
        End Select
        mblnIsStrKeyValueExportAllVBACodesDicInitialized = True
    End If

    Set mobjStrKeyValueExportAllVBACodesDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for ExportAllVBACodes key-values cache
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Sub AddStringKeyValueForExportAllVBACodesByEnglishValues(ByRef robjDic As Object)

    With robjDic

        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA", "Select a Office file which includes VBA codes"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_FILE_OF_EXCEL_WORD_POWERPOINT", "Office file of Excel, Word, or PowerPoint"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_EXPORT_SOURCE_FILES_FROM", "Export source files from"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE", "This office file has no VBA source code modules without object modules"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_FAILED_TO_EXPORT_BECAUSE_EXISTED", "Failed to export, because the VBA souce files have already existed in the specified directory"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTING_FUNCTION_MODULE_FILE_NAME", "This Exporting Function Module File Name"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "Processed date-time"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_COUNT_OF_EXPORTED_FILES", "Count of exported VBA module files"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTED_LIST", "Exported List"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "Current user name"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "Current computer name"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for ExportAllVBACodes key-values cache
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Sub AddStringKeyValueForExportAllVBACodesByJapaneseValues(ByRef robjDic As Object)

    With robjDic

        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA", "VBAコードが含まれるファイルを選択してください"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_FILE_OF_EXCEL_WORD_POWERPOINT", "Excelファイル、Wordファイル、PowerPointファイル"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_EXPORT_SOURCE_FILES_FROM", "エクスポートファイル -"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE", "オブジェクトモジュールコードを除いてVBAソースコードがありませんでした"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_FAILED_TO_EXPORT_BECAUSE_EXISTED", "VBAソースコードの出力に失敗しました。既に同名ファイルがあります。"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTING_FUNCTION_MODULE_FILE_NAME", "エクスポート機能のモジュール名"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "エクスポート日時"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_COUNT_OF_EXPORTED_FILES", "エクスポートしたファイル数"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTED_LIST", "エクスポートしたリスト"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "ユーザー名"
        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "コンピューター名"
    End With
End Sub

'''
''' Remove Keys for ExportAllVBACodes key-values cache
'''
''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueExportAllVBACodes(ByRef robjDic As Object)

    With robjDic

        If .Exists("STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA"
            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_FILE_OF_EXCEL_WORD_POWERPOINT"
            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_EXPORT_SOURCE_FILES_FROM"
            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE"
            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_FAILED_TO_EXPORT_BECAUSE_EXISTED"
            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTING_FUNCTION_MODULE_FILE_NAME"
            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME"
            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_COUNT_OF_EXPORTED_FILES"
            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTED_LIST"
            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME"
            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME"
        End If
    End With
End Sub



