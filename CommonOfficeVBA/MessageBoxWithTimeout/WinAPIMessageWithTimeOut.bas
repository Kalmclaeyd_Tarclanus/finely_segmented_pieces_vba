Attribute VB_Name = "WinAPIMessageWithTimeOut"
'
'   Wrapper solution of MessageBoxTimeoutA API, because the WshShell.Popup doesn't often work in 64bit Windows.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun, 24/May/2020    Takeichi                A part of basic idea has been disclosed at https://takeichi.work/messeagebox-autoclose-timeoutavswsh/
'       Sat,  2/Jul/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Private WinAPI declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function MessageBoxTimeoutA Lib "user32" (ByVal hwnd As LongPtr, ByVal lpText As String, ByVal lpCaption As String, ByVal uType As VbMsgBoxStyle, ByVal wLanguageId As Long, ByVal dwMilliseconds As Long) As Long
#Else
    Private Declare Function MessageBoxTimeoutA Lib "user32" (ByVal hWnd As Long, ByVal lpText As String, ByVal lpCaption As String, ByVal uType As VbMsgBoxStyle, ByVal wLanguageId As Long, ByVal dwMilliseconds As Long) As Long
#End If


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private IsFeatureUsingWshPopupMethodEnabled As Boolean  ' default value is false
    
'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
Public Function MessageBoxWithTimeoutBySecondUnit(ByVal vstrMessage As String, ByVal vintSecondsToWait As Long, ByVal vstrTitle As String, ByVal venmMsgBoxStyle As VbMsgBoxStyle, Optional ByVal vblnAddDisappearTimeMessage As Boolean = True) As Long

    Dim intRet As Long

    If IsFeatureUsingWshPopupMethodEnabled Then
    
        intRet = mfintMessageBoxWithTimeoutByWshPopup(vstrMessage, vintSecondsToWait, vstrTitle, venmMsgBoxStyle, vblnAddDisappearTimeMessage)
    Else
        intRet = mfintMessageBoxWithTimeoutByAPI(vstrMessage, vintSecondsToWait, vstrTitle, venmMsgBoxStyle, vblnAddDisappearTimeMessage)
    End If

    MessageBoxWithTimeoutBySecondUnit = intRet
End Function

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
Private Function mfintMessageBoxWithTimeoutByAPI(ByVal vstrMessage As String, ByVal vintSecondsToWait As Long, ByVal vstrTitle As String, ByVal venmMsgBoxStyle As VbMsgBoxStyle, Optional ByVal vblnAddDisappearTimeMessage As Boolean = True) As Long
    
#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If
    
    Dim strMessage As String
    
    intHWnd = GetThisApplicationHWnd()  ' for only Excel, Application.hwnd
    
    strMessage = mfstrGetMessageWithNotingDisappearingTimeIfItIsNeeded(vstrMessage, vintSecondsToWait, vblnAddDisappearTimeMessage)
    
    mfintMessageBoxWithTimeoutByAPI = MessageBoxTimeoutA(intHWnd, strMessage, vstrTitle, venmMsgBoxStyle, 0, vintSecondsToWait * 1000)
End Function


Private Function mfintMessageBoxWithTimeoutByWshPopup(ByVal vstrMessage As String, ByVal vintSecondsToWait As Long, ByVal vstrTitle As String, ByVal venmMsgBoxStyle As VbMsgBoxStyle, Optional ByVal vblnAddDisappearTimeMessage As Boolean = True) As Long

    Dim strMessage As String
    
    strMessage = mfstrGetMessageWithNotingDisappearingTimeIfItIsNeeded(vstrMessage, vintSecondsToWait, vblnAddDisappearTimeMessage)

    With New IWshRuntimeLibrary.WshShell
        
        mfintMessageBoxWithTimeoutByWshPopup = .Popup(strMessage, vintSecondsToWait, vstrTitle, venmMsgBoxStyle)
    End With
End Function

'''
'''
'''
Private Function mfstrGetMessageWithNotingDisappearingTimeIfItIsNeeded(ByVal vstrMessage As String, ByVal vintSecondsToWait As Long, ByVal vblnAddDisappearTimeMessage As Boolean) As String

    Dim strMessage As String, strAddingMessage As String

    If vblnAddDisappearTimeMessage And vintSecondsToWait > 0 Then
        If vintSecondsToWait > 1 Then
        
            strAddingMessage = "This message will disappear " & CStr(vintSecondsToWait) & " seconds later."
        Else
            strAddingMessage = "This message will disappear " & CStr(vintSecondsToWait) & " second later."
        End If
    
        strMessage = vstrMessage & vbNewLine & vbNewLine & strAddingMessage
        
    Else
        strMessage = vstrMessage
    End If

    mfstrGetMessageWithNotingDisappearingTimeIfItIsNeeded = strMessage
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfMessageBoxWithTimeoutByAPI()

    'mfintMessageBoxWithTimeoutByAPI "Sample message", 3, "Sample title", vbOKOnly Or vbInformation
    
    mfintMessageBoxWithTimeoutByAPI "Sample message", 5, "Sample title", vbOKOnly Or vbExclamation
End Sub

'''
'''
'''
Private Sub msubSanityTestOfMessageBoxWithTimeoutByWshPopup()

    mfintMessageBoxWithTimeoutByWshPopup "Sample message", 3, "Sample title", vbOKOnly Or vbInformation
End Sub

