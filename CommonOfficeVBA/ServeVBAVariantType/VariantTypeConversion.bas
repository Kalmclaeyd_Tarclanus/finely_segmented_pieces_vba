Attribute VB_Name = "VariantTypeConversion"
'
'   Tools for conversions between Variant type and general data type something
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 18/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Compare between two of Variant two-dimensional array
'**---------------------------------------------
'''
'''
'''
Public Function AreSomeDifferentDTColIncludedInAIntegreatedDTCol(ByRef robjAllDifferentDTColExceptPartialMatched As Collection, _
        ByRef robjPartialMatchedDifferentDTCol As Collection, _
        ByVal vobjIntegratedDTCol As Collection, _
        ByVal vobjSourceDTCol As Collection, _
        ByVal vobjMatchingSubsetRowIndexesDic As Scripting.Dictionary) As Boolean

    Dim objDiffDTColWithIncludingPartialMatched As Collection, blnAreAllIncluded As Boolean
    
    blnAreAllIncluded = True
    
    If Not AreAllSourceDTColIncludedInAIntegratedDTCol(objDiffDTColWithIncludingPartialMatched, vobjIntegratedDTCol, vobjSourceDTCol) Then
    
        blnAreAllIncluded = False
    
        If Not AreAllSourceDTColIncludedPartiallyInAIntegratedDTCol(robjAllDifferentDTColExceptPartialMatched, vobjIntegratedDTCol, objDiffDTColWithIncludingPartialMatched, vobjMatchingSubsetRowIndexesDic) Then
        
            If robjAllDifferentDTColExceptPartialMatched.Count < objDiffDTColWithIncludingPartialMatched.Count Then
            
                AreAllSourceDTColIncludedInAIntegratedDTCol robjPartialMatchedDifferentDTCol, robjAllDifferentDTColExceptPartialMatched, objDiffDTColWithIncludingPartialMatched
            End If
        Else
            Set robjPartialMatchedDifferentDTCol = objDiffDTColWithIncludingPartialMatched
        End If
    End If

    AreSomeDifferentDTColIncludedInAIntegreatedDTCol = blnAreAllIncluded
End Function


'''
''' compare by exact matching
'''
''' <Argument>robjDifferentDTCol: Output</Argument>
''' <Argument>vobjIntegratedDTCol: Input</Argument>
''' <Argument>vobjSourceDTCol: Input</Argument>
Public Function AreAllSourceDTColIncludedInAIntegratedDTCol(ByRef robjDifferentDTCol As Collection, ByVal vobjIntegratedDTCol As Collection, ByVal vobjSourceDTCol As Collection)

    Dim blnAreAllIncluded As Boolean, intIntegratedDTRowIndex As Long, intSourceDTRowIndex As Long, intColumnMax As Long, j As Long
    Dim varSourceRowCol As Variant, objSourceRowCol As Collection, varIntegratedRowCol As Variant, objIntegratedRowCol As Collection
    Dim varSourceRowValue() As Variant, objFoundRowIndexDic As Scripting.Dictionary, blnAreRowValuesAllExactlyMatched As Boolean
    Dim blnAMatchingRowExists As Boolean, varItem As Variant
    
    blnAreAllIncluded = True
    
    intColumnMax = vobjIntegratedDTCol.Item(1).Count
    
    ReDim varSourceRowValue(1 To intColumnMax)
    
    intSourceDTRowIndex = 1
    
    Set robjDifferentDTCol = Nothing
    
    Set objFoundRowIndexDic = New Scripting.Dictionary
    
    For Each varSourceRowCol In vobjSourceDTCol
    
        Set objSourceRowCol = varSourceRowCol
        
        j = 1
        
        For Each varItem In objSourceRowCol
        
            varSourceRowValue(j) = varItem
            
            j = j + 1
        Next
        
        blnAMatchingRowExists = False
        
        intIntegratedDTRowIndex = 1
        
        For Each varIntegratedRowCol In vobjIntegratedDTCol
        
            Set objIntegratedRowCol = varIntegratedRowCol
        
            With objFoundRowIndexDic
            
                If Not .Exists(intIntegratedDTRowIndex) Then
            
                    blnAreRowValuesAllExactlyMatched = True
            
                    j = 1
                    
                    For Each varItem In objIntegratedRowCol
                    
                        If varItem <> varSourceRowValue(j) Then
                        
                            blnAreRowValuesAllExactlyMatched = False
                            
                            Exit For
                        End If
                    
                        j = j + 1
                    Next
                    
                    If blnAreRowValuesAllExactlyMatched Then
                    
                        blnAMatchingRowExists = True
                    
                        .Add intIntegratedDTRowIndex, 0
                        
                        Exit For
                    End If
                End If
            End With
        
            intIntegratedDTRowIndex = intIntegratedDTRowIndex + 1
        Next
        
        If Not blnAMatchingRowExists Then
        
            blnAreAllIncluded = False
            
            If robjDifferentDTCol Is Nothing Then Set robjDifferentDTCol = New Collection
            
            robjDifferentDTCol.Add objSourceRowCol
        End If

        intSourceDTRowIndex = intSourceDTRowIndex + 1
    Next
    
    AreAllSourceDTColIncludedInAIntegratedDTCol = blnAreAllIncluded
End Function

'''
'''
'''
Public Sub msubExpandVariantValuesFromCollection(ByRef rvarArray As Variant, ByRef robjCol As Collection)

    Dim varItem As Variant, i As Long
    
    ReDim rvarArray(1 To robjCol.Count)
    
    i = 1
    
    For Each varItem In robjCol
    
        rvarArray(i) = varItem
        
        i = i + 1
    Next
End Sub


'''
'''
'''
Public Function AreTwoColsExactlyMatched(ByVal vobj01Col As Collection, ByVal vobj02Col As Collection) As Boolean

    Dim blnIsExactlyMatched As Boolean, i As Long, var01Item As Variant, var02Array As Variant

    blnIsExactlyMatched = True

    If Not vobj01Col Is Nothing And Not vobj02Col Is Nothing Then
    
        If vobj01Col.Count = vobj02Col.Count Then
        
            If vobj01Col.Count > 0 Then
            
                If TypeName(vobj01Col.Item(1)) = TypeName(vobj02Col.Item(1)) Then
                
                    If TypeName(vobj01Col.Item(1)) = "Collection" Then
                    
                        blnIsExactlyMatched = AreTwoDTColsExactlyMatched(vobj01Col, vobj02Col)
                    Else
                        i = 1
                    
                        msubExpandVariantValuesFromCollection var02Array, vobj02Col
                    
                        For Each var01Item In vobj01Col
                    
                            If TypeName(var01Item) = TypeName(var02Array(i)) Then
                            
                                If var01Item = var02Array(i) Then
                                
                                    ' Matched
                                Else
                                    blnIsExactlyMatched = False
                                
                                    Exit For
                                End If
                            Else
                                blnIsExactlyMatched = False
                                
                                Exit For
                            End If
                            
                            i = i + 1
                        Next
                    End If
                Else
                    blnIsExactlyMatched = False
                End If
            End If
        Else
            blnIsExactlyMatched = False
        End If
    Else
        If vobj01Col Is Nothing And vobj02Col Is Nothing Then
        
        Else
            blnIsExactlyMatched = False
        End If
    End If

    AreTwoColsExactlyMatched = blnIsExactlyMatched
End Function



'''
'''
'''
Public Function AreTwoDTColsExactlyMatched(ByVal vobjDT01Col As Collection, ByVal vobjDT02Col As Collection) As Boolean

    Dim objDiffDT01Col As Collection, objDiffDT02Col As Collection, blnIsExactlyMatched As Boolean

    blnIsExactlyMatched = True

    If AreAllSourceDTColIncludedInAIntegratedDTCol(objDiffDT01Col, vobjDT01Col, vobjDT02Col) Then
    
        If AreAllSourceDTColIncludedInAIntegratedDTCol(objDiffDT02Col, vobjDT02Col, vobjDT01Col) Then
    
        Else
            blnIsExactlyMatched = False
        End If
    Else
        blnIsExactlyMatched = False
    End If

    AreTwoDTColsExactlyMatched = blnIsExactlyMatched
End Function


'''
'''
'''
Public Function AreTwoCountOfColumnsOfDTColsExactlyMatched(ByVal vobjDT01Col As Collection, ByVal vobjDT02Col As Collection) As Boolean

    Dim blnIsColumnsCountEquals As Boolean
    
    blnIsColumnsCountEquals = True
    
    If vobjDT01Col Is Nothing And vobjDT02Col Is Nothing Then
    
        ' Unknown
     
    ElseIf Not vobjDT01Col Is Nothing And Not vobjDT02Col Is Nothing Then
    
        If vobjDT01Col.Count > 0 And vobjDT02Col.Count > 0 Then
        
            If TypeOf vobjDT01Col.Item(1) Is Collection And TypeOf vobjDT02Col.Item(1) Is Collection Then
            
                If vobjDT01Col.Item(1).Count = vobjDT02Col.Item(1).Count Then
                
                    ' Exactly matched
                Else
                    blnIsColumnsCountEquals = False
                End If
            
            ElseIf StrComp(TypeName(vobjDT01Col.Item(1)), TypeName(vobjDT02Col.Item(1))) = 0 Then
        
                ' Exactly matched
            Else
                blnIsColumnsCountEquals = False
            End If
        Else
            blnIsColumnsCountEquals = False
        End If
    Else
        blnIsColumnsCountEquals = False
    End If

    AreTwoCountOfColumnsOfDTColsExactlyMatched = blnIsColumnsCountEquals
End Function



'''
''' compare by partially matching
'''
''' <Argument>robjDifferentDTCol: Output</Argument>
''' <Argument>vobjIntegratedDTCol: Input</Argument>
''' <Argument>vobjSourceDTCol: Input</Argument>
''' <Argument>vobjMatchingSubsetRowIndexesDic: Input</Argument>
Public Function AreAllSourceDTColIncludedPartiallyInAIntegratedDTCol(ByRef robjDifferentDTCol As Collection, _
        ByVal vobjIntegratedDTCol As Collection, _
        ByVal vobjSourceDTCol As Collection, _
        ByVal vobjMatchingSubsetRowIndexesDic As Scripting.Dictionary)

    Dim blnAreAllIncluded As Boolean, intIntegratedDTRowIndex As Long, intSourceDTRowIndex As Long, intColumnMax As Long, j As Long
    Dim varSourceRowCol As Variant, objSourceRowCol As Collection, varIntegratedRowCol As Variant, objIntegratedRowCol As Collection
    Dim varSourceRowValue() As Variant, objFoundRowIndexDic As Scripting.Dictionary, blnAreRowValuesAllPartiallyMatched As Boolean
    Dim blnAPartialMatchingRowExists As Boolean, varItem As Variant
    
    blnAreAllIncluded = True
    
    intColumnMax = vobjIntegratedDTCol.Item(1).Count
    
    ReDim varSourceRowValue(1 To intColumnMax)
    
    intSourceDTRowIndex = 1
    
    Set robjDifferentDTCol = Nothing
    
    Set objFoundRowIndexDic = New Scripting.Dictionary
    
    For Each varSourceRowCol In vobjSourceDTCol
    
        Set objSourceRowCol = varSourceRowCol
        
        j = 1
        
        For Each varItem In objSourceRowCol
        
            varSourceRowValue(j) = varItem
            
            j = j + 1
        Next
        
        blnAPartialMatchingRowExists = False
        
        intIntegratedDTRowIndex = 1
        
        For Each varIntegratedRowCol In vobjIntegratedDTCol
        
            Set objIntegratedRowCol = varIntegratedRowCol
        
            With objFoundRowIndexDic
            
                If Not .Exists(intIntegratedDTRowIndex) Then
            
                    blnAreRowValuesAllPartiallyMatched = True
            
                    j = 1
                    
                    With vobjMatchingSubsetRowIndexesDic  ' This is a only different from AreAllSourceDTColIncludedInAIntegratedDTCol
                    
                        For Each varItem In objIntegratedRowCol
                        
                            If .Exists(j) Then
                        
                                ' partially compare columns
                        
                                If varItem <> varSourceRowValue(j) Then
                                
                                    blnAreRowValuesAllPartiallyMatched = False
                                    
                                    Exit For
                                End If
                            End If
                            
                            j = j + 1
                        Next
                    End With
                    
                    If blnAreRowValuesAllPartiallyMatched Then
                    
                        blnAPartialMatchingRowExists = True
                    
                        .Add intIntegratedDTRowIndex, 0
                        
                        Exit For
                    End If
                End If
            End With
        
            intIntegratedDTRowIndex = intIntegratedDTRowIndex + 1
        Next
        
        If Not blnAPartialMatchingRowExists Then
        
            blnAreAllIncluded = False
            
            If robjDifferentDTCol Is Nothing Then Set robjDifferentDTCol = New Collection
            
            robjDifferentDTCol.Add objSourceRowCol
        End If

        intSourceDTRowIndex = intSourceDTRowIndex + 1
    Next
    
    AreAllSourceDTColIncludedPartiallyInAIntegratedDTCol = blnAreAllIncluded
End Function


'**---------------------------------------------
'** Conversion between String and Variant
'**---------------------------------------------
'''
'''
'''
Public Function GetVariantValueFromGeneralStringInformation(ByRef rstrInfo As String) As Variant

    Dim varValue As Variant
    
    If IsDate(rstrInfo) Then
    
        varValue = CDate(rstrInfo)
        
    ElseIf IsNumeric(rstrInfo) Then
    
        If Abs(CDbl(rstrInfo) - Fix(CDbl(rstrInfo))) > 0 Then
        
            varValue = CDbl(rstrInfo)
        Else
            varValue = CLng(rstrInfo)
        End If
    Else
        varValue = CStr(rstrInfo)
    End If

    GetVariantValueFromGeneralStringInformation = varValue
End Function

'''
''' Updating variant type from String data
'''
Public Sub UpdateVariantValueFromDifferences(ByRef rvarValueToUpdate As Variant, ByRef rvarStringValueOfVariousType As Variant)

    Dim strValue As String
    
    strValue = Trim(rvarStringValueOfVariousType)

    ' Conversion between string type and general Variant data-type

    If strValue <> "" Then
    
        If IsDate(rvarValueToUpdate) And IsDate(strValue) Then
        
            rvarValueToUpdate = CDate(strValue)
            
        ElseIf IsNumeric(rvarValueToUpdate) And IsNumeric(strValue) Then
        
            If Abs(CDbl(strValue) - Fix(CDbl(strValue))) > 0 Then
            
                rvarValueToUpdate = CDbl(strValue)
            Else
                rvarValueToUpdate = CLng(strValue)
            End If
        Else
            rvarValueToUpdate = strValue
        End If
    Else
        If Trim(CStr(rvarValueToUpdate)) <> "" Then
        
            rvarValueToUpdate = Empty
        End If
    End If
End Sub


'''
''' Variant type comparing
'''
''' Attention - 1). this omits strict comparing between floating numerical figures.
'''             2). this serves Empty value as same as "" void-string
'''
Public Function AreTheseVariantAndSpecializedStringPatternMatched(ByRef rvarValue As Variant, _
        ByRef rvarStringPatternValue As Variant, _
        Optional ByVal vblnAllowPartialMatchingAboutString As Boolean = True, _
        Optional ByVal vblnAllowToHitAsFiguresBySpecialKeyWord As Boolean = True) As Boolean

    Dim strPatternValue As String, strValue As String, blnAreMatchedAll As Boolean

    Const strIsNumericMatchSpecialWord As String = "<IsValueNumeric>"


    blnAreMatchedAll = True
    
    strPatternValue = Trim(rvarStringPatternValue)
    
    strValue = Trim(rvarValue)

    If IsDate(rvarValue) And IsDate(strPatternValue) Then

        If CDate(rvarValue) <> CDate(strPatternValue) Then
        
            blnAreMatchedAll = False
        Else
            ' Matched
        End If
    ElseIf IsNumeric(rvarValue) And IsNumeric(strPatternValue) Then
    
        If Abs(rvarValue - Fix(rvarValue)) > 0# Then
        
            ' Omit strict numerical errors in this case
        
            If CDbl(strPatternValue) <> rvarValue Then
            
                blnAreMatchedAll = False
            Else
                ' Matched
            End If
        Else
            If CLng(strPatternValue) <> rvarValue Then
            
                blnAreMatchedAll = False
            Else
                ' Matched
            End If
        End If
    Else
    
        If vblnAllowToHitAsFiguresBySpecialKeyWord Then
        
            If StrComp(strIsNumericMatchSpecialWord, strPatternValue) = 0 Then
        
                If Not IsNumeric(rvarValue) Then
                
                    blnAreMatchedAll = False
                Else
                    ' Numeric matched specially
                End If
            Else
                msubStringMatched blnAreMatchedAll, strValue, strPatternValue, vblnAllowPartialMatchingAboutString
            End If
        Else
            msubStringMatched blnAreMatchedAll, strValue, strPatternValue, vblnAllowPartialMatchingAboutString
        End If
    End If

    AreTheseVariantAndSpecializedStringPatternMatched = blnAreMatchedAll
End Function

'///////////////////////////////////////////////
'/// Internal function
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubStringMatched(ByRef rblnAreMatchedAll As Boolean, ByRef rstrValue As String, ByRef rstrPatternValue As String, ByRef rblnAllowPartialMatchingAboutString As Boolean)

    If rstrValue <> rstrPatternValue Then
    
        If rblnAllowPartialMatchingAboutString Then
        
            If rstrPatternValue = "" Then
            
                rblnAreMatchedAll = False
            Else
                If InStr(1, rstrValue, rstrPatternValue) = 0 Then
                
                    rblnAreMatchedAll = False
                Else
                    ' Partially matched
                End If
            End If
        Else
            rblnAreMatchedAll = False
        End If
    Else
        ' Exactly matched, where this means the "" void string is equals to Empty
    End If
End Sub

