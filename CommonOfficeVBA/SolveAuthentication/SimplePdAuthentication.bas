Attribute VB_Name = "SimplePdAuthentication"
'
'   Simply password keeper only while Office application process exists temporarily
'   If users feel some risks for keeping passwords in VB project variable cache, they can remove this VB code module from this VB project without any VBA syntax error.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  6/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjCacheBookPathToPasswordDic As Scripting.Dictionary  ' Dictionary(Of String[file path], String[password])

Private mobjCacheAccdbPathToPasswordDic As Scripting.Dictionary  ' Dictionary(Of String[file path], String[password])


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Password caching enabling interfaces
'**---------------------------------------------
'''
''' This is called from Application.Run
'''
Public Function IsMsExcelFilePdCachingEnabled() As Boolean

    IsMsExcelFilePdCachingEnabled = True
End Function

'''
''' This is called from Application.Run
'''
Public Function IsMsAccessDbFilePdCachingEnabled() As Boolean

    IsMsAccessDbFilePdCachingEnabled = True
End Function


'**---------------------------------------------
'** Cache some password temporarily
'**---------------------------------------------
'''
'''
'''
Public Function GetLockingBookPasswordByPath(ByRef rstrBookPath As String) As String

    Dim strCachePassword As String
    
    strCachePassword = ""

    If Not mobjCacheBookPathToPasswordDic Is Nothing Then
    
        With mobjCacheBookPathToPasswordDic
        
            If .Exists(rstrBookPath) Then
            
                strCachePassword = .Item(rstrBookPath)
            End If
        End With
    End If

    GetLockingBookPasswordByPath = strCachePassword
End Function

'''
'''
'''
Public Function GetLockingAccdbPasswordByPath(ByRef rstrAccdbPath As String) As String

    Dim strCachePassword As String
    
    strCachePassword = ""

    If Not mobjCacheAccdbPathToPasswordDic Is Nothing Then
    
        With mobjCacheAccdbPathToPasswordDic
        
            If .Exists(rstrAccdbPath) Then
            
                strCachePassword = .Item(rstrAccdbPath)
            End If
        End With
    End If

    GetLockingAccdbPasswordByPath = strCachePassword
End Function

'''
'''
'''
Public Sub ClearCacheTemporaryBookPasswords()

    If Not mobjCacheBookPathToPasswordDic Is Nothing Then
    
        mobjCacheBookPathToPasswordDic.RemoveAll
    
        Set mobjCacheBookPathToPasswordDic = Nothing
    End If
End Sub

'''
'''
'''
Public Sub ClearCacheTemporaryAccdbPasswords()

    If Not mobjCacheAccdbPathToPasswordDic Is Nothing Then
    
        mobjCacheAccdbPathToPasswordDic.RemoveAll
    
        Set mobjCacheAccdbPathToPasswordDic = Nothing
    End If
End Sub

'''
''' Serve .xlsx, .xls file of Microsoft Excel
'''
Public Sub StorePasswordOfBookTemporarily(ByRef rstrBookPath As String, ByRef rstrPassword As String)

    If mobjCacheBookPathToPasswordDic Is Nothing Then Set mobjCacheBookPathToPasswordDic = New Scripting.Dictionary

    With mobjCacheBookPathToPasswordDic
    
        If Not .Exists(rstrBookPath) Then
        
            .Add rstrBookPath, rstrPassword
        Else
            .Item(rstrBookPath) = rstrPassword
        End If
    End With
End Sub

'''
''' Serve .accdb file of Microsoft Access
'''
Public Sub StorePasswordOfAccdbTemporarily(ByRef rstrAccdbPath As String, ByRef rstrPassword As String)

    If mobjCacheAccdbPathToPasswordDic Is Nothing Then Set mobjCacheAccdbPathToPasswordDic = New Scripting.Dictionary

    With mobjCacheAccdbPathToPasswordDic
    
        If Not .Exists(rstrAccdbPath) Then
        
            .Add rstrAccdbPath, rstrPassword
        Else
            .Item(rstrAccdbPath) = rstrPassword
        End If
    End With
End Sub

