Attribute VB_Name = "UTfAdoConnectSqLiteDb"
'
'   Sanity tests to connect SQLite3 database by ADO
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and SQLite3
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 11/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToConnectSqLite3DbByAdo()

    Dim strDbPath As String, strConnectionString As String
    
    strDbPath = GetTemporarySqLiteDataBaseDir() & "\TestSqLiteSample.db"

    CreateSqLiteDatabaseByCmd strDbPath

    strConnectionString = "DRIVER=SQLite3 ODBC Driver;Database=" & strDbPath & ";"

    If IsADOConnectionStringEnabled(strConnectionString) Then
    
        Debug.Print "ADODB connection is enabled for SQLite3 data-base"
    Else
        Debug.Print "Failed to SQLite3 data-base ADODB connection, probably, the SQLite3 ODBC driver has not been isntalled yet."
    End If
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetPathAfterCreateSampleSqLiteDbByCmd()

    CreateSqLiteDbSampleTableByUsingCmd "TestSqLiteSample.db"

    ExploreParentFolderWhenOpenedThatIsNothing GetTemporarySqLiteDataBaseDir()
End Sub



'''
''' The following is to be failed because the ADOX.Catalog.Create interface is not supported at SQLite3 ODBC Driver
'''
Private Sub msubSanityTestToGetPathAfterCreateSampleSqLiteDbByADOX()

    Dim strPath As String

    strPath = GetPathAfterCreateSampleSqLiteDbByADOX("TestSqLiteSample.db")

    ExploreParentFolderWhenOpenedThatIsNothing strPath
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub OpenTestingSQLite3Cmd()

    OpenCmdWithSpecifyingBothWorkingDirectoryAndCmdWindowTitleText GetTemporarySqLiteDataBaseDir(), "SQLite3 testing directory"
End Sub

'''
'''
'''
Public Function PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist(ByRef rstrDbFileName As String) As String

    Dim strDbPath As String
    
    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & rstrDbFileName
    
    If Not FileExistsByVbaDir(strDbPath) Then
    
        CreateSqLiteDbSampleTableByUsingCmd rstrDbFileName
    End If
    
    PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist = strDbPath
End Function


'''
'''
'''
Public Function CreateSqLiteDbSampleTableByUsingCmd(ByRef rstrDbFileName As String) As String

    Dim strDbFileName As String, strDbPath As String, strStdOut As String

    strDbFileName = rstrDbFileName

    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & strDbFileName
    
    If FileExistsByVbaDir(strDbPath) Then
        
        VBA.Kill strDbPath
    End If

    'strStdOut = ExecuteWinCmd("echo .open --new " & rstrDbFileName & " | sqlite3", GetTemporarySqLiteDataBaseDir())
    
    strStdOut = CreateSqLiteDatabaseByCmd(strDbPath)
    
    Debug.Print strStdOut
    
    CreateSqLiteDbSampleTableByUsingCmd = strDbPath
End Function


'''
''' This will be failed because the ADOX.Catalog.Create interface is not supported at SQLite3 ODBC Driver
'''
Public Function GetPathAfterCreateSampleSqLiteDbByADOX(ByVal vstrDbName As String) As String

    Dim strDbPath As String, strConnectionString As String


    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & vstrDbName

    If FileExistsByVbaDir(strDbPath) Then
        
        VBA.Kill strDbPath
    End If
    
    strConnectionString = "DRIVER=SQLite3 ODBC Driver;Database=" & strDbPath & ";"
    
    msubCreateSqLiteDbSampleTableByUsingSQL strConnectionString

    GetPathAfterCreateSampleSqLiteDbByADOX = strDbPath
End Function


'''
'''
'''
Public Function GetTemporarySqLiteDataBaseDir() As String

    Dim strDir As String

    strDir = GetTemporaryDevelopmentVBARootDir() & "\DataBases\SQLite"

    ForceToCreateDirectory strDir

    GetTemporarySqLiteDataBaseDir = strDir
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
''' This will be failed because the ADOX.Catalog.Create interface is not supported at SQLite3 ODBC Driver
'''
Private Sub msubCreateSqLiteDbSampleTableByUsingSQL(ByRef rstrConnectionString As String)

    Dim strTableName As String, strTableFieldTypes As String, strSQL As String

    strTableName = "TestTable"
    
    strTableFieldTypes = "氏名 varchar(20),身長 float"
    
    
    With New ADOX.Catalog
    
        ' create SQLite3 data-base
    
        ' The following are to be failed because the .Create interface doesn't supported at 'SQLite3 ODBC Driver'
    
        With .Create(rstrConnectionString)
        
            strSQL = "create table " & strTableName & " (" & strTableFieldTypes & ");"
            
            ' create a table
            .Execute strSQL
        
            .BeginTrans
        
            strSQL = "insert into " & strTableName & " (氏名, 身長)  values('Suzuki data', 174.6);": .Execute strSQL
            
            strSQL = "insert into " & strTableName & " (氏名, 身長)  values('Takahashi data', 166.2);": .Execute strSQL
            
            strSQL = "insert into " & strTableName & " (氏名, 身長)  values('Tanaka data', 151.4);": .Execute strSQL
            
            .CommitTrans
            
            .Close
        End With
    End With
End Sub

