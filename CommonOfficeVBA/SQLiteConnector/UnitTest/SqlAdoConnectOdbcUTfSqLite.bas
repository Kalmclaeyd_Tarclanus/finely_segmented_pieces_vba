Attribute VB_Name = "SqlAdoConnectOdbcUTfSqLite"
'
'   Sanity test to output SQL results into Excel sheets for a SQLite database using ADO ODBC
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'       Dependent on an installed SQLite ODBC driver
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri,  4/Aug/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** SqLiteAdoConnector
'**---------------------------------------------
'''
''' Create an in-line table from the only SELECT sql inside
'''
Private Sub msubSanityTestToConnectByVirtualTableSqLiteAdoConnector()
    
    Dim strSQL As String

    With New SqLiteAdoConnector

        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoConnectorVirtual.sqlitedb")

        .AllowToRecordSQLLog = True

        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"

        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub

'''
''' create sample Table about SELECT
'''
Private Sub msubSanityTestToConnectToSampleTable01BySqLiteAdoConnector()
    
    Dim strSQL As String, strDbPath As String
    
    
    strDbPath = CreateSqLiteDbSampleTableByUsingCmd("TestDb01OfSqLiteAdoConnector.sqlitedb")

    strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3); INSERT INTO Test_Table VALUES (1, 2, 3); INSERT INTO Test_Table VALUES (4, 5, 6); INSERT INTO Test_Table VALUES (7, 8, 9); INSERT INTO Test_Table VALUES (10, 11, 12)"
    
    ' execute SQL by cmd.exe
    
    ExecuteSqlInSqLiteDatabaseByCmd strSQL, strDbPath

    With New SqLiteAdoConnector

        .SetODBCParametersWithoutDSN strDbPath

        .AllowToRecordSQLLog = True

        strSQL = "SELECT * FROM Test_Table"

        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub


'''
''' create sample Table about CREATE TABLE
'''
''' If the SqLite database file doesn't exist, the SQLite3 ODBC Driver support the ADO ODBC connection. (The SQLite3 database system doesn't need to execute The ADOX.Catalog.Create())
'''
Private Sub msubSanityTestToConnectToSampleTableFromNoDbFileBySqLiteAdoConnector()
    
    Dim strSQL As String, strDbPath As String
    
    strDbPath = GetTemporarySqLiteDataBaseDir() & "\TestDbFromNoExistedOfSqLiteAdoConnector.sqlitedb"

    If FileExistsByVbaDir(strDbPath) Then
    
        VBA.Kill strDbPath
    End If

    With New SqLiteAdoConnector

        .SetODBCParametersWithoutDSN strDbPath

        .AllowToRecordSQLLog = True

        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3)"
        
        .ExecuteSQL strSQL
        
        ' The following causes a ODBC error
'        strSQL = "INSERT INTO Test_Table VALUES (1, 2, 3); INSERT INTO Test_Table VALUES (4, 5, 6)"
'
'        .ExecuteSQL strSQL
        
        strSQL = "INSERT INTO Test_Table VALUES (1, 2, 3)"

        .ExecuteSQL strSQL

        strSQL = "INSERT INTO Test_Table VALUES (4, 5, 6)"

        .ExecuteSQL strSQL
        
        strSQL = "INSERT INTO Test_Table VALUES (7, 8, 9)"

        .ExecuteSQL strSQL
        
        .CloseConnection

        strSQL = "SELECT * FROM Test_Table"

        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTestsWithUsingDropTable(ByRef ritfSqLiteConnector As IADOConnector)

    Dim strSQL As String

    With ritfSqLiteConnector
    
        strSQL = "DROP TABLE Test_Table"
            
        On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
        
        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3, ColText)" ' "create table Test_Table (Col1 integer, Col2 integer, Col3 integer, ColText varchar)"
    
        .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorByExcelSheetFlag
        
        msubAddTableDataFor04Columns ritfSqLiteConnector
        
        .CommitTransaction
    End With
End Sub

'''
'''
'''
Public Sub PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests(ByRef ritfSqLiteConnector As IADOConnector)

    Dim strSQL As String

    With ritfSqLiteConnector
    
        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3, ColText)"
        
        On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
        
        strSQL = "DELETE FROM Test_Table"
    
        .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorByExcelSheetFlag
        
        msubAddTableDataFor04Columns ritfSqLiteConnector
        
        .CommitTransaction
    End With
End Sub

'''
'''
'''
Private Sub msubAddTableDataFor04Columns(ByRef ritfSqLiteConnector As IADOConnector)

    Dim strSQL As String

    With ritfSqLiteConnector

        strSQL = "INSERT INTO Test_Table values (1, 2, 3, 'Type1');"
        
        .ExecuteSQL strSQL
        
        strSQL = "INSERT INTO Test_Table values (4, 5, 6, 'Type1');"
        
        .ExecuteSQL strSQL
        
        strSQL = "INSERT INTO Test_Table values (7, 8, 9, 'Type2');"
        
        .ExecuteSQL strSQL
    
        strSQL = "INSERT INTO Test_Table values (10, 11, 12, 'Type2');"
    
        .ExecuteSQL strSQL
    End With
End Sub


'''
'''
'''
Public Sub PrepareTestTable3ColumnOfSqLiteTestingDataBaseForSqlTests(ByRef ritfSqLiteConnector As IADOConnector)

    Dim strSQL As String

    With ritfSqLiteConnector
    
        strSQL = "DROP TABLE Test_Table"
            
        On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
        
        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3)"
    
        .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorByExcelSheetFlag

        strSQL = "INSERT INTO Test_Table VALUES (1, 2, 3)"

        .ExecuteSQL strSQL

        strSQL = "INSERT INTO Test_Table VALUES (4, 5, 6)"
        
        .ExecuteSQL strSQL
        
        strSQL = "INSERT INTO Test_Table VALUES (7, 8, 9)"
        
        .ExecuteSQL strSQL
        
        strSQL = "INSERT INTO Test_Table VALUES (10, 11, 12)"
        
        .ExecuteSQL strSQL
        
        .CommitTransaction
    End With
End Sub

'''
''' create sample Table about INSERT
'''
Private Sub msubSanityTestToConnectToSampleTable02BySqLiteAdoConnector()
    
    Dim strSQL As String, strDbPath As String
    
    
    strDbPath = CreateSqLiteDbSampleTableByUsingCmd("TestDb02OfSqLiteAdoConnector.sqlitedb")

    strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3); INSERT INTO Test_Table VALUES (1, 2, 3); INSERT INTO Test_Table VALUES (4, 5, 6); INSERT INTO Test_Table VALUES (7, 8, 9); INSERT INTO Test_Table VALUES (10, 11, 12)"
    
    ' execute SQL by cmd.exe
    
    ExecuteSqlInSqLiteDatabaseByCmd strSQL, strDbPath

    With New SqLiteAdoConnector

        .SetODBCParametersWithoutDSN strDbPath

        .AllowToRecordSQLLog = True

        strSQL = "INSERT INTO Test_Table VALUES (15, 16, 17)"

        .ExecuteSQL strSQL

        'DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
    
    
    strSQL = "SELECT * FROM Test_Table"

    Debug.Print ExecuteSqlInSqLiteDatabaseByCmd(strSQL, strDbPath)
End Sub


'''
''' create sample Table about UPDATE
'''
Private Sub msubSanityTestToConnectToSampleTable03BySqLiteAdoConnector()
    
    Dim strSQL As String, strDbPath As String
    
    strDbPath = CreateSqLiteDbSampleTableByUsingCmd("TestDb03OfSqLiteAdoConnector.sqlitedb")

    strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3); INSERT INTO Test_Table VALUES (1, 2, 3); INSERT INTO Test_Table VALUES (4, 5, 6); INSERT INTO Test_Table VALUES (7, 8, 9); INSERT INTO Test_Table VALUES (10, 11, 12)"
    
    ' execute SQL by cmd.exe
    
    ExecuteSqlInSqLiteDatabaseByCmd strSQL, strDbPath

    With New SqLiteAdoConnector

        .SetODBCParametersWithoutDSN strDbPath

        .AllowToRecordSQLLog = True

        strSQL = "UPDATE Test_Table SET Col1 = 31, Col2 = 32 WHERE Col1 = 4 AND Col2 = 5 AND Col3 = 6"

        .ExecuteSQL strSQL

        'DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
    
    
    strSQL = "SELECT * FROM Test_Table"

    Debug.Print ExecuteSqlInSqLiteDatabaseByCmd(strSQL, strDbPath)
End Sub


'''
''' create sample Table about DELETE
'''
Private Sub msubSanityTestToConnectToSampleTable04BySqLiteAdoConnector()
    
    Dim strSQL As String, strDbPath As String
    
    strDbPath = CreateSqLiteDbSampleTableByUsingCmd("TestDb04OfSqLiteAdoConnector.sqlitedb")

    strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3); INSERT INTO Test_Table VALUES (1, 2, 3); INSERT INTO Test_Table VALUES (4, 5, 6); INSERT INTO Test_Table VALUES (7, 8, 9); INSERT INTO Test_Table VALUES (10, 11, 12)"
    
    ' execute SQL by cmd.exe
    
    ExecuteSqlInSqLiteDatabaseByCmd strSQL, strDbPath

    With New SqLiteAdoConnector

        .SetODBCParametersWithoutDSN strDbPath

        .AllowToRecordSQLLog = True

        strSQL = "DELETE FROM Test_Table WHERE Col1 = 4 AND Col2 = 5 AND Col3 = 6"

        .ExecuteSQL strSQL

        .CloseConnection

        strSQL = "SELECT * FROM Test_Table"

        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub


'''
''' create sample Table about DELETE 02
'''
Private Sub msubSanityTestToConnectToSampleTable05BySqLiteAdoConnector()
    
    Dim strSQL As String, strDbPath As String, objRSet As ADODB.Recordset
    
    Dim objConnector As SqLiteAdoConnector
    
    
    Set objConnector = New SqLiteAdoConnector
    
    With objConnector
    
        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestDb05OfSqLiteAdoConnector.sqlitedb"

        .AllowToRecordSQLLog = True
        
        PrepareTestTable3ColumnOfSqLiteTestingDataBaseForSqlTests objConnector
        
    
        strSQL = "SELECT * FROM Test_Table"
        
        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
        

        strSQL = "DELETE FROM Test_Table WHERE Col1 = 4 AND Col2 = 5 AND Col3 = 6"

        .ExecuteSQL strSQL

        .CloseConnection

        strSQL = "SELECT * FROM Test_Table"

        Set objRSet = .GetRecordset(strSQL)

        DebugCol GetTableCollectionFromRSet(objRSet)
    End With
End Sub


'''
'''
'''
Private Sub msubSanityTestToConnectToSampleTable05AndListUpRecordsetProperties()

    Dim strSQL As String, strDbPath As String, objRSet As ADODB.Recordset
    Dim objProperty As ADODB.Property
    
    With New SqLiteAdoConnector
    
        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestDb05OfSqLiteAdoConnector.sqlitedb"

        .AllowToRecordSQLLog = True
        
        PrepareTestTable3ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface
    
        strSQL = "SELECT * FROM Test_Table"
        
        Set objRSet = .GetRecordset(strSQL)
        
        With objRSet
        
            'Debug.Print TypeName(.DataSource)
        
            For Each objProperty In .Properties
        
                With objProperty
                
                    Debug.Print "ADO Recordset property [Name]: " & .Name & ", [Type]: " & CStr(.Type) & ", [Value]: " & CStr(.Value)
                End With
            Next
        End With

        DebugCol GetTableCollectionFromRSet(objRSet)
    End With
End Sub


'''
''' create sample Table
'''
Private Sub msubSanityTestToConnectToSampleTableBySqLiteAdoConnectorWithADOConnectorInterfaceWithUsingDropTable()
    
    Dim strSQL As String, strDbPath As String, objRSet As ADODB.Recordset
    
    Dim objConnector As SqLiteAdoConnector
    
    
    Set objConnector = New SqLiteAdoConnector
    
    With objConnector
    
        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestDb05OfSqLiteAdoConnector.sqlitedb"

        .AllowToRecordSQLLog = True
        
        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTestsWithUsingDropTable objConnector
        
        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTestsWithUsingDropTable objConnector
    
        strSQL = "SELECT * FROM Test_Table"
        
        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub


'''
''' create sample Table
'''
Private Sub msubSanityTestToConnectToSampleTableBySqLiteAdoConnectorWithADOConnectorInterface()
    
    Dim strSQL As String, strDbPath As String, objRSet As ADODB.Recordset
    
    Dim objConnector As SqLiteAdoConnector
    
    
    Set objConnector = New SqLiteAdoConnector
    
    With objConnector
    
        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestDb05OfSqLiteAdoConnector.sqlitedb"

        .AllowToRecordSQLLog = True
        
        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests objConnector
        
        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests objConnector
    
        strSQL = "SELECT * FROM Test_Table"
        
        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub
