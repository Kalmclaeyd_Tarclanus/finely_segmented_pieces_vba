Attribute VB_Name = "OperateSqLite3"
'
'   Utilities for serve SQLite3 at Windows
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on SQLite3
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 11/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleKey As String = "OperateSqLite3"   ' 1st part of registry sub-key

Private Const mstrSubjectInstalledSqLite3PathKey As String = "SQLite3"   ' 2nd part of registry sub-key, recommend local hard drive

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Check whether the sqlite3.exe is installed or not
'**---------------------------------------------
Private mblnIsSQLite3InstallationChecked As Boolean    ' VBA default value is false

Private mblnIsSQLite3InstalledInThisComputer As Boolean                ' VBA default value is false


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function CreateSqLiteDatabaseByCmd(ByVal vstrDbPath As String, Optional ByVal vblnAllowToDeleteAllTablesIfItAlreadyExisted As Boolean = False) As String

    Dim strDbFileName As String, strDir As String, strSqLiteCommand As String, strCmdCommand As String, strStdOut As String
    
    strStdOut = ""
    
    If Not FileExistsByVbaDir(vstrDbPath) Or vblnAllowToDeleteAllTablesIfItAlreadyExisted Then
    
        GetParentDirectoryPathToArgByVbaDir strDir, vstrDbPath
    
        GetFileNameToArgFromPathByVbaDir strDbFileName, vstrDbPath
    
        If Not DirectoryExistsByVbaDir(strDir) Then
        
            ' The following uses FileSystemObject
            ForceToCreateDirectory strDir
        End If
        
        
        If vblnAllowToDeleteAllTablesIfItAlreadyExisted Then
        
            strSqLiteCommand = ".open --new " & strDbFileName
        Else
            strSqLiteCommand = ".open " & strDbFileName
        End If
    
        strCmdCommand = "echo " & strSqLiteCommand & " | " & mfstrGetSqLite3Path()
    
        strStdOut = ExecuteWinCmd(strCmdCommand, strDir)
    End If
    
    
    CreateSqLiteDatabaseByCmd = strStdOut
End Function

'''
'''
'''
Public Function ExecuteSqlInSqLiteDatabaseByCmd(ByVal vstrSQL As String, ByVal vstrDbPath As String) As String

    Dim strDir As String, strDbFileName As String, strCmdCommand As String, strStdOut As String
    
    strDir = GetParentDirectoryPathByVbaDir(vstrDbPath)
    
    strDbFileName = GetFileNameFromPathByVbaDir(vstrDbPath)
    
    strCmdCommand = "echo " & vstrSQL & " | " & mfstrGetSqLite3Path() & " " & strDbFileName
    
    strStdOut = ExecuteWinCmd(strCmdCommand, strDir)

    ExecuteSqlInSqLiteDatabaseByCmd = strStdOut
End Function


'**---------------------------------------------
'** solute path generaly
'**---------------------------------------------
'''
''' search path from registry CURRENT_USER
'''
Private Function mfstrGetSqLite3Path() As String

    Dim objSubPaths As Collection, strTargetPath As String, strModuleNameAndSubjectKey As String
    
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectInstalledSqLite3PathKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If Not mblnIsSQLite3InstallationChecked Then

        If strTargetPath = "" Then
            
            strTargetPath = SearchSpecifiedFileFromCurrentHardDrivesAndSetRegistryIfFound(strModuleNameAndSubjectKey, "sqlite3.exe", "ODBC Driver")
        End If
    
        If strTargetPath <> "" Then
        
             mblnIsSQLite3InstalledInThisComputer = True
             
             SetUNCPathToCurUserReg strModuleNameAndSubjectKey, strTargetPath
        End If
    
        mblnIsSQLite3InstallationChecked = True
    End If
    
    mfstrGetSqLite3Path = strTargetPath
End Function


'''
''' Individual-Set-Registy - AIP Label control
'''
Public Sub ISRInInstalledSqLite3PathOfGetSqLite3Path(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectInstalledSqLite3PathKey, vstrTargetPath
End Sub
Public Sub IDelRInInstalledSqLite3PathOfGetSqLite3Path()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectInstalledSqLite3PathKey
End Sub



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About SQLite relative path
'**---------------------------------------------
'''
''' About 'sqlite3.exe' installed path
'''
Private Sub msubSanityTestToGetSqLite3Path()

    Debug.Print mfstrGetSqLite3Path()
End Sub

