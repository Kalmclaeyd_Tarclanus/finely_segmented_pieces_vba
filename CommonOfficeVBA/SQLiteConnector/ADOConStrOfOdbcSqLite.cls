VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOConStrOfOdbcSqLite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO ODBC connection string generator for SQLite3 RDB for both DSN or DSNless
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on SQLite3 ODBC driver at this Windows
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 12/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IADOConnectStrGenerator

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrSqLite3OdbcDriverName As String = "SQLite3 ODBC Driver"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjADOConStrOfDsn As ADOConStrOfDsn

Private mobjADOConStrOfDsnlessSqLite As ADOConStrOfDsnlessSqLite

Private mblnUseDSN As Boolean

'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub Class_Initialize()

    Set mobjADOConStrOfDsn = New ADOConStrOfDsn

    mobjADOConStrOfDsn.SetOdbcConnectionDestinationInformation OdbcConnectionDestinationToSQLite3

    Set mobjADOConStrOfDsnlessSqLite = New ADOConStrOfDsnlessSqLite
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get DsnAdoConStrGenerator() As ADOConStrOfDsn

    Set DsnAdoConStrGenerator = mobjADOConStrOfDsn
End Property

'''
'''
'''
Public Property Get DsnlessAdoConStrGenerator() As ADOConStrOfDsnlessSqLite

    Set DsnlessAdoConStrGenerator = mobjADOConStrOfDsnlessSqLite
End Property

'''
'''
'''
Public Property Get UseDSN() As Boolean

    UseDSN = mblnUseDSN
End Property
Public Property Let UseDSN(ByVal vblnUseDSN As Boolean)

    mblnUseDSN = vblnUseDSN
End Property

'''
''' read-only SQLite database path
'''
Public Property Get ConnectingFilePath() As String

    ConnectingFilePath = mobjADOConStrOfDsnlessSqLite.ConnectingFilePath
End Property

'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
Private Function IADOConnectStrGenerator_GetConnectionString() As String

    If mblnUseDSN Then
    
        IADOConnectStrGenerator_GetConnectionString = mobjADOConStrOfDsn.GetConnectionString
    Else
        IADOConnectStrGenerator_GetConnectionString = mobjADOConStrOfDsnlessSqLite.GetConnectionString
    End If
End Function

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetConnectionString() As String

    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
End Function

'**---------------------------------------------
'** Set ADODB ODBC parameters directly
'**---------------------------------------------
'''
''' using ODBC DataSource setting (DSN)
'''
Public Sub SetODBCConnectionWithDSN(ByVal vstrDSN As String)

    mobjADOConStrOfDsn.SetODBCConnectionWithDSN vstrDSN, "", ""
    
    mblnUseDSN = True
End Sub

'''
'''
'''
Public Sub SetSQLite3OdbcConnectionWithoutDSN(ByVal vstrDbFilePath As String)

    mobjADOConStrOfDsnlessSqLite.SetSQLite3OdbcConnectionWithoutDSN vstrDbFilePath
    
    mblnUseDSN = False
End Sub


'**---------------------------------------------
'** By a User-form, set ADODB ODBC parameters
'**---------------------------------------------
'''
''' use SimpleAdoConStrAuthentication.bas
'''
''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
''' <Argument>vstrDSN: Input</Argument>
Public Function IsAdoConnectionContinuedAfterSqLiteOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDSN As String = "") As Boolean

    Dim blnIsRequestedToCancelAdoConnection As Boolean

    mblnUseDSN = True
    
    SetOdbcConnectionParametersBySqLiteForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, , , vstrDSN
    
    IsAdoConnectionContinuedAfterSqLiteOdbcConnectionDsnParametersByForm = Not blnIsRequestedToCancelAdoConnection
End Function

'''
''' DSNless connection, with referring SimpleAdoConStrAuthentication.bas
'''
''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
''' <Argument>vstrDbFilePath: Input</Argument>
''' <Argument>vstrDriverName: Input</Argument>
Public Function IsAdoConnectionContinuedAfterSqLiteOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDbFilePath As String = "", _
        Optional ByVal vstrDriverName As String = mstrSqLite3OdbcDriverName) As Boolean

    
    Dim blnIsRequestedToCancelAdoConnection As Boolean
    
    mblnUseDSN = False
    
    SetOdbcConnectionParametersBySqLiteForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, vstrDriverName, vstrDbFilePath
    
    IsAdoConnectionContinuedAfterSqLiteOdbcConnectionDsnlessParametersByForm = Not blnIsRequestedToCancelAdoConnection
End Function



