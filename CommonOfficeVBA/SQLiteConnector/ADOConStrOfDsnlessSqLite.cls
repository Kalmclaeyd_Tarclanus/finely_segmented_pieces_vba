VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOConStrOfDsnlessSqLite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO ODBC connection string generator for SQLite RDB without DSN
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on SQLite3 ODBC driver at this Windows
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 12/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IADOConnectStrGenerator

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrSqLite3OdbcDriverName As String = "SQLite3 ODBC Driver"


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mstrConnectingDbFilePath As String


'///////////////////////////////////////////////
'/// Implemented properties
'///////////////////////////////////////////////
Private Function IADOConnectStrGenerator_GetConnectionString() As String

    IADOConnectStrGenerator_GetConnectionString = GetADODBConnectionOdbcDsnlessStringToSqLite3Db()
End Function

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** ODBC DSNless connection
'**---------------------------------------------
'''
''' read-only Driver Name for ODBC
'''
Public Property Get DriverName() As String

    DriverName = mstrSqLite3OdbcDriverName
End Property

'''
''' read-only SQLite3 data base path
'''
Public Property Get ConnectingFilePath() As String

    ConnectingFilePath = mstrConnectingDbFilePath
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub SetSQLite3OdbcConnectionWithoutDSN(ByVal vstrDbFilePath As String)

    mstrConnectingDbFilePath = vstrDbFilePath
End Sub


Public Function GetConnectionString() As String

    GetConnectionString = GetADODBConnectionOdbcDsnlessStringToSqLite3Db()
End Function

'''
'''
'''
Public Function GetADODBConnectionOdbcDsnlessStringToSqLite3Db() As String

    GetADODBConnectionOdbcDsnlessStringToSqLite3Db = GetADODBConnectionOdbcStringToSqLite3Db(mstrConnectingDbFilePath)
End Function


