VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} USetAdoOdbcConStrForSqLite 
   Caption         =   "USetAdoOdbcConStrForSqLite"
   ClientHeight    =   7030
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   5600
   OleObjectBlob   =   "USetAdoOdbcConStrForSqLite.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "USetAdoOdbcConStrForSqLite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   ADO Connection string parameters form for SQLite database by ODBC
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 14/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "USetAdoOdbcConStrForSqLite"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

' User-form document object
Private mobjFormStateToSetAdoConStr As FormStateToSetAdoConStr


' user-form input result cache
Private mobjCurrentUserInputParametersDic As Scripting.Dictionary

' When FixedSettingNameMode is false, then the following is read from Registry
Private mobjSettingKeyNameToUserInputParametersDic As Scripting.Dictionary  ' Registry cache

'**---------------------------------------------
'** Setting state
'**---------------------------------------------
Private mstrAdodbConnectionSettingKeyName As String


'**---------------------------------------------
'** Form state control
'**---------------------------------------------
Private mblnFixedSettingNameMode As Boolean

Private mblnUseDSN As Boolean


Private mblnIsFormInitialized As Boolean

Private mintInitializing As Long

Private mblnIsUsingDSNOptionChangedAfterRefresh As Boolean

'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub UserForm_Initialize()

    mblnFixedSettingNameMode = False

    mblnIsFormInitialized = False


    mblnIsUsingDSNOptionChangedAfterRefresh = False
End Sub

'''
'''
'''
Private Sub UserForm_Layout()

    If Not mblnIsFormInitialized Then
    
        msubClearAll
        
        GetCurrentFormPositionFromUserReg mstrModuleName, Me, Application.Name
    
        msubInitializeFixedSettingNameMode
    
        msubInitializeCachingConStrParamsOptions
    
        msubLocalize
        
        msubRefresh
        
        
        msubInitializeFirstControlFocus
        
        mblnIsFormInitialized = True
    End If
End Sub

'''
'''
'''
Private Sub UserForm_QueryClose(ByRef rintCancel As Integer, ByRef rintCloseMode As Integer)

    ' When an user clicks the cancel X button, then rintCloseMode becomes False

    If VBA.VbQueryClose.vbFormControlMenu = rintCloseMode Then
    
        mobjFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection = True
    Else
        msubUpdateFormStateToSetAdoConStr
    End If
    
    SetCurrentFormPositionToUserReg mstrModuleName, Me, Application.Name
End Sub


'''
'''
'''
Private Sub optUseDSN_Change()

    If mintInitializing <= 0 Then

        If optUseDSN.Value And Not mblnUseDSN Then
            
            mblnUseDSN = True
            
            msubRefreshDSNEnabledUIControls
        End If
    End If
End Sub

'''
'''
'''
Private Sub optDsnless_Change()

    If mintInitializing <= 0 Then
    
        If optDsnless.Value And mblnUseDSN Then
        
            mblnUseDSN = False
            
            msubRefreshDSNEnabledUIControls
        End If
    End If
End Sub


'''
'''
'''
Private Sub cboConStrCacheName_Change()

    Dim strSettingKeyName As String

    If mintInitializing <= 0 Then

        If cboConStrCacheName.ListCount > 0 Or cboConStrCacheName.Text <> "" Then

            cmdOK.Enabled = True
    
            msubRefresh
        Else
            cmdOK.Enabled = False
        End If
    End If
End Sub


'''
'''
'''
Private Sub cmdAdoConnectionTest_Click()

    Dim strConStr As String
    
    msubUpdateInputDicOfFormState
    
    strConStr = GetADOConnectionOdbcSqLiteStringFromInputDic(mobjCurrentUserInputParametersDic)
    
    AdoVariousTypeConnectionTestAndShowMessageBox strConStr, AdoOdbcAndSQLite
End Sub

'''
'''
'''
Private Sub cmdOK_Click()

    VBA.Global.Unload Me
End Sub

'''
'''
'''
Private Sub chkSaveParametersInRegistoryExceptForPassword_Change()

    If mintInitializing <= 0 Then
    
        SetAdoConnectionSaveParametersInRegistoryExceptForPassword chkSaveParametersInRegistoryExceptForPassword.Value
    End If
End Sub

'''
'''
'''
Private Sub cmdBrowse_Click()

    Dim strInitialPath As String, strPath As String
    
    strInitialPath = txtFullPathOfSqLite.Text
    
    If Not FileExistsByVbaDir(strInitialPath) Then
    
        strInitialPath = GetParentDirectoryPathOfCurrentOfficeFile()
    End If
    
    strPath = GetFilePathByApplicationFileDialog(GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectSqliteDatabaseFile(), strInitialPath, GetTextOfStrKeyUserFormToolsForAdoConStrDatabaseSqlite() & ",*.db;*.sqlitedb", "", True, "BrowseSQLiteDataBaseDirPath")
    
    If strPath <> "" Then
    
        mintInitializing = mintInitializing + 1
    
        txtFullPathOfSqLite.Text = strPath
    
        mintInitializing = mintInitializing - 1
    End If
End Sub


Private Sub txtDSN_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
        
            msubCloseFormToCancel
        End If
    End If
End Sub
Private Sub txtSqLiteOdbcDriver_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
        
            msubCloseFormToCancel
        End If
    End If
End Sub
Private Sub txtFullPathOfSqLite_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
        
            msubCloseFormToCancel
        End If
    End If
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub SetInitialFormStateToSetAdoConStr(ByVal vobjFormStateToSetAdoConStr As FormStateToSetAdoConStr, _
        Optional ByVal vblnFixedSettingNameMode As Boolean = True)

    Set mobjFormStateToSetAdoConStr = vobjFormStateToSetAdoConStr

    With mobjFormStateToSetAdoConStr
    
        Set mobjCurrentUserInputParametersDic = .UserInputParametersDic
    End With

    mblnFixedSettingNameMode = vblnFixedSettingNameMode
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubLocalize()

    Me.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrOdbcSqlite3FormTitle()

    LocalizeUIControlOfAdoConStrInfoForAdoConStrLblConStrCacheName lblConStrCacheName, fraAdoConnectionParameters

    
    ' About fraDsnType
    LocalizeUIControlOfAdoConStrInfoForAdoConStrFraDsnParameters fraDsnType, optUseDSN, optDsnless, fraDSNParameters, lblDataSourceName

    msubLocalizeSqLiteDsnlessSettings

    cmdAdoConnectionTest.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrCmdAdoConnectionTest()

    ' About fraConnectionStringInfoCachingOption
    LocalizeUIControlOfAdoConStrInfoForSaveParametersInRegistoryExceptForPassword fraConnectionStringInfoCachingOption, chkSaveParametersInRegistoryExceptForPassword

End Sub

'''
'''
'''
Private Sub msubLocalizeSqLiteDsnlessSettings()

    fraDSNlessSqLiteParameters.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessSqliteSqlParameters()

    lblOdbcDriver.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblOdbcDriver()

    lblFullPathOfSqLite.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfSqliteDb()
    
    cmdBrowse.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrCmdBrowse()
End Sub


'**---------------------------------------------
'** Update mobjFormStateToSetAdoOdbcConSt from UI
'**---------------------------------------------
'''
'''
'''
Private Sub msubUpdateFormStateToSetAdoConStr()

    Dim objUIInputResultDic As Scripting.Dictionary

    If Not mblnFixedSettingNameMode Then
    
        mobjFormStateToSetAdoConStr.SettingKeyName = GetCurrentSettingKeyNameFromComboBox
    End If
    
    msubUpdateInputDicOfFormState
End Sub

'''
'''
'''
Private Function GetCurrentSettingKeyNameFromComboBox() As String

    GetCurrentSettingKeyNameFromComboBox = GetCurrentValueOfComboBoxWithTextEditable(cboConStrCacheName)
End Function


'''
'''
'''
Private Sub msubUpdateInputDicOfFormState()

    Dim objUIInputResultDic As Scripting.Dictionary

    If mblnIsUsingDSNOptionChangedAfterRefresh Then
    
        Set objUIInputResultDic = New Scripting.Dictionary
        
        msubUpdateInputDicFromUI objUIInputResultDic
        
        If IsAnyDifferencesBetweenTwoDictionariesAboutBothKeyAndValue(mobjCurrentUserInputParametersDic, objUIInputResultDic) Then
        
            Set mobjCurrentUserInputParametersDic = objUIInputResultDic
        End If
        
        Set mobjFormStateToSetAdoConStr.UserInputParametersDic = mobjCurrentUserInputParametersDic
    Else
        msubUpdateInputDicFromUI mobjCurrentUserInputParametersDic
    End If
End Sub


'''
''' Update objUserInputParametersDic from UI
'''
Private Sub msubUpdateInputDicFromUI(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String

    With robjUserInputParametersDic

        strKey = "UseDSN"
        
        If .Exists(strKey) Then
        
            .Item(strKey) = CInt(mblnUseDSN)
        Else
            .Add strKey, CInt(mblnUseDSN)
        End If

        If mblnUseDSN Then
        
            msubUpdateInputDicForDSNFromUI robjUserInputParametersDic
        Else
            msubUpdateInputDicForDSNlessPgSqlFromUI robjUserInputParametersDic
        End If
    End With
End Sub

'''
'''
'''
Private Sub msubUpdateInputDicForDSNFromUI(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String, strValue As String
    
    With robjUserInputParametersDic
    
        strKey = "DSN": strValue = txtDSN.Text
    
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
    End With
End Sub


'''
'''
'''
Private Sub msubUpdateInputDicForDSNlessPgSqlFromUI(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String, strValue As String, intValue As Long
    
    With robjUserInputParametersDic
    
        strKey = "DriverName": strValue = txtSqLiteOdbcDriver.Text
    
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        strKey = mstrAdoDbFilePathKey: strValue = txtFullPathOfSqLite.Text
        
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
    End With
End Sub


'**---------------------------------------------
'** Refresh UI
'**---------------------------------------------
'''
'''
'''
Private Sub msubRefresh()

    mintInitializing = mintInitializing + 1

    msubRefreshFixedSettingNameMode

    msubRefreshDSNEnabling mobjCurrentUserInputParametersDic

    If mblnUseDSN Then
    
        msubRefreshDSNParameters mobjCurrentUserInputParametersDic
    Else
        msubRefreshDSNlessSqLiteParameters mobjCurrentUserInputParametersDic
    End If

    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubRefreshFixedSettingNameMode()

    If Not mblnFixedSettingNameMode Then
    
        msubRefreshCurrentSettingKeyAndUserInputParametersDic
    End If
End Sub

'''
'''
'''
Private Sub msubRefreshCurrentSettingKeyAndUserInputParametersDic()

    Set mobjCurrentUserInputParametersDic = GetCurrentUserInputParametersDicFromComboBoxAndCacheDic(cboConStrCacheName, mobjSettingKeyNameToUserInputParametersDic)
End Sub


'''
'''
'''
Private Sub msubRefreshDSNParameters(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    With robjUserInputParametersDic

        txtDSN.Text = .Item("DSN")
    End With
End Sub

'''
'''
'''
Private Sub msubRefreshDSNlessSqLiteParameters(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim intPortNumber As Long

    With robjUserInputParametersDic

        txtSqLiteOdbcDriver.Text = .Item("DriverName")
        
        txtFullPathOfSqLite.Text = .Item(mstrAdoDbFilePathKey)
    End With
End Sub


'''
'''
'''
Private Sub msubRefreshDSNEnabling(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    With robjUserInputParametersDic
    
        mblnUseDSN = .Item("UseDSN")
    
        msubRefreshDSNEnabledUIControls
    End With
End Sub


'''
'''
'''
Private Sub msubRefreshDSNEnabledUIControls()

    mintInitializing = mintInitializing + 1

    mblnIsUsingDSNOptionChangedAfterRefresh = True

    If mblnUseDSN Then
    
        optUseDSN.Value = True
    
        msubRefreshDSNEnabled True
        
        msubRefreshDSNlessUIEnabled False
    Else
        optDsnless.Value = True
    
        msubRefreshDSNEnabled False
        
        msubRefreshDSNlessUIEnabled True
    End If

    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubRefreshDSNEnabled(ByVal vblnEnabled As Boolean)

    If vblnEnabled Then
    
        fraDSNParameters.Enabled = True
        
        lblDataSourceName.Enabled = True
    
        txtDSN.Enabled = True
    Else
        txtDSN.Enabled = False
    
        lblDataSourceName.Enabled = False
    
        fraDSNParameters.Enabled = False
    End If
End Sub

'''
'''
'''
Private Sub msubRefreshDSNlessUIEnabled(ByVal vblnEnabled As Boolean)

    If vblnEnabled Then
    
        fraDSNlessSqLiteParameters.Enabled = True
    
        msubRefreshDsnlessDetailUIEnabled True
    Else
        msubRefreshDsnlessDetailUIEnabled False
        
        fraDSNlessSqLiteParameters.Enabled = False
    End If
End Sub

'''
'''
'''
Private Sub msubRefreshDsnlessDetailUIEnabled(ByRef rblnEnabled As Boolean)

    lblOdbcDriver.Enabled = rblnEnabled

    txtSqLiteOdbcDriver.Enabled = rblnEnabled
    
    lblFullPathOfSqLite.Enabled = rblnEnabled
    
    txtFullPathOfSqLite.Enabled = rblnEnabled
    
    cmdBrowse.Enabled = rblnEnabled
End Sub



'''
'''
'''
Private Sub msubClearAll()

    msubClearSettingKeyName
    
    msubClearDSN
    
    msubClearDSNlessPostgreSQLParameters
End Sub

'''
'''
'''
Private Sub msubClearSettingKeyName()
    
    mintInitializing = mintInitializing + 1
    
    With cboConStrCacheName
    
        .Clear
    End With
    
    mintInitializing = mintInitializing - 1
End Sub



'''
'''
'''
Private Sub msubClearDSN()

    mintInitializing = mintInitializing + 1
    
    txtDSN.Text = ""
    
    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubClearDSNlessPostgreSQLParameters()

    mintInitializing = mintInitializing + 1

    txtSqLiteOdbcDriver.Text = ""

    txtFullPathOfSqLite.Text = ""

    mintInitializing = mintInitializing - 1
End Sub


'''
'''
'''
Private Sub msubInitializeFixedSettingNameMode()

    mintInitializing = mintInitializing + 1

    InitializeFixedSettingNameModeForEachRDB mobjSettingKeyNameToUserInputParametersDic, cboConStrCacheName, mobjFormStateToSetAdoConStr, AdoOdbcAndPostgreSQL, mblnFixedSettingNameMode, cmdOK
    
    mintInitializing = mintInitializing - 1
End Sub


'''
'''
'''
Private Sub msubInitializeCachingConStrParamsOptions()

    mintInitializing = mintInitializing + 1

    chkSaveParametersInRegistoryExceptForPassword.Value = GetAdoConnectionSaveParametersInRegistoryExceptForPassword()

    mintInitializing = mintInitializing - 1
End Sub



'''
'''
'''
Private Sub msubInitializeFirstControlFocus()

    mintInitializing = mintInitializing + 1
    
    If mblnUseDSN Then
    
        If txtDSN.Text = "" Then
        
            txtDSN.SetFocus
        End If
    Else
        Select Case True
        
            Case txtSqLiteOdbcDriver.Text = ""
        
                txtSqLiteOdbcDriver.SetFocus
                
            Case txtFullPathOfSqLite.Text = ""
            
                txtFullPathOfSqLite.SetFocus
        End Select
    End If
    
    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubCloseFormToCancel()

    mobjFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection = True
    
    cmdOK_Click
End Sub

