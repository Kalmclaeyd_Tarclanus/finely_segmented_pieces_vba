Attribute VB_Name = "ADOConStrToolsForSqLite"
'
'   Generate ADODB connection string to a SQLite data base
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 20/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrSqLite3OdbcDriverName As String = "SQLite3 ODBC Driver"


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetADODBConnectionOdbcStringToSqLite3Db(ByVal vstrDbPath As String) As String
    
    Dim strConnectionString As String, strProvider As String
    
    strConnectionString = "DRIVER=" & mstrSqLite3OdbcDriverName & ";Database=" & vstrDbPath & ";"
        
    GetADODBConnectionOdbcStringToSqLite3Db = strConnectionString
End Function


'**---------------------------------------------
'** for control USetAdoOdbcConStrForPgSql user-form
'**---------------------------------------------
'''
'''
'''
Public Function GetInitialUserInputParametersDicForAdoDSNlessConnectionOfSqLite(Optional ByVal vstrDbFilePath As String = "", _
        Optional ByVal vstrDriverName As String = mstrSqLite3OdbcDriverName) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary

    With objDic
    
        .Add "UseDSN", False
    
        .Add "DriverName", vstrDriverName
    
        .Add mstrAdoDbFilePathKey, vstrDbFilePath
        
        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOdbcConnectionDestinationRDBType(OdbcConnectionDestinationToSQLite3)
    End With

    Set GetInitialUserInputParametersDicForAdoDSNlessConnectionOfSqLite = objDic
End Function


'''
'''
'''
Public Sub UpdateADOConStrOfOdbcSqLiteFromInputDic(ByRef robjADOConStrOfOdbcSqLite As ADOConStrOfOdbcSqLite, ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim blnUseDSN As Boolean

    blnUseDSN = robjUserInputParametersDic.Item("UseDSN")
    
    robjADOConStrOfOdbcSqLite.UseDSN = blnUseDSN
    
    If blnUseDSN Then
    
        UpdateADOConStrOfDSNFromInputDic robjADOConStrOfOdbcSqLite.DsnAdoConStrGenerator, robjUserInputParametersDic
    Else
        UpdateADOConStrOfDsnlessSqLiteFromInputDic robjADOConStrOfOdbcSqLite.DsnlessAdoConStrGenerator, robjUserInputParametersDic
    End If
End Sub

'''
'''
'''
Public Sub UpdateADOConStrOfDsnlessSqLiteFromInputDic(ByRef robjADOConStrOfDsnlessSqLite As ADOConStrOfDsnlessSqLite, ByRef robjUserInputParametersDic As Scripting.Dictionary)

    With robjUserInputParametersDic
    
        robjADOConStrOfDsnlessSqLite.SetSQLite3OdbcConnectionWithoutDSN .Item(mstrAdoDbFilePathKey)
    End With
End Sub

'''
'''
'''
Public Function GetADOConStrOfDsnlessSqLiteFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfDsnlessSqLite

    Dim objADOConStrOfDsnlessSqLite As ADOConStrOfDsnlessSqLite
    
    Set objADOConStrOfDsnlessSqLite = New ADOConStrOfDsnlessSqLite
    
    UpdateADOConStrOfDsnlessSqLiteFromInputDic objADOConStrOfDsnlessSqLite, robjUserInputParametersDic
    
    Set GetADOConStrOfDsnlessSqLiteFromInputDic = objADOConStrOfDsnlessSqLite
End Function


'''
'''
'''
Public Function GetADOConnectionOdbcSqLiteStringFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As String

    Dim strConnection As String, blnUseDSN As Boolean

    With robjUserInputParametersDic
    
        blnUseDSN = .Item("UseDSN")
        
        If blnUseDSN Then
        
            strConnection = GetADOConStrOfDSNFromInputDic(robjUserInputParametersDic).GetConnectionString()
        Else
            strConnection = GetADOConStrOfDsnlessSqLiteFromInputDic(robjUserInputParametersDic).GetConnectionString()
        End If
    End With

    GetADOConnectionOdbcSqLiteStringFromInputDic = strConnection
End Function

'**---------------------------------------------
'** ADO connection string parameters solution interfaces by each special form
'**---------------------------------------------
'''
'''
'''
''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
''' <Argument>robjADOConStrOfOdbcSqLite: Output</Argument>
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrDbFilePath: Input</Argument>
''' <Argument>vstrDSN: Input</Argument>
Public Sub SetOdbcConnectionParametersBySqLiteForm(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
        ByRef robjADOConStrOfOdbcSqLite As ADOConStrOfOdbcSqLite, _
        ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDriverName As String = mstrSqLite3OdbcDriverName, _
        Optional ByVal vstrDbFilePath As String = "", _
        Optional ByVal vstrDSN As String = "")

    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsOpeningFormNeeded As Boolean
        
    rblnIsRequestedToCancelAdoConnection = False
        
    blnIsOpeningFormNeeded = IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfSqLite(objUserInputParametersDic, vstrSettingKeyName, vstrDriverName, vstrDbFilePath, vstrDSN)

    If blnIsOpeningFormNeeded Then
    
        msubSetUserInputParametersDicOfSqLiteByOpeningFormWithFixedSettingKeyName rblnIsRequestedToCancelAdoConnection, objUserInputParametersDic, vstrSettingKeyName
    End If

    If Not rblnIsRequestedToCancelAdoConnection Then
    
        UpdateADOConStrOfOdbcSqLiteFromInputDic robjADOConStrOfOdbcSqLite, objUserInputParametersDic
    End If
End Sub

'''
'''
'''
''' <Argument>robjUserInputParametersDic: Output</Argument>
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrDbFilePath: Input</Argument>
''' <Argument>vstrDSN: Input</Argument>
Private Function IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfSqLite(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDriverName As String = mstrSqLite3OdbcDriverName, _
        Optional ByVal vstrDbFilePath As String = "", _
        Optional ByVal vstrDSN As String = "") As Boolean

    Dim blnIsOpeningFormNeeded As Boolean
    
    blnIsOpeningFormNeeded = True

    ' Try to get parameters from Win-registry cache
    
    Set robjUserInputParametersDic = GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(vstrSettingKeyName)

    Select Case True
    
        Case robjUserInputParametersDic Is Nothing, robjUserInputParametersDic.Count = 0
    
            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfSqLite(robjUserInputParametersDic, vstrDriverName, vstrDbFilePath, vstrDSN)

    End Select

    IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfSqLite = blnIsOpeningFormNeeded
End Function


'''
'''
'''
''' <Argument>robjUserInputParametersDic: Output</Argument>
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrDbFilePath: Input</Argument>
''' <Argument>vstrDSN: Input</Argument>
Private Function IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfSqLite(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        Optional ByVal vstrDriverName As String = mstrSqLite3OdbcDriverName, _
        Optional ByVal vstrDbFilePath As String = "", _
        Optional ByVal vstrDSN As String = "") As Boolean


    Dim blnIsOpeningFormNeeded As Boolean
    
    blnIsOpeningFormNeeded = True
    
    If vstrDSN <> "" Then
    
        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToSQLite3, vstrDSN)
    Else
        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfSqLite(vstrDbFilePath, vstrDriverName)
    End If

    If IsADOConnectionStringEnabled(GetADOConnectionOdbcSqLiteStringFromInputDic(robjUserInputParametersDic)) Then
    
        If vstrDSN <> "" Or (vstrDbFilePath <> "" And FileExistsByVbaDir(vstrDbFilePath)) Then
        
            blnIsOpeningFormNeeded = False
        End If
    End If

    IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfSqLite = blnIsOpeningFormNeeded
End Function



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
''' <Argument>robjUserInputParametersDic: Input - Output</Argument>
''' <Argument>rstrSettingKeyName: Input</Argument>
Private Sub msubSetUserInputParametersDicOfSqLiteByOpeningFormWithFixedSettingKeyName(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        ByRef rstrSettingKeyName As String)


    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr, objUSetAdoOdbcConStrForSqLite As USetAdoOdbcConStrForSqLite
   
    Set objFormStateToSetAdoConStr = GetFormStateToSetAdoConStrByDefaultParams(robjUserInputParametersDic, rstrSettingKeyName)
   
    Set objUSetAdoOdbcConStrForSqLite = New USetAdoOdbcConStrForSqLite
    
    With objUSetAdoOdbcConStrForSqLite
    
        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
    
        .Show vbModal
    End With

    AfterProcessAdoConStrFormSetting rblnIsRequestedToCancelAdoConnection, robjUserInputParametersDic, objFormStateToSetAdoConStr
End Sub



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Object creation tests
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCreateADOConStrOfOdbcSqLite()

    Dim objADOConStrOfOdbcSqLite As ADOConStrOfOdbcSqLite, objADOConStrOfDsnlessSqLite As ADOConStrOfDsnlessSqLite
    
    Set objADOConStrOfOdbcSqLite = New ADOConStrOfOdbcSqLite
    
    Set objADOConStrOfDsnlessSqLite = New ADOConStrOfDsnlessSqLite
End Sub


'**---------------------------------------------
'** USetAdoOdbcConStrForSqLite form tests
'**---------------------------------------------
'''
''' delete testing SettingKeyName from the registory
'''
Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToSqLite()

    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestSqLiteDSNlessKey"
    
    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestSqLiteDSNKey"
End Sub



'''
'''
'''
Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForSqLiteForDSNless()

    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
 
    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfSqLite()

    msubSetUserInputParametersDicOfSqLiteByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestSqLiteDSNlessKey"

    If Not blnIsRequestedToCancelAdoConnection Then
    
        If IsADOConnectionStringEnabled(GetADOConnectionOdbcSqLiteStringFromInputDic(objUserInputParametersDic)) Then
    
            Debug.Print "Connected to SqLite by ADO"
        End If
    End If
End Sub

'''
''' DSN connection to PostgreSQL
'''
Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForSqLiteForDSN()

    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
 
    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToSQLite3)

    msubSetUserInputParametersDicOfSqLiteByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestSqLiteDSNKey"

    If Not blnIsRequestedToCancelAdoConnection Then
    
        If IsADOConnectionStringEnabled(GetADOConnectionOdbcSqLiteStringFromInputDic(objUserInputParametersDic)) Then
    
            Debug.Print "Connected to SqLite by ADO"
        End If
    End If
End Sub
