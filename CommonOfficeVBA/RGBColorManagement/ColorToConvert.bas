Attribute VB_Name = "ColorToConvert"
'
'   color conversion utilities
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 19/Nov/2001    Mr.Medeiros, Mr.Donald  A part of the idea has been disclosed at http://www.xbeat.net/vbspeed/c_HSLToRGB.htm, http://www.xbeat.net/vbspeed/f_ValidateHSL.htm
'       Mon,  6/Jun/2022    Kalmclaeyd Tarclanus    Started refactoring
'

Option Explicit

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' for unit-test, uses at RGBToHSB01
'''
Public Type HSB

    Hue As Long: Saturation  As Long: Brightness  As Long
End Type

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjCacheBackgroundRGBToBoolOfUsingWhiteFontDic As Scripting.Dictionary


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToColorToConvert()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "ColorDefinedByUser,ColorDefinedByUserForXl,UTfColorToConvert"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Automatic color arrangement
'**---------------------------------------------
'''
'''
'''
Public Function IsWhiteFontColorRecommended(ByVal vintBackGroundRGBValue As Long) As Boolean

    Dim blnIsRecommended As Boolean
    Dim intR As Long, intG As Long, intB As Long, intHue As Long, intSaturation As Long, intBrightness As Long
    
    blnIsRecommended = False

    GetLongValueToRGB intR, intG, intB, vintBackGroundRGBValue

    RGBToHSB intHue, intSaturation, intBrightness, vintBackGroundRGBValue

    Select Case True
    
        Case intR < 60 And intG < 60 And intB < 60
            
            blnIsRecommended = True
        
        Case intSaturation < 60 And intBrightness < 75 ' intSaturation < 50 And intBrightness < 50
        
            blnIsRecommended = True
            
        Case ((270 > intHue And intHue > 210) Or (320 < intHue Or intHue < 40)) And intSaturation > 62 And intBrightness > 62
            
            blnIsRecommended = True
    End Select

    IsWhiteFontColorRecommended = blnIsRecommended
End Function


'''
'''
'''
Public Function IsWhiteFontColorRecommendedWithUsingCache(ByVal vintBackGroundRGBValue As Long) As Boolean

    Dim blnIsRecommended As Boolean

    If mobjCacheBackgroundRGBToBoolOfUsingWhiteFontDic Is Nothing Then
    
        Set mobjCacheBackgroundRGBToBoolOfUsingWhiteFontDic = New Scripting.Dictionary
    End If

    With mobjCacheBackgroundRGBToBoolOfUsingWhiteFontDic
    
        If .Exists(vintBackGroundRGBValue) Then
        
            blnIsRecommended = .Item(vintBackGroundRGBValue)
        Else
        
            blnIsRecommended = IsWhiteFontColorRecommended(vintBackGroundRGBValue)
        
            .Add vintBackGroundRGBValue, blnIsRecommended
        End If
    End With

    IsWhiteFontColorRecommendedWithUsingCache = blnIsRecommended
End Function

'''
'''
'''
Public Sub ClearCacheOfBackgroundRGBToBoolOfUsingWhiteFontDic()

    If Not mobjCacheBackgroundRGBToBoolOfUsingWhiteFontDic Is Nothing Then
    
        mobjCacheBackgroundRGBToBoolOfUsingWhiteFontDic.RemoveAll
        
        Set mobjCacheBackgroundRGBToBoolOfUsingWhiteFontDic = Nothing
    End If
End Sub


'**---------------------------------------------
'** Convert RGB value tools
'**---------------------------------------------
'''
'''
'''
Public Function GetTextFromRGB(ByVal vintRGBValue As Long) As String

    Dim intR As Long, intG As Long, intB As Long

    GetLongValueToRGB intR, intG, intB, vintRGBValue

    GetTextFromRGB = GetTextFromEachRGB(intR, intG, intB)
End Function

'''
'''
'''
Public Function GetTextAsFunctionStatementFromRGB(ByVal vintRGBValue As Long) As String

    Dim intR As Long, intG As Long, intB As Long

    GetLongValueToRGB intR, intG, intB, vintRGBValue

    GetTextAsFunctionStatementFromRGB = GetTextFromEachRGBAsFunctionStatement(intR, intG, intB)
End Function

'''
'''
'''
''' <Argument>vintR: Input</Argument>
''' <Argument>vintG: Input</Argument>
''' <Argument>vintB: Input</Argument>
''' <Return>String</Return>
Public Function GetTextFromEachRGB(ByVal vintR As Long, ByVal vintG As Long, ByVal vintB As Long) As String

    GetTextFromEachRGB = "R " & CStr(vintR) & ", G " & CStr(vintG) & ", B " & CStr(vintB)
End Function

'''
'''
'''
''' <Argument>vintR: Input</Argument>
''' <Argument>vintG: Input</Argument>
''' <Argument>vintB: Input</Argument>
''' <Return>String</Return>
Public Function GetTextFromEachRGBAsFunctionStatement(ByVal vintR As Long, ByVal vintG As Long, ByVal vintB As Long) As String

    GetTextFromEachRGBAsFunctionStatement = "RGB(" & CStr(vintR) & ", " & CStr(vintG) & ", " & CStr(vintB) & ")"
End Function

'''
'''
'''
''' <Argument>rintR: Output</Argument>
''' <Argument>rintG: Output</Argument>
''' <Argument>rintB: Output</Argument>
''' <Argument>vintRGBValue: Input</Argument>
Public Sub GetLongValueToRGB(ByRef rintR As Long, _
        ByRef rintG As Long, _
        ByRef rintB As Long, _
        ByVal vintRGBValue As Long)
    
    Dim intValue As Long, intMiddleValue As Long
    
    intMiddleValue = vintRGBValue
    
    intValue = intMiddleValue Mod &H100 ' 256
    
    rintR = intValue
    
    
    intMiddleValue = Fix(intMiddleValue / &H100) ' 256
    
    intValue = intMiddleValue Mod &H100
    
    rintG = intValue
    
    
    intMiddleValue = Fix(intMiddleValue / &H100) ' 256
    
    intValue = intMiddleValue Mod &H100
    
    rintB = intValue
End Sub


Public Function GetBlueColorElementFromRGB(ByVal vintRGBValue As Long) As Long

    GetBlueColorElementFromRGB = vintRGBValue And &HFF
End Function

Public Function GetGreenElementFromRGB(ByVal vintRGBValue As Long) As Long

    GetGreenElementFromRGB = (vintRGBValue \ &H100) And &HFF ' (vintRGBValue \ (2 ^ 8)) And &HFF
End Function

Public Function GetRedElementFromRGB(ByVal vintRGBValue As Long) As Long

    GetRedElementFromRGB = (vintRGBValue \ &H10000) And &HFF     ' (vintRGBValue \ (2 ^ 16)) And &HFF
End Function

'**---------------------------------------------
'** Convert RGB value to gray-scale
'**---------------------------------------------
Public Function GetGraySacleColorFromRGB(ByVal vintRGBValue As Long) As Long

    Dim intRed As Long, intGreen As Long, intBlue As Long

    GetLongValueToRGB intRed, intGreen, intBlue, vintRGBValue
    
    GetGraySacleColorFromRGB = Int((intRed + intGreen + intBlue) / 3)
End Function

'''
''' inverted color of the gray-scale
'''
Public Function GetInverseColorOfGrayScale(ByVal vintRGBValue As Long) As Long

    GetInverseColorOfGrayScale = &HFF - GetGraySacleColorFromRGB(vintRGBValue)
End Function

'''
''' 8 bit inverted color of the gray-scale
'''
Public Function Get8BitInverseGrayScaleColor(ByVal vintRGBValue As Long) As Long

    Get8BitInverseGrayScaleColor = Int(GetInverseColorOfGrayScale(vintRGBValue) / 16)
End Function


'**---------------------------------------------
'** Convertion between RGB and HSB
'**---------------------------------------------
'''
'''
'''
Public Function GetTextFromRGBAndHSB(ByVal vintRGBValue As Long) As String

    Dim intR As Long, intG As Long, intB As Long, intHue As Long, intSaturation As Long, intBrightness  As Long

    GetLongValueToRGB intR, intG, intB, vintRGBValue
    
    RGBToHSB intHue, intSaturation, intBrightness, vintRGBValue

    GetTextFromRGBAndHSB = GetTextFromRGB(vintRGBValue) & ", Hue " & CStr(intHue) & ", Saturation " & CStr(intSaturation) & ", Brightness " & CStr(intBrightness)
End Function



'''
'''
'''
Public Sub ValidateHSB(ByRef rintHue As Long, rintSaturation As Long, rintBrightness As Long)
    
  
    ' rintHue: 0-360, wrapped around (rintHue is a circle)
    '   eg. -2 -> 358, 362 -> 2
    If rintHue < 0 Then
    
        If rintHue < -2147483520 Then   '5965232 * 360
        
            rintHue = (rintHue + 2147483520) + 360
        Else
            rintHue = (rintHue + 2147483520)
        End If
    End If
    rintHue = rintHue Mod 360
      
    ' rintSaturation: 0-100, cut-off
    '   eg. -2 -> 0, 120 -> 100
    If rintSaturation < 0 Then
    
        rintSaturation = 0
        
    ElseIf rintSaturation > 100 Then
    
        rintSaturation = 100
    End If
    
    ' rintBrightness: same as rintSaturation
    If rintBrightness < 0 Then
    
        rintBrightness = 0
        
    ElseIf rintBrightness > 100 Then
    
        rintBrightness = 100
    End If
  
  ''Debug.Print rintHue, rintSaturation, rintBrightness
End Sub



'''
''' base original code is 03
'''
Public Function HSBToRGB(ByVal vintHue As Long, ByVal vintSaturation As Long, ByVal vintBrightness As Long, Optional ByVal Validate As Boolean = False) As Long

    Dim intR As Long, intG As Long, intB As Long
    Dim intMaxBrightness As Long, intMediumBrightness As Long, intMinBrightness As Long
    Dim q As Single
    
    If Validate Then ValidateHSB vintHue, vintSaturation, vintBrightness
    
    intMaxBrightness = (vintBrightness * 255) / 100
    
    If vintSaturation > 0 Then
      
        intMinBrightness = (100 - vintSaturation) * intMaxBrightness / 100
        
        q = (intMaxBrightness - intMinBrightness) / 60
        
        Select Case vintHue
        
            Case 0 To 60
            
                intMediumBrightness = (vintHue - 0) * q + intMinBrightness
                
                intR = intMaxBrightness: intG = intMediumBrightness: intB = intMinBrightness
                
            Case 60 To 120
            
                intMediumBrightness = -(vintHue - 120) * q + intMinBrightness
                
                intR = intMediumBrightness: intG = intMaxBrightness: intB = intMinBrightness
                
            Case 120 To 180
            
                intMediumBrightness = (vintHue - 120) * q + intMinBrightness
                
                intR = intMinBrightness: intG = intMaxBrightness: intB = intMediumBrightness
                
            Case 180 To 240
            
                intMediumBrightness = -(vintHue - 240) * q + intMinBrightness
                
                intR = intMinBrightness: intG = intMediumBrightness: intB = intMaxBrightness
                
            Case 240 To 300
            
                intMediumBrightness = (vintHue - 240) * q + intMinBrightness
                
                intR = intMediumBrightness: intG = intMinBrightness: intB = intMaxBrightness
                
            Case 300 To 360
            
                intMediumBrightness = -(vintHue - 360) * q + intMinBrightness
                
                intR = intMaxBrightness: intG = intMinBrightness: intB = intMediumBrightness
        End Select
        
        HSBToRGB = intB * &H10000 + intG * &H100& + intR
    Else
      
        HSBToRGB = intMaxBrightness * &H10101
    End If

End Function


'''
''' base original code is 03
'''
''' <Argument>rintHue: Output - from Min 0 to Max 360</Argument>
''' <Argument>rintSaturation: Output - from Min 0 to Max 100</Argument>
''' <Argument>rintBrightness: Output - from Min 0 to Max 100</Argument>
''' <Argument>vintRGBValue: Input</Argument>
Public Sub RGBToHSB(ByRef rintHue As Long, ByRef rintSaturation As Long, ByRef rintBrightness As Long, ByVal vintRGBValue As Long)
    
    Dim intR As Long, intG As Long, intB As Long
    Dim intMaxBrightness As Long, intMinBrightness As Long
    Dim q As Single
    Dim intBrightnessDifference As Long
    
    Static Lum(255) As Long: Static QTab(255) As Single
    
    Static intInitializing As Long
    
    If intInitializing = 0 Then
    
        For intInitializing = 2 To 255 ' 0 and 1 are both 0
        
            Lum(intInitializing) = intInitializing * 100 / 255
        Next
        
        For intInitializing = 1 To 255
        
            QTab(intInitializing) = 60 / intInitializing
        Next
    End If
    
    intR = vintRGBValue And &HFF
    
    intG = (vintRGBValue And &HFF00&) \ &H100&
    
    intB = (vintRGBValue And &HFF0000) \ &H10000
    
    If intR > intG Then
    
        intMaxBrightness = intR: intMinBrightness = intG
    Else
        intMaxBrightness = intG: intMinBrightness = intR
    End If
    
    If intB > intMaxBrightness Then
    
        intMaxBrightness = intB
        
    ElseIf intB < intMinBrightness Then
    
        intMinBrightness = intB
    End If
    
    rintBrightness = Lum(intMaxBrightness)
    
    intBrightnessDifference = intMaxBrightness - intMinBrightness
    
    'If intBrightnessDifference Then
    If intBrightnessDifference >= 0 Then
        
        If intMaxBrightness = 0 Then
        
            If intR = 0 And intG = 0 And intB = 0 Then
        
                rintSaturation = 0
            Else
                rintSaturation = 100
            End If
            
            rintHue = 0
        Else
            rintSaturation = (intBrightnessDifference) * 100 / intMaxBrightness
            
            q = QTab(intBrightnessDifference)
            
            Select Case intMaxBrightness
            
                Case intR
                
                    If intB > intG Then
                    
                        rintHue = q * (intG - intB) + 360
                    Else
                        rintHue = q * (intG - intB)
                    End If
                    
                Case intG
                
                    rintHue = q * (intB - intR) + 120
                    
                Case intB
                
                    rintHue = q * (intR - intG) + 240
            End Select
        End If
    End If
End Sub


'''
''' base original code is 03
'''
Public Function GetHSBFromRGB(ByVal vintRGBValue As Long) As HSB
    
    With GetHSBFromRGB
    
        RGBToHSB .Hue, .Saturation, .Brightness, vintRGBValue
    End With
End Function

