Attribute VB_Name = "UTfColorToConvert"
'
'   Unit tests for converting color data
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 24/Nov/2001    VBSpeed                 A part of the idea has been disclosed at http://www.xbeat.net/vbspeed/IsGoodHSBToRGB.htm
'       Sat, 18/Jun/2022    Kalmclaeyd Tarclanus    Started re-design
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetLongValueToRGB()

    DebugGetLongValueToRGB RGB(198, 89, 17)

    DebugGetLongValueToRGB RGB(201, 208, 247)
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub DebugGetLongValueToRGB(ByVal vintRGBValue As Long)

    Dim intR As Long, intG As Long, intB As Long

    GetLongValueToRGB intR, intG, intB, vintRGBValue
    
    Debug.Print GetTextFromEachRGB(intR, intG, intB)
End Sub


'''
'''
'''
Public Function GetRGBFromHSBInterface(ByVal vstrHSBToRGBName As String, ByVal vintHue As Long, ByVal vintSaturation As Long, ByVal vintBrightness As Long, Optional ByVal Validate As Boolean = False) As Long

    Dim intRGB As Long
    
    With GetCurrentOfficeFileObject()
    
        intRGB = .Application.Run(vstrHSBToRGBName, vintHue, vintSaturation, vintBrightness)
    End With

    GetRGBFromHSBInterface = intRGB
End Function

'''
'''
'''
Public Function GetHSBFromRGBInterface(ByVal vstrRGBToHSBName As String, ByVal vintRGB As Long) As HSB
    
    With GetHSBFromRGBInterface
    
        GetCurrentOfficeFileObject().Application.Run vstrRGBToHSBName, .Hue, .Saturation, .Brightness, vintRGB
    End With
End Function


'''
''' For unit test
'''
Public Function RGBAsString(ByVal vintRGB As Long) As String
    
    Dim intR As Long, intG As Long, intB As Long
    
    intR = vintRGB And &HFF
    
    intG = (vintRGB And &HFF00&) \ &H100&
    
    intB = (vintRGB And &HFF0000) \ &H10000
    
    RGBAsString = intR & "," & intG & "," & intB
    
    Debug.Print "[R]" & intR & ", [G]" & intG & ", [B]" & intB
End Function


'''
'''
'''
Public Function HSBAsString(ByRef rstdHSB As HSB) As String
  
    With rstdHSB
    
        Debug.Print "[H]" & .Hue & ", [S]" & .Saturation & ", [L]" & .Brightness
    
        HSBAsString = .Hue & "," & .Saturation & "," & .Brightness
        
    End With
End Function

