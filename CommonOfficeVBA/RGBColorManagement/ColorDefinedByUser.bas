Attribute VB_Name = "ColorDefinedByUser"
'
'   color conversion between a defined enumeration and color value
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 21/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
Public Enum trRGBColor

    trRGBUnknown = 0

    trRGBDarkOrangeFontColor = 1

    trRGBPaleBlueFontColor = 2

    trRGBDeepBlueFontColor = 3

    trRGBDarkSepiaFontColor = 4

    trRGBBeigeWhiteFontColor = 5

    trRGBPaleYellowFontColor = 7

    trRGBPaleGreenFontColor = 8
    
    trRGBLightGrayFontColor = 9

    trRGBWhiteFontColor = 10
    
    trRGBBlackFontColor = 11

    ' trRGBDarkYellowFontColor

End Enum

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjRGBTextToValueCacheDic As Scripting.Dictionary

Private mobjRGBValueToTextCacheDic As Scripting.Dictionary

Private mobjRGBEnmToValueCacheDic As Scripting.Dictionary


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Enumeration trRGBDarkOrangeFontColor and RGB values
'**---------------------------------------------
'''
'''
'''
Private Sub msubSetRGBTextToValueCache()

    Dim varText As Variant

    ClearCacheOfRGBTextToDicAndReverse
    
    Set mobjRGBTextToValueCacheDic = New Scripting.Dictionary
    
    With mobjRGBTextToValueCacheDic
    
        .Add "trRGBDarkOrangeFontColor", RGB(198, 89, 17)
    
        .Add "trRGBPaleBlueFontColor", RGB(201, 208, 247)
    
        .Add "trRGBDeepBlueFontColor", RGB(16, 15, 255)
    
        .Add "trRGBDarkSepiaFontColor", RGB(105, 83, 21)
    
        .Add "trRGBBeigeWhiteFontColor", RGB(248, 203, 173)
    
        
    
        .Add "trRGBPaleYellowFontColor", RGB(255, 242, 204)
        
        .Add "trRGBPaleGreenFontColor", RGB(226, 240, 217)
    
        .Add "trRGBLightGrayFontColor", RGB(242, 242, 242)
    
    
        .Add "trRGBWhiteFontColor", RGB(255, 255, 255)
        
        .Add "trRGBBlackFontColor", RGB(0, 0, 0)
    End With
    
    Set mobjRGBValueToTextCacheDic = GetInverseDictionaryAsDictionaryFromValueToKeyWithoutDuplicatedValues(mobjRGBTextToValueCacheDic)

    Set mobjRGBEnmToValueCacheDic = New Scripting.Dictionary
    
    
    With mobjRGBTextToValueCacheDic
    
        For Each varText In .Keys
        
            mobjRGBEnmToValueCacheDic.Add GetTrRGBEnumFromText(varText), .Item(varText)
        Next
    End With
End Sub

'''
'''
'''
Public Function GetTrRGBTextToValueCacheDic() As Scripting.Dictionary

    If mobjRGBTextToValueCacheDic Is Nothing Then
    
        msubSetRGBTextToValueCache
    End If

    Set GetTrRGBTextToValueCacheDic = mobjRGBTextToValueCacheDic
End Function

'''
'''
'''
Public Function GetTrRGBEnmToValueCacheDic() As Scripting.Dictionary

    If mobjRGBEnmToValueCacheDic Is Nothing Then
    
        msubSetRGBTextToValueCache
    End If

    Set GetTrRGBEnmToValueCacheDic = mobjRGBEnmToValueCacheDic
End Function

'''
'''
'''
Public Function GetTrRGBValueToTextCacheDic() As Scripting.Dictionary

    If mobjRGBValueToTextCacheDic Is Nothing Then
    
        msubSetRGBTextToValueCache
    End If

    Set GetTrRGBValueToTextCacheDic = mobjRGBValueToTextCacheDic
End Function


'''
'''
'''
Private Function GetTrRGBEnumFromText(ByVal vstrEnumText As String) As trRGBColor

    Dim enmRGBColor As trRGBColor
    
    enmRGBColor = trRGBUnknown
    
    Select Case vstrEnumText
    
        Case "trRGBDarkOrangeFontColor"
    
            enmRGBColor = trRGBDarkOrangeFontColor
            
        Case "trRGBPaleBlueFontColor"
        
            enmRGBColor = trRGBPaleBlueFontColor
        
        Case "trRGBDarkSepiaFontColor"
        
            enmRGBColor = trRGBDarkSepiaFontColor
        
        Case "trRGBBeigeWhiteFontColor"
        
            enmRGBColor = trRGBBeigeWhiteFontColor
        
        Case "trRGBPaleYellowFontColor"
        
            enmRGBColor = trRGBPaleYellowFontColor
            
        Case "trRGBPaleGreenFontColor"
        
            enmRGBColor = trRGBPaleGreenFontColor
            
        Case "trRGBLightGrayFontColor"
        
            enmRGBColor = trRGBLightGrayFontColor
        
        Case "trRGBWhiteFontColor"
        
            enmRGBColor = trRGBWhiteFontColor
    
        Case "trRGBBlackFontColor"
    
            enmRGBColor = trRGBBlackFontColor
    End Select
    

    GetTrRGBEnumFromText = enmRGBColor
End Function


'''
'''
'''
Public Sub ClearCacheOfRGBTextToDicAndReverse()

    If Not mobjRGBTextToValueCacheDic Is Nothing Then
    
        mobjRGBTextToValueCacheDic.RemoveAll
        
        Set mobjRGBTextToValueCacheDic = Nothing
    End If
    
    If Not mobjRGBValueToTextCacheDic Is Nothing Then
    
        mobjRGBValueToTextCacheDic.RemoveAll
    
        Set mobjRGBValueToTextCacheDic = Nothing
    End If
    
    If Not mobjRGBEnmToValueCacheDic Is Nothing Then
    
        mobjRGBEnmToValueCacheDic.RemoveAll
    
        Set mobjRGBEnmToValueCacheDic = Nothing
    End If
End Sub



'**---------------------------------------------
'** Conversion between a text and a RGB value
'**---------------------------------------------
'''
''' The string format is limited as ... [RGB(25, 36, 210)]
'''
Public Function GetRGBValueFromText(ByRef rstrText As String) As Long

    Dim intR As Long, intG As Long, intB As Long
    
    GetEachRGBValueFromText intR, intG, intB, rstrText

    'Debug.Print strValues

    GetRGBValueFromText = RGB(intR, intG, intB)
End Function

'''
'''
'''
Public Sub GetEachRGBValueFromText(ByRef rintR As Long, ByRef rintG As Long, ByRef rintB As Long, ByRef rstrText As String)

    Dim intLeftParen As Long, intRightParen As Long
    Dim strArgument As String, strValues() As String
    
    intLeftParen = InStr(1, rstrText, "(")
    
    intRightParen = InStrRev(rstrText, ")")

    strArgument = Mid(rstrText, intLeftParen + 1, intRightParen - intLeftParen - 1)

    strValues = Split(strArgument, ",")

    rintR = CLng(Trim(strValues(0))): rintG = CLng(Trim(strValues(1))): rintB = CLng(Trim(strValues(2)))
End Sub


'''
'''
'''
Public Function GetRGBColorDTCol(ByVal vobjColorTextCol As Collection) As Collection

    Set GetRGBColorDTCol = GetRGBAndHSBColorDTCol(vobjColorTextCol, False)
End Function


'''
'''
'''
Public Function GetRGBAndHSBColorDTCol(ByVal vobjColorTextCol As Collection, Optional ByVal vblnAllowToIncludeHSBValues As Boolean = True) As Collection

    Dim varText As Variant, strText As String
    Dim intR As Long, intG  As Long, intB As Long, intHue As Long, intSaturation As Long, intBrightness As Long
    Dim objCol As Collection, objRowCol As Collection
    
    
    Set objCol = New Collection
    
    For Each varText In vobjColorTextCol
    
        strText = varText
    
        GetEachRGBValueFromText intR, intG, intB, strText
    
        Set objRowCol = New Collection
        
        With objRowCol
        
            .Add intR: .Add intG: .Add intB
            
            .Add RGB(intR, intG, intB)
        
            If vblnAllowToIncludeHSBValues Then
            
                RGBToHSB intHue, intSaturation, intBrightness, RGB(intR, intG, intB)
                
                .Add intHue: .Add intSaturation: .Add intBrightness
            End If
        End With
    
        objCol.Add objRowCol
    Next

    Set GetRGBAndHSBColorDTCol = objCol
End Function


'''
''' testing interface
'''
Public Function GetSampleRGBColorTextCol() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add "RGB(198,89,17)"
        
        .Add "RGB(201, 208, 247)"
        
        .Add "RGB(16, 15, 255)"
        
        .Add "RGB(105, 83, 21)"
        
        .Add "RGB(248,203,173)"
        
        .Add "RGB(255, 242, 204)"
        
        .Add "RGB(226, 240, 217)"
        
        .Add "RGB(242, 242, 242)"
        
        .Add "RGB(255, 255, 255)"
        
        .Add "RGB(0, 0, 0)"
        
        .Add "RGB(1, 0, 0)"
        
        .Add "RGB(0, 1, 0)"
        
        .Add "RGB(0, 0, 1)"
        
        .Add "RGB(15, 15, 15)"
        
        .Add "RGB(94, 94, 94)"
        
        .Add "RGB(128, 128, 128)"
    
        .Add "RGB(70, 56, 38)"
    
        .Add "RGB(255, 0, 0)"
        
        .Add "RGB(0, 255, 0)"
        
        .Add "RGB(0, 0, 255)"
        
        .Add "RGB(255, 255, 0)"
    
        .Add "RGB(255, 0, 255)"
        
        .Add "RGB(0, 255, 255)"
        
        .Add "RGB(68, 114, 196)"
    End With
    
    Set GetSampleRGBColorTextCol = objCol
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetColorInfo()

    Dim objDTCol As Collection
    
    Set objDTCol = GetRGBColorDTCol(GetSampleRGBColorTextCol())

    DebugCol objDTCol
End Sub




