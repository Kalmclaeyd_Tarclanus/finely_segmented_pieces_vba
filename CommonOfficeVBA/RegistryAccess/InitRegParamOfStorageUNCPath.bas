Attribute VB_Name = "InitRegParamOfStorageUNCPath"
'
'   solute to generate flex RegParamOfStorageUNCPath objects
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 12/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetSingleColOfNewRegParamOfStorageUNCPath(ByVal vobjRegParamOfStorageUNCPath As RegParamOfStorageUNCPath) As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    objCol.Add vobjRegParamOfStorageUNCPath

    Set GetSingleColOfNewRegParamOfStorageUNCPath = objCol
End Function


'''
''' get new RegParamOfStrageUNCPath object from three parameters for directory
'''
Public Function GetNewRegParamOfStorageUNCDirectoryPath(ByVal venmStorageSearchMode As _
        StoragePathSearchingFlag, _
        ByVal vstrLocalPriorSubPath As String, _
        ByVal vstrNetworkPriorSubPath As String, _
        Optional ByVal vstrSolveNetworkHostAndDirectoryPathByPlugInInterface As String = "", _
        Optional ByVal vstrSolveLocalDriveLetterByPlugInInterface As String = "") As RegParamOfStorageUNCPath

    Dim objRegParamOfStorageUNCPath As RegParamOfStorageUNCPath
    
    Set objRegParamOfStorageUNCPath = New RegParamOfStorageUNCPath
    
    With objRegParamOfStorageUNCPath
    
        .StorageSearchMode = venmStorageSearchMode
        
        .LocalPriorSubPath = vstrLocalPriorSubPath
        
        .NetworkPriorSubPath = vstrNetworkPriorSubPath
        
        If "" <> vstrSolveNetworkHostAndDirectoryPathByPlugInInterface Then
            
            .SolveNetworkHostAndDirectoryPathByPlugInInterface = vstrSolveNetworkHostAndDirectoryPathByPlugInInterface
        End If
        
        If "" <> vstrSolveLocalDriveLetterByPlugInInterface Then
            
            .SolveLocalDriveLetterByPlugInInterface = vstrSolveLocalDriveLetterByPlugInInterface
        End If
    End With
    
    Set GetNewRegParamOfStorageUNCDirectoryPath = objRegParamOfStorageUNCPath
End Function

'''
'''
'''
Public Function GetNewRegParamOfStorageUNCFilePath(ByVal vstrDefaultFileName As String, ByVal venmStorageSearchMode As StoragePathSearchingFlag, ByVal vstrLocalPriorSubPath As String, ByVal vstrNetworkPriorSubPath As String, Optional ByVal vblnIsRecommendedToCreateNewWhenItDoesNotExist As Boolean = False, Optional ByVal vstrSolveNetworkHostAndDirectoryPathByPlugInInterface As String = "", Optional ByVal vstrSolveLocalDriveLetterByPlugInInterface As String = "") As RegParamOfStorageUNCPath

    Dim objRegParamOfStorageUNCPath As RegParamOfStorageUNCPath
    
    Set objRegParamOfStorageUNCPath = New RegParamOfStorageUNCPath
    
    With objRegParamOfStorageUNCPath
    
        .DefaultFileName = vstrDefaultFileName
    
        If vblnIsRecommendedToCreateNewWhenItDoesNotExist Then
        
            .IsRecommendedToCreateNewWhenItDoesNotExist = vblnIsRecommendedToCreateNewWhenItDoesNotExist
        End If
    
        If (venmStorageSearchMode And PathIsDirectory) > 0 Then
        
            MsgBox "Has to be a File, The flag 'PathIsDirectory' is enabled", vbCritical Or vbOKOnly, "Wrong parameter setting"
        End If
    
        .StorageSearchMode = venmStorageSearchMode
        
        .LocalPriorSubPath = vstrLocalPriorSubPath
        
        .NetworkPriorSubPath = vstrNetworkPriorSubPath
        
        If "" <> vstrSolveNetworkHostAndDirectoryPathByPlugInInterface Then
            
            .SolveNetworkHostAndDirectoryPathByPlugInInterface = vstrSolveNetworkHostAndDirectoryPathByPlugInInterface
        End If
        
        If "" <> vstrSolveLocalDriveLetterByPlugInInterface Then
            
            .SolveLocalDriveLetterByPlugInInterface = vstrSolveLocalDriveLetterByPlugInInterface
        End If
    End With
    
    Set GetNewRegParamOfStorageUNCFilePath = objRegParamOfStorageUNCPath
End Function
