Attribute VB_Name = "WinRegKeepingFormState"
'
'   Utilities for keeping the user-inputted state of form in the Windows registory
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon,  6/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrControlTextBoxRegKey As String = "TextBoxes"

Private Const mstrControlCheckBoxRegKey As String = "CheckBoxes"

Private Const mstrControlOptionButtonRegKey As String = "OptionButtons"

Private Const mstrWindowPositionRegKey As String = "WindowPosition"


Private Const mstrWindowLeftPositionEntryName As String = "WindowLeftPoint"

Private Const mstrWindowTopPositionEntryName As String = "WindowTopPoint"


Private Const mstrWindowWidthEntryName As String = "WindowWidth"

Private Const mstrWindowHeightEntryName As String = "WindowHeight"


Private Const mstrUserInputtedTextRegKey As String = "UserInputtedText"

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** each solutions
'**---------------------------------------------
'''
''' keep user-inputted-text in TextBox at the registry
'''
Public Sub SetCurUserRegUserInputtedStringForKeepingFormInputtedState(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vstrChildRegKey As String, _
        ByVal vstrEngryValue As String)
    
    Dim strModuleNameAndAllSubjectKey As String
    
    strModuleNameAndAllSubjectKey = vstrModuleNameAndSubjectKey & "\" & vstrChildRegKey
    
    SetCurUserRegStrForKeepingFormInputtedState strModuleNameAndAllSubjectKey, vstrEngryValue, mstrUserInputtedTextRegKey
End Sub

'''
''' get user-inputted-text in TextBox from the registry
'''
Public Function GetCurUserRegUserInputtedStringForKeepingFormInputtedState(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vstrChildRegKey As String) As String
    
    Dim strModuleNameAndAllSubjectKey As String, strText As String

    strModuleNameAndAllSubjectKey = vstrModuleNameAndSubjectKey & "\" & vstrChildRegKey
    
    strText = GetCurUserRegStrForKeepingFormInputtedState(strModuleNameAndAllSubjectKey, mstrUserInputtedTextRegKey)

    GetCurUserRegUserInputtedStringForKeepingFormInputtedState = strText
End Function


'**---------------------------------------------
'** Keeps Form control values, which codes dependent on Form class
'**---------------------------------------------
'''
'''
'''
Public Sub SetCurrentFormControlValuesFromUserReg(ByRef rstrModuleName As String, _
        ByRef robjControls As Collection)

    Dim varControl As Object, objTextBox As MSForms.TextBox, objCheckBox As MSForms.CheckBox, objOptionButton As MSForms.OptionButton
    Dim strControlName As String

    For Each varControl In robjControls
    
        If TypeOf varControl Is MSForms.TextBox Then
        
            strControlName = varControl.Name
            
            Set objTextBox = varControl
            
            ' Debug.Print strControlName
            
            SetCurrentFormTextBoxTextToUserReg rstrModuleName, strControlName, objTextBox.Text
            
        ElseIf TypeOf varControl Is MSForms.CheckBox Then
            
            strControlName = varControl.Name
            
            Set objCheckBox = varControl
            
            ' Debug.Print strControlName
            
            SetCurrentFormCheckBoxValueToUserReg rstrModuleName, strControlName, objCheckBox.Value
            
        ElseIf TypeOf varControl Is MSForms.OptionButton Then
            
            strControlName = varControl.Name
            
            Set objOptionButton = varControl
            
            ' Debug.Print strControlName
            
            SetCurrentFormOptionButtonValueToUserReg rstrModuleName, strControlName, objOptionButton.Value
        End If
    Next
End Sub


'''
'''
'''
Public Sub SetCurrentFormControlValuesFromUserRegOfAllInForm(ByRef rstrModuleName As String, _
        ByRef robjForm As MSForms.UserForm)

    Dim varControl As Object, objControlsCol As Collection
    
    Set objControlsCol = New Collection

    For Each varControl In robjForm.Controls
    
        If TypeOf varControl Is MSForms.TextBox Then
        
            objControlsCol.Add varControl
            
        ElseIf TypeOf varControl Is MSForms.CheckBox Then
        
            objControlsCol.Add varControl
            
        ElseIf TypeOf varControl Is MSForms.OptionButton Then
        
            objControlsCol.Add varControl
        End If
    Next
    
    SetCurrentFormControlValuesFromUserReg rstrModuleName, objControlsCol
End Sub


'''
'''
'''
Public Sub GetCurrentFormControlValuesFromUserReg(ByRef rstrModuleName As String, _
        ByRef robjControls As Collection)

    Dim varControl As Object, objTextBox As MSForms.TextBox, objCheckBox As MSForms.CheckBox, objOptionButton As MSForms.OptionButton
    Dim strControlName As String
    Dim strValue As String, blnValue As Boolean, varValue As Boolean

    For Each varControl In robjControls
    
        If TypeOf varControl Is MSForms.TextBox Then
        
            strControlName = varControl.Name
            
            Set objTextBox = varControl
            
            ' Debug.Print varControl.Name
            
            strValue = GetCurrentFormTextBoxTextToUserReg(rstrModuleName, strControlName)
            
            If strValue <> "" Then
            
                objTextBox.Text = strValue
            End If
            
        ElseIf TypeOf varControl Is MSForms.CheckBox Then
            
            strControlName = varControl.Name
            
            Set objCheckBox = varControl
            
            blnValue = GetCurrentFormCheckBoxValueToUserReg(rstrModuleName, strControlName)
            
            objCheckBox.Value = blnValue
            
        ElseIf TypeOf varControl Is MSForms.OptionButton Then
            
            strControlName = varControl.Name
            
            Set objOptionButton = varControl
            
            varValue = GetCurrentFormOptionButtonValueToUserReg(rstrModuleName, strControlName)
            
            objOptionButton.Value = varValue
        End If
    Next
End Sub

'''
''' load text of each form control from registry
'''
Public Sub GetCurrentFormControlValuesFromUserRegOfAllInForm(ByRef rstrModuleName As String, _
        ByRef robjForm As MSForms.UserForm)

    Dim varControl As Object, objControlsCol As Collection
    
    Set objControlsCol = New Collection

    For Each varControl In robjForm.Controls
    
        If TypeOf varControl Is MSForms.TextBox Then
        
            objControlsCol.Add varControl
            
        ElseIf TypeOf varControl Is MSForms.CheckBox Then
        
            objControlsCol.Add varControl
            
        ElseIf TypeOf varControl Is MSForms.OptionButton Then
        
            objControlsCol.Add varControl
        End If
    Next
    
    GetCurrentFormControlValuesFromUserReg rstrModuleName, objControlsCol
End Sub


'''
''' set TextBox.Text to registry
'''
Public Sub SetCurrentFormTextBoxTextToUserReg(ByVal vstrModuleName As String, _
        ByRef rstrEntryName As String, _
        ByRef rstrText As String)

    Dim strFullRegKey As String

    strFullRegKey = strRegKeySecureExcelVBAFormState & "\" & vstrModuleName & "\" & mstrControlTextBoxRegKey
    
    ForceToWriteRegistryValue strFullRegKey, rstrEntryName, rstrText
End Sub

'''
''' get TextBox.Text from registry
'''
Public Function GetCurrentFormTextBoxTextToUserReg(ByVal vstrModuleName As String, _
        ByRef rstrEntryName As String) As String

    Dim strFullRegKey As String

    strFullRegKey = strRegKeySecureExcelVBAFormState & "\" & vstrModuleName & "\" & mstrControlTextBoxRegKey

    GetCurrentFormTextBoxTextToUserReg = ReadStringWinRegistryValue(strFullRegKey, rstrEntryName)
End Function


'''
''' set CheckBox.Value to registry
'''
Public Sub SetCurrentFormCheckBoxValueToUserReg(ByVal vstrModuleName As String, _
        ByRef rstrEntryName As String, _
        ByRef rblnValue As Boolean)

    Dim strFullRegKey As String, intValue As Long

    strFullRegKey = strRegKeySecureExcelVBAFormState & "\" & vstrModuleName & "\" & mstrControlCheckBoxRegKey

    intValue = CInt(rblnValue)
    
    WriteInt32WinRegistryValue strFullRegKey, rstrEntryName, intValue
End Sub

'''
''' get CheckBox.Value from registry
'''
Public Function GetCurrentFormCheckBoxValueToUserReg(ByVal vstrModuleName As String, _
        ByRef rstrEntryName As String) As Boolean

    Dim strFullRegKey As String, intValue As Long

    strFullRegKey = strRegKeySecureExcelVBAFormState & "\" & vstrModuleName & "\" & mstrControlCheckBoxRegKey

    intValue = ReadInt32WinRegistryValue(strFullRegKey, rstrEntryName, CLng(False))

    GetCurrentFormCheckBoxValueToUserReg = CBool(intValue)
End Function


'''
''' set OptionButton.Value to registry
'''
Public Sub SetCurrentFormOptionButtonValueToUserReg(ByVal vstrModuleName As String, _
        ByRef rstrEntryName As String, _
        ByRef rvarValue As Variant)

    Dim strFullRegKey As String, intValue As Long

    strFullRegKey = strRegKeySecureExcelVBAFormState & "\" & vstrModuleName & "\" & mstrControlCheckBoxRegKey

    intValue = CInt(rvarValue)
    
    WriteInt32WinRegistryValue strFullRegKey, rstrEntryName, intValue
End Sub

Public Function GetCurrentFormOptionButtonValueToUserReg(ByVal vstrModuleName As String, _
        ByRef rstrEntryName As String) As Variant


    Dim strFullRegKey As String, intValue As Long

    strFullRegKey = strRegKeySecureExcelVBAFormState & "\" & vstrModuleName & "\" & mstrControlCheckBoxRegKey

    intValue = ReadInt32WinRegistryValue(strFullRegKey, rstrEntryName)

    GetCurrentFormOptionButtonValueToUserReg = intValue
End Function

'**---------------------------------------------
'** Keeps Form Window position, which codes dependent on Form class
'**---------------------------------------------
'''
'''
'''
''' <Argument>vstrModuleName: User defined form module-name</Argument>
''' <Argument>robjForm: Each user defined form, because the MSForms.UserForm doesn't have the property interfaces both of Left and Top</Argument>
Public Sub SetCurrentFormPositionToUserReg(ByVal vstrModuleName As String, _
        ByRef robjForm As Object, _
        Optional ByVal vstrAdditionalKey As String = "")


    Dim strModuleNameAndSubjectKey As String, strFullRegKey As String, intLeft As Long, intTop As Long
    
    
    If vstrAdditionalKey <> "" Then
        
        strModuleNameAndSubjectKey = vstrModuleName & "\" & vstrAdditionalKey & "\" & mstrWindowPositionRegKey
    Else
        strModuleNameAndSubjectKey = vstrModuleName & "\" & mstrWindowPositionRegKey
    End If
    
    strFullRegKey = strRegKeySecureExcelVBAFormState & "\" & strModuleNameAndSubjectKey
    
    With robjForm
    
        intLeft = .Left
        
        intTop = .Top
    End With

    WriteInt32WinRegistryValue strFullRegKey, mstrWindowLeftPositionEntryName, intLeft
    
    WriteInt32WinRegistryValue strFullRegKey, mstrWindowTopPositionEntryName, intTop
End Sub

'''
'''
'''
Public Sub GetCurrentFormPositionFromUserReg(ByVal vstrModuleName As String, _
        ByRef robjForm As Object, _
        Optional ByVal vstrAdditionalKey As String = "")


    Dim strModuleNameAndSubjectKey As String, strFullRegKey As String, intLeft As Long, intTop As Long
    
    If vstrAdditionalKey <> "" Then
        
        strModuleNameAndSubjectKey = vstrModuleName & "\" & vstrAdditionalKey & "\" & mstrWindowPositionRegKey
    Else
        strModuleNameAndSubjectKey = vstrModuleName & "\" & mstrWindowPositionRegKey
    End If

    strFullRegKey = strRegKeySecureExcelVBAFormState & "\" & strModuleNameAndSubjectKey

    If IsWinFullRegKeyExisted(strFullRegKey) Then

        intLeft = ReadInt32WinRegistryValue(strFullRegKey, mstrWindowLeftPositionEntryName)
        
        intTop = ReadInt32WinRegistryValue(strFullRegKey, mstrWindowTopPositionEntryName)
    
        With robjForm
        
            .StartUpPosition = 0    ' This zero "0" means the manual setting
        
            .Left = intLeft
    
            .Top = intTop
        End With
    End If
End Sub

'**---------------------------------------------
'** Keeps Form Window size, which codes dependent on Form class
'**---------------------------------------------
'''
'''
'''
''' <Argument>vstrModuleName: User defined form module-name</Argument>
''' <Argument>robjForm: Each user defined form, because the MSForms.UserForm doesn't have the property interfaces both of Width and Height</Argument>
Public Sub SetCurrentFormSizeToUserReg(ByVal vstrModuleName As String, _
        ByRef robjForm As Object, _
        Optional ByVal vstrAdditionalKey As String = "")


    Dim strModuleNameAndSubjectKey As String, strFullRegKey As String
    
    Dim intWidth As Long, intHeight As Long
    
    If vstrAdditionalKey <> "" Then
        
        strModuleNameAndSubjectKey = vstrModuleName & "\" & vstrAdditionalKey & "\" & mstrWindowPositionRegKey
    Else
        strModuleNameAndSubjectKey = vstrModuleName & "\" & mstrWindowPositionRegKey
    End If
    
    strFullRegKey = strRegKeySecureExcelVBAFormState & "\" & strModuleNameAndSubjectKey
    
    With robjForm
    
        intWidth = .Width
        
        intHeight = .Height
    End With


    WriteInt32WinRegistryValue strFullRegKey, mstrWindowWidthEntryName, intWidth
    
    WriteInt32WinRegistryValue strFullRegKey, mstrWindowHeightEntryName, intHeight
End Sub

'''
'''
'''
Public Sub GetCurrentFormSizeFromUserReg(ByVal vstrModuleName As String, _
        ByRef robjForm As Object, _
        Optional ByVal vstrAdditionalKey As String = "")


    Dim strModuleNameAndSubjectKey As String, strFullRegKey As String
    
    Dim intWidth As Long, intHeight As Long
    
    
    If vstrAdditionalKey <> "" Then
        
        strModuleNameAndSubjectKey = vstrModuleName & "\" & vstrAdditionalKey & "\" & mstrWindowPositionRegKey
    Else
        strModuleNameAndSubjectKey = vstrModuleName & "\" & mstrWindowPositionRegKey
    End If

    strFullRegKey = strRegKeySecureExcelVBAFormState & "\" & strModuleNameAndSubjectKey

    intWidth = ReadInt32WinRegistryValue(strFullRegKey, mstrWindowWidthEntryName)
    
    intHeight = ReadInt32WinRegistryValue(strFullRegKey, mstrWindowHeightEntryName)

    With robjForm
    
        If intWidth > 0 Then
        
            .Width = intWidth
        End If
        
        If intHeight > 0 Then
        
            .Height = intHeight
        End If
    End With
End Sub


'**---------------------------------------------
'** Form Window parenet key managing Set/Get functions
'**---------------------------------------------
'''
'''
'''
Public Sub SetCurUserRegStrForKeepingFormInputtedState(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vstrEntryValue As String, _
        ByVal vstrEntryName As String)
    
    
    ForceToWriteRegistryValue strRegKeySecureExcelVBAFormState & "\" & vstrModuleNameAndSubjectKey, _
            vstrEntryName, _
            vstrEntryValue
End Sub

Public Function GetCurUserRegStrForKeepingFormInputtedState(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vstrEntryName As String) As String
    
    GetCurUserRegStrForKeepingFormInputtedState = ""
    
    Dim strEntryValue As String

    strEntryValue = ""
    
    strEntryValue = ReadStringWinRegistryValue(strRegKeySecureExcelVBAFormState & "\" & vstrModuleNameAndSubjectKey, _
            vstrEntryName)

    If strEntryValue <> "" Then
    
        GetCurUserRegStrForKeepingFormInputtedState = strEntryValue
    End If
End Function


'''
'''
'''
Public Sub SetCurUserRegInt32ForKeepingFormInputtedState(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vintEntryValue As Long, _
        ByVal vstrEntryName As String)
    
    
    ForceToWriteRegistryValue strRegKeySecureExcelVBAFormState & "\" & vstrModuleNameAndSubjectKey, _
            vstrEntryName, _
            vintEntryValue
End Sub

Public Function GetCurUserRegInt32ForKeepingFormInputtedState(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vstrEntryName As String) As String
    
    
    Dim intEntryValue As Long

    intEntryValue = 0
    
    intEntryValue = ReadInt32WinRegistryValue(strRegKeySecureExcelVBAFormState & "\" & vstrModuleNameAndSubjectKey, _
            vstrEntryName)

    
    GetCurUserRegInt32ForKeepingFormInputtedState = intEntryValue
End Function


