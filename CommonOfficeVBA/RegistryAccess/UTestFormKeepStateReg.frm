VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UTestFormKeepStateReg 
   Caption         =   "UTestFormKeepStateReg"
   ClientHeight    =   4020
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   4580
   OleObjectBlob   =   "UTestFormKeepStateReg.frx":0000
End
Attribute VB_Name = "UTestFormKeepStateReg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   Sanity test for including FormTopMostEnabledCtlHdr.cls case
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "UTestFormKeepStateReg"


Private mintInitializing As Long


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub UserForm_Initialize()

    mintInitializing = 0

    Localize

    GetCurrentFormPositionFromUserReg mstrModuleName, Me

    GetCurrentFormControlValuesFromUserRegOfAllInForm mstrModuleName, Me
End Sub

Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)

    SetCurrentFormPositionToUserReg mstrModuleName, Me

    SetCurrentFormControlValuesFromUserRegOfAllInForm mstrModuleName, Me
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////

Public Sub Localize()

    mintInitializing = mintInitializing + 1

    Me.Caption = "Unit test to keep form control states"    ' ... Request to see the result on the screen.

    lblKeepsTextBoxValue.Caption = "The next text is kept when the Form close"

    chkTestToKeepCheckedState.Caption = "Keeping checked state when the Form close"

    frmTestFrame.Caption = "Test to keep the selected option state"

    optTest01.Caption = "Test option 01"
    
    optTest02.Caption = "Test option 02"
    
    optTest03.Caption = "Test option 03"

    mintInitializing = mintInitializing - 1
End Sub



