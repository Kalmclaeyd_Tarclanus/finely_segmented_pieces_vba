Attribute VB_Name = "WinRegStorageUNCPath"
'
'   logic definitions for searching or deciding UNC path
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 12/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjModuleNameAndSubjectKeyToCurrentPathDic As Scripting.Dictionary


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' clear UNC path caches
'''
Public Sub ClearCacheOfModuleNameAndSubjectKeyToCurrentPathDic()

    ' for Test
    If Not mobjModuleNameAndSubjectKeyToCurrentPathDic Is Nothing Then
    
        mobjModuleNameAndSubjectKeyToCurrentPathDic.RemoveAll
    
        Set mobjModuleNameAndSubjectKeyToCurrentPathDic = Nothing
    End If
End Sub


'''
'''
'''
Private Function mfobjGetModuleNameAndSubjectKeyToCurrentPathDic() As Scripting.Dictionary

    If mobjModuleNameAndSubjectKeyToCurrentPathDic Is Nothing Then
    
        Set mobjModuleNameAndSubjectKeyToCurrentPathDic = New Scripting.Dictionary
    End If

    Set mfobjGetModuleNameAndSubjectKeyToCurrentPathDic = mobjModuleNameAndSubjectKeyToCurrentPathDic
End Function


'''
''' get UNC-path from a sub-key of Windows registry HKEY_CURRENT_USER root
'''
''' <Argument>vstrModuleNameAndSubjectKey: subkey of module-name and subject-title delimitated by '\'(back-slash) </Argument>
Public Function GetUNCPathToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String) As String
    
    Dim intCountOfRegisterdPaths As Long, i As Long, strPath As String, objDrive As Scripting.Drive
    
    intCountOfRegisterdPaths = 0
    
    strPath = ""
    
    ' If this computer has plural hard-drives and the first prior path is a removable USB hard-drive and the hard-drive doesn't exist, the second prior path is selected.
    ' The plural prior paths permanently remains in Windows registry at CURRENT_USER. When the path is requested, this checks the state of each hard-drive by prior order, and the checked state keeps in the cache (static variable)
    ' The cache is to be cleard when the Office application is closed
    ' After it is confirmed that the dirve of the first prior path is ready at one, then drive ready state is not checked while the Office application runs.
    With mfobjGetModuleNameAndSubjectKeyToCurrentPathDic()
    
        If .Exists(vstrModuleNameAndSubjectKey) Then
        
            strPath = .Item(vstrModuleNameAndSubjectKey)
        End If
    End With
    
    If strPath = "" Then
    
        On Error Resume Next
        
        intCountOfRegisterdPaths = GetCurUserRegInt32ForDecidingUNCPath(vstrModuleNameAndSubjectKey, "CountOfUNCPaths")
        
        On Error GoTo 0
        
        If intCountOfRegisterdPaths > 1 Then
        
            With New Scripting.FileSystemObject
            
                For i = 1 To intCountOfRegisterdPaths
            
                    strPath = ""
            
                    strPath = GetCurUserRegStrForDecidingUNCPath(vstrModuleNameAndSubjectKey, "Path" & Format(i, "00"))
                
                    If .DriveExists(.GetDriveName(strPath)) Then
                    
                        Set objDrive = .GetDrive(.GetDriveName(strPath))
                    
                        If objDrive.IsReady Then    ' If the drive is encrypted by the Bit-Locker and is locked, The IsReady proparty should has been return False...
                    
                            Exit For
                        End If
                    End If
                Next
            End With
        Else
            strPath = GetCurUserRegStrForDecidingUNCPath(vstrModuleNameAndSubjectKey, "Path")
        End If
        
        If strPath <> "" Then
        
            With mfobjGetModuleNameAndSubjectKeyToCurrentPathDic()
            
                .Add vstrModuleNameAndSubjectKey, strPath
            End With
        End If
    End If
    
    GetUNCPathToCurUserReg = strPath
End Function

'''
''' set UNC-path from a sub-key of Windows registry HKEY_CURRENT_USER root
'''
''' support plural candidates
'''
''' <Argument>vstrModuleNameAndSubjectKey: subkey of module-name and subject-title delimitated by '\'(back-slash) </Argument>
''' <Argument>vstrPathsDelimitedSemiColon: if more then one paths are set, the paths have to be delimitered by semi-coron ';'. </Argument>
Public Sub SetUNCPathToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String, ByVal vstrPathsDelimitedSemiColon As String)

    Dim strPaths() As String, i As Long, intCountOfRegisterdPaths As Long
    
    strPaths = Split(vstrPathsDelimitedSemiColon, ";")
    

    intCountOfRegisterdPaths = 0

    intCountOfRegisterdPaths = ReadInt32WinRegistryValue(vstrModuleNameAndSubjectKey, "CountOfUNCPaths")

    If intCountOfRegisterdPaths < 0 Then
    
        SetCurUserRegInt32ForDecidingUNCPathSingle vstrModuleNameAndSubjectKey, UBound(strPaths) + 1, "CountOfUNCPaths"
        
        intCountOfRegisterdPaths = UBound(strPaths) + 1
    End If

    For i = LBound(strPaths) To UBound(strPaths)
    
        SetCurUserRegStrForDecidingUNCPath vstrModuleNameAndSubjectKey, Trim(strPaths(i)), "Path", i + 1, intCountOfRegisterdPaths
    Next
End Sub


'**---------------------------------------------
'** general string loading tools
'**---------------------------------------------
'''
''' manage to get UNC path with RegParamOfStorageUNCPath object
'''
Public Function ManageToGetUNCPathFromCurReg(ByVal vstrModuleNameAndSubjectKey As String, ByVal vobjRegParamOfStorageUNCPathCol As Collection) As String
    
    Dim strTargetPath As String, objPriorOrderToTargetPathDic As Scripting.Dictionary
    
    strTargetPath = "" ' GetUNCPathToCurUserReg(vstrModuleNameAndSubjectKey)
    
    If strTargetPath = "" Then
        
        ' Set UNC path searching parameters to Windows 32 registry
        InitializeLoadingPathFromCurReg vstrModuleNameAndSubjectKey, vobjRegParamOfStorageUNCPathCol
        
        msubSearchUNCPathAtCurrentComputerState objPriorOrderToTargetPathDic, vstrModuleNameAndSubjectKey, vobjRegParamOfStorageUNCPathCol
    End If
    
    strTargetPath = mfstrGetTargetPathFromPriorOrderToTargetPathDic(objPriorOrderToTargetPathDic)
    
    ManageToGetUNCPathFromCurReg = strTargetPath
End Function

'''
''' register the paths
'''
''' <Argument>robjPriorOrderToTargetPathDic: output </Argument>
Private Sub msubSearchUNCPathAtCurrentComputerState(ByRef robjPriorOrderToTargetPathDic As Scripting.Dictionary, _
        ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vobjRegParamOfStorageUNCPathCol As Collection)
    
    
    Dim objRegParamOfStorageUNCPath As RegParamOfStorageUNCPath, varRegParamOfStorageUNCPath As Variant
    Dim enmStorageSearchFlag As StoragePathSearchingFlag, strTargetPath As String
    Dim i As Long, intCountOfUNCPaths As Long
    
    
    If robjPriorOrderToTargetPathDic Is Nothing Then Set robjPriorOrderToTargetPathDic = New Scripting.Dictionary
    
    intCountOfUNCPaths = vobjRegParamOfStorageUNCPathCol.Count
    
    i = 1
    
    For Each varRegParamOfStorageUNCPath In vobjRegParamOfStorageUNCPathCol
    
        Set objRegParamOfStorageUNCPath = varRegParamOfStorageUNCPath

        enmStorageSearchFlag = GetUNCPathStoragePathSearchingFlagToCurUserReg(vstrModuleNameAndSubjectKey, i, intCountOfUNCPaths)
        
        ' is there DNS server setting in this computer?
        '<Mock operation>
        
        strTargetPath = ""
        
        If (enmStorageSearchFlag And FromNetworkDrive) > 0 Then
        
            ' If SolvePathByPlugInInterface is a implemented function and a public interface of this VB project, the target-path is the return value of the function
            ' the implemented function should be separated to other souce file (.bas).
        
            strTargetPath = mfstrGetTargetPathFromNetworkPath(vstrModuleNameAndSubjectKey, enmStorageSearchFlag, i, intCountOfUNCPaths, objRegParamOfStorageUNCPath.SolveNetworkHostAndDirectoryPathByPlugInInterface)
        End If

        If strTargetPath = "" And ((enmStorageSearchFlag And FromLocalDrive) > 0) Then
        
            strTargetPath = mfstrGetTargetPathFromLocalDrive(vstrModuleNameAndSubjectKey, enmStorageSearchFlag, i, intCountOfUNCPaths, objRegParamOfStorageUNCPath.SolveLocalDriveLetterByPlugInInterface)
        End If
        
        If strTargetPath <> "" Then
    
            robjPriorOrderToTargetPathDic.Add i, strTargetPath
        End If

        i = i + 1
    Next

    msubSetUpNewTargetPath robjPriorOrderToTargetPathDic, vstrModuleNameAndSubjectKey, vobjRegParamOfStorageUNCPathCol
End Sub



'''
''' After initialize target-path string and create new file, if it is needed.
'''
Private Sub msubSetUpNewTargetPath(ByVal vobjPriorOrderToTargetPathDic As Scripting.Dictionary, _
        ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vobjRegParamOfStorageUNCPathCol As Collection)


    Dim strTargetPaths As String, strPath As String, objFS As Scripting.FileSystemObject
    Dim intOrder As Long, varOrder As Variant
    Dim objRegParamOfStorageUNCPath As RegParamOfStorageUNCPath, varRegParamOfStorageUNCPath As Variant, blnIsFirstItemCreated As Boolean
    Dim i As Long

    blnIsFirstItemCreated = False

    With vobjPriorOrderToTargetPathDic
    
        strTargetPaths = ""
    
        i = 1
    
        For Each varOrder In .Keys
        
            intOrder = varOrder
            
            strPath = .Item(varOrder)
        
            If Not blnIsFirstItemCreated Then
            
                If objFS Is Nothing Then Set objFS = New Scripting.FileSystemObject
            
                With objFS
                
                    If .DriveExists(.GetDriveName(strPath)) Then
                    
                        Set objRegParamOfStorageUNCPath = vobjRegParamOfStorageUNCPathCol.Item(intOrder)
                    
                        If objRegParamOfStorageUNCPath.IsRecommendedToCreateNewWhenItDoesNotExist Then
                        
                            CreateDefaultNewFileWhenItIsNeeded strPath, objRegParamOfStorageUNCPath
                            
                            blnIsFirstItemCreated = True
                        End If
                    End If
                End With
            End If
        
            If i = 1 Then
            
                strTargetPaths = strPath
            Else
                strTargetPaths = strTargetPaths & ";" & strPath
            End If
            
            i = i + 1
        Next
    End With

    SetUNCPathToCurUserReg vstrModuleNameAndSubjectKey, strTargetPaths
End Sub


'''
'''
'''
Private Function mfstrGetTargetPathFromPriorOrderToTargetPathDic(ByVal vobjPriorOrderToTargetPathDic As Scripting.Dictionary) As String

    Dim intPriorOrder As Long, varPriorOrder As Variant, strPath As String
    Dim objFS As Scripting.FileSystemObject, objDrive As Scripting.Drive

    With vobjPriorOrderToTargetPathDic
    
        For Each varPriorOrder In .Keys
        
            strPath = .Item(varPriorOrder)
    
            If objFS Is Nothing Then Set objFS = New Scripting.FileSystemObject
    
            With objFS
            
                If .DriveExists(.GetDriveName(strPath)) Then
                
                    Set objDrive = .GetDrive(.GetDriveName(strPath))
                    
                    If objDrive.IsReady Then
                    
                        Debug.Print "Drive '" & .GetDriveName(strPath) & "' is ready."
                    
                        Exit For
                    End If
                End If
            End With
        Next
    End With

    mfstrGetTargetPathFromPriorOrderToTargetPathDic = strPath
End Function


'''
'''
'''
Private Function mfstrGetTargetPathFromLocalDrive(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal venmStorageSearchFlag As StoragePathSearchingFlag, _
        ByVal vintPriorOrder As Long, ByVal vintMaxOrder As Long, _
        Optional ByVal vstrSolvePathByPlugInInterface As String = "") As String
    
    Dim strTargetPath As String, strLocalDriveLetter As String, strFoundDirectoryPath As String
    
    
    strTargetPath = ""
    
    strLocalDriveLetter = ""
    
    If vstrSolvePathByPlugInInterface <> "" Then
    
        strLocalDriveLetter = GetStringFromCallBackInterface(vstrSolvePathByPlugInInterface)
    End If
    
    If strLocalDriveLetter = "" Then
    
        strLocalDriveLetter = GetWorkingLocalHardDriveLetterPriorToExcludingSystemDrive()
    End If
    
    strFoundDirectoryPath = strLocalDriveLetter & ":\" & GetLocalPriorSubPathToCurUserReg(vstrModuleNameAndSubjectKey, vintPriorOrder, vintMaxOrder)
    
    ' find local drive
    If (venmStorageSearchFlag And PathIsDirectory) > 0 Then
        
        ' directory path
        strTargetPath = strFoundDirectoryPath
    Else
        ' file path (UNC Path)
        strTargetPath = strFoundDirectoryPath & "\" & GetDefaultFileNameToCurUserReg(vstrModuleNameAndSubjectKey, vintPriorOrder, vintMaxOrder)
    End If

    mfstrGetTargetPathFromLocalDrive = strTargetPath
End Function

'''
'''
'''
Private Function mfstrGetTargetPathFromNetworkPath(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal venmStorageSearchFlag As StoragePathSearchingFlag, _
        ByVal vintPriorOrder As Long, _
        ByVal vintMaxOrder As Long, _
        Optional ByVal vstrSolvePathByPlugInInterface As String = "") As String
    
    
    Dim strTargetPath As String, strHostAndDirectoryPath As String, strFoundDirectoryPath As String
    
    strTargetPath = ""
    
    If vstrSolvePathByPlugInInterface <> "" Then
        
        ' solve path by plug-in interface
        strTargetPath = GetStringFromCallBackInterfaceAndOneArgument(vstrSolvePathByPlugInInterface, "")
    End If
    
    If "" = strTargetPath Then
    
        ' If 'FindNetworkHostNameAndDirectoryPath' is implemented in each user VB project module, get the target network UNC directory path in each user computer environment
    
        strHostAndDirectoryPath = GetStringFromCallBackInterface("FindThisComputerNetworkHostNameAndDirectoryPath")
        
        If strHostAndDirectoryPath <> "" Then
        
            strFoundDirectoryPath = strHostAndDirectoryPath & "\" & GetNetworkCommonFirstSubDirectoryPath(strHostAndDirectoryPath) & "\" & GetNetworkPriorSubPathToCurUserReg(vstrModuleNameAndSubjectKey, vintPriorOrder, vintMaxOrder)
            
            If (venmStorageSearchFlag And PathIsDirectory) > 0 Then
                
                ' directory path
                strTargetPath = strFoundDirectoryPath
            Else
                ' load default file name
                strTargetPath = strFoundDirectoryPath & "\" & GetDefaultFileNameToCurUserReg(vstrModuleNameAndSubjectKey, vintPriorOrder, vintMaxOrder)
            End If
        End If
    End If
    
    mfstrGetTargetPathFromNetworkPath = strTargetPath
End Function


'''
'''
'''
Public Sub CreateDefaultNewFileWhenItIsNeeded(ByVal vstrTargetPath As String, _
        ByVal vobjRegParamOfStorageUNCPath As RegParamOfStorageUNCPath)
    
    
    Dim objFS As Scripting.FileSystemObject
    
    Set objFS = New Scripting.FileSystemObject
    
    With vobjRegParamOfStorageUNCPath
        
        If .IsRecommendedToCreateNewWhenItDoesNotExist And ((.StorageSearchMode And PathIsDirectory) = 0) Then
        
            If Not objFS.FileExists(vstrTargetPath) Then
                
                ForceToCreateDirectory objFS.GetParentFolderName(vstrTargetPath)
                
                If objFS.FolderExists(objFS.GetParentFolderName(vstrTargetPath)) Then
                
                    Select Case LCase(objFS.GetExtensionName(vstrTargetPath))
                    
                        Case "xls", "xlsx", "xlsm"
                            
                            On Error Resume Next
                            
                            Application.Run "CreateDefaultWorkbookAndCloseIfItDoesntExist", vstrTargetPath
                            
                            On Error GoTo 0
                        Case "txt"
                            
                            With objFS.CreateTextFile(vstrTargetPath, True)
                                
                                .WriteLine "Auto-created at " & Format(Now(), "yyyy/mm/dd hh:mm:dd")
                                
                                .Close
                            End With
                    End Select
                End If
            End If
        End If
    End With
End Sub

'''
'''
'''
Private Sub msubSepareteModuleNameAndSubjectKey(ByRef rstrModuleNameKey As String, _
        ByRef rstrSubjectTitleKey As String, _
        ByVal vstrModuleNameAndSubjectKey As String)
    
    
    Dim intP1 As Integer
    
    intP1 = InStr(1, vstrModuleNameAndSubjectKey, "\")
    
    rstrModuleNameKey = Left(vstrModuleNameAndSubjectKey, intP1 - 1)
    
    rstrSubjectTitleKey = Right(vstrModuleNameAndSubjectKey, Len(vstrModuleNameAndSubjectKey) - intP1)
End Sub



'''
''' Initialize the UNC path for each data purpose
'''
Public Sub InitializeLoadingPathFromCurReg(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vobjRegParamOfStorageUNCPathCol As Collection)
    
    
    Dim objRegParamOfStorageUNCPath As RegParamOfStorageUNCPath, varRegParamOfStorageUNCPath As Variant
    Dim i As Long, intCountOfUNCPaths As Long
    
    
    intCountOfUNCPaths = vobjRegParamOfStorageUNCPathCol.Count
    
    SetCurUserRegInt32ForDecidingUNCPathSingle vstrModuleNameAndSubjectKey, intCountOfUNCPaths, "CountOfUNCPaths"
    
    i = 1
    
    For Each varRegParamOfStorageUNCPath In vobjRegParamOfStorageUNCPathCol
        
        Set objRegParamOfStorageUNCPath = varRegParamOfStorageUNCPath
        
        With objRegParamOfStorageUNCPath
        
            SetUNCPathStoragePathSearchingFlagToCurUserReg vstrModuleNameAndSubjectKey, .StorageSearchMode, i, intCountOfUNCPaths
    
            If .LocalPriorSubPath <> "" Then
            
                SetCurUserRegStrForDecidingUNCPath vstrModuleNameAndSubjectKey, .LocalPriorSubPath, "LocalPriorSubPath", i, intCountOfUNCPaths
            End If
            
            If .NetworkPriorSubPath <> "" Then
            
                SetCurUserRegStrForDecidingUNCPath vstrModuleNameAndSubjectKey, .NetworkPriorSubPath, "NetworkPriorSubPath", i, intCountOfUNCPaths
            End If
            
            If .DefaultFileName <> "" Then
            
                SetCurUserRegStrForDecidingUNCPath vstrModuleNameAndSubjectKey, .DefaultFileName, "DefaultFileName", i, intCountOfUNCPaths
            End If
            
            If .IsRecommendedToCreateNewWhenItDoesNotExist Then
            
                SetCurUserRegInt32ForDecidingUNCPath vstrModuleNameAndSubjectKey, 1, "IsRecommendedToCreateNewWhenItDoesNotExist", i, intCountOfUNCPaths
            End If
        End With
        
        i = i + 1
    Next
End Sub

'''
'''
'''
Public Sub InitializeHostNameAndDirForDicidingUNCPath(ByVal vstrHostName As String, _
        ByVal vstrSharingDirectoryName As String, _
        ByVal vintSettingNumber As Long)
    
    Dim strHostAndDir As String, strEntryName As String
    
    strHostAndDir = "\\" & vstrHostName & "\" & vstrSharingDirectoryName

    strEntryName = "NetworkStorageHostAndDir" & Format(vintSettingNumber, "00")
    
    ForceToWriteRegistryValue strRegKeySecureExcelVBAUNCPathPos, strEntryName, strHostAndDir
End Sub

'''
'''
'''
Public Sub DeleteHostNameAndDirForDicidingUNCPath(ByVal vintSettingNumber As Long)

    Dim strEntryName As String
    
    strEntryName = "NetworkStorageHostAndDir" & Format(vintSettingNumber, "00")
    
    DeleteWinRegistryValueInKey strRegKeySecureExcelVBAUNCPathPos, strEntryName
End Sub

'''
'''
'''
Public Function GetHostNameToDirFromCurUserReg(ByVal vintSettingNumber As Long) As String

    Dim strEntryName As String

    strEntryName = "NetworkStorageHostAndDir" & Format(vintSettingNumber, "00")

    GetHostNameToDirFromCurUserReg = ReadStringWinRegistryValue(strRegKeySecureExcelVBAUNCPathPos, strEntryName)
End Function

'''
''' get sub-path for working generaly
''' If other .bas file has 'GetPlugInCommonSubDirectoryPath' function, it decide the sub-directory path, if there is no plug-in function in this project, return the default path
'''
Public Function GetLocalCommonSubDirectoryPath(ByVal vstrDriveLetter As String) As String
    
    Dim strSubPath As String
    
    strSubPath = ""
    
    strSubPath = GetStringFromCallBackInterfaceAndOneArgument("GetPlugInCommonSubDirectoryPath", vstrDriveLetter)
    
    If "" = strSubPath Then
    
        ' decide the following default path
        With New WshNetwork
            
            strSubPath = "VBAWorks\" & .UserName
        End With
    End If

    GetLocalCommonSubDirectoryPath = strSubPath
End Function

'''
''' get sub-path for repositories, such as Git
'''
Public Function GetLocalCommonRepositorySubDirectoryPath(ByVal vstrDriveLetter As String) As String
    
    Dim strSubPath As String
    
    strSubPath = ""
    
    strSubPath = GetStringFromCallBackInterfaceAndOneArgument("GetPlugInCommonVbaRepositorySubDirectoryPath", vstrDriveLetter)
    
    If "" = strSubPath Then
    
        ' decide the following default path
        
        With New WshNetwork
            
            strSubPath = "VBAWorks\" & .UserName & "\Repositories"
        End With
    End If

    GetLocalCommonRepositorySubDirectoryPath = strSubPath
End Function

'''
''' For example, C:\tmp, D:\tmp
'''
Public Function GetLocalTemporarySubDirectoryPath() As String
    
    Dim strSubPath As String
    
    strSubPath = ""
    
    strSubPath = GetStringFromCallBackInterface("GetPlugInTemporarySubDirectoryPath")
    
    If "" = strSubPath Then
    
        ' decide the following default path
        
        With New WshNetwork
            
            strSubPath = "tmp\VBAWorks\" & .UserName
        End With
    End If

    GetLocalTemporarySubDirectoryPath = strSubPath
End Function


'''
''' Need to separate
'''
''' It is dependent on the Production Division network attached storage system
'''
Public Function GetNetworkCommonFirstSubDirectoryPath(ByVal vstrHostAndDir As String) As String
    
    Dim strFirstSubDir As String
        
    strFirstSubDir = GetStringFromCallBackInterfaceAndOneArgument("GetNetworkCommonFirstSubDirectoryPathPlugIn", vstrHostAndDir)
    
    If strFirstSubDir = "" Then
    
        strFirstSubDir = mfstrGetNetworkCommonFirstSubDirectoryPath(vstrHostAndDir)
    End If
    
    GetNetworkCommonFirstSubDirectoryPath = strFirstSubDir
End Function

'''
''' default algorithm
'''
Private Function mfstrGetNetworkCommonFirstSubDirectoryPath(ByVal vstrHostAndDir As String) As String

    Dim strDirPath As String, strFirstSubDir As String, strUserName As String
    
    With New WshNetwork
    
        strUserName = .UserName
    End With

    strFirstSubDir = ""
    
    With New Scripting.FileSystemObject
    
        If .FolderExists(vstrHostAndDir) Then
        
             strDirPath = vstrHostAndDir & "\" & strUserName
        
            If .FolderExists(strDirPath) Then
            
                strFirstSubDir = strDirPath
            Else
                strFirstSubDir = "VBAWork" & Format(Now(), "yyyymmdd")
            End If
        End If
    End With
    
    mfstrGetNetworkCommonFirstSubDirectoryPath = strFirstSubDir
End Function


'**---------------------------------------------
'** basic tools
'**---------------------------------------------
Public Sub DeleteUNCPathToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String)
 
    DeleteCurUserRegStrForDecidingUNCPath vstrModuleNameAndSubjectKey, "Path"
End Sub
Public Sub DeleteUNCPathAllToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String)

    DeleteRegistryKeyWithAllChildKeys strRegKeySecureExcelVBAUNCPathPos & "\" & vstrModuleNameAndSubjectKey
End Sub

Public Sub SetInt32StopSearchingPathToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String, ByVal vintValue As Long)

    SetCurUserRegInt32ForDecidingUNCPath vstrModuleNameAndSubjectKey, vintValue, "StopSearchingPath", 1, 1
End Sub


Public Function GetInt32StopSearchingPathToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String) As Long

     GetInt32StopSearchingPathToCurUserReg = ReadInt32WinRegistryValue(vstrModuleNameAndSubjectKey, "StopSearchingPath")
End Function



Public Function GetUNCGeneralSubPathToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vstrEntryBaseName As String, _
        ByVal vintPriorOrder As Long, _
        ByVal vintMaxOrder As Long) As String
    
    Dim strEntryName As String

    strEntryName = vstrEntryBaseName
    
    If vintMaxOrder > 1 Then
    
        strEntryName = strEntryName & Format(vintPriorOrder, "00")
    End If
    
    GetUNCGeneralSubPathToCurUserReg = GetCurUserRegStrForDecidingUNCPath(vstrModuleNameAndSubjectKey, strEntryName)
End Function

'''
''' get network-prior-sub-path from a sub-key of Windows registry HKEY_CURRENT_USER root
'''
''' <Argument>vstrModuleNameAndSubjectKey: subkey of module-name and subject-title delimitated by '\'(back-slash) </Argument>
Public Function GetNetworkPriorSubPathToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vintPriorOrder As Long, _
        ByVal vintMaxOrder As Long) As String
    
    
    GetNetworkPriorSubPathToCurUserReg = GetUNCGeneralSubPathToCurUserReg(vstrModuleNameAndSubjectKey, _
            "NetworkPriorSubPath", _
            vintPriorOrder, _
            vintMaxOrder)
End Function

'''
''' get local-drive-prior-sub-path from a sub-key of Windows registry HKEY_CURRENT_USER root
'''
''' <Argument>vstrModuleNameAndSubjectKey: subkey of module-name and subject-title delimitated by '\'(back-slash) </Argument>
Public Function GetLocalPriorSubPathToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vintPriorOrder As Long, _
        ByVal vintMaxOrder As Long) As String


    GetLocalPriorSubPathToCurUserReg = GetUNCGeneralSubPathToCurUserReg(vstrModuleNameAndSubjectKey, _
            "LocalPriorSubPath", _
            vintPriorOrder, _
            vintMaxOrder)
End Function


Public Function GetDefaultFileNameToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vintPriorOrder As Long, _
        ByVal vintMaxOrder As Long) As String


    GetDefaultFileNameToCurUserReg = GetUNCGeneralSubPathToCurUserReg(vstrModuleNameAndSubjectKey, _
            "DefaultFileName", _
            vintPriorOrder, _
            vintMaxOrder)
End Function


Public Sub DeleteCurUserRegStrForDecidingUNCPath(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vstrEntryName As String)
    
    
    DeleteWinRegistryValueInKey strRegKeySecureExcelVBAUNCPathPos & "\" & vstrModuleNameAndSubjectKey, _
            vstrEntryName
End Sub

Public Function GetCurUserRegStrForDecidingUNCPath(ByVal vstrModuleNameAndSubjectKey As String, ByVal vstrEntryName As String) As String
    
    GetCurUserRegStrForDecidingUNCPath = ""
    
    Dim strFullRegKey As String, strEntryValue As String


    strFullRegKey = strRegKeySecureExcelVBAUNCPathPos & "\" & vstrModuleNameAndSubjectKey

    strEntryValue = ""
    
    strEntryValue = ReadStringWinRegistryValue(strFullRegKey, vstrEntryName)

    If strEntryValue <> "" Then
    
        GetCurUserRegStrForDecidingUNCPath = strEntryValue
    End If
End Function


Public Function GetCurUserRegInt32ForDecidingUNCPath(ByVal vstrModuleNameAndSubjectKey As String, ByVal vstrEntryName As String) As Long
    
    GetCurUserRegInt32ForDecidingUNCPath = 0
    
    Dim strFullRegKey As String, intEntryValue As Long

    strFullRegKey = strRegKeySecureExcelVBAUNCPathPos & "\" & vstrModuleNameAndSubjectKey

    intEntryValue = 0
    
    intEntryValue = ReadInt32WinRegistryValue(strFullRegKey, vstrEntryName)

    If intEntryValue <> 0 Then
    
        GetCurUserRegInt32ForDecidingUNCPath = intEntryValue
    End If
End Function


'''
''' set StoragePathSearchingFlag of the spcified UNC-path from a sub-key of Windows registry HKEY_CURRENT_USER root
'''
''' <Argument>vstrModuleNameAndSubjectKey: subkey of module-name and subject-title delimitated by '\'(back-slash) </Argument>
Public Sub SetUNCPathStoragePathSearchingFlagToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal venmStoragePathSearchingFlag As StoragePathSearchingFlag, _
        ByVal vintPriorOrder As Long, _
        ByVal vintMaxOrder As Long)


    Dim strEntryName As String

    strEntryName = "StorageSearchFlag"
    
    If vintMaxOrder > 1 Then
    
        strEntryName = strEntryName & Format(vintPriorOrder, "00")
    End If

    ForceToWriteRegistryValue strRegKeySecureExcelVBAUNCPathPos & "\" & vstrModuleNameAndSubjectKey, strEntryName, CLng(venmStoragePathSearchingFlag)
End Sub


Public Sub SetCurUserRegStrForDecidingUNCPath(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vstrEntryValue As String, _
        ByVal vstrEntryBaseName As String, _
        ByVal vintPriorOrder As Long, _
        ByVal vintMaxOrder As Long)
    
    
    Dim strEntryName As String

    strEntryName = vstrEntryBaseName
    
    If vintMaxOrder > 1 Then
    
        strEntryName = strEntryName & Format(vintPriorOrder, "00")
    End If
    
    ForceToWriteRegistryValue strRegKeySecureExcelVBAUNCPathPos & "\" & vstrModuleNameAndSubjectKey, strEntryName, vstrEntryValue
End Sub


Public Sub SetCurUserRegInt32ForDecidingUNCPath(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vintEntryValue As Long, _
        ByVal vstrEntryBaseName As String, _
        ByVal vintPriorOrder As Long, _
        ByVal vintMaxOrder As Long)
    
    Dim strEntryName As String

    strEntryName = vstrEntryBaseName
    
    If vintMaxOrder > 1 Then
    
        strEntryName = strEntryName & Format(vintPriorOrder, "00")
    End If
    
    ForceToWriteRegistryValue strRegKeySecureExcelVBAUNCPathPos & "\" & vstrModuleNameAndSubjectKey, strEntryName, vintEntryValue
End Sub

Public Sub SetCurUserRegInt32ForDecidingUNCPathSingle(ByVal vstrModuleNameAndSubjectKey As String, ByVal vintEntryValue As Long, ByVal vstrEntryName As String)
    
    ForceToWriteRegistryValue strRegKeySecureExcelVBAUNCPathPos & "\" & vstrModuleNameAndSubjectKey, vstrEntryName, vintEntryValue
End Sub

'''
''' get StoragePathSearchingFlag of the spcified UNC-path from a sub-key of Windows registry HKEY_CURRENT_USER root
'''
''' <Argument>vstrModuleNameAndSubjectKey: subkey of module-name and subject-title delimitated by '\'(back-slash) </Argument>
Public Function GetUNCPathStoragePathSearchingFlagToCurUserReg(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vintPriorOrder As Long, _
        ByVal vintMaxOrder As Long) As StoragePathSearchingFlag
    
    
    Dim strFullRegKey As String
    Dim enmStoragePathSearchingFlag As StoragePathSearchingFlag, strEntryName As String
    
    strFullRegKey = strRegKeySecureExcelVBAUNCPathPos & "\" & vstrModuleNameAndSubjectKey


    strEntryName = "StorageSearchFlag"
    
    If vintMaxOrder > 1 Then
    
        strEntryName = strEntryName & Format(vintPriorOrder, "00")
    End If

    enmStoragePathSearchingFlag = ReadInt32WinRegistryValue(strFullRegKey, strEntryName)
    
    GetUNCPathStoragePathSearchingFlagToCurUserReg = enmStoragePathSearchingFlag
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' simple unittest
'''
Private Sub msubUnitTestOfSepareteModuleNameAndSubjectKey()

    Dim strModuleNameAndSubjectKey As String, strSubjectTitleKey As String
    
    msubSepareteModuleNameAndSubjectKey strModuleNameAndSubjectKey, strSubjectTitleKey, "A\B"

    Debug.Print strModuleNameAndSubjectKey & ", " & strSubjectTitleKey
End Sub
