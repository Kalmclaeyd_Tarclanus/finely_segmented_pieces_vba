Attribute VB_Name = "WinRegGeneral"
'
'   Windows registory utilities
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  1/Sep/2016    Atsushi Oomura          Disclosed at https://www.shuwasystem.co.jp/support/7980html/4734.html
'       Tue, 31/May/2022    Kalmclaeyd Tarclanus    Refactoring
'

Option Explicit

'///////////////////////////////////////////////
'/// Private WinAPI Definitions
'///////////////////////////////////////////////
'**---------------------------------------------
'** WinAPI private constants
'**---------------------------------------------
' Root keys
Private Const HKEY_CLASSES_ROOT = &H80000000    ' key contains file name extension associations and COM class registration information such as ProgIDs, CLSIDs, and IIDs
Private Const HKEY_CURRENT_USER = &H80000001    ' Registry entries subordinate to this key define the preferences of the current user. These preferences include the settings of environment variables, data about program groups, colors, printers, network connections, and application preferences
Private Const HKEY_LOCAL_MACHINE = &H80000002   ' Registry entries subordinate to this key define the physical state of the computer, including data about the bus type, system memory, and installed hardware and software
Private Const HKEY_USERS = &H80000003   ' Registry entries subordinate to this key define the default user configuration for new users on the local computer and the user configuration for the current user
Private Const HKEY_CURRENT_CONFIG = &H80000005  ' Contains information about the current hardware profile of the local computer system. The information under HKEY_CURRENT_CONFIG describes only the differences between the current hardware configuration and the standard configuration.
Private Const HKEY_DYN_DATA = &H80000006    ' Only for Windows 95/98, Contains Windows Plug&Play information, these are included in HKEY_LOCAL_MACHINE


Private Const REG_OPTION_VOLATILE = 1   ' All keys created by the function are volatile. The information is stored in memory and is not preserved when the corresponding registry hive is unloaded. For HKEY_LOCAL_MACHINE, this occurs only when the system initiates a full shutdown. For registry keys loaded by the RegLoadKey function, this occurs when the corresponding RegUnLoadKey is performed.
Private Const REG_OPTION_NON_VOLATILE = 0    ' save setting parameters to registry; This key is not volatile; this is the default. The information is stored in a file and is preserved when the system is restarted. The RegSaveKey function saves keys that are not volatile.



Private Const KEY_QUERY_VALUE = &H1&         ' query a value of registry
Private Const KEY_SET_VALUE = &H2&           ' set a value of registry
Private Const KEY_CREATE_SUB_KEY = &H4&      ' create sub key
Private Const KEY_ENUMERATE_SUB_KEYS = &H8&  ' enumerate sub keys
Private Const KEY_NOTIFY = &H10&             ' request to notify the change of the registry
Private Const KEY_CREATE_LINK = &H20&        ' create a link of the key object

Private Const KEY_READ = KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY
Private Const KEY_WRITE = KEY_SET_VALUE Or KEY_CREATE_SUB_KEY
Private Const KEY_ALL_ACCESS = KEY_READ Or KEY_WRITE Or KEY_CREATE_LINK
'Private Const KEY_ALL_ACCESS = ((STANDARD_RIGHTS_ALL Or KEY_QUERY_VALUE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK) And (Not SYNCHRONIZE))


Private Const STANDARD_RIGHTS_ALL = &H1F0000
Private Const SYNCHRONIZE = &H100000

Private Const REG_CREATED_NEW_KEY = &H1      ' create new key
Private Const REG_OPENED_EXISTING_KEY = &H2  ' open existed key


Private Const REG_SZ = 1                  ' string type
Private Const REG_BINARY = 3              ' binary type
Private Const REG_DWORD = 4               ' 32 bit integer
Private Const REG_QWORD = 11
Private Const REG_DWORD_LITTLE_ENDIAN = 4
Private Const REG_DWORD_BIG_ENDIAN = 5
Private Const REG_EXPAND_SZ = 2
Private Const REG_LINK = 6
Private Const REG_MULTI_SZ = 7
Private Const REG_NONE = 0
Private Const REG_RESOURCE_LIST = 8



Private Const ERROR_SUCCESS As Long = 0     ' the processing is finished in a success
Private Const ERROR_FILE_NOT_FOUND As Long = &H2    ' the file is not found.
Private Const ERROR_PATH_NOT_FOUND As Long = &H3    ' the path is not found.
Private Const ERROR_MORE_DATA As Long = &HEA        ' 234; There is more data.
Private Const ERROR_NO_MORE_ITEMS As Long = &H103   ' 259; No more data



'**---------------------------------------------
'** Windows API private declarations
'**---------------------------------------------
'''
''' SECURITY_ATTRIBUTES declatation; The SECURITY_ATTRIBUTES structure contains the security descriptor for an object and specifies whether the handle retrieved by specifying this structure is inheritable. This structure provides security settings for objects created by various functions, such as CreateFile, CreatePipe, CreateProcess, RegCreateKeyEx, or RegSaveKeyEx.
'''
#If VBA7 Then

    Private Type SECURITY_ATTRIBUTES
    
        nLength As Long
        lpSecurityDescriptor As LongPtr
        bInheritHandle As Long
    End Type
#Else
    Private Type SECURITY_ATTRIBUTES
    
        nLength As Long     ' The size, in bytes, of this structure. Set this value to the size of the SECURITY_ATTRIBUTES structure.
        lpSecurityDescriptor As Long    ' A pointer to a SECURITY_DESCRIPTOR structure that controls access to the object. If the value of this member is NULL, the object is assigned the default security descriptor associated with the access token of the calling process.
        bInheritHandle As Long  ' A Boolean value that specifies whether the returned handle is inherited when a new process is created.
    End Type
#End If

Private Type FILETIME

    dwLowDateTime As Long    ' Low order 32 bit
    dwHighDateTime As Long   ' High order 32 bit
End Type


#If VBA7 Then
    ' enumerate registry sub-keys in the specified registry-key;
    Private Declare PtrSafe Function RegEnumKeyEx Lib "advapi32.dll" Alias "RegEnumKeyExA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpName As String, lpcbName As Long, ByVal lpReserved As Long, ByVal lpClass As String, lpcbClass As Long, lpftLastWriteTime As FILETIME) As LongPtr

    ' enumerate registry values in the specified registry-key
    Private Declare PtrSafe Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, lpData As Byte, lpcbData As Long) As LongPtr

    ' copy byte string
    Private Declare PtrSafe Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

    ' open registry key
    Private Declare PtrSafe Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, vintResultKey As Long) As LongPtr

    ' close registry key
    Private Declare PtrSafe Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As LongPtr

    ' create new registry key or open the existed key
    Private Declare PtrSafe Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal Reserved As Long, ByVal lpClass As String, ByVal dwOptions As Long, ByVal samDesired As Long, lpSecurityAttributes As SECURITY_ATTRIBUTES, phkResult As Long, lpdwDisposition As Long) As LongPtr

    ' delete registry key and the sub keys
    Private Declare PtrSafe Function RegDeleteKey Lib "advapi32.dll" Alias "RegDeleteKeyA" (ByVal hKey As Long, ByVal lpSubKey As String) As LongPtr

    ' put a value into a value-field of a registry key
    Private Declare PtrSafe Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, lpData As Any, ByVal cbData As Long) As LongPtr

    ' query a value of the registry key
    Private Declare PtrSafe Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As LongPtr

    ' query a string value of the registry key
    Private Declare PtrSafe Function RegQueryValueExStr Lib "ADVAPI32" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, ByVal lpType As Long, ByVal lpData As String, lpcbData As Long) As LongPtr
    
    Private Declare PtrSafe Function RegDeleteValue Lib "advapi32.dll" Alias "RegDeleteValueA" (ByVal hKey As Long, ByVal lpValueName As String) As LongPtr
#Else
    
    Private Declare Function RegEnumKeyEx Lib "advapi32.dll" Alias "RegEnumKeyExA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpName As String, lpcbName As Long, ByVal lpReserved As Long, ByVal lpClass As String, lpcbClass As Long, lpftLastWriteTime As FILETIME) As Long
    
    Private Declare Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, lpData As Byte, lpcbData As Long) As Long
    
    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
    
    Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, vintResultKey As Long) As Long
    
    Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
    
    Private Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal Reserved As Long, ByVal lpClass As String, ByVal dwOptions As Long, ByVal samDesired As Long, lpSecurityAttributes As SECURITY_ATTRIBUTES, phkResult As Long, lpdwDisposition As Long) As Long
    
    Private Declare Function RegDeleteKey Lib "advapi32.dll" Alias "RegDeleteKeyA" (ByVal hKey As Long, ByVal lpSubKey As String) As Long
    
    Private Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, lpData As Any, ByVal cbData As Long) As Long
    
    Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long
    
    Private Declare Function RegQueryValueExStr Lib "ADVAPI32" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, ByVal lpType As Long, ByVal lpData As String, lpcbData As Long) As Long
    
    Private Declare Function RegDeleteValue Lib "advapi32.dll" Alias "RegDeleteValueA" (ByVal hKey As Long, ByVal lpValueName As String) As Long
#End If

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mintStringBufferSize As Long = 600
Private Const mintByteBufferSize As Long = 255
Private Const mintByteBufferSizeMinusOne As Long = mintByteBufferSize - 1

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Write or read for Win-registry values
'**---------------------------------------------
'''
'''
'''
''' <Argument>vstrFullRegKey: Input</Argument>
''' <Argument>robjOneNestedDic: Input - Dictionary(Of String[ChildSubKeyName], Dictionary(Of String[ValueName], Variant[Value]))</Argument>
Public Sub WriteWinRegistryFromOneNestedDic(ByVal vstrFullRegKey As String, ByRef robjOneNestedDic As Scripting.Dictionary)

    Dim intRootKey As Long, strSubKey As String
    
    SeparateRegistryRootKeyAndSubKey vstrFullRegKey, intRootKey, strSubKey
    
    WriteWinRegistryFromOneNestedDicBySeparated intRootKey, strSubKey, robjOneNestedDic
End Sub

'''
'''
'''
''' <Argument>rintRootKey: Input</Argument>
''' <Argument>rstrSubKey: Input</Argument>
''' <Argument>robjOneNestedDic: Input - Dictionary(Of String[ChildSubKeyName], Dictionary(Of String[ValueName], Variant[Value]))</Argument>
Public Sub WriteWinRegistryFromOneNestedDicBySeparated(ByRef rintRootKey As Long, _
        ByRef rstrSubKey As String, _
        ByRef robjOneNestedDic As Scripting.Dictionary)

    Dim intResultParentKey As Long, blnIsParentRegKeyOpened As Boolean
    Dim varChildSubKeyName As Variant, strChildSubKey As String, objChildDic As Scripting.Dictionary
    
    
    intResultParentKey = CreateWinRegistryKeyBySeparatedAndKeepOpening(blnIsParentRegKeyOpened, rintRootKey, rstrSubKey)
    
    If blnIsParentRegKeyOpened Then
    
        With robjOneNestedDic
        
            For Each varChildSubKeyName In .Keys
            
                strChildSubKey = rstrSubKey & "\" & varChildSubKeyName
            
                Set objChildDic = .Item(varChildSubKeyName)
            
                WriteWinRegistryFromDicBySeparated rintRootKey, strChildSubKey, objChildDic
            Next
        End With
    
        CloseWinRegistryKeyHandle intResultParentKey
    End If
End Sub

'''
'''
'''
''' <Return>Dictionary(Of String[ChildSubKeyName], Dictionary(Of String[ValueName], Variant[Value]))</Return>
Public Function GetOneNestedDicFromReadingWinRegChiledKeysAndValuesOfEachKey(ByVal vstrFullRegKey As String) As Scripting.Dictionary

    Dim objOneNestedDic As Scripting.Dictionary, objChildDic As Scripting.Dictionary

    Dim intRootKey As Long, strSubKey As String, blnIsParentRegKeyOpened As Boolean, blnIsChildRegKeyOpened As Boolean, intParentResultKey As Long, intChildResultKey As Long

    Dim strChildSubKey As String, intSubKeyLength As Long
    Dim strClassName As String, intClassLength As Long, udtFile As FILETIME, i As Long
    
    Dim strCorrectedChildSubKey As String, strCorrectedChildSubKeyName As String

#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    Set objOneNestedDic = New Scripting.Dictionary

    intParentResultKey = OpenWinRegistryKeyAndKeepOpeningForReading(blnIsParentRegKeyOpened, vstrFullRegKey)

    If blnIsParentRegKeyOpened Then
    
        i = 0
        
        Do
            ' get buffer
            strChildSubKey = Space(mintStringBufferSize)
            
            intSubKeyLength = Len(strChildSubKey)
        
            strClassName = Space(mintByteBufferSize)
            
            intClassLength = Len(strClassName)
            
            ' get subkey
            intRet = RegEnumKeyEx(intParentResultKey, i, strChildSubKey, intSubKeyLength, 0, strClassName, intClassLength, udtFile)
              
            ' determine whether exit loop end or not
            If intRet <> ERROR_SUCCESS Then
            
                Select Case intRet
                
                    Case ERROR_NO_MORE_ITEMS
                        ' loop end success
                        
                    Case Else
                    
                        Debug.Print "Error Code: " & CStr(intRet)
                
                        Debug.Print Trim(Replace(strChildSubKey, vbNullChar, " "))
                End Select
            
                Exit Do
            End If
            
            ' get sub-key name
            'Debug.Print Trim(Replace(strChildSubKey, vbNullChar, " "))
            
            'Debug.Print Left(strChildSubKey, intSubKeyLength)
            
            strCorrectedChildSubKeyName = Left(strChildSubKey, intSubKeyLength)
            
            strCorrectedChildSubKey = strSubKey & "\" & strCorrectedChildSubKeyName
            
            intChildResultKey = OpenWinRegistryKeyBySeparatedAndKeepOpeningForReading(blnIsChildRegKeyOpened, intRootKey, strCorrectedChildSubKey)
            
            If blnIsChildRegKeyOpened Then
            
                Set objChildDic = GetDicFromReadingWinRegValuesByOpened(intChildResultKey)
            
                CloseWinRegistryKeyHandle intChildResultKey
                
                objOneNestedDic.Add strCorrectedChildSubKeyName, objChildDic
            End If
            
            i = i + 1
        Loop

        CloseWinRegistryKeyHandle intParentResultKey
    End If
    
    Set GetOneNestedDicFromReadingWinRegChiledKeysAndValuesOfEachKey = objOneNestedDic
End Function

'''
'''
'''
''' <Argument>vstrFullRegKey: Input</Argument>
''' <Argument>robjDic: Input - Dictionary(Of String[ValueName], Variant[Value])</Argument>
Public Sub WriteWinRegistryFromDic(ByVal vstrFullRegKey As String, ByRef robjDic As Scripting.Dictionary)

    Dim intRootKey As Long, strSubKey As String
    
    SeparateRegistryRootKeyAndSubKey vstrFullRegKey, intRootKey, strSubKey

    WriteWinRegistryFromDicBySeparated intRootKey, strSubKey, robjDic
End Sub

'''
'''
'''
''' <Argument>rintRootKey: Input</Argument>
''' <Argument>rstrSubKey: Input</Argument>
''' <Argument>robjDic: Input - Dictionary(Of String[ValueName], Variant[Value])</Argument>
Public Sub WriteWinRegistryFromDicBySeparated(ByRef rintRootKey As Long, _
        ByRef rstrSubKey As String, _
        ByRef robjDic As Scripting.Dictionary)

    Dim intResultKey As Long, blnIsRegKeyOpened As Boolean
    
    intResultKey = CreateWinRegistryKeyBySeparatedAndKeepOpening(blnIsRegKeyOpened, rintRootKey, rstrSubKey)
    
    If blnIsRegKeyOpened Then
    
        WriteWinRegistryFromDicByOpened intResultKey, robjDic
        
        CloseWinRegistryKeyHandle intResultKey
    End If
End Sub

'''
'''
'''
Public Sub WriteWinRegistryFromDicByOpened(ByRef rintResultKey As Long, ByRef robjDic As Scripting.Dictionary)

    Dim varKey As Variant, strKey As String
    
    With robjDic
    
        For Each varKey In .Keys
        
            strKey = varKey
        
            WriteWinRegistryValueSimply rintResultKey, strKey, .Item(varKey)
        Next
    End With
End Sub

'''
'''
'''
Public Function GetDicFromReadingWinRegValues(ByVal vstrFullRegKey As String) As Scripting.Dictionary

    Dim intRootKey As Long, strSubKey As String
    
    SeparateRegistryRootKeyAndSubKey vstrFullRegKey, intRootKey, strSubKey
    
    Set GetDicFromReadingWinRegValues = GetDicFromReadingWinRegValuesBySeparated(intRootKey, strSubKey)
End Function


'''
'''
'''
Public Function GetDicFromReadingWinRegValuesBySeparated(ByRef rintRootKey As Long, ByRef rstrSubKey As String) As Scripting.Dictionary

    Dim intResultKey As Long, blnIsRegKeyOpened As Boolean, objDic As Scripting.Dictionary
    
    intResultKey = OpenWinRegistryKeyBySeparatedAndKeepOpeningForReading(blnIsRegKeyOpened, rintRootKey, rstrSubKey)

    If blnIsRegKeyOpened Then
    
        Set objDic = GetDicFromReadingWinRegValuesByOpened(intResultKey)
    
        CloseWinRegistryKeyHandle intResultKey
    Else
        Set objDic = New Scripting.Dictionary
    End If

    Set GetDicFromReadingWinRegValuesBySeparated = objDic
End Function

'''
'''
'''
''' <Argument>rintResultKey: Input</Argument>
''' <Return>Dictionary(Of String, Variant)</Return>
Public Function GetDicFromReadingWinRegValuesByOpened(ByRef rintResultKey As Long) As Scripting.Dictionary
    
    Dim i As Long
    ' to collect registry values
    Dim strValueName As String, intValueLength As Long
    Dim intDataType As Long        ' receives data type of value
    Dim bytData(0 To mintByteBufferSizeMinusOne) As Byte  ' 255-byte data buffer for read information
    Dim intDataLength As Long      ' size of data buffer information
    Dim strDataString As String    ' will receive data converted to a string, if necessary
    
    Dim objDic As Scripting.Dictionary, varValue As Variant
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    i = 0
    
    Set objDic = New Scripting.Dictionary
    
    Do
        strValueName = Space(mintStringBufferSize)  ' 255-space buffer
        
        intValueLength = mintByteBufferSize  ' length of the string
        
        intDataLength = mintByteBufferSize  ' size of data buffer
        
        ' Get the next value to be enumerated
        intRet = RegEnumValue(rintResultKey, i, strValueName, intValueLength, 0, intDataType, bytData(0), intDataLength)
        
        If intRet = ERROR_SUCCESS Then
        
            strValueName = Left(strValueName, intValueLength)
            
            ReadWinRegistryValueToArgSimply varValue, rintResultKey, intDataType, strValueName, bytData, intDataLength
            
            objDic.Add strValueName, varValue
        Else
            ' determine whether exit loop end or not
            Select Case intRet
            
                Case ERROR_NO_MORE_ITEMS
                
                    ' loop end success
                
                Case ERROR_MORE_DATA
                    Debug.Print "Error Code: " & CStr(intRet) & " The more data should have existed. [" & Left(strValueName, intValueLength) & "]"
                
                Case Else
                    Debug.Print "Error Code: " & CStr(intRet) & ", " & GetDllErrorMessage(CInt(intRet))
            End Select
        
            Exit Do
        End If
        
        i = i + 1
    Loop
    
    Set GetDicFromReadingWinRegValuesByOpened = objDic
End Function


'''
'''
'''
''' <Argument>rvarValue: Output</Argument>
''' <Argument>rintResultKey: Input</Argument>
''' <Argument>rintDataType: Input</Argument>
''' <Argument>rstrValueName: Input</Argument>
''' <Argument>rbytData: Input</Argument>
''' <Argument>rintDataLength: Input</Argument>
Public Sub ReadWinRegistryValueToArgSimply(ByRef rvarValue As Variant, _
        ByRef rintResultKey As Long, _
        ByRef rintDataType As Long, _
        ByRef rstrValueName As String, _
        ByRef rbytData() As Byte, _
        ByRef rintDataLength As Long)


    Dim strDataString As String    ' will receive data converted to a string, if necessary
    Dim intCharCounter As Long     ' counter variable
    Dim strPrintedBinaryValue As String, lngBuffer As Long

#If Win64 Then

    Dim int64Buffer As LongLong
#Else
    Dim int64Buffer As Long
#End If

#If VBA7 Then

    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    Select Case rintDataType
    
        Case REG_DWORD ' 32 bit unsigned integer
            
            intRet = RegQueryValueEx(rintResultKey, rstrValueName, 0, REG_DWORD, lngBuffer, Len(lngBuffer))

            If intRet = ERROR_SUCCESS Then
                
                'Debug.Print rstrValueName & ",  Data (Long): " & CStr(lngBuffer)
                
                rvarValue = lngBuffer
            End If
        
        Case REG_QWORD  ' 64 bit unsigned integer
            
            ' ! Attention This need to correct, these are invalid codes...
            
            intRet = RegQueryValueEx(rintResultKey, rstrValueName, 0, REG_QWORD, int64Buffer, Len(int64Buffer))

            If intRet = ERROR_SUCCESS Then
                
                'Debug.Print rstrValueName & ",  Data (Long): " & CStr(int64Buffer)
                
                rvarValue = int64Buffer
            End If
        
        Case REG_SZ  ' null-terminated string
        
            ' Copy the information from the byte array into the string.
            ' We subtract one because we don't want the trailing null.
            
            strDataString = Space(rintDataLength - 1)  ' make just enough room in the string
            
            CopyMemory ByVal strDataString, rbytData(0), rintDataLength - 1  ' copy useful data
            
            ' The following can get Date type
            
            rvarValue = GetVariantValueFromGeneralStringInformation(strDataString)
            
        Case REG_EXPAND_SZ  ' String able to extract of the Windows environment variables�i%�`%�j
        
            strDataString = Space(rintDataLength - 1)  ' make just enough room in the string
            
            CopyMemory ByVal strDataString, rbytData(0), rintDataLength - 1  ' copy useful data
            
            Debug.Print rstrValueName & ",  Data (Expandable-string): " & strDataString
            
            ' This can be extracted by plural methods, for example, it is the objShell.ExpandEnvironmentStrings("%SystemRoot%")
            
            rvarValue = strDataString
            
        Case REG_MULTI_SZ   ' multiline strings
        
            strDataString = Space(rintDataLength - 1)  ' make just enough room in the string
            
            CopyMemory ByVal strDataString, rbytData(0), rintDataLength - 1  ' copy useful data
            
            Debug.Print rstrValueName & ",  Data (Multline-string): " & strDataString
            
            strDataString = Replace(strDataString, Chr(0), vbLf)
            
            rvarValue = strDataString
            
        Case REG_BINARY  ' binary data
        
            ' Display the hexadecimal values of each byte of data, separated by
            ' sapces.  Use the strDataString buffer to allow us to assure each byte
            ' is represented by a two-character string.
            
            'Debug.Print rstrValueName & ",  Data (binary):";
            strPrintedBinaryValue = ""
            
            For intCharCounter = 0 To rintDataLength - 1  ' loop through returned information
            
                strDataString = Hex(rbytData(intCharCounter))  ' convert value into hex
                
                ' If needed, add leading zero(s).
                
                If Len(strDataString) < 2 Then
                    
                    strDataString = String(2 - Len(strDataString), "0") & strDataString
                End If
                'Debug.Print " "; strDataString;
                
                strPrintedBinaryValue = strPrintedBinaryValue & " " & strDataString
            Next
            'Debug.Print  ' end the line
    
            strPrintedBinaryValue = Trim(strPrintedBinaryValue)
            
            rvarValue = strPrintedBinaryValue
    End Select
End Sub


'''
'''
'''
Public Sub WriteWinRegistryValueSimply(ByRef rintResultKey As Long, ByRef rstrValueName As String, ByRef rvarDataValue As Variant)
    
    Dim strBuffer As String, intBuffer As Long, bytBuffer() As Byte

#If VBA7 Then

    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    ' set registry value by each variable-type
    Select Case varType(rvarDataValue)
    
        Case VBA.VbVarType.vbDate
        
            strBuffer = Format(rvarDataValue, "yyyy/m/d hh:mm:ss")
            
            intRet = RegSetValueEx(rintResultKey, rstrValueName, 0&, REG_SZ, ByVal strBuffer, LenB(StrConv(strBuffer, vbFromUnicode)) + 1)
    
        Case VBA.VbVarType.vbString                           ' string
        
            strBuffer = rvarDataValue
            
            ' About LenB(StrConv(rvarDataValue, vbFromUnicode)), Ref. https://qiita.com/cyrt/items/1f23dfbac5f4c8ed80f1
            '

            If strBuffer <> "" Then

                intRet = RegSetValueEx(rintResultKey, rstrValueName, 0&, REG_SZ, ByVal strBuffer, LenB(StrConv(rvarDataValue, vbFromUnicode)) + 1)
            Else
                intRet = RegSetValueEx(rintResultKey, rstrValueName, 0&, REG_SZ, 0, 0)  ' Set null string
            End If

        Case VBA.VbVarType.vbInteger, VBA.VbVarType.vbLong, VBA.VbVarType.vbBoolean       ' 32bit integer
        
            intBuffer = rvarDataValue
            
            intRet = RegSetValueEx(rintResultKey, rstrValueName, 0&, REG_DWORD, intBuffer, 4)

        Case VBA.VbVarType.vbSingle, VBA.VbVarType.vbDouble, VBA.VbVarType.vbDecimal
    
            ' Convert to String type
    
            strBuffer = CStr(rvarDataValue)
            
            intRet = RegSetValueEx(rintResultKey, rstrValueName, 0&, REG_SZ, ByVal strBuffer, LenB(StrConv(strBuffer, vbFromUnicode)) + 1)

        Case VBA.VbVarType.vbByte Or VBA.VbVarType.vbArray                  ' binary-data
        
            bytBuffer = rvarDataValue
            
            intRet = RegSetValueEx(rintResultKey, rstrValueName, 0&, REG_BINARY, bytBuffer(1), 5)
    End Select

    If intRet <> ERROR_SUCCESS Then
    
        Debug.Print "Failed to call RegSetValueEx function, ErrorCode: " & intRet & ", " & GetDllErrorMessage(CInt(intRet))
    End If
End Sub


'**---------------------------------------------
'** Create Win-registry key
'**---------------------------------------------
'''
'''
'''
Public Sub CreateWinRegistryKey(ByVal vstrFullRegKey As String, _
        Optional ByVal vintRegKeyAccessOption As Long = KEY_CREATE_SUB_KEY)

    Dim intRootKey As Long, strSubKey As String

    Dim udtSECURITYATTRIBUTES As SECURITY_ATTRIBUTES
    Dim intResultKey As Long, intDisposition As Long  ' A pointer to a variable that receives one of the following disposition values; REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    SeparateRegistryRootKeyAndSubKey vstrFullRegKey, intRootKey, strSubKey

    If Not IsWinRegKeyExisted(intRootKey, strSubKey) Then
    
        intRet = RegCreateKeyEx(intRootKey, strSubKey, 0&, vbNullString, REG_OPTION_NON_VOLATILE, vintRegKeyAccessOption, udtSECURITYATTRIBUTES, intResultKey, intDisposition)
    
        If intRet = ERROR_SUCCESS Then
        
            RegCloseKey intResultKey
        End If
    Else
        Debug.Print "WinRegKey has already existed: " & vstrFullRegKey
    End If
End Sub

'''
'''
'''
Public Function CreateWinRegistryKeyAndKeepOpening(ByRef rblnIsRegKeyOpened As Boolean, _
        ByRef rstrFullRegKey As String, _
        Optional ByVal vintRegKeyAccessOption As Long = KEY_ALL_ACCESS) As Long

    Dim intRootKey As Long, strSubKey As String

    SeparateRegistryRootKeyAndSubKey rstrFullRegKey, intRootKey, strSubKey

    CreateWinRegistryKeyAndKeepOpening = CreateWinRegistryKeyBySeparatedAndKeepOpening(rblnIsRegKeyOpened, intRootKey, strSubKey, vintRegKeyAccessOption)
End Function


'''
'''
'''
Public Function CreateWinRegistryKeyBySeparatedAndKeepOpening(ByRef rblnIsRegKeyOpened As Boolean, _
        ByRef rintRootKey As Long, _
        ByRef rstrSubKey As String, _
        Optional ByVal vintRegKeyAccessOption As Long = KEY_ALL_ACCESS) As Long


    Dim udtSECURITYATTRIBUTES As SECURITY_ATTRIBUTES
    Dim intResultKey As Long, intDisposition As Long  ' A pointer to a variable that receives one of the following disposition values; REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    intResultKey = 0

    intRet = RegCreateKeyEx(rintRootKey, rstrSubKey, 0&, vbNullString, REG_OPTION_NON_VOLATILE, vintRegKeyAccessOption, udtSECURITYATTRIBUTES, intResultKey, intDisposition)

    If intRet = ERROR_SUCCESS Then
    
        If intDisposition = REG_OPENED_EXISTING_KEY Then
        
            'Debug.Print "WinRegKey has already existed: " & GetFullWinRegKey(rintRootKey, rstrSubKey)
        End If
        
        rblnIsRegKeyOpened = True
    Else
        Debug.Print "Failed to create Win-reg key: " & GetFullWinRegKey(rintRootKey, rstrSubKey)
        
        rblnIsRegKeyOpened = False
    End If
    
    CreateWinRegistryKeyBySeparatedAndKeepOpening = intResultKey
End Function


'**---------------------------------------------
'** Open Win-registry key for reading
'**---------------------------------------------
'''
'''
'''
Public Function OpenWinRegistryKeyAndKeepOpeningForReading(ByRef rblnIsRegKeyOpened As Boolean, _
        ByRef rstrFullRegKey As String, _
        Optional ByVal vintRegKeyAccessOption As Long = KEY_READ) As Long

    Dim intRootKey As Long, strSubKey As String
    
    SeparateRegistryRootKeyAndSubKey rstrFullRegKey, intRootKey, strSubKey

    OpenWinRegistryKeyAndKeepOpeningForReading = OpenWinRegistryKeyBySeparatedAndKeepOpeningForReading(rblnIsRegKeyOpened, intRootKey, strSubKey, vintRegKeyAccessOption)
End Function

'''
'''
'''
Public Function OpenWinRegistryKeyBySeparatedAndKeepOpeningForReading(ByRef rblnIsRegKeyOpened As Boolean, _
        ByRef rintRootKey As Long, _
        ByRef rstrSubKey As String, _
        Optional ByVal vintRegKeyAccessOption As Long = KEY_READ) As Long

    Dim intResultKey As Long
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    intResultKey = 0

    ' get registry-key handle
    intRet = RegOpenKeyEx(rintRootKey, rstrSubKey, 0, vintRegKeyAccessOption, intResultKey)
    
    If intRet = ERROR_SUCCESS Then
    
        rblnIsRegKeyOpened = True
    Else
        rblnIsRegKeyOpened = False
    End If

    OpenWinRegistryKeyBySeparatedAndKeepOpeningForReading = intResultKey
End Function



'''
'''
'''
Public Function CloseWinRegistryKeyHandle(ByRef rintResultKey As Long) As Long

    Dim int32bitRet As Long
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    intRet = RegCloseKey(rintResultKey)

    int32bitRet = CLng(intRet)

    CloseWinRegistryKeyHandle = int32bitRet
End Function



'**---------------------------------------------
'** Win-registry key string tools
'**---------------------------------------------
'''
'''
'''
Public Function GetFullWinRegKey(ByVal vintRootKey As Long, ByVal vstrSubKey As String) As String
    
    Dim strFullRegKey As String
    
    If vstrSubKey = "" Then
    
        strFullRegKey = GetStrFromRegistryRootKeyValue(vintRootKey)
    Else
        strFullRegKey = GetStrFromRegistryRootKeyValue(vintRootKey) & "\" & vstrSubKey
    End If

    GetFullWinRegKey = strFullRegKey
End Function

'''
'''
'''
Public Function GetFullWinSubRegKey(ByVal vstrParentSubKey As String, ByVal vstrChildSubKey As String) As String
    
    Dim strFullSubRegKey As String

    If vstrParentSubKey = "" Then
    
        strFullSubRegKey = vstrChildSubKey
    Else
        strFullSubRegKey = vstrParentSubKey & "\" & vstrChildSubKey
    End If

    GetFullWinSubRegKey = strFullSubRegKey
End Function


'**---------------------------------------------
'** Registry key conversions
'**---------------------------------------------
'''
'''
'''
Public Function IsWinFullRegKeyExisted(ByRef rstrFullRegKey As String) As Boolean

    Dim intRootKey As Long, strSubKey As String

    SeparateRegistryRootKeyAndSubKey rstrFullRegKey, intRootKey, strSubKey
    
    IsWinFullRegKeyExisted = IsWinRegKeyExisted(intRootKey, strSubKey)
End Function


'''
'''
'''
Public Function IsWinRegKeyExisted(ByRef rintRootKey As Long, ByRef rstrSubKey As String) As Boolean

    Dim intResultKey As Long
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    ' get registry-key handle
    intRet = RegOpenKeyEx(rintRootKey, rstrSubKey, 0, KEY_READ, intResultKey)
    
    If intRet = ERROR_SUCCESS Then
    
        IsWinRegKeyExisted = True
        
        intRet = RegCloseKey(intResultKey)
    Else
        IsWinRegKeyExisted = False
    End If
End Function


'''
'''
'''
Public Function GetValueFromRegistryRootKeyString(ByVal vstrRootKey As String) As Long

    Dim intRootKey As Long
    
    Select Case vstrRootKey
    
        Case "HKEY_CURRENT_USER"
        
            intRootKey = HKEY_CURRENT_USER
            
        Case "HKEY_CLASSES_ROOT"
        
            intRootKey = HKEY_CLASSES_ROOT
            
        Case "HKEY_LOCAL_MACHINE"
        
            intRootKey = HKEY_LOCAL_MACHINE
            
        Case "HKEY_USERS"
        
            intRootKey = HKEY_USERS
            
        Case "HKEY_CURRENT_CONFIG"
        
            intRootKey = HKEY_CURRENT_CONFIG
            
        Case "HKEY_DYN_DATA"
        
            intRootKey = HKEY_DYN_DATA
    End Select

    GetValueFromRegistryRootKeyString = intRootKey
End Function

'''
'''
'''
Public Function GetStrFromRegistryRootKeyValue(ByVal vintRootKey As Long) As String

    Dim strRootKey As String
    
    Select Case vintRootKey
    
        Case HKEY_CURRENT_USER
        
            strRootKey = "HKEY_CURRENT_USER"
            
        Case HKEY_CLASSES_ROOT
        
            strRootKey = "HKEY_CLASSES_ROOT"
            
        Case HKEY_LOCAL_MACHINE
        
            strRootKey = "HKEY_LOCAL_MACHINE"
            
        Case HKEY_USERS
        
            strRootKey = "HKEY_USERS"
            
        Case HKEY_CURRENT_CONFIG
        
            strRootKey = "HKEY_CURRENT_CONFIG"
            
        Case HKEY_DYN_DATA
        
            strRootKey = "HKEY_DYN_DATA"
    End Select

    GetStrFromRegistryRootKeyValue = strRootKey
End Function


'''
''' separate RootKey value and SubKey string from FullSubKey string
'''
''' <Argument>rstrFullRegKey: Input string</Argument>
''' <Argument>rintRootKey: Output number</Argument>
''' <Argument>rstrSubKey: Output string</Argument>
Public Sub SeparateRegistryRootKeyAndSubKey(ByRef rstrFullRegKey As String, ByRef rintRootKey As Long, ByRef rstrSubKey As String)

    Dim intP1 As Integer, strRootKey As String
    
    intP1 = InStr(1, rstrFullRegKey, "\")
    
    rstrSubKey = Right(rstrFullRegKey, Len(rstrFullRegKey) - intP1)
    
    strRootKey = UCase(Left(rstrFullRegKey, intP1 - 1))
    
    rintRootKey = GetValueFromRegistryRootKeyString(strRootKey)
End Sub

'''
'''
'''
Public Function GetLastChildRegKeyName(ByRef rstrFullRegKey As String)

    Dim intPointOfString As Long

    intPointOfString = InStrRev(rstrFullRegKey, "\")
    
    If intPointOfString > 0 Then
    
        GetLastChildRegKeyName = Right(rstrFullRegKey, Len(rstrFullRegKey) - intPointOfString)
    Else
        GetLastChildRegKeyName = rstrFullRegKey
    End If
End Function


