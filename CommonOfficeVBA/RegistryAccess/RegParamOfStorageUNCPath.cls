VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "RegParamOfStorageUNCPath"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Seaching or diciding path parameters class
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 12/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'***********************************************
'**
'** Registry key using rules
'**     default common parent subkey: HKEY_CURRENT_USER\Software\VBASecurePosition
'**     Excel VBA application subkey: HKEY_CURRENT_USER\Software\VBASecurePosition\ExcelVBA
'**
'**     VBA Application Drive and UNC Path SubKey:
'**         HKEY_CURRENT_USER\Software\VBASecurePosition\ExcelVBA\UNCPath
'**             <EntryName>NetworkStorageHostAndDir01:  for Example "\\<host-name>\<share directory name>"
'**             <EntryName>NetworkStorageHostAndDir02:
'**             <EntryName>NetworkStorageHostAndDir03:
'**         HKEY_CURRENT_USER\Software\VBASecurePosition\ExcelVBA\UNCPath\<ProjectName(Module Name)>\<SubjectTitle>\
'**             <EntryName>Path,    (for Load, predetermined)
'**             <EntryName>StorageSearchFlag: Prior to search local(&H2) or Prior to search Network(&H1)
'**             <EntryName>LocalPriorSubPath:
'**             <EntryName>NetworkPriorSubPath:
'**
'***********************************************

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mstrSolveNetworkHostAndDirectoryPathByPlugInInterface As String

Private mstrSolveLocalDriveLetterByPlugInInterface As String


Private menmStorageSearchMode As StoragePathSearchingFlag

Private mstrLocalPriorSubPath As String

Private mstrNetworkPriorSubPath As String

Private mstrDefaultFileName As String

Private mblnIsRecommendedToCreateNewWhenItDoesNotExist As Boolean


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mstrSolveNetworkHostAndDirectoryPathByPlugInInterface = ""
    
    mstrSolveLocalDriveLetterByPlugInInterface = ""
    
    
    menmStorageSearchMode = StoragePathSearchingFlag.NoSetSearchPathCondition
    
    
    mstrLocalPriorSubPath = ""
    
    mstrNetworkPriorSubPath = ""
    
    mblnIsRecommendedToCreateNewWhenItDoesNotExist = False
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
''' Get this computer network host and sub-directorypath, call back function name by Plug-in VBA module
'''
''' If this has no setting (this Stiring and the Public interface function implementation), the default behavior works without occuring error
'''
Public Property Get SolveNetworkHostAndDirectoryPathByPlugInInterface() As String

    SolveNetworkHostAndDirectoryPathByPlugInInterface = mstrSolveNetworkHostAndDirectoryPathByPlugInInterface
End Property
Public Property Let SolveNetworkHostAndDirectoryPathByPlugInInterface(ByVal vstrSolveNetworkHostAndDirectoryPathByPlugInInterface As String)

    mstrSolveNetworkHostAndDirectoryPathByPlugInInterface = vstrSolveNetworkHostAndDirectoryPathByPlugInInterface
End Property
'''
''' Get this computer local drive letter, call back function name by Plug-in VBA module
'''
''' If this has no setting (this Stiring and the Public interface function implementation), the default behavior works without occuring error
'''
Public Property Get SolveLocalDriveLetterByPlugInInterface() As String

    SolveLocalDriveLetterByPlugInInterface = mstrSolveLocalDriveLetterByPlugInInterface
End Property
Public Property Let SolveLocalDriveLetterByPlugInInterface(ByVal vstrSolveLocalDriveLetterByPlugInInterface As String)

    mstrSolveLocalDriveLetterByPlugInInterface = vstrSolveLocalDriveLetterByPlugInInterface
End Property



Public Property Get StorageSearchMode() As StoragePathSearchingFlag

    StorageSearchMode = menmStorageSearchMode
End Property
Public Property Let StorageSearchMode(ByVal venmStorageSearchMode As StoragePathSearchingFlag)

    menmStorageSearchMode = venmStorageSearchMode
End Property


Public Property Get LocalPriorSubPath() As String

    LocalPriorSubPath = mstrLocalPriorSubPath
End Property
Public Property Let LocalPriorSubPath(ByVal vstrLocalPriorSubPath As String)

    mstrLocalPriorSubPath = vstrLocalPriorSubPath
End Property


Public Property Get NetworkPriorSubPath() As String

    NetworkPriorSubPath = mstrNetworkPriorSubPath
End Property
Public Property Let NetworkPriorSubPath(ByVal vstrNetworkPriorSubPath As String)

    mstrNetworkPriorSubPath = vstrNetworkPriorSubPath
End Property



Public Property Get DefaultFileName() As String

    DefaultFileName = mstrDefaultFileName
End Property
Public Property Let DefaultFileName(ByVal vstrDefaultFileName As String)

    mstrDefaultFileName = vstrDefaultFileName
End Property


Public Property Get IsRecommendedToCreateNewWhenItDoesNotExist() As Boolean

    IsRecommendedToCreateNewWhenItDoesNotExist = mblnIsRecommendedToCreateNewWhenItDoesNotExist
End Property
Public Property Let IsRecommendedToCreateNewWhenItDoesNotExist(ByVal vblnIsRecommendedToCreateNewWhenItDoesNotExist As Boolean)

    mblnIsRecommendedToCreateNewWhenItDoesNotExist = vblnIsRecommendedToCreateNewWhenItDoesNotExist
End Property


