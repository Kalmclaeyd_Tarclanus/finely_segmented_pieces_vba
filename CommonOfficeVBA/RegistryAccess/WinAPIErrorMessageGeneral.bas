Attribute VB_Name = "WinAPIErrorMessageGeneral"
'
'   get Windows API error message
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 29/Apr/2020    Mr. nukie_53            The original idea has been disclosed from https://qiita.com/nukie_53/items/745d3b15c45cd3885811
'       Tue, 10/May/2022    Kalmclaeyd Tarclanus    Forked.
'

Option Explicit

'///////////////////////////////////////////////
'/// Private WinAPI Definitions
'///////////////////////////////////////////////
#If Win64 Then
    
    ' https://docs.microsoft.com/en-us/windows/desktop/api/winbase/nf-winbase-formatmessage
    Private Declare PtrSafe Function FormatMessage Lib "kernel32" Alias "FormatMessageA" (ByVal dwFlags As Long, ByVal lpSource As Long, ByVal dwMessageId As Long, ByVal dwLanguageId As Long, ByVal lpBuffer As String, ByVal nSize As Long, Arguments As LongPtr) As Long
#Else
    Private Declare Function FormatMessage Lib "kernel32" Alias "FormatMessageA" (ByVal dwFlags As Long, ByVal lpSource As Long, ByVal dwMessageId As Long, ByVal dwLanguageId As Long, ByVal lpBuffer As String, ByVal nSize As Long, Arguments As Long) As Long
#End If


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
'''
'''
Private Enum FORMAT_MESSAGE_FLAGS   '

    MAX_WIDTH_MASK = &HFF&

    ALLOCATE_BUFFER = &H100&    ' Assigning the string buffer on FormatMessage. For you get results, you have to extract it from the memory
    
    IGNORE_INSERTS = &H200&
    
    FROM_STRING = &H400&
    
    FROM_HMODULE = &H800&
    
    FROM_SYSTEM = &H1000&   ' Get some messages from this Windows system, for example, there are some error messages when you use some DLL functions.
    
    ARGUMENT_ARRAY = &H2000&
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' get DLL function error message.
'''
''' <Argument>dwMessageId: error-message ID, If it is omitted, use Err.LastDllError</Argument>
Public Function GetDllErrorMessage(Optional ByVal dwMessageId As Long = 0) As String

    Dim intPaddingSize As Long, intAPIResult As Long, strBuffer As String, intSize As Long
    
    Const strPaddingChar = VBA.Constants.vbNullChar
    
    If dwMessageId = 0 Then dwMessageId = VBA.Information.Err().LastDllError

    ' get a buffer, because this doesn't specify ALLOCATE_BUFFER
    
    intPaddingSize = &HFF
    
    Do
        ' get a buffer in order to put the error message description
        
        strBuffer = VBA.Strings.String$(intPaddingSize, strPaddingChar)
        
        intSize = VBA.Strings.Len(strBuffer)

        intAPIResult = FormatMessage(FROM_SYSTEM Or MAX_WIDTH_MASK, 0, dwMessageId, 0, strBuffer, intSize, 0)


        ' If it is failed, or the size of the buffer may be lack, the the intAPIResult should have been zero.
        
        If intAPIResult <> 0 Then Exit Do

        ' retry to get error-message with emphasizing the buffer
        
        intPaddingSize = intPaddingSize * 2
    Loop

    ' Cut the waste string from the buffer and output the enabling error-massege
    
    GetDllErrorMessage = VBA.Strings.Left$(strBuffer, VBA.Strings.InStr(1, strBuffer, strPaddingChar) - 1)
End Function

