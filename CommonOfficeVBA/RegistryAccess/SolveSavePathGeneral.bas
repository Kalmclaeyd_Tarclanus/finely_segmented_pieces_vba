Attribute VB_Name = "SolveSavePathGeneral"
'
'   utilities to solute this VB project system common paths.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on WinRegStorageUNCPath.bas, InitRegParamOfStorageUNCPath.bas
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'       Wed, 10/May/2023    Kalmclaeyd Tarclanus    Moved the some operations, such as GetTemporaryDevelopmentVBARootDir(), from CodeAnalysisVBA.bas
'       Tue, 13/Jun/2023    Kalmclaeyd Tarclanus    Moved GetTemporaryCodesBaseDir() method from CodeExternalOfficeCommonOut.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
'**---------------------------------------------
'** UNC pathes portability solution
'**---------------------------------------------
Private Const mstrModuleKey As String = "SolveSavePathGeneral"   ' 1st part of registry sub-key


Private Const mstrSubjectDevelopmentVBARootDirKey As String = "DevelopmentVBARootDir"   ' 2nd part of registry sub-key, recommend network drive

Private Const mstrSubjectTmpRootDirKey As String = "TmpRootDir"   ' 2nd part of registry sub-key, recommend local hard drive

Private Const mstrSubjectCodesRepositoryRootDirKey As String = "CodesRepositoryStorageRootDir"   ' 2nd part of registry sub-key, recommend network drive

Private Const mstrSubjectPackedVbaCodesRepositoryRootDirKey As String = "PackedVbaCodesRepositoryStorageRootDir"   ' 2nd part of registry sub-key, recommend network drive

Private Const mstrSubjectTmpRepositoryRootDirKey As String = "TmpRepositoryRootDir"   ' 2nd part of registry sub-key, recommend local hard drive


Private Const mstrSubjectCurrentBookOutputDirKey As String = "OutputBookRootDir"   ' 2nd part of registry sub-key, recommend local hard drive

Private Const mstrSubjectCurrentCSVOutputDirKey As String = "OutputCSVRootDir"   ' 2nd part of registry sub-key, recommend local hard drive

Private Const mstrSubjectTmpCodesBaseKey As String = "TmpCodeBase"   ' 2nd part of registry sub-key, recommend local drive


' AIP(Azure Information Protection) support

Private Const mstrSubjectAIPLabeledExcelBookPathKey As String = "LocalAIPLabledExcelBookPath"   ' 2nd part of registry sub-key, recommend local hard drive


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mblnIsPreparedAIPLabeledExcelBookExistenceChecked As Boolean

Private mblnIsAIPLabeledExcelBookPreparedInThisComputer As Boolean

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Solute paths of codes which both output and modify
'**---------------------------------------------
'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetTmpCompareRootPathOriginal() As String

    GetTmpCompareRootPathOriginal = GetTemporaryCodesBaseDir() & "\TmpCompareOrigCodes"
End Function


'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetTmpCompareRootPathExternal() As String

    GetTmpCompareRootPathExternal = GetTemporaryCodesBaseDir() & "\TmpCompareExtrCodes"
End Function


'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetTmpCompareRootPathModified() As String

    GetTmpCompareRootPathModified = GetTemporaryCodesBaseDir() & "\TmpCompareModifiedCodes"
End Function

'**---------------------------------------------
'** Actual implementation of IOutputBookPath
'**---------------------------------------------
'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetXlConnectingPrearrangedLogBookPath() As String
    
    GetXlConnectingPrearrangedLogBookPath = GetCurrentBookOutputDir() & "\SQLTests\XlAdoConnectingUnittests.xlsx"
End Function

'''
'''
'''
Public Function GetAccDbConnectingPrearrangedLogBookPath() As String
    
    GetAccDbConnectingPrearrangedLogBookPath = GetCurrentBookOutputDir() & "\SQLTests\AccDbAdoConnectingUnittests.xlsx"
End Function

'''
'''
'''
Public Function GetSqLiteConnectingPrearrangedLogBookPath() As String
    
    GetSqLiteConnectingPrearrangedLogBookPath = GetCurrentBookOutputDir() & "\SQLTests\SqLiteAdoConnectingUnittests.xlsx"
End Function


'///////////////////////////////////////////////
'/// Operations - a UNC path portability solution
'///////////////////////////////////////////////
'**---------------------------------------------
'** set user-specified path
'**---------------------------------------------
'''
''' Individual-Set-Registy
'''
Public Sub ISRInCodeAnalysisVBAOfDevelopmentVBARootDir(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectDevelopmentVBARootDirKey, vstrTargetPath
End Sub
Public Sub IDelRInCodeAnalysisVBAOfDevelopmentVBARootDir()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectDevelopmentVBARootDirKey
End Sub

Public Sub ISRInCodeAnalysisVBAOfTemporaryDevelopmentVBARootDir(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectTmpRootDirKey, vstrTargetPath
End Sub
Public Sub IDelRInCodeAnalysisVBAOfTemporaryDevelopmentVBARootDir()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectTmpRootDirKey
End Sub

'''
''' Individual-Set-Registy
'''
Public Sub ISRInCodeAnalysisVBAOfRepositoryCodesRootDir(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectCodesRepositoryRootDirKey, vstrTargetPath
End Sub
Public Sub IDelRInCodeAnalysisVBAOfRepositoryCodesRootDir()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectCodesRepositoryRootDirKey
End Sub


Public Sub ISRInCodeAnalysisVBAOfRepositoryPackedVbaCodesRootDir(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectPackedVbaCodesRepositoryRootDirKey, vstrTargetPath
End Sub
Public Sub IDelRInCodeAnalysisVBAOfRepositoryPackedVbaCodesRootDir()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectPackedVbaCodesRepositoryRootDirKey
End Sub



'''
''' Individual-Set-Registy
'''
Public Sub ISRInCodeAnalysisVBAOfTmpRepositoryRootDir(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectTmpRepositoryRootDirKey, vstrTargetPath
End Sub
Public Sub IDelRInCodeAnalysisVBAOfTmpRepositoryRootDir()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectTmpRepositoryRootDirKey
End Sub

'''
''' Individual-Set-Registy
'''
Public Sub ISRInCodeAnalysisVBAOfCurrentBookOutputDir(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectCurrentBookOutputDirKey, vstrTargetPath
End Sub
Public Sub IDelRInCodeAnalysisVBAOfCurrentBookOutputDir()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectCurrentBookOutputDirKey
End Sub

'''
''' Individual-Set-Registy
'''
Public Sub ISRInCodeAnalysisVBAOfCurrentCSVOutputDir(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectCurrentCSVOutputDirKey, vstrTargetPath
End Sub
Public Sub IDelRInCodeAnalysisVBAOfCurrentCSVOutputDir()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectCurrentCSVOutputDirKey
End Sub

'''
''' Individual-Set-Registy
'''
Public Sub ISRInCodeExternalOfficeCommonOutOfGetTemporaryCodesBaseDir(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectTmpCodesBaseKey, vstrTargetPath
End Sub
Public Sub IDelRInCodeExternalOfficeCommonOutOfGetTemporaryCodesBaseDir()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectTmpCodesBaseKey
End Sub

'''
''' Individual-Set-Registy - AIP Label control
'''
Public Sub ISRInAIPLabeledExcelBookPathOfGetPreparedAIPLabeledExcelBookPath(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectAIPLabeledExcelBookPathKey, vstrTargetPath
End Sub
Public Sub IDelRInAIPLabeledExcelBookPathOfGetPreparedAIPLabeledExcelBookPath()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectAIPLabeledExcelBookPathKey
End Sub

'**---------------------------------------------
'** solute path generaly
'**---------------------------------------------
'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetDevelopmentVBARootDir() As String

    Dim strModuleNameAndSubjectKey As String, strTargetPath As String
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectDevelopmentVBARootDirKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If strTargetPath = "" Then
    
        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForDevelopmentVBARoot())
    End If
    
    GetDevelopmentVBARootDir = strTargetPath
End Function

Private Function mfobjGetRegParamOfStorageUNCPathColForDevelopmentVBARoot() As Collection
    
    Dim objCol As Collection, strSubDirectoryPath As String
    
    Set objCol = Nothing
    
    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForDevelopmentVBARoot")
    
    If objCol Is Nothing Then   ' If interface cannot be soluted
        
        With New WshNetwork
            
            strSubDirectoryPath = "VBAWorks\" & .UserName & "\DevVBA"
        End With
    
        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCDirectoryPath(StoragePathSearchingFlag.NetWorkOrLocalAsDirectory, _
                strSubDirectoryPath, _
                strSubDirectoryPath))
    End If
    
    Set mfobjGetRegParamOfStorageUNCPathColForDevelopmentVBARoot = objCol
End Function


'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetTemporaryDevelopmentVBARootDir() As String
    
    Dim strModuleNameAndSubjectKey As String, strTargetPath As String
    
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectTmpRootDirKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If strTargetPath = "" Then
    
        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForTemporaryDevelopmentVBARoot())
    End If
    
    GetTemporaryDevelopmentVBARootDir = strTargetPath
End Function

Private Function mfobjGetRegParamOfStorageUNCPathColForTemporaryDevelopmentVBARoot() As Collection
    
    Dim objCol As Collection, strSubDirectoryPath As String
    
    
    Set objCol = Nothing
    
    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForTemporaryDevelopmentVBARoot")
    
    If objCol Is Nothing Then   ' If interface cannot be soluted
        
        With New WshNetwork
            
            strSubDirectoryPath = "tmp\" & .UserName & "\DevVBA"
        End With
    
        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCDirectoryPath(StoragePathSearchingFlag.NetWorkOrLocalAsDirectory, _
                strSubDirectoryPath, _
                strSubDirectoryPath))
    End If
    
    Set mfobjGetRegParamOfStorageUNCPathColForTemporaryDevelopmentVBARoot = objCol
End Function


'''
''' get current file repository root directory
'''
''' solute directory path from registry CURRENT_USER
''' this is also refered by CodeAnalysisVBAAllPaths.bas and CodeExternalBookOut.bas
'''
Public Function GetRepositoryCodesRootDir() As String
    
    Dim strModuleNameAndSubjectKey As String, strTargetPath As String
    
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectCodesRepositoryRootDirKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If strTargetPath = "" Then
    
        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForRepositoryCodesRoot())
    End If
    
    GetRepositoryCodesRootDir = strTargetPath
End Function


Private Function mfobjGetRegParamOfStorageUNCPathColForRepositoryCodesRoot() As Collection
    
    Dim objCol As Collection, strSubDirectoryPath As String
    
    
    Set objCol = Nothing
    
    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForRepositoryCodesRoot")
    
    If objCol Is Nothing Then   ' If interface cannot be soluted
        
        With New WshNetwork
            
            strSubDirectoryPath = "VBAWorks\" & .UserName & "\Repositories"
        End With
    
        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCDirectoryPath(StoragePathSearchingFlag.NetWorkOrLocalAsDirectory, strSubDirectoryPath, strSubDirectoryPath))
    End If
    
    Set mfobjGetRegParamOfStorageUNCPathColForRepositoryCodesRoot = objCol
End Function


'''
''' solute directory path from registry CURRENT_USER
''' this is also refered by CodeAnalysisVBAAllPaths.bas and CodeExternalBookOut.bas
'''
Public Function GetRepositoryPackedVbaCodesRootDir() As String
    
    Dim strModuleNameAndSubjectKey As String, strTargetPath As String
    
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectPackedVbaCodesRepositoryRootDirKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If strTargetPath = "" Then
    
        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForRepositoryPackedVbaCodesRoot())
    End If
    
    GetRepositoryPackedVbaCodesRootDir = strTargetPath
End Function


Private Function mfobjGetRegParamOfStorageUNCPathColForRepositoryPackedVbaCodesRoot() As Collection
    
    Dim objCol As Collection, strSubDirectoryPath As String
    
    
    Set objCol = Nothing
    
    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForRepositoryPackedVbaCodesRoot")
    
    If objCol Is Nothing Then   ' If interface cannot be soluted
        
        With New WshNetwork
            
            strSubDirectoryPath = "VBAWorks\" & .UserName & "\PackedVbaRepositories"
        End With
    
        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCDirectoryPath(StoragePathSearchingFlag.NetWorkOrLocalAsDirectory, strSubDirectoryPath, strSubDirectoryPath))
    End If
    
    Set mfobjGetRegParamOfStorageUNCPathColForRepositoryPackedVbaCodesRoot = objCol
End Function



'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetTmporaryRepositoryRootDir() As String
    
    Dim strModuleNameAndSubjectKey As String, strTargetPath As String
    
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectTmpRepositoryRootDirKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If strTargetPath = "" Then
    
        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForTemporaryRepositoryRoot())
    End If
    
    GetTmporaryRepositoryRootDir = strTargetPath
End Function



Private Function mfobjGetRegParamOfStorageUNCPathColForTemporaryRepositoryRoot() As Collection
    
    Dim objCol As Collection, strSubDirectoryPath As String
    
    
    Set objCol = Nothing
    
    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForTmporaryRepositoryRoot")
    
    If objCol Is Nothing Then   ' If interface cannot be soluted
        
        With New WshNetwork
            
            strSubDirectoryPath = "tmp\" & .UserName & "\Repositories"
        End With
    
        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCDirectoryPath(StoragePathSearchingFlag.NetWorkOrLocalAsDirectory, strSubDirectoryPath, strSubDirectoryPath))
    End If
    
    Set mfobjGetRegParamOfStorageUNCPathColForTemporaryRepositoryRoot = objCol
End Function


'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetCurrentBookOutputDir() As String
    
    Dim strModuleNameAndSubjectKey As String, strTargetPath As String
    
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectCurrentBookOutputDirKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If strTargetPath = "" Then
    
        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForCurrentBookOutputDir())
    End If
    
    GetCurrentBookOutputDir = strTargetPath
End Function

'''
'''
'''
Private Function mfobjGetRegParamOfStorageUNCPathColForCurrentBookOutputDir() As Collection
    
    Dim objCol As Collection, strSubDirectoryPath As String
    
    
    Set objCol = Nothing
    
    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForCurrentBookOutputDir")
    
    If objCol Is Nothing Then   ' If interface cannot be soluted
        
        With New WshNetwork
            
            strSubDirectoryPath = "VBAWorks\" & .UserName & "\BookOutputs"
        End With
    
        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCDirectoryPath(StoragePathSearchingFlag.NetWorkOrLocalAsDirectory, strSubDirectoryPath, strSubDirectoryPath))
    End If
    
    Set mfobjGetRegParamOfStorageUNCPathColForCurrentBookOutputDir = objCol
End Function



'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetCurrentCSVOutputDir() As String
    
    Dim strModuleNameAndSubjectKey As String, strTargetPath As String
    
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectCurrentCSVOutputDirKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If strTargetPath = "" Then
    
        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForCurrentCSVOutputDir())
    End If
    
    GetCurrentCSVOutputDir = strTargetPath
End Function

'''
'''
'''
Private Function mfobjGetRegParamOfStorageUNCPathColForCurrentCSVOutputDir() As Collection
    
    Dim objCol As Collection, strSubDirectoryPath As String
    
    
    Set objCol = Nothing
    
    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForCurrentCSVOutputDir")
    
    If objCol Is Nothing Then   ' If interface cannot be soluted
        
        With New WshNetwork
            
            strSubDirectoryPath = "VBAWorks\" & .UserName & "\CSVOutputs"
        End With
    
        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCDirectoryPath(StoragePathSearchingFlag.NetWorkOrLocalAsDirectory, strSubDirectoryPath, strSubDirectoryPath))
    End If
    
    Set mfobjGetRegParamOfStorageUNCPathColForCurrentCSVOutputDir = objCol
End Function



'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetTemporaryCodesBaseDir() As String

    Dim strModuleNameAndSubjectKey As String, strTargetPath As String
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectTmpCodesBaseKey

    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If strTargetPath = "" Then
    
        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForTemporaryCodesBaseDir())
    End If
    
    GetTemporaryCodesBaseDir = strTargetPath
End Function

'''
'''
'''
Private Function mfobjGetRegParamOfStorageUNCPathColForTemporaryCodesBaseDir() As Collection

    Dim objCol As Collection, strSubDirectoryPath As String
    
    Set objCol = Nothing
    
    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForTemporaryCodesBaseDir")
    
    If objCol Is Nothing Then   ' If interface cannot be soluted
        
        With New WshNetwork
            
            strSubDirectoryPath = "tmp\" & .UserName & "\CodesBase"
        End With
    
        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCDirectoryPath(StoragePathSearchingFlag.NetWorkOrLocalAsDirectory, strSubDirectoryPath, strSubDirectoryPath))
    End If
    
    Set mfobjGetRegParamOfStorageUNCPathColForTemporaryCodesBaseDir = objCol
End Function


'''
''' search already AIP(Azure Information Protection) labeled Excel book file path from registry CURRENT_USER
'''
Public Function GetPreparedAIPLabeledExcelBookPath(Optional ByVal vstrAlreadyAIPLabeledBookNameTip As String = "AIPLabeled") As String

    Dim objSubPaths As Collection, strTargetPath As String, strModuleNameAndSubjectKey As String
    
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectAIPLabeledExcelBookPathKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If Not mblnIsPreparedAIPLabeledExcelBookExistenceChecked Then

        If strTargetPath = "" Then
            
            strTargetPath = SearchSpecifiedFileFromCurrentHardDrivesAndSetRegistryIfFound(strModuleNameAndSubjectKey, "*" & vstrAlreadyAIPLabeledBookNameTip & "*.xls*", "", 0, 3)
        End If
    
        If strTargetPath <> "" Then
        
             mblnIsAIPLabeledExcelBookPreparedInThisComputer = True
             
             SetUNCPathToCurUserReg strModuleNameAndSubjectKey, strTargetPath
        End If
    
        mblnIsPreparedAIPLabeledExcelBookExistenceChecked = True
    End If
    
    GetPreparedAIPLabeledExcelBookPath = strTargetPath
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfGetImportantDirectoryPaths()

    Debug.Print GetDevelopmentVBARootDir()
    
    Debug.Print GetTemporaryDevelopmentVBARootDir()
    
    
    Debug.Print GetRepositoryCodesRootDir()
    
    Debug.Print GetRepositoryPackedVbaCodesRootDir()
    
    Debug.Print GetTmporaryRepositoryRootDir()
    
    
    Debug.Print GetCurrentBookOutputDir()
    
    Debug.Print GetCurrentCSVOutputDir()
    
    
    Debug.Print GetTmpCompareRootPathOriginal()
End Sub

'''
'''
'''
Private Sub msubTestToDeleteImportantDirectories()

    ClearCacheOfModuleNameAndSubjectKeyToCurrentPathDic

    IDelRInCodeAnalysisVBAOfDevelopmentVBARootDir
    IDelRInCodeAnalysisVBAOfTemporaryDevelopmentVBARootDir
    
    IDelRInCodeAnalysisVBAOfRepositoryCodesRootDir
    IDelRInCodeAnalysisVBAOfRepositoryPackedVbaCodesRootDir
    IDelRInCodeAnalysisVBAOfTmpRepositoryRootDir
    
    IDelRInCodeAnalysisVBAOfCurrentBookOutputDir
    IDelRInCodeAnalysisVBAOfCurrentCSVOutputDir
    
    IDelRInCodeExternalOfficeCommonOutOfGetTemporaryCodesBaseDir
End Sub

'''
''' Check current repository directory-path
'''
Private Sub msubSanityTestToGetCodeRepositoryDirectoryPaths()
    
    Debug.Print GetRepositoryCodesRootDir() & vbTab & "(GetRepositoryCodesRootDir)"
    
    Debug.Print GetRepositoryPackedVbaCodesRootDir() & vbTab & "(GetRepositoryPackedVbaCodesRootDir)"
    
    Debug.Print GetTmporaryRepositoryRootDir()
    
    Debug.Print GetTmpCompareRootPathOriginal()
End Sub



'**---------------------------------------------
'** For AIP controled Excel Application
'**---------------------------------------------
'''
''' About Excel AIP labeled book
'''
Private Sub msubSanityTestToGetPreparedAIPLabeledExcelBookPath()

    Debug.Print GetPreparedAIPLabeledExcelBookPath()
End Sub

Private Sub msubSanityTestToGetPreparedAIPLabeledExcelBookPathAndDeleting()

    'Debug.Print GetPreparedAIPLabeledExcelBookPath()
    
    
    mblnIsPreparedAIPLabeledExcelBookExistenceChecked = False
    
    mblnIsAIPLabeledExcelBookPreparedInThisComputer = False
    
    IDelRInAIPLabeledExcelBookPathOfGetPreparedAIPLabeledExcelBookPath
End Sub
