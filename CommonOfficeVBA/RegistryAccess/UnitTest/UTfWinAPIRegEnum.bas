Attribute VB_Name = "UTfWinAPIRegEnum"
'
'   Sanity tests to read the keys and values of the Windows 32 registory
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 31/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Private WinAPI Definitions
'///////////////////////////////////////////////
'**---------------------------------------------
'** WinAPI private constants
'**---------------------------------------------
' Root keys
Private Const HKEY_CLASSES_ROOT = &H80000000    ' key contains file name extension associations and COM class registration information such as ProgIDs, CLSIDs, and IIDs
Private Const HKEY_CURRENT_USER = &H80000001    ' Registry entries subordinate to this key define the preferences of the current user. These preferences include the settings of environment variables, data about program groups, colors, printers, network connections, and application preferences
Private Const HKEY_LOCAL_MACHINE = &H80000002   ' Registry entries subordinate to this key define the physical state of the computer, including data about the bus type, system memory, and installed hardware and software
Private Const HKEY_USERS = &H80000003   ' Registry entries subordinate to this key define the default user configuration for new users on the local computer and the user configuration for the current user
Private Const HKEY_CURRENT_CONFIG = &H80000005  ' Contains information about the current hardware profile of the local computer system. The information under HKEY_CURRENT_CONFIG describes only the differences between the current hardware configuration and the standard configuration.
Private Const HKEY_DYN_DATA = &H80000006    ' Only for Windows 95/98, Contains Windows Plug&Play information, these are included in HKEY_LOCAL_MACHINE


Private Const KEY_QUERY_VALUE = &H1&         ' query a value of registry
Private Const KEY_SET_VALUE = &H2&           ' set a value of registry
Private Const KEY_CREATE_SUB_KEY = &H4&      ' create sub key
Private Const KEY_ENUMERATE_SUB_KEYS = &H8&  ' enumerate sub keys
Private Const KEY_NOTIFY = &H10&             ' request to notify the change of the registry
Private Const KEY_CREATE_LINK = &H20&        ' create a link of the key object

Private Const KEY_READ = KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY
Private Const KEY_WRITE = KEY_SET_VALUE Or KEY_CREATE_SUB_KEY
Private Const KEY_ALL_ACCESS = KEY_READ Or KEY_WRITE Or KEY_CREATE_LINK
'Private Const KEY_ALL_ACCESS = ((STANDARD_RIGHTS_ALL Or KEY_QUERY_VALUE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK) And (Not SYNCHRONIZE))


Private Const REG_SZ = 1                  ' string type
Private Const REG_BINARY = 3              ' binary type
Private Const REG_DWORD = 4               ' 32 bit integer
Private Const REG_QWORD = 11
Private Const REG_DWORD_LITTLE_ENDIAN = 4
Private Const REG_DWORD_BIG_ENDIAN = 5
Private Const REG_EXPAND_SZ = 2
Private Const REG_LINK = 6
Private Const REG_MULTI_SZ = 7
Private Const REG_NONE = 0
Private Const REG_RESOURCE_LIST = 8



Private Const ERROR_SUCCESS As Long = 0
Private Const ERROR_NO_MORE_ITEMS As Long = &H103
Private Const ERROR_MORE_DATA As Long = &HEA


'**---------------------------------------------
'** Windows API private declarations
'**---------------------------------------------
Private Type FILETIME

    dwLowDateTime As Long    ' Low order 32 bit
    dwHighDateTime As Long   ' High order 32 bit
End Type


#If VBA7 Then

    ' enumerate registry sub-keys in the specified registry-key;
    Private Declare PtrSafe Function RegEnumKeyEx Lib "advapi32.dll" Alias "RegEnumKeyExA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpName As String, lpcbName As Long, ByVal lpReserved As Long, ByVal lpClass As String, lpcbClass As Long, lpftLastWriteTime As FILETIME) As LongPtr
    
    ' enumerate registry values in the specified registry-key
    Private Declare PtrSafe Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, lpData As Byte, lpcbData As Long) As LongPtr

    ' copy byte string
    Private Declare PtrSafe Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

    ' query a value of the registry key
    Private Declare PtrSafe Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As LongPtr

    ' close registry key
    Private Declare PtrSafe Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As LongPtr

    ' open registry key
    Private Declare PtrSafe Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As LongPtr
#Else

    Private Declare Function RegEnumKeyEx Lib "advapi32.dll" Alias "RegEnumKeyExA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpName As String, lpcbName As Long, ByVal lpReserved As Long, ByVal lpClass As String, lpcbClass As Long, lpftLastWriteTime As FILETIME) As Long

    Private Declare Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, lpData As Byte, lpcbData As Long) As Long

    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

    Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long

    Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long

    Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
#End If

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mintStringBufferSize As Long = 600

Private Const mintByteBufferSize As Long = 255

Private Const mintByteBufferSizeMinusOne As Long = mintByteBufferSize - 1


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** About UTestFormKeepStateReg form
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestOfKeepingFormPositions()

    Dim objForm As UTestFormKeepStateReg

    Set objForm = New UTestFormKeepStateReg

    objForm.Show
End Sub


'**---------------------------------------------
'** Windows API tests
'**---------------------------------------------
'''
''' sample test of RegEnumKeyEx
'''
Private Sub msubSanityTestToGetRegistryKeyByUsingRegEnumKeyEx()
    
    Dim strChildSubKey As String, intSubKeyLength As Long, strClassName As String, intClassLength As Long
    Dim udtFile As FILETIME, i As Long, strSubKey As String, intResultKey As Long
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    strSubKey = ""
    
    'strSubKey = "Software\Microsoft\Windows\CurrentVersion"
    'strSubKey = "Software\Microsoft\Windows\CurrentVersion\Action Center"

    ' get registry-key handle
    intRet = RegOpenKeyEx(HKEY_CURRENT_USER, strSubKey, 0, KEY_READ, intResultKey)

    i = 0
    
    Do
        ' get buffer
        strChildSubKey = Space(mintStringBufferSize): intSubKeyLength = Len(strChildSubKey)
    
        strClassName = Space(mintByteBufferSize): intClassLength = Len(strClassName)
        
        ' get subkey
        intRet = RegEnumKeyEx(intResultKey, i, strChildSubKey, intSubKeyLength, 0, strClassName, intClassLength, udtFile)
          
        ' determine whether exit loop end or not
        If intRet <> ERROR_SUCCESS Then
        
            Select Case intRet
            
                Case ERROR_NO_MORE_ITEMS
                    ' loop end success
                    
                Case Else
                    Debug.Print "Error Code: " & CStr(intRet)
            
                    Debug.Print Trim(Replace(strChildSubKey, vbNullChar, " "))
            End Select
        
            Exit Do
        End If
        
        ' get subkey name
        'Debug.Print Trim(Replace(strChildSubKey, vbNullChar, " "))
        Debug.Print Left(strChildSubKey, intSubKeyLength)
          
        i = i + 1
    Loop

    ' release registry-key handle
    intRet = RegCloseKey(intResultKey)
End Sub


'''
''' sample test of RegEnumValue
'''
Private Sub msubSanityTestToGetRegistryValueUsingRegEnumValue()

    Dim strValueName As String, intValueLength As Long
    Dim strClassName As String, intClassLength As Long
    
    Dim intDataType As Long        ' receives data type of value
    Dim bytData(0 To mintByteBufferSizeMinusOne) As Byte  ' 255-byte data buffer for read information
    Dim intDataLength As Long      ' size of data buffer information
    Dim strDataString As String    ' will receive data converted to a string, if necessary
    Dim intCharCounter As Long     ' counter variable
    
    Dim lngBuffer As Long, i As Long, strSubKey As String
    Dim intResultKey As Long
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    ' get buffer
    strValueName = String(mintStringBufferSize, " "): intValueLength = Len(strValueName)

    strClassName = String(mintByteBufferSize, " "): intClassLength = Len(strClassName)

    'strSubKey = "Software\Microsoft\Windows\CurrentVersion"
    strSubKey = "Software\Microsoft\Windows\CurrentVersion\Action Center"

    ' get registry-key handle
    intRet = RegOpenKeyEx(HKEY_CURRENT_USER, strSubKey, 0, KEY_READ, intResultKey)

    i = 0
    
    Do
        strValueName = Space(mintByteBufferSize)  ' 255-space buffer
        
        intValueLength = mintByteBufferSize  ' length of the string
        
        intDataLength = mintByteBufferSize  ' size of data buffer
        
        ' Get the next value to be enumerated
        intRet = RegEnumValue(intResultKey, i, strValueName, intValueLength, 0, intDataType, bytData(0), intDataLength)
        
        If intRet = ERROR_SUCCESS Then
        
            strValueName = Left(strValueName, intValueLength)
            
            Select Case intDataType
            
                Case REG_DWORD ' 32 bit integer
                
                    intRet = RegQueryValueEx(intResultKey, strValueName, 0, REG_DWORD, lngBuffer, Len(lngBuffer))

                    If intRet = ERROR_SUCCESS Then
                    
                        Debug.Print strValueName & ",  Data (Long): " & CStr(lngBuffer)
                    End If
                
                Case REG_SZ  ' null-terminated string
                
                    ' Copy the information from the byte array into the string.
                    ' We subtract one because we don't want the trailing null.
                    
                    strDataString = Space(intDataLength - 1)  ' make just enough room in the string
                    
                    CopyMemory ByVal strDataString, bytData(0), intDataLength - 1  ' copy useful data
                    
                    Debug.Print strValueName & ",  Data (string): "; strDataString
            
                Case REG_BINARY  ' binary data
                
                    ' Display the hexadecimal values of each byte of data, separated by
                    ' sapces.  Use the strDataString buffer to allow us to assure each byte
                    ' is represented by a two-character string.
                    
                    Debug.Print strValueName & ",  Data (binary):"
                    
                    For intCharCounter = 0 To intDataLength - 1  ' loop through returned information
                    
                        strDataString = Hex(bytData(intCharCounter))  ' convert value into hex
                        
                        ' If needed, add leading zero(s).
                        If Len(strDataString) < 2 Then
                        
                            strDataString = String(2 - Len(strDataString), "0") & strDataString
                        End If
                        
                        Debug.Print " "; strDataString;
                        
                    Next intCharCounter
                    
                    Debug.Print  ' end the line
            End Select
        Else
            ' determine whether exit loop end or not
            Select Case intRet
            
                Case ERROR_NO_MORE_ITEMS
                    ' loop end success
                    
                Case Else
                    Debug.Print "Error Code: " & CStr(intRet)
            End Select
        
            Exit Do
        End If
        
        i = i + 1
    Loop

    ' release registry-key handle
    intRet = RegCloseKey(intResultKey)
End Sub
