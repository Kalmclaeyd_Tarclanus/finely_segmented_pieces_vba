Attribute VB_Name = "UTfWinAPIRegistryRW"
'
'   unit test to operate Windows registry
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 31/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Private WinAPI Definitions
'///////////////////////////////////////////////
'**---------------------------------------------
'** WinAPI private constants
'**---------------------------------------------
' Root keys
Private Const HKEY_CLASSES_ROOT = &H80000000
Private Const HKEY_CURRENT_USER = &H80000001
Private Const HKEY_LOCAL_MACHINE = &H80000002
Private Const HKEY_USERS = &H80000003
Private Const HKEY_PERFORMANCE_DATA = &H80000004
Private Const HKEY_CURRENT_CONFIG = &H80000005
Private Const HKEY_DYN_DATA = &H80000006

Private Const REG_OPTION_VOLATILE = 1   ' All keys created by the function are volatile. The information is stored in memory and is not preserved when the corresponding registry hive is unloaded. For HKEY_LOCAL_MACHINE, this occurs only when the system initiates a full shutdown. For registry keys loaded by the RegLoadKey function, this occurs when the corresponding RegUnLoadKey is performed.
Private Const REG_OPTION_NON_VOLATILE = 0    ' save setting parameters to registry; This key is not volatile; this is the default. The information is stored in a file and is preserved when the system is restarted. The RegSaveKey function saves keys that are not volatile.


Private Const KEY_QUERY_VALUE = &H1&         ' query a value of registry
Private Const KEY_SET_VALUE = &H2&           ' set a value of registry
Private Const KEY_CREATE_SUB_KEY = &H4&      ' create sub key
Private Const KEY_ENUMERATE_SUB_KEYS = &H8&  ' enumerate sub keys
Private Const KEY_NOTIFY = &H10&             ' request to notify the change of the registry
Private Const KEY_CREATE_LINK = &H20&        ' create a link of the key object

Private Const KEY_READ = KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY
Private Const KEY_WRITE = KEY_SET_VALUE Or KEY_CREATE_SUB_KEY
Private Const KEY_ALL_ACCESS = KEY_READ Or KEY_WRITE Or KEY_CREATE_LINK
'Private Const KEY_ALL_ACCESS = ((STANDARD_RIGHTS_ALL Or KEY_QUERY_VALUE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK) And (Not SYNCHRONIZE))



Private Const STANDARD_RIGHTS_ALL = &H1F0000
Private Const SYNCHRONIZE = &H100000

Private Const REG_CREATED_NEW_KEY = &H1      ' create new key
Private Const REG_OPENED_EXISTING_KEY = &H2  ' open existed key

Private Const REG_SZ = 1                  ' string type
Private Const REG_BINARY = 3              ' binary type
Private Const REG_DWORD = 4               ' 32 bit integer
Private Const REG_DWORD_LITTLE_ENDIAN = 4
Private Const REG_DWORD_BIG_ENDIAN = 5
Private Const REG_EXPAND_SZ = 2
Private Const REG_LINK = 6
Private Const REG_MULTI_SZ = 7
Private Const REG_NONE = 0
Private Const REG_RESOURCE_LIST = 8

Private Const ERROR_SUCCESS As Long = 0     ' passed without any errors
Private Const ERROR_FILE_NOT_FOUND As Long = &H2    ' not found the specified file
Private Const ERROR_PATH_NOT_FOUND As Long = &H3    ' not found the specified path
Private Const ERROR_MORE_DATA As Long = &HEA        ' 234; contains more data yet
Private Const ERROR_NO_MORE_ITEMS As Long = &H103   ' 259; No more data


'**---------------------------------------------
'** Windows API private declarations
'**---------------------------------------------
'''
''' SECURITY_ATTRIBUTES declatation; The SECURITY_ATTRIBUTES structure contains the security descriptor for an object and specifies whether the handle retrieved by specifying this structure is inheritable. This structure provides security settings for objects created by various functions, such as CreateFile, CreatePipe, CreateProcess, RegCreateKeyEx, or RegSaveKeyEx.
'''
#If VBA7 Then

    Private Type SECURITY_ATTRIBUTES
    
        nLength As Long
        lpSecurityDescriptor As LongPtr
        bInheritHandle As Long
    End Type
#Else
    Private Type SECURITY_ATTRIBUTES
    
        nLength As Long     ' The size, in bytes, of this structure. Set this value to the size of the SECURITY_ATTRIBUTES structure.
        lpSecurityDescriptor As Long    ' A pointer to a SECURITY_DESCRIPTOR structure that controls access to the object. If the value of this member is NULL, the object is assigned the default security descriptor associated with the access token of the calling process.
        bInheritHandle As Long  ' A Boolean value that specifies whether the returned handle is inherited when a new process is created.
    End Type
#End If


Private Type FILETIME

    dwLowDateTime As Long    ' Low order 32 bit
    dwHighDateTime As Long   ' High order 32 bit
End Type


#If VBA7 Then
    ' enumerate registry sub-keys in the specified registry-key;
    Private Declare PtrSafe Function RegEnumKeyEx Lib "advapi32.dll" Alias "RegEnumKeyExA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpName As String, lpcbName As Long, ByVal lpReserved As Long, ByVal lpClass As String, lpcbClass As Long, lpftLastWriteTime As FILETIME) As LongPtr

    ' enumerate registry values in the specified registry-key
    Private Declare PtrSafe Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, lpData As Byte, lpcbData As Long) As LongPtr

    ' copy byte string
    Private Declare PtrSafe Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

    ' open registry key
    Private Declare PtrSafe Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, vintResultKey As Long) As LongPtr

    ' close registry key
    Private Declare PtrSafe Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As LongPtr

    ' create new registry key or open the existed key
    Private Declare PtrSafe Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal Reserved As Long, ByVal lpClass As String, ByVal dwOptions As Long, ByVal samDesired As Long, lpSecurityAttributes As SECURITY_ATTRIBUTES, phkResult As Long, lpdwDisposition As Long) As LongPtr

    ' delete registry key and the sub keys
    Private Declare PtrSafe Function RegDeleteKey Lib "advapi32.dll" Alias "RegDeleteKeyA" (ByVal hKey As Long, ByVal lpSubKey As String) As LongPtr

    ' put a value into a value-field of a registry key
    Private Declare PtrSafe Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, lpData As Any, ByVal cbData As Long) As LongPtr

    ' query a value of the registry key
    Private Declare PtrSafe Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As LongPtr

    ' query a string value of the registry key
    Private Declare PtrSafe Function RegQueryValueExStr Lib "ADVAPI32" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, ByVal lpType As Long, ByVal lpData As String, lpcbData As Long) As LongPtr
    
    Private Declare PtrSafe Function RegDeleteValue Lib "advapi32.dll" Alias "RegDeleteValueA" (ByVal hKey As Long, ByVal lpValueName As String) As LongPtr
#Else
    
    Private Declare Function RegEnumKeyEx Lib "advapi32.dll" Alias "RegEnumKeyExA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpName As String, lpcbName As Long, ByVal lpReserved As Long, ByVal lpClass As String, lpcbClass As Long, lpftLastWriteTime As FILETIME) As Long
    
    Private Declare Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, lpData As Byte, lpcbData As Long) As Long
    
    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
    
    Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, vintResultKey As Long) As Long
    
    Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
    
    Private Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal Reserved As Long, ByVal lpClass As String, ByVal dwOptions As Long, ByVal samDesired As Long, lpSecurityAttributes As SECURITY_ATTRIBUTES, phkResult As Long, lpdwDisposition As Long) As Long
    
    Private Declare Function RegDeleteKey Lib "advapi32.dll" Alias "RegDeleteKeyA" (ByVal hKey As Long, ByVal lpSubKey As String) As Long
    
    Private Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, lpData As Any, ByVal cbData As Long) As Long
    
    Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long
    
    Private Declare Function RegQueryValueExStr Lib "ADVAPI32" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, ByVal lpType As Long, ByVal lpData As String, lpcbData As Long) As Long
    
    Private Declare Function RegDeleteValue Lib "advapi32.dll" Alias "RegDeleteValueA" (ByVal hKey As Long, ByVal lpValueName As String) As Long
#End If
 


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Use VBA.Interaction
'**---------------------------------------------
Private Sub SanityTestGetSetting()

    VBA.Interaction.SaveSetting "VBASample", "Main", "Test", "Sample"
    
    Debug.Print GetSetting("VBASample", "Main", "Test", "Sample")
    
    
    VBA.Interaction.SaveSetting "VBASample", "Main\SubMain", "Test", "ChildSample"

    Debug.Print GetSetting("VBASample", "Main\SubMain", "Test", "ChildSample")
End Sub


'**---------------------------------------------
'** Use Windows API
'**---------------------------------------------
'''
''' create a registry key
'''
Private Sub msubSanityTestToRegCreateKeyEx()

    Dim intRootKey As Long, strSubKey As String, udtSECURITYATTRIBUTES As SECURITY_ATTRIBUTES
    
Dim intResultKey As Long, intDisposition As Long  ' A pointer to a variable that receives one of the following disposition values; REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If
    intRootKey = HKEY_CURRENT_USER
    
    strSubKey = "Software\Microsoft\Windows\CurrentVersion\Test"

    intRet = RegCreateKeyEx(intRootKey, strSubKey, 0&, vbNullString, REG_OPTION_NON_VOLATILE, KEY_CREATE_SUB_KEY, udtSECURITYATTRIBUTES, intResultKey, intDisposition)

    RegCloseKey intResultKey
    
    Debug.Print "Make HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Test"
End Sub


'''
''' delete a registry key, 1st method
'''
Private Sub msubSanityTestToRegDeleteKey()

    Dim intRootKey As Long, strSubKey As String
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If
      
    intRootKey = HKEY_CURRENT_USER
    
    strSubKey = "Software\Microsoft\Windows\CurrentVersion\Test"

    intRet = RegDeleteKey(intRootKey, strSubKey)
    
    If intRet <> ERROR_SUCCESS Then
    
        Debug.Print "Failed to RegDeleteKey - " & GetDllErrorMessage(CInt(intRet))
    End If
    
    Debug.Print "Deleted - HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Test"
End Sub


'''
''' delete a registry key, 2nd method
'''
Private Sub msubSanityTestToRegDeleteKey02()

    Dim intRootKey As Long, strSubKey As String, intResultKey As Long
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If
          
    intRootKey = HKEY_CURRENT_USER
    
    strSubKey = "Software\Microsoft\Windows\CurrentVersion"
    
    ' open registry key
    intRet = RegOpenKeyEx(intRootKey, strSubKey, 0&, KEY_ENUMERATE_SUB_KEYS, intResultKey)

    If intRet <> ERROR_SUCCESS Then
    
        Debug.Print "Failed to call RegOpenKeyEx function - " & GetDllErrorMessage(CInt(intRet))
        
        Exit Sub
    End If
    
    intRet = RegDeleteKey(intResultKey, "Test")

    RegCloseKey intResultKey
    
    Debug.Print "Deleted : HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Test"
End Sub

'''
''' set a value into the registry key
'''
Private Sub msubSanityTestToRegSetValueEx()

    Dim intRootKey As Long, strSubKey As String, intResultKey As Long
    
    Dim varData As Variant, bytData(1 To 5) As Byte, i As Long
    
#If VBA7 Then

    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If
  
    intRootKey = HKEY_CURRENT_USER
    
    strSubKey = "Software\Microsoft\Windows\CurrentVersion\Test"
    
    ' Open registry key
    intRet = RegOpenKeyEx(intRootKey, strSubKey, 0&, KEY_SET_VALUE, intResultKey)

    If intRet <> ERROR_SUCCESS Then
    
        Debug.Print "Failed to call RegOpenKeyEx function - " & GetDllErrorMessage(CInt(intRet))
        
        Exit Sub
    End If
    
    ' Store the string
    varData = "StringTest"
    
    msubTestToRegSetValueEx intResultKey, varData
    
    ' Store the numeric value
    varData = 12345
    
    msubTestToRegSetValueEx intResultKey, varData
    
    ' Store the binary value
    For i = 1 To 5
    
        bytData(i) = i
    Next
    
    varData = bytData

    msubTestToRegSetValueEx intResultKey, varData

    Call RegCloseKey(intResultKey)
        
    Debug.Print "Stored value - HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Test"
End Sub

'''
'''
'''
Private Sub msubTestToRegSetValueEx(ByVal vintResultKey As Long, ByRef rvarDataValue As Variant)

    Select Case varType(rvarDataValue)
        
        Case vbString                           ' string
        
            WriteWinRegistryValueSimply vintResultKey, "String", rvarDataValue

        Case vbInteger, vbLong, vbBoolean       ' 32bit integer
        
            WriteWinRegistryValueSimply vintResultKey, "Long", rvarDataValue

        Case vbByte Or vbArray                  ' binary-data
        
            WriteWinRegistryValueSimply vintResultKey, "Binary", rvarDataValue

        Case Else
        
            WriteWinRegistryValueSimply vintResultKey, "AnyType", rvarDataValue
    End Select
End Sub


'''
''' query a value from the registry key
'''
Private Sub msubSanityTestToRegQueryValueEx01()

    Dim intRootKey As Long, strSubKey As String, intResultKey As Long
    
    Dim strBuffer As String * 80, lngBuffer As Long, bytBuffer(1 To 5) As Byte, i As Long
    
    Dim strBinary As String
  
  
#If VBA7 Then

    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If
  
    intRootKey = HKEY_CURRENT_USER
    
    strSubKey = "Software\Microsoft\Windows\CurrentVersion\Test"
    
    intRet = RegOpenKeyEx(intRootKey, strSubKey, 0&, KEY_QUERY_VALUE, intResultKey)

    If intRet <> ERROR_SUCCESS Then
        
        Debug.Print "Failed to RegOpenKeyEx - " & GetDllErrorMessage(CInt(intRet))
        
        Exit Sub
    End If


    strSubKey = "String"
    
    intRet = RegQueryValueEx(intResultKey, strSubKey, 0&, REG_SZ, ByVal strBuffer, Len(strBuffer))
                        
    Debug.Print strSubKey & vbTab & Left(strBuffer, InStr(strBuffer, vbNullChar) - 1)
    
    ' 32 bit integer
    strSubKey = "Long"
    
    intRet = RegQueryValueEx(intResultKey, strSubKey, 0&, REG_DWORD, lngBuffer, Len(lngBuffer))
                        
    Debug.Print strSubKey & vbTab & lngBuffer
    
    ' binary data
    strSubKey = "Binary"
    
    intRet = RegQueryValueEx(intResultKey, strSubKey, 0&, REG_BINARY, bytBuffer(1), 5)
                        
    For i = 1 To 5
    
        strBinary = strBinary & Space(1) & Format(Hex(bytBuffer(i)), "00")
    Next
    
    strBinary = Mid(strBinary, 2)
    
    Debug.Print strSubKey & vbTab & strBinary
    
    Call RegCloseKey(intResultKey)
End Sub


'''
''' delete a value from the registry key
'''
Private Sub msubSanityTestToRegDeleteValue01()

    Dim intRootKey As Long, strSubKey As String, intResultKey As Long
    
    Dim strValueName As String

#If VBA7 Then

    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If
  
    intRootKey = HKEY_CURRENT_USER
    
    strSubKey = "Software\Microsoft\Windows\CurrentVersion\Test"
    
    intRet = RegOpenKeyEx(intRootKey, strSubKey, 0&, KEY_SET_VALUE, intResultKey)

    If intRet <> ERROR_SUCCESS Then
    
        Debug.Print "Failed to RegOpenKeyEx - " & GetDllErrorMessage(CInt(intRet))
        
        Exit Sub
    End If

    strValueName = "String"

    intRet = RegDeleteValue(intResultKey, strValueName)

    RegCloseKey intResultKey
        
    Debug.Print "Deleted - HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Test"
End Sub

