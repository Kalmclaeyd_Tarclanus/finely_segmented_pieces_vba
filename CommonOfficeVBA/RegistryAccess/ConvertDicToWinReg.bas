Attribute VB_Name = "ConvertDicToWinReg"
'
'   Conversion between dictionary and Windows Registory
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and Scripting.Dictionary
'       Dependent on WinRegGeneral.bas, WinRegDelete.bas, CompareDictionaries.bas
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 13/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrRegKeySecureOfficeCommonVBADictionaryTest As String = "HKEY_CURRENT_USER\Software\VBASecurePosition\OfficeCommonVBA\TestForDictionaryConversion"


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Dictionary convert small tools
'**---------------------------------------------
'''
'''
'''
Public Function GetFilteredDicExceptForSpecifiedPluralKeys(ByRef robjInputDic As Scripting.Dictionary, ByVal vstrExceptKeysDelimitedByComma As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, objExceptDic As Scripting.Dictionary, varKey As Variant
    
    Set objDic = New Scripting.Dictionary
    
    Set objExceptDic = GetDicOnlyKeysFromLineDelimitedChar(vstrExceptKeysDelimitedByComma)
    
    With robjInputDic
    
        For Each varKey In .Keys
    
            If Not objExceptDic.Exists(varKey) Then
            
                objDic.Add varKey, .Item(varKey)
            End If
        Next
    End With
    
    Set GetFilteredDicExceptForSpecifiedPluralKeys = objDic
End Function


'**---------------------------------------------
'** Dictionary conversion based on Windows registory
'**---------------------------------------------
'''
''' write Windows-registry from a Dictionary object
'''
Public Sub UpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromDic(ByVal vstrFullParentRegKey As String, _
        ByVal vstrChildSubRegKeyName As String, _
        ByVal vobjDic As Scripting.Dictionary)


    Dim strFullRegKey As String, objReadDic As Scripting.Dictionary, blnIsRegKeyOpened As Boolean, intResultKey As Long
    
    strFullRegKey = vstrFullParentRegKey & "\" & vstrChildSubRegKeyName
    
    If Not IsWinFullRegKeyExisted(strFullRegKey) Then
    
        CreateWinRegistryKey strFullRegKey
        
        intResultKey = CreateWinRegistryKeyAndKeepOpening(blnIsRegKeyOpened, strFullRegKey)
        
        If blnIsRegKeyOpened Then
        
            WriteWinRegistryFromDicByOpened intResultKey, vobjDic
            
            CloseWinRegistryKeyHandle intResultKey
        End If
    Else
        Set objReadDic = GetDicFromReadingWinRegValues(vstrFullParentRegKey)
    
        If IsAnyDifferencesBetweenTwoDictionariesAboutBothKeyAndValue(vobjDic, objReadDic) Then
        
            ' Delete all
            
            DeleteRegistryKeyWithAllChildKeys strFullRegKey
        
            WriteWinRegistryFromDic strFullRegKey, vobjDic
        End If
    End If
End Sub

'''
'''
'''
Public Sub DeleteWinRegKeyValuesBySpecifyingChildSubKeyName(ByVal vstrFullParentRegKey As String, _
        ByVal vstrChildSubRegKeyName As String)

    Dim strFullRegKey As String
    
    strFullRegKey = vstrFullParentRegKey & "\" & vstrChildSubRegKeyName

    If IsWinFullRegKeyExisted(strFullRegKey) Then

        ' Delete all
            
        DeleteRegistryKeyWithAllChildKeys strFullRegKey
    End If
End Sub

'''
''' get Dictionary from a Windows-registry key
'''
Public Function GetDicBySpecifyingChildSubKeyNameFromReadingWinRegValues(ByVal vstrFullParentRegKey As String, _
        ByVal vstrChildSubRegKeyName As String) As Scripting.Dictionary

    Dim strFullRegKey As String, objDic As Scripting.Dictionary
    
    strFullRegKey = vstrFullParentRegKey & "\" & vstrChildSubRegKeyName

    If IsWinFullRegKeyExisted(strFullRegKey) Then
    
        Set objDic = GetDicFromReadingWinRegValues(strFullRegKey)
    Else
        Set objDic = New Scripting.Dictionary
    End If

    Set GetDicBySpecifyingChildSubKeyNameFromReadingWinRegValues = objDic
End Function


'''
'''
'''
Public Function GetReadWinRegValuesTemporaryDirectoryPath() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\WinRegValues"
    
    ForceToCreateDirectory strDir
    
    GetReadWinRegValuesTemporaryDirectoryPath = strDir
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////


'**---------------------------------------------
'** About getting testing dictionary objects
'**---------------------------------------------
'''
'''
'''
Private Function mfobjGetSample01Dic() As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add "Sample-Key-01", "a"
    
        .Add "Sample-Key-02", 12
        
        .Add "Sample-Key-03", 5.65
    End With

    Set mfobjGetSample01Dic = objDic
End Function

'''
'''
'''
Private Function mfobjGetSample02Dic() As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add "Sample Key 01", "bcd"
    
        .Add "Sample Key 02", 52
        
        .Add "Sample Key 03", Now()
    End With

    Set mfobjGetSample02Dic = objDic
End Function

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** About above tools tests
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToUpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromDic()

    Dim strChildSubKeyName As String, objDic As Scripting.Dictionary
    
    strChildSubKeyName = "SetSubKey01"
    
    UpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromDic mstrRegKeySecureOfficeCommonVBADictionaryTest, strChildSubKeyName, mfobjGetSample01Dic()
    
    ' This will not be updated.
    UpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromDic mstrRegKeySecureOfficeCommonVBADictionaryTest, strChildSubKeyName, mfobjGetSample01Dic()
    
    ' This will be updated.
    UpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromDic mstrRegKeySecureOfficeCommonVBADictionaryTest, strChildSubKeyName, mfobjGetSample02Dic()

    ' Delete
    DeleteWinRegKeyValuesBySpecifyingChildSubKeyName mstrRegKeySecureOfficeCommonVBADictionaryTest, strChildSubKeyName
    
    ' Re-add
    UpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromDic mstrRegKeySecureOfficeCommonVBADictionaryTest, strChildSubKeyName, mfobjGetSample02Dic()
    

    Set objDic = GetDicBySpecifyingChildSubKeyNameFromReadingWinRegValues(mstrRegKeySecureOfficeCommonVBADictionaryTest, strChildSubKeyName)

    DebugDic objDic
End Sub

'**---------------------------------------------
'** About test key-values
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCreateSampleRegKeyValues()

    WriteWinRegistryFromDic mstrRegKeySecureOfficeCommonVBADictionaryTest & "\SetSubKey01", mfobjGetSample01Dic()
End Sub

'''
'''
'''
Private Sub msubSanityTestToDeleteWinRegistryOnlyValuesFromDic()

    DeleteWinRegistryOnlyValuesFromDic mstrRegKeySecureOfficeCommonVBADictionaryTest & "\SetSubKey01", mfobjGetSample01Dic()
End Sub

'''
'''
'''
Private Sub msubSanityTestToReadSampleRegKeyValues()

    Dim objDic As Scripting.Dictionary
    
    Set objDic = GetDicFromReadingWinRegValues(mstrRegKeySecureOfficeCommonVBADictionaryTest & "\SetSubKey01")

    DebugDic objDic
End Sub

'''
'''
'''
Private Sub msubSanityTestToReadSampleRegKeyValuesAsOneNestedDic()

    Dim objOneNestedDic As Scripting.Dictionary
    
    Set objOneNestedDic = GetOneNestedDicFromReadingWinRegChiledKeysAndValuesOfEachKey(mstrRegKeySecureOfficeCommonVBADictionaryTest)

    DebugDic objOneNestedDic
End Sub

'**---------------------------------------------
'** About create or delete Win-registry key
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCreateTestRootRegKey()

    CreateWinRegistryKey mstrRegKeySecureOfficeCommonVBADictionaryTest
End Sub

'''
'''
'''
Private Sub msubSanityTestToDeleteTestRootRegKey()

    DeleteWinRegistryKey mstrRegKeySecureOfficeCommonVBADictionaryTest
End Sub






