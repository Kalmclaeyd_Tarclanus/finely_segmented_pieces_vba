Attribute VB_Name = "WinRegView"
'
'   Read the Windows 32 bit registries and set to data-table, such as Collection or Scripting.Dictionary
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 31/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Private WinAPI declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** WinAPI private constants
'**---------------------------------------------
' Root keys
Private Const HKEY_CLASSES_ROOT = &H80000000    ' key contains file name extension associations and COM class registration information such as ProgIDs, CLSIDs, and IIDs
Private Const HKEY_CURRENT_USER = &H80000001    ' Registry entries subordinate to this key define the preferences of the current user. These preferences include the settings of environment variables, data about program groups, colors, printers, network connections, and application preferences
Private Const HKEY_LOCAL_MACHINE = &H80000002   ' Registry entries subordinate to this key define the physical state of the computer, including data about the bus type, system memory, and installed hardware and software
Private Const HKEY_USERS = &H80000003   ' Registry entries subordinate to this key define the default user configuration for new users on the local computer and the user configuration for the current user
Private Const HKEY_CURRENT_CONFIG = &H80000005  ' Contains information about the current hardware profile of the local computer system. The information under HKEY_CURRENT_CONFIG describes only the differences between the current hardware configuration and the standard configuration.
Private Const HKEY_DYN_DATA = &H80000006    ' Only for Windows 95/98, Contains Windows Plug&Play information, these are included in HKEY_LOCAL_MACHINE


Private Const KEY_QUERY_VALUE = &H1&         ' query a value of registry
Private Const KEY_SET_VALUE = &H2&           ' set a value of registry
Private Const KEY_CREATE_SUB_KEY = &H4&      ' create sub key
Private Const KEY_ENUMERATE_SUB_KEYS = &H8&  ' enumerate sub keys
Private Const KEY_NOTIFY = &H10&             ' request to notify the change of the registry
Private Const KEY_CREATE_LINK = &H20&        ' create a link of the key object

Private Const KEY_READ = KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY
Private Const KEY_WRITE = KEY_SET_VALUE Or KEY_CREATE_SUB_KEY
Private Const KEY_ALL_ACCESS = KEY_READ Or KEY_WRITE Or KEY_CREATE_LINK
'Private Const KEY_ALL_ACCESS = ((STANDARD_RIGHTS_ALL Or KEY_QUERY_VALUE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK) And (Not SYNCHRONIZE))


Private Const REG_SZ = 1                  ' string type
Private Const REG_BINARY = 3              ' binary type
Private Const REG_DWORD = 4               ' 32 bit integer
Private Const REG_QWORD = 11
Private Const REG_DWORD_LITTLE_ENDIAN = 4
Private Const REG_DWORD_BIG_ENDIAN = 5
Private Const REG_EXPAND_SZ = 2
Private Const REG_LINK = 6
Private Const REG_MULTI_SZ = 7
Private Const REG_NONE = 0
Private Const REG_RESOURCE_LIST = 8


Private Const ERROR_SUCCESS As Long = 0
Private Const ERROR_NO_MORE_ITEMS As Long = &H103
Private Const ERROR_MORE_DATA As Long = &HEA


'**---------------------------------------------
'** Windows API private declarations
'**---------------------------------------------
Private Type FILETIME

    dwLowDateTime As Long    ' Low order 32 bit
    dwHighDateTime As Long   ' High order 32 bit
End Type

#If VBA7 Then

    ' enumerate registry sub-keys in the specified registry-key;
    Private Declare PtrSafe Function RegEnumKeyEx Lib "advapi32.dll" Alias "RegEnumKeyExA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpName As String, lpcbName As Long, ByVal lpReserved As Long, ByVal lpClass As String, lpcbClass As Long, lpftLastWriteTime As FILETIME) As LongPtr
    
    ' enumerate registry values in the specified registry-key
    Private Declare PtrSafe Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, lpData As Byte, lpcbData As Long) As LongPtr

    ' copy byte string
    Private Declare PtrSafe Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

    ' query a value of the registry key
    Private Declare PtrSafe Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As LongPtr

    ' close registry key
    Private Declare PtrSafe Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As LongPtr

    ' open registry key
    Private Declare PtrSafe Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As LongPtr
#Else

    Private Declare Function RegEnumKeyEx Lib "advapi32.dll" Alias "RegEnumKeyExA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpName As String, lpcbName As Long, ByVal lpReserved As Long, ByVal lpClass As String, lpcbClass As Long, lpftLastWriteTime As FILETIME) As Long

    Private Declare Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, lpData As Byte, lpcbData As Long) As Long

    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

    Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long

    Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long

    Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
#End If

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mintStringBufferSize As Long = 600
Private Const mintByteBufferSize As Long = 255
Private Const mintByteBufferSizeMinusOne As Long = mintByteBufferSize - 1

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** load regstries
'**---------------------------------------------
'''
''' get Subkeys with searching child Subkeys; This is subset of GetRegValuesWithChildSubKeys
'''
Public Sub GetRegSubKeys(ByVal vintExplosionHierarchicalOrder As Long, ByVal vintRootKey As Long, ByVal vstrSubKey As String, ByRef robjSubKeys As Collection, Optional vblnSearchChildSubkey As Boolean = True)

    GetRegValuesWithChildSubKeys vintExplosionHierarchicalOrder, vintRootKey, vstrSubKey, robjSubKeys, Nothing, vblnSearchChildSubkey, RegCollectTargetFlag.CollectWinRegSubKeys
End Sub

'''
''' get registry values with searching child Subkeys; This is subset of GetRegValuesWithChildSubKeys
'''
Public Sub GetRegValues(ByVal vintExplosionHierarchicalOrder As Long, ByVal vintRootKey As Long, ByVal vstrSubKey As String, ByRef robjRegValues As Collection, Optional vblnSearchChildSubkey As Boolean = True)

    GetRegValuesWithChildSubKeys vintExplosionHierarchicalOrder, vintRootKey, vstrSubKey, Nothing, robjRegValues, vblnSearchChildSubkey, RegCollectTargetFlag.CollectWinRegValues
End Sub

'''
''' recursive call
'''
''' get registry values and subkeys
'''
''' <Argument>vintRootKey: for example, HKEY_CURRENT_USER</Argument>
Public Sub GetRegValuesWithChildSubKeys(ByVal vintExplosionHierarchicalOrder As Long, _
        ByVal vintRootKey As Long, _
        ByVal vstrSubKey As String, _
        ByRef robjSubKeys As Collection, _
        ByRef robjRegValues As Collection, _
        Optional ByVal vblnSearchChildSubkey As Boolean = True, _
        Optional ByVal venmRegCollectTargetFlag As RegCollectTargetFlag = RegCollectTargetFlag.CollectWinRegSubKeys Or RegCollectTargetFlag.CollectWinRegValues)
    
    
    Dim objChildSubKeys As Collection, intValuesExplosionHierarchicalOrder As Long
    ' to collect child SubKeys
    Dim strChildSubKey As String, strFullRegKey As String, strFullChildSubKey As String
    Dim varChildSubKey As Variant, strItems() As String
    Dim intResultKey As Long
    
#If VBA7 Then

    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    ' get registry-key handle
    intRet = RegOpenKeyEx(vintRootKey, vstrSubKey, 0, KEY_READ, intResultKey)
    
    Set objChildSubKeys = GetWinRegChildSubKeyNamesByOpened(intResultKey)
    

    ' get registry values
    If (venmRegCollectTargetFlag And RegCollectTargetFlag.CollectWinRegValues) > 0 Then
    
        intValuesExplosionHierarchicalOrder = vintExplosionHierarchicalOrder - 1

        strFullRegKey = GetFullWinRegKey(vintRootKey, vstrSubKey)
    
        AddReadWinRegValuesIntoColToArgByOpened robjRegValues, intResultKey, intValuesExplosionHierarchicalOrder, strFullRegKey
    End If

    ' release registry-key handle
    intRet = RegCloseKey(intResultKey)
    
    If objChildSubKeys.Count > 0 Then
        
        For Each varChildSubKey In objChildSubKeys
            
            strChildSubKey = varChildSubKey
            
            strFullChildSubKey = GetFullWinSubRegKey(vstrSubKey, strChildSubKey)
        
            ' ! Attention: It may need for the debugging. It may be circular looping
            
            strItems = Split(strFullChildSubKey, strChildSubKey)
            
            If UBound(strItems) > 4 Then
            
                vblnSearchChildSubkey = False
            End If
        
            If (venmRegCollectTargetFlag And RegCollectTargetFlag.CollectWinRegSubKeys) > 0 Then
            
                msubSetRegistrySubKey robjSubKeys, vintExplosionHierarchicalOrder, GetFullWinRegKey(vintRootKey, vstrSubKey) & strChildSubKey
            End If
            
            ' recursive call
            If vblnSearchChildSubkey Then
            
                GetRegValuesWithChildSubKeys vintExplosionHierarchicalOrder + 1, vintRootKey, strFullChildSubKey, robjSubKeys, robjRegValues, vblnSearchChildSubkey, venmRegCollectTargetFlag
            End If
        Next
    End If
End Sub

'''
'''
'''
Public Function GetWinRegChildSubKeys(ByVal vintRootKey As Long, ByVal vstrSubKey As String) As Collection
    
    Dim objChildSubKeys As Collection, intResultKey As Long
#If VBA7 Then

    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    ' get registry-key handle
    intRet = RegOpenKeyEx(vintRootKey, vstrSubKey, 0, KEY_READ, intResultKey)
    
    Set objChildSubKeys = GetWinRegChildSubKeyNamesByOpened(intResultKey)
    
    ' release registry-key handle
    intRet = RegCloseKey(intResultKey)
    
    Set GetWinRegChildSubKeys = objChildSubKeys
End Function


'''
'''
'''
Public Function GetWinRegChildSubKeyNamesByOpened(ByRef rintOpenedResultKey As Long) As Collection

    Dim objChildSubKeys As Collection, strChildSubKey As String, intSubKeyLength As Long
    Dim strClassName As String, intClassLength As Long, udtFile As FILETIME, i As Long
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    Set objChildSubKeys = New Collection
    
    i = 0
    
    Do
        ' get buffer
        strChildSubKey = Space(mintStringBufferSize)
        
        intSubKeyLength = Len(strChildSubKey)
    
        strClassName = Space(mintByteBufferSize)
        
        intClassLength = Len(strClassName)
        
        ' get subkey
        intRet = RegEnumKeyEx(rintOpenedResultKey, i, strChildSubKey, intSubKeyLength, 0, strClassName, intClassLength, udtFile)
          
        ' determine whether exit loop end or not
        If intRet <> ERROR_SUCCESS Then
        
            Select Case intRet
            
                Case ERROR_NO_MORE_ITEMS
                
                    ' loop end success
                    
                Case Else
                
                    Debug.Print "Error Code: " & CStr(intRet)
            
                    Debug.Print Trim(Replace(strChildSubKey, vbNullChar, " "))
            End Select
        
            Exit Do
        End If
        
        ' get subkey name
        'Debug.Print Trim(Replace(strChildSubKey, vbNullChar, " "))
        
        'Debug.Print Left(strChildSubKey, intSubKeyLength)
        
        objChildSubKeys.Add Left(strChildSubKey, intSubKeyLength)
        
        i = i + 1
    Loop

    Set GetWinRegChildSubKeyNamesByOpened = objChildSubKeys
End Function

'''
'''
'''
''' <Argument>robjRegValues: Output</Argument>
''' <Argument>vintOpenedResultKey: Input</Argument>
''' <Argument>vintValuesExplosionHierarchicalOrder: Input</Argument>
''' <Argument>vstrFullRegKey: Input</Argument>
Public Sub AddReadWinRegValuesIntoColToArgByOpened(ByRef robjRegValues As Collection, _
        ByVal vintOpenedResultKey As Long, _
        ByVal vintValuesExplosionHierarchicalOrder As Long, _
        ByVal vstrFullRegKey As String)
          
    Dim i As Long
    ' to collect registry values
    Dim strValueName As String, intValueLength As Long
    Dim intDataType As Long        ' receives data type of value
    Dim bytData(0 To mintByteBufferSizeMinusOne) As Byte  ' 255-byte data buffer for read information
    Dim intDataLength As Long      ' size of data buffer information
    Dim strDataString As String    ' will receive data converted to a string, if necessary
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    i = 0
    
    Do
        strValueName = Space(mintStringBufferSize)  ' 255-space buffer
        
        intValueLength = mintByteBufferSize  ' length of the string
        
        intDataLength = mintByteBufferSize  ' size of data buffer
        
        ' Get the next value to be enumerated
        intRet = RegEnumValue(vintOpenedResultKey, i, strValueName, intValueLength, 0, intDataType, bytData(0), intDataLength)
        
        If intRet = ERROR_SUCCESS Then
        
            strValueName = Left(strValueName, intValueLength)
            
            AddReadWinRegValuesIntoColToArgFromEnumeratedWinRegistries robjRegValues, vintOpenedResultKey, intDataType, vintValuesExplosionHierarchicalOrder, vstrFullRegKey, strValueName, bytData, intDataLength
        Else
            ' determine whether exit loop end or not
            Select Case intRet
            
                Case ERROR_NO_MORE_ITEMS
                    ' loop end success
                
                Case ERROR_MORE_DATA
                    Debug.Print "Error Code: " & CStr(intRet) & " The more data should have existed. [" & Left(strValueName, intValueLength) & "]"
                
                Case Else
                    Debug.Print "Error Code: " & CStr(intRet) & ", " & GetDllErrorMessage(CInt(intRet))
            End Select
        
            Exit Do
        End If
        
        i = i + 1
    Loop
End Sub


'''
''' inputted values are the outputs of RegEnumValue
'''
''' <Argument>robjRegValues: Output</Argument>
''' <Argument>vintOpenedResultKey: Input</Argument>
''' <Argument>vintDataType: Input - receives data type of value</Argument>
''' <Argument>rbytData: 255-byte data buffer for read information</Argument>
''' <Argument>vintDataLength: size of data buffer information</Argument>
Public Sub AddReadWinRegValuesIntoColToArgFromEnumeratedWinRegistries(ByRef robjRegValues As Collection, _
        ByVal vintOpenedResultKey As Long, _
        ByVal vintDataType As Long, _
        ByVal vintValuesExplosionHierarchicalOrder As Long, _
        ByVal vstrFullRegKey As String, _
        ByVal vstrValueName As String, _
        ByRef rbytData() As Byte, _
        ByVal vintDataLength As Long)
    
    
    Dim strDataString As String    ' will receive data converted to a string, if necessary
    Dim intCharCounter As Long     ' counter variable
    Dim strPrintedBinaryValue As String, lngBuffer As Long
    
#If Win64 Then

    Dim int64Buffer As LongLong
#Else
    Dim int64Buffer As Long
#End If

#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    Select Case vintDataType
    
        Case REG_DWORD ' 32 bit unsigned integer
            
            intRet = RegQueryValueEx(vintOpenedResultKey, vstrValueName, 0, REG_DWORD, lngBuffer, Len(lngBuffer))

            If intRet = ERROR_SUCCESS Then
                'Debug.Print vstrValueName & ",  Data (Long): " & CStr(lngBuffer)
                
                robjRegValues.Add mfobjGetRowItemOfWindowsRegistryInfo(vintValuesExplosionHierarchicalOrder, vstrValueName, lngBuffer, "REG_DWORD", vstrFullRegKey)
            End If
        
        Case REG_QWORD  ' 64 bit unsigned integer
            
            ' ! Attention This need to correct, these are invalid codes...
            
            intRet = RegQueryValueEx(vintOpenedResultKey, vstrValueName, 0, REG_QWORD, int64Buffer, Len(int64Buffer))

            If intRet = ERROR_SUCCESS Then
                
                Debug.Print vstrValueName & ",  Data (Long): " & CStr(int64Buffer)
                
                robjRegValues.Add mfobjGetRowItemOfWindowsRegistryInfo(vintValuesExplosionHierarchicalOrder, vstrValueName, int64Buffer, "REG_QWORD", vstrFullRegKey)
            End If
        
        Case REG_SZ  ' null-terminated string
        
            ' Copy the information from the byte array into the string.
            ' We subtract one because we don't want the trailing null.
            
            strDataString = Space(vintDataLength - 1)  ' make just enough room in the string
            
            CopyMemory ByVal strDataString, rbytData(0), vintDataLength - 1  ' copy useful data
            
            robjRegValues.Add mfobjGetRowItemOfWindowsRegistryInfo(vintValuesExplosionHierarchicalOrder, vstrValueName, strDataString, "REG_SZ", vstrFullRegKey)
    
        Case REG_EXPAND_SZ  ' String able to extract of the Windows environment variables�i%�`%�j
        
            strDataString = Space(vintDataLength - 1)  ' make just enough room in the string
            
            CopyMemory ByVal strDataString, rbytData(0), vintDataLength - 1  ' copy useful data
            
            Debug.Print vstrValueName & ",  Data (Expandable-string): " & strDataString
            
            ' This can be extracted by plural methods, for example, it is the objShell.ExpandEnvironmentStrings("%SystemRoot%")
            
            robjRegValues.Add mfobjGetRowItemOfWindowsRegistryInfo(vintValuesExplosionHierarchicalOrder, vstrValueName, strDataString, "REG_EXPAND_SZ", vstrFullRegKey)
    
        Case REG_MULTI_SZ   ' multiline strings
        
            strDataString = Space(vintDataLength - 1)  ' make just enough room in the string
            
            CopyMemory ByVal strDataString, rbytData(0), vintDataLength - 1  ' copy useful data
            
            Debug.Print vstrValueName & ",  Data (Multline-string): " & strDataString
            
            strDataString = Replace(strDataString, Chr(0), vbLf)
            
            robjRegValues.Add mfobjGetRowItemOfWindowsRegistryInfo(vintValuesExplosionHierarchicalOrder, vstrValueName, strDataString, "REG_MULTI_SZ", vstrFullRegKey)
    
        Case REG_BINARY  ' binary data
        
            ' Display the hexadecimal values of each byte of data, separated by
            ' sapces.  Use the strDataString buffer to allow us to assure each byte
            ' is represented by a two-character string.
            
            'Debug.Print vstrValueName & ",  Data (binary):";
            strPrintedBinaryValue = ""
            
            For intCharCounter = 0 To vintDataLength - 1  ' loop through returned information
            
                strDataString = Hex(rbytData(intCharCounter))  ' convert value into hex
                
                ' If needed, add leading zero(s).
                
                If Len(strDataString) < 2 Then
                    
                    strDataString = String(2 - Len(strDataString), "0") & strDataString
                End If
                'Debug.Print " "; strDataString;
                
                strPrintedBinaryValue = strPrintedBinaryValue & " " & strDataString
            Next
            'Debug.Print  ' end the line
    
            strPrintedBinaryValue = Trim(strPrintedBinaryValue)
            
            robjRegValues.Add mfobjGetRowItemOfWindowsRegistryInfo(vintValuesExplosionHierarchicalOrder, vstrValueName, strPrintedBinaryValue, "REG_BINARY", vstrFullRegKey)
    End Select
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetRowItemOfWindowsRegistryInfo(ByRef rintHierarchicalOrder As Long, ByRef rstrValueName As String, ByVal vvarValue As Variant, ByVal vstrDataName As String, ByRef rstrFullRegKey As String) As Collection

    Dim objRowItem As Collection

    Set objRowItem = New Collection
    
    With objRowItem
    
        .Add rintHierarchicalOrder
    
        .Add rstrValueName
        
        .Add vvarValue
        
        .Add vstrDataName
    
        .Add rstrFullRegKey
    End With
    
    Set mfobjGetRowItemOfWindowsRegistryInfo = objRowItem
End Function



'''
'''
'''
Private Sub msubSetRegistrySubKey(ByRef robjSubKeys As Collection, ByVal vintExplosionHierarchicalOrder As Long, ByVal vstrFullRegKey As String)
    
    Dim objRowItems As Collection

    Set objRowItems = New Collection
    
    With objRowItems
        
        .Add vintExplosionHierarchicalOrder
        
        .Add vstrFullRegKey
    End With

    robjSubKeys.Add objRowItems
End Sub


