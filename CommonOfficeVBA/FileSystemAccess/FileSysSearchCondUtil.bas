Attribute VB_Name = "FileSysSearchCondUtil"
'
'   utilities for both FileSysSearchCondition and FileSysSearchResult
'   Tools for listing files dependent on FileSysSearchCondition class
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 22/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** get various FileSysSearchCondition
'**---------------------------------------------
'''
''' search files for return Collection
'''
''' <Argument>vstrStringOfFileInfomationTypesDelimitedByComma: Input</Argument>
''' <Argument>vstrRawWildCardsByDelimitedChar: Input</Argument>
''' <Argument>vintMinimumHierarchicalOrderToSearch: Input</Argument>
''' <Argument>vintMaximumHierarchicalOrderToSearch: Input</Argument>
''' <Argument>vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma</Argument>
Public Function GetFSSearchConditionAsCollectionForSearchingFilesGenerally(Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "RelativeFullPath", _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As FileSysSearchCondition

    Dim objFSSearchCondition As FileSysSearchCondition
    
    Set objFSSearchCondition = New FileSysSearchCondition
    
    With objFSSearchCondition
    
        .FileInfomationTypesDelimitedByComma = vstrStringOfFileInfomationTypesDelimitedByComma
    
        .RawWildCardsByDelimitedChar = vstrRawWildCardsByDelimitedChar
    
        .IsFileInfoNeeded = True: .IsDirectoryInfoNeeded = False
        
        .SearchedDataTableType = ExistedAsCollectionObjectInMemory
        
        .MinimumHierarchicalOrderToSearch = vintMinimumHierarchicalOrderToSearch
        
        .MaximumHierarchicalOrderToSearch = vintMaximumHierarchicalOrderToSearch
        
        .SearchingExceptionFileOrDirectoryNamesDelimitedByComma = vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma
    End With
    
    Set GetFSSearchConditionAsCollectionForSearchingFilesGenerally = objFSSearchCondition
End Function

'''
''' search files for return Dictionary
'''
''' <Argument>vstrStringOfFileInfomationTypesDelimitedByComma: Input</Argument>
''' <Argument>vstrRawWildCardsByDelimitedChar: Input</Argument>
''' <Argument>vintMinimumHierarchicalOrderToSearch: Input</Argument>
''' <Argument>vintMaximumHierarchicalOrderToSearch: Input</Argument>
''' <Argument>vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma</Argument>
Public Function GetFSSearchConditionAsDictionaryForSearchingFilesGenerally(Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "FileName,FullPath", _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As FileSysSearchCondition


    Dim objFSSearchCondition As FileSysSearchCondition
    
    Set objFSSearchCondition = New FileSysSearchCondition
    
    With objFSSearchCondition
    
        .FileInfomationTypesDelimitedByComma = vstrStringOfFileInfomationTypesDelimitedByComma
    
        .RawWildCardsByDelimitedChar = vstrRawWildCardsByDelimitedChar
    
        .IsFileInfoNeeded = True: .IsDirectoryInfoNeeded = False
        
        .SearchedDataTableType = ExistedAsDictionaryObjectInMemory
        
        .MinimumHierarchicalOrderToSearch = vintMinimumHierarchicalOrderToSearch
        
        .MaximumHierarchicalOrderToSearch = vintMaximumHierarchicalOrderToSearch
        
        .SearchingExceptionFileOrDirectoryNamesDelimitedByComma = vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma
    End With
    
    Set GetFSSearchConditionAsDictionaryForSearchingFilesGenerally = objFSSearchCondition
End Function

'''
''' search directories for return Collection
'''
''' <Argument>vstrStringOfFileInfomationTypesDelimitedByComma: Input</Argument>
''' <Argument>vstrRawWildCardsByDelimitedChar: Input</Argument>
''' <Argument>vintMinimumHierarchicalOrderToSearch: Input</Argument>
''' <Argument>vintMaximumHierarchicalOrderToSearch: Input</Argument>
''' <Argument>vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma</Argument>
Public Function GetFSSearchConditionAsCollectionForSearchingDirectoriesGenerally(Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "RelativeFullPath", _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As FileSysSearchCondition

    Dim objFSSearchCondition As FileSysSearchCondition
    
    Set objFSSearchCondition = New FileSysSearchCondition
    
    With objFSSearchCondition
    
        .FileInfomationTypesDelimitedByComma = vstrStringOfFileInfomationTypesDelimitedByComma
    
        .RawWildCardsByDelimitedChar = vstrRawWildCardsByDelimitedChar
    
        .IsFileInfoNeeded = False: .IsDirectoryInfoNeeded = True
        
        .SearchedDataTableType = ExistedAsCollectionObjectInMemory
        
        .MinimumHierarchicalOrderToSearch = vintMinimumHierarchicalOrderToSearch
        
        .MaximumHierarchicalOrderToSearch = vintMaximumHierarchicalOrderToSearch
        
        .SearchingExceptionFileOrDirectoryNamesDelimitedByComma = vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma
    End With
    
    Set GetFSSearchConditionAsCollectionForSearchingDirectoriesGenerally = objFSSearchCondition
End Function



'**---------------------------------------------
'** log text generation for FileSysSearchCondition
'**---------------------------------------------
'''
'''
'''
Public Function GetLogOfSearchingHierarchicalOrder(ByVal vobjFileSysSearchCondition As FileSysSearchCondition) As String

    Dim strLog As String

    With vobjFileSysSearchCondition
    
        If .AllowToSearchSubDirectories Then
        
            strLog = "Searching sub-directory hierarchical size: "
            
            strLog = strLog & "All sub-directories"
        Else
            If .MinimumHierarchicalOrderToSearch = 0 And .MaximumHierarchicalOrderToSearch = 0 Then
            
                strLog = "Searching sub-directory hierarchical size: "
                
                strLog = strLog & "No, only specified directory"
            Else
                strLog = "Minimum hierarchical order of searching sub-directories: " & CStr(.MinimumHierarchicalOrderToSearch) & vbNewLine
                
                strLog = strLog & "Maximum hierarchical order of searching sub-directories: " & CStr(.MaximumHierarchicalOrderToSearch)
            End If
        End If
    End With

    GetLogOfSearchingHierarchicalOrder = strLog
End Function

