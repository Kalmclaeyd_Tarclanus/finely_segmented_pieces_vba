Attribute VB_Name = "WinExplorer"
'
'   Windows Explorer auto-operations
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 14/Jun/2010    Kaijunko                A part of the idea has been disclosed at http://www.excel.studio-kazu.jp/kw/20100614222952.html
'       Tue,  7/Sep/2010    JK                      A part of the idea has been disclosed at https://dobon.net/vb/bbs/log3-45/27286.html
'       Tue, 31/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = True

'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
    
    Private Declare PtrSafe Function BringWindowToTop Lib "user32" (ByVal hwnd As LongPtr) As Long
    Private Declare PtrSafe Function SetForegroundWindow Lib "user32" (ByVal hwnd As LongPtr) As Long
#Else
    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
    
    Private Declare Function BringWindowToTop Lib "user32" (ByVal hWnd As Long) As Long
    Private Declare Function SetForegroundWindow Lib "user32" (ByVal hWnd As Long) As Long
#End If

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'''
'''
'''
Private Enum OpenedExplorersGetInfoDictionaryItemType

    FoundExplorerInstancesCount
    
    HWndsOfFoundAll
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** open the special path with the explorer
'**---------------------------------------------
'''
''' open the current user Microsoft Office AddIns directory
'''
Public Sub ExploreCurrentUserAddInsLibraryDirectory()

    ' Open <System drive letter>:\Users\<User Name>\AppData\Roaming\Microsoft\AddIns
    
    Select Case Trim(Replace(LCase(Application.Name), "microsoft", ""))
    
        Case "excel"
    
            ExploreParentFolderWhenOpenedThatIsNothing GetCurrentOfficeFileObject().Application.UserLibraryPath
            
        Case Else
    
            ExploreParentFolderWhenOpenedThatIsNothing GetMicrosoftWindowsUserAppDataDirectoryPath() & "\Microsoft\AddIns"
    End Select
End Sub

'''
''' open the current user SendTo directory
'''
Public Sub ExploreCurrentUserSendToDirectory()

    ExploreParentFolderWhenOpenedThatIsNothing GetMicrosoftWindowsUserSendToDirectoryPath()
End Sub



'**---------------------------------------------
'** open explorer with selecting either the file or the directory
'**---------------------------------------------
'''
''' open Explorer with selecting the file by IWshRuntimeLibrary.WshShell
'''
Public Sub ExploreParentFolderWithSelecting(ByVal vstrPath As String)

    With New IWshRuntimeLibrary.WshShell
    
        .Run "explorer.exe /select,""" & vstrPath & """", 1
    End With
End Sub


'''
''' open Explorer by IWshRuntimeLibrary.WshShell
'''
Public Sub ExploreParentFolderWithSelectingWhenOpenedThatIsNothing(ByVal vstrPath As String)

    Dim strDir As String, strTrimmedPath As String
    
    strTrimmedPath = GetDirectoryPathAfterCuttingLastBackSlashFromThePathIfItHas(vstrPath)
    
    strDir = GetParentDirectoryPathByFSO(strTrimmedPath)

    If strDir <> "" Then
    
        With GetOpenedDirectoriesAndCountDic()
            
            If Not .Exists(strDir) Then
            
                With New IWshRuntimeLibrary.WshShell
                
                    .Run "explorer.exe /select,""" & strTrimmedPath & """", 1
                End With
'            Else
'                ExploreFolderWhenOpenedThatIsNothing strDir
            End If
        End With
    End If
End Sub

'''
''' open Explorer by Shell32.Shell
'''
Public Sub ExploreParentFolder(ByVal vstrPath As String)

    ExploreFolderByShell GetParentDirectoryPathByFSO(vstrPath)
End Sub

'''
''' Open the parenet folder by Explorer.exe when there is no opened folders of its path
'''
Public Sub ExploreParentFolderWhenOpenedThatIsNothing(ByVal vstrPath As String)
    
    ExploreFolderWhenOpenedThatIsNothing GetParentDirectoryPathByFSO(vstrPath)
End Sub


'''
''' open Explorer by Shell32.Shell
'''
Public Sub ExploreFolderByShell(ByVal vstrPath As String)

    With New Scripting.FileSystemObject
        
        If .FolderExists(vstrPath) Then
            
            With New Shell32.Shell  ' At VBScript, then With CreateObject("Shell.Application")
                
                .Explore vstrPath
            End With
        End If
    End With
End Sub

'''
'''
'''
Public Sub ExploreFolderWhenOpenedThatIsNothing(ByVal vstrPath As String)
    
    Dim objOpenedDirectoriesAndCountsDic As Scripting.Dictionary, strTrimmedPath As String
    
    Dim objHWnds As Collection, varIE As Variant
    
#If VBA7 Then

    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If
    
    strTrimmedPath = GetDirectoryPathAfterCuttingLastBackSlashFromThePathIfItHas(vstrPath)
    
    With New Scripting.FileSystemObject
    
        If .FolderExists(strTrimmedPath) Then
            
            With GetOpenedDirectoriesAndHWndsDic()
                
                If Not .Exists(strTrimmedPath) Then
                    
                    With New Shell32.Shell  ' At VBScript, then With CreateObject("Shell.Application")
                        
                        .Explore strTrimmedPath
                    End With
                Else
                    Set objHWnds = .Item(strTrimmedPath)
                    
                    For Each varIE In objHWnds
                    
                        intHWnd = varIE
                    
                        BringWindowToTop intHWnd
                        
                        SetForegroundWindow intHWnd
                        
                        Sleep 10
                    Next
                    
                    DoEvents: DoEvents: DoEvents:
                End If
            End With
        End If
    End With
End Sub

'''
'''
'''
Public Sub CloseExploredFolderWhenOneOrMoreThanOpenedItExist(ByVal vstrPath As String)

#If HAS_REF Then

    Dim objShell As Shell32.Shell
    Dim objWindows As SHDocVw.IShellWindows     ' Reference 'Microsoft Internet Controls' C:\Windows\SysWOW6\ieframe.dll
    Dim objIE As SHDocVw.InternetExplorer       ' Reference 'Microsoft Internet Controls' C:\Windows\SysWOW6\ieframe.dll
    
    
    Set objShell = New Shell32.Shell
#Else
    Dim objShell As Object, objWindows As Object, objIE As Object
    
    Set objShell = CreateObject("Shell.Application")
#End If

    Dim strURLTemp As String, intCount As Long, strUNCPath As String
    
    Set objWindows = objShell.Windows
    
    For Each objIE In objWindows
        
        ' Remove the already opened files from the Internt Explorer
        
        If LCase(objIE.FullName) Like "*explorer.exe" Then
        
            If objIE.LocationURL <> "" Then
            
                strUNCPath = GetUNCPathFromLocationURL(objIE.LocationURL)
      
                If strUNCPath <> "" Then
                
                    If StrComp(UCase(vstrPath), UCase(strUNCPath)) = 0 Then
                    
                        objIE.Quit
                    End If
                End If
            End If
        End If
    Next
End Sub


'''
''' Assume that you use IWebBrowser2.LocationURL
'''
Public Function GetUNCPathFromLocationURL(ByVal vstrLocationURL As String) As String
    
    Dim strURLTemp As String, strUNCPath As String, i As Long

    strUNCPath = ""

    If vstrLocationURL <> "" Then
    
        strURLTemp = vstrLocationURL
        
        ' except an object folder
        If Left(strURLTemp, 10) <> "file:///::" Then
        
            ' remove first "file:///"
            
            If strURLTemp Like "file:///?:*" Then
                
                ' for a local directory
                strURLTemp = Right(strURLTemp, Len(strURLTemp) - Len("file:///"))
            Else
                ' for a network sharing directory
                strURLTemp = Right(strURLTemp, Len(strURLTemp) - Len("file:"))
            End If
            
            ' replace each character one by one on forward direction
            
            For i = 1 To Len(strURLTemp)
                
                Select Case Mid(strURLTemp, i, 1)
                
                    Case "/"
                    
                        strUNCPath = strUNCPath & "\"
                        
                    Case "%"
                    
                        strUNCPath = strUNCPath & Chr(CInt("&H" & Mid(strURLTemp, i + 1, 2)))
                        
                        i = i + 2
                    Case Else
                    
                        strUNCPath = strUNCPath & Mid(strURLTemp, i, 1)
                End Select
            Next
        End If
    End If

    GetUNCPathFromLocationURL = strUNCPath
End Function


'///////////////////////////////////////////////
'/// Internal Functions
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetOpenedDirectoriesAndCountDic() As Scripting.Dictionary

    Set GetOpenedDirectoriesAndCountDic = mfobjGetOpenedDirectoriesAndParametersDic(OpenedExplorersGetInfoDictionaryItemType.FoundExplorerInstancesCount)
End Function
Private Function GetOpenedDirectoriesAndHWndsDic() As Scripting.Dictionary

    Set GetOpenedDirectoriesAndHWndsDic = mfobjGetOpenedDirectoriesAndParametersDic(OpenedExplorersGetInfoDictionaryItemType.HWndsOfFoundAll)
End Function


'''
'''
'''
Private Function mfobjGetOpenedDirectoriesAndParametersDic(Optional ByVal venmOpenedExplorersGetInfoDictionaryItemType As OpenedExplorersGetInfoDictionaryItemType = OpenedExplorersGetInfoDictionaryItemType.FoundExplorerInstancesCount) As Scripting.Dictionary

#If HAS_REF Then

    Dim objShell As Shell32.Shell
    Dim objWindows As SHDocVw.IShellWindows     ' Reference 'Microsoft Internet Controls' C:\Windows\SysWOW6\ieframe.dll
    Dim objIE As SHDocVw.InternetExplorer       ' Reference 'Microsoft Internet Controls' C:\Windows\SysWOW6\ieframe.dll
    
    
    Set objShell = New Shell32.Shell
#Else
    Dim objShell As Object, objWindows As Object, objIE As Object
    
    Set objShell = CreateObject("Shell.Application")
#End If

    Dim strURLTemp As String, intCount As Long, strUNCPath As String
    Dim objDirectoryPathAndCountDic As Scripting.Dictionary
    Dim objHWnds As Collection
    
    
    Set objDirectoryPathAndCountDic = New Scripting.Dictionary
    
    Set objWindows = objShell.Windows
    
    For Each objIE In objWindows
        
        ' Remove the already opened files from the Internet Explorer
        
        If LCase(objIE.FullName) Like "*explorer.exe" Then
        
            strURLTemp = objIE.LocationURL
            
            If strURLTemp <> "" Then
            
                strUNCPath = GetUNCPathFromLocationURL(strURLTemp)
      
                If strUNCPath <> "" Then
                
                    With objDirectoryPathAndCountDic
                    
                        Select Case venmOpenedExplorersGetInfoDictionaryItemType
                        
                            Case OpenedExplorersGetInfoDictionaryItemType.FoundExplorerInstancesCount
                            
                                If Not .Exists(strUNCPath) Then
                                
                                    .Add strUNCPath, CLng(1)
                                Else
                                    intCount = .Item(strUNCPath)
                                    
                                    .Item(strUNCPath) = intCount + 1
                                End If
                        
                            Case OpenedExplorersGetInfoDictionaryItemType.HWndsOfFoundAll
                            
                                If Not .Exists(strUNCPath) Then
                                
                                    Set objHWnds = New Collection
                                    
                                    objHWnds.Add "" & objIE.hwnd
                                    
                                    .Add strUNCPath, objHWnds
                                Else
                                    Set objHWnds = .Item(strUNCPath)
                                    
                                    objHWnds.Add "" & objIE.hwnd
                                    
                                    Set .Item(strUNCPath) = objHWnds
                                End If
                        End Select
                    End With
                End If
            End If
        End If
    Next
    
    Set mfobjGetOpenedDirectoriesAndParametersDic = objDirectoryPathAndCountDic
End Function

'''
'''
'''
Public Function GetMicrosoftWindowsCurrentUserRootDirectoryPath() As String

    Dim strMyDocumentsPath As String, strUserRootPath As String
    
    strMyDocumentsPath = GetMicrosoftWindowsSpecialDirectoryPath("MyDocuments")

    GetParentDicrectoryPathToArgFromCurrentExistedDirectoryPathByVbaDir strUserRootPath, strMyDocumentsPath, 2

    GetMicrosoftWindowsCurrentUserRootDirectoryPath = strUserRootPath
End Function


'''
'''
'''
''' <Argument>vvarSpecialDirectoryIndex: Index integer or specified name string</Argument>
Public Function GetMicrosoftWindowsSpecialDirectoryPath(ByVal vvarSpecialDirectoryIndex As Variant) As String

#If HAS_REF Then
    Dim objWshShell As IWshRuntimeLibrary.WshShell
    
    Set objWshShell = New IWshRuntimeLibrary.WshShell
#Else
    Dim objWshShell As Object
    
    Set objWshShell = CreateObject("WScript.Shell")
#End If
    
    GetMicrosoftWindowsSpecialDirectoryPath = objWshShell.SpecialFolders(vvarSpecialDirectoryIndex)
End Function

'''
'''
'''
Private Function GetMicrosoftWindowsUserSendToDirectoryPath() As String

    GetMicrosoftWindowsUserSendToDirectoryPath = GetMicrosoftWindowsSpecialDirectoryPath("SendTo")
End Function


'''
'''
'''
Private Function GetMicrosoftWindowsUserAppDataDirectoryPath() As String

    GetMicrosoftWindowsUserAppDataDirectoryPath = GetMicrosoftWindowsSpecialDirectoryPath("AppData")
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToOpenSomeSpecialFolder()

    Dim strUserInstallPath As String
    
    ' Open <System drive letter>:\Users\<User Name>\AppData\Roaming
    
    strUserInstallPath = GetMicrosoftWindowsUserAppDataDirectoryPath()

    ExploreFolderByShell strUserInstallPath
End Sub


'''
'''
'''
Private Sub msubSanityTestToDumpOpenedDirList()

#If HAS_REF Then

    Dim objShell As Shell32.Shell
    Dim objWindows As SHDocVw.IShellWindows     ' Reference 'Microsoft Internet Controls' C:\Windows\SysWOW6\ieframe.dll
    Dim objIE As SHDocVw.InternetExplorer       ' Reference 'Microsoft Internet Controls' C:\Windows\SysWOW6\ieframe.dll
    
    Set objShell = New Shell32.Shell
#Else
    Dim objShell As Object, objWindows As Object, objIE As Object
    
    Set objShell = CreateObject("Shell.Application")
#End If

    Set objWindows = objShell.Windows
    
    For Each objIE In objWindows
        
        Debug.Print "LocationName[" & objIE.LocationName & "],  FullName[" & objIE.FullName & "]"
    Next
End Sub


'''
'''
'''
Private Sub msubSanityTestOfGetOpenedDirectoriesAndCountsDic()

    DebugDic mfobjGetOpenedDirectoriesAndParametersDic()
End Sub


'''
'''
'''
Private Sub msubSanityTestToCloseExplorerTest()

#If HAS_REF Then
    Dim objShell As Shell32.Shell, objFolder As Shell32.Folder
    Dim objWindows As SHDocVw.IShellWindows     ' Reference 'Microsoft Internet Controls' C:\Windows\SysWOW6\ieframe.dll
    Dim objIE As SHDocVw.InternetExplorer       ' Reference 'Microsoft Internet Controls' C:\Windows\SysWOW6\ieframe.dll
    
    Set objShell = New Shell32.Shell
#Else
    Dim objShell As Object, objWindows As Object, objIE As Object
    
    Dim objFolder As Object
    
    
    Set objShell = CreateObject("Shell.Application")
#End If

    Set objFolder = objShell.Namespace(GetCurrentOfficeFileObject().Path)
    
    For Each objIE In objShell.Windows
    
        If objIE.LocationName = objFolder.Title Then
            
            objIE.Quit
        End If
    Next
End Sub

'''
'''
'''
Private Sub msubSanityTestToCloseExploredFolderWhenOneOrMoreThanOpenedItExist()

    Dim strDir As String
    
    With GetCurrentOfficeFileObject()
    
        strDir = .Path()
    End With

    ExploreParentFolderWhenOpenedThatIsNothing strDir
    
'    Sleep 3000
'
'    CloseExploredFolderWhenOneOrMoreThanOpenedItExist strDir
End Sub

