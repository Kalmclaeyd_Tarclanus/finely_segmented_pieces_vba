Attribute VB_Name = "FileSysFilesInfo"
'
'   List files in the specified directries and get these attribute information
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 12/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'       Fri, 17/Jun/2022    Kalmclaeyd Tarclanus    Redesigned about tools which uses FindFirstFileEx
'

Option Explicit


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Logging options
'**---------------------------------------------
'''
''' general logging Data-Table visualizing type
'''
Public Enum LogDTVisualizationType

    GenerateNewBookWithDeletingOldSheets = 0
    
    GenerateNewBookWithAddingSheet = 1
    
    AddNewSheetToCurrentBook = 2    ' add new Worksheet to ThisWorkbook
    
    GenerateCSVFile = 3
End Enum

'''
'''
'''
Public Enum InMemoryDataTableType

    ExistedAsCollectionObjectInMemory
    
    ExistedAsDictionaryObjectInMemory
End Enum


'**---------------------------------------------
'** Needed file information type
'**---------------------------------------------
'''
''' getting data-table collection flag
'''
Public Enum FileGeneralInfo

    giNormal = VbFileAttribute.vbNormal     ' 0     File Attribute flag
    
    giReadOnly = VbFileAttribute.vbReadOnly ' &H1   File Attribute flag
    
    giHidden = VbFileAttribute.vbHidden     ' &H2   File Attribute flag
    
    giSystem = VbFileAttribute.vbSystem     ' &H4   File Attribute flag
    
    giVolume = VbFileAttribute.vbVolume     ' &H8   File Attribute flag ' Is reffered from VBA.Dir function or FindFirstFile function
    
    giDirectory = VbFileAttribute.vbDirectory ' &H10 (16)       File Attribute flag ' Is reffered from VBA.Dir function or FindFirstFile function
    
    giArchive = VbFileAttribute.vbArchive   ' &H20 (32)         File Attribute flag
    
    giAlias = VbFileAttribute.vbAlias       ' &H40    (64)      File Attribute flag for example, .lnk file
    
    giCompressed = &H80          '  &H800 (128)     File Attribute flag, for example .zip file
    
    
    giFullPath = &H800           ' (2048)
    
    giFileName = &H100          ' (256)
    
    giFileBaseName = &H200      '   (512)
    
    giFileExtension = &H400    '   (1024)
    
    giFileSize = &H20000          ' kB (131072)
    
    giLastModifiedDate = &H1000   ' (4096)
    
    giCreatedDate = &H8000  ' (32768)
    
    giParentDirectoryPath = &H2000  ' (8192)
    
    giRelativeDirectoryPath = &H4000    ' (16384)
    
    giRelativeFullPath = &H10000    ' (65536)
    
    giLinkDestination = &H40000 '   (262144)
    
    giLinkDestFileName = &H80000 '  (524288)
    
    giFilePathAndNameAndSize = giFileName Or giFileSize
End Enum

'''
'''
'''
Public Enum CreateDateDirOption

    SeparateMonthFromDay = 0
    
    MergeMonthAndDay = 1
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** File name management
'**---------------------------------------------
'''
'''
'''
Public Function AddSuffixToFileName(ByVal vstrPath As String, ByVal vstrSuffix As String) As String
    
    Dim strNewPath As String, strDirPath As String, strFileName As String
    
    strNewPath = vstrPath
    
    If vstrSuffix <> "" Then
    
        With New Scripting.FileSystemObject

            strDirPath = .GetParentFolderName(vstrPath)
            
            strFileName = .GetBaseName(vstrPath) & vstrSuffix
            
            strNewPath = strDirPath & "\" & strFileName & "." & .GetExtensionName(vstrPath)
        End With
    End If

    AddSuffixToFileName = strNewPath
End Function


'''
''' Add the suffix '_##' to the file name when the specified path has already existed.
'''
Public Function FindNewNameWhenAlreadyExist(ByVal vstrPath As String, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing) As String
    
    FindNewNameWhenAlreadyExist = vstrPath
    
    Dim intCount As Long, objFS As Scripting.FileSystemObject, strNewPath As String
    
    If Not vobjFS Is Nothing Then
    
        Set objFS = vobjFS
    Else
        Set objFS = New Scripting.FileSystemObject
    End If
    
    
    With objFS

        If .FileExists(vstrPath) Then
        
            intCount = 2
            
            strNewPath = Replace(vstrPath, "." & .GetExtensionName(vstrPath), "") & "_" & PadStringLeftZero2Digits(intCount) & "." & .GetExtensionName(vstrPath)
        
            While .FileExists(strNewPath)
            
                intCount = intCount + 1
                
                strNewPath = Replace(vstrPath, "." & .GetExtensionName(vstrPath), "") & "_" & PadStringLeftZero2Digits(intCount) & "." & .GetExtensionName(vstrPath)
            Wend
            
            FindNewNameWhenAlreadyExist = strNewPath
        End If
    End With
End Function

'''
''' add a prefix or a suffix into file base name
'''
Public Function GetModifiedFullPathWithAddingFileNamePrefixOrSuffix(ByVal vstrFullPath As String, Optional vstrPrefix As String = "", Optional vstrSuffix As String = "", Optional vblnAvoidExistedFileName As Boolean = False) As String
    
    Dim strNewPath As String
    
    strNewPath = vstrFullPath
    
    If vstrPrefix <> "" Or vstrSuffix <> "" Or vblnAvoidExistedFileName Then
        
        With New Scripting.FileSystemObject
            
            strNewPath = .GetParentFolderName(vstrFullPath) & "\" & vstrPrefix & .GetBaseName(vstrFullPath) & vstrSuffix & "." & .GetExtensionName(vstrFullPath)
            
            If vblnAvoidExistedFileName Then
            
                strNewPath = FindNewNameWhenAlreadyExist(strNewPath)
            End If
        End With
    End If

    GetModifiedFullPathWithAddingFileNamePrefixOrSuffix = strNewPath
End Function

'''
''' get control parameters for SearchingExceptionFileOrDirectoryNamesDelimitedByComma
'''
''' <Argument>rblnIsFileOrDirectoryExceptionListEnabled: Output</Argument>
''' <Argument>robjFileOrDirectoryExceptionListKeys: Output</Argument>
''' <Argument>vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma: Input</Argument>
Public Sub GetFileOrDirectoryNameExceptionListCondition(ByRef rblnIsFileOrDirectoryExceptionListEnabled As Boolean, _
        ByRef robjFileOrDirectoryExceptionListKeys As Scripting.Dictionary, _
        ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String)

    rblnIsFileOrDirectoryExceptionListEnabled = False

    If vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma <> "" Then
    
        rblnIsFileOrDirectoryExceptionListEnabled = True
    
        Set robjFileOrDirectoryExceptionListKeys = GetDicOnlyKeysFromLineDelimitedChar(vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma)
    End If
End Sub

'**---------------------------------------------
'** File path management
'**---------------------------------------------
'''
'''
'''
Public Function GetDifferentPathPieceNamesInfoBetweenCurrentPathAndIdealPath(ByVal vstrCurrentPath As String, ByVal vstrIdealPath As String) As String

    Dim varIndex As Variant, strPieces() As String, strCurrentPathPiece As String, strIdealPathPiece As String
    
    Dim strInfo As String
    
    strInfo = ""
    
    With GetIndexToDifferentPathPieceNameDicFromSimilarTwoFileSystemPaths(vstrCurrentPath, vstrIdealPath)
    
        For Each varIndex In .Keys
        
            strPieces = Split(.Item(varIndex), ",")
            
            strCurrentPathPiece = Trim(strPieces(0))
            
            strIdealPathPiece = Trim(strPieces(1))
        
            If strInfo <> "" Then
            
                strInfo = strInfo & "; "
            End If
            
            Select Case True
            
                Case "" = strCurrentPathPiece
                
                    strInfo = strInfo & "(" & CStr(varIndex) & ", " & "[Ideal path only]  >-> " & strIdealPathPiece & ")"
                    
                Case "" = strIdealPathPiece
            
                    strInfo = strInfo & "(" & CStr(varIndex) & ", " & strCurrentPathPiece & " <-< [Current path only])"
                    
                Case Else
                
                    strInfo = strInfo & "(" & CStr(varIndex) & ", " & strCurrentPathPiece & " <<->> " & strIdealPathPiece & ")"
            End Select
        Next
    End With

    GetDifferentPathPieceNamesInfoBetweenCurrentPathAndIdealPath = strInfo
End Function


'''
'''
'''
Public Function GetIndexToDifferentPathPieceNameDicFromSimilarTwoFileSystemPaths(ByVal vstrPath01 As String, ByVal vstrPath02 As String) As Scripting.Dictionary

    Dim strPieces01() As String, strPieces02() As String, intLengthOfPieces01 As Long, intLengthOfPieces02 As Long
    Dim i As Long, objDic As Scripting.Dictionary
    
    strPieces01 = Split(vstrPath01, "\")
    
    strPieces02 = Split(vstrPath02, "\")
    
    intLengthOfPieces01 = UBound(strPieces01)
    
    intLengthOfPieces02 = UBound(strPieces02)
    
    Set objDic = New Scripting.Dictionary
    
    For i = LBound(strPieces01) To intLengthOfPieces01
    
        If i <= intLengthOfPieces02 Then
        
            If strPieces01(i) <> strPieces02(i) Then
            
                objDic.Add i, strPieces01(i) & "," & strPieces02(i)
            End If
        Else
            Exit For
        End If
    Next
    
    If intLengthOfPieces01 < intLengthOfPieces02 Then
    
        For i = intLengthOfPieces01 + 1 To intLengthOfPieces02
        
            objDic.Add i, "," & strPieces02(i)
        Next
    ElseIf intLengthOfPieces02 < intLengthOfPieces01 Then
    
        For i = intLengthOfPieces02 + 1 To intLengthOfPieces01
        
            objDic.Add i, strPieces01(i) & ","
        Next
    End If

    Set GetIndexToDifferentPathPieceNameDicFromSimilarTwoFileSystemPaths = objDic
End Function


'**---------------------------------------------
'** Creating directory in this computer and network drive
'**---------------------------------------------
'''
''' vstrDirPath has to be a directory path, This uses FSO
'''
Public Function ForceToCreateDirectory(ByVal vstrDirPath As String, _
        Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing) As Long
    
    
    Dim intRet As Long, intP1 As Long, strDrive As String, strTargetDirPath As String
    
    Dim objFS As Scripting.FileSystemObject
    
    
    If Not vobjFS Is Nothing Then
        
        Set objFS = vobjFS
    Else
        Set objFS = New Scripting.FileSystemObject
    End If
    
    intRet = 0
    
    On Error GoTo ErrHandler
    
    With objFS
    
        If .FolderExists(vstrDirPath) Then
        
            intRet = 0
            
            ForceToCreateDirectory = intRet
            
            Exit Function
        End If

        strDrive = .GetDriveName(vstrDirPath)
        
        If Not .DriveExists(strDrive) Then
        
            intRet = -1
            
            ForceToCreateDirectory = intRet
            
            Exit Function
        End If
        
        intP1 = InStr(Len(strDrive) + 2, vstrDirPath, "\")
        
        If intP1 > 0 Then
        
            Do
                strTargetDirPath = Left(vstrDirPath, intP1 - 1)
                
                If Not .FolderExists(strTargetDirPath) Then
                
                    .CreateFolder strTargetDirPath
                End If
                
                intP1 = InStr(intP1 + 1, vstrDirPath, "\")
                            
            Loop While intP1 > 0
        
            If Not .FolderExists(vstrDirPath) Then
            
                .CreateFolder vstrDirPath
            End If
        Else
            .CreateFolder vstrDirPath
        End If
    End With

    ForceToCreateDirectory = intRet
    
    Exit Function
    
ErrHandler:

    Debug.Print Err.Description
    
    ForceToCreateDirectory = 2
End Function

'''
''' Create new directory with the IP-address and current user name of this computer
'''
Public Function GetDirectoryNameAsPathLeteralFromUserNameAndIPv4Address() As String


    Dim strUserName As String, strIPv4Address As String, strIPv4AddressConverted As String
    
    GetCurrentLoginUserNameAndIPvAddress strUserName, strIPv4Address
    
    strIPv4AddressConverted = Replace(strIPv4Address, ".", "_")
    
    If InStr(1, strIPv4AddressConverted, ";") > 0 Then
    
        strIPv4AddressConverted = Replace(Replace(strIPv4AddressConverted, ";", "_"), ":", "")
    End If

    GetDirectoryNameAsPathLeteralFromUserNameAndIPv4Address = strIPv4AddressConverted & "_" & Replace(strUserName, ".", "_")
End Function


'''
''' create directory using date string to get working directory
'''
Public Function CreateDirectoryUsingDate(ByVal vstrParentDir As String, _
        ByVal vobjDateForLogFilePath As Date, _
        Optional vintDirStructure As Integer = CreateDateDirOption.SeparateMonthFromDay) As String
    
    Dim strPath As String
    
    With New Scripting.FileSystemObject
        
        If .FolderExists(vstrParentDir) Then
        
            strPath = vstrParentDir & "\" & Format(vobjDateForLogFilePath, "yyyy")
            
            If Not .FolderExists(strPath) Then
                
                .CreateFolder (strPath)
            End If
            
            Select Case vintDirStructure
                
                Case CreateDateDirOption.SeparateMonthFromDay
                
                    strPath = strPath & "\" & Format(vobjDateForLogFilePath, "mm")
                    
                    If Not .FolderExists(strPath) Then
                        
                        .CreateFolder (strPath)
                    End If
                
                    strPath = strPath & "\" & Format(vobjDateForLogFilePath, "dd")
                    
                    If Not .FolderExists(strPath) Then
                        
                        .CreateFolder (strPath)
                    End If
                
                Case CreateDateDirOption.MergeMonthAndDay
                
                    strPath = strPath & "\" & Format(vobjDateForLogFilePath, "mmdd")
                    
                    If Not .FolderExists(strPath) Then
                        
                        .CreateFolder (strPath)
                    End If
            End Select
        End If
    End With
    
    CreateDirectoryUsingDate = strPath
End Function


'**---------------------------------------------
'** Searching either files or directories
'**---------------------------------------------
'''
'''
'''
Public Function GetDescriptionAboutCombinationOfFileInfoAndDirectoryInfo(ByVal vblnIsFileInfoNeeded As Boolean, _
        ByVal vblnIsDirectoryInfoNeeded As Boolean, _
        Optional ByVal vblnIncludesSearchedCountDescription As Boolean = False) As String

    Dim strDescription As String
    
    Select Case True

        Case vblnIsFileInfoNeeded And Not vblnIsDirectoryInfoNeeded
                                
            strDescription = "Only file info"
        
            If vblnIncludesSearchedCountDescription Then
            
                strDescription = strDescription & ", count of files"
            End If
        
        Case Not vblnIsFileInfoNeeded And vblnIsDirectoryInfoNeeded
                            
            strDescription = "Only directory info"
        
            If vblnIncludesSearchedCountDescription Then
            
                strDescription = strDescription & ", count of directories"
            End If
            
        Case vblnIsFileInfoNeeded And vblnIsDirectoryInfoNeeded
                                
            strDescription = "File-directory info"
    
            If vblnIncludesSearchedCountDescription Then
            
                strDescription = strDescription & ", count of files and directories"
            End If
        Case Not vblnIsFileInfoNeeded And Not vblnIsDirectoryInfoNeeded
    
            strDescription = "No search neither file or directory"
    End Select

    GetDescriptionAboutCombinationOfFileInfoAndDirectoryInfo = strDescription
End Function

'''
'''
'''
Public Function GetLogAboutSearchingInfoTypeAndWildCard(ByVal vstrFileInfomationTypesDelimitedByComma As String, _
        ByVal vstrExtensionTextByDelimitedChar As String, _
        ByVal vstrRawWildCardsByDelimitedChar As String, _
        Optional ByVal vstrDelimitedChar As String = ",") As String

    Dim strLog As String
    
    Dim strInfoTypes() As String, strWildCards() As String
    
    
    strInfoTypes() = Split(vstrFileInfomationTypesDelimitedByComma, vstrDelimitedChar)
    
    If UBound(strInfoTypes) = 0 Then
    
        strLog = "File information data type: " & vstrFileInfomationTypesDelimitedByComma
    Else
        strLog = "File information data types: " & vstrFileInfomationTypesDelimitedByComma
    End If
    
    'strLog = strLog & vbNewLine

    If vstrExtensionTextByDelimitedChar = "*" And vstrRawWildCardsByDelimitedChar = "*" Then
    
        strLog = strLog & vbNewLine
        
        strLog = strLog & "No wild-cards"
    Else
        If vstrExtensionTextByDelimitedChar <> "*" Then
        
            strLog = strLog & vbNewLine
        
            strLog = strLog & "Searching extensions: " & vstrExtensionTextByDelimitedChar
        End If
    
        If vstrRawWildCardsByDelimitedChar <> "*" Then
        
            strLog = strLog & vbNewLine
        
            strWildCards = Split(vstrRawWildCardsByDelimitedChar, vstrDelimitedChar)
        
            If UBound(strWildCards) = 0 Then
            
                strLog = strLog & "Wild card: " & vstrRawWildCardsByDelimitedChar
            Else
                strLog = strLog & "Wild cards: " & vstrRawWildCardsByDelimitedChar
            End If
        End If
    End If

    GetLogAboutSearchingInfoTypeAndWildCard = strLog
End Function


'''
'''
'''
''' <Argument>rblnPutInCollection: Output</Argument>
''' <Argument>rblnPutInDictionary: Output</Argument>
''' <Argument>venmInMemoryDataTableType: Input</Argument>
Public Sub GetPutInDataTypeFromInMemoryDataTableType(ByRef rblnPutInCollection As Boolean, _
        ByRef rblnPutInDictionary As Boolean, _
        ByVal venmInMemoryDataTableType As InMemoryDataTableType)


    Select Case venmInMemoryDataTableType
    
        Case ExistedAsCollectionObjectInMemory
    
            rblnPutInCollection = True
            
            rblnPutInDictionary = False
        
        Case ExistedAsDictionaryObjectInMemory
    
            rblnPutInCollection = False
            
            rblnPutInDictionary = True
    End Select
End Sub


'**---------------------------------------------
'** FileGeneralInfo type management
'**---------------------------------------------
'''
''' convert a String data delimited some character into Collection object
'''
Public Function GetFileGeneralInfoColFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Collection
    
    Dim objCol As Collection, i As Long, strArray() As String

    Set objCol = New Collection
    
    If vstrLineDelimited <> "" Then
    
        strArray = Split(vstrLineDelimited, vstrDelimiter)
        
        For i = LBound(strArray) To UBound(strArray)
        
            objCol.Add mfenmGetFileGeneralInfoTypeFromText(Trim(strArray(i)))
        Next
    End If
    
    Set GetFileGeneralInfoColFromLineDelimitedChar = objCol
End Function

'''
'''
'''
Public Function GetFileGeneralInfoFlagValueFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As FileGeneralInfo
    
    Dim enmFlag As FileGeneralInfo, i As Long, strArray() As String

    enmFlag = 0
    
    If vstrLineDelimited <> "" Then
    
        strArray = Split(vstrLineDelimited, vstrDelimiter)
        
        For i = LBound(strArray) To UBound(strArray)
        
            enmFlag = enmFlag Or mfenmGetFileGeneralInfoTypeFromText(Trim(strArray(i)))
        Next
    End If
    
    GetFileGeneralInfoFlagValueFromLineDelimitedChar = enmFlag
End Function


'''
'''
'''
Private Function mfenmGetFileGeneralInfoTypeFromText(ByVal vstrText As String) As FileGeneralInfo
    
    Dim enmFileGeneralInfo As FileGeneralInfo

    enmFileGeneralInfo = 0 ' Normal

    Select Case vstrText
    
        Case "RelativeFullPath"
        
            enmFileGeneralInfo = giRelativeFullPath
            
        Case "Normal"   ' This stands for 'Normal' attribute file in Windows file system
        
            enmFileGeneralInfo = giNormal   ' 0
        
        Case "ReadOnly"
        
            enmFileGeneralInfo = giReadOnly
    
        Case "Hidden"
        
            enmFileGeneralInfo = giHidden
    
        Case "System"
        
            enmFileGeneralInfo = giSystem
        
        Case "Volume"
        
            enmFileGeneralInfo = giVolume
        
        Case "Directory"
        
            enmFileGeneralInfo = giDirectory    ' it is whether directory or file
        
        Case "Archive"
        
            enmFileGeneralInfo = giArchive
            
        Case "Alias"
        
            enmFileGeneralInfo = giAlias
        
        Case "Compressed"
        
            enmFileGeneralInfo = giCompressed
        
        Case "FullPath"
        
            enmFileGeneralInfo = giFullPath
            
        Case "FileName"
        
            enmFileGeneralInfo = giFileName
    
        Case "FileBaseName"
        
            enmFileGeneralInfo = giFileBaseName
        
        Case "FileExtension"
        
            enmFileGeneralInfo = giFileExtension
        
        Case "FileSize"
        
            enmFileGeneralInfo = giFileSize
            
        Case "LastModifiedDate"
        
            enmFileGeneralInfo = giLastModifiedDate
        
        Case "CreatedDate"
        
            enmFileGeneralInfo = giCreatedDate
        
        Case "ParentDirectoryPath"
        
            enmFileGeneralInfo = giParentDirectoryPath  ' Full path of parent directory of either file or directory
        
        Case "RelativeDirectoryPath"
        
            enmFileGeneralInfo = giRelativeDirectoryPath
        
        Case "LinkDestination"
        
            enmFileGeneralInfo = giLinkDestination
        
        Case "LinkDestFileName"
        
            enmFileGeneralInfo = giLinkDestFileName
    End Select

    mfenmGetFileGeneralInfoTypeFromText = enmFileGeneralInfo
End Function


'''
''' get Field title from FileGeneralInfo type
'''
Public Function GetFileGeneralInfoFieldTitleFromValue(ByVal venmFileGeneralInfo As FileGeneralInfo) As String
    
    Dim strFieldTitle As String

    strFieldTitle = ""
    
    Select Case venmFileGeneralInfo
    
        Case FileGeneralInfo.giRelativeFullPath
        
            strFieldTitle = "RelativeFullPath"
            
        Case FileGeneralInfo.giFileName
        
            strFieldTitle = "FileName"
        
        Case FileGeneralInfo.giFileBaseName
        
            strFieldTitle = "FileBaseName"
        
        Case FileGeneralInfo.giFileExtension
        
            strFieldTitle = "FileExtension"
        
        Case FileGeneralInfo.giFileSize
        
            strFieldTitle = "FileSize"
    
        Case FileGeneralInfo.giFullPath
        
            strFieldTitle = "FullPath"
            
        Case FileGeneralInfo.giParentDirectoryPath
        
            strFieldTitle = "ParentDirectoryPath"
    
        Case FileGeneralInfo.giRelativeDirectoryPath
        
            strFieldTitle = "RelativeDirectoryPath"
    
        Case FileGeneralInfo.giLastModifiedDate
        
            strFieldTitle = "LastModifiedDate"
    
        Case FileGeneralInfo.giCreatedDate
        
            strFieldTitle = "CreatedDate"

        Case FileGeneralInfo.giLinkDestination  ' such as Shortcut file link target-full path
        
            strFieldTitle = "LinkDestination"

        Case FileGeneralInfo.giLinkDestFileName  ' such as Shortcut file link target file name
        
            strFieldTitle = "LinkDestFileName"
        
        ' About file attributes
        Case FileGeneralInfo.giReadOnly
        
            strFieldTitle = "ReadOnly"

        Case FileGeneralInfo.giHidden
        
            strFieldTitle = "Hidden"
            
        Case FileGeneralInfo.giSystem
        
            strFieldTitle = "System"
            
        Case FileGeneralInfo.giArchive
        
            strFieldTitle = "Archive"
        
        Case FileGeneralInfo.giCompressed
        
            strFieldTitle = "Compressed"
    End Select

    GetFileGeneralInfoFieldTitleFromValue = strFieldTitle
End Function


'''
'''
'''
Public Function GetFileSystemObjectIfItNeededBy(ByVal vobjNecessaryFileInfoTypeCol As Collection) As Scripting.FileSystemObject

    Dim enmFileGeneralInfo As FileGeneralInfo, varFileGeneralInfo As Variant
    Dim blnIsNeeded As Boolean
    
    blnIsNeeded = False

    For Each varFileGeneralInfo In vobjNecessaryFileInfoTypeCol
        
        enmFileGeneralInfo = varFileGeneralInfo
        
        Select Case enmFileGeneralInfo
        
            Case FileGeneralInfo.giCreatedDate, FileGeneralInfo.giLinkDestFileName
            
                blnIsNeeded = True
            
                Exit For
        End Select
    Next

    If blnIsNeeded Then
    
        Set GetFileSystemObjectIfItNeededBy = New Scripting.FileSystemObject
    End If
End Function


'''
'''
'''
Public Function GetWshShellObjectIfItNeededBy(ByVal vobjNecessaryFileInfoTypeCol As Collection) As IWshRuntimeLibrary.WshShell

    Dim enmFileGeneralInfo As FileGeneralInfo, varFileGeneralInfo As Variant
    Dim blnIsNeeded As Boolean
    
    blnIsNeeded = False

    For Each varFileGeneralInfo In vobjNecessaryFileInfoTypeCol
        
        enmFileGeneralInfo = varFileGeneralInfo
        
        Select Case enmFileGeneralInfo
        
            Case FileGeneralInfo.giLinkDestination, giLinkDestFileName
            
                blnIsNeeded = True
            
                Exit For
        End Select
    Next

    If blnIsNeeded Then
    
        Set GetWshShellObjectIfItNeededBy = New IWshRuntimeLibrary.WshShell
    End If
End Function

'**---------------------------------------------
'** path format about directory
'**---------------------------------------------
'''
'''
'''
Public Function GetDirectoryPathWithAddingLastBackSlashIfThePathHasNoIt(ByVal vstrDirectoryPath As String) As String

    Dim strDirectoryPathWithSuffixBackSlash As String

    If InStrRev(vstrDirectoryPath, "\") = Len(vstrDirectoryPath) Then
    
        strDirectoryPathWithSuffixBackSlash = vstrDirectoryPath
    Else
        strDirectoryPathWithSuffixBackSlash = vstrDirectoryPath & "\"
    End If

    GetDirectoryPathWithAddingLastBackSlashIfThePathHasNoIt = strDirectoryPathWithSuffixBackSlash
End Function

'''
'''
'''
Public Function GetDirectoryPathAfterCuttingLastBackSlashFromThePathIfItHas(ByRef rstrPath As String) As String

    If InStrRev(rstrPath, "\") = Len(rstrPath) Then
    
        GetDirectoryPathAfterCuttingLastBackSlashFromThePathIfItHas = Left(rstrPath, Len(rstrPath) - 1)
    Else
        GetDirectoryPathAfterCuttingLastBackSlashFromThePathIfItHas = rstrPath
    End If
End Function

'''
'''
'''
Public Function IsFileSysDriveRootPath(ByRef rstrPath As String) As Boolean

    If InStrRev(GetDirectoryPathAfterCuttingLastBackSlashFromThePathIfItHas(rstrPath), "\") = 0 Then
        
        IsFileSysDriveRootPath = True
    Else
        IsFileSysDriveRootPath = False
    End If
End Function


'**---------------------------------------------
'** List up files path from specified directory path
'**---------------------------------------------
'''
'''
'''
''' <Argument>rblnIsWildCardsRestricted: Output</Argument>
''' <Argument>rstrMergedWildCardArray: Output</Argument>
''' <Argument>vstrExtensionTextByDelimitedChar: Input</Argument>
''' <Argument>vstrRawWildCardsByDelimitedChar: Input</Argument>
''' <Argument>vstrDelimitedChar: Input</Argument>
Public Sub GetWildCardsFromExtensionsInfoAndRawWildCards(ByRef rblnIsWildCardsRestricted As Boolean, _
        ByRef rstrMergedWildCardArray() As String, _
        Optional ByVal vstrExtensionTextByDelimitedChar As String = "*", _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vstrDelimitedChar As String = ",")


    Dim i As Long, strExtensions() As String, strRawWildCards() As String
    
    Dim objWildCardCol As Collection
    
    
    If vstrExtensionTextByDelimitedChar <> "" Then
        
        strExtensions = Split(vstrExtensionTextByDelimitedChar, vstrDelimitedChar)
        
        For i = LBound(strExtensions) To UBound(strExtensions)
        
            If objWildCardCol Is Nothing Then Set objWildCardCol = New Collection
        
            If strExtensions(i) <> "*" Then
            
                objWildCardCol.Add "*." & strExtensions(i)
            End If
        Next
    End If
    
    If vstrRawWildCardsByDelimitedChar <> "" Then
        
        strRawWildCards = Split(vstrRawWildCardsByDelimitedChar, vstrDelimitedChar)
        
        For i = LBound(strRawWildCards) To UBound(strRawWildCards)
        
            If objWildCardCol Is Nothing Then Set objWildCardCol = New Collection
            
            If strRawWildCards(i) <> "*" Then
            
                objWildCardCol.Add strRawWildCards(i)
            End If
        Next
    End If
    
    rblnIsWildCardsRestricted = False
    
    If Not objWildCardCol Is Nothing Then
    
        If objWildCardCol.Count > 0 Then
    
            rblnIsWildCardsRestricted = True
            
            ReDim rstrMergedWildCardArray(0 To objWildCardCol.Count - 1)
        
            For i = 0 To UBound(rstrMergedWildCardArray)
            
                rstrMergedWildCardArray(i) = objWildCardCol(i + 1)
            Next
        End If
    End If

    If Not rblnIsWildCardsRestricted Then
    
        ReDim rstrMergedWildCardArray(0)
        
        rstrMergedWildCardArray(0) = "*"
    End If
End Sub


'''
'''
'''
''' <Argument>rblnAllowToFilterByExtensions: Output</Argument>
''' <Argument>rstrSearchExtensions: Output</Argument>
''' <Argument>vstrExtensions: Input</Argument>
''' <Argument>vstrDelimitedChar: Input</Argument>
Public Sub GetFilteringFileExtensionsInfo(ByRef rblnAllowToFilterByExtensions As Boolean, _
        ByRef rstrSearchExtensions() As String, _
        ByVal vstrExtensions As String, _
        ByVal vstrDelimitedChar As String)
    
    Dim i As Long
    
    Select Case vstrExtensions
    
        Case "", "*", "*.*"
        
            rblnAllowToFilterByExtensions = False
        Case Else
        
            rblnAllowToFilterByExtensions = True
        
            rstrSearchExtensions = Split(vstrExtensions, vstrDelimitedChar)
            
            For i = LBound(rstrSearchExtensions) To UBound(rstrSearchExtensions)
            
                rstrSearchExtensions(i) = LCase(Trim(rstrSearchExtensions(i)))
            Next
    End Select
End Sub


'**---------------------------------------------
'** Want to check that that is opened or not from someone
'**---------------------------------------------
'''
''' In network drive, need to detail tests...
''' If the file is locked, it should has been opened.
'''
'''
Public Function IsFileOpened(ByVal vstrPath As String) As Boolean

    Dim blnIsOpened As Boolean, intFileNumber As Integer
    
    blnIsOpened = False
    
    If vstrPath <> "" Then
    
        If "" <> VBA.Dir(vstrPath) Then
        
            On Error GoTo ErrHandler
            
            Open vstrPath For Binary Lock Read Write As #intFileNumber
            
            'Open vstrPath For Binary Access Read Shared As #intFileNumber
            
            Close #intFileNumber
            
            IsFileOpened = False
            
            Exit Function
            
ErrHandler:
            IsFileOpened = True
            
            On Error GoTo 0
            
            Exit Function
        End If
    End If

    IsFileOpened = blnIsOpened
End Function


'**---------------------------------------------
'** Get general file information
'**---------------------------------------------
'''
'''
'''
Public Function GetFileGeneralInfoFieldTitles(ByVal vobjNecessaryFileInfoTypeCol As Collection) As Collection
    
    Dim varFileGeneralInfo As Variant, enmFileGeneralInfo As FileGeneralInfo, objFieldTitles As Collection
    
    Set objFieldTitles = New Collection

    For Each varFileGeneralInfo In vobjNecessaryFileInfoTypeCol
        
        enmFileGeneralInfo = varFileGeneralInfo
        
        objFieldTitles.Add GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo)
    Next

    Set GetFileGeneralInfoFieldTitles = objFieldTitles
End Function

'''
'''
'''
Public Function GetFileGeneralInfoDataTypeFromValue(ByVal venmFileGeneralInfo As FileGeneralInfo) As String
    
    Dim strDataType As String

    strDataType = ""
    
    Select Case venmFileGeneralInfo
        
        Case FileGeneralInfo.giFileName, FileGeneralInfo.giFileBaseName, FileGeneralInfo.giFileExtension, FileGeneralInfo.giFullPath, FileGeneralInfo.giParentDirectoryPath, FileGeneralInfo.giRelativeDirectoryPath, FileGeneralInfo.giRelativeFullPath, FileGeneralInfo.giLinkDestination, giLinkDestFileName
            
            strDataType = "String"
        
        Case FileGeneralInfo.giFileSize
            
            strDataType = "Double"
    
        Case FileGeneralInfo.giLastModifiedDate, FileGeneralInfo.giCreatedDate
            
            strDataType = "Date"
        
        Case giReadOnly, giHidden, giSystem, giVolume, giDirectory, giArchive, giAlias, giCompressed
            
            strDataType = "Boolean"
    End Select

    GetFileGeneralInfoDataTypeFromValue = strDataType
End Function

'''
'''
'''
Public Function GetTitlesTextDelimitedByCommaFromFileGeneralInfoGettings(ByVal vobjNecessaryFileInfoTypeCol As Collection, ByVal vstrDataTypeName As String) As String

    Dim varFileGeneralInfo As Variant, enmFileGeneralInfo As FileGeneralInfo
    Dim strTitlesTextDelimitedByComma As String, i As Long
    

    strTitlesTextDelimitedByComma = ""
    i = 1

    For Each varFileGeneralInfo In vobjNecessaryFileInfoTypeCol
    
        enmFileGeneralInfo = varFileGeneralInfo
        
        If StrComp(GetFileGeneralInfoDataTypeFromValue(enmFileGeneralInfo), vstrDataTypeName) = 0 Then
        
            If i > 1 Then
            
               strTitlesTextDelimitedByComma = strTitlesTextDelimitedByComma & ","
            End If
            
            strTitlesTextDelimitedByComma = strTitlesTextDelimitedByComma & GetFileGeneralInfoFieldTitleFromValue(enmFileGeneralInfo)
            
            i = i + 1
        End If
    Next

    GetTitlesTextDelimitedByCommaFromFileGeneralInfoGettings = strTitlesTextDelimitedByComma
End Function


'**---------------------------------------------
'** Output files info data table to Excel book or sheet
'**---------------------------------------------
'''
''' Improvement point: Delaying point is that this expand the sigle directory-path list into data-table after the file-path list is gotten
'''
Public Function WriteFilesGeneralInformationByExtensionsDelimited(ByVal vstrExecutingApplicationFilePath As String, _
        ByVal vstrDirectoryPath As String, _
        ByVal vblnAllowToSearchSubDirectories As Boolean, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection, _
        ByVal venmLogDTVisualizationType As LogDTVisualizationType, _
        ByVal vstrOutputFilePath As String, _
        Optional ByVal vstrSheetName As String = "FilesInformation", _
        Optional ByVal vstrExtensions As String = "*", _
        Optional ByVal vstrDelimitedChar As String = ",") As Variant
    
    
    Dim objFilePaths As Collection

    ' using FileSystemObject
    Set objFilePaths = GetSubFilePathsByExtensionsDelimited(vstrDirectoryPath, vstrExtensions, vstrDelimitedChar, vblnAllowToSearchSubDirectories)

    Select Case venmLogDTVisualizationType

        Case LogDTVisualizationType.GenerateNewBookWithDeletingOldSheets, LogDTVisualizationType.GenerateNewBookWithAddingSheet, LogDTVisualizationType.AddNewSheetToCurrentBook
    
            Set WriteFilesGeneralInformationByExtensionsDelimited = WriteFilesGeneralInformation(vstrExecutingApplicationFilePath, objFilePaths, vstrDirectoryPath, vobjNecessaryFileInfoTypeCol, venmLogDTVisualizationType, vstrOutputFilePath, vstrSheetName)
    
        Case LogDTVisualizationType.GenerateCSVFile
        
            Let WriteFilesGeneralInformationByExtensionsDelimited = WriteFilesGeneralInformation(vstrExecutingApplicationFilePath, objFilePaths, vstrDirectoryPath, vobjNecessaryFileInfoTypeCol, venmLogDTVisualizationType, vstrOutputFilePath, vstrSheetName)
    End Select
End Function


'''
''' Improvement point: Delaying point is that this expand the sigle directory-path list into data-table after the file-path list is gotten
'''
Public Function WriteFilesGeneralInformation(ByVal vstrExecutingApplicationFilePath As String, _
        ByVal vobjFilePaths As Collection, _
        ByVal vstrSearchingRootDirectoryPath As String, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection, _
        ByVal venmLogDTVisualizationType As LogDTVisualizationType, _
        ByVal vstrOutputFilePath As String, _
        ByVal vstrSheetName As String) As Variant
    
    Dim objDTCol As Collection, objFieldTitlesCol As Collection
    
    Set objDTCol = GetFilesInformationDataTable(vobjFilePaths, vobjNecessaryFileInfoTypeCol, vstrSearchingRootDirectoryPath)
    
    Set objFieldTitlesCol = GetFileGeneralInfoFieldTitles(vobjNecessaryFileInfoTypeCol)

    Select Case venmLogDTVisualizationType

        Case LogDTVisualizationType.GenerateNewBookWithDeletingOldSheets, LogDTVisualizationType.GenerateNewBookWithAddingSheet, LogDTVisualizationType.AddNewSheetToCurrentBook
        
            ' Excel.Worksheet
            Set WriteFilesGeneralInformation = WriteFilesGeneralInformationByGeneralParams(vstrExecutingApplicationFilePath, objDTCol, objFieldTitlesCol, venmLogDTVisualizationType, vstrOutputFilePath, vstrSheetName, vobjNecessaryFileInfoTypeCol)
    
        Case LogDTVisualizationType.GenerateCSVFile
            
            ' String: CSV file path
            Let WriteFilesGeneralInformation = WriteFilesGeneralInformationByGeneralParams(vstrExecutingApplicationFilePath, objDTCol, objFieldTitlesCol, venmLogDTVisualizationType, vstrOutputFilePath, vstrSheetName, vobjNecessaryFileInfoTypeCol)
    End Select
End Function


'''
''' Expand the input data-table into something type, for example, Excel.Sheet, CSV
'''
Public Function WriteFilesGeneralInformationByGeneralParams(ByVal vstrExecutingApplicationFilePath As String, _
        ByVal vobjDTCol As Collection, _
        ByVal vobjFieldTitlesCol As Collection, _
        ByVal venmLogDTVisualizationType As LogDTVisualizationType, _
        ByVal vstrOutputBookPath As String, _
        ByVal vstrSheetName As String, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection) As Variant
    
    
    Dim varObject As Variant, objSheet As Object, strCSVFilePath As String

    Select Case venmLogDTVisualizationType
        
        Case LogDTVisualizationType.GenerateNewBookWithDeletingOldSheets, LogDTVisualizationType.GenerateNewBookWithAddingSheet, LogDTVisualizationType.AddNewSheetToCurrentBook
        
            ' Prevent occuring a compile error when the current VB project doesn't include some Excel book output codes
            
            Set objSheet = GetObjectFromCallBackSixArgumentsInterface("OutputFilesGeneralInformationDTColIntoExcelSheetByOutputBookPath", vobjDTCol, vobjFieldTitlesCol, venmLogDTVisualizationType, vstrOutputBookPath, vstrSheetName, vobjNecessaryFileInfoTypeCol, vstrExecutingApplicationFilePath)
            
            'OutputFilesGeneralInformationDTColIntoExcelSheetByOutputBookPath vobjDTCol, vobjFieldTitlesCol, venmLogDTVisualizationType, vstrOutputBookPath, vstrSheetName, vobjNecessaryFileInfoTypeCol
            
            Set WriteFilesGeneralInformationByGeneralParams = objSheet
            
        Case LogDTVisualizationType.GenerateCSVFile
            
            With New Scripting.FileSystemObject
            
                strCSVFilePath = .GetParentFolderName(vstrOutputBookPath) & "\" & .GetBaseName(vstrOutputBookPath) & ".csv"
            End With
    
            OutputColToCSVFile vobjDTCol, vobjFieldTitlesCol, strCSVFilePath
            
            OpenTextBySomeSelectedTextEditorFromWshShell strCSVFilePath
            
            Let WriteFilesGeneralInformationByGeneralParams = strCSVFilePath
    End Select
End Function








