VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "FileSysSearchCondition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   file system searching condition class
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 22/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mstrParentDirectoryPath As String: Private mstrRelativeParentDirectoryPath As String

Private menmInMemoryDataTableType As InMemoryDataTableType  ' this includes 'vblnPutInCollection' boolean value and 'vblnPutInDictionary' boolean value

Private mstrFileInfomationTypesDelimitedByComma As String

Private mstrExtensionTextByDelimitedChar As String

Private mstrRawWildCardsByDelimitedChar As String

Private mstrDelimitedChar As String


Private mintMinimumHierarchicalOrderToSearch As Long: Private mintMaximumHierarchicalOrderToSearch As Long

Private mintCurrentHierarchicalOrder As Long


Private mblnAllowToSortByFileNameOrDirectoryNameWhenUseWinAPI As Boolean

Private mblnIsFileInfoNeeded As Boolean

Private mblnIsDirectoryInfoNeeded As Boolean

Private mstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mstrParentDirectoryPath = ""
    
    mstrRelativeParentDirectoryPath = ""


    menmInMemoryDataTableType = ExistedAsCollectionObjectInMemory

    mstrFileInfomationTypesDelimitedByComma = "RelativeFullPath"

    mstrExtensionTextByDelimitedChar = "*"
    
    mstrRawWildCardsByDelimitedChar = "*"

    mstrDelimitedChar = ","


    mintMinimumHierarchicalOrderToSearch = -1    ' If 0, search only the 'Parent directory path', If you need to search sub-directories till last hierarchy, set to -1
    
    mintMaximumHierarchicalOrderToSearch = -1    ' If 0, search only the 'Parent directory path', If you need to search sub-directories till last hierarchy, set to -1
    
    mintCurrentHierarchicalOrder = 0
    

    mstrRelativeParentDirectoryPath = ""

    mblnAllowToSortByFileNameOrDirectoryNameWhenUseWinAPI = True

    mblnIsFileInfoNeeded = True
    
    mblnIsDirectoryInfoNeeded = False
    
    mstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma = ""
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////

'''
'''
'''
Public Property Get ParentDirectoryPath() As String
    
    ParentDirectoryPath = mstrParentDirectoryPath
End Property
Public Property Let ParentDirectoryPath(ByVal vstrParentDirectoryPath As String)

    mstrParentDirectoryPath = vstrParentDirectoryPath
End Property

'''
'''
'''
Public Property Get RelativeParentDirectoryPath() As String
    
    RelativeParentDirectoryPath = mstrRelativeParentDirectoryPath
End Property
Public Property Let RelativeParentDirectoryPath(ByVal vstrRelativeParentDirectoryPath As String)

    mstrRelativeParentDirectoryPath = vstrRelativeParentDirectoryPath
End Property


'**---------------------------------------------
'** getting data type either Collection or Scripting.Dictionary
'**
'** If you need to confirm the only list, the Collection is faster than Scripting.Dictionary
'**---------------------------------------------
Public Property Get SearchedDataTableType() As InMemoryDataTableType

    SearchedDataTableType = menmInMemoryDataTableType
End Property
Public Property Let SearchedDataTableType(ByVal venmInMemoryDataTableType As InMemoryDataTableType)

    menmInMemoryDataTableType = venmInMemoryDataTableType
End Property


'**---------------------------------------------
'** Needed file system information type and Wild-card options in searching
'**---------------------------------------------
'''
''' information strings are restricted from specified characters ''
'''
Public Property Get FileInfomationTypesDelimitedByComma() As String

    FileInfomationTypesDelimitedByComma = mstrFileInfomationTypesDelimitedByComma
End Property
Public Property Let FileInfomationTypesDelimitedByComma(ByVal vstrFileInfomationTypesDelimitedByComma As String)

    mstrFileInfomationTypesDelimitedByComma = vstrFileInfomationTypesDelimitedByComma
End Property


Public Property Get ExtensionTextByDelimitedChar() As String

    ExtensionTextByDelimitedChar = mstrExtensionTextByDelimitedChar
End Property
Public Property Let ExtensionTextByDelimitedChar(ByVal vstrExtensionTextByDelimitedChar As String)

    mstrExtensionTextByDelimitedChar = vstrExtensionTextByDelimitedChar
End Property

Public Property Get RawWildCardsByDelimitedChar() As String

    RawWildCardsByDelimitedChar = mstrRawWildCardsByDelimitedChar
End Property
Public Property Let RawWildCardsByDelimitedChar(ByVal vstrRawWildCardsByDelimitedChar As String)

    mstrRawWildCardsByDelimitedChar = vstrRawWildCardsByDelimitedChar
End Property

Public Property Get DelimitedChar() As String

    DelimitedChar = mstrDelimitedChar
End Property
Public Property Let DelimitedChar(ByVal vstrDelimitedChar As String)

    mstrDelimitedChar = vstrDelimitedChar
End Property



'**---------------------------------------------
'** control searching hierarchical order period about sub-directories of the parent directory
'**---------------------------------------------
'''
'''
'''
Public Property Get MinimumHierarchicalOrderToSearch() As Long
    
    MinimumHierarchicalOrderToSearch = mintMinimumHierarchicalOrderToSearch
End Property
Public Property Let MinimumHierarchicalOrderToSearch(ByVal vintMinimumHierarchicalOrderToSearch As Long)

    mintMinimumHierarchicalOrderToSearch = vintMinimumHierarchicalOrderToSearch
End Property

Public Property Get MaximumHierarchicalOrderToSearch() As Long
    
    MaximumHierarchicalOrderToSearch = mintMaximumHierarchicalOrderToSearch
End Property
Public Property Let MaximumHierarchicalOrderToSearch(ByVal vintMaximumHierarchicalOrderToSearch As Long)

    mintMaximumHierarchicalOrderToSearch = vintMaximumHierarchicalOrderToSearch
End Property

'''
''' this is always 0 before searching file-system
'''
Public Property Get CurrentHierarchicalOrder() As Long
    
    CurrentHierarchicalOrder = mintCurrentHierarchicalOrder
End Property
Public Property Let CurrentHierarchicalOrder(ByVal vintCurrentHierarchicalOrder As Long)

    mintCurrentHierarchicalOrder = vintCurrentHierarchicalOrder
End Property

'''
''' easy setter
'''
Public Property Get AllowToSearchSubDirectories() As Boolean

    Dim blnAllow As Boolean
    
    If mintMinimumHierarchicalOrderToSearch = -1 And mintMaximumHierarchicalOrderToSearch = -1 Then
    
        blnAllow = True
    Else
        blnAllow = False
    End If

    AllowToSearchSubDirectories = blnAllow
End Property

Public Property Let AllowToSearchSubDirectories(ByVal vblnAllowToSearchSubDirectories As Boolean)

    If vblnAllowToSearchSubDirectories Then
    
        If mintMinimumHierarchicalOrderToSearch <> -1 Then
        
            mintMinimumHierarchicalOrderToSearch = -1
        End If
        
        If mintMaximumHierarchicalOrderToSearch <> -1 Then
        
            mintMaximumHierarchicalOrderToSearch = -1
        End If
    Else
        
        If mintMinimumHierarchicalOrderToSearch = -1 Then
        
            mintMinimumHierarchicalOrderToSearch = 0
        End If
        
        If mintMaximumHierarchicalOrderToSearch = -1 Then
        
            mintMaximumHierarchicalOrderToSearch = 0
        End If
    End If
End Property


'**---------------------------------------------
'** listing of either file or directory, or both file and directory
'**---------------------------------------------
Public Property Get IsFileInfoNeeded() As Boolean

    IsFileInfoNeeded = mblnIsFileInfoNeeded
End Property
Public Property Let IsFileInfoNeeded(ByVal vblnIsFileInfoNeeded As Boolean)

    mblnIsFileInfoNeeded = vblnIsFileInfoNeeded
End Property

Public Property Get IsDirectoryInfoNeeded() As Boolean

    IsDirectoryInfoNeeded = mblnIsDirectoryInfoNeeded
End Property
Public Property Let IsDirectoryInfoNeeded(ByVal vblnIsDirectoryInfoNeeded As Boolean)

    mblnIsDirectoryInfoNeeded = vblnIsDirectoryInfoNeeded
End Property

'**---------------------------------------------
'** Options when FindFileFile Windows API
'**---------------------------------------------
Public Property Get AllowToSortByFileNameOrDirectoryNameWhenUseWinAPI() As Boolean

    AllowToSortByFileNameOrDirectoryNameWhenUseWinAPI = mblnAllowToSortByFileNameOrDirectoryNameWhenUseWinAPI
End Property
Public Property Let AllowToSortByFileNameOrDirectoryNameWhenUseWinAPI(ByVal vblnAllowToSortByFileNameOrDirectoryNameWhenUseWinAPI As Boolean)

    mblnAllowToSortByFileNameOrDirectoryNameWhenUseWinAPI = vblnAllowToSortByFileNameOrDirectoryNameWhenUseWinAPI
End Property


'**---------------------------------------------
'** Searching exception for file-name or directory name
'**---------------------------------------------
Public Property Get SearchingExceptionFileOrDirectoryNamesDelimitedByComma() As String

    SearchingExceptionFileOrDirectoryNamesDelimitedByComma = mstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma
End Property
Public Property Let SearchingExceptionFileOrDirectoryNamesDelimitedByComma(ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String)

    mstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma = vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma
End Property

