Attribute VB_Name = "FileSysConvertCodePages"
'
'   Tools for coverting code-texts
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADODB.Stream in ADO
'       Dependent on TextFileCodePagesConversion.bas and FileSysListFilesByVBADir.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun, 21/Jan/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub ConvertPluralACharSetTextFilesToAnotherCharSetInSpecifyingDirectory(ByVal vstrInputRootDir As String, _
        Optional ByVal vstrOutputRootDir As String, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vstrInputTextCharset As String = "UTF-8", _
        Optional ByVal vstrOutputTextCharset As String = "Shift-JIS")
        
    Dim objInputPaths As Collection, strInputPath As String, strOutputPath As String, strOutputDir As String
    Dim varInputPath As Variant
    
    If Not DirectoryExistsByVbaDir(vstrOutputRootDir) Then
    
        ForceToCreateDirectoryByVbaDir vstrOutputRootDir
    End If
    
    Set objInputPaths = GetFilePathsUsingVbaDir(vstrInputRootDir, vstrRawWildCardsByDelimitedChar)
    
    For Each varInputPath In objInputPaths
    
        strInputPath = varInputPath
        
        strOutputPath = Replace(strInputPath, vstrInputRootDir, vstrOutputRootDir)
        
        strOutputDir = GetParentDirectoryPathByVbaDir(strOutputPath)
        
        If Not DirectoryExistsByVbaDir(strOutputDir) Then
        
            ForceToCreateDirectoryByVbaDir strOutputDir
        End If
        
        ConvertACharSetTextFileToAnotherCharSetTextFile strInputPath, strOutputPath, vstrInputTextCharset, vstrOutputTextCharset
    Next
End Sub
        

'''
''' In converting, create temporary directory as a file-system access
'''
Public Sub ConvertACharSetTextFileToAnotherCharSetWithUsingTemporaryFile(ByRef rstrACharSetTextFilePath As String, _
        Optional ByVal vstrBeforeTextCharset As String = "UTF-8", _
        Optional ByVal vstrAfterTextCharset As String = "Shift-JIS", _
        Optional ByVal vblnAllowToDeleteTemporaryFile As Boolean = True)

    Dim strDir As String, strTmpPath As String

    Const strSubDir As String = "CharSetTmp"
    
    strDir = GetParentDirectoryPathByVbaDir(rstrACharSetTextFilePath) & "\" & strSubDir
    
    strTmpPath = strDir & "\" & GetBaseNameByVbaDir(rstrACharSetTextFilePath) & "." & GetExtensionNameByVbaDir(rstrACharSetTextFilePath)
    
    ForceToCreateDirectoryByVbaDir strDir
    
    ConvertACharSetTextFileToAnotherCharSetTextFile rstrACharSetTextFilePath, strTmpPath, vstrBeforeTextCharset, vstrAfterTextCharset

    ' Do over-write copy
    VBA.FileSystem.FileCopy strTmpPath, rstrACharSetTextFilePath

    If vblnAllowToDeleteTemporaryFile Then
    
        VBA.FileSystem.Kill strTmpPath
        
        VBA.FileSystem.RmDir strDir
    End If
End Sub


