Attribute VB_Name = "UTfFileSysFilesInfo"
'
'   List up files below the selected directory by plural method, such as Dir function and FileSystemObject
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 17/Jun/2022    Kalmclaeyd Tarclanus    Refactoring
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity test
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfGetFileDirectoryListComprehensivelyBelowCurrentOfficeFileObject()


'    msubSanityTestOfGetFileDirectoryListComprehensivelyByFSO GetCurrentOfficeFileObject().Path
'
    msubSanityTestOfGetFileDirectoryListComprehensivelyByVBADir GetCurrentOfficeFileObject().Path
End Sub


'''
'''
'''
Private Sub msubSanityTestOfGetFileDirectoryListComprehensivelyByVBADir(ByVal vstrRootDirectoryPath As String)

    Dim objDataTableCol As Collection, objDataTableDic As Scripting.Dictionary
    
    
    Debug.Print "About files"

'    Set objDataTableCol = GetRelativeFilePathColByVBADir(vstrRootDirectoryPath)
'
'    DebugDumpColAsString objDataTableCol
'
'    Set objDataTableDic = GetRelativeFilePathAndLastModifedDateDicByVBADir(vstrRootDirectoryPath)
'
'    DebugDic objDataTableDic

    Set objDataTableCol = GetFileAttributeInfoColOfDefaultCombinationByVBADir(vstrRootDirectoryPath)

    DebugDumpNestedColAsString objDataTableCol

'    Set objDataTableDic = GetRelativeFilePathOnlyKeyDicByVBADir(vstrRootDirectoryPath)
'
'    DebugDic objDataTableDic
'
'    Set objDataTableDic = GetRelativeFilePathAndFileAttributeInfoColOfDefaultCombinationDicByVBADir(vstrRootDirectoryPath)
'
'    DebugDic objDataTableDic
'
'    Debug.Print "About directories"
'
'    Set objDataTableCol = GetRelativeDirectoryPathColByVBADir(vstrRootDirectoryPath)
'
'    DebugDumpColAsString objDataTableCol
'
'    Set objDataTableDic = GetRelativeDirectoryPathAndLastModifedDateDicByVBADir(vstrRootDirectoryPath)
'
'    DebugDic objDataTableDic
End Sub



'''
'''
'''
Private Sub msubSanityTestOfGetFileDirectoryListComprehensivelyByFSO(ByVal vstrRootDirectoryPath As String)

    Dim objDataTableCol As Collection, objDataTableDic As Scripting.Dictionary
    
    
    Debug.Print "About files"
    
    Set objDataTableCol = GetRelativeFilePathColByFSO(vstrRootDirectoryPath)
    
    DebugDumpColAsString objDataTableCol
    
    Set objDataTableDic = GetRelativeFilePathAndLastModifedDateDicByFSO(vstrRootDirectoryPath)

    DebugDic objDataTableDic

    Set objDataTableCol = GetFileAttributeInfoColOfDefaultCombinationByFSO(vstrRootDirectoryPath)

    DebugDumpNestedColAsString objDataTableCol

    Set objDataTableDic = GetRelativeFilePathOnlyKeyDicByFSO(vstrRootDirectoryPath)

    DebugDic objDataTableDic

    Set objDataTableDic = GetRelativeFilePathAndFileAttributeInfoColOfDefaultCombinationDicByFSO(vstrRootDirectoryPath)
    
    DebugDic objDataTableDic

    Debug.Print "About directories"
    
    Set objDataTableCol = GetRelativeDirectoryPathColByFSO(vstrRootDirectoryPath)
    
    DebugDumpColAsString objDataTableCol
    
    Set objDataTableDic = GetRelativeDirectoryPathAndLastModifedDateDicByFSO(vstrRootDirectoryPath)

    DebugDic objDataTableDic
End Sub

'''
'''
'''
Private Sub msubSanityTestOfVBADirFunction()

    Dim objFilePaths As Collection


    Set objFilePaths = GetSubFilePathsByVBADir(GetCurrentOfficeFileObject().Path, "*.xlsm")
    
    If Not objFilePaths Is Nothing Then DebugDumpColAsString objFilePaths
    
    ' following input is illegal
    Debug.Print "About test of *.xlsm;*.docx"
    
    Set objFilePaths = GetSubFilePathsByVBADir(GetCurrentOfficeFileObject().Path, "*.xlsm;*.docx")

    ' Nothing
    If Not objFilePaths Is Nothing Then DebugDumpColAsString objFilePaths

    Debug.Print "About test of *.docx"
    
    Set objFilePaths = GetSubFilePathsByVBADir(GetCurrentOfficeFileObject().Path, "*.docx")
    
    If Not objFilePaths Is Nothing Then DebugDumpColAsString objFilePaths
    
    Debug.Print "About test of *"
    
    Set objFilePaths = GetSubFilePathsByVBADir(GetCurrentOfficeFileObject().Path, "*")
    
    If Not objFilePaths Is Nothing Then DebugDumpColAsString objFilePaths
    
    
    Debug.Print "About test of *.acc"
    
    Set objFilePaths = GetSubFilePathsByVBADir(GetCurrentOfficeFileObject().Path, "*.acc")
    
    If Not objFilePaths Is Nothing Then DebugDumpColAsString objFilePaths
    
        Debug.Print "About test of *.accdb"
        
    Set objFilePaths = GetSubFilePathsByVBADir(GetCurrentOfficeFileObject().Path, "*.accdb")
    
    If Not objFilePaths Is Nothing Then DebugDumpColAsString objFilePaths
End Sub


