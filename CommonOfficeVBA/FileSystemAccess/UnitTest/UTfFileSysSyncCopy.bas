Attribute VB_Name = "UTfFileSysSyncCopy"
'
'   Sanity test to synchronize files-structure
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       Dependent on IWshRuntimeLibrary.WshShell for managing short-cut file
'       Dependent on MSComctlLib.ProgressBar
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 11/Jan/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToUTfFileSysSyncCopy()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
    
    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", mfstrGetModuleNamesToOpenInVBE()
End Sub

'''
'''
'''
Private Function mfstrGetModuleNamesToOpenInVBE() As String

    Dim strNames As String
    
    strNames = "FileSysSyncCopy,FileSysListFilesByVBADir,FileSysFilesInfo"

    mfstrGetModuleNamesToOpenInVBE = strNames
End Function


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub SanityTestToSynchronizeCopyOfFiles()

    SychronizeFilesBetweenTwoDirectories GetTemporarySynchronizingCopySourceDir(), GetTemporarySynchronizingCopyDestinationDir()
End Sub

'''
'''
'''
Public Sub SanityTestToDeleteAllSynchronizeCopyTestDirectories()

    DeleteWildCardMatchedFilesByVbaDir GetTemporarySynchronizingCopySourceDir()
    
    DeleteWildCardMatchedFilesByVbaDir GetTemporarySynchronizingCopyDestinationDir()
End Sub

'''
'''
'''
Public Function GetTemporarySynchronizingCopySourceDir() As String

    Dim strDir As String: strDir = GetTemporaryDevelopmentVBARootDir() & "\SyncFileCopyTest\Src"

    ForceToCreateDirectory strDir
    
    GetTemporarySynchronizingCopySourceDir = strDir
End Function

'''
'''
'''
Public Function GetTemporarySynchronizingCopyDestinationDir() As String

    Dim strDir As String: strDir = GetTemporaryDevelopmentVBARootDir() & "\SyncFileCopyTest\Dst"

    ForceToCreateDirectory strDir
    
    GetTemporarySynchronizingCopyDestinationDir = strDir
End Function

'''
'''
'''
Public Sub GenerateTestingTextFilesForTestingSynchronizing(ByVal vstrTestDir As String)

    Dim objFS As Scripting.FileSystemObject: Set objFS = New Scripting.FileSystemObject
    
    GenerateTestingTextFilesForTestingSynchronizingForResursiveCall vstrTestDir, 0, objFS
End Sub


'''
'''
'''
Public Sub GenerateTestingTextFilesForTestingSynchronizingForResursiveCall(ByVal vstrTestDir As String, _
        ByVal vintDirectoryDepth As Long, _
        ByRef robjFS As Scripting.FileSystemObject, _
        Optional ByVal vblnAllowToForceToUseSameFileNameForEachDirectoryDepth As Boolean = False)

    Dim objChildDirs As Collection, objExistedFiles As Collection, varSubDirPath As Variant, strSubDirPath As String
    Dim varExistedFilePath As Variant, strExistedFilePath As String, objDir As Scripting.Folder
    
    Dim strNewDirName As String, strNewTextFileName As String
    
    
    Set objChildDirs = GetDirectoryPathsUsingVbaDirWithoutSearchingSubDirectoriesOfFoundDirectories(vstrTestDir)

    Set objExistedFiles = GetFilePathsUsingVbaDirWithoutSearchingSubDirectories(vstrTestDir, "*.txt")

    For Each varSubDirPath In objChildDirs
    
        strSubDirPath = varSubDirPath
        
        ' resursive call
        
        GenerateTestingTextFilesForTestingSynchronizingForResursiveCall strSubDirPath, vintDirectoryDepth + 1, robjFS
    Next

    If vblnAllowToForceToUseSameFileNameForEachDirectoryDepth Then

        strNewDirName = "D" & CStr(objChildDirs.Count + 1)
    Else
        If vintDirectoryDepth > 0 Then
    
            strNewDirName = "D" & mfstrConvertXlColumnIndexToLetter(vintDirectoryDepth) & CStr(objChildDirs.Count + 1)
        Else
            strNewDirName = "D" & CStr(objChildDirs.Count + 1)
        End If
    End If
    
    If vblnAllowToForceToUseSameFileNameForEachDirectoryDepth Then

        strNewTextFileName = "Generated_" & CStr(objExistedFiles.Count + 1) & ".txt"
    Else
        If vintDirectoryDepth > 0 Then
        
            Set objDir = robjFS.GetFolder(vstrTestDir)
        
            strNewTextFileName = "Generated_" & objDir.Name & "_" & CStr(objExistedFiles.Count + 1) & ".txt"
        Else
            strNewTextFileName = "Generated_" & CStr(objExistedFiles.Count + 1) & ".txt"
        End If
    End If
    
    For Each varExistedFilePath In objExistedFiles
    
        strExistedFilePath = varExistedFilePath
    
        msubAppendTestingText strExistedFilePath, robjFS
    Next

    msubCreateTestingText vstrTestDir & "\" & strNewTextFileName, robjFS

    robjFS.CreateFolder vstrTestDir & "\" & strNewDirName
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////


'**---------------------------------------------
'** Tools for data conversion on Excel.Worksheet
'**---------------------------------------------
'''
''' Duplicated for test
'''
''' Ref. https://support.microsoft.com/ja-jp/help/833402/how-to-convert-excel-column-numbers-into-alphabetical-characters
'''
Private Function mfstrConvertXlColumnIndexToLetter(ByVal vintColumn As Integer) As String
   
   Dim intAlpha As Integer, intRemainder As Integer
   
   intAlpha = Int((vintColumn - 1) / 26)
   
   intRemainder = vintColumn - (intAlpha * 26)
   
   If intAlpha > 0 Then
      
      mfstrConvertXlColumnIndexToLetter = Chr(intAlpha + 64)
   End If
   
   If intRemainder > 0 Then
      
      mfstrConvertXlColumnIndexToLetter = mfstrConvertXlColumnIndexToLetter & Chr(intRemainder + 64)
   End If
End Function



'''
'''
'''
Private Sub msubAppendTestingText(ByRef rstrTextFilePath As String, ByRef robjFS As Scripting.FileSystemObject)

    With robjFS
    
        With .OpenTextFile(rstrTextFilePath, ForAppending, False)
    
            .WriteLine "Added text - File-system synchonizing-copy test at " & Format(Now(), "ddd, yyyy/mm/dd hh:mm:ss") & vbNewLine
    
            .Close
        End With
    End With
End Sub

'''
'''
'''
Private Sub msubCreateTestingText(ByRef rstrTextFilePath As String, ByRef robjFS As Scripting.FileSystemObject)

    With robjFS
    
        With .CreateTextFile(rstrTextFilePath, True)
        
            .WriteLine "Created text - File-system synchonizing-copy test at " & Format(Now(), "ddd, yyyy/mm/dd hh:mm:ss") & vbNewLine
    
            .Close
        End With
    End With
End Sub


