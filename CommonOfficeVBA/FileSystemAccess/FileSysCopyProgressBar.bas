Attribute VB_Name = "FileSysCopyProgressBar"
'
'   Copy files with showing the MSComctlLib.ProgressBar
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       Dependent on MSComctlLib.ProgressBar
'       Dependent on UProgressBarForm.frm
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 12/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Copy for synchronizing
'**---------------------------------------------
'''
'''
'''
''' <Argument>vobjFilePaths: Input</Argument>
''' <Argument>vstrSourceParentDirectoryPathPart: Input</Argument>
''' <Argument>vstrDestinationParentDirectoryPathPart: Input</Argument>
''' <Return></Return>
Public Function CopyFilesWithKeepingDirectoryStructure(ByVal vobjFilePaths As Collection, _
        ByVal vstrSourceParentDirectoryPathPart As String, _
        ByVal vstrDestinationParentDirectoryPathPart As String) As Collection
        
        
    Dim objLogDTCol As Collection, objDoCopySrcToDstDic As Scripting.Dictionary, objDoCreateShortcutDstToTargetPathDic As Scripting.Dictionary
    
    
    Set objLogDTCol = GetListsOfCopyFilesWithKeepingDirectoryStructure(objDoCopySrcToDstDic, _
            objDoCreateShortcutDstToTargetPathDic, _
            vobjFilePaths, _
            vstrSourceParentDirectoryPathPart, _
            vstrDestinationParentDirectoryPathPart)
    
    
    CopyFilesOrCreateShortcutsWithSimpleProgressBar objDoCopySrcToDstDic, _
            objDoCreateShortcutDstToTargetPathDic, _
            New Scripting.FileSystemObject, _
            New IWshRuntimeLibrary.WshShell
     
    Set CopyFilesWithKeepingDirectoryStructure = objLogDTCol
End Function

'''
''' Use MSComctlLib.ProgressBar
'''
''' <Argument>robjDoCopySrcToDstDic: Input</Argument>
''' <Argument>robjDoCreateShortcutDstToTargetPathDic: Input</Argument>
''' <Argument>vobjFS: Input</Argument>
''' <Argument>vobjShell: Input</Argument>
Public Sub CopyFilesOrCreateShortcutsWithSimpleProgressBar(ByRef robjDoCopySrcToDstDic As Scripting.Dictionary, _
        ByRef robjDoCreateShortcutDstToTargetPathDic As Scripting.Dictionary, _
        ByVal vobjFS As Scripting.FileSystemObject, _
        ByVal vobjShell As IWshRuntimeLibrary.WshShell)

    CopyFilesWithSimpleProgressBar robjDoCopySrcToDstDic, vobjFS
 
    CreateShortCutWithSimpleProgressBar robjDoCreateShortcutDstToTargetPathDic, vobjShell
End Sub

'**---------------------------------------------
'** copy with MSComctlLib.ProgressBar
'**---------------------------------------------
'''
'''
'''
''' <Argument>vobjDoCopySrcToDstDic: Input</Argument>
''' <Argument>vobjFS: Input</Argument>
''' <Argument>vblnUseProgressBar: Input</Argument>
''' <Return>Collection</Return>
Public Function CopyFilesWithSimpleProgressBar(ByVal vobjDoCopySrcToDstDic As Scripting.Dictionary, _
        ByVal vobjFS As Scripting.FileSystemObject, _
        Optional ByVal vblnUseProgressBar As Boolean = True) As Collection


    Dim varSourcePathAndFileCopySituation As Variant, strSourcePath As String, strDestinationPath As String, objDoubleStopWatch As DoubleStopWatch
    
    Dim enmFileCopySituation As FileSyncCopySituation, strCompounds() As String, strCopyMessage As String
    
    Dim i As Long, intMax As Long
    
    Dim objCopiedFilePaths As Collection, strDestinationFilePath As String
    

    Set objCopiedFilePaths = New Collection

    If Not vobjDoCopySrcToDstDic Is Nothing Then
    
        If vobjDoCopySrcToDstDic.Count > 0 Then

            If vblnUseProgressBar Then
            
                Set objDoubleStopWatch = New DoubleStopWatch
            
                With New UProgressBarForm
                
                    .Caption = GetTextOfStrKeyFileSysSyncCopyCaptionUpdateFiles()
                    
                    .ShortMessage = ""
                    
                    intMax = vobjDoCopySrcToDstDic.Count
                    
                    .Min = 0: .Value = 0: .Max = intMax
                    
                    .Show vbModeless
                    
                    objDoubleStopWatch.MeasureStart
                    
                    i = 1
                    
                    For Each varSourcePathAndFileCopySituation In vobjDoCopySrcToDstDic.Keys
                    
                        strCompounds = Split(varSourcePathAndFileCopySituation, ";")
                    
                        enmFileCopySituation = CInt(strCompounds(1))
                    
                        strSourcePath = strCompounds(0)
                        
                        strDestinationPath = vobjDoCopySrcToDstDic.Item(varSourcePathAndFileCopySituation)
                    
                        objDoubleStopWatch.MeasureInterval
                    
                        Select Case enmFileCopySituation
                        
                            Case FileSyncCopySituation.CopyFileNewToFileSystemSync
                            
                                strCopyMessage = GetTextOfStrKeyFileSysSyncCopyMesCopyingNew()
                                
                            Case FileSyncCopySituation.UpdateFileToFileSystemSync
                        
                                strCopyMessage = GetTextOfStrKeyFileSysSyncCopyMesCopyingAsUpdate()
                                
                            Case Else
                                strCopyMessage = GetTextOfStrKeyFileSysSyncCopyCaptionUpdateFiles()
                        End Select
                    
                        .Caption = strCopyMessage & " - " & CStr(i) & " / " & CStr(intMax)
                        
                        .ShortMessage = GetTextOfStrKeyFileSysSyncCopyMesCopying() & "... " & vobjFS.GetFileName(strSourcePath) & " ..." & vbNewLine & GetTextOfStrKeyFileSysSyncCopyMesElapsedTime() & ": " & objDoubleStopWatch.ElapsedTimeByString
                        
                        On Error Resume Next
                        
                        vobjFS.CopyFile strSourcePath, strDestinationPath, True
                        
                        If Err.Number = 0 Then
                        
                            'Debug.Print "Finished copying: " & vobjFS.GetFileName(strSourcePath) & " at " & Format(Now(), "hh:mm:ss") & " " & FormatDateAndWeekdayJapanese(Now)
                            
                            strDestinationFilePath = GetDirectoryPathAfterCuttingLastBackSlashFromThePathIfItHas(strDestinationPath) & "\" & vobjFS.GetFileName(strSourcePath)
                            
                            objCopiedFilePaths.Add strDestinationFilePath
                        Else
                            Debug.Print "Failed to copy, probably the Administrator authority is needed: " & vobjFS.GetFileName(strSourcePath) & " at " & Format(Now(), "hh:mm:ss") & " " & FormatDateAndWeekdayJapanese(Now)
                        End If
                        
                        On Error GoTo 0
                        
                        .Value = .Value + 1
                        
                        i = i + 1
                    Next
                    
                    .CloseForm
                End With
            Else
                With vobjDoCopySrcToDstDic
                
                    For Each varSourcePathAndFileCopySituation In .Keys
                    
                        strCompounds = Split(varSourcePathAndFileCopySituation, ";")
                    
                        enmFileCopySituation = CInt(strCompounds(1))
                    
                        strSourcePath = strCompounds(0)
                        
                        strDestinationPath = .Item(varSourcePathAndFileCopySituation)
                
                        On Error Resume Next
                
                        vobjFS.CopyFile strSourcePath, strDestinationPath, True
                        
                        If Err.Number = 0 Then
                        
                            'Debug.Print "Finished copying: " & vobjFS.GetFileName(strSourcePath) & " at " & Format(Now(), "hh:mm:ss") & " " & FormatDateAndWeekdayJapanese(Now)
                            
                            strDestinationFilePath = GetDirectoryPathAfterCuttingLastBackSlashFromThePathIfItHas(strDestinationPath) & "\" & vobjFS.GetFileName(strSourcePath)
                            
                            objCopiedFilePaths.Add strDestinationFilePath
                        Else
                            Debug.Print "Failed to copy, probably the Administrator authority is needed: " & vobjFS.GetFileName(strSourcePath) & " at " & Format(Now(), "hh:mm:ss") & " " & FormatDateAndWeekdayJapanese(Now)
                        End If
                        
                        On Error GoTo 0
                    Next
                End With
            End If
        End If
    End If
    
    Set CopyFilesWithSimpleProgressBar = objCopiedFilePaths
End Function



'''
'''
'''
Public Sub CreateShortCutWithSimpleProgressBar(ByVal vobjDoCreateShortcutDstToTargetPathDic As Scripting.Dictionary, _
        ByVal vobjShell As IWshRuntimeLibrary.WshShell, _
        Optional ByVal vblnUseProgressBar As Boolean = True)


    Dim varDestinationShortcutFilePath As Variant, strDestinationShortcutFilePath As String, strDestinationSideTargetPath As String
    Dim objShortcut As IWshRuntimeLibrary.WshShortcut, objDoubleStopWatch As DoubleStopWatch
    Dim i As Long, intMax As Long
    Dim objFS As Scripting.FileSystemObject


    If Not vobjDoCreateShortcutDstToTargetPathDic Is Nothing Then
    
        If vobjDoCreateShortcutDstToTargetPathDic.Count > 0 Then
        
            If vblnUseProgressBar Then
            
                Set objFS = New Scripting.FileSystemObject
            
                Set objDoubleStopWatch = New DoubleStopWatch
            
                With New UProgressBarForm
                
                    
                    .Caption = GetTextOfStrKeyFileSysSyncCopyCaptionUpdateDestinationShortcut()
                    
                    .ShortMessage = ""
                    
                    intMax = vobjDoCreateShortcutDstToTargetPathDic.Count
                    
                    .Min = 0: .Value = 0: .Max = intMax
                    
                    .Show vbModeless
                    
                    objDoubleStopWatch.MeasureStart
                    
                    i = 1
                    
                    For Each varDestinationShortcutFilePath In vobjDoCreateShortcutDstToTargetPathDic.Keys
                    
                        strDestinationShortcutFilePath = varDestinationShortcutFilePath
                        
                        strDestinationSideTargetPath = vobjDoCreateShortcutDstToTargetPathDic.Item(varDestinationShortcutFilePath)
                    
                        objDoubleStopWatch.MeasureInterval
                    
                        .Caption = GetTextOfStrKeyFileSysSyncCopyCaptionUpdateDestinationShortcut() & " - " & CStr(i) & " / " & CStr(intMax)
                        
                        .ShortMessage = GetTextOfStrKeyFileSysSyncCopyMesUpdateShortcut() & " " & objFS.GetFileName(strDestinationSideTargetPath) & " ..." & vbNewLine & GetTextOfStrKeyFileSysSyncCopyMesElapsedTime() & ": " & objDoubleStopWatch.ElapsedTimeByString
                        
                        Set objShortcut = vobjShell.CreateShortcut(strDestinationShortcutFilePath)
        
                        With objShortcut
        
                            .TargetPath = strDestinationSideTargetPath
        
                            .Save
                        End With
                        
                        .Value = .Value + 1
                        
                        i = i + 1
                    Next
                End With
            Else
                With vobjDoCreateShortcutDstToTargetPathDic
                
                    For Each varDestinationShortcutFilePath In .Keys
                    
                        strDestinationShortcutFilePath = varDestinationShortcutFilePath
                        
                        strDestinationSideTargetPath = .Item(varDestinationShortcutFilePath)
                    
                        Set objShortcut = vobjShell.CreateShortcut(strDestinationShortcutFilePath)
        
                        With objShortcut
        
                            .TargetPath = strDestinationSideTargetPath
        
                            .Save
                        End With
                    Next
                End With
            End If
        End If
    End If
End Sub


