Attribute VB_Name = "FileSysSyncCopyForTextFiles"
'
'   Copy files only when there are some specified differences. This is dependent on IsDifferentBetweenTextFilesWithSpecifiedIgnoreRegExpPatternLinesByFileSystemObject() function
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       Dependent on CompareTextFiles.bas
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  4/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' ref. CopyFilesWithKeepingDirectoryStructure in FileSysCopyProgressBar.bas
'''
''' <Argument>robjLogDTCol: Output - loggings when checking diff</Argument>
''' <Argument>vobjTextFilePaths: Input - Excel book full paths</Argument>
''' <Argument>vstrSourceParentDirectoryPathPart: Input</Argument>
''' <Argument>vstrDestinationParentDirectoryPathPart: Input</Argument>
''' <Argument>vobjIgnoreRegExpPatternsDic: Input - Dictionary(Of Key[1st pattern], Value[2nd pattern])</Argument>
''' <Return>Collection: copied file paths</Return>
Public Function CopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreTextDifferences(ByRef robjLogDTCol As Collection, _
        ByVal vobjTextFilePaths As Collection, _
        ByVal vstrSourceParentDirectoryPathPart As String, _
        ByVal vstrDestinationParentDirectoryPathPart As String, _
        Optional ByVal vobjIgnoreRegExpPatternsDic As Scripting.Dictionary = Nothing) As Collection
        

    Dim objCopiedFilePaths As Collection, objLogDTCol As Collection, objDoCopySrcToDstDic As Scripting.Dictionary, objDoCreateShortcutDstToTargetPathDic As Scripting.Dictionary
    
    
    Set objLogDTCol = GetListsOfCopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreTextDifferences(objDoCopySrcToDstDic, _
            vobjTextFilePaths, _
            vstrSourceParentDirectoryPathPart, _
            vstrDestinationParentDirectoryPathPart, _
            vobjIgnoreRegExpPatternsDic)

    Set objCopiedFilePaths = CopyFilesWithSimpleProgressBar(objDoCopySrcToDstDic, New Scripting.FileSystemObject)

    ' Outputs
    Set robjLogDTCol = objLogDTCol
    
    Set CopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreTextDifferences = objCopiedFilePaths
End Function


'''
''' ref. GetListsOfCopyFilesWithKeepingDirectoryStructure in FileSysSyncCopy.bas
'''
''' <Argument>robjDoCopySrcToDstDic: Output</Argument>
''' <Argument>vobjTextFilePaths: Input</Argument>
''' <Argument>vstrSourceParentDirectoryPathPart: Input</Argument>
''' <Argument>vstrDestinationParentDirectoryPathPart: Input</Argument>
''' <Argument>vobjIgnoreRegExpPatternsDic: Input - Dictionary(Of Key[1st pattern], Value[2nd pattern])</Argument>
''' <Return>Collection</Return>
Public Function GetListsOfCopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreTextDifferences(ByRef robjDoCopySrcToDstDic As Scripting.Dictionary, _
        ByVal vobjTextFilePaths As Collection, _
        ByVal vstrSourceParentDirectoryPathPart As String, _
        ByVal vstrDestinationParentDirectoryPathPart As String, _
        Optional ByVal vobjIgnoreRegExpPatternsDic As Scripting.Dictionary = Nothing) As Collection


    Dim objFS As Scripting.FileSystemObject
    
    Dim varPath As Variant, strSourcePath As String, strDestinationDir As String, strDestinationPath As String
    
    Dim blnDoCopy As Boolean, objLogRowCol As Collection, objLogDTCol As Collection
    
    Dim enmFileCopySituation As FileSyncCopySituation, varCompoundKey As Variant


    Set objFS = New Scripting.FileSystemObject
    
    If robjDoCopySrcToDstDic Is Nothing Then Set robjDoCopySrcToDstDic = New Scripting.Dictionary
    
    Set objLogDTCol = New Collection
    
    For Each varPath In vobjTextFilePaths
    
        strSourcePath = varPath
    
        If InStr(1, strSourcePath, vstrSourceParentDirectoryPathPart) > 0 Then
    
            With objFS
            
                strDestinationDir = .GetParentFolderName(Replace(strSourcePath, vstrSourceParentDirectoryPathPart, vstrDestinationParentDirectoryPathPart))
            
                strDestinationPath = strDestinationDir & "\" & .GetFileName(strSourcePath)
                
                blnDoCopy = mfblnIsNeedToCopyOnlyWhenThereAreTextDifferences(enmFileCopySituation, objLogRowCol, strSourcePath, strDestinationPath, objFS, vobjIgnoreRegExpPatternsDic)
                
                If Not objLogRowCol Is Nothing Then
                
                    objLogDTCol.Add objLogRowCol
                End If
            End With
    
            If blnDoCopy Then
                
                ForceToCreateDirectory strDestinationDir, objFS
                
                varCompoundKey = strSourcePath & ";" & CStr(enmFileCopySituation)
                
                robjDoCopySrcToDstDic.Add varCompoundKey, strDestinationDir & "\"
            End If
        End If
    Next

    Set GetListsOfCopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreTextDifferences = objLogDTCol
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
''' ref. mfblnIsNeedToCopy in FileSysSyncCopy.bas
'''
Private Function mfblnIsNeedToCopyOnlyWhenThereAreTextDifferences(ByRef renmFileCopySituation As FileSyncCopySituation, _
        ByRef robjLogRowCol As Collection, _
        ByVal vstrSourcePath As String, _
        ByVal vstrDestinationPath As String, _
        ByVal vobjFS As Scripting.FileSystemObject, _
        Optional ByVal vobjIgnoreRegExpPatternsDic As Scripting.Dictionary = Nothing) As Boolean


    Dim blnDoCopy As Boolean, intSecondDiff As Long, intDayDiff As Long
    
    
    Const intEpsilonOfSecond As Long = 5    ' When the Window copy any file in special combination of an internal HDD and an external HDD, each file timestamp are always shifted between one and two about each file timestamp.
    
    blnDoCopy = False
                
    With vobjFS
                
        If .FileExists(vstrDestinationPath) Then
    
            Set robjLogRowCol = Nothing
    
            ' compare only data-tables on sheets. the auto-shapes on each sheet and modified-last-date and office-file-built-in properties are ignored
            
            blnDoCopy = False
            
            If vobjIgnoreRegExpPatternsDic Is Nothing Then
            
                If IsDifferentBetweenTextFilesByFileSystemObject(vstrSourcePath, vstrDestinationPath) Then
                
                    blnDoCopy = True
                End If
            Else
                If IsDifferentBetweenTextFilesWithSpecifiedIgnoreRegExpPatternLinesByFileSystemObject(vstrSourcePath, vstrDestinationPath, vobjIgnoreRegExpPatternsDic) Then
                
                    blnDoCopy = True
                End If
            End If
            
            If blnDoCopy Then
                
                renmFileCopySituation = UpdateFileToFileSystemSync
            End If
            
            'msubLogOfComparingDateLastModifiedBetweenSourceAndDestination robjLogRowCol, objSourceFile, objDestinationFile, blnDoCopy, intSecondDiff, intDayDiff
        Else
            blnDoCopy = True
            
            renmFileCopySituation = CopyFileNewToFileSystemSync
        End If
    End With

    mfblnIsNeedToCopyOnlyWhenThereAreTextDifferences = blnDoCopy
End Function
