VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "FileSysSearchResult"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   file system searching result data class
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 22/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjFileSysSearchCondition As FileSysSearchCondition

Private mobjCol As Collection

Private mobjDic As Scripting.Dictionary

Private mdblElapsedSecondTime As Double

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mdblElapsedSecondTime = 0#
End Sub
Private Sub Class_Terminate()

    Set mobjFileSysSearchCondition = Nothing
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Get FileSystemSearchCondition() As FileSysSearchCondition

    Set FileSystemSearchCondition = mobjFileSysSearchCondition
End Property

Public Property Get ResultDataTableCol() As Collection

    Set ResultDataTableCol = mobjCol
End Property
Public Property Get ResultDataTableDic() As Scripting.Dictionary

    Set ResultDataTableDic = mobjDic
End Property

Public Property Get ElapsedSecondTime() As Double

    ElapsedSecondTime = mdblElapsedSecondTime
End Property


'**---------------------------------------------
'** Connoted properties of FileSysSearchCondition
'**---------------------------------------------
Public Property Get SearchedDataTableType() As InMemoryDataTableType

    SearchedDataTableType = mobjFileSysSearchCondition.SearchedDataTableType
End Property


Public Property Get FileInfomationTypesDelimitedByComma() As String

    FileInfomationTypesDelimitedByComma = mobjFileSysSearchCondition.FileInfomationTypesDelimitedByComma
End Property

Public Property Get ExtensionTextByDelimitedChar() As String

    ExtensionTextByDelimitedChar = mobjFileSysSearchCondition.ExtensionTextByDelimitedChar
End Property

Public Property Get RawWildCardsByDelimitedChar() As String

    RawWildCardsByDelimitedChar = mobjFileSysSearchCondition.RawWildCardsByDelimitedChar
End Property

Public Property Get DelimitedChar() As String

    DelimitedChar = mobjFileSysSearchCondition.DelimitedChar
End Property


Public Property Get MinimumHierarchicalOrderToSearch() As Long
    
    MinimumHierarchicalOrderToSearch = mobjFileSysSearchCondition.MinimumHierarchicalOrderToSearch
End Property

Public Property Get MaximumHierarchicalOrderToSearch() As Long
    
    MaximumHierarchicalOrderToSearch = mobjFileSysSearchCondition.MaximumHierarchicalOrderToSearch
End Property

Public Property Get CurrentHierarchicalOrder() As Long
    
    CurrentHierarchicalOrder = mobjFileSysSearchCondition.CurrentHierarchicalOrder
End Property


Public Property Get IsFileInfoNeeded() As Boolean

    IsFileInfoNeeded = mobjFileSysSearchCondition.IsFileInfoNeeded
End Property
Public Property Get IsDirectoryInfoNeeded() As Boolean

    IsDirectoryInfoNeeded = mobjFileSysSearchCondition.IsDirectoryInfoNeeded
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub SetFileSystemSearchedResultDataTable(ByVal vobjDataTableResultObject As Object, _
        ByVal vobjFileSystemSearchCondition As FileSysSearchCondition, _
        Optional ByVal vdblElapsedSecondTime As Double = 0#)


    Set mobjFileSysSearchCondition = vobjFileSystemSearchCondition
    
    Select Case mobjFileSysSearchCondition.SearchedDataTableType
    
        Case InMemoryDataTableType.ExistedAsCollectionObjectInMemory
        
            Set mobjCol = vobjDataTableResultObject
        
        Case InMemoryDataTableType.ExistedAsDictionaryObjectInMemory
    
            Set mobjDic = vobjDataTableResultObject
    End Select

    If vdblElapsedSecondTime <> 0# Then
    
        mdblElapsedSecondTime = vdblElapsedSecondTime
    End If
End Sub


'''
'''
'''
Public Function GetMessageOfFileSystemSearchResult(Optional ByVal vblnAllowToAddLoggedTimeStamp As Boolean = True) As String

    Dim strMessage As String
    
    strMessage = ""
    
    With mobjFileSysSearchCondition
    
        strMessage = strMessage & "Searching root-directory: " & .ParentDirectoryPath & vbNewLine
        
        strMessage = strMessage & GetDescriptionAboutCombinationOfFileInfoAndDirectoryInfo(.IsFileInfoNeeded, .IsDirectoryInfoNeeded, True) & ": "
    
        strMessage = strMessage & CStr(Me.GetRecordCountOfSearchedDataTable())
        
        If mdblElapsedSecondTime <> 0 Then
        
            strMessage = strMessage & vbNewLine
            
            strMessage = strMessage & "Elapsed time: " & Format(mdblElapsedSecondTime, "0.000") & " [s]"
        End If
        
        If vblnAllowToAddLoggedTimeStamp Then
        
            strMessage = strMessage & vbNewLine & "Searched date-time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
        End If
        
        'strMessage = strMessage & vbNewLine & vbNewLine
    End With
    
    GetMessageOfFileSystemSearchResult = strMessage
End Function

'''
'''
'''
Public Function GetRecordCountOfSearchedDataTable() As Long

    Dim intCount As Long
    
    intCount = 0

    With mobjFileSysSearchCondition
    
        If .SearchedDataTableType = ExistedAsCollectionObjectInMemory Then
        
            If Not mobjCol Is Nothing Then
        
                intCount = mobjCol.Count
            End If
            
        ElseIf .SearchedDataTableType = ExistedAsDictionaryObjectInMemory Then
        
            If Not mobjDic Is Nothing Then
        
                intCount = mobjDic.Count
            End If
        End If
    End With

    GetRecordCountOfSearchedDataTable = intCount
End Function


