Attribute VB_Name = "FileSysListFilesByFSO"
'
'   Using FileSystemObject, search files in the specified directries and get these attribute information
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and Scripting.FileSystemObject
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 18/Jun/2022    Kalmclaeyd Tarclanus    Refactoring
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Compare two files about Date-last-modifed by FSO
'**---------------------------------------------
'''
'''
'''
Public Function IsDifferentBetweenFilesAboutDateLastModifiedByFileSystemObject(ByRef rstrSrcPath As String, ByRef rstrDstPath As String, ByRef robjFS As Scripting.FileSystemObject) As Boolean

    Dim intSecondDiff As Long, intDayDiff As Long, blnDoCopy As Boolean
    Dim objSourceFile As Scripting.File, objDestinationFile As Scripting.File
        
    Const intEpsilonOfSecond As Long = 5    ' When the Window copy any file in special combination of an internal HDD and an external HDD, each file timestamp are always shifted between one and two about each file timestamp.

    blnDoCopy = False

    With robjFS
    
        Set objSourceFile = .GetFile(rstrSrcPath)
        
        Set objDestinationFile = .GetFile(rstrDstPath)
        
        intSecondDiff = DateDiff("s", objDestinationFile.DateLastModified, objSourceFile.DateLastModified)
        
        intDayDiff = DateDiff("d", objDestinationFile.DateLastModified, objSourceFile.DateLastModified)
    
        If Math.Abs(intSecondDiff) <= intEpsilonOfSecond Or intSecondDiff < 0 Or intDayDiff < 0 Then
        
            blnDoCopy = False
        Else
            blnDoCopy = True
        End If
    End With
    
    IsDifferentBetweenFilesAboutDateLastModifiedByFileSystemObject = blnDoCopy
End Function



'**---------------------------------------------
'** Using FileSystemObject
'**---------------------------------------------
'''
''' use FileSystemObject
'''
Public Function GetParentDirectoryPathByFSO(ByRef rstrPath As String, Optional ByVal vobjFSO As Scripting.FileSystemObject = Nothing) As String

    Dim strParentDirectoryPath As String
    
    GetParentDirectoryPathToArgByFSO strParentDirectoryPath, rstrPath, vobjFSO

    GetParentDirectoryPathByFSO = strParentDirectoryPath
End Function

'''
''' use FileSystemObject
'''
Public Sub GetParentDirectoryPathToArgByFSO(ByRef rstrParentDirectoryPath As String, ByRef rstrPath As String, Optional ByVal vobjFSO As Scripting.FileSystemObject = Nothing)

    Dim intPointOfString As Long, objFS As Scripting.FileSystemObject
    
    If vobjFSO Is Nothing Then
    
        Set objFS = New Scripting.FileSystemObject
    Else
        Set objFS = vobjFSO
    End If
    
    With objFS
    
        If .FileExists(rstrPath) Then
        
            rstrParentDirectoryPath = .GetParentFolderName(rstrPath)
        
        ElseIf .FolderExists(rstrPath) Then
        
            rstrParentDirectoryPath = rstrPath
        Else
            intPointOfString = InStrRev(rstrPath, "\")
        
            If intPointOfString > 0 Then
            
                rstrParentDirectoryPath = Left(rstrPath, intPointOfString - 1)
            End If
        End If
    End With
End Sub


'**---------------------------------------------
'** Search file-paths simply
'**---------------------------------------------
'''
'''
'''
Public Function GetFilePathsUsingFSO(ByRef rstrParentDirectoryPath As String, Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*") As Collection

    Dim objFSSearchCondition As FileSysSearchCondition, objSearchedResult As FileSysSearchResult, objDTCol As Collection

    Set objFSSearchCondition = GetFSSearchConditionAsCollectionForSearchingFilesGenerally("FullPath", vstrRawWildCardsByDelimitedChar)

    Set objSearchedResult = GetFileSysSearchedResultCollectionByConditionUsingFSO(rstrParentDirectoryPath, objFSSearchCondition)
    
    If Not objSearchedResult.ResultDataTableCol Is Nothing Then
     
        Set objDTCol = objSearchedResult.ResultDataTableCol
    Else
        Set objDTCol = New Collection
    End If

    Set GetFilePathsUsingFSO = objDTCol
End Function

'**---------------------------------------------
'** List up files path using both FileSysSearchCondition and FileSysSearchResult
'**---------------------------------------------
'''
''' get list as Collection
'''
Public Function GetFileSysSearchedResultCollectionByConditionUsingFSO(ByRef rstrParentDirectoryPath As String, _
        ByVal vobjFileSysSearchCondition As FileSysSearchCondition) As FileSysSearchResult
        
    Dim objCol As Collection, objSearchedResult As FileSysSearchResult

    With New DoubleStopWatch

        .MeasureStart
    
        With vobjFileSysSearchCondition
        
            .SearchedDataTableType = ExistedAsCollectionObjectInMemory
        
            .ParentDirectoryPath = rstrParentDirectoryPath
        End With
        
        ' use Scripting.FileSystemObject
        GetFSOFileDirectoryInfoFromFileSysSearchCondition objCol, Nothing, vobjFileSysSearchCondition
        
        Set objSearchedResult = New FileSysSearchResult
        
        .MeasureInterval
        
        objSearchedResult.SetFileSystemSearchedResultDataTable objCol, vobjFileSysSearchCondition, .ElapsedSecondTime
    End With
    
    Set GetFileSysSearchedResultCollectionByConditionUsingFSO = objSearchedResult
End Function

'''
''' get list as Dictionary
'''
Public Function GetFileSysSearchedResultDictionaryByConditionUsingFSO(ByRef rstrParentDirectoryPath As String, _
        ByVal vobjFileSysSearchCondition As FileSysSearchCondition) As FileSysSearchResult
        
    Dim objDic As Scripting.Dictionary, objSearchedResult As FileSysSearchResult

    With New DoubleStopWatch

        .MeasureStart
    
        With vobjFileSysSearchCondition
        
            .SearchedDataTableType = ExistedAsDictionaryObjectInMemory
        
            .ParentDirectoryPath = rstrParentDirectoryPath
        End With
        
        ' use Scripting.FileSystemObject
        GetFSOFileDirectoryInfoFromFileSysSearchCondition Nothing, objDic, vobjFileSysSearchCondition
        
        Set objSearchedResult = New FileSysSearchResult
        
        .MeasureInterval
        
        objSearchedResult.SetFileSystemSearchedResultDataTable objDic, vobjFileSysSearchCondition, .ElapsedSecondTime
    End With
    
    Set GetFileSysSearchedResultDictionaryByConditionUsingFSO = objSearchedResult
End Function



'''
'''
'''
Public Sub GetFSOFileDirectoryInfoFromFileSysSearchCondition(ByRef robjDataTableCol As Collection, _
        ByRef robjDataTableDic As Scripting.Dictionary, _
        ByVal vobjFileSysSearchCondition As FileSysSearchCondition)
                        
                        
    Dim objNecessaryFileInfoTypeCol As Collection, strFilePath As String, strDirectoryPathWithSuffixBackSlash As String
    Dim blnIsWildCardsRestricted As Boolean, strWildCardArray() As String
    
    Dim blnPutInCollection As Boolean, blnPutInDictionary As Boolean, objFS As Scripting.FileSystemObject, blnIsFileOrDirectoryExceptionListEnabled As Boolean, objFileOrDirectoryExceptionListKeys As Scripting.Dictionary
    
    
    Set objFS = New Scripting.FileSystemObject
    
    With vobjFileSysSearchCondition
    
        GetWildCardsFromExtensionsInfoAndRawWildCards blnIsWildCardsRestricted, strWildCardArray, .ExtensionTextByDelimitedChar, .RawWildCardsByDelimitedChar, .DelimitedChar
    
        Set objNecessaryFileInfoTypeCol = GetFileGeneralInfoColFromLineDelimitedChar(.FileInfomationTypesDelimitedByComma)
        
        strDirectoryPathWithSuffixBackSlash = GetDirectoryPathWithAddingLastBackSlashIfThePathHasNoIt(.ParentDirectoryPath)
        
        GetPutInDataTypeFromInMemoryDataTableType blnPutInCollection, blnPutInDictionary, .SearchedDataTableType
        
        ' file or directory name exception list
        GetFileOrDirectoryNameExceptionListCondition blnIsFileOrDirectoryExceptionListEnabled, objFileOrDirectoryExceptionListKeys, .SearchingExceptionFileOrDirectoryNamesDelimitedByComma
        
        msubGetFSOFileDirectoryInfoWithDetailParamsRecursively robjDataTableCol, robjDataTableDic, strDirectoryPathWithSuffixBackSlash, objNecessaryFileInfoTypeCol, blnIsWildCardsRestricted, strWildCardArray, _
                                 .MinimumHierarchicalOrderToSearch, .MaximumHierarchicalOrderToSearch, .CurrentHierarchicalOrder, .RelativeParentDirectoryPath, blnPutInCollection, blnPutInDictionary, .IsFileInfoNeeded, .IsDirectoryInfoNeeded, objFS
    End With
End Sub

'**---------------------------------------------
'** List up files path from specified directory path
'**---------------------------------------------
'''
''' about files, get data table Collection of RelativeFullPath
'''
Public Function GetRelativeFilePathColByFSO(ByVal vstrParentDirectoryPath As String) As Collection

    Dim objCol As Collection
    
    GetFSOFileDirectoryInfoByExtensionsDelimited objCol, Nothing, vstrParentDirectoryPath, "RelativeFullPath", "*", "*", ",", True, True, False, True, False

    Set GetRelativeFilePathColByFSO = objCol
End Function

'''
''' about directories, get data table Collection of RelativeFullPath
'''
Public Function GetRelativeDirectoryPathColByFSO(ByVal vstrParentDirectoryPath As String) As Collection

    Dim objCol As Collection
    
    GetFSOFileDirectoryInfoByExtensionsDelimited objCol, Nothing, vstrParentDirectoryPath, "RelativeFullPath", "*", "*", ",", True, True, False, False, True

    Set GetRelativeDirectoryPathColByFSO = objCol
End Function


'''
''' about files, get data table Collection of FileBaseName, FileExtension, RelativeDirectoryPath, LastModifiedDate, FullPath
'''
Public Function GetFileAttributeInfoColOfDefaultCombinationByFSO(ByVal vstrParentDirectoryPath As String) As Collection

    Dim objCol As Collection
    
    GetFSOFileDirectoryInfoByExtensionsDelimited objCol, Nothing, vstrParentDirectoryPath, "FileBaseName, FileExtension, RelativeDirectoryPath, LastModifiedDate, FullPath", "*", "*", ",", True, True, False, True, True

    Set GetFileAttributeInfoColOfDefaultCombinationByFSO = objCol
End Function


'''
''' about files, get data table Dictionary, which Key is RelativeFullPath and Value is all 0
'''
Public Function GetRelativeFilePathOnlyKeyDicByFSO(ByVal vstrParentDirectoryPath As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    GetFSOFileDirectoryInfoByExtensionsDelimited Nothing, objDic, vstrParentDirectoryPath, "RelativeFullPath", "*", "*", ",", True, False, True, True, False

    Set GetRelativeFilePathOnlyKeyDicByFSO = objDic
End Function

'''
''' about files, get data table Dictionary, which Key is RelativeFullPath and Value is LastModifiedDate
'''
Public Function GetRelativeFilePathAndLastModifedDateDicByFSO(ByVal vstrParentDirectoryPath As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    GetFSOFileDirectoryInfoByExtensionsDelimited Nothing, objDic, vstrParentDirectoryPath, "RelativeFullPath,LastModifiedDate", "*", "*", ",", True, False, True, True, False

    Set GetRelativeFilePathAndLastModifedDateDicByFSO = objDic
End Function


'''
''' about directories, get data table Dictionary, which Key is RelativeFullPath and Value is LastModifiedDate
'''
Public Function GetRelativeDirectoryPathAndLastModifedDateDicByFSO(ByVal vstrParentDirectoryPath As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    GetFSOFileDirectoryInfoByExtensionsDelimited Nothing, objDic, vstrParentDirectoryPath, "RelativeFullPath,LastModifiedDate", "*", "*", ",", True, False, True, False, True

    Set GetRelativeDirectoryPathAndLastModifedDateDicByFSO = objDic
End Function


'''
''' about files, get data table Dictionary, which Key is RelativeFullPath and values collection are FileBaseName, FileExtension, RelativeDirectoryPath, LastModifiedDate, FullPath
'''
Public Function GetRelativeFilePathAndFileAttributeInfoColOfDefaultCombinationDicByFSO(ByVal vstrParentDirectoryPath As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    GetFSOFileDirectoryInfoByExtensionsDelimited Nothing, objDic, vstrParentDirectoryPath, "RelativeFullPath, FileBaseName, FileExtension, RelativeDirectoryPath, LastModifiedDate, FullPath", "*", "*", ",", True, False, True, True, False

    Set GetRelativeFilePathAndFileAttributeInfoColOfDefaultCombinationDicByFSO = objDic
End Function

'**---------------------------------------------
'** Core operation to list up files path from specified directory path
'**---------------------------------------------
'''
''' This is slowest than either the VBA.Dir method or the FindFirstFileEx API method
'''
Public Sub GetFSOFileDirectoryInfoByExtensionsDelimited(ByRef robjDataTableCol As Collection, ByRef robjDataTableDic As Scripting.Dictionary, _
                        ByVal vstrDirectoryPath As String, Optional ByVal vstrFileInfomationTypesDelimitedByComma As String = "RelativeFullPath", _
                        Optional ByVal vstrExtensionTextByDelimitedChar As String = "*", Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", Optional ByVal vstrDelimitedChar As String = ",", _
                        Optional ByVal vblnAllowToSearchSubDirectories As Boolean = False, _
                        Optional ByVal vblnPutInCollection As Boolean = True, Optional ByVal vblnPutInDictionary As Boolean = False, _
                        Optional ByVal vblnIsFileInfoNeeded As Boolean = False, Optional ByVal vblnIsDirectoryInfoNeeded As Boolean = False, _
                        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "")    ' A unit-testcomment for testing intarnal meta syntax function
                        
                 
    Dim objNecessaryFileInfoTypeCol As Collection, strFilePath As String, objFS As Scripting.FileSystemObject
    Dim blnIsWildCardsRestricted As Boolean, strWildCardArray() As String, strDirectoryPathWithSuffixBackSlash As String, blnIsFileOrDirectoryExceptionListEnabled As Boolean, objFileOrDirectoryExceptionListKeys As Scripting.Dictionary
    
    GetWildCardsFromExtensionsInfoAndRawWildCards blnIsWildCardsRestricted, strWildCardArray, vstrExtensionTextByDelimitedChar, vstrRawWildCardsByDelimitedChar, vstrDelimitedChar

    Set objNecessaryFileInfoTypeCol = GetFileGeneralInfoColFromLineDelimitedChar(vstrFileInfomationTypesDelimitedByComma)

    Set objFS = New Scripting.FileSystemObject
    
    strDirectoryPathWithSuffixBackSlash = GetDirectoryPathWithAddingLastBackSlashIfThePathHasNoIt(vstrDirectoryPath)

    ' file or directory name exception list
    GetFileOrDirectoryNameExceptionListCondition blnIsFileOrDirectoryExceptionListEnabled, objFileOrDirectoryExceptionListKeys, vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma

    msubGetFSOFileDirectoryInfoWithAllSubDirSearch robjDataTableCol, robjDataTableDic, strDirectoryPathWithSuffixBackSlash, objNecessaryFileInfoTypeCol, _
                        blnIsWildCardsRestricted, strWildCardArray, _
                        vblnAllowToSearchSubDirectories, "", vblnPutInCollection, vblnPutInDictionary, vblnIsFileInfoNeeded, vblnIsDirectoryInfoNeeded, objFS, blnIsFileOrDirectoryExceptionListEnabled, objFileOrDirectoryExceptionListKeys
End Sub


'''
'''
'''
Private Sub msubGetFSOFileDirectoryInfoWithAllSubDirSearch(ByRef robjDataTableCol As Collection, ByRef robjDataTableDic As Scripting.Dictionary, _
                        ByVal vstrDirectoryPath As String, ByVal vobjNecessaryFileInfoTypeCol As Collection, _
                        ByVal vblnIsWildCardsRestricted As Boolean, ByRef rstrMergedWildCardArray() As String, _
                        Optional ByVal vblnAllowToSearchSubDirectories As Boolean = False, _
                        Optional ByVal vstrRelativeParentDirectoryPath As String = "", _
                        Optional ByVal vblnPutInCollection As Boolean = True, Optional ByVal vblnPutInDictionary As Boolean = False, _
                        Optional ByVal vblnIsFileInfoNeeded As Boolean = True, Optional ByVal vblnIsDirectoryInfoNeeded As Boolean = False, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing, _
                        Optional ByVal vblnIsFileOrDirectoryExceptionListEnabled As Boolean = False, Optional ByVal vobjFileOrDirectoryExceptionListKeys As Scripting.Dictionary = Nothing)


    If vblnAllowToSearchSubDirectories Then
    
        msubGetFSOFileDirectoryInfoWithDetailParamsRecursively robjDataTableCol, robjDataTableDic, vstrDirectoryPath, vobjNecessaryFileInfoTypeCol, _
                            vblnIsWildCardsRestricted, rstrMergedWildCardArray, _
                            -1, -1, 0, _
                            vstrRelativeParentDirectoryPath, vblnPutInCollection, vblnPutInDictionary, vblnIsFileInfoNeeded, vblnIsDirectoryInfoNeeded, vobjFS, _
                            vblnIsFileOrDirectoryExceptionListEnabled, vobjFileOrDirectoryExceptionListKeys
    Else
    
        msubGetFSOFileDirectoryInfoWithDetailParamsRecursively robjDataTableCol, robjDataTableDic, vstrDirectoryPath, vobjNecessaryFileInfoTypeCol, _
                            vblnIsWildCardsRestricted, rstrMergedWildCardArray, _
                            0, 0, 0, _
                            vstrRelativeParentDirectoryPath, vblnPutInCollection, vblnPutInDictionary, vblnIsFileInfoNeeded, vblnIsDirectoryInfoNeeded, vobjFS, _
                            vblnIsFileOrDirectoryExceptionListEnabled, vobjFileOrDirectoryExceptionListKeys
    
    End If


End Sub

'**---------------------------------------------
'** A base procedure for searching files or directories using FileSystemObject (FSO)
'**---------------------------------------------
'''
''' use FileSystemObject, which is slowest in other VBA.Dir function or Windows API FineNextFile
'''
''' In order to get result speedily, this doesn't include user-defined classes, this uses only primitive data type except the Scripting.Dictinary.
''' Because this is almost called recursively.
'''
Private Sub msubGetFSOFileDirectoryInfoWithDetailParamsRecursively(ByRef robjDataTableCol As Collection, ByRef robjDataTableDic As Scripting.Dictionary, _
                        ByVal vstrDirectoryPath As String, ByVal vobjNecessaryFileInfoTypeCol As Collection, _
                        ByVal vblnIsWildCardsRestricted As Boolean, ByRef rstrMergedWildCardArray() As String, _
                        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = 0, _
                        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = 0, _
                        Optional ByVal vintCurrentHierarchicalOrder As Long = 0, _
                        Optional ByVal vstrRelativeParentDirectoryPath As String = "", _
                        Optional ByVal vblnPutInCollection As Boolean = True, _
                        Optional ByVal vblnPutInDictionary As Boolean = False, _
                        Optional ByVal vblnIsFileInfoNeeded As Boolean = True, _
                        Optional ByVal vblnIsDirectoryInfoNeeded As Boolean = False, _
                        Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing, _
                        Optional ByVal vblnIsFileOrDirectoryExceptionListEnabled As Boolean = False, Optional ByVal vobjFileOrDirectoryExceptionListKeys As Scripting.Dictionary = Nothing)


    Dim objDir As Scripting.Folder, objFile As Scripting.File, objSubDir As Scripting.Folder, strPath As String, objDrive As Scripting.Drive
    Dim varKey As Variant, varValueCollectionOrNotObjectData As Variant
    Dim i As Long, blnPutThisFileOrDirectoryInfo As Boolean, blnIsDirectoryExpansionEnabled As Boolean, blnIsFilesExpansionEnabled As Boolean, intCount As Long
    Dim blnContinueByFileOrDirectoryException As Boolean
    
    With New Scripting.FileSystemObject

        strPath = Left(vstrDirectoryPath, Len(vstrDirectoryPath) - 1)

        If IsFileSysDriveRootPath(strPath) Then
        
            ' When the vstrDirectoryPath is the system-drive root path, the .GetFolder returns the current user My Document folder, which is a Scripting.FileSystemObject specification
        
            Set objDrive = .GetDrive(strPath)
            
            Set objDir = objDrive.RootFolder
        Else
            Set objDir = .GetFolder(strPath)
        End If

        If vblnIsFileInfoNeeded And (vintMinimumHierarchicalOrderToSearch = -1 Or vintMinimumHierarchicalOrderToSearch <= vintCurrentHierarchicalOrder) Then
            
            blnIsFilesExpansionEnabled = True
            
            On Error Resume Next
            
            intCount = objDir.SubFolders.Count
            
            If Err.Number <> 0 Then
            
                Debug.Print "Error to expand a directory: " & objDir.Path
            
                blnIsFilesExpansionEnabled = False
            End If
            
            If blnIsFilesExpansionEnabled Then
            
                For Each objFile In objDir.Files
                
                    blnContinueByFileOrDirectoryException = True
                
                    If vblnIsFileOrDirectoryExceptionListEnabled Then
                    
                        If vobjFileOrDirectoryExceptionListKeys.Exists(objFile.Name) Then
                        
                            blnContinueByFileOrDirectoryException = False
                        End If
                    End If
                
                    If blnContinueByFileOrDirectoryException Then
                
                        blnPutThisFileOrDirectoryInfo = False
                        
                        If vblnIsWildCardsRestricted Then
                            
                            For i = LBound(rstrMergedWildCardArray) To UBound(rstrMergedWildCardArray)
                                
                                ' ignore case
                                
                                If LCase(objFile.Name) Like LCase(rstrMergedWildCardArray(i)) Then
                                
                                    blnPutThisFileOrDirectoryInfo = True
                                    
                                    Exit For
                                End If
                            Next
                        Else
                            blnPutThisFileOrDirectoryInfo = True
                        End If
                        
                        If blnPutThisFileOrDirectoryInfo Then
        
                            ' Data-table collection
                            If vblnPutInCollection Then
                                
                                If robjDataTableCol Is Nothing Then Set robjDataTableCol = New Collection
                            
                                robjDataTableCol.Add mfvarGetRowColOfFileGeneralInfoByFSO(objFile.Name, True, vstrDirectoryPath, vstrRelativeParentDirectoryPath, vobjNecessaryFileInfoTypeCol, objFile, Nothing, vobjFS)
                            End If
                            
                            ' Data-table Scripting.Dictionary
                            If vblnPutInDictionary Then
                                
                                If robjDataTableDic Is Nothing Then Set robjDataTableDic = New Scripting.Dictionary
                                
                                msubGetKeyAndValueOfFileGeneralInfoByFSO varKey, varValueCollectionOrNotObjectData, objFile.Name, True, vstrDirectoryPath, vstrRelativeParentDirectoryPath, vobjNecessaryFileInfoTypeCol, objFile, Nothing, vobjFS
                            
                                With robjDataTableDic
                                
                                    If Not .Exists(varKey) Then
                                
                                        .Add varKey, varValueCollectionOrNotObjectData
                                    End If
                                End With
                            End If
                        End If
                    End If
                Next
            End If
        End If
    
        blnIsDirectoryExpansionEnabled = True
    
        On Error Resume Next
        
        intCount = objDir.SubFolders.Count
        
        If Err.Number <> 0 Then
        
            Debug.Print "Error to expand a directory: " & objDir.Path
        
            blnIsDirectoryExpansionEnabled = False
        End If
        
        On Error GoTo 0
    
        If blnIsDirectoryExpansionEnabled Then
        
            For Each objSubDir In objDir.SubFolders
            
                blnContinueByFileOrDirectoryException = True
                
                If vblnIsFileOrDirectoryExceptionListEnabled Then
                    
                    If vobjFileOrDirectoryExceptionListKeys.Exists(objSubDir.Name) Then
                    
                        blnContinueByFileOrDirectoryException = False
                    End If
                End If
            
                If blnContinueByFileOrDirectoryException Then
            
                    If vblnIsDirectoryInfoNeeded And (vintMinimumHierarchicalOrderToSearch = -1 Or vintMinimumHierarchicalOrderToSearch <= vintCurrentHierarchicalOrder) Then
                    
                        'robjFilePaths.Add objSubDir.Path
                        
                        blnPutThisFileOrDirectoryInfo = False
                    
                        If vblnIsWildCardsRestricted Then
                            
                            For i = LBound(rstrMergedWildCardArray) To UBound(rstrMergedWildCardArray)
                                
                                ' ignore case
                                
                                If LCase(objSubDir.Name) Like LCase(rstrMergedWildCardArray(i)) Then
                                
                                    blnPutThisFileOrDirectoryInfo = True
                                    
                                    Exit For
                                End If
                            Next
                        Else
                            blnPutThisFileOrDirectoryInfo = True
                        End If
                        
                        If blnPutThisFileOrDirectoryInfo Then
                        
                            ' Data-table collection
                            If vblnPutInCollection Then
                                
                                If robjDataTableCol Is Nothing Then Set robjDataTableCol = New Collection
                            
                                robjDataTableCol.Add mfvarGetRowColOfFileGeneralInfoByFSO(objSubDir.Name, False, vstrDirectoryPath, vstrRelativeParentDirectoryPath, vobjNecessaryFileInfoTypeCol, Nothing, objSubDir, vobjFS)
                            End If
                            
                            ' Data-table Scripting.Dictionary
                            If vblnPutInDictionary Then
                                
                                If robjDataTableDic Is Nothing Then Set robjDataTableDic = New Scripting.Dictionary
                                
                                msubGetKeyAndValueOfFileGeneralInfoByFSO varKey, varValueCollectionOrNotObjectData, objSubDir.Name, True, vstrDirectoryPath, vstrRelativeParentDirectoryPath, vobjNecessaryFileInfoTypeCol, Nothing, objSubDir, vobjFS
                                
                                With robjDataTableDic
                                
                                    If Not .Exists(varKey) Then
                                
                                        .Add varKey, varValueCollectionOrNotObjectData
                                    End If
                                End With
                            End If
                        End If
                    End If
                
                    If vintMaximumHierarchicalOrderToSearch = -1 Or vintMaximumHierarchicalOrderToSearch > vintCurrentHierarchicalOrder Then
                    
                        ' recursive call
                        msubGetFSOFileDirectoryInfoWithDetailParamsRecursively robjDataTableCol, robjDataTableDic, vstrDirectoryPath & objSubDir.Name & "\", vobjNecessaryFileInfoTypeCol, _
                                        vblnIsWildCardsRestricted, rstrMergedWildCardArray, vintMinimumHierarchicalOrderToSearch, vintMaximumHierarchicalOrderToSearch, vintCurrentHierarchicalOrder + 1, vstrRelativeParentDirectoryPath & objSubDir.Name & "\", vblnPutInCollection, vblnPutInDictionary, vblnIsFileInfoNeeded, vblnIsDirectoryInfoNeeded, vobjFS, vblnIsFileOrDirectoryExceptionListEnabled, vobjFileOrDirectoryExceptionListKeys
                    End If
                End If
            Next
        End If
    End With
End Sub


'''
''' Using FileSystemObject
'''
''' In order to convert the following code into VBScript codes, the designer has to investigate ablishing using Collection type
'''
Public Function GetSubFilePathsByExtensionsDelimited(ByVal vstrDirectoryPath As String, _
        Optional ByVal vstrExtensionTextByDelimitedChar As String = "*", _
        Optional ByVal vstrDelimitedChar As String = ",", _
        Optional ByVal vblnAllowToSearchSubDirectories As Boolean = False) As Collection
    
    
    Dim objFilePaths As Collection, blnAllowToFilterByExtensions As Boolean, strSearchExtensions() As String
    
    
    GetFilteringFileExtensionsInfo blnAllowToFilterByExtensions, _
            strSearchExtensions, _
            vstrExtensionTextByDelimitedChar, _
            vstrDelimitedChar
    
    Set objFilePaths = New Collection

    msubGetFSOFilePathsByExtensionsDelimitedRecursively objFilePaths, _
            vstrDirectoryPath, _
            blnAllowToFilterByExtensions, _
            strSearchExtensions, _
            vblnAllowToSearchSubDirectories
    
    
    Set GetSubFilePathsByExtensionsDelimited = objFilePaths
End Function


'''
''' use FileSystemObject, which is slowest in other Dir function or Windows API FineNextFile
'''
Private Sub msubGetFSOFilePathsByExtensionsDelimitedRecursively(ByRef robjFilePaths As Collection, _
        ByVal vstrDirectoryPath As String, _
        ByVal vblnAllowToFilterByExtensions As Boolean, _
        ByRef rstrSearchExtensions() As String, _
        Optional ByVal vblnAllowToSearchSubDirectories As Boolean = False)


    Dim objDir As Scripting.Folder, objFile As Scripting.File, objSubDir As Scripting.Folder
    
    Dim strExtension As String, i As Long
    
    
    With New Scripting.FileSystemObject
    
        Set objDir = .GetFolder(vstrDirectoryPath)
    
        For Each objFile In objDir.Files
        
            If vblnAllowToFilterByExtensions Then
            
                strExtension = LCase(.GetExtensionName(objFile.Name))
                
                For i = LBound(rstrSearchExtensions) To UBound(rstrSearchExtensions)
                
                    If StrComp(strExtension, rstrSearchExtensions(i)) = 0 Then
                    
                        robjFilePaths.Add objFile.Path
                    End If
                Next
            Else
                robjFilePaths.Add objFile.Path
            End If
        Next
        
        If vblnAllowToSearchSubDirectories Then
        
            For Each objSubDir In objDir.SubFolders
            
                msubGetFSOFilePathsByExtensionsDelimitedRecursively robjFilePaths, _
                        objSubDir.Path, _
                        vblnAllowToFilterByExtensions, _
                        rstrSearchExtensions, _
                        vblnAllowToSearchSubDirectories
            Next
        End If
    End With
End Sub


'**---------------------------------------------
'** Get general file information
'**---------------------------------------------
'''
''' use FileSystemObject
'''
''' Improvement point: Delaying point is that this expand the sigle directory-path list into data-table after the file-path list is gotten
'''
Public Function GetFilesInformationDataTableFromDirectoryPathAndExtensionsDelimitedByFSO(ByVal vstrDirectoryPath As String, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection, _
        Optional ByVal vstrExtensions As String = "*", _
        Optional ByVal vstrDelimitedChar As String = ",") As Collection
    
    Dim objFilePaths As Collection

    Set objFilePaths = GetSubFilePathsByExtensionsDelimited(vstrDirectoryPath, vstrExtensions, vstrDelimitedChar)

    Set GetFilesInformationDataTableFromDirectoryPathAndExtensionsDelimitedByFSO = GetFilesInformationDataTable(objFilePaths, vobjNecessaryFileInfoTypeCol, vstrDirectoryPath)
End Function


'''
'''
'''
''' <Argument>vobjFilePaths: Input</Argument>
''' <Argument>vobjNecessaryFileInfoTypeCol: Input</Argument>
''' <Argument>vstrSearchingRootDirectoryPath: Input</Argument>
''' <Return></Return>
Public Function GetFilesInformationDataTable(ByVal vobjFilePaths As Collection, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection, _
        ByVal vstrSearchingRootDirectoryPath As String) As Collection
    
    Dim objDataTableCol As Collection, varPath As Variant, strFilePath As String
    
    Dim objFS As Scripting.FileSystemObject
    
    
    Set objFS = New Scripting.FileSystemObject

    Set objDataTableCol = New Collection

    For Each varPath In vobjFilePaths
        
        strFilePath = varPath
        
        objDataTableCol.Add GetRowColOfFileGeneralInfo(strFilePath, vobjNecessaryFileInfoTypeCol, objFS, vstrSearchingRootDirectoryPath)
    Next

    Set GetFilesInformationDataTable = objDataTableCol
End Function


'''
''' Using FileSystemObject
'''
''' Improvement point: Delaying point is that this expand the sigle file-path list into data-table after the file-path list is gotten
'''
Public Function GetRowColOfFileGeneralInfo(ByVal vstrFileOrDirectoryPath As String, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection, _
        ByVal vobjFS As Scripting.FileSystemObject, _
        ByVal vstrSearchingRootDirectoryPath As String) As Collection
    
    Dim objRowCol As Collection, varFileGeneralInfo As Variant, enmFileGeneralInfo As FileGeneralInfo, objFile As Scripting.File
    
    
    Set objRowCol = New Collection
    
    ' If the file-path is too long, the GetFile() method may be failed...
    
    Set objFile = vobjFS.GetFile(vstrFileOrDirectoryPath)
    
    For Each varFileGeneralInfo In vobjNecessaryFileInfoTypeCol
    
        enmFileGeneralInfo = varFileGeneralInfo
        
        objRowCol.Add GetFileGeneralInfoValue(vstrFileOrDirectoryPath, enmFileGeneralInfo, objFile, vobjFS, vstrSearchingRootDirectoryPath)
    Next
    
    Set GetRowColOfFileGeneralInfo = objRowCol
End Function

'''
''' old type
'''
''' using FileSystemObject
'''
Public Function GetFileGeneralInfoValue(ByVal vstrFileOrDirectoryPath As String, _
        ByVal venmFileGeneralInfo As FileGeneralInfo, _
        ByVal vobjFile As Scripting.File, _
        ByVal vobjFS As Scripting.FileSystemObject, _
        ByVal vstrSearchingRootDirectoryPath As String) As Variant
    
    
    Dim varValue As Variant, objShortcut As IWshRuntimeLibrary.WshShortcut
    
    Select Case venmFileGeneralInfo
    
        Case FileGeneralInfo.giFullPath
        
            varValue = vstrFileOrDirectoryPath
        
        Case FileGeneralInfo.giFileName ' file name
            
            varValue = vobjFS.GetFileName(vstrFileOrDirectoryPath)
        
        Case FileGeneralInfo.giFileBaseName ' file base name without extension
        
            varValue = vobjFS.GetBaseName(vstrFileOrDirectoryPath)
        
        Case FileGeneralInfo.giFileExtension
        
            varValue = vobjFS.GetExtensionName(vstrFileOrDirectoryPath)
        
        Case FileGeneralInfo.giFileSize ' Size unit is kilo-Byte
        
            varValue = CDbl(vobjFile.Size) / 1024#  ' kB
    
        Case FileGeneralInfo.giParentDirectoryPath
        
            varValue = vobjFS.GetParentFolderName(vstrFileOrDirectoryPath)

        Case FileGeneralInfo.giRelativeDirectoryPath
        
            varValue = Replace(vobjFS.GetParentFolderName(vstrFileOrDirectoryPath), vstrSearchingRootDirectoryPath, "")

            If varValue = "" Then
            
                varValue = ".\"
            Else
                varValue = "." & varValue
            End If

        Case FileGeneralInfo.giLastModifiedDate
        
            With vobjFS
            
                If .FileExists(vstrFileOrDirectoryPath) Then
                
                    varValue = .GetFile(vstrFileOrDirectoryPath).DateLastModified
                    
                ElseIf .FolderExists(vstrFileOrDirectoryPath) Then
                
                    varValue = .GetFolder(vstrFileOrDirectoryPath).DateLastModified
                End If
            End With
        
        Case FileGeneralInfo.giCreatedDate
        
            With vobjFS
            
                If .FileExists(vstrFileOrDirectoryPath) Then
                
                    varValue = .GetFile(vstrFileOrDirectoryPath).DateCreated
                    
                ElseIf .FolderExists(vstrFileOrDirectoryPath) Then
                
                    varValue = .GetFolder(vstrFileOrDirectoryPath).DateCreated
                End If
            End With
            
        Case giReadOnly, giHidden, giSystem, giVolume, giDirectory, giArchive, giAlias, giCompressed
        
            With vobjFS
            
                If .FileExists(vstrFileOrDirectoryPath) Then
                
                    varValue = mfblnGetAttributeEnabledForFSO(.GetFile(vstrFileOrDirectoryPath).Attributes, venmFileGeneralInfo)
                    
                ElseIf .FolderExists(vstrFileOrDirectoryPath) Then
                
                    varValue = mfblnGetAttributeEnabledForFSO(.GetFolder(vstrFileOrDirectoryPath).DateCreated, venmFileGeneralInfo)
                End If
            End With
        
        Case FileGeneralInfo.giLinkDestination
        
            With New IWshRuntimeLibrary.WshShell
        
                Set objShortcut = .CreateShortcut(vstrFileOrDirectoryPath)
                
                varValue = objShortcut.TargetPath
            End With
        
        Case FileGeneralInfo.giLinkDestFileName
        
            With New IWshRuntimeLibrary.WshShell
        
                Set objShortcut = .CreateShortcut(vstrFileOrDirectoryPath)
                
                varValue = vobjFS.GetFileName(objShortcut.TargetPath)
            End With
    End Select

    GetFileGeneralInfoValue = varValue
End Function

'''
'''
'''
Private Function mfblnGetAttributeEnabledForFSO(ByVal venmAttribute As Scripting.FileAttribute, _
        ByVal venmFileGeneralInfo As FileGeneralInfo) As Boolean
    
    Dim blnEnabled As Boolean
    
    blnEnabled = False
    
    Select Case venmFileGeneralInfo
    
        Case giReadOnly, giHidden, giSystem, giVolume, giDirectory, giArchive
        
            If (venmAttribute And venmFileGeneralInfo) > 0 Then
            
                blnEnabled = True
            End If
        
        Case giAlias
        
            If (venmAttribute And Alias) > 0 Then
        
                blnEnabled = True
            End If
            
        Case giCompressed
        
            If (venmAttribute And Compressed) > 0 Then
        
                blnEnabled = True
            End If
    End Select

    mfblnGetAttributeEnabledForFSO = blnEnabled
End Function



'**---------------------------------------------
'** get individual element information of each row of data-table
'**---------------------------------------------
'''
'''
'''
Private Function mfvarGetRowColOfFileGeneralInfoByFSO(ByVal vstrFileOrDirectoryName As String, ByVal vblnIsFile As Boolean, ByVal vstrParentDirectoryFullPathWithSuffixBackSlash As String, ByVal vstrRelativeDirectoryPathWithSuffixBackSlash As String, ByVal vobjNecessaryFileInfoTypeCol As Collection, Optional ByVal vobjFile As Scripting.File = Nothing, Optional ByVal vobjDirectory As Scripting.Folder = Nothing, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing, Optional ByVal vintIncludingColumnStartIndex As Long = 1) As Variant
    Dim objRowCol As Collection, i As Long
    Dim varFileGeneralInfo As Variant, enmFileGeneralInfo As FileGeneralInfo
    
        
    Set objRowCol = New Collection
    
    ' If the file-path is too long, the GetFile() method may be failed...
    
'    Set objFile = vobjFS.GetFile(vstrFileOrDirectoryPath)
    
    If vobjNecessaryFileInfoTypeCol.Count = 1 Then
    
        enmFileGeneralInfo = vobjNecessaryFileInfoTypeCol.Item(1)
    
        Let mfvarGetRowColOfFileGeneralInfoByFSO = mfvarGetFileOrDirectoryGeneralInfoValueByFSO(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFile, vobjDirectory, vobjFS)
    Else
        i = 1
        
        For Each varFileGeneralInfo In vobjNecessaryFileInfoTypeCol
        
            If i >= vintIncludingColumnStartIndex Then
            
                enmFileGeneralInfo = varFileGeneralInfo
                
                objRowCol.Add mfvarGetFileOrDirectoryGeneralInfoValueByFSO(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFile, vobjDirectory, vobjFS)
            End If
            
            i = i + 1
        Next
        
        Set mfvarGetRowColOfFileGeneralInfoByFSO = objRowCol
    End If
End Function

'''
''' new type
'''
''' using FileSystemObject
'''
Private Function mfvarGetFileOrDirectoryGeneralInfoValueByFSO(ByVal vstrFileOrDirectoryName As String, _
        ByVal vblnIsFile As Boolean, _
        ByVal vstrParentDirectoryFullPathWithSuffixBackSlash As String, _
        ByVal vstrRelativeDirectoryPathWithSuffixBackSlash As String, _
        ByVal venmFileGeneralInfo As FileGeneralInfo, _
        Optional ByVal vobjFile As Scripting.File = Nothing, _
        Optional ByVal vobjDirectory As Scripting.Folder = Nothing, _
        Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing) As Variant
    
    
    Dim varValue As Variant, objShortcut As IWshRuntimeLibrary.WshShortcut
    
    Select Case venmFileGeneralInfo
    
        Case FileGeneralInfo.giRelativeFullPath
        
            varValue = vstrRelativeDirectoryPathWithSuffixBackSlash & vstrFileOrDirectoryName
    
        Case FileGeneralInfo.giFullPath
        
            varValue = vstrParentDirectoryFullPathWithSuffixBackSlash & vstrFileOrDirectoryName
        
        Case FileGeneralInfo.giFileName
            
            varValue = vstrFileOrDirectoryName
        
        Case FileGeneralInfo.giFileBaseName ' file base name without extension
        
            If vblnIsFile Then
            
                varValue = vobjFS.GetBaseName(vstrFileOrDirectoryName)
            Else
                varValue = vstrFileOrDirectoryName
            End If
            
        Case FileGeneralInfo.giFileExtension    ' file extension
        
            If vblnIsFile Then
            
                varValue = vobjFS.GetExtensionName(vstrFileOrDirectoryName)
            Else
                varValue = ""
            End If
        
        Case FileGeneralInfo.giFileSize ' Size unit is a kilo-Byte
        
            'varValue = CDbl(vobjFile.Size) / 1024#  ' kB
            If vblnIsFile Then
            
                varValue = CDbl(vobjFile.Size) / &H400  ' kB
            Else
                varValue = CDbl(vobjDirectory.Size) / &H400 ' kB
            End If
    
        Case FileGeneralInfo.giParentDirectoryPath
        
            If InStrRev(vstrParentDirectoryFullPathWithSuffixBackSlash, "\") = Len(vstrParentDirectoryFullPathWithSuffixBackSlash) Then
                
                varValue = Left(vstrParentDirectoryFullPathWithSuffixBackSlash, Len(vstrParentDirectoryFullPathWithSuffixBackSlash) - 1)
            Else
                varValue = vstrParentDirectoryFullPathWithSuffixBackSlash
            End If

        Case FileGeneralInfo.giRelativeDirectoryPath
        
            If vstrRelativeDirectoryPathWithSuffixBackSlash <> "" Then
            
                If InStrRev(vstrRelativeDirectoryPathWithSuffixBackSlash, "\") = Len(vstrRelativeDirectoryPathWithSuffixBackSlash) Then
                
                    varValue = Left(vstrRelativeDirectoryPathWithSuffixBackSlash, Len(vstrRelativeDirectoryPathWithSuffixBackSlash) - 1)
                Else
                    varValue = vstrRelativeDirectoryPathWithSuffixBackSlash
                End If
                
                varValue = ".\" & varValue
            Else
                varValue = ".\"
            End If

        Case FileGeneralInfo.giLastModifiedDate
        
            'varValue = vobjFile.DateLastModified
            varValue = VBA.FileDateTime(vstrParentDirectoryFullPathWithSuffixBackSlash & vstrFileOrDirectoryName)
        
        Case FileGeneralInfo.giCreatedDate
        
            If vblnIsFile Then
            
                varValue = vobjFile.DateCreated
            Else
                varValue = vobjDirectory.DateCreated
            End If
        
        Case giReadOnly, giHidden, giSystem, giVolume, giDirectory, giArchive, giAlias, giCompressed
        
            If vblnIsFile Then
            
                varValue = mfblnGetAttributeEnabledForFSO(vobjFile.Attributes, venmFileGeneralInfo)
            Else
                varValue = mfblnGetAttributeEnabledForFSO(vobjDirectory.DateCreated, venmFileGeneralInfo)
            End If
    
        Case FileGeneralInfo.giLinkDestination
        
            If vblnIsFile Then
                
                With New IWshRuntimeLibrary.WshShell
            
                    On Error Resume Next
            
                    Set objShortcut = .CreateShortcut(vstrRelativeDirectoryPathWithSuffixBackSlash & vstrFileOrDirectoryName)
                    
                    On Error GoTo 0
                    
                    If Not objShortcut Is Nothing Then
                    
                        varValue = objShortcut.TargetPath
                    Else
                        varValue = ""
                    End If
                End With
            Else
                varValue = ""
            End If
            
        Case FileGeneralInfo.giLinkDestFileName
            
            If vblnIsFile Then
            
                With New IWshRuntimeLibrary.WshShell
            
                    On Error Resume Next
            
                    Set objShortcut = .CreateShortcut(vstrRelativeDirectoryPathWithSuffixBackSlash & vstrFileOrDirectoryName)
                    
                    On Error GoTo 0
                    
                    If Not objShortcut Is Nothing Then
                    
                        varValue = vobjFS.GetFileName(objShortcut.TargetPath)
                    Else
                        varValue = ""
                    End If
                End With
            Else
                varValue = ""
            End If
    End Select

    mfvarGetFileOrDirectoryGeneralInfoValueByFSO = varValue
End Function


'''
'''
'''
Private Sub msubGetKeyAndValueOfFileGeneralInfoByFSO(ByRef rvarKey As Variant, ByRef rvarValueCollectionOrNotObjectTypeData As Variant, _
                            ByVal vstrFileOrDirectoryName As String, ByVal vblnIsFile As Boolean, _
                            ByVal vstrParentDirectoryFullPathWithSuffixBackSlash As String, ByVal vstrRelativeDirectoryPathWithSuffixBackSlash As String, _
                            ByVal vobjNecessaryFileInfoTypeCol As Collection, _
                            Optional ByVal vobjFile As Scripting.File = Nothing, Optional ByVal vobjDirectory As Scripting.Folder = Nothing, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing)
    
    
    Dim objRowCol As Collection, varFileGeneralInfo As Variant, enmFileGeneralInfo As FileGeneralInfo
        
        
    Set objRowCol = New Collection
    
    If vobjNecessaryFileInfoTypeCol.Count = 1 Then
        
        enmFileGeneralInfo = vobjNecessaryFileInfoTypeCol.Item(1)
    
        Let rvarKey = mfvarGetFileOrDirectoryGeneralInfoValueByFSO(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFile, vobjDirectory, vobjFS)
        
        Let rvarValueCollectionOrNotObjectTypeData = 0
    
    ElseIf vobjNecessaryFileInfoTypeCol.Count = 2 Then
    
        enmFileGeneralInfo = vobjNecessaryFileInfoTypeCol.Item(1)
    
        Let rvarKey = mfvarGetFileOrDirectoryGeneralInfoValueByFSO(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFile, vobjDirectory, vobjFS)
        
        enmFileGeneralInfo = vobjNecessaryFileInfoTypeCol.Item(2)
    
        Let rvarValueCollectionOrNotObjectTypeData = mfvarGetFileOrDirectoryGeneralInfoValueByFSO(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFile, vobjDirectory, vobjFS)
    Else
        enmFileGeneralInfo = vobjNecessaryFileInfoTypeCol.Item(1)
    
        Let rvarKey = mfvarGetFileOrDirectoryGeneralInfoValueByFSO(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFile, vobjDirectory, vobjFS)
        
        Set rvarValueCollectionOrNotObjectTypeData = mfvarGetRowColOfFileGeneralInfoByFSO(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, vobjNecessaryFileInfoTypeCol, vobjFile, vobjDirectory, vobjFS, 2)
    End If
End Sub




