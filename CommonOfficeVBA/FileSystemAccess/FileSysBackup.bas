Attribute VB_Name = "FileSysBackup"
'
'   Back up files, which include the office file of this VB project
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 15/Apr/2019    Mr. Atsushi Oomura      A part of idea has been disclosed at https://gihyo.jp/book/2016/978-4-7741-8177-6/support (514.xlsm)
'       Mon, 13/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Private Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then

    ' Copies, moves, renames, or deletes a file system object. This function has been replaced in Windows Vista by IFileOperation.
    
    Private Declare PtrSafe Function SHFileOperation Lib "shell32.dll" Alias "SHFileOperationA" (lpFileOp As SHFILEOPSTRUCT) As LongPtr
    
    Private Type SHFILEOPSTRUCT
    
        hwnd As LongPtr                 ' A window handle to the dialog box to display information about the status of the file operation.
        wFunc As Long                   ' A value that indicates which operation to perform
        pFrom As String                 ' A pointer to one or more source file names. These names should be fully qualified paths to prevent unexpected results.
        pTo As String                   ' A pointer to the destination file or directory name. This parameter must be set to NULL if it is not used. Wildcard characters are not allowed. Their use will lead to unpredictable results.
        fFlags As Integer               ' Flags that control the file operation. This member can take a combination of the following flags.
        fAnyOperationsAborted As Long   ' When the function returns, this member contains TRUE if any file operations were aborted before they were completed; otherwise, FALSE. An operation can be manually aborted by the user through UI or it can be silently aborted by the system if the FOF_NOERRORUI or FOF_NOCONFIRMATION flags were set.
        hNameMappings As LongPtr        ' When the function returns, this member contains a handle to a name mapping object that contains the old and new names of the renamed files. This member is used only if the fFlags member includes the FOF_WANTMAPPINGHANDLE flag. See Remarks for more details.
        lpszProgressTitle As String     ' A pointer to the title of a progress dialog box. This is a null-terminated string. This member is used only if fFlags includes the FOF_SIMPLEPROGRESS flag.
    End Type
#Else
    Private Declare Function SHFileOperation Lib "shell32.dll" Alias "SHFileOperationA" (lpFileOp As SHFILEOPSTRUCT) As Long
    
    Private Type SHFILEOPSTRUCT
    
        hwnd As Long
        wFunc As Long
        pFrom As String
        pTo As String
        fFlags As Integer
        fAnyOperationsAborted As Long
        hNameMappings As Long
        lpszProgressTitle As String
    End Type
#End If

'**---------------------------------------------
'** WinAPI private constants
'**---------------------------------------------
' enumerations to specify the type of operation
Private Const FO_MOVE = &H1&         ' Move the files specified in pFrom to the location specified in pTo.
Private Const FO_COPY = &H2&         ' Copy the files specified in the pFrom member to the location specified in the pTo member.
Private Const FO_DELETE = &H3&       ' Delete the files specified in pFrom.
Private Const FO_RENAME = &H4&       ' Rename the file specified in pFrom. You cannot use this flag to rename multiple files with a single function call. Use FO_MOVE instead

' flags to specify the options
Private Const FOF_MULTIDESTFILES = &H1&      ' The pTo member specifies multiple destination files (one for each source file in pFrom) rather than one directory where all source files are to be deposited.
Private Const FOF_CONFIRMMOUSE = &H2&        ' Not used.
Private Const FOF_SILENT = &H4&              ' Do not display a progress dialog box.
Private Const FOF_RENAMEONCOLLISION = &H8&   ' Give the file being operated on a new name in a move, copy, or rename operation if a file with the target name already exists at the destination.
Private Const FOF_NOCONFIRMATION = &H10&     ' Respond with Yes to All for any dialog box that is displayed.
Private Const FOF_WANTMAPPINGHANDLE = &H20&  ' If FOF_RENAMEONCOLLISION is specified and any files were renamed, assign a name mapping object that contains their old and new names to the hNameMappings member. This object must be freed using SHFreeNameMappings when it is no longer needed.
Private Const FOF_ALLOWUNDO = &H40&          ' Preserve undo information, if possible. Prior to Windows Vista, operations could be undone only from the same process that performed the original operation.
Private Const FOF_FILESONLY = &H80&          ' Perform the operation only on files (not on folders) if a wildcard file name (.) is specified.
Private Const FOF_SIMPLEPROGRESS = &H100&    ' Display a progress dialog box but do not show individual file names as they are operated on.
Private Const FOF_NOCONFIRMMKDIR = &H200&    ' Do not ask the user to confirm the creation of a new directory if the operation requires one to be created.
Private Const FOF_NOERRORUI = &H400&         ' Do not display a dialog to the user if an error occurs.


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
Public Sub DeleteFileAndSendItToTrashBox(ByVal vstrPath As String, Optional ByVal vblnNoConfirmationToDelete As Boolean = True, Optional ByVal vblnNoProgressBarWindowWhenDelete As Boolean = False)

    msubDeleteEitherFileOrDirectoryAndSendTheseToTrashBox vstrPath, vblnNoConfirmationToDelete, vblnNoProgressBarWindowWhenDelete
End Sub

'''
''' This code is prior to readability
'''
Public Sub DeleteDirectoryAndSendItToTrashBox(ByVal vstrPath As String, Optional ByVal vblnNoConfirmationToDelete As Boolean = True, Optional ByVal vblnNoProgressBarWindowWhenDelete As Boolean = False)

    msubDeleteEitherFileOrDirectoryAndSendTheseToTrashBox vstrPath, vblnNoConfirmationToDelete, vblnNoProgressBarWindowWhenDelete
End Sub

'**---------------------------------------------
'** Back up office files
'**---------------------------------------------
'''
''' save as auto backup within changing Application.DisplayAlert
'''
Public Sub SaveAsAutoBackupWithoutDisplayAlert(ByVal vobjOfficeFile As Object, Optional ByVal vstrSuffix As String = "")
    
    Dim strChildDirName As String
    Dim varDisplayAlerts As Variant
    Dim objFS As Scripting.FileSystemObject
    Dim strPath As String, strDir As String, objOfficeApplication As Object
    
    
    Set objOfficeApplication = vobjOfficeFile.Application
    
    Set objFS = New Scripting.FileSystemObject
    
    varDisplayAlerts = objOfficeApplication.DisplayAlerts
        
    On Error GoTo ErrHandler:
    
    Select Case GetOfficeApplicationLowerName(objOfficeApplication)
    
        Case "excel"
            
            strChildDirName = "BackupExcelVBA"
            
            objOfficeApplication.DisplayAlerts = False
            
        Case "word"
        
            strChildDirName = "BackupWordVBA"
            
            objOfficeApplication.DisplayAlerts = 0
            
        Case "powerpoint"
        
            strChildDirName = "BackupPowerPointVBA"
            
            objOfficeApplication.DisplayAlerts = 0
    End Select
    
    
    With objFS
    
        strDir = .GetParentFolderName(vobjOfficeFile.FullName) & "\" & strChildDirName
        
        If Not .FolderExists(strDir) Then
            
            ForceToCreateDirectory strDir
        End If
    
        If vstrSuffix = "" Then
        
            strPath = strDir & "\" & .GetBaseName(vobjOfficeFile.FullName) & "_AutoBak_" & WeekdayEnglishShort3Chars(Now()) & "_" & Format(Now(), "yyyymmdd_hhnnss") & "." & .GetExtensionName(vobjOfficeFile.FullName)
        Else
            strPath = strDir & "\" & .GetBaseName(vobjOfficeFile.FullName) & vstrSuffix & "." & .GetExtensionName(vobjOfficeFile.FullName)
        End If
    
        .CopyFile vobjOfficeFile.FullName, strPath
    End With
     
    'vobjOfficeFile.SaveAs strPath
    
ErrHandler:
    objOfficeApplication.DisplayAlerts = varDisplayAlerts
End Sub

'''
''' easy backup of ThisWorkbook with date info suffix
'''
Public Sub SaveThisCurrentOfficeFileAsAutoBackupWithSuffixToSpecifiedDirectoryWithoutDisplayAlert()
    
    Dim varDisplayAlerts As Variant, objFS As Scripting.FileSystemObject, strPath As String, strDir As String
    
    Dim objOfficeApplication As Object, objCurrentOfficeFile As Object
    
    
    Set objCurrentOfficeFile = GetCurrentOfficeFileObject()
    
    Set objOfficeApplication = objCurrentOfficeFile.Application
    
    Set objFS = New Scripting.FileSystemObject
    
    varDisplayAlerts = objOfficeApplication.DisplayAlerts
    
    On Error GoTo ErrHandler:
    
    Select Case GetOfficeApplicationLowerName(objOfficeApplication)
    
        Case "excel"
            
            objOfficeApplication.DisplayAlerts = False
            
        Case "word", "powerpoint"
        
            objOfficeApplication.DisplayAlerts = 0
    End Select
    
    With objFS
        
        strDir = GetMainVBProjectMacroOfficeFileBackUpDirectoryPath() & "\bak" & Format(Now, "yyyymmdd")
        
        ForceToCreateDirectory strDir
        
        strPath = strDir & "\" & .GetBaseName(objCurrentOfficeFile.FullName) & "_" & Format(Now(), "yyyymmdd") & "." & .GetExtensionName(objCurrentOfficeFile.FullName)
    
        .CopyFile objCurrentOfficeFile.FullName, strPath
    End With
     
ErrHandler:
    objOfficeApplication.DisplayAlerts = varDisplayAlerts
End Sub

'''
'''
'''
Public Function GetMainVBProjectMacroOfficeFileBackUpDirectoryPath() As String

    Dim strDir As String

    strDir = GetDevelopmentVBARootDir() & "\OldBackUp"

    GetMainVBProjectMacroOfficeFileBackUpDirectoryPath = strDir
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubDeleteEitherFileOrDirectoryAndSendTheseToTrashBox(ByVal vstrPath As String, Optional ByVal vblnNoConfirmationToDelete As Boolean = True, Optional ByVal vblnNoProgressBarWindowWhenDelete As Boolean = False)

    With New Scripting.FileSystemObject
    
        If .FileExists(vstrPath) Then
        
            msubSendTrashBox vstrPath, vblnNoConfirmationToDelete, vblnNoProgressBarWindowWhenDelete
        
        ElseIf .FolderExists(vstrPath) Then
        
            msubSendTrashBox vstrPath, vblnNoConfirmationToDelete, vblnNoProgressBarWindowWhenDelete
        End If
    End With
End Sub

'''
'''
'''
Private Sub msubSendTrashBox(ByVal vstrPath As String, _
        Optional ByVal vblnNoConfirmationToDelete As Boolean = True, _
        Optional ByVal vblnNoProgressBarWindowWhenDelete As Boolean = False)

    Dim udtSHFILEOPSTRUCT As SHFILEOPSTRUCT
    
    With udtSHFILEOPSTRUCT
        
        .hwnd = GetThisApplicationHWnd()    ' for only Excel, Application.hwnd
        
        .wFunc = FO_DELETE                            ' Delete the files specified in pFrom.
        
        .pFrom = vstrPath
        
        .fFlags = FOF_ALLOWUNDO    ' Preserve undo information
         
        If vblnNoProgressBarWindowWhenDelete Then
        
            .fFlags = .fFlags Or FOF_SILENT
        End If
        If vblnNoConfirmationToDelete Then
        
            .fFlags = .fFlags Or FOF_NOCONFIRMATION
        End If
    End With
    
    SHFileOperation udtSHFILEOPSTRUCT
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Sanity test for DeleteFileAndSendItToTrashBox
'**---------------------------------------------
Private Sub msubSanityTestOfDeleteFileAndSendTrashBox()

    Dim strDir As String, strFilePath As String
    
    strDir = GetDevelopmentVBARootDir() & "\TemporaryDirectoryForSendingTrashBox"
    
    strFilePath = strDir & "\TestFileToSendingTrashBoxBySHFileOperation.txt"

    ForceToCreateDirectory strDir
    
    msubCreateTextFileForTest strFilePath

    ' Test to move a file to Windows trash box

    msubDeleteEitherFileOrDirectoryAndSendTheseToTrashBox strFilePath
    
    msubDeleteEitherFileOrDirectoryAndSendTheseToTrashBox strDir
End Sub
'''
'''
'''
Private Sub msubSanityTestToDeleteSample()
    
    Dim udtSHFILEOPSTRUCT As SHFILEOPSTRUCT
    
    With udtSHFILEOPSTRUCT
    
        .hwnd = GetThisApplicationHWnd()    ' for only Excel, Application.hwnd
        
        .wFunc = FO_DELETE                            ' Delete the files specified in pFrom.
        
        .pFrom = "C:\TestFolder"            ' Specify the deliting directory
        
        .fFlags = FOF_ALLOWUNDO             ' Preserve undo information
    End With
    
    SHFileOperation udtSHFILEOPSTRUCT
End Sub


Private Sub msubCreateTextFileForTest(ByVal vstrPath As String)

    With New Scripting.FileSystemObject
    
        With .CreateTextFile(vstrPath, True)
        
            .WriteLine "Created at " & Format(Now(), "yyyy/m/d hh:mm:ss") & vbNewLine
            
            .WriteLine "Test to move trash box"
        
            .Close
        End With
    End With
End Sub

