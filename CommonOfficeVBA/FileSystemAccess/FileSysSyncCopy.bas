Attribute VB_Name = "FileSysSyncCopy"
'
'   Copy files only which are changed and synchronizing files-structure
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       Dependent on IWshRuntimeLibrary.WshShell for managing short-cut file
'       Dependent on MSComctlLib.ProgressBar
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun,  1/Jan/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrModuleName As String = "FileSysSyncCopy"


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjSuffixDateRegExp As VBScript_RegExp_55.RegExp


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
Public Enum FileSyncCopySituation

    CopyFileNewToFileSystemSync
    
    UpdateFileToFileSystemSync
End Enum

'**---------------------------------------------
'** Key-Value cache preparation for FileSysSyncCopy
'**---------------------------------------------
Private mobjStrKeyValueFileSysSyncCopyDic As Scripting.Dictionary

Private mblnIsStrKeyValueFileSysSyncCopyDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** General operation for avoiding using FileSystemObject
'**---------------------------------------------
'''
'''
'''
Public Function GetFileNameFromRelativeFilePath(ByVal vstrRelativeFullPath As String) As String

    Dim intP1 As Long, strFileName As String

    intP1 = InStrRev(vstrRelativeFullPath, "\")
            
    If intP1 > 0 Then
    
        strFileName = Mid(vstrRelativeFullPath, intP1 + 1, Len(vstrRelativeFullPath) - intP1)
        
        'Debug.Print strFileName
    Else
        strFileName = vstrRelativeFullPath
    End If

    GetFileNameFromRelativeFilePath = strFileName
End Function



'**---------------------------------------------
'** By file extension restriction
'**---------------------------------------------
'''
''' use wild-cards as *.pdf,*.lnk
'''
Private Function GetPDFAndShortcutsListsCol(ByVal vstrParentDirectoryPath As String, Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "RelativeFullPath") As Collection

    Set GetPDFAndShortcutsListsCol = GetAnyTypeFileListsColByVbaDir(vstrParentDirectoryPath, vstrStringOfFileInfomationTypesDelimitedByComma, "*.pdf,*.lnk")
End Function

'''
''' use wild-cards as only .lnk
'''
Private Function GetShortcutFilesListsCol(ByVal vstrParentDirectoryPath As String, Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "RelativeFullPath") As Collection

    Set GetShortcutFilesListsCol = GetAnyTypeFileListsColByVbaDir(vstrParentDirectoryPath, vstrStringOfFileInfomationTypesDelimitedByComma, "*.lnk")
End Function


Public Function GetPDFAndDocumentsListsCol(ByVal vstrParentDirectoryPath As String, Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "RelativeFullPath") As Collection

    Set GetPDFAndDocumentsListsCol = GetAnyTypeFileListsColByVbaDir(vstrParentDirectoryPath, vstrStringOfFileInfomationTypesDelimitedByComma, "*.pdf,*.lnk,*.txt,*.doc,*.docx")
End Function


'''
'''
'''
Public Function GetShortcutFilePathToShortcutTargetPathDic(ByVal vstrParentDirectoryPath As String, _
        Optional ByVal vblnAllowToSearchSubDirectories As Boolean = False) As Scripting.Dictionary


    Dim objLinkTargetPathToFullPathsDic As Scripting.Dictionary, objFSSearchCondition As FileSysSearchCondition, objFSSearchResult As FileSysSearchResult

    Set objFSSearchCondition = GetFSSearchConditionAsCollectionForSearchingFilesGenerally("FullPath,LinkDestination", "*.lnk")

    ' using VBA.Dir
    
    Set objFSSearchResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(vstrParentDirectoryPath, objFSSearchCondition)

    Set objLinkTargetPathToFullPathsDic = GetInverseDictionaryFrom2ColumnDataTableCollection(objFSSearchResult.ResultDataTableCol)

    Set GetShortcutFilePathToShortcutTargetPathDic = objLinkTargetPathToFullPathsDic
End Function

'''
''' no search any sub-directory
'''
Public Function GetShrtcutFilePathToShortcutTargetFileNameDic(ByVal vstrParentDirectoryPath As String) As Scripting.Dictionary

    Dim objDataTableCol As Collection, objLinkTargetPathToFullPathsDic As Scripting.Dictionary
    Dim objFSSearchCondition As FileSysSearchCondition, objFSSearchResult As FileSysSearchResult

    Set objFSSearchCondition = GetFSSearchConditionAsCollectionForSearchingFilesGenerally("FullPath,LinkDestFileName", "*.lnk")

    With objFSSearchCondition
    
        .AllowToSearchSubDirectories = False
    End With

    ' using VBA.Dir
    
    Set objFSSearchResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(vstrParentDirectoryPath, objFSSearchCondition)

    Set objLinkTargetPathToFullPathsDic = GetInverseDictionaryFrom2ColumnDataTableCollection(objFSSearchResult.ResultDataTableCol)

    Set GetShrtcutFilePathToShortcutTargetFileNameDic = objLinkTargetPathToFullPathsDic
End Function



'**---------------------------------------------
'** Synchronize files among directories
'**---------------------------------------------
'''
''' delete copy destination files which same file names of these exist and the relative file path are different.
'''
Public Sub DeleteFliesAboutSameModifiedDateAndFileNameAndDifferentRelativeDirPath(ByVal vstrParentDirectoryPath As String, _
        ByVal vstrSourcePathPart As String, _
        ByVal vstrDestinationPathPart As String, _
        Optional ByVal vblnAllowToDelete As Boolean = False, _
        Optional ByVal vblnReplacingSourceInfoOnlyAboutShortcut As Boolean = False, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*")


    Dim strSourceDirectoryPath As String, strDestinationDirectoryPath As String
    Dim objSourceFilePathInfoDic As Scripting.Dictionary, objDestinationFilePathInfoDic As Scripting.Dictionary
   
   
    strSourceDirectoryPath = vstrParentDirectoryPath
    
    strDestinationDirectoryPath = Replace(vstrParentDirectoryPath, vstrSourcePathPart, vstrDestinationPathPart)

    ' use VBA.Dir
    Set objSourceFilePathInfoDic = GetAnyTypeFileInfoDicByVbaDir(strSourceDirectoryPath, _
            "RelativeFullPath,LastModifiedDate,FullPath", _
            vstrRawWildCardsByDelimitedChar)
    
    ' use VBA.Dir
    Set objDestinationFilePathInfoDic = GetAnyTypeFileInfoDicByVbaDir(strDestinationDirectoryPath, _
            "RelativeFullPath,LastModifiedDate,FullPath", _
            vstrRawWildCardsByDelimitedChar)
    
    
    DeleteFliesAboutSameModifiedDateAndFileNameAndDifferentRelativeDirPathFromDictionaries vstrParentDirectoryPath, _
            objSourceFilePathInfoDic, _
            objDestinationFilePathInfoDic, _
            vstrSourcePathPart, _
            vstrDestinationPathPart, _
            vblnAllowToDelete, _
            vblnReplacingSourceInfoOnlyAboutShortcut, _
            vstrRawWildCardsByDelimitedChar
End Sub

'''
'''
'''
''' <Argument>vstrParentDirectoryPath: Input</Argument>
''' <Argument>vstrSourcePathPart: Input</Argument>
''' <Argument>vstrDestinationPathPart: Input</Argument>
''' <Argument>vstrRawWildCardsByDelimitedChar: Input</Argument>
''' <Argument>vblnShowWarningMessageBoxWhenFileSystemDriveTroubles: Input</Argument>
Public Sub UpdateCopyFilesWithoutDuplicated(ByVal vstrParentDirectoryPath As String, _
        ByVal vstrSourcePathPart As String, _
        ByVal vstrDestinationPathPart As String, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vblnShowWarningMessageBoxWhenFileSystemDriveTroubles As Boolean = True)

    Dim objDataTableCol As Collection, objLogDTCol As Collection

    If AreBothDrivesReady(vstrSourcePathPart, vstrDestinationPathPart, vblnShowWarningMessageBoxWhenFileSystemDriveTroubles) Then

        ' deleting files which are changed about relative-path.
        DeleteFliesAboutSameModifiedDateAndFileNameAndDifferentRelativeDirPath vstrParentDirectoryPath, _
                vstrSourcePathPart, _
                vstrDestinationPathPart, _
                False, _
                True, _
                vstrRawWildCardsByDelimitedChar
        
        ' use VBA.Dir
        Set objDataTableCol = GetFilePathsUsingVbaDir(vstrParentDirectoryPath, vstrRawWildCardsByDelimitedChar)
        
        ' use MSComctlLib.ProgressBar
        Set objLogDTCol = CopyFilesWithKeepingDirectoryStructure(objDataTableCol, vstrSourcePathPart, vstrDestinationPathPart)
        
        If Not objLogDTCol Is Nothing Then
        
            If objLogDTCol.Count > 0 Then
        
                On Error Resume Next
        
                Application.Run "OutputLogOfOverwritingFilesToSheet", _
                        objLogDTCol, _
                        vstrSourcePathPart, _
                        vstrDestinationPathPart
            
                On Error GoTo 0
            End If
        End If
    End If
End Sub

'''
'''
'''
Public Sub SychronizeFilesBetweenTwoDirectories(ByVal vstrSourceDirectoryPath As String, _
        ByVal vstrDestinationDirectoryPath As String, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*")

    If AreBothDrivesReady(vstrSourceDirectoryPath, vstrDestinationDirectoryPath, False) Then
    
        ' Forward direction
        UpdateCopyFilesWithoutDuplicated vstrSourceDirectoryPath, _
                vstrSourceDirectoryPath, _
                vstrDestinationDirectoryPath, _
                vstrRawWildCardsByDelimitedChar
    
        ' Reverse direction
        UpdateCopyFilesWithoutDuplicated vstrDestinationDirectoryPath, _
                vstrDestinationDirectoryPath, _
                vstrSourceDirectoryPath, _
                vstrRawWildCardsByDelimitedChar
    End If
End Sub

'''
'''
'''
Public Sub SychronizeFilesInPluralDirectories(ByVal vobjDirectoryPaths As Collection, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*")

    Dim i As Long, varPath As Variant, strSrcPath As String, strDstPath As String
    Dim blnContinue As Boolean

    If vobjDirectoryPaths.Count >= 2 Then

        blnContinue = True: i = 1
        
        For Each varPath In vobjDirectoryPaths
        
            If i = 1 Then
                
                strSrcPath = varPath
                
                If Not IsDriveReady(strSrcPath) Then
                
                    blnContinue = False
                End If
            Else
                strDstPath = varPath
            End If
        
            If Not blnContinue Then
            
                Exit For
            End If
            
            If i > 1 Then
            
                If IsDriveReady(strDstPath, False) Then
                
                    SychronizeFilesBetweenTwoDirectories strSrcPath, strDstPath, vstrRawWildCardsByDelimitedChar
                End If
            End If
        
            i = i + 1
        Next
    End If
End Sub

'''
''' independent tool
'''
Public Sub RelpaceShortcutsLinks(ByVal vstrSearchingDirectoryPath As String, _
        ByVal vstrFromPathPart As String, _
        ByVal vstrToPathPart As String)


    Dim objDataTableCol As Collection, objShortcut As IWshRuntimeLibrary.WshShortcut, varPath As Variant, strPath As String


    Set objDataTableCol = GetShortcutFilesListsCol(vstrSearchingDirectoryPath, "FullPath")

    With New IWshRuntimeLibrary.WshShell

        For Each varPath In objDataTableCol

            strPath = varPath

            On Error Resume Next
            
            Set objShortcut = .CreateShortcut(strPath)

            On Error GoTo 0


            If Not objShortcut Is Nothing Then
            
                With objShortcut
            
                    If InStr(1, .TargetPath, vstrFromPathPart) > 0 Then
                    
                        'Debug.Print objShortcut.TargetPath
                        
                        .TargetPath = Replace(.TargetPath, vstrFromPathPart, vstrToPathPart)
    
                        .Save
                    End If
                End With
            End If
        Next
    End With
End Sub

'**---------------------------------------------
'** Copy for synchronizing
'**---------------------------------------------
'''
'''
'''
''' <Argument>robjDoCopySrcToDstDic: Output</Argument>
''' <Argument>robjDoCreateShortcutDstToTargetPathDic: Output</Argument>
''' <Argument>vobjFilePaths: Input</Argument>
''' <Argument>vstrSourceParentDirectoryPathPart: Input</Argument>
''' <Argument>vstrDestinationParentDirectoryPathPart: Input</Argument>
''' <Return>Collection</Return>
Public Function GetListsOfCopyFilesWithKeepingDirectoryStructure(ByRef robjDoCopySrcToDstDic As Scripting.Dictionary, _
        ByRef robjDoCreateShortcutDstToTargetPathDic As Scripting.Dictionary, _
        ByVal vobjFilePaths As Collection, _
        ByVal vstrSourceParentDirectoryPathPart As String, _
        ByVal vstrDestinationParentDirectoryPathPart As String) As Collection


    Dim objFS As Scripting.FileSystemObject, objShell As IWshRuntimeLibrary.WshShell, objShortcut As IWshRuntimeLibrary.WshShortcut
    
    Dim varPath As Variant, strPath As String, strDestinationDir As String, strDestinationPath As String
    
    Dim strSourcePath As String, blnDoCopy As Boolean
    
    Dim objLogRowCol As Collection, objLogDTCol As Collection
    
    Dim objSourceTargetFileLastModifiedDateAndFileNameDic As Scripting.Dictionary
    
    Dim objDestinationTargetFileLastModifiedDateAndFileNameDic As Scripting.Dictionary, objLinkTargetPathToFullPathsDic As Scripting.Dictionary
    
    Dim strKey As String, strFileName As String, strDestinationSideTargetPath As String, strDestinationShortcutFilePath As String
    
    Dim enmFileCopySituation As FileSyncCopySituation, varCompoundKey As Variant
    
    Dim objDoCopySrcToDstDic As Scripting.Dictionary, objDoCreateShortcutDstToTargetPathDic As Scripting.Dictionary



    Set objFS = New Scripting.FileSystemObject
    
    Set objShell = New IWshRuntimeLibrary.WshShell

    Set objSourceTargetFileLastModifiedDateAndFileNameDic = New Scripting.Dictionary
    
    Set objLogDTCol = New Collection
    
    For Each varPath In vobjFilePaths
    
        strPath = varPath
    
        If InStr(1, strPath, vstrSourceParentDirectoryPathPart) > 0 Then
    
            If StrComp(LCase(objFS.GetExtensionName(strPath)), "lnk") = 0 Then
                
                strSourcePath = mfstrGetTargetPathFromShortcutFilePath(strPath, objSourceTargetFileLastModifiedDateAndFileNameDic, objFS, objShell)
            Else
                strSourcePath = strPath
            End If
    
            If strSourcePath <> "" Then
            
                With objFS
                
                    strDestinationPath = .GetParentFolderName(Replace(strPath, vstrSourceParentDirectoryPathPart, vstrDestinationParentDirectoryPathPart)) & "\" & .GetFileName(strSourcePath)
            
                    strDestinationDir = .GetParentFolderName(strDestinationPath)
                
                
                    Set objLogRowCol = Nothing
                    
                    blnDoCopy = mfblnIsNeedToCopy(enmFileCopySituation, objLogRowCol, strSourcePath, strDestinationPath, objFS)
                    
                    If Not objLogRowCol Is Nothing Then
                    
                        objLogDTCol.Add objLogRowCol
                    End If
                End With
        
                If blnDoCopy Then
                    
                    ForceToCreateDirectory strDestinationDir, objFS
                    
                    ' When there has been more then one shortcut file which indicates the same file, and the shortcut has been forbidden creating, then the file will have to be copied to more than one destination directory path.
                    ' In the destination directory, after second file-copy is replaced to the short-cut file
                    
                    With objFS.GetFile(strSourcePath)
                    
                        strKey = Format(.DateLastModified, "yyyymmddhhmmss") & "," & .Name
                    
                        strFileName = .Name
                    
                        With objSourceTargetFileLastModifiedDateAndFileNameDic
                        
                            If Not .Exists(strKey) Then
    
                                ' Do copy
                                
                                If objDoCopySrcToDstDic Is Nothing Then Set objDoCopySrcToDstDic = New Scripting.Dictionary
                                
                                varCompoundKey = strSourcePath & ";" & CStr(enmFileCopySituation)
                                
                                objDoCopySrcToDstDic.Add varCompoundKey, strDestinationDir & "\"
                            Else
                                ' When it is the first item, do copy. Otherwise, create new shortcut
                                
                                If objDestinationTargetFileLastModifiedDateAndFileNameDic Is Nothing Then Set objDestinationTargetFileLastModifiedDateAndFileNameDic = New Scripting.Dictionary
                                
                                ' Create shortcut, if it hasn't already exist.
                                With objDestinationTargetFileLastModifiedDateAndFileNameDic
                                
                                    If Not .Exists(strKey) Then
                                    
                                        ' Do copy
                                        If objDoCopySrcToDstDic Is Nothing Then Set objDoCopySrcToDstDic = New Scripting.Dictionary
                                
                                        varCompoundKey = strSourcePath & ";" & CStr(enmFileCopySituation)
                                
                                        objDoCopySrcToDstDic.Add varCompoundKey, strDestinationDir & "\"
                                        
                                        .Add strKey, strDestinationDir & "\" & strFileName
                                    Else
                                        strDestinationSideTargetPath = .Item(strKey)
                                    
                                        ' Get shortcut file information in the destination-directory
                                        Set objLinkTargetPathToFullPathsDic = GetShortcutFilePathToShortcutTargetPathDic(strDestinationDir)
                                        
                                        With objLinkTargetPathToFullPathsDic
                                        
                                            If Not .Exists(strDestinationSideTargetPath) Then
                                        
                                                ' Need to create shortcut
                                                strDestinationShortcutFilePath = strDestinationDir & "\" & FindNewNameWhenAlreadyExist(objFS.GetBaseName(strFileName) & ".lnk", objFS)
                                                
                                                If objDoCreateShortcutDstToTargetPathDic Is Nothing Then Set objDoCreateShortcutDstToTargetPathDic = New Scripting.Dictionary
                                                
                                                objDoCreateShortcutDstToTargetPathDic.Add strDestinationShortcutFilePath, strDestinationSideTargetPath
                                            End If
                                        End With
                                    End If
                                End With
                            End If
                        End With
                    End With
                End If
            End If
        End If
    Next


    Set robjDoCopySrcToDstDic = objDoCopySrcToDstDic
    
    Set robjDoCreateShortcutDstToTargetPathDic = objDoCreateShortcutDstToTargetPathDic

    Set GetListsOfCopyFilesWithKeepingDirectoryStructure = objLogDTCol
End Function


'**---------------------------------------------
'** Deleting unrelated files between the source directory and the destination directory
'**---------------------------------------------
'''
''' delete copy destination files which same file names of these exist and the relative file path are different.
'''
Public Sub DeleteFliesAboutSameModifiedDateAndFileNameAndDifferentRelativeDirPathFromDictionaries(ByRef rstrParentDirectoryPath As String, _
        ByRef robjSourceFilePathInfoDic As Scripting.Dictionary, _
        ByRef robjDestinationFilePathInfoDic As Scripting.Dictionary, _
        ByVal vstrSourcePathPart As String, _
        ByVal vstrDestinationPathPart As String, _
        Optional ByVal vblnAllowToDelete As Boolean = False, _
        Optional ByVal vblnReplacingSourceInfoOnlyAboutShortcut As Boolean = False, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*")


    Dim strSourceDirectoryPath As String, strDestinationDirectoryPath As String
    
    Dim objSourceLastModifiedDateAndFileNameKeyDic As Scripting.Dictionary

    Dim varRelativeFullPath As Variant, strRelativeFullPath As String, strDestinationFileName As String, objSourceRowCol As Collection, objDestinationRowCol As Collection
    Dim varDestinationRowCol As Variant
    Dim stdSourceLastModifiedDate As Date, stdDestinationLastModifiedDate As Date
    Dim strPathToDelete As String
    
    Dim blnContinueToDeletingProcess As Boolean, objFS As Scripting.FileSystemObject
    Dim objDestDeletingCandidates As Collection, objDeletePathInfos As Collection, objRowCol As Collection

    Dim objCandidatesDirectoriesToDeleteDic As Scripting.Dictionary, strDirPath As String, strDestinationFullPath As String
    Dim strDestinationLinkedTargetFileName As String
    Dim objDeletePaths As Collection, varPath As Variant, strKey As String

    Dim strCurrentSourceCorrespondingDirPathFromDstShortCutFile As String, strDestinationLinkedTargetLastModifiedDateAndFileNameKey As String
    Dim strCurrentSourceCorrespondingDirPathFromDstShortCutTargetFilePath As String


    strSourceDirectoryPath = rstrParentDirectoryPath
    
    strDestinationDirectoryPath = Replace(rstrParentDirectoryPath, vstrSourcePathPart, vstrDestinationPathPart)

    
    If vblnReplacingSourceInfoOnlyAboutShortcut Then
    
        Set robjSourceFilePathInfoDic = GetDicOfSourceFilePathInfoWithReplacingOnlyShortcutFile(robjSourceFilePathInfoDic)
    End If
    
    Set objSourceLastModifiedDateAndFileNameKeyDic = mfobjGetKeysOfLastModifiedDateAndFileNameDic(robjSourceFilePathInfoDic)

    
    
    Set objFS = New Scripting.FileSystemObject
    
    If Not robjDestinationFilePathInfoDic Is Nothing Then
    
        With robjDestinationFilePathInfoDic
        
            For Each varRelativeFullPath In .Keys
            
                strRelativeFullPath = varRelativeFullPath
            
                ' No deletion when the same relative directory path and same name of the file exists
                
                If Not robjSourceFilePathInfoDic.Exists(strRelativeFullPath) Then
            
                    Set objDestinationRowCol = .Item(varRelativeFullPath)
                
                    strDestinationFileName = GetFileNameFromRelativeFilePath(strRelativeFullPath)
                    
                    strDestinationFullPath = objDestinationRowCol.Item(2)
                    
                    'udtLastModifiedDate = objRowCol.Item(1)
                    
                    ' Deletion process
                    
                    If StrComp(LCase(objFS.GetExtensionName(strDestinationFileName)), "lnk") = 0 Then
                        
                        ' About shortcut file
                        
                        ' In the destination side shortcut file case,
                        '
                        ' If the destination short-cut file exists and corresponding source side directory doesn't exist, delete the destination short-cut file.
                        
                        
                        strDirPath = objFS.GetParentFolderName(strDestinationFullPath)
                    
                        strCurrentSourceCorrespondingDirPathFromDstShortCutFile = Replace(strDirPath, vstrDestinationPathPart, vstrSourcePathPart)
                    
                        If Not objFS.FolderExists(strCurrentSourceCorrespondingDirPathFromDstShortCutFile) Then
                        
                            ' Do delete the shortcut
                            msubPrepareToDeleteFile strDestinationFullPath, objDeletePaths, objDeletePathInfos, objCandidatesDirectoriesToDeleteDic, vblnAllowToDelete, objFS
                        Else
                        
                            With New IWshRuntimeLibrary.WshShell
                        
                                With .CreateShortcut(strDestinationFullPath)
                            
                                    strDestinationLinkedTargetLastModifiedDateAndFileNameKey = Format(objFS.GetFile(.TargetPath).DateLastModified, "yyyymmddhhmmss") & "," & objFS.GetFileName(.TargetPath)
                                    
                                    strDestinationLinkedTargetFileName = objFS.GetFileName(.TargetPath)
                                    
                                    If objFS.FileExists(.TargetPath) Then
                                    
                                        strCurrentSourceCorrespondingDirPathFromDstShortCutTargetFilePath = objFS.GetParentFolderName(Replace(.TargetPath, vstrDestinationPathPart, vstrSourcePathPart))
                                    Else
                                        If objFS.FolderExists(.TargetPath) Then
                                        
                                            strCurrentSourceCorrespondingDirPathFromDstShortCutTargetFilePath = Replace(.TargetPath, vstrDestinationPathPart, vstrSourcePathPart)
                                        Else
                                        
                                            strCurrentSourceCorrespondingDirPathFromDstShortCutTargetFilePath = ""
                                        
                                            ' Shortcut.TargetPath doesn't exists
                                        End If
                                    End If
                                End With
                            End With
                        
                            ' confirm the relative path and last-modified date between source side path and destination linked target file
                            '
                            ' And, if the source side file, which is corresponding the destination side target file of the destination short cut file, doesn't exist, delete also the destination short-cut file.
                            
                            With GetShrtcutFilePathToShortcutTargetFileNameDic(strCurrentSourceCorrespondingDirPathFromDstShortCutTargetFilePath)
                        
                                If Not .Exists(strDestinationLinkedTargetFileName) Then
                                
                                    ' Do delete the shortcut
                                
                                    msubPrepareToDeleteFile strDestinationFullPath, objDeletePaths, objDeletePathInfos, objCandidatesDirectoriesToDeleteDic, vblnAllowToDelete, objFS
                                Else
                                    
                                    ' If the destination-target file is old than the source side file, the short-cut file should be deleted
                                End If
                            End With
                        End If
                    Else
                        strKey = mfstrGetKeyFromModifiedDateAndFullPathRowCol(objDestinationRowCol)
                    
                        ' Except for '.lnk' files, such as pdf, doc, docx, txt, .....
                        
                        If objSourceLastModifiedDateAndFileNameKeyDic.Exists(strKey) Then
                        
                            ' Confirm the file existence of source file of short-cut target file name
                            ' A method: when the destination file of short cut target exists and the source file exists, then doesn't delete (or not)
                            
                            ' Do delete
                            
                            msubPrepareToDeleteFile strDestinationFullPath, objDeletePaths, objDeletePathInfos, objCandidatesDirectoriesToDeleteDic, vblnAllowToDelete, objFS
                        End If
                    End If
                End If
            Next
        End With
    End If

    If vblnAllowToDelete Then
    
        For Each varPath In objDeletePaths
        
            strDestinationFullPath = varPath
            
            DeleteFileAndSendItToTrashBox strDestinationFullPath
        Next
    
        DeleteDirecotoriesWhenThereAreNoBothFilesAndSubDirectories objCandidatesDirectoriesToDeleteDic, objFS
    Else
        On Error Resume Next
    
        Application.Run "OutputDestinationDeletingFilesInfoBecauseOfChangingReletivePathToTemporaryBook", objDeletePathInfos
        
        On Error GoTo 0
    End If
End Sub


'**---------------------------------------------
'** Delete duplicated files
'**---------------------------------------------
'''
'''
'''
Public Sub SearchAndDeleteDuplicatedFiles(ByVal vobjFilePaths As Collection, Optional ByVal vblnAllowToDelete As Boolean = False)

    Dim objDuplicatedFileNameToPathsDic As Scripting.Dictionary
    Dim varFileName As Variant, strFileName As String
    
    Dim objDateLastModifiedAndFilePathCol As Collection, objFailedToDeletingPaths As Collection
    
    Dim objFS As Scripting.FileSystemObject
    Dim objCandidatesDirectoriesToDeleteDic As Scripting.Dictionary, objDuplicatedFilesDataTableCol As Collection, objDoubleStopWatch As DoubleStopWatch
    
    
    Set objFS = New Scripting.FileSystemObject


    Set objDoubleStopWatch = New DoubleStopWatch
    
    objDoubleStopWatch.MeasureStart


    Set objDuplicatedFileNameToPathsDic = GetDuplicatedFiles(vobjFilePaths)

    With objDuplicatedFileNameToPathsDic

        If .Count > 0 Then
        
            For Each varFileName In .Keys
            
                strFileName = varFileName
                
                Set objDateLastModifiedAndFilePathCol = .Item(strFileName)
                
                SortCollection objDateLastModifiedAndFilePathCol, Decending
                
                If Not vblnAllowToDelete Then
                
                    AddInfoOfFilePathAndPlusAlphaInfoColToDataTableCol objDateLastModifiedAndFilePathCol, objDuplicatedFilesDataTableCol, objFS
                Else
                
                    DeleteDuplicatedFilesWithoutLatestOne objDateLastModifiedAndFilePathCol, objCandidatesDirectoriesToDeleteDic, objFailedToDeletingPaths, objFS
                End If
            Next
        Else
            MsgBox "No duplicated files exist."
        End If
    End With
    
    DeleteDirecotoriesWhenThereAreNoBothFilesAndSubDirectories objCandidatesDirectoriesToDeleteDic, objFS
    
    objDoubleStopWatch.MeasureInterval
    
    If Not vblnAllowToDelete Then
    
        On Error Resume Next
    
        Application.Run "OutputDuplicatedFilesInfoToTemporaryBook", objDuplicatedFilesDataTableCol, objDoubleStopWatch.ElapsedTimeByString
        
        On Error GoTo 0
    End If
End Sub

'**---------------------------------------------
'** Searching files which name includes Underbar with eight digits numbers
'**---------------------------------------------
'''
'''
'''
Public Function FilterNoUnderbar8DigitsDateSuffixFiles(ByVal vobjFilePaths As Collection) As Collection

    Dim varPath As Variant, strPath As String, strFileName As String
    
    Dim objFS As Scripting.FileSystemObject
    
    Dim objRegExp As VBScript_RegExp_55.RegExp
    
    Dim objFilteredFilePaths As Collection
    
    
    Set objRegExp = GetUnderbarSuffixDateRegExp()
    
    Set objFS = New Scripting.FileSystemObject
    
    Set objFilteredFilePaths = New Collection
    
    For Each varPath In vobjFilePaths
    
        strPath = varPath
    
        strFileName = objFS.GetFileName(strPath)
    
        If Not objRegExp.Test(strFileName) Then
        
            objFilteredFilePaths.Add strPath
        End If
    Next

    Set FilterNoUnderbar8DigitsDateSuffixFiles = objFilteredFilePaths
End Function

'''
'''
'''
Public Function GetSummaryFileInformationTableFromFilePaths(ByVal vobjFilePaths As Collection) As Collection

    Dim varPath As Variant, strPath As String, strFileName As String
    Dim objFS As Scripting.FileSystemObject
    
    Dim udtDateLastModified As Date, udtDateCreated As Date
    Dim strParentDirectoryPath As String
    Dim objRowCol As Collection, objDTCol As Collection
    
    
    Set objFS = New Scripting.FileSystemObject
    
    Set objDTCol = New Collection

    For Each varPath In vobjFilePaths
    
        strPath = varPath

        With objFS
        
            With .GetFile(strPath)
            
                udtDateLastModified = .DateLastModified
                
                udtDateCreated = .DateCreated
            End With
        
            strParentDirectoryPath = .GetParentFolderName(strPath)
        
            strFileName = .GetFileName(strPath)
        End With
        
        Set objRowCol = New Collection
        
        With objRowCol
        
            .Add strFileName
            
            .Add udtDateLastModified
            
            .Add udtDateCreated
            
            .Add strParentDirectoryPath
            
            .Add strPath
        End With

        objDTCol.Add objRowCol
    Next

    Set GetSummaryFileInformationTableFromFilePaths = objDTCol
End Function

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** Delete duplicated files
'**---------------------------------------------
'''
'''
'''
Private Sub AddInfoOfFilePathAndPlusAlphaInfoColToDataTableCol(ByVal vobjFilePathAndPlusAlphaInfoCol As Collection, _
        ByRef robjSummaryDataTableCol As Collection, _
        ByVal vobjFS As Scripting.FileSystemObject)


    Dim varDateLastModifiedAndFilePath As Variant, strDateLastModifiedAndFilePath As String
    Dim strTmpArray() As String, objRowCol As Collection
    
    Dim udtDateLastModified As Date, udtDateCreated As Date, strPath As String
    Dim strFileName As String, strParentDirectoryPath As String


    If robjSummaryDataTableCol Is Nothing Then Set robjSummaryDataTableCol = New Collection

    For Each varDateLastModifiedAndFilePath In vobjFilePathAndPlusAlphaInfoCol
    
        strDateLastModifiedAndFilePath = varDateLastModifiedAndFilePath
        
        strTmpArray = Split(strDateLastModifiedAndFilePath, ",")
        
        udtDateLastModified = Convert14DigitsStringToDateTime(strTmpArray(0))
        
        udtDateCreated = Convert14DigitsStringToDateTime(strTmpArray(1))
        
        strPath = strTmpArray(2)
        
        With vobjFS
        
            strParentDirectoryPath = .GetParentFolderName(strPath)
        
            strFileName = .GetFileName(strPath)
        End With
        
        Set objRowCol = New Collection
        
        With objRowCol
        
            .Add strFileName
            
            .Add udtDateLastModified
            
            .Add udtDateCreated
            
            .Add strParentDirectoryPath
            
            .Add strPath
        End With
        
        robjSummaryDataTableCol.Add objRowCol
    Next
End Sub




'''
'''
'''
Private Function mfstrGetTargetPathFromShortcutFilePath(ByVal vstrShortcutPath As String, _
        ByRef robjSourceTargetFileLastModifiedDateAndFileNameDic As Scripting.Dictionary, _
        ByVal vobjFS As Scripting.FileSystemObject, _
        ByVal vobjShell As IWshRuntimeLibrary.WshShell) As String


    Dim objShortcut As IWshRuntimeLibrary.WshShortcut, strKey As String, strTargetPath As String

    strTargetPath = ""

    Set objShortcut = vobjShell.CreateShortcut(vstrShortcutPath)

    With objShortcut

        If InStr(1, .TargetPath, "http") = 1 Then

            ' This is URL

            strTargetPath = vstrShortcutPath
        Else
            If vobjFS.FileExists(objShortcut.TargetPath) Then
        
                strTargetPath = objShortcut.TargetPath
        
                With vobjFS.GetFile(strTargetPath)
                
                    strKey = Format(.DateLastModified, "yyyymmddhhmmss") & "," & .Name
                
                    With robjSourceTargetFileLastModifiedDateAndFileNameDic
                        
                        If Not .Exists(strKey) Then
                        
                            .Add strKey, vobjFS.GetParentFolderName(vstrShortcutPath)
                        End If
                    End With
                End With
            End If
        End If
    End With

    mfstrGetTargetPathFromShortcutFilePath = strTargetPath
End Function

'''
'''
'''
Private Function mfblnIsNeedToCopy(ByRef renmFileCopySituation As FileSyncCopySituation, _
        ByRef robjLogRowCol As Collection, _
        ByVal vstrSourcePath As String, ByVal vstrDestinationPath As String, _
        ByVal vobjFS As Scripting.FileSystemObject) As Boolean


    Dim blnDoCopy As Boolean, intSecondDiff As Long, intDayDiff As Long
    Dim objSourceFile As Scripting.File, objDestinationFile As Scripting.File
    
    blnDoCopy = False
                
    With vobjFS
                
        If .FileExists(vstrDestinationPath) Then
    
            Set robjLogRowCol = Nothing
            
            If IsDifferentBetweenFilesAboutDateLastModifiedByFileSystemObject(vstrSourcePath, vstrDestinationPath, vobjFS) Then
            
                renmFileCopySituation = UpdateFileToFileSystemSync
                
                blnDoCopy = True
            End If
    
            ' for logging
            Set objSourceFile = .GetFile(vstrSourcePath)
        
            Set objDestinationFile = .GetFile(vstrDestinationPath)
            
            intSecondDiff = DateDiff("s", objDestinationFile.DateLastModified, objSourceFile.DateLastModified)
            
            intDayDiff = DateDiff("d", objDestinationFile.DateLastModified, objSourceFile.DateLastModified)
        
            msubLogOfComparingDateLastModifiedBetweenSourceAndDestination robjLogRowCol, objSourceFile, objDestinationFile, blnDoCopy, intSecondDiff, intDayDiff
        Else
            blnDoCopy = True
            
            renmFileCopySituation = CopyFileNewToFileSystemSync
        End If
    End With

    mfblnIsNeedToCopy = blnDoCopy
End Function


'''
'''
'''
Private Sub msubLogOfComparingDateLastModifiedBetweenSourceAndDestination(ByRef robjLogRowCol As Collection, _
        ByRef robjSourceFile As Scripting.File, _
        ByRef robjDestinationFile As Scripting.File, _
        ByRef rblnDoCopy As Boolean, _
        ByRef rintSecondDiff As Long, _
        ByRef rintDayDiff As Long)

    If robjSourceFile.DateLastModified <> robjDestinationFile.DateLastModified Then
    
        Set robjLogRowCol = New Collection
        
        With robjLogRowCol
        
            .Add rblnDoCopy
            
            .Add rintSecondDiff
            
            .Add rintDayDiff
            
            .Add robjSourceFile.Name
            
            .Add CDbl(robjSourceFile.Size / 1024#)
            
            .Add robjDestinationFile.Name
            
            .Add CDbl(robjDestinationFile.Size / 1024#)
            
            .Add robjSourceFile.DateLastModified
            
            .Add robjDestinationFile.DateLastModified
            
            .Add robjSourceFile.Path
            
            .Add robjDestinationFile.Path
        End With
    End If
End Sub


'''
'''
'''
Private Sub DeleteDuplicatedFilesWithoutLatestOne(ByVal vobjFilePathAndPlusAlphaInfoCol As Collection, _
        ByRef robjCandidatesDirectoriesToDeleteDic As Scripting.Dictionary, _
        ByRef robjFailedToDeletingPaths As Collection, _
        ByVal vobjFS As Scripting.FileSystemObject)


    Dim objDateLastModifiedAndFilePathCol As Collection
    Dim varDateLastModifiedAndFilePath As Variant, strDateLastModifiedAndFilePath As String
    Dim i As Long
    Dim str1stDateLastModifiedAndFilePath As String, str1stDateString As String, strTmpArray() As String
    Dim strCurrentDateString As String, strCurrentPath As String, strDirPath As String

    i = 1
                    
    For Each varDateLastModifiedAndFilePath In vobjFilePathAndPlusAlphaInfoCol
    
        strDateLastModifiedAndFilePath = varDateLastModifiedAndFilePath
    
        strTmpArray = Split(strDateLastModifiedAndFilePath, ",")
        
        If i = 1 Then
        
            str1stDateString = strTmpArray(0) & "_" & strTmpArray(1)
        Else
            strCurrentDateString = strTmpArray(0) & "_" & strTmpArray(1)
            
            strCurrentPath = strTmpArray(2)
        End If
    
        If i > 1 Then
        
            If StrComp(str1stDateString, strCurrentDateString) <> 0 Then
            
                strDirPath = vobjFS.GetParentFolderName(strCurrentPath)
            
                If robjCandidatesDirectoriesToDeleteDic Is Nothing Then Set robjCandidatesDirectoriesToDeleteDic = New Scripting.Dictionary
                
                With robjCandidatesDirectoriesToDeleteDic
                    
                    If Not .Exists(strDirPath) Then
                    
                        .Add strDirPath, 0
                    End If
                End With
            
                DeleteFileAndSendItToTrashBox strCurrentPath
            Else
                ' failed to delete
            
                If robjFailedToDeletingPaths Is Nothing Then Set robjFailedToDeletingPaths = New Collection
            
                robjFailedToDeletingPaths.Add strCurrentPath
            End If
        End If
    
        i = i + 1
    Next
End Sub


'''
'''
'''
Private Sub DeleteDirecotoriesWhenThereAreNoBothFilesAndSubDirectories(ByRef robjDirectoryToDeleteDic As Scripting.Dictionary, _
        ByVal vobjFS As Scripting.FileSystemObject)

    Dim strDirPath As String, varDirPath As Variant, objFailedToDeletingPaths As Collection
    Dim blnAllowToDeleteDirecotory As Boolean

    If Not robjDirectoryToDeleteDic Is Nothing Then
    
        For Each varDirPath In robjDirectoryToDeleteDic.Keys
        
            strDirPath = varDirPath
        
            blnAllowToDeleteDirecotory = False
        
            With vobjFS.GetFolder(strDirPath)
            
                If .Files.Count = 0 And .SubFolders.Count = 0 Then
                
                    ' .Delete True
                    
                    blnAllowToDeleteDirecotory = True
                End If
            End With
            
            If blnAllowToDeleteDirecotory Then
            
                DeleteDirectoryAndSendItToTrashBox strDirPath
            End If
        Next
    End If
End Sub


'''
'''
'''
Private Function GetDuplicatedFiles(ByVal vobjFilePaths As Collection) As Scripting.Dictionary

    Dim objFileNameToPathDic As Scripting.Dictionary, varPath As Variant, strPath As String, objFS As Scripting.FileSystemObject
    Dim objDuplicatedFileNameToPathsDic As Scripting.Dictionary
    
    Dim objSuffixDateRegExp As VBScript_RegExp_55.RegExp, strFileName As String, strFileNameWithoutSuffixDate As String
    
    
    Set objFileNameToPathDic = New Scripting.Dictionary
    
    Set objDuplicatedFileNameToPathsDic = New Scripting.Dictionary
    
    Set objFS = New Scripting.FileSystemObject
    
    
    For Each varPath In vobjFilePaths
    
        strPath = varPath
    
        strFileName = objFS.GetFileName(strPath)
    
        With objFileNameToPathDic
        
            If Not .Exists(strFileName) Then
        
                .Add strFileName, strPath
            Else
                ' It is a duplicated file
                
                msubAddDuplicatedFileInfoToDic objDuplicatedFileNameToPathsDic, objFileNameToPathDic, strPath, strFileName, objFS
            End If
        End With
    Next
    
    Set objSuffixDateRegExp = GetUnderbarSuffixDateRegExp()
    
    For Each varPath In vobjFilePaths
        
        strPath = varPath
    
        strFileName = objFS.GetFileName(strPath)
        
        With objSuffixDateRegExp
    
            If .Test(strFileName) Then
    
                strFileNameWithoutSuffixDate = .Replace(strFileName, "")
                
                If objFileNameToPathDic.Exists(strFileNameWithoutSuffixDate) Then
                
                    msubAddDuplicatedFileInfoToDic objDuplicatedFileNameToPathsDic, objFileNameToPathDic, strPath, strFileNameWithoutSuffixDate, objFS
                End If
            End If
        End With
    Next

    Set GetDuplicatedFiles = objDuplicatedFileNameToPathsDic
End Function


'''
'''
'''
Private Sub msubAddDuplicatedFileInfoToDic(ByRef robjDuplicatedFileNameToPathsDic As Scripting.Dictionary, _
        ByVal vobjFileNameToPathDic As Scripting.Dictionary, _
        ByVal vstrCurrentPath As String, _
        ByVal vstrCurrentFileName As String, _
        ByVal vobjFS As Scripting.FileSystemObject)


    Dim strPreviousFilePath As String, objDateLastModifiedAndFilePaths As Collection
    Dim strDateLastModifiedAndFilePath As String, strPreviousDateLastModifiedAndFilePath As String


    With robjDuplicatedFileNameToPathsDic
                
        If Not .Exists(vstrCurrentFileName) Then
        
            strPreviousFilePath = vobjFileNameToPathDic.Item(vstrCurrentFileName)
            
            With vobjFS.GetFile(strPreviousFilePath)
            
                strPreviousDateLastModifiedAndFilePath = Format(.DateLastModified, "yyyymmddhhmmss") & "," & Format(.DateCreated, "yyyymmddhhmmss") & "," & strPreviousFilePath
            End With
            
            With vobjFS.GetFile(vstrCurrentPath)
            
                strDateLastModifiedAndFilePath = Format(.DateLastModified, "yyyymmddhhmmss") & "," & Format(.DateCreated, "yyyymmddhhmmss") & "," & vstrCurrentPath
            End With
        
            Set objDateLastModifiedAndFilePaths = New Collection
        
            With objDateLastModifiedAndFilePaths
            
                .Add strPreviousDateLastModifiedAndFilePath
                
                .Add strDateLastModifiedAndFilePath
            End With
        
            .Add vstrCurrentFileName, objDateLastModifiedAndFilePaths
        Else
            Set objDateLastModifiedAndFilePaths = .Item(vstrCurrentFileName)
            
            With vobjFS.GetFile(vstrCurrentPath)
            
                strDateLastModifiedAndFilePath = Format(.DateLastModified, "yyyymmddhhmmss") & "," & Format(.DateCreated, "yyyymmddhhmmss") & "," & vstrCurrentPath
            End With
            
            objDateLastModifiedAndFilePaths.Add strDateLastModifiedAndFilePath
            
            Set .Item(vstrCurrentFileName) = objDateLastModifiedAndFilePaths
        End If
    End With
End Sub



'''
'''
'''
Private Sub msubPrepareToDeleteFile(ByVal vstrPathToDelete As String, _
        ByRef robjDeletePaths As Collection, _
        ByRef robjDeletePathInfos As Collection, _
        ByRef robjCandidatesDirectoriesToDeleteDic As Scripting.Dictionary, _
        ByVal vblnAllowToDelete As Boolean, _
        ByVal vobjFS As Scripting.FileSystemObject)


    Dim strDirPath As String, objRowCol As Collection

    If vblnAllowToDelete Then
    
        If robjDeletePaths Is Nothing Then Set robjDeletePaths = New Collection
        
        If robjCandidatesDirectoriesToDeleteDic Is Nothing Then Set robjCandidatesDirectoriesToDeleteDic = New Scripting.Dictionary
        
        robjDeletePaths.Add vstrPathToDelete
        
        strDirPath = vobjFS.GetParentFolderName(vstrPathToDelete)
        
        With robjCandidatesDirectoriesToDeleteDic
                                
            If Not .Exists(strDirPath) Then
            
                .Add strDirPath, 0
            End If
        End With
    Else
        If robjDeletePathInfos Is Nothing Then Set robjDeletePathInfos = New Collection
                                    
        Set objRowCol = New Collection
        
        With objRowCol
        
            .Add vstrPathToDelete
            
            .Add vobjFS.GetFile(vstrPathToDelete).DateLastModified
        End With
    
        robjDeletePathInfos.Add objRowCol
    End If
End Sub


'''
'''
'''
Private Function mfobjGetKeysOfLastModifiedDateAndFileNameDic(ByVal vobjSourceFilePathInfoDic As Scripting.Dictionary) As Scripting.Dictionary

    Dim varRelativePath As Variant, strRelativePath As String
    Dim objDic As Scripting.Dictionary, strNewKey As String
    
    Set objDic = New Scripting.Dictionary
    
    If Not vobjSourceFilePathInfoDic Is Nothing Then
    
        If vobjSourceFilePathInfoDic.Count > 0 Then
            
            With vobjSourceFilePathInfoDic
            
                For Each varRelativePath In .Keys
            
                    strRelativePath = varRelativePath
            
                    strNewKey = mfstrGetKeyFromModifiedDateAndFullPathRowCol(.Item(varRelativePath))
            
                    With objDic
                    
                        If Not .Exists(strNewKey) Then
                        
                            .Add strNewKey, 0
                        End If
                    End With
                Next
            End With
        End If
    End If

    Set mfobjGetKeysOfLastModifiedDateAndFileNameDic = objDic
End Function


'''
'''
'''
Private Function mfstrGetKeyFromModifiedDateAndFullPathRowCol(ByVal vobjLastModifiedDateAndFullPathRowCol As Collection) As String

    Dim strFullPath As String, udtLastModifiedDate As Date
    
    With vobjLastModifiedDateAndFullPathRowCol
    
        udtLastModifiedDate = .Item(1)
        
        strFullPath = .Item(2)
    End With

    mfstrGetKeyFromModifiedDateAndFullPathRowCol = Format(udtLastModifiedDate, "yyyymmddhhmmss") & "," & GetFileNameFromRelativeFilePath(strFullPath)
End Function



'''
'''
'''
''' <Argument>vobjSourceFilePathInfoDic : Dictionary(Of Key[RelativeFullPath], Of Collection(Date[LastModifiedDate],String[FullPath]))</Argument>
''' <Return>Dictionary(Of Key[RelativeFullPath], Of Collection(Date[LastModifiedDate],String[converted FullPath]))</Return>
Private Function GetDicOfSourceFilePathInfoWithReplacingOnlyShortcutFile(ByVal vobjSourceFilePathInfoDic As Scripting.Dictionary) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, objNewDic As Scripting.Dictionary, blnFoundShortcut As Boolean
    Dim varRelativeFullPath As Variant, strRelativeFullPath As String, strFileName As String
    Dim strConvertedRelativeFullPath As String, objConvertedRowCol As Collection
    Dim objFS As Scripting.FileSystemObject, objShell As IWshRuntimeLibrary.WshShell
    
    
    Set objFS = New Scripting.FileSystemObject
    
    Set objShell = New IWshRuntimeLibrary.WshShell
    
    Set objDic = vobjSourceFilePathInfoDic

    blnFoundShortcut = False
    
    If Not vobjSourceFilePathInfoDic Is Nothing Then
    
        If vobjSourceFilePathInfoDic.Count > 0 Then
    
            Set objNewDic = New Scripting.Dictionary
            
            With vobjSourceFilePathInfoDic
            
                For Each varRelativeFullPath In .Keys
                
                    strRelativeFullPath = varRelativeFullPath
                    
                    strFileName = GetFileNameFromRelativeFilePath(strRelativeFullPath)
        
                    If LCase(objFS.GetExtensionName(strFileName)) = "lnk" Then
        
                        blnFoundShortcut = True
        
                        msubGetConvertedRelativePathAndInfoFromShortcut strConvertedRelativeFullPath, objConvertedRowCol, strRelativeFullPath, .Item(varRelativeFullPath), objFS, objShell
                        
                        If strConvertedRelativeFullPath <> "" Then
                        
                            objNewDic.Add strConvertedRelativeFullPath, objConvertedRowCol
                        Else
                            Debug.Print "Shortcut target file is missing: " & objShell.CreateShortcut(.Item(varRelativeFullPath).Item(2)).TargetPath
                        End If
                    Else
                        objNewDic.Add strRelativeFullPath, .Item(varRelativeFullPath)
                    End If
                Next
            End With
        End If
    End If

    If blnFoundShortcut Then
    
        Set objDic = objNewDic
    End If

    Set GetDicOfSourceFilePathInfoWithReplacingOnlyShortcutFile = objDic
End Function


'''
'''
'''
Private Sub msubGetConvertedRelativePathAndInfoFromShortcut(ByRef rstrConvertedRelativeFullPath As String, _
        ByRef robjConvertedRowCol As Collection, _
        ByVal vstrRelativeFullPath As String, _
        ByVal vobjRowCol As Collection, _
        ByVal vobjFS As Scripting.FileSystemObject, _
        ByVal vobjShell As IWshRuntimeLibrary.WshShell)


    Dim strSourceShortcutFullPath As String, intBackslashPoint As Long, objShortcut As IWshRuntimeLibrary.WshShortcut
    Dim strRelativeParentDirPath As String, strNewFullPath As String, stdNewLastModifiedDate As Date


    strSourceShortcutFullPath = vobjRowCol.Item(2)

    Set objShortcut = vobjShell.CreateShortcut(strSourceShortcutFullPath)

    If vobjFS.FileExists(objShortcut.TargetPath) Then

        With vobjFS
        
            strNewFullPath = .GetParentFolderName(strSourceShortcutFullPath) & "\" & .GetFileName(objShortcut.TargetPath)
            
            With .GetFile(objShortcut.TargetPath)
            
                stdNewLastModifiedDate = .DateLastModified
            End With
        End With
        
        Set robjConvertedRowCol = New Collection
        
        With robjConvertedRowCol
        
            .Add stdNewLastModifiedDate
            
            .Add strNewFullPath
        End With
        
    
        intBackslashPoint = InStrRev(vstrRelativeFullPath, "\")
    
        If intBackslashPoint > 0 Then
        
            rstrConvertedRelativeFullPath = Replace(vstrRelativeFullPath, GetFileNameFromRelativeFilePath(vstrRelativeFullPath), "") & vobjFS.GetFileName(objShortcut.TargetPath)
        Else
            ' it stands for only file name
        
            rstrConvertedRelativeFullPath = vobjFS.GetFileName(objShortcut.TargetPath)
        End If
    Else
        rstrConvertedRelativeFullPath = ""
        
        Set robjConvertedRowCol = Nothing
    End If
End Sub


'**---------------------------------------------
'** Searching files which name includes Underbar with eight digits numbers
'**---------------------------------------------
'''
''' this is dependent on static variable 'mobjSuffixDateRegExp' in this module declaration
'''
Private Function GetUnderbarSuffixDateRegExp() As VBScript_RegExp_55.RegExp

    If mobjSuffixDateRegExp Is Nothing Then
    
        Set mobjSuffixDateRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjSuffixDateRegExp
        
            .Global = False
            
            .Pattern = "_[0-9]{4}[0-9]{2}[0-9]{2}"
        End With
    End If

    Set GetUnderbarSuffixDateRegExp = mobjSuffixDateRegExp
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToConfirmDirectionPlusMinusAboutDateDiff()

    Dim udtDate As Date, udtLaterDate As Date
    
    
    udtDate = Now()
    
    udtLaterDate = DateAdd("d", 1, udtDate)
    
    Debug.Print "Base date - " & Format(udtDate, "d") & ", later date - " & Format(udtLaterDate, "d")
    
    Debug.Print "It should be plus - " & DateDiff("d", udtDate, udtLaterDate)
    
    Debug.Print "It should be minus - " & DateDiff("d", udtLaterDate, udtDate)
End Sub


'**---------------------------------------------
'** Key-Value cache preparation for FileSysSyncCopy
'**---------------------------------------------
'''
''' get string Value from STR_KEY_FILE_SYS_SYNC_COPY_CAPTION_UPDATE_FILES
'''
''' automatically-added for FileSysSyncCopy string-key-value management for standard module and class module
Public Function GetTextOfStrKeyFileSysSyncCopyCaptionUpdateFiles() As String

    If Not mblnIsStrKeyValueFileSysSyncCopyDicInitialized Then

        msubInitializeTextForFileSysSyncCopy
    End If

    GetTextOfStrKeyFileSysSyncCopyCaptionUpdateFiles = mobjStrKeyValueFileSysSyncCopyDic.Item("STR_KEY_FILE_SYS_SYNC_COPY_CAPTION_UPDATE_FILES")
End Function

'''
''' get string Value from STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING
'''
''' automatically-added for FileSysSyncCopy string-key-value management for standard module and class module
Public Function GetTextOfStrKeyFileSysSyncCopyMesCopying() As String

    If Not mblnIsStrKeyValueFileSysSyncCopyDicInitialized Then

        msubInitializeTextForFileSysSyncCopy
    End If

    GetTextOfStrKeyFileSysSyncCopyMesCopying = mobjStrKeyValueFileSysSyncCopyDic.Item("STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING")
End Function

'''
''' get string Value from STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING_NEW
'''
''' automatically-added for FileSysSyncCopy string-key-value management for standard module and class module
Public Function GetTextOfStrKeyFileSysSyncCopyMesCopyingNew() As String

    If Not mblnIsStrKeyValueFileSysSyncCopyDicInitialized Then

        msubInitializeTextForFileSysSyncCopy
    End If

    GetTextOfStrKeyFileSysSyncCopyMesCopyingNew = mobjStrKeyValueFileSysSyncCopyDic.Item("STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING_NEW")
End Function

'''
''' get string Value from STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING_AS_UPDATE
'''
''' automatically-added for FileSysSyncCopy string-key-value management for standard module and class module
Public Function GetTextOfStrKeyFileSysSyncCopyMesCopyingAsUpdate() As String

    If Not mblnIsStrKeyValueFileSysSyncCopyDicInitialized Then

        msubInitializeTextForFileSysSyncCopy
    End If

    GetTextOfStrKeyFileSysSyncCopyMesCopyingAsUpdate = mobjStrKeyValueFileSysSyncCopyDic.Item("STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING_AS_UPDATE")
End Function

'''
''' get string Value from STR_KEY_FILE_SYS_SYNC_COPY_MES_ELAPSED_TIME
'''
''' automatically-added for FileSysSyncCopy string-key-value management for standard module and class module
Public Function GetTextOfStrKeyFileSysSyncCopyMesElapsedTime() As String

    If Not mblnIsStrKeyValueFileSysSyncCopyDicInitialized Then

        msubInitializeTextForFileSysSyncCopy
    End If

    GetTextOfStrKeyFileSysSyncCopyMesElapsedTime = mobjStrKeyValueFileSysSyncCopyDic.Item("STR_KEY_FILE_SYS_SYNC_COPY_MES_ELAPSED_TIME")
End Function

'''
''' get string Value from STR_KEY_FILE_SYS_SYNC_COPY_CAPTION_UPDATE_DESTINATION_SHORTCUT
'''
''' automatically-added for FileSysSyncCopy string-key-value management for standard module and class module
Public Function GetTextOfStrKeyFileSysSyncCopyCaptionUpdateDestinationShortcut() As String

    If Not mblnIsStrKeyValueFileSysSyncCopyDicInitialized Then

        msubInitializeTextForFileSysSyncCopy
    End If

    GetTextOfStrKeyFileSysSyncCopyCaptionUpdateDestinationShortcut = mobjStrKeyValueFileSysSyncCopyDic.Item("STR_KEY_FILE_SYS_SYNC_COPY_CAPTION_UPDATE_DESTINATION_SHORTCUT")
End Function

'''
''' get string Value from STR_KEY_FILE_SYS_SYNC_COPY_MES_UPDATE_SHORTCUT
'''
''' automatically-added for FileSysSyncCopy string-key-value management for standard module and class module
Public Function GetTextOfStrKeyFileSysSyncCopyMesUpdateShortcut() As String

    If Not mblnIsStrKeyValueFileSysSyncCopyDicInitialized Then

        msubInitializeTextForFileSysSyncCopy
    End If

    GetTextOfStrKeyFileSysSyncCopyMesUpdateShortcut = mobjStrKeyValueFileSysSyncCopyDic.Item("STR_KEY_FILE_SYS_SYNC_COPY_MES_UPDATE_SHORTCUT")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for FileSysSyncCopy string-key-value management for standard module and class module
Private Sub msubInitializeTextForFileSysSyncCopy()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueFileSysSyncCopyDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForFileSysSyncCopyByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForFileSysSyncCopyByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueFileSysSyncCopyDicInitialized = True
    End If

    Set mobjStrKeyValueFileSysSyncCopyDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for FileSysSyncCopy key-values cache
'''
''' automatically-added for FileSysSyncCopy string-key-value management for standard module and class module
Private Sub AddStringKeyValueForFileSysSyncCopyByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_FILE_SYS_SYNC_COPY_CAPTION_UPDATE_FILES", "Update files"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING", "Copying"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING_NEW", "New copying"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING_AS_UPDATE", "Update"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_MES_ELAPSED_TIME", "Elapsed time"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_CAPTION_UPDATE_DESTINATION_SHORTCUT", "Update destination shortcut files"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_MES_UPDATE_SHORTCUT", "Update the shortcut"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for FileSysSyncCopy key-values cache
'''
''' automatically-added for FileSysSyncCopy string-key-value management for standard module and class module
Private Sub AddStringKeyValueForFileSysSyncCopyByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_FILE_SYS_SYNC_COPY_CAPTION_UPDATE_FILES", "ファイルを更新中"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING", "コピー中"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING_NEW", "新たにコピー中"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING_AS_UPDATE", "更新上書きコピー中"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_MES_ELAPSED_TIME", "経過時間"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_CAPTION_UPDATE_DESTINATION_SHORTCUT", "コピー先のショートカットを更新中"
        .Add "STR_KEY_FILE_SYS_SYNC_COPY_MES_UPDATE_SHORTCUT", "ショートカットをコピー中"
    End With
End Sub

'''
''' Remove Keys for FileSysSyncCopy key-values cache
'''
''' automatically-added for FileSysSyncCopy string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueFileSysSyncCopy(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_FILE_SYS_SYNC_COPY_CAPTION_UPDATE_FILES") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_FILE_SYS_SYNC_COPY_CAPTION_UPDATE_FILES"
            .Remove "STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING"
            .Remove "STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING_NEW"
            .Remove "STR_KEY_FILE_SYS_SYNC_COPY_MES_COPYING_AS_UPDATE"
            .Remove "STR_KEY_FILE_SYS_SYNC_COPY_MES_ELAPSED_TIME"
            .Remove "STR_KEY_FILE_SYS_SYNC_COPY_CAPTION_UPDATE_DESTINATION_SHORTCUT"
            .Remove "STR_KEY_FILE_SYS_SYNC_COPY_MES_UPDATE_SHORTCUT"
        End If
    End With
End Sub

