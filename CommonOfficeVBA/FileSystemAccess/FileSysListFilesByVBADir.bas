Attribute VB_Name = "FileSysListFilesByVBADir"
'
'   Using VBA.Dir functions, search files in the specified directries and get these attribute information
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on VBA.Dir function
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun, 19/Jun/2022    Kalmclaeyd Tarclanus    Refactoring
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Compare two files about Date-last-modifed by VBA library
'**---------------------------------------------
'''
''' No use FileSystemObject, use VBA library
'''
Public Function IsDifferentBetweenFilesAboutDateLastModified(ByRef rstrSrcPath As String, ByRef rstrDstPath As String) As Boolean

    Dim intSecondDiff As Long, intDayDiff As Long, blnDoCopy As Boolean
        
    Const intEpsilonOfSecond As Long = 5    ' When the Window copy any file in special combination of an internal HDD and an external HDD, each file timestamp are always shifted between one and two about each file timestamp.

    blnDoCopy = False

    intSecondDiff = DateDiff("s", VBA.FileDateTime(rstrDstPath), VBA.FileDateTime(rstrSrcPath))
    
    intDayDiff = DateDiff("d", VBA.FileDateTime(rstrDstPath), VBA.FileDateTime(rstrSrcPath))

    If Math.Abs(intSecondDiff) <= intEpsilonOfSecond Or intSecondDiff < 0 Or intDayDiff < 0 Then
    
        blnDoCopy = False
    Else
        blnDoCopy = True
    End If
    
    IsDifferentBetweenFilesAboutDateLastModified = blnDoCopy
End Function

'**---------------------------------------------
'** Delete files for matching items with searching sub-directories
'**---------------------------------------------
'''
'''
'''
Public Sub DeleteWildCardMatchedFilesByVbaDir(ByRef rstrParentDir As String, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "", _
        Optional ByVal vblnSearchSubDirectoriesRecursively As Boolean = True, _
        Optional ByVal vblnAllowToDeleteDirectoriesIfItHasNoFiles As Boolean = True)

    
    Dim objChildDirs As Collection, objFoundMatchedFiles As Collection, objRemainedFiles As Collection, objGrandChildDirs As Collection
    
    Dim varChildDir As Variant, strChildDir As String, varFilePath As Variant, strFilePath As String


    Set objFoundMatchedFiles = GetFilePathsUsingVbaDirWithoutSearchingSubDirectories(rstrParentDir, _
            vstrRawWildCardsByDelimitedChar, _
            "FullPath", _
            vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma)


    For Each varFilePath In objFoundMatchedFiles
    
        strFilePath = varFilePath
    
        VBA.Kill strFilePath
    Next

    If vblnSearchSubDirectoriesRecursively Then
    
        Set objChildDirs = GetDirectoryPathsUsingVbaDirWithoutSearchingSubDirectoriesOfFoundDirectories(rstrParentDir)
    
        For Each varChildDir In objChildDirs
    
            strChildDir = varChildDir
    
            DeleteWildCardMatchedFilesByVbaDir strChildDir, _
                    vstrRawWildCardsByDelimitedChar, _
                    vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma, _
                    vblnSearchSubDirectoriesRecursively, _
                    vblnAllowToDeleteDirectoriesIfItHasNoFiles
            
            
            If vblnAllowToDeleteDirectoriesIfItHasNoFiles Then
            
                Set objGrandChildDirs = GetDirectoryPathsUsingVbaDirWithoutSearchingSubDirectoriesOfFoundDirectories(strChildDir)
                
                Set objRemainedFiles = GetFilePathsUsingVbaDirWithoutSearchingSubDirectories(strChildDir, _
                        "*", _
                        "FullPath", _
                        vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma)
                
                If objGrandChildDirs.Count = 0 And objRemainedFiles.Count = 0 Then
                
                    VBA.RmDir strChildDir
                End If
            End If
        Next
    End If
End Sub


'**---------------------------------------------
'** Using VBA library
'**---------------------------------------------
'''
''' use VBA.Dir
'''
Public Function FileExistsByVbaDir(ByRef rstrFilePath As String) As Boolean

    Dim blnIsFound As Boolean
    
    blnIsFound = False
    
    If VBA.Dir(rstrFilePath) <> "" Then
    
        blnIsFound = True
    End If

    FileExistsByVbaDir = blnIsFound
End Function

'''
''' use VBA.Dir
'''
Public Function DirectoryExistsByVbaDir(ByRef rstrDirectoryPath As String) As Boolean

    Dim blnIsFound As Boolean
    
    blnIsFound = False
    
    If VBA.Dir(rstrDirectoryPath) = "" And VBA.Dir(rstrDirectoryPath, vbDirectory) <> "" Then
    
        blnIsFound = True
    End If

    DirectoryExistsByVbaDir = blnIsFound
End Function

'''
'''
'''
Public Function GetExtensionNameByVbaDir(ByRef rstrFilePath As String) As String

    Dim strExtension As String, strFileName As String, intPointOfString As Integer
    
    strExtension = "": strFileName = ""
    
    GetFileNameToArgFromPathByVbaDir strFileName, rstrFilePath

    If strFileName <> "" Then

        GetExtensionToArgNameFromFileNameByVbaDir strExtension, strFileName
    End If

    GetExtensionNameByVbaDir = strExtension
End Function

'''
'''
'''
Public Function GetBaseNameByVbaDir(ByRef rstrFilePath As String) As String

    Dim strBaseName As String, strFileName As String, intPointOfString As Integer, strRightFilePath As String
    
    strBaseName = "": strFileName = ""
    
    GetFileNameToArgFromPathByVbaDir strFileName, rstrFilePath

    If strFileName <> "" Then
    
        GetBaseNameToArgFromFileNameByVbaDir strBaseName, strFileName
    Else
        Select Case InStr(1, rstrFilePath, "\")
        
            Case 0
            
                GetBaseNameToArgFromFileNameByVbaDir strBaseName, rstrFilePath
            
            Case 1
            
                strRightFilePath = Right(rstrFilePath, Len(rstrFilePath) - 1)
            
                If InStr(1, strRightFilePath, "\") = 0 Then
        
                    GetBaseNameToArgFromFileNameByVbaDir strBaseName, strRightFilePath
                End If
        End Select
    End If

    GetBaseNameByVbaDir = strBaseName
End Function

'''
''' use VBA.Dir
'''
Public Function GetParentDirectoryPathByVbaDir(ByRef rstrPath As String) As String

    Dim strParentDirectoryPath As String
    
    GetParentDirectoryPathToArgByVbaDir strParentDirectoryPath, rstrPath

    GetParentDirectoryPathByVbaDir = strParentDirectoryPath
End Function

'''
''' use VBA.Dir
'''
Public Sub GetParentDirectoryPathToArgByVbaDir(ByRef rstrParentDirectoryPath As String, _
        ByRef rstrPath As String)

    Dim intPointOfString As Long

    intPointOfString = InStrRev(rstrPath, "\")

    If intPointOfString > 0 Then
    
        If VBA.Dir(rstrPath) <> "" Then
        
            ' The rstrPath is a file, and it exists
        
            rstrParentDirectoryPath = Left(rstrPath, intPointOfString - 1)
        
        ElseIf VBA.Dir(rstrPath, vbDirectory) <> "" Then
        
            rstrParentDirectoryPath = rstrPath
        Else
            ' This path may have not existed yet
        
            rstrParentDirectoryPath = Left(rstrPath, intPointOfString - 1)
        End If
    Else
        If VBA.Dir(rstrPath, vbDirectory) <> "" Then
    
            rstrParentDirectoryPath = rstrPath
        Else
            rstrParentDirectoryPath = ""
        End If
    End If
End Sub


'''
'''
'''
Public Function GetParentDicrectoryPathFromCurrentExistedDirectoryPathByVbaDir(ByRef rstrDirectoryPath As String, _
        Optional ByVal vintCountOfTraceParent As Long = 1) As String

    Dim strParentDirectoryPath As String

    GetParentDicrectoryPathToArgFromCurrentExistedDirectoryPathByVbaDir strParentDirectoryPath, _
            rstrDirectoryPath, _
            vintCountOfTraceParent


    GetParentDicrectoryPathFromCurrentExistedDirectoryPathByVbaDir = strParentDirectoryPath
End Function


'''
'''
'''
''' <Argument>rstrParentDirectoryPath: Output</Argument>
''' <Argument>rstrDirectoryPath: Input</Argument>
''' <Argument>vintCountOfTraceParent: Input</Argument>
Public Sub GetParentDicrectoryPathToArgFromCurrentExistedDirectoryPathByVbaDir(ByRef rstrParentDirectoryPath As String, _
        ByRef rstrDirectoryPath As String, _
        Optional ByVal vintCountOfTraceParent As Long = 1)

    Dim i As Long, intPointOfString As Long, strCurrentPath As String

    strCurrentPath = rstrDirectoryPath

    For i = 1 To vintCountOfTraceParent
    
        If VBA.Dir(strCurrentPath, vbDirectory) <> "" Then
        
            intPointOfString = InStrRev(strCurrentPath, "\")
            
            If intPointOfString > 0 Then
        
                strCurrentPath = Left(strCurrentPath, intPointOfString - 1)
            Else
                strCurrentPath = ""
                
                Exit For
            End If
        End If
    Next
    
    If strCurrentPath <> "" Then
    
        rstrParentDirectoryPath = strCurrentPath
    Else
        rstrParentDirectoryPath = ""
    End If
End Sub


'''
''' This also supports the relative path
'''
''' <Argument>rstrFileName: Output</Argument>
''' <Argument>rstrPath: Input</Argument>
Public Sub GetFileNameToArgFromPathByVbaDir(ByRef rstrFileName As String, _
        ByRef rstrPath As String)

    Dim intPointOfString As Long

    intPointOfString = InStrRev(rstrPath, "\")
    
    If intPointOfString > 0 Then
    
        If VBA.Dir(rstrPath) <> "" Then
    
            rstrFileName = VBA.Dir(rstrPath)
        Else
            rstrFileName = Right(rstrPath, Len(rstrPath) - intPointOfString)
        End If
    Else
        If VBA.Dir(rstrPath, vbDirectory) = "" Then
        
            If rstrPath <> "" Then
            
                rstrFileName = rstrPath
            End If
        End If
    End If
End Sub

'''
'''
'''
''' <Argument>rstrPath</Argument>
''' <Return>String</Return>
Public Function GetFileNameFromPathByVbaDir(ByRef rstrPath As String) As String

    Dim strFileName As String
    
    strFileName = ""
    
    GetFileNameToArgFromPathByVbaDir strFileName, rstrPath
    
    GetFileNameFromPathByVbaDir = strFileName
End Function

'''
'''
'''
''' <Argument>rstrExtension: Output</Argument>
''' <Argument>rstrFileName: Input</Argument>
Public Sub GetExtensionToArgNameFromFileNameByVbaDir(ByRef rstrExtension As String, _
        ByRef rstrFileName As String)

    Dim intPointOfString As Long
    
    intPointOfString = InStrRev(rstrFileName, ".")
    
    If intPointOfString > 0 Then
                
        rstrExtension = Right(rstrFileName, Len(rstrFileName) - intPointOfString)
    Else
        rstrExtension = ""
    End If
End Sub

'''
'''
'''
''' <Argument>rstrBaseName: Output</Argument>
''' <Argument>rstrFileName: Input</Argument>
Public Sub GetBaseNameToArgFromFileNameByVbaDir(ByRef rstrBaseName As String, _
        ByRef rstrFileName As String)

    Dim intPointOfString As Long
    
    intPointOfString = InStrRev(rstrFileName, ".")
    
    If intPointOfString > 0 Then
                
        rstrBaseName = Left(rstrFileName, intPointOfString - 1)
    Else
        rstrBaseName = rstrFileName
    End If
End Sub

'''
'''
'''
''' <Argument>rstrFileName: Input</Argument>
''' <Return>String: file base name</Return>
Public Function GetBaseNameFromFileNameByVbaDir(ByRef rstrFileName As String) As String

    Dim strBaseName As String
    
    GetBaseNameToArgFromFileNameByVbaDir strBaseName, rstrFileName

    GetBaseNameFromFileNameByVbaDir = strBaseName
End Function

'''
''' use VBA.Kill
'''
''' <Argument>vstrParentDirectoryPath: Input</Argument>
''' <Argument>vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma: Input</Argument>
Public Sub DeleteAllFilesBelowSpecifiedDirectory(ByVal vstrParentDirectoryPath As String, _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "")

    Dim objFSSearchCondition As FileSysSearchCondition, objFSSearchResult As FileSysSearchResult, varPath As Variant
    
    
    Set objFSSearchCondition = New FileSysSearchCondition
    
    With objFSSearchCondition
    
        .FileInfomationTypesDelimitedByComma = "FullPath"
        
        .SearchingExceptionFileOrDirectoryNamesDelimitedByComma = vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma
    End With

    Set objFSSearchResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(vstrParentDirectoryPath, objFSSearchCondition)

    DebugCol objFSSearchResult.ResultDataTableCol

    With objFSSearchResult
    
        If Not .ResultDataTableCol Is Nothing Then
        
            If .ResultDataTableCol.Count > 0 Then
            
                For Each varPath In .ResultDataTableCol
                
                    VBA.Kill varPath
                Next
            End If
        End If
    End With
End Sub

'''
''' use VBA.Dir
'''
''' <Argument></Argument>
Public Sub DeleteFileIfExistsByVbaDir(ByVal vstrFilePath As String)

    If FileExistsByVbaDir(vstrFilePath) Then

        VBA.Kill vstrFilePath
    End If
End Sub

'**---------------------------------------------
'** Search Windows file system using VBA.Dir
'**---------------------------------------------
'''
'''
'''
Public Function GetFileNameToPathDicByVbaDir(ByVal vstrParentDirectoryPath As String, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As Scripting.Dictionary


    Set GetFileNameToPathDicByVbaDir = GetAnyTypeFileInfoDicByVbaDir(vstrParentDirectoryPath, _
            "FileName,FullPath", _
            vstrRawWildCardsByDelimitedChar, _
            -1, _
            -1, _
            vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma)
End Function

'''
'''
'''
Public Function GetFileNameToPathDicByVbaDirWithoutSearchingSubDirectories(ByVal vstrParentDirectoryPath As String, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As Scripting.Dictionary


    Set GetFileNameToPathDicByVbaDirWithoutSearchingSubDirectories = GetAnyTypeFileInfoDicByVbaDir(vstrParentDirectoryPath, _
            "FileName,FullPath", _
            vstrRawWildCardsByDelimitedChar, _
            0, _
            0, _
            vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma)
End Function

'''
'''
'''
Public Function GetAnyTypeFileListsColByVbaDir(ByVal vstrParentDirectoryPath As String, _
        Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "RelativeFullPath", _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As Collection

    Dim objFSSearchCondition As FileSysSearchCondition, objFSSearchResult As FileSysSearchResult

    If IsDriveReady(vstrParentDirectoryPath) Then
    
        Set objFSSearchCondition = GetFSSearchConditionAsCollectionForSearchingFilesGenerally(vstrStringOfFileInfomationTypesDelimitedByComma, _
                vstrRawWildCardsByDelimitedChar, _
                vintMinimumHierarchicalOrderToSearch, _
                vintMaximumHierarchicalOrderToSearch, _
                vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma)
    
        ' using VBA.Dir
        Set objFSSearchResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(vstrParentDirectoryPath, _
                objFSSearchCondition)
    End If

    Set GetAnyTypeFileListsColByVbaDir = objFSSearchResult.ResultDataTableCol
End Function

'''
'''
'''
Public Function GetAnyTypeFileInfoDicByVbaDir(ByVal vstrParentDirectoryPath As String, _
        Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "RelativeFullPath,FileName", _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As Scripting.Dictionary


    Dim objFSSearchCondition As FileSysSearchCondition, objFSSearchResult As FileSysSearchResult

    If IsDriveReady(vstrParentDirectoryPath) Then
    
        Set objFSSearchCondition = GetFSSearchConditionAsDictionaryForSearchingFilesGenerally(vstrStringOfFileInfomationTypesDelimitedByComma, _
                vstrRawWildCardsByDelimitedChar, _
                vintMinimumHierarchicalOrderToSearch, _
                vintMaximumHierarchicalOrderToSearch, _
                vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma)
    
        ' using VBA.Dir
        Set objFSSearchResult = GetFileSysSearchedResultDictionaryByConditionUsingVbaDir(vstrParentDirectoryPath, objFSSearchCondition)
    End If

    Set GetAnyTypeFileInfoDicByVbaDir = objFSSearchResult.ResultDataTableDic
End Function

'''
'''
'''
Public Function GetDirectoriesListsCol(ByVal vstrParentDirectoryPath As String, _
        Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "RelativeFullPath", _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*") As Collection

    Dim objFSSearchCondition As FileSysSearchCondition, objFSSearchResult As FileSysSearchResult

    If IsDriveReady(vstrParentDirectoryPath) Then
    
        Set objFSSearchCondition = GetFSSearchConditionAsCollectionForSearchingDirectoriesGenerally(vstrStringOfFileInfomationTypesDelimitedByComma, _
                vstrRawWildCardsByDelimitedChar)
    
        ' using VBA.Dir
        Set objFSSearchResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(vstrParentDirectoryPath, objFSSearchCondition)
    End If

    Set GetDirectoriesListsCol = objFSSearchResult.ResultDataTableCol
End Function



'**---------------------------------------------
'** Search file-paths simply
'**---------------------------------------------
'''
'''
'''
Public Function GetFilePathsUsingVbaDir(ByRef rstrParentDirectoryPath As String, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "FullPath", _
        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = -1, _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As Collection

    Dim objFSSearchCondition As FileSysSearchCondition, objSearchedResult As FileSysSearchResult, objDTCol As Collection


    Set objFSSearchCondition = GetFSSearchConditionAsCollectionForSearchingFilesGenerally(vstrStringOfFileInfomationTypesDelimitedByComma, _
            vstrRawWildCardsByDelimitedChar, _
            vintMinimumHierarchicalOrderToSearch, _
            vintMaximumHierarchicalOrderToSearch, _
            vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma)

    'use VBA.Dir
    Set objSearchedResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(rstrParentDirectoryPath, objFSSearchCondition)
    
    If Not objSearchedResult.ResultDataTableCol Is Nothing Then
     
        Set objDTCol = objSearchedResult.ResultDataTableCol
    Else
        Set objDTCol = New Collection
    End If

    Set GetFilePathsUsingVbaDir = objDTCol
End Function

'''
'''
'''
Public Function GetDirectoryPathsUsingVbaDir(ByRef rstrParentDirectoryPath As String, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = 0, _
        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = 2, _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As Collection

    Dim objFSSearchCondition As FileSysSearchCondition, objSearchedResult As FileSysSearchResult, objDTCol As Collection

    Set objFSSearchCondition = New FileSysSearchCondition
    
    With objFSSearchCondition
    
        .FileInfomationTypesDelimitedByComma = "FullPath"
    
        .MinimumHierarchicalOrderToSearch = vintMinimumHierarchicalOrderToSearch
        
        .MaximumHierarchicalOrderToSearch = vintMaximumHierarchicalOrderToSearch
    
        .RawWildCardsByDelimitedChar = vstrRawWildCardsByDelimitedChar
    
        .IsDirectoryInfoNeeded = True
        
        .IsFileInfoNeeded = False
        
        .SearchingExceptionFileOrDirectoryNamesDelimitedByComma = vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma
    End With

    'use VBA.Dir
    Set objSearchedResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(rstrParentDirectoryPath, objFSSearchCondition)

    If Not objSearchedResult.ResultDataTableCol Is Nothing Then
     
        Set objDTCol = objSearchedResult.ResultDataTableCol
    Else
        Set objDTCol = New Collection
    End If

    Set GetDirectoryPathsUsingVbaDir = objDTCol
End Function

'''
'''
'''
Public Function GetFilePathsUsingVbaDirWithoutSearchingSubDirectories(ByRef rstrParentDirectoryPath As String, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vstrStringOfFileInfomationTypesDelimitedByComma As String = "FullPath", _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As Collection


    Set GetFilePathsUsingVbaDirWithoutSearchingSubDirectories = GetFilePathsUsingVbaDir(rstrParentDirectoryPath, _
            vstrRawWildCardsByDelimitedChar, _
            vstrStringOfFileInfomationTypesDelimitedByComma, _
            0, _
            0, _
            vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma)
End Function


'''
'''
'''
Public Function GetDirectoryPathsUsingVbaDirWithoutSearchingSubDirectoriesOfFoundDirectories(ByRef rstrParentDirectoryPath As String, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As Collection


    Set GetDirectoryPathsUsingVbaDirWithoutSearchingSubDirectoriesOfFoundDirectories = GetDirectoryPathsUsingVbaDir(rstrParentDirectoryPath, _
            vstrRawWildCardsByDelimitedChar, _
            0, _
            0, _
            vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma)
End Function


'''
'''
'''
Public Function GetDirectoryPathsForAllHarddrivesUsingVbaDir(Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = 0, _
        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = 2, _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As Collection

    Dim varDriveLetter As Variant, strDrivePath As String
    Dim objFSSearchCondition As FileSysSearchCondition, objSearchedResult As FileSysSearchResult, objDTCol As Collection
    Dim objAllDirs As Collection

    Set objFSSearchCondition = New FileSysSearchCondition

    With objFSSearchCondition
    
        .FileInfomationTypesDelimitedByComma = "FullPath"
    
        .MinimumHierarchicalOrderToSearch = vintMinimumHierarchicalOrderToSearch
        
        .MaximumHierarchicalOrderToSearch = vintMaximumHierarchicalOrderToSearch
    
        .RawWildCardsByDelimitedChar = vstrRawWildCardsByDelimitedChar
    
        .IsDirectoryInfoNeeded = True
        
        .IsFileInfoNeeded = False
        
        .SearchingExceptionFileOrDirectoryNamesDelimitedByComma = vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma
    End With

    Set objAllDirs = New Collection

    For Each varDriveLetter In GetLocalHardDriveLettersInThisPC()
    
        strDrivePath = varDriveLetter & ":\"
    
        'use VBA.Dir
        Set objSearchedResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(strDrivePath, objFSSearchCondition)
    
        UnionDoubleCollectionsToSingle objAllDirs, objSearchedResult.ResultDataTableCol
    Next

    Set GetDirectoryPathsForAllHarddrivesUsingVbaDir = objAllDirs
End Function


'''
'''
'''
Public Function GetFilePathsForAllHarddrivesUsingVbaDir(Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = 0, _
        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = 2, _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "") As Collection

    Dim varDriveLetter As Variant, strDrivePath As String
    Dim objFSSearchCondition As FileSysSearchCondition, objSearchedResult As FileSysSearchResult, objDTCol As Collection
    Dim objAllDirs As Collection

    Set objFSSearchCondition = New FileSysSearchCondition

    With objFSSearchCondition
    
        .FileInfomationTypesDelimitedByComma = "FullPath"
    
        .MinimumHierarchicalOrderToSearch = vintMinimumHierarchicalOrderToSearch
        
        .MaximumHierarchicalOrderToSearch = vintMaximumHierarchicalOrderToSearch
    
        .RawWildCardsByDelimitedChar = vstrRawWildCardsByDelimitedChar
    
        .IsDirectoryInfoNeeded = False
        
        .IsFileInfoNeeded = True
        
        .SearchingExceptionFileOrDirectoryNamesDelimitedByComma = vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma
    End With

    Set objAllDirs = New Collection

    For Each varDriveLetter In GetLocalHardDriveLettersInThisPC()
    
        strDrivePath = varDriveLetter & ":\"
    
        'use VBA.Dir
        Set objSearchedResult = GetFileSysSearchedResultCollectionByConditionUsingVbaDir(strDrivePath, objFSSearchCondition)
    
        UnionDoubleCollectionsToSingle objAllDirs, objSearchedResult.ResultDataTableCol
    Next

    Set GetFilePathsForAllHarddrivesUsingVbaDir = objAllDirs
End Function


'**---------------------------------------------
'** Modify path
'**---------------------------------------------
'''
'''
'''
Public Function GetPathByChagingFileExtension(ByVal vstrPath As String, ByVal vstrNewFileExtension As String) As String

    Dim strDir As String, strBaseName As String

    GetParentDirectoryPathToArgByVbaDir strDir, vstrPath

    strBaseName = GetBaseNameByVbaDir(vstrPath)
    
    GetPathByChagingFileExtension = strDir & "\" & strBaseName & "." & vstrNewFileExtension
End Function

'''
'''
'''
Public Sub GetExtractedRightSideRelativePath(ByRef rstrRelativePath As String, ByRef rstrFullPath As String, ByRef rstrLeftSideDictionaryPath As String)

    If rstrLeftSideDictionaryPath <> "" Then
    
        rstrRelativePath = Right(rstrFullPath, Len(rstrFullPath) - Len(rstrLeftSideDictionaryPath) - 1)
    Else
        rstrRelativePath = rstrFullPath
    End If
End Sub

'''
''' for cmd.exe, get a relative-path
'''
Public Function GetShellRelativePath(ByRef rstrFullPath As String, ByRef rstrLeftSideDictionaryPath As String) As String

    Dim strRelativePath As String
    
    GetExtractedRightSideRelativePath strRelativePath, rstrFullPath, rstrLeftSideDictionaryPath
            
    strRelativePath = """.\" & strRelativePath & """"

    GetShellRelativePath = strRelativePath
End Function


'**---------------------------------------------
'** Creating directory in this computer and network drive
'**---------------------------------------------
'''
''' using VBA.Dir, which doesn't check drive existence
'''
Public Function ForceToCreateDirectoryByVbaDir(ByVal vstrDirPath As String) As Long
    
    Dim intRet As Long
    Dim strCurrentDir As String, strNextDir As String, objNotExistdDirs As Collection, i As Long
    
    intRet = 0
    
    On Error GoTo ErrHandler
    
    If VBA.Dir(vstrDirPath, vbDirectory) <> "" Then
    
        ' The directory exists
    
        intRet = 0
        
        ForceToCreateDirectoryByVbaDir = intRet
        
        Exit Function
    End If
    
    GetParentDirectoryPathToArgByVbaDir strCurrentDir, vstrDirPath
    
    Set objNotExistdDirs = New Collection
    
    objNotExistdDirs.Add vstrDirPath
    
    Do While VBA.Dir(strCurrentDir, vbDirectory) = ""
    
        objNotExistdDirs.Add strCurrentDir
        
        GetParentDirectoryPathToArgByVbaDir strNextDir, strCurrentDir
    
        strCurrentDir = strNextDir
    Loop
    
    With objNotExistdDirs
    
        For i = .Count To 1 Step -1
        
            VBA.MkDir .Item(i)
        Next
    End With
        
    ForceToCreateDirectoryByVbaDir = intRet
    
    Exit Function
    
ErrHandler:

    Debug.Assert False

    Debug.Print Err.Description
    
    ForceToCreateDirectoryByVbaDir = 2
End Function



'**---------------------------------------------
'** List up files path using both FileSysSearchCondition and FileSysSearchResult
'**---------------------------------------------
'''
''' get file (or directory) list as Collection
'''
Public Function GetFileSysSearchedResultCollectionByConditionUsingVbaDir(ByRef rstrParentDirectoryPath As String, _
        ByVal vobjFileSysSearchCondition As FileSysSearchCondition) As FileSysSearchResult
        
    Dim objCol As Collection, objSearchedResult As FileSysSearchResult

    With New DoubleStopWatch

        .MeasureStart
    
        With vobjFileSysSearchCondition
        
            .SearchedDataTableType = ExistedAsCollectionObjectInMemory
        
            .ParentDirectoryPath = rstrParentDirectoryPath
        End With
        
        ' use VBA.Dir function
        GetVBADirFileDirectoryInfoFromFileSysSearchCondition objCol, Nothing, vobjFileSysSearchCondition
        
        Set objSearchedResult = New FileSysSearchResult
        
        .MeasureInterval
        
        objSearchedResult.SetFileSystemSearchedResultDataTable objCol, vobjFileSysSearchCondition, .ElapsedSecondTime
    End With
    
    Set GetFileSysSearchedResultCollectionByConditionUsingVbaDir = objSearchedResult
End Function

'''
''' get file (or directory) list as Dictionary
'''
Public Function GetFileSysSearchedResultDictionaryByConditionUsingVbaDir(ByRef rstrParentDirectoryPath As String, _
        ByVal vobjFileSysSearchCondition As FileSysSearchCondition) As FileSysSearchResult
        
    Dim objDic As Scripting.Dictionary, objSearchedResult As FileSysSearchResult

    With New DoubleStopWatch

        .MeasureStart
    
        With vobjFileSysSearchCondition
        
            .SearchedDataTableType = ExistedAsDictionaryObjectInMemory
        
            .ParentDirectoryPath = rstrParentDirectoryPath
        End With
        
        ' use VBA.Dir function
        GetVBADirFileDirectoryInfoFromFileSysSearchCondition Nothing, objDic, vobjFileSysSearchCondition
        
        Set objSearchedResult = New FileSysSearchResult
        
        .MeasureInterval
        
        objSearchedResult.SetFileSystemSearchedResultDataTable objDic, vobjFileSysSearchCondition, .ElapsedSecondTime
    End With
    
    Set GetFileSysSearchedResultDictionaryByConditionUsingVbaDir = objSearchedResult
End Function


'''
'''
''
Public Sub GetVBADirFileDirectoryInfoFromFileSysSearchCondition(ByRef robjDataTableCol As Collection, _
        ByRef robjDataTableDic As Scripting.Dictionary, _
        ByVal vobjFileSysSearchCondition As FileSysSearchCondition)
                        
    Dim objNecessaryFileInfoTypeCol As Collection, strFilePath As String, strDirectoryPathWithSuffixBackSlash As String
    Dim blnIsWildCardsRestricted As Boolean, strWildCardArray() As String, blnIsFileOrDirectoryExceptionListEnabled As Boolean, objFileOrDirectoryExceptionListKeys As Scripting.Dictionary
    
    Dim blnPutInCollection As Boolean, blnPutInDictionary As Boolean
    
    With vobjFileSysSearchCondition
    
        GetWildCardsFromExtensionsInfoAndRawWildCards blnIsWildCardsRestricted, strWildCardArray, .ExtensionTextByDelimitedChar, .RawWildCardsByDelimitedChar, .DelimitedChar
    
        Set objNecessaryFileInfoTypeCol = GetFileGeneralInfoColFromLineDelimitedChar(.FileInfomationTypesDelimitedByComma)
        
        strDirectoryPathWithSuffixBackSlash = GetDirectoryPathWithAddingLastBackSlashIfThePathHasNoIt(.ParentDirectoryPath)
        
        GetPutInDataTypeFromInMemoryDataTableType blnPutInCollection, blnPutInDictionary, .SearchedDataTableType
        
        ' file or directory name exception list
        GetFileOrDirectoryNameExceptionListCondition blnIsFileOrDirectoryExceptionListEnabled, objFileOrDirectoryExceptionListKeys, .SearchingExceptionFileOrDirectoryNamesDelimitedByComma
        
        msubGetVBADirFileDirectoryInfoWithDetailParamsRecursively robjDataTableCol, robjDataTableDic, strDirectoryPathWithSuffixBackSlash, objNecessaryFileInfoTypeCol, blnIsWildCardsRestricted, strWildCardArray, _
                                 .MinimumHierarchicalOrderToSearch, .MaximumHierarchicalOrderToSearch, .CurrentHierarchicalOrder, .RelativeParentDirectoryPath, blnPutInCollection, blnPutInDictionary, .IsFileInfoNeeded, .IsDirectoryInfoNeeded, GetFileSystemObjectIfItNeededBy(objNecessaryFileInfoTypeCol), blnIsFileOrDirectoryExceptionListEnabled, objFileOrDirectoryExceptionListKeys
    End With
End Sub

'**---------------------------------------------
'** List up files path from specified directory path
'**---------------------------------------------
'''
''' about files, get data table Collection of RelativeFullPath
'''
Public Function GetRelativeFilePathColByVBADir(ByVal vstrParentDirectoryPath As String) As Collection

    Dim objCol As Collection
    
    GetVBADirFileDirectoryInfoByExtensionsDelimited objCol, Nothing, vstrParentDirectoryPath, "RelativeFullPath", "*", "*", ",", True, True, False, True, False

    Set GetRelativeFilePathColByVBADir = objCol
End Function

'''
''' about directories, get data table Collection of RelativeFullPath
'''
Public Function GetRelativeDirectoryPathColByVBADir(ByVal vstrParentDirectoryPath As String) As Collection

    Dim objCol As Collection
    
    GetVBADirFileDirectoryInfoByExtensionsDelimited objCol, Nothing, vstrParentDirectoryPath, "RelativeFullPath", "*", "*", ",", True, True, False, False, True

    Set GetRelativeDirectoryPathColByVBADir = objCol
End Function


'''
''' about files, get data table Collection of FileBaseName, FileExtension, RelativeDirectoryPath, LastModifiedDate, FullPath
'''
Public Function GetFileAttributeInfoColOfDefaultCombinationByVBADir(ByVal vstrParentDirectoryPath As String) As Collection

    Dim objCol As Collection
    
    GetVBADirFileDirectoryInfoByExtensionsDelimited objCol, Nothing, vstrParentDirectoryPath, "FileBaseName, FileExtension, RelativeDirectoryPath, LastModifiedDate, FullPath", "*", "*", ",", True, True, False, True, True

    Set GetFileAttributeInfoColOfDefaultCombinationByVBADir = objCol
End Function


'''
''' about files, get data table Dictionary, which Key is RelativeFullPath and Value is all 0
'''
Public Function GetRelativeFilePathOnlyKeyDicByVBADir(ByVal vstrParentDirectoryPath As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    GetVBADirFileDirectoryInfoByExtensionsDelimited Nothing, objDic, vstrParentDirectoryPath, "RelativeFullPath", "*", "*", ",", True, False, True, True, False

    Set GetRelativeFilePathOnlyKeyDicByVBADir = objDic
End Function

'''
''' about files, get data table Dictionary, which Key is RelativeFullPath and Value is LastModifiedDate
'''
Public Function GetRelativeFilePathAndLastModifedDateDicByVBADir(ByVal vstrParentDirectoryPath As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    GetVBADirFileDirectoryInfoByExtensionsDelimited Nothing, objDic, vstrParentDirectoryPath, "RelativeFullPath,LastModifiedDate", "*", "*", ",", True, False, True, True, False

    Set GetRelativeFilePathAndLastModifedDateDicByVBADir = objDic
End Function


'''
''' about directories, get data table Dictionary, which Key is RelativeFullPath and Value is LastModifiedDate
'''
Public Function GetRelativeDirectoryPathAndLastModifedDateDicByVBADir(ByVal vstrParentDirectoryPath As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    GetVBADirFileDirectoryInfoByExtensionsDelimited Nothing, objDic, vstrParentDirectoryPath, "RelativeFullPath,LastModifiedDate", "*", "*", ",", True, False, True, False, True

    Set GetRelativeDirectoryPathAndLastModifedDateDicByVBADir = objDic
End Function


'''
''' about files, get data table Dictionary, which Key is RelativeFullPath and values collection are FileBaseName, FileExtension, RelativeDirectoryPath, LastModifiedDate, FullPath
'''
Public Function GetRelativeFilePathAndFileAttributeInfoColOfDefaultCombinationDicByVBADir(ByVal vstrParentDirectoryPath As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    GetVBADirFileDirectoryInfoByExtensionsDelimited Nothing, objDic, vstrParentDirectoryPath, "RelativeFullPath, FileBaseName, FileExtension, RelativeDirectoryPath, LastModifiedDate, FullPath", "*", "*", ",", True, False, True, True, False

    Set GetRelativeFilePathAndFileAttributeInfoColOfDefaultCombinationDicByVBADir = objDic
End Function



'**---------------------------------------------
'** Core operation to list up files path from specified directory path
'**---------------------------------------------
'''
''' This is slower than the FindFirstFileEx API method
'''
''' <Argument>robjDataTableCol: Output</Argument>
''' <Argument>robjDataTableDic: Output</Argument>
''' <Argument>vstrDirectoryPath: Input</Argument>
''' <Argument>vstrFileInfomationTypesDelimitedByComma: Input</Argument>
''' <Argument>vstrExtensionTextByDelimitedChar: Input</Argument>
''' <Argument>vstrRawWildCardsByDelimitedChar: Input</Argument>
''' <Argument>vstrDelimitedChar: Input</Argument>
''' <Argument>vblnAllowToSearchSubDirectories: Input</Argument>
''' <Argument>vblnPutInCollection: Input</Argument>
''' <Argument>vblnPutInDictionary: Input</Argument>
''' <Argument>vblnIsFileInfoNeeded: Input</Argument>
''' <Argument>vblnIsDirectoryInfoNeeded: Input</Argument>
''' <Argument>vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma: Input</Argument>
Public Sub GetVBADirFileDirectoryInfoByExtensionsDelimited(ByRef robjDataTableCol As Collection, _
        ByRef robjDataTableDic As Scripting.Dictionary, _
        ByVal vstrDirectoryPath As String, _
        Optional ByVal vstrFileInfomationTypesDelimitedByComma As String = "RelativeFullPath", _
        Optional ByVal vstrExtensionTextByDelimitedChar As String = "*", _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*", _
        Optional ByVal vstrDelimitedChar As String = ",", _
        Optional ByVal vblnAllowToSearchSubDirectories As Boolean = False, _
        Optional ByVal vblnPutInCollection As Boolean = True, _
        Optional ByVal vblnPutInDictionary As Boolean = False, _
        Optional ByVal vblnIsFileInfoNeeded As Boolean = False, _
        Optional ByVal vblnIsDirectoryInfoNeeded As Boolean = False, _
        Optional ByVal vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma As String = "")
                        
                 
    Dim objNecessaryFileInfoTypeCol As Collection, strFilePath As String, strDirectoryPathWithSuffixBackSlash As String
    
    Dim blnIsWildCardsRestricted As Boolean, strWildCardArray() As String, blnIsFileOrDirectoryExceptionListEnabled As Boolean, objFileOrDirectoryExceptionListKeys As Scripting.Dictionary
    
    
    GetWildCardsFromExtensionsInfoAndRawWildCards blnIsWildCardsRestricted, _
            strWildCardArray, _
            vstrExtensionTextByDelimitedChar, _
            vstrRawWildCardsByDelimitedChar, _
            vstrDelimitedChar

    Set objNecessaryFileInfoTypeCol = GetFileGeneralInfoColFromLineDelimitedChar(vstrFileInfomationTypesDelimitedByComma)

    strDirectoryPathWithSuffixBackSlash = GetDirectoryPathWithAddingLastBackSlashIfThePathHasNoIt(vstrDirectoryPath)


    ' file or directory name exception list
    GetFileOrDirectoryNameExceptionListCondition blnIsFileOrDirectoryExceptionListEnabled, objFileOrDirectoryExceptionListKeys, vstrSearchingExceptionFileOrDirectoryNamesDelimitedByComma


    msubGetVBADirFileDirectoryInfoWithAllSubDirSearch robjDataTableCol, _
            robjDataTableDic, _
            strDirectoryPathWithSuffixBackSlash, _
            objNecessaryFileInfoTypeCol, _
            blnIsWildCardsRestricted, _
            strWildCardArray, _
            vblnAllowToSearchSubDirectories, "", _
            vblnPutInCollection, _
            vblnPutInDictionary, _
            vblnIsFileInfoNeeded, _
            vblnIsDirectoryInfoNeeded, _
            GetFileSystemObjectIfItNeededBy(objNecessaryFileInfoTypeCol), _
            blnIsFileOrDirectoryExceptionListEnabled, _
            objFileOrDirectoryExceptionListKeys
End Sub


'''
'''
'''
''' <Argument>robjDataTableCol: Output</Argumnet>
''' <Argument>robjDataTableDic: Output</Argumnet>
''' <Argument>vstrDirectoryPath: Input</Argumnet>
''' <Argument>vobjNecessaryFileInfoTypeCol: Input</Argumnet>
''' <Argument>vblnIsWildCardsRestricted: Input</Argumnet>
''' <Argument>strMergedWildCardArray: Input</Argumnet>
''' <Argument>vblnAllowToSearchSubDirectories: Input</Argumnet>
''' <Argument>vstrRelativeParentDirectoryPath: Input</Argumnet>
''' <Argument>vblnPutInCollection: Input</Argumnet>
''' <Argument>vblnPutInDictionary: Input</Argumnet>
''' <Argument>vblnIsFileInfoNeeded: Input</Argumnet>
''' <Argument>vblnIsDirectoryInfoNeeded: Input</Argumnet>
''' <Argument>vobjFS: Input</Argumnet>
''' <Argument>vblnIsFileOrDirectoryExceptionListEnabled: Input</Argumnet>
''' <Argument>vobjFileOrDirectoryExceptionListKeys: Input</Argument>
Private Sub msubGetVBADirFileDirectoryInfoWithAllSubDirSearch(ByRef robjDataTableCol As Collection, _
        ByRef robjDataTableDic As Scripting.Dictionary, _
        ByVal vstrDirectoryPath As String, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection, _
        ByVal vblnIsWildCardsRestricted As Boolean, _
        ByRef rstrMergedWildCardArray() As String, _
        Optional ByVal vblnAllowToSearchSubDirectories As Boolean = False, _
        Optional ByVal vstrRelativeParentDirectoryPath As String = "", _
        Optional ByVal vblnPutInCollection As Boolean = True, _
        Optional ByVal vblnPutInDictionary As Boolean = False, _
        Optional ByVal vblnIsFileInfoNeeded As Boolean = True, _
        Optional ByVal vblnIsDirectoryInfoNeeded As Boolean = False, _
        Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing, _
        Optional ByVal vblnIsFileOrDirectoryExceptionListEnabled As Boolean = False, _
        Optional ByVal vobjFileOrDirectoryExceptionListKeys As Scripting.Dictionary = Nothing)
                        

    If vblnAllowToSearchSubDirectories Then
    
        msubGetVBADirFileDirectoryInfoWithDetailParamsRecursively robjDataTableCol, robjDataTableDic, vstrDirectoryPath, vobjNecessaryFileInfoTypeCol, _
                            vblnIsWildCardsRestricted, rstrMergedWildCardArray, _
                            -1, -1, 0, _
                            vstrRelativeParentDirectoryPath, vblnPutInCollection, vblnPutInDictionary, vblnIsFileInfoNeeded, vblnIsDirectoryInfoNeeded, vobjFS, _
                            vblnIsFileOrDirectoryExceptionListEnabled, vobjFileOrDirectoryExceptionListKeys
    Else
    
        msubGetVBADirFileDirectoryInfoWithDetailParamsRecursively robjDataTableCol, robjDataTableDic, vstrDirectoryPath, vobjNecessaryFileInfoTypeCol, _
                            vblnIsWildCardsRestricted, rstrMergedWildCardArray, _
                            0, 0, 0, _
                            vstrRelativeParentDirectoryPath, vblnPutInCollection, vblnPutInDictionary, vblnIsFileInfoNeeded, vblnIsDirectoryInfoNeeded, vobjFS, _
                            vblnIsFileOrDirectoryExceptionListEnabled, vobjFileOrDirectoryExceptionListKeys
    End If
End Sub


'**---------------------------------------------
'** A base procedure for searching files or directories using VBA.Dir function
'**---------------------------------------------
'''
''' use VBA.Dir, which is slower than Windows API FineNextFile method
''' VBA.Dir doesn't support the file name which including kind of only Unicode character-set, doesn't include ANSI set
'''
''' In order to get result speedily, this doesn't include user-defined classes, this uses only primitive data type except the Scripting.Dictinary.
''' Because this is almost called recursively.
'''
Private Sub msubGetVBADirFileDirectoryInfoWithDetailParamsRecursively(ByRef robjDataTableCol As Collection, ByRef robjDataTableDic As Scripting.Dictionary, _
                        ByVal vstrDirectoryPath As String, ByVal vobjNecessaryFileInfoTypeCol As Collection, _
                        ByVal vblnIsWildCardsRestricted As Boolean, ByRef rstrMergedWildCardArray() As String, _
                        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = 0, _
                        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = 0, _
                        Optional ByVal vintCurrentHierarchicalOrder As Long = 0, _
                        Optional ByVal vstrRelativeParentDirectoryPath As String = "", _
                        Optional ByVal vblnPutInCollection As Boolean = True, Optional ByVal vblnPutInDictionary As Boolean = False, _
                        Optional ByVal vblnIsFileInfoNeeded As Boolean = True, Optional ByVal vblnIsDirectoryInfoNeeded As Boolean = False, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing, _
                        Optional ByVal vblnIsFileOrDirectoryExceptionListEnabled As Boolean = False, Optional ByVal vobjFileOrDirectoryExceptionListKeys As Scripting.Dictionary = Nothing)
                        

    Dim objDirectoryNamesDic As Scripting.Dictionary, objFileNames As Collection
    
    Dim varChildDirectoryName As Variant, enmFileAttributes As VBA.VbFileAttribute, i As Long
    

    ' vbVolume, vbAlias occurs error

    'enmFileAttributes = vbNormal Or vbReadOnly Or vbDirectory Or vbHidden Or vbVolume Or vbArchive Or vbAlias

    'enmFileAttributes = vbNormal Or vbReadOnly Or vbDirectory Or vbHidden Or vbArchive
    
    enmFileAttributes = vbNormal Or vbReadOnly Or vbDirectory
    
    For i = LBound(rstrMergedWildCardArray) To UBound(rstrMergedWildCardArray)
    
    
        msubGetFileOrDirectoryFromVBADirAndSetOneElementOfEitherDTColOrDTDic robjDataTableCol, robjDataTableDic, objFileNames, objDirectoryNamesDic, _
                                    vstrDirectoryPath, vobjNecessaryFileInfoTypeCol, _
                                    rstrMergedWildCardArray(i), enmFileAttributes, _
                                    vintMinimumHierarchicalOrderToSearch, vintMaximumHierarchicalOrderToSearch, _
                                    vintCurrentHierarchicalOrder, vstrRelativeParentDirectoryPath, vblnPutInCollection, vblnPutInDictionary, _
                                    vblnIsFileInfoNeeded, vblnIsDirectoryInfoNeeded, vobjFS, _
                                    vblnIsFileOrDirectoryExceptionListEnabled, vobjFileOrDirectoryExceptionListKeys
    Next

    If vblnIsWildCardsRestricted Then
    
        ' need to search only sub-directory in order to recursive call this-function-self
    
        If vintMaximumHierarchicalOrderToSearch = -1 Or vintMaximumHierarchicalOrderToSearch > vintCurrentHierarchicalOrder Then
        
            'enmFileAttributes = vbNormal Or vbReadOnly Or vbDirectory Or vbHidden Or vbVolume Or vbArchive Or vbAlias
            
            enmFileAttributes = vbNormal Or vbReadOnly Or vbDirectory Or vbHidden Or vbArchive
        
            msubGetFileOrDirectoryFromVBADirAndSetOneElementOfEitherDTColOrDTDic robjDataTableCol, robjDataTableDic, objFileNames, objDirectoryNamesDic, _
                                    vstrDirectoryPath, vobjNecessaryFileInfoTypeCol, _
                                    "*", enmFileAttributes, _
                                    vintMinimumHierarchicalOrderToSearch, vintMaximumHierarchicalOrderToSearch, _
                                    vintCurrentHierarchicalOrder, vstrRelativeParentDirectoryPath, vblnPutInCollection, vblnPutInDictionary, _
                                    vblnIsFileInfoNeeded, vblnIsDirectoryInfoNeeded, vobjFS, _
                                    vblnIsFileOrDirectoryExceptionListEnabled, vobjFileOrDirectoryExceptionListKeys, _
                                    True
            
        End If
    End If

    

    If vblnIsDirectoryInfoNeeded And (Not objDirectoryNamesDic Is Nothing) Then
    
        msubSetDirectoryInfoToDataTableIfItNeeded robjDataTableCol, robjDataTableDic, _
                                        objDirectoryNamesDic, vstrDirectoryPath, vobjNecessaryFileInfoTypeCol, vblnIsWildCardsRestricted, rstrMergedWildCardArray, vstrRelativeParentDirectoryPath, _
                                        vblnPutInCollection, vblnPutInDictionary, _
                                        vblnIsFileInfoNeeded, vblnIsDirectoryInfoNeeded, vobjFS
    
    End If

    If (vintMaximumHierarchicalOrderToSearch = -1 Or vintMaximumHierarchicalOrderToSearch > vintCurrentHierarchicalOrder) And (Not objDirectoryNamesDic Is Nothing) Then
        
        For Each varChildDirectoryName In objDirectoryNamesDic.Keys
            
            ' recursive call
            msubGetVBADirFileDirectoryInfoWithDetailParamsRecursively robjDataTableCol, robjDataTableDic, vstrDirectoryPath & varChildDirectoryName & "\", vobjNecessaryFileInfoTypeCol, _
                            vblnIsWildCardsRestricted, rstrMergedWildCardArray, vintMinimumHierarchicalOrderToSearch, vintMaximumHierarchicalOrderToSearch, vintCurrentHierarchicalOrder + 1, vstrRelativeParentDirectoryPath & varChildDirectoryName & "\", vblnPutInCollection, vblnPutInDictionary, vblnIsFileInfoNeeded, vblnIsDirectoryInfoNeeded, vobjFS, vblnIsFileOrDirectoryExceptionListEnabled, vobjFileOrDirectoryExceptionListKeys
        
        Next
    End If
End Sub


'''
'''
'''
''' <Argument>robjDataTableCol: Output</Argument>
''' <Argument>robjDataTableDic: Output</Argument>
''' <Argument>robjDirectoryNamesDic: Input</Argument>
''' <Argument>vstrDirectoryPath</Argument>
Private Sub msubSetDirectoryInfoToDataTableIfItNeeded(ByRef robjDataTableCol As Collection, _
                                                ByRef robjDataTableDic As Scripting.Dictionary, _
                                                ByRef robjDirectoryNamesDic As Scripting.Dictionary, _
                                                ByVal vstrDirectoryPath As String, _
                                                ByRef robjNecessaryFileInfoTypeCol As Collection, _
                                                ByVal vblnIsWildCardsRestricted As Boolean, _
                                                ByRef rstrMergedWildCardArray() As String, _
                                                Optional ByVal vstrRelativeParentDirectoryPath As String = "", _
                                                Optional ByVal vblnPutInCollection As Boolean = True, _
                                                Optional ByVal vblnPutInDictionary As Boolean = False, _
                                                Optional ByVal vblnIsFileInfoNeeded As Boolean = True, _
                                                Optional ByVal vblnIsDirectoryInfoNeeded As Boolean = False, _
                                                Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing)
    
    Dim varChildDirectoryName As Variant
    Dim varKey As Variant, varValueCollectionOrNotObjectData As Variant, blnAllowToAdd As Boolean, varWildCard As Variant
    
    For Each varChildDirectoryName In robjDirectoryNamesDic.Keys
    
        If vblnIsWildCardsRestricted Then
            
            blnAllowToAdd = False
        
            For Each varWildCard In rstrMergedWildCardArray
            
                If LCase(varChildDirectoryName) Like LCase(varWildCard) Then
                
                    blnAllowToAdd = True
                    
                    Exit For
                End If
            Next
        Else
            blnAllowToAdd = True
        End If
    
        If blnAllowToAdd Then
        
            ' Data-table collection
            If vblnPutInCollection Then
            
                If robjDataTableCol Is Nothing Then Set robjDataTableCol = New Collection
            
                ' Last Update Date VBA.FileDateTime function
                robjDataTableCol.Add mfvarGetRowColOfFileGeneralInfoByVBADir(varChildDirectoryName, False, vstrDirectoryPath, vstrRelativeParentDirectoryPath, robjNecessaryFileInfoTypeCol, vobjFS)
            End If
            
            ' Data-table Scripting.Dictionary
            If vblnPutInDictionary Then
                
                If robjDataTableDic Is Nothing Then Set robjDataTableDic = New Scripting.Dictionary
                
                msubGetKeyAndValueOfFileGeneralInfoByVBADir varKey, varValueCollectionOrNotObjectData, varChildDirectoryName, False, vstrDirectoryPath, vstrRelativeParentDirectoryPath, robjNecessaryFileInfoTypeCol, vobjFS
                
                With robjDataTableDic
                
                    If Not .Exists(varKey) Then
                    
                        .Add varKey, varValueCollectionOrNotObjectData
                    End If
                End With
            End If
        End If
    Next
End Sub



'''
'''
'''
Private Sub msubGetFileOrDirectoryFromVBADirAndSetOneElementOfEitherDTColOrDTDic(ByRef robjDataTableCol As Collection, ByRef robjDataTableDic As Scripting.Dictionary, _
                                                            ByRef robjFileNames As Collection, ByRef robjDirectoryNamesDic As Scripting.Dictionary, _
                                                            ByVal vstrDirectoryPath As String, _
                                                            ByRef robjNecessaryFileInfoTypeCol As Collection, _
                                                            ByVal vstrWildCard As String, ByVal venmFileAttributes As VBA.VbFileAttribute, _
                                                            Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = 0, _
                                                            Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = 0, _
                                                            Optional ByVal vintCurrentHierarchicalOrder As Long = 0, _
                                                            Optional ByVal vstrRelativeParentDirectoryPath As String = "", _
                                                            Optional ByVal vblnPutInCollection As Boolean = True, Optional ByVal vblnPutInDictionary As Boolean = False, _
                                                            Optional ByVal vblnIsFileInfoNeeded As Boolean = True, Optional ByVal vblnIsDirectoryInfoNeeded As Boolean = False, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing, _
                                                            Optional ByVal vblnIsFileOrDirectoryExceptionListEnabled As Boolean = False, Optional ByVal vobjFileOrDirectoryExceptionListKeys As Scripting.Dictionary = Nothing, _
                                                            Optional ByVal vblnSuppressCollectingFileInfo As Boolean = False)


    Dim strFileOrDirectoryName As String, blnContinueToSearchByExceptionNames As Boolean
    
    strFileOrDirectoryName = ""
    
    On Error Resume Next
    
    strFileOrDirectoryName = VBA.Dir(vstrDirectoryPath & vstrWildCard, venmFileAttributes)

    If Err.Number <> 0 Then
    
        Debug.Print "VBA.Dir search condition: " & vstrDirectoryPath & vstrWildCard & ", Error description - " & Err.Description & vbTab & " - Probably, the current user doesn't have a file access authority."
    End If

    On Error GoTo 0

    Do While strFileOrDirectoryName <> ""
        
        blnContinueToSearchByExceptionNames = True
        
        If vblnIsFileOrDirectoryExceptionListEnabled Then
            
            If vobjFileOrDirectoryExceptionListKeys.Exists(strFileOrDirectoryName) Then
            
                blnContinueToSearchByExceptionNames = False
            End If
        End If
        
        If blnContinueToSearchByExceptionNames Then
        
            msubSetOneElementOfEitherDTColOrDTDicForVBADir robjDataTableCol, robjDataTableDic, robjFileNames, robjDirectoryNamesDic, _
                                    vstrDirectoryPath, robjNecessaryFileInfoTypeCol, _
                                    strFileOrDirectoryName, vintMinimumHierarchicalOrderToSearch, vintMaximumHierarchicalOrderToSearch, _
                                    vintCurrentHierarchicalOrder, vstrRelativeParentDirectoryPath, vblnPutInCollection, vblnPutInDictionary, _
                                    vblnIsFileInfoNeeded, vblnIsDirectoryInfoNeeded, vobjFS, vblnSuppressCollectingFileInfo
        End If
        
        strFileOrDirectoryName = VBA.Dir()
    Loop
End Sub



'''
'''
'''
Private Sub msubSetOneElementOfEitherDTColOrDTDicForVBADir(ByRef robjDataTableCol As Collection, ByRef robjDataTableDic As Scripting.Dictionary, _
                                                            ByRef robjFileNames As Collection, ByRef robjDirectoryNamesDic As Scripting.Dictionary, _
                                                            ByVal vstrDirectoryPath As String, _
                                                            ByRef robjNecessaryFileInfoTypeCol As Collection, _
                                                            ByVal vstrFileOrDirectoryName As String, _
                                                            Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = 0, _
                                                            Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = 0, _
                                                            Optional ByVal vintCurrentHierarchicalOrder As Long = 0, _
                                                            Optional ByVal vstrRelativeParentDirectoryPath As String = "", _
                                                            Optional ByVal vblnPutInCollection As Boolean = True, Optional ByVal vblnPutInDictionary As Boolean = False, _
                                                            Optional ByVal vblnIsFileInfoNeeded As Boolean = True, Optional ByVal vblnIsDirectoryInfoNeeded As Boolean = False, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing, _
                                                            Optional ByVal vblnSuppressCollectingFileInfo As Boolean = False)

    Dim strPath As String
    Dim varKey As Variant, varValueCollectionOrNotObjectData As Variant
    Dim enmAttr As VBA.VbFileAttribute, blnContinue As Boolean

    If vstrFileOrDirectoryName <> "." And vstrFileOrDirectoryName <> ".." Then
        
        strPath = vstrDirectoryPath & vstrFileOrDirectoryName
        
        enmAttr = 0: blnContinue = True
        
        On Error Resume Next
        
        enmAttr = VBA.GetAttr(strPath)
        
        If Err.Number <> 0 Then
        
            If Len(strPath) > 255 Then
            
                Debug.Print "VBA.GetAttr error: " & Err.Description & " - Probably, the file path is too long, which length is " & Len(strPath)
            Else
                Debug.Print "VBA.GetAttr error: " & Err.Description
            End If
            
            blnContinue = False
        End If
        
        On Error GoTo 0
        
        If blnContinue Then
        
            If Not ((VBA.GetAttr(strPath) And vbDirectory) > 0) Then
                
                If Not vblnSuppressCollectingFileInfo Then
                
                    If vblnIsFileInfoNeeded And (vintMinimumHierarchicalOrderToSearch = -1 Or vintMinimumHierarchicalOrderToSearch <= vintCurrentHierarchicalOrder) Then
                        
                        If robjFileNames Is Nothing Then Set robjFileNames = New Collection
                        
                        If vblnIsFileInfoNeeded Then
                        
                            robjFileNames.Add strPath
                            
                            ' Data-table collection
                            If vblnPutInCollection Then
                                
                                If robjDataTableCol Is Nothing Then Set robjDataTableCol = New Collection
                            
                                robjDataTableCol.Add mfvarGetRowColOfFileGeneralInfoByVBADir(vstrFileOrDirectoryName, True, vstrDirectoryPath, vstrRelativeParentDirectoryPath, robjNecessaryFileInfoTypeCol, vobjFS)
                            End If
                            
                            ' Data-table Scripting.Dictionary
                            If vblnPutInDictionary Then
                                
                                If robjDataTableDic Is Nothing Then Set robjDataTableDic = New Scripting.Dictionary
                                
                                msubGetKeyAndValueOfFileGeneralInfoByVBADir varKey, varValueCollectionOrNotObjectData, vstrFileOrDirectoryName, True, vstrDirectoryPath, vstrRelativeParentDirectoryPath, robjNecessaryFileInfoTypeCol, vobjFS
                        
                                With robjDataTableDic
                        
                                    If Not .Exists(varKey) Then
                                    
                                        .Add varKey, varValueCollectionOrNotObjectData
                                    End If
                                End With
                            End If
                        End If
                    End If
                End If
            Else
                ' Directory, if rstrMergedWildCardArray(i) is "*", then the following codes are executed, which the vblnIsWildCardsRestricted should have been false.
                
                Select Case True
                
                    Case (vintMaximumHierarchicalOrderToSearch = -1 Or vintMaximumHierarchicalOrderToSearch > vintCurrentHierarchicalOrder), (vblnIsDirectoryInfoNeeded And (vintMinimumHierarchicalOrderToSearch = -1 Or vintMinimumHierarchicalOrderToSearch <= vintCurrentHierarchicalOrder))
                
                        ' Note: (vintMaximumHierarchicalOrderToSearch = -1 Or vintMaximumHierarchicalOrderToSearch > vintCurrentHierarchicalOrder) stands for necessity of searching sub-directory
                        
                        ' Note: (vblnIsDirectoryInfoNeeded And (vintMinimumHierarchicalOrderToSearch = -1 Or vintMinimumHierarchicalOrderToSearch <= vintCurrentHierarchicalOrder)) stands for adding to directory-info to returning either Collection or Scripting.Dictionary
                
                        If robjDirectoryNamesDic Is Nothing Then Set robjDirectoryNamesDic = New Scripting.Dictionary
                                
                        With robjDirectoryNamesDic
                        
                            If Not .Exists(vstrFileOrDirectoryName) Then
                            
                                .Add vstrFileOrDirectoryName, VBA.GetAttr(strPath)
                            End If
                        End With
                End Select
            End If
        End If
    End If
End Sub


'''
''' Using VBA.Dir function, The file including Unicode characters is not supported.
'''
Public Function GetSubFilePathsByVBADir(ByVal vstrDirectoryPath As String, Optional ByVal vstrWildCard As String = "*.*") As Collection
    
    Dim objFilePaths As Collection, blnIsDirectoryExists As Boolean
    Dim strFileName As String, strPath As String
    
    blnIsDirectoryExists = False
    
    With New Scripting.FileSystemObject
    
        blnIsDirectoryExists = .FolderExists(vstrDirectoryPath)
    End With
    
    If blnIsDirectoryExists Then
    
        strFileName = VBA.Dir(vstrDirectoryPath & "\" & vstrWildCard)
        
        Do While strFileName <> ""
            
            strPath = vstrDirectoryPath & "\" & strFileName
            
            If Not (VBA.GetAttr(strPath) And vbDirectory) Then
            
                If strFileName <> "." And strFileName <> ".." Then
                
                    If objFilePaths Is Nothing Then Set objFilePaths = New Collection
                    
                    objFilePaths.Add strPath
                End If
            End If
        
            strFileName = VBA.Dir()
        Loop
    End If
    
    Set GetSubFilePathsByVBADir = objFilePaths
End Function


'**---------------------------------------------
'** Get general file information
'**---------------------------------------------
'''
''' Improvement point: Delaying point is that this expand the sigle directory-path list into data-table after the file-path list is gotten
'''
''' use VBA.Dir function
'''
''' <Argument>vstrDirectoryPath: Input</Argument>
''' <Argument>vobjNecessaryFileInfoTypeCol: Input</Argument>
''' <Return>Collection: DataTable</Return>
Public Function GetFilesInformationDataTableFromDirectoryPathAndWildCardByVbaDir(ByVal vstrDirectoryPath As String, _
        ByVal vobjNecessaryFileInfoTypeCol As Collection, _
        Optional ByVal vstrWildCard As String = "*.*") As Collection
    
    Dim objFilePaths As Collection

    Set objFilePaths = GetSubFilePathsByVBADir(vstrDirectoryPath, vstrWildCard)

    Set GetFilesInformationDataTableFromDirectoryPathAndWildCardByVbaDir = GetFilesInformationDataTable(objFilePaths, vobjNecessaryFileInfoTypeCol, vstrDirectoryPath)
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** get individual element information of each row of data-table
'**---------------------------------------------
'''
'''
'''
Private Function mfvarGetRowColOfFileGeneralInfoByVBADir(ByVal vstrFileOrDirectoryName As String, ByVal vblnIsFile As Boolean, ByVal vstrParentDirectoryFullPathWithSuffixBackSlash As String, ByVal vstrRelativeDirectoryPathWithSuffixBackSlash As String, ByVal vobjNecessaryFileInfoTypeCol As Collection, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing, Optional ByVal vintIncludingColumnStartIndex As Long = 1) As Variant
    
    Dim objRowCol As Collection, i As Long
    Dim varFileGeneralInfo As Variant, enmFileGeneralInfo As FileGeneralInfo
    
        
    Set objRowCol = New Collection
    
    ' If the file-path is too long, the GetFile() method may be failed...
    
'    Set objFile = vobjFS.GetFile(vstrFileOrDirectoryPath)
    
    If vobjNecessaryFileInfoTypeCol.Count = 1 Then
    
        enmFileGeneralInfo = vobjNecessaryFileInfoTypeCol.Item(1)
    
        Let mfvarGetRowColOfFileGeneralInfoByVBADir = mfvarGetFileOrDirectoryGeneralInfoValueByVBADir(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFS)
    Else
        i = 1
        
        For Each varFileGeneralInfo In vobjNecessaryFileInfoTypeCol
        
            If i >= vintIncludingColumnStartIndex Then
            
                enmFileGeneralInfo = varFileGeneralInfo
                
                objRowCol.Add mfvarGetFileOrDirectoryGeneralInfoValueByVBADir(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFS)
            End If
            
            i = i + 1
        Next
        
        Set mfvarGetRowColOfFileGeneralInfoByVBADir = objRowCol
    End If
End Function



'''
''' using FileSystemObject
'''
Private Function mfvarGetFileOrDirectoryGeneralInfoValueByVBADir(ByVal vstrFileOrDirectoryName As String, ByVal vblnIsFile As Boolean, ByVal vstrParentDirectoryFullPathWithSuffixBackSlash As String, ByVal vstrRelativeDirectoryPathWithSuffixBackSlash As String, ByVal venmFileGeneralInfo As FileGeneralInfo, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing) As Variant
    
    Dim varValue As Variant, intPointOfString As Long, objFS As Scripting.FileSystemObject, strExtension As String, strBaseName As String
    Dim objShortcut As IWshRuntimeLibrary.WshShortcut
    
    Select Case venmFileGeneralInfo
    
        Case FileGeneralInfo.giRelativeFullPath
        
            varValue = vstrRelativeDirectoryPathWithSuffixBackSlash & vstrFileOrDirectoryName
    
        Case FileGeneralInfo.giFullPath
        
            varValue = vstrParentDirectoryFullPathWithSuffixBackSlash & vstrFileOrDirectoryName
        
        Case FileGeneralInfo.giFileName
            
            varValue = vstrFileOrDirectoryName
        
        Case FileGeneralInfo.giFileBaseName ' file name without extension
        
            If vblnIsFile Then
                
                GetBaseNameToArgFromFileNameByVbaDir strBaseName, vstrFileOrDirectoryName
                
                varValue = strBaseName
'                intPointOfString = InStrRev(vstrFileOrDirectoryName, ".")
'
'                If intPointOfString > 0 Then
'
'                    varValue = Left(vstrFileOrDirectoryName, intPointOfString - 1)
'                Else
'                    varValue = vstrFileOrDirectoryName
'                End If
            Else
                varValue = vstrFileOrDirectoryName
            End If
            
        Case FileGeneralInfo.giFileExtension    ' file extension
        
            If vblnIsFile Then
            
                GetExtensionToArgNameFromFileNameByVbaDir strExtension, vstrFileOrDirectoryName
            
                varValue = strExtension
            
'                intPointOfString = InStrRev(vstrFileOrDirectoryName, ".")
'
'                If intPointOfString > 0 Then
'
'                    varValue = Right(vstrFileOrDirectoryName, Len(vstrFileOrDirectoryName) - intPointOfString)
'                Else
'                    varValue = ""
'                End If
            Else
                varValue = ""
            End If
        
        Case FileGeneralInfo.giFileSize ' Size unit is kilo-Byte
            
            varValue = CDbl(VBA.FileLen(vstrParentDirectoryFullPathWithSuffixBackSlash & vstrFileOrDirectoryName) / &H400) ' kB    - &H400 is equals to 1024
    
        Case FileGeneralInfo.giParentDirectoryPath
        
            If InStrRev(vstrParentDirectoryFullPathWithSuffixBackSlash, "\") = Len(vstrParentDirectoryFullPathWithSuffixBackSlash) Then
                
                varValue = Left(vstrParentDirectoryFullPathWithSuffixBackSlash, Len(vstrParentDirectoryFullPathWithSuffixBackSlash) - 1)
            Else
                varValue = vstrParentDirectoryFullPathWithSuffixBackSlash
            End If
            
        Case FileGeneralInfo.giRelativeDirectoryPath
        
            If vstrRelativeDirectoryPathWithSuffixBackSlash <> "" Then
            
                If InStrRev(vstrRelativeDirectoryPathWithSuffixBackSlash, "\") = Len(vstrRelativeDirectoryPathWithSuffixBackSlash) Then
                
                    varValue = Left(vstrRelativeDirectoryPathWithSuffixBackSlash, Len(vstrRelativeDirectoryPathWithSuffixBackSlash) - 1)
                Else
                    varValue = vstrRelativeDirectoryPathWithSuffixBackSlash
                End If

                varValue = ".\" & varValue
            Else
                varValue = ".\"
            End If

        Case FileGeneralInfo.giLastModifiedDate
        
            varValue = VBA.FileDateTime(vstrParentDirectoryFullPathWithSuffixBackSlash & vstrFileOrDirectoryName)
        
        Case FileGeneralInfo.giCreatedDate
        
            ' need for FileSystemObject
            If Not vobjFS Is Nothing Then
            
                Set objFS = vobjFS
            Else
                Set objFS = New Scripting.FileSystemObject
            End If
        
            If vblnIsFile Then
            
                varValue = objFS.GetFile(vstrParentDirectoryFullPathWithSuffixBackSlash & vstrFileOrDirectoryName).DateCreated
            Else
                varValue = objFS.GetFolder(vstrParentDirectoryFullPathWithSuffixBackSlash & vstrFileOrDirectoryName).DateCreated
            End If
        
        Case giReadOnly, giHidden, giSystem, giVolume, giDirectory, giArchive, giAlias, giCompressed
            
            varValue = mfblnGetAttributeEnabledForVBA(VBA.GetAttr(vstrParentDirectoryFullPathWithSuffixBackSlash & vstrFileOrDirectoryName), venmFileGeneralInfo)
    
        Case FileGeneralInfo.giLinkDestination
        
            If vblnIsFile Then
            
                With New IWshRuntimeLibrary.WshShell
            
                    On Error Resume Next
            
                    Set objShortcut = .CreateShortcut(vstrRelativeDirectoryPathWithSuffixBackSlash & vstrFileOrDirectoryName)
                    
                    On Error GoTo 0
                    
                    If Not objShortcut Is Nothing Then
                    
                        varValue = objShortcut.TargetPath
                    Else
                        varValue = ""
                    End If
                End With
            Else
            
                varValue = ""
            End If
            
        Case FileGeneralInfo.giLinkDestFileName
        
            If vblnIsFile Then
            
                With New IWshRuntimeLibrary.WshShell
            
                    On Error Resume Next
            
                    Set objShortcut = .CreateShortcut(vstrRelativeDirectoryPathWithSuffixBackSlash & vstrFileOrDirectoryName)
                    
                    On Error GoTo 0
                    
                    If Not objShortcut Is Nothing Then
                    
                        varValue = vobjFS.GetFileName(objShortcut.TargetPath)
                    Else
                        varValue = ""
                    End If
                End With
            Else
                varValue = ""
            End If
    End Select

    mfvarGetFileOrDirectoryGeneralInfoValueByVBADir = varValue
End Function


'''
'''
'''
Private Function mfblnGetAttributeEnabledForVBA(ByVal venmAttribute As VBA.VbFileAttribute, ByVal venmFileGeneralInfo As FileGeneralInfo) As Boolean
    
    Dim blnEnabled As Boolean
    
    blnEnabled = False
    
    If (venmAttribute And venmFileGeneralInfo) > 0 Then
    
        blnEnabled = True
    End If
    
    mfblnGetAttributeEnabledForVBA = blnEnabled
End Function

'''
'''
'''
Private Sub msubGetKeyAndValueOfFileGeneralInfoByVBADir(ByRef rvarKey As Variant, ByRef rvarValueCollectionOrNotObjectTypeData As Variant, _
                            ByVal vstrFileOrDirectoryName As String, ByVal vblnIsFile As Boolean, _
                            ByVal vstrParentDirectoryFullPathWithSuffixBackSlash As String, ByVal vstrRelativeDirectoryPathWithSuffixBackSlash As String, _
                            ByVal vobjNecessaryFileInfoTypeCol As Collection, _
                            Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing)
    
    
    Dim objRowCol As Collection
    Dim varFileGeneralInfo As Variant, enmFileGeneralInfo As FileGeneralInfo
        
    Set objRowCol = New Collection
    
    
    If vobjNecessaryFileInfoTypeCol.Count = 1 Then
        
        enmFileGeneralInfo = vobjNecessaryFileInfoTypeCol.Item(1)
    
        Let rvarKey = mfvarGetFileOrDirectoryGeneralInfoValueByVBADir(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFS)
        
        Let rvarValueCollectionOrNotObjectTypeData = 0
    
    ElseIf vobjNecessaryFileInfoTypeCol.Count = 2 Then
    
        enmFileGeneralInfo = vobjNecessaryFileInfoTypeCol.Item(1)
    
        Let rvarKey = mfvarGetFileOrDirectoryGeneralInfoValueByVBADir(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFS)
        
        enmFileGeneralInfo = vobjNecessaryFileInfoTypeCol.Item(2)
    
        Let rvarValueCollectionOrNotObjectTypeData = mfvarGetFileOrDirectoryGeneralInfoValueByVBADir(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFS)
    Else
        enmFileGeneralInfo = vobjNecessaryFileInfoTypeCol.Item(1)
    
        Let rvarKey = mfvarGetFileOrDirectoryGeneralInfoValueByVBADir(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, enmFileGeneralInfo, vobjFS)
        
        Set rvarValueCollectionOrNotObjectTypeData = mfvarGetRowColOfFileGeneralInfoByVBADir(vstrFileOrDirectoryName, vblnIsFile, vstrParentDirectoryFullPathWithSuffixBackSlash, vstrRelativeDirectoryPathWithSuffixBackSlash, vobjNecessaryFileInfoTypeCol, vobjFS, 2)
    End If
End Sub



