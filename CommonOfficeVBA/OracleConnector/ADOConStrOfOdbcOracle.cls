VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOConStrOfOdbcOracle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO ODBC connection string generator for Oracle RDB for both DSN or DSNless
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on Oracle ODBC driver such as {Oracle in OraClient12g_home1} at this Windows
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   Modification History:
'       Wed, 12/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mintOracleDefaultPortNumber As Long = 1521

#If Win64 Then

    Private Const mstrDriverName As String = "{Oracle in OraClient11g_home1}"    ' it is dependent on the installed ODBC drivers
#Else
    Private Const mstrDriverName As String = "{Oracle in OraClient10g_home1}"
#End If

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IADOConnectStrGenerator

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjADOConStrOfDsn As ADOConStrOfDsn

Private mobjADOConStrOfDsnlessOracle As ADOConStrOfDsnlessOracle

Private mblnUseDSN As Boolean


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub Class_Initialize()

    Set mobjADOConStrOfDsn = New ADOConStrOfDsn

    mobjADOConStrOfDsn.SetOdbcConnectionDestinationInformation OdbcConnectionDestinationToOracle

    Set mobjADOConStrOfDsnlessOracle = New ADOConStrOfDsnlessOracle
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get DsnAdoConStrGenerator() As ADOConStrOfDsn

    Set DsnAdoConStrGenerator = mobjADOConStrOfDsn
End Property

'''
'''
'''
Public Property Get DsnlessAdoConStrGenerator() As ADOConStrOfDsnlessOracle

    Set DsnlessAdoConStrGenerator = mobjADOConStrOfDsnlessOracle
End Property

'''
'''
'''
Public Property Get UseDSN() As Boolean

    UseDSN = mblnUseDSN
End Property
Public Property Let UseDSN(ByVal vblnUseDSN As Boolean)

    mblnUseDSN = vblnUseDSN
End Property


'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
Private Function IADOConnectStrGenerator_GetConnectionString() As String

    If mblnUseDSN Then
    
        IADOConnectStrGenerator_GetConnectionString = mobjADOConStrOfDsn.GetConnectionString
    Else
        IADOConnectStrGenerator_GetConnectionString = mobjADOConStrOfDsnlessOracle.GetConnectionString
    End If
End Function


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetConnectionString() As String

    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
End Function

'**---------------------------------------------
'** Set ADODB ODBC parameters directly
'**---------------------------------------------
'''
''' using ODBC DataSource setting (DSN)
'''
''' <Argument>vstrDSN: Input</Argument>
''' <Argument>vstrUID: Input</Argument>
''' <Argument>vstrPWD: Input</Argument>
Public Sub SetODBCConnectionWithDSN(ByVal vstrDSN As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String)


    mobjADOConStrOfDsn.SetODBCConnectionWithDSN vstrDSN, vstrUID, vstrPWD
    
    mblnUseDSN = True
End Sub

'''
'''
'''
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrNetworkServiceName: Input</Argument>
''' <Argument>vstrUserid: Input</Argument>
''' <Argument>vstrPassword: Input</Argument>
''' <Argument>vintPortNumber: Input</Argument>
Public Sub SetOracleOdbcConnectionWithoutDSN(ByVal vstrDriverName As String, _
        ByVal vstrServerHostName As String, _
        ByVal vstrNetworkServiceName As String, _
        ByVal vstrUserid As String, _
        ByVal vstrPassword As String, _
        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber)

    mobjADOConStrOfDsnlessOracle.SetOracleOdbcConnectionWithoutDSN vstrDriverName, _
            vstrServerHostName, _
            vstrNetworkServiceName, _
            vstrUserid, _
            vstrPassword, _
            vintPortNumber
    
    mblnUseDSN = False
End Sub


'**---------------------------------------------
'** By a User-form, set ADODB ODBC parameters
'**---------------------------------------------
'''
''' use SimpleAdoConStrAuthentication.bas
'''
''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
''' <Argument>vstrDSN: Input</Argument>
''' <Argument>vstrUID: Input</Argument>
''' <Argument>vstrPWD: Input</Argument>
Public Function IsAdoConnectionContinuedAfterOracleOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDSN As String = "", _
        Optional ByVal vstrUID As String = "", _
        Optional ByVal vstrPWD As String = "") As Boolean

    Dim blnIsRequestedToCancelAdoConnection As Boolean

    mblnUseDSN = True
    
    SetOdbcConnectionParametersByOracleForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, "", "", "", vstrUID, vstrPWD, , vstrDSN
    
    IsAdoConnectionContinuedAfterOracleOdbcConnectionDsnParametersByForm = Not blnIsRequestedToCancelAdoConnection
End Function


'''
''' DSNless connection, with referring SimpleAdoConStrAuthentication.bas
'''
''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrNetworkServiceName: Input</Argument>
''' <Argument>vstrUserid: Input</Argument>
''' <Argument>vstrPassword: Input</Argument>
''' <Argument>vintPortNumber: Input</Argument>
Public Function IsAdoConnectionContinuedAfterOracleOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDriverName As String = mstrDriverName, _
        Optional ByVal vstrServerHostName As String = "", _
        Optional ByVal vstrNetworkServiceName As String = "", _
        Optional ByVal vstrUserid As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber) As Boolean

    
    Dim blnIsRequestedToCancelAdoConnection As Boolean
    
    mblnUseDSN = False
    
    SetOdbcConnectionParametersByOracleForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber
    
    IsAdoConnectionContinuedAfterOracleOdbcConnectionDsnlessParametersByForm = Not blnIsRequestedToCancelAdoConnection
End Function


