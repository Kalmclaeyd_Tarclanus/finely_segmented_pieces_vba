VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} USetAdoOdbcConStrForOracle 
   Caption         =   "USetAdoOdbcConStrForOracle"
   ClientHeight    =   8730.001
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   5650
   OleObjectBlob   =   "USetAdoOdbcConStrForOracle.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "USetAdoOdbcConStrForOracle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   ADO Connection string parameters form for Oracle database by ODBC
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   Modification History:
'       Fri, 14/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "USetAdoOdbcConStrForOracle"


Private Const mintOracleDefaultPortNumber As Long = 1521

Private Const mstrDefaultPortNumberText As String = "(default)"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

' User-form document object
Private mobjFormStateToSetAdoConStr As FormStateToSetAdoConStr

' user-form input result cache
Private mobjCurrentUserInputParametersDic As Scripting.Dictionary

' When FixedSettingNameMode is false, then the following is read from Registry
Private mobjSettingKeyNameToUserInputParametersDic As Scripting.Dictionary  ' Registry cache


'**---------------------------------------------
'** Form state control
'**---------------------------------------------
Private mblnFixedSettingNameMode As Boolean

Private mblnUseDSN As Boolean


Private mblnIsFormInitialized As Boolean

Private mintInitializing As Long

Private mblnIsUsingDSNOptionChangedAfterRefresh As Boolean


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub UserForm_Initialize()

    mblnFixedSettingNameMode = False

    mblnIsFormInitialized = False


    mblnIsUsingDSNOptionChangedAfterRefresh = False
End Sub

'''
'''
'''
Private Sub UserForm_Layout()

    If Not mblnIsFormInitialized Then
    
        msubClearAll
    
        GetCurrentFormPositionFromUserReg mstrModuleName, Me, Application.Name
        
        msubInitializePasswordTextBox
        
        msubInitializeFixedSettingNameMode
        
        msubInitializeCachingConStrParamsOptions
    
        msubLocalize
        
        msubRefresh
        
        
        msubInitializeFirstControlFocus
    
        mblnIsFormInitialized = True
    End If
End Sub

'''
'''
'''
Private Sub UserForm_QueryClose(ByRef rintCancel As Integer, ByRef rintCloseMode As Integer)

    ' When an user clicks the cancel X button, then rintCloseMode becomes False

    If VBA.VbQueryClose.vbFormControlMenu = rintCloseMode Then
    
        mobjFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection = True
    Else
        msubUpdateFormStateToSetAdoConStr
    End If
    
    SetCurrentFormPositionToUserReg mstrModuleName, Me, Application.Name
End Sub


'''
'''
'''
Private Sub optUseDSN_Change()

    If mintInitializing <= 0 Then

        If optUseDSN.Value And Not mblnUseDSN Then
            
            mblnUseDSN = True
            
            msubRefreshDSNEnabledUIControls
        End If
    End If
End Sub

'''
'''
'''
Private Sub optDsnless_Change()

    If mintInitializing <= 0 Then
    
        If optDsnless.Value And mblnUseDSN Then
        
            mblnUseDSN = False
            
            msubRefreshDSNEnabledUIControls
        End If
    End If
End Sub


'''
'''
'''
Private Sub cboConStrCacheName_Change()

    Dim strSettingKeyName As String

    If mintInitializing <= 0 Then

        If cboConStrCacheName.ListCount > 0 Or cboConStrCacheName.Text <> "" Then

            cmdOK.Enabled = True
    
            msubRefresh
        Else
            cmdOK.Enabled = False
        End If
    End If
End Sub


'''
'''
'''
Private Sub cmdAdoConnectionTest_Click()

    Dim strConStr As String
    
    msubUpdateInputDicOfFormState
    
    strConStr = GetADOConnectionOdbcOracleStringFromInputDic(mobjCurrentUserInputParametersDic)
    
    AdoVariousTypeConnectionTestAndShowMessageBox strConStr, AdoOdbcAndOracle
End Sub

'''
'''
'''
Private Sub cmdOK_Click()

    VBA.Global.Unload Me
End Sub

'''
'''
'''
Private Sub chkSaveParametersInRegistoryExceptForPassword_Change()

    If mintInitializing <= 0 Then
    
        SetAdoConnectionSaveParametersInRegistoryExceptForPassword chkSaveParametersInRegistoryExceptForPassword.Value
    End If
End Sub

'''
'''
'''
Private Sub chkSavePasswordTemporaryCache_Change()

    If mintInitializing <= 0 Then

        SetAllowToSaveAdoConStrPasswordInTemporaryCache chkSavePasswordTemporaryCache.Value
    End If
End Sub

'''
'''
'''
Private Sub txtPassword_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    Dim strConStr As String

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyReturn Then
    
            msubUpdateInputDicOfFormState
            
            strConStr = GetADOConnectionOdbcOracleStringFromInputDic(mobjCurrentUserInputParametersDic)
            
            If IsADOConnectionStringEnabled(strConStr) Then
    
                cmdOK_Click
            End If
        ElseIf venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
        
            msubCloseFormToCancel
        End If
    End If
End Sub


Private Sub txtDSN_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
        
            msubCloseFormToCancel
        End If
    End If
End Sub
Private Sub txtOracleOdbcDriver_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
        
            msubCloseFormToCancel
        End If
    End If
End Sub
Private Sub txtNetworkService_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
        
            msubCloseFormToCancel
        End If
    End If
End Sub
Private Sub txtServerHost_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
        
            msubCloseFormToCancel
        End If
    End If
End Sub
Private Sub txtPortNo_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
        
            msubCloseFormToCancel
        End If
    End If
End Sub
Private Sub txtUserName_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
        
            msubCloseFormToCancel
        End If
    End If
End Sub
'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vobjFormStateToSetAdoConStr: Input</Argument>
''' <Argument>vblnFixedSettingNameMode: Input</Argument>
Public Sub SetInitialFormStateToSetAdoConStr(ByVal vobjFormStateToSetAdoConStr As FormStateToSetAdoConStr, _
        Optional ByVal vblnFixedSettingNameMode As Boolean = True)

    Set mobjFormStateToSetAdoConStr = vobjFormStateToSetAdoConStr

    With mobjFormStateToSetAdoConStr
    
        Set mobjCurrentUserInputParametersDic = .UserInputParametersDic
    End With

    mblnFixedSettingNameMode = vblnFixedSettingNameMode
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubLocalize()

    Me.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrOdbcOracleFormTitle()

    LocalizeUIControlOfAdoConStrInfoForAdoConStrLblConStrCacheName lblConStrCacheName, fraAdoConnectionParameters

    
    ' About fraDsnType
    LocalizeUIControlOfAdoConStrInfoForAdoConStrFraDsnParameters fraDsnType, optUseDSN, optDsnless, fraDSNParameters, lblDataSourceName

    
    msubLocalizeOracleDsnlessSettings

    lblUserName.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblUserName()
    
    lblPassword.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblUserPassword()

    cmdAdoConnectionTest.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrCmdAdoConnectionTest()

    ' About fraConnectionStringInfoCachingOption
    LocalizeUIControlOfAdoConStrInfoForAdoConStrFraConnectionStringInfoCachingOption fraConnectionStringInfoCachingOption, chkSaveParametersInRegistoryExceptForPassword, chkSavePasswordTemporaryCache

End Sub

'''
'''
'''
Private Sub msubLocalizeOracleDsnlessSettings()

    fraDSNlessOracleParameters.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessOraSqlParameters()

    lblOdbcDriver.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblOdbcDriver()

    lblServerHost.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblServerHost()

    lblNetworkService.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblNetworkServiceName()

    lblPortNo.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblPortNo()
End Sub



'**---------------------------------------------
'** Update mobjFormStateToSetAdoOdbcConStr from UI
'**---------------------------------------------
'''
'''
'''
Private Sub msubUpdateFormStateToSetAdoConStr()

    Dim objUIInputResultDic As Scripting.Dictionary

    If Not mblnFixedSettingNameMode Then
    
        mobjFormStateToSetAdoConStr.SettingKeyName = GetCurrentSettingKeyNameFromComboBox
    End If
    
    msubUpdateInputDicOfFormState
End Sub

'''
'''
'''
Private Function GetCurrentSettingKeyNameFromComboBox() As String

    GetCurrentSettingKeyNameFromComboBox = GetCurrentValueOfComboBoxWithTextEditable(cboConStrCacheName)
End Function


'''
'''
'''
Private Sub msubUpdateInputDicOfFormState()

    Dim objUIInputResultDic As Scripting.Dictionary

    If mblnIsUsingDSNOptionChangedAfterRefresh Then
    
        Set objUIInputResultDic = New Scripting.Dictionary
        
        msubUpdateInputDicFromUI objUIInputResultDic
        
        If IsAnyDifferencesBetweenTwoDictionariesAboutBothKeyAndValue(mobjCurrentUserInputParametersDic, objUIInputResultDic) Then
        
            Set mobjCurrentUserInputParametersDic = objUIInputResultDic
        End If
        
        Set mobjFormStateToSetAdoConStr.UserInputParametersDic = mobjCurrentUserInputParametersDic
    Else
        msubUpdateInputDicFromUI mobjCurrentUserInputParametersDic
    End If
End Sub


'''
''' Update objUserInputParametersDic from UI
'''
Private Sub msubUpdateInputDicFromUI(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String

    With robjUserInputParametersDic

        strKey = "UseDSN"
        
        If .Exists(strKey) Then
        
            .Item(strKey) = CInt(mblnUseDSN)
        Else
            .Add strKey, CInt(mblnUseDSN)
        End If

        If mblnUseDSN Then
        
            msubUpdateInputDicForDSNFromUI robjUserInputParametersDic
        Else
            msubUpdateInputDicForDSNlessOracleFromUI robjUserInputParametersDic
        End If
    End With
End Sub

'''
'''
'''
Private Sub msubUpdateInputDicForDSNFromUI(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String, strValue As String
    
    With robjUserInputParametersDic
    
        strKey = "DSN": strValue = txtDSN.Text
    
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        strKey = "UserName": strValue = txtUserName.Text
    
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        strKey = mstrTemporaryPdKey: strValue = txtPassword.Text
        
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
    End With
End Sub


'''
'''
'''
Private Sub msubUpdateInputDicForDSNlessOracleFromUI(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String, strValue As String, intValue As Long
    
    With robjUserInputParametersDic
    
        strKey = "DriverName": strValue = txtOracleOdbcDriver.Text
    
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        strKey = "ServerHostName": strValue = txtServerHost.Text
        
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        strKey = "NetworkServiceName": strValue = txtNetworkService.Text
        
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        strKey = "PortNumber": strValue = txtPortNo.Text
        
        If strValue <> mstrDefaultPortNumberText And IsNumeric(strValue) Then
        
            intValue = CLng(strValue)
        
            If .Exists(strKey) Then .Item(strKey) = intValue Else .Add strKey, intValue
        Else
            If .Exists(strKey) Then .Remove strKey
        End If
        
        
        strKey = "UserName": strValue = txtUserName.Text
    
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        strKey = mstrTemporaryPdKey: strValue = txtPassword.Text
        
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
    End With
End Sub


'**---------------------------------------------
'** Refresh UI
'**---------------------------------------------
'''
'''
'''
Private Sub msubRefresh()

    mintInitializing = mintInitializing + 1

    msubRefreshFixedSettingNameMode

    msubRefreshDSNEnabling mobjCurrentUserInputParametersDic

    If mblnUseDSN Then
    
        msubRefreshDSNParameters mobjCurrentUserInputParametersDic
    Else
        msubRefreshDSNlessOracleParameters mobjCurrentUserInputParametersDic
    End If

    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubRefreshFixedSettingNameMode()

    If Not mblnFixedSettingNameMode Then
    
        msubRefreshCurrentSettingKeyAndUserInputParametersDic
    End If
End Sub

'''
'''
'''
Private Sub msubRefreshCurrentSettingKeyAndUserInputParametersDic()

    Set mobjCurrentUserInputParametersDic = GetCurrentUserInputParametersDicFromComboBoxAndCacheDic(cboConStrCacheName, mobjSettingKeyNameToUserInputParametersDic)
End Sub


'''
'''
'''
Private Sub msubRefreshDSNParameters(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    With robjUserInputParametersDic

        txtDSN.Text = .Item("DSN")

        txtUserName.Text = .Item("UserName")

        txtPassword.Text = .Item(mstrTemporaryPdKey)
    End With
End Sub

'''
'''
'''
Private Sub msubRefreshDSNlessOracleParameters(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim intPortNumber As Long

    With robjUserInputParametersDic

        txtOracleOdbcDriver.Text = .Item("DriverName")
        
        txtServerHost.Text = .Item("ServerHostName")
        
        txtNetworkService.Text = .Item("NetworkServiceName")
        
        If .Exists("PortNumber") Then
        
            intPortNumber = .Item("PortNumber")
            
            If intPortNumber = mintOracleDefaultPortNumber Then
            
                txtPortNo.Text = mstrDefaultPortNumberText
            Else
                txtPortNo.Text = CStr(intPortNumber)
            End If
        Else
            txtPortNo.Text = mstrDefaultPortNumberText
        End If
        
        txtUserName.Text = .Item("UserName")

        txtPassword.Text = .Item(mstrTemporaryPdKey)
    End With
End Sub


'''
'''
'''
Private Sub msubRefreshDSNEnabling(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    With robjUserInputParametersDic
    
        mblnUseDSN = .Item("UseDSN")
    
        msubRefreshDSNEnabledUIControls
    End With
End Sub


'''
'''
'''
Private Sub msubRefreshDSNEnabledUIControls()

    mintInitializing = mintInitializing + 1

    mblnIsUsingDSNOptionChangedAfterRefresh = True

    If mblnUseDSN Then
    
        optUseDSN.Value = True
    
        msubRefreshDSNEnabled True
        
        msubRefreshDSNlessUIEnabled False
    Else
        optDsnless.Value = True
    
        msubRefreshDSNEnabled False
        
        msubRefreshDSNlessUIEnabled True
    End If

    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubRefreshDSNEnabled(ByVal vblnEnabled As Boolean)

    If vblnEnabled Then
    
        fraDSNParameters.Enabled = True
        
        lblDataSourceName.Enabled = True
    
        txtDSN.Enabled = True
    Else
        txtDSN.Enabled = False
    
        lblDataSourceName.Enabled = False
    
        fraDSNParameters.Enabled = False
    End If
End Sub

'''
'''
'''
Private Sub msubRefreshDSNlessUIEnabled(ByVal vblnEnabled As Boolean)

    If vblnEnabled Then
    
        fraDSNlessOracleParameters.Enabled = True
    
        msubRefreshDsnlessDetailUIEnabled True
    Else
        msubRefreshDsnlessDetailUIEnabled False
        
        fraDSNlessOracleParameters.Enabled = False
    End If
End Sub

'''
'''
'''
Private Sub msubRefreshDsnlessDetailUIEnabled(ByRef rblnEnabled As Boolean)

    lblOdbcDriver.Enabled = rblnEnabled

    txtOracleOdbcDriver.Enabled = rblnEnabled
    
    lblServerHost.Enabled = rblnEnabled
    
    txtServerHost.Enabled = rblnEnabled
    
    lblNetworkService.Enabled = rblnEnabled
    
    txtNetworkService.Enabled = rblnEnabled
    
    lblPortNo.Enabled = rblnEnabled
    
    txtPortNo.Enabled = rblnEnabled
End Sub

'''
'''
'''
Private Sub msubClearAll()


    msubClearSettingKeyName
    
    msubClearDSN
    
    msubClearUserIDAndPassword

    msubClearDSNlessOracleParameters
End Sub

'''
'''
'''
Private Sub msubClearSettingKeyName()
    
    mintInitializing = mintInitializing + 1
    
    With cboConStrCacheName
    
        .Clear
    End With
    
    mintInitializing = mintInitializing - 1
End Sub



'''
'''
'''
Private Sub msubClearDSN()

    mintInitializing = mintInitializing + 1
    
    txtDSN.Text = ""
    
    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubClearUserIDAndPassword()

    mintInitializing = mintInitializing + 1
    
    txtUserName.Text = ""
    
    txtPassword.Text = ""
    
    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubClearDSNlessOracleParameters()

    mintInitializing = mintInitializing + 1

    txtOracleOdbcDriver.Text = ""
    
    txtServerHost.Text = ""
    
    txtNetworkService.Text = ""

    txtPortNo.Text = ""

    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubInitializePasswordTextBox()

    With txtPassword
    
        .IMEMode = fmIMEModeAlpha
    
        .PasswordChar = "*"
    End With
End Sub


'''
'''
'''
Private Sub msubInitializeFixedSettingNameMode()

    mintInitializing = mintInitializing + 1

    InitializeFixedSettingNameModeForEachRDB mobjSettingKeyNameToUserInputParametersDic, cboConStrCacheName, mobjFormStateToSetAdoConStr, AdoOdbcAndOracle, mblnFixedSettingNameMode, cmdOK
    
    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubInitializeCachingConStrParamsOptions()

    mintInitializing = mintInitializing + 1

    chkSaveParametersInRegistoryExceptForPassword.Value = GetAdoConnectionSaveParametersInRegistoryExceptForPassword()

    InitializeSavePasswordTemporaryCacheCheckBox chkSavePasswordTemporaryCache

    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubInitializeFirstControlFocus()

    mintInitializing = mintInitializing + 1
    
    If mblnUseDSN Then
    
        If txtDSN.Text <> "" And txtUserName <> "" Then
        
            txtPassword.SetFocus
        Else
            If txtDSN.Text = "" Then
            
                txtDSN.SetFocus
            
            ElseIf txtUserName.Text = "" Then
            
                txtUserName.SetFocus
            End If
        End If
    Else
        If txtOracleOdbcDriver <> "" And txtNetworkService.Text <> "" And txtServerHost.Text <> "" And txtPortNo.Text <> "" Then
    
            txtPassword.SetFocus
        Else
            Select Case True
            
                Case txtOracleOdbcDriver.Text = ""
            
                    txtOracleOdbcDriver.SetFocus
                    
                Case txtNetworkService.Text = ""
                
                    txtNetworkService.SetFocus
                    
                Case txtServerHost.Text = ""
            
                    txtServerHost.SetFocus
                    
                Case txtUserName.Text = ""
                
                    txtUserName.SetFocus
            End Select
        End If
    End If
    
    mintInitializing = mintInitializing - 1
End Sub


'''
'''
'''
Private Sub msubCloseFormToCancel()

    mobjFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection = True
    
    cmdOK_Click
End Sub


