VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOConStrOfDsnlessOracle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO connection string generator for Oracle RDB without DSN
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on both an installed Oracle and Oracle ODBC driver at this Windows
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   Modification History:
'       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IADOConnectStrGenerator

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mstrUID As String   ' User ID

Private mstrPWD As String   ' Password

'**---------------------------------------------
'** ODBC DSNless connection
'**---------------------------------------------
Private mstrDriverName As String    ' ODBC driver name string

Private mstrServerHostName As String

Private mstrNetworkServiceName As String    ' Network service name, for Oracle server

Private mintPortNo As Long  ' connection TCP/IP port number, it is ordinary omitted, for Oracle 1521, for PostgreSQL 5432


'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
Private Function IADOConnectStrGenerator_GetConnectionString() As String

    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForOracleODBCWithoutDSN()
End Function
'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
''' read-only User Name - ODBC
'''
Public Property Get UserName() As String

    UserName = mstrUID
End Property

'**---------------------------------------------
'** ODBC DSNless connection
'**---------------------------------------------
'''
''' read-only Driver Name for ODBC
'''
Public Property Get DriverName() As String

    DriverName = mstrDriverName
End Property

'''
''' read-only RDB server host name or IP address
'''
Public Property Get ServerHostName() As String

    ServerHostName = mstrServerHostName
End Property


'''
''' read-only Oracle network-service name
'''
Public Property Get NetworkServiceName() As String

    NetworkServiceName = mstrNetworkServiceName
End Property

'''
''' read-only Port number
'''
Public Property Get PortNo() As Long

    PortNo = mintPortNo
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' using Oracle ODBC Driver connection string
'''
Public Sub SetOracleOdbcConnectionWithoutDSN(ByVal vstrDriverName As String, _
        ByVal vstrServerHostName As String, _
        ByVal vstrNetworkServiceName As String, _
        ByVal vstrUserid As String, _
        ByVal vstrPassword As String, _
        Optional ByVal vintPortNumber As Long = 1521)
    
    
    mstrDriverName = vstrDriverName ' for Oracle
    
    mstrServerHostName = vstrServerHostName
    
    mstrNetworkServiceName = vstrNetworkServiceName
    
    
    mstrUID = vstrUserid   ' User ID
    
    mstrPWD = vstrPassword   ' Password
    
    mintPortNo = vintPortNumber
End Sub

'''
'''
'''
Public Function GetConnectionString()

    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
End Function

'''
''' For Oracle, DSNless connection
'''
Public Function GetConnectionStringForOracleODBCWithoutDSN() As String

    GetConnectionStringForOracleODBCWithoutDSN = GetAdoDbConnectionOdbcDSNLessStringToOracle(mstrDriverName, mstrServerHostName, mstrNetworkServiceName, mstrUID, mstrPWD)

'    GetConnectionStringForOracleODBCWithoutDSN = "DRIVER=" & mstrDriverName & _
'            ";SERVER=" & mstrServerHostName & _
'            ";DBQ=" & mstrNetworkServiceName & _
'            ";UID=" & mstrUID & ";PWD=" & mstrPWD & ";"
End Function



