Attribute VB_Name = "ADOConStrToolsForOracle"
'
'   Generate ADODB connection string to a Oracle data base
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   Modification History:
'       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectionUtilities.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mintOracleDefaultPortNumber As Long = 1521

#If Win64 Then

    Private Const mstrDriverName As String = "{Oracle in OraClient11g_home1}"    ' it is dependent on the installed ODBC drivers
#Else
    Private Const mstrDriverName As String = "{Oracle in OraClient10g_home1}"
#End If

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** By Oracle OLE DB provider
'**---------------------------------------------
'''
''' use the OraOLEDB.Oracle provider
'''
Public Function GetAdoDbConnectionOraOleDbString(ByVal vstrDataSource As String, ByVal vstrUID As String, ByVal vstrPWD As String) As String

    Dim strConnectionString As String

    strConnectionString = "Provider=OraOLEDB.Oracle;"
    
    strConnectionString = strConnectionString & "Data Source=" & vstrDataSource & ";"
    
    strConnectionString = strConnectionString & "User ID=" & vstrUID & ";"
    
    strConnectionString = strConnectionString & "Password=" & vstrPWD

    GetAdoDbConnectionOraOleDbString = strConnectionString
End Function


'''
''' use a Oracle RDB OLEDB provider
'''
Public Function GetAdoDbConnectionOleDbStringToOracleWithFixedProviderName(ByVal vstrServerHostName As String, _
        ByVal vstrPortNo As String, _
        ByVal vstrNetworkServiceName As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String) As String
        
    Dim strConnectionString As String
    
    Const strProvider As String = "OraOLEDB.Oracle"

    strConnectionString = GetAdoDbConnectionOleDbStringToOracle(strProvider, vstrServerHostName, vstrPortNo, vstrNetworkServiceName, vstrUID, vstrPWD)
                        
    GetAdoDbConnectionOleDbStringToOracleWithFixedProviderName = strConnectionString
End Function


'''
''' use a Oracle RDB OLEDB provider
'''
Public Function GetAdoDbConnectionOleDbStringToOracle(ByVal vstrProviderName As String, _
        ByVal vstrServerHostName As String, _
        ByVal vstrPortNo As String, _
        ByVal vstrNetworkServiceName As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String) As String
        
    Dim strConnectionString As String
    
    strConnectionString = "Provider=" & vstrProviderName _
                        & ";Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)" _
                        & "(HOST=" & vstrServerHostName & ")" _
                        & "(PORT=" & vstrPortNo & "))" _
                        & "(CONNECT_DATA=" _
                        & "(SERVICE_NAME=" & vstrNetworkServiceName & ")))" _
                        & ";User ID=" & vstrUID _
                        & ";PASSWORD=" & vstrPWD
                        
    GetAdoDbConnectionOleDbStringToOracle = strConnectionString
End Function

'**---------------------------------------------
'** By Oracle ODBC driver
'**---------------------------------------------
'''
''' use a Oracle RDB ODBC driver
'''
Public Function GetAdoDbConnectionOdbcDSNLessStringToOracle(ByVal vstrDriverName As String, _
        ByVal vstrServerHostName As String, _
        ByVal vstrNetworkServiceName As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String) As String
    
    Dim strConnectionString As String

    strConnectionString = "DRIVER=" & vstrDriverName & ";" _
                        & "SERVER=" & vstrServerHostName & ";" _
                        & "DBQ=" & vstrNetworkServiceName & ";" _
                        & "UID=" & vstrUID & ";" _
                        & "PWD=" & vstrPWD & ";"

    GetAdoDbConnectionOdbcDSNLessStringToOracle = strConnectionString
End Function


'''
''' use a Oracle RDB ODBC driver
'''
Public Function GetAdoDbConnectionOdbcDSNLessStringToOracleWithPresetDriverName(ByVal vstrServerHostName As String, _
        ByVal vstrNetworkServiceName As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String) As String
        
    Dim strConnectionString As String

    strConnectionString = GetAdoDbConnectionOdbcDSNLessStringToOracle(mstrDriverName, _
            vstrServerHostName, _
            vstrNetworkServiceName, _
            vstrUID, _
            vstrPWD)
                        
    GetAdoDbConnectionOdbcDSNLessStringToOracleWithPresetDriverName = strConnectionString
End Function


'**---------------------------------------------
'** About connecting to Oracle
'**---------------------------------------------
'''
''' DSN less ODBC Oracle DB connection test
'''
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrNetworkServiceName: Input</Argument>
''' <Argument>vstrUID: input - User ID</Argument>
''' <Argument>vstrPWD: input - Password</Argument>
Public Function DoesOdbcOracleDSNLessSettingExists(ByVal vstrServerHostName As String, _
        ByVal vstrNetworkServiceName As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String) As Boolean
    
    Dim strConnectionString As String

    strConnectionString = GetAdoDbConnectionOdbcDSNLessStringToOracle(mstrDriverName, _
            vstrServerHostName, _
            vstrNetworkServiceName, _
            vstrUID, _
            vstrPWD)
    
    DoesOdbcOracleDSNLessSettingExists = IsADOConnectionStringEnabled(strConnectionString)
End Function


'''
''' OLE-DB oracle DB connection test
'''
Public Function DoesOLEDBOracleSettingExists(ByVal vstrServerHostName As String, _
        ByVal vstrPortNo As String, _
        ByVal vstrNetworkServiceName As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String) As Boolean
    
    Dim strConnectionString As String

    strConnectionString = GetAdoDbConnectionOleDbStringToOracleWithFixedProviderName(vstrServerHostName, vstrPortNo, vstrNetworkServiceName, vstrUID, vstrPWD)

    DoesOLEDBOracleSettingExists = IsADOConnectionStringEnabled(strConnectionString)
End Function


'**---------------------------------------------
'** Find a proper Oracle ODBC driver name
'**---------------------------------------------
'''
'''
'''
Public Function GetProperOracleDriverName(ByVal vobjDriverNameCandidates As Collection, _
        ByVal vstrServerHostName As String, _
        ByVal vstrNetworkServiceName As String, _
        ByVal vstrUID As String, ByVal vstrPWD) As String
    
    
    Dim varDriverName As Variant, strDriverName As String
    
    Dim strConnectionString As String, strProperDriverName As String
    
    
    strProperDriverName = ""
    
    For Each varDriverName In vobjDriverNameCandidates
        
        strDriverName = varDriverName
        
        strConnectionString = GetAdoDbConnectionOdbcDSNLessStringToOracle(strDriverName, _
                vstrServerHostName, _
                vstrNetworkServiceName, _
                vstrUID, _
                vstrPWD)
        
        If IsADOConnectionStringEnabled(strConnectionString) Then
        
            strProperDriverName = strDriverName
        End If
    Next

    GetProperOracleDriverName = strProperDriverName
End Function


'''
''' Return the candidate names of Oracle driver installed
'''
Public Function GetDefaultOracleODBCDriverNameList() As Collection

    Dim objODBCDriverNames As Collection
    
#If Win64 Then

    Const strDefaultDriverNames As String = "{Oracle in OraClient11g_home1},{Oracle in OraClient11g_home2},{Oracle in OraClient11g_home3}"  ' it is dependent on the installed ODBC drivers
#Else

    Const strDefaultDriverNames As String = "{Oracle in OraClient10g_home1},{Oracle in OraClient10g_home2},{Oracle in OraClient10g_home3}"  ' it is dependent on the installed ODBC drivers
#End If

    Set objODBCDriverNames = GetColFromLineDelimitedChar(strDefaultDriverNames)

    Set GetDefaultOracleODBCDriverNameList = objODBCDriverNames
End Function

'''
'''
'''
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrNetworkServiceName: Input</Argument>
''' <Argument>vstrUID: Input</Argument>
''' <Argument>vstrPWD: Input</Argument>
''' <Return>String</Return>
Public Function GetProperOracleDriverNameFromPreservedODBCDriverNames(ByVal vstrServerHostName As String, _
        ByVal vstrNetworkServiceName As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String) As String


    GetProperOracleDriverNameFromPreservedODBCDriverNames = GetProperOracleDriverName(GetDefaultOracleODBCDriverNameList(), _
            vstrServerHostName, _
            vstrNetworkServiceName, _
            vstrUID, _
            vstrPWD)
End Function


'**---------------------------------------------
'** for control USetAdoOdbcConStrForOracle user-form
'**---------------------------------------------
'''
'''
'''
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrNetworkServiceName: Input</Argument>
''' <Argument>vstrUserid: Input</Argument>
''' <Argument>vstrPassword: Input</Argument>
''' <Argument>vintPortNumber: Input</Argument>
Public Function GetInitialUserInputParametersDicForAdoDSNlessConnectionOfOracle(Optional ByVal vstrDriverName As String = "", _
        Optional ByVal vstrServerHostName As String = "", _
        Optional ByVal vstrNetworkServiceName As String = "", _
        Optional ByVal vstrUserid As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary

    With objDic
    
        .Add "UseDSN", False
    
    
        .Add "DriverName", vstrDriverName
    
        .Add "ServerHostName", vstrServerHostName
    
        .Add "NetworkServiceName", vstrNetworkServiceName
    
        .Add "PortNumber", vintPortNumber
        
    
        .Add "UserName", vstrUserid
    
        .Add mstrTemporaryPdKey, vstrPassword
        
        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOdbcConnectionDestinationRDBType(OdbcConnectionDestinationToOracle)
    End With

    Set GetInitialUserInputParametersDicForAdoDSNlessConnectionOfOracle = objDic
End Function

'''
'''
'''
Public Function GetADOConStrOfOdbcOracleFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfOdbcOracle

    Dim objADOConStrOfOdbcOracle As ADOConStrOfOdbcOracle
    
    Set objADOConStrOfOdbcOracle = New ADOConStrOfOdbcOracle
    
    UpdateADOConStrOfOdbcOracleFromInputDic objADOConStrOfOdbcOracle, robjUserInputParametersDic
    
    Set GetADOConStrOfOdbcOracleFromInputDic = objADOConStrOfOdbcOracle
End Function


'''
'''
'''
Public Sub UpdateADOConStrOfOdbcOracleFromInputDic(ByRef robjADOConStrOfOdbcOracle As ADOConStrOfOdbcOracle, _
        ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim blnUseDSN As Boolean

    blnUseDSN = robjUserInputParametersDic.Item("UseDSN")
    
    robjADOConStrOfOdbcOracle.UseDSN = blnUseDSN
    
    If blnUseDSN Then
    
        UpdateADOConStrOfDSNFromInputDic robjADOConStrOfOdbcOracle.DsnAdoConStrGenerator, robjUserInputParametersDic
    Else
        UpdateADOConStrOfDsnlessOracleFromInputDic robjADOConStrOfOdbcOracle.DsnlessAdoConStrGenerator, robjUserInputParametersDic
    End If
End Sub

'''
'''
'''
Public Sub UpdateADOConStrOfDsnlessOracleFromInputDic(ByRef robjADOConStrOfDsnlessOracle As ADOConStrOfDsnlessOracle, ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String, intPortNo As Long
    
    With robjUserInputParametersDic
    
        strKey = "PortNumber"
        
        intPortNo = 0
        
        If .Exists(strKey) Then
        
            intPortNo = .Item(strKey)
        End If
    
        If intPortNo > 0 Then
        
            robjADOConStrOfDsnlessOracle.SetOracleOdbcConnectionWithoutDSN .Item("DriverName"), .Item("ServerHostName"), .Item("NetworkServiceName"), .Item("UserName"), .Item(mstrTemporaryPdKey), intPortNo
        Else
            robjADOConStrOfDsnlessOracle.SetOracleOdbcConnectionWithoutDSN .Item("DriverName"), .Item("ServerHostName"), .Item("NetworkServiceName"), .Item("UserName"), .Item(mstrTemporaryPdKey)
        End If
    End With
End Sub


'''
'''
'''
Public Function GetADOConStrOfDsnlessOracleFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfDsnlessOracle

    Dim objADOConStrOfDsnlessOracle As ADOConStrOfDsnlessOracle
    
    Set objADOConStrOfDsnlessOracle = New ADOConStrOfDsnlessOracle
    
    UpdateADOConStrOfDsnlessOracleFromInputDic objADOConStrOfDsnlessOracle, robjUserInputParametersDic
    
    Set GetADOConStrOfDsnlessOracleFromInputDic = objADOConStrOfDsnlessOracle
End Function

'''
'''
'''
Public Function GetADOConnectionOdbcOracleStringFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As String

    Dim strConnection As String, blnUseDSN As Boolean

    With robjUserInputParametersDic
    
        blnUseDSN = .Item("UseDSN")
        
        If blnUseDSN Then
        
            strConnection = GetADOConStrOfDSNFromInputDic(robjUserInputParametersDic).GetConnectionString()
        Else
            strConnection = GetADOConStrOfDsnlessOracleFromInputDic(robjUserInputParametersDic).GetConnectionString()
        End If
    End With

    GetADOConnectionOdbcOracleStringFromInputDic = strConnection
End Function



'**---------------------------------------------
'** ADO connection string parameters solution interfaces by each special form
'**---------------------------------------------
'''
'''
'''
''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
''' <Argument>robjADOConStrOfOdbcOracle: Output</Argument>
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrNetworkServiceName: Input</Argument>
''' <Argument>vstrUserid: Input</Argument>
''' <Argument>vstrPassword: Input</Argument>
''' <Argument>vintPortNumber: Input</Argument>
''' <Argument>vstrDSN: Input</Argument>
Public Sub SetOdbcConnectionParametersByOracleForm(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
        ByRef robjADOConStrOfOdbcOracle As ADOConStrOfOdbcOracle, _
        ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDriverName As String = "", _
        Optional ByVal vstrServerHostName As String = "", _
        Optional ByVal vstrNetworkServiceName As String = "", _
        Optional ByVal vstrUserid As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber, _
        Optional ByVal vstrDSN As String = "")

    Dim objUserInputParametersDic As Scripting.Dictionary, strPassword As String, blnIsOpeningFormNeeded As Boolean
        
    rblnIsRequestedToCancelAdoConnection = False
        
    blnIsOpeningFormNeeded = IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfOracle(objUserInputParametersDic, vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber, vstrDSN)

    If blnIsOpeningFormNeeded Then
    
        msubSetUserInputParametersDicOfOdbcOracleByOpeningFormWithFixedSettingKeyName rblnIsRequestedToCancelAdoConnection, objUserInputParametersDic, vstrSettingKeyName
    End If

    If Not rblnIsRequestedToCancelAdoConnection Then
    
        UpdateADOConStrOfOdbcOracleFromInputDic robjADOConStrOfOdbcOracle, objUserInputParametersDic
    End If
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
''' <Argument>robjUserInputParametersDic: Input - Output</Argument>
''' <Argument>rstrSettingKeyName: Input</Argument>
Private Sub msubSetUserInputParametersDicOfOdbcOracleByOpeningFormWithFixedSettingKeyName(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        ByRef rstrSettingKeyName As String)


    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr, objUSetAdoOdbcConStrForOracle As USetAdoOdbcConStrForOracle
   
    Set objFormStateToSetAdoConStr = GetFormStateToSetAdoConStrByDefaultParams(robjUserInputParametersDic, rstrSettingKeyName)
   
    Set objUSetAdoOdbcConStrForOracle = New USetAdoOdbcConStrForOracle
    
    With objUSetAdoOdbcConStrForOracle
    
        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
    
        .Show vbModal
    End With

    AfterProcessAdoConStrFormSetting rblnIsRequestedToCancelAdoConnection, robjUserInputParametersDic, objFormStateToSetAdoConStr
End Sub


'''
'''
'''
''' <Argument>robjUserInputParametersDic: Output</Argument>
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrNetworkServiceName: Input</Argument>
''' <Argument>vstrUserid: Input</Argument>
''' <Argument>vstrPassword: Input</Argument>
''' <Argument>vintPortNumber: Input</Argument>
''' <Argument>vstrDSN: Input</Argument>
Private Function IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfOracle(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDriverName As String = "", _
        Optional ByVal vstrServerHostName As String = "", _
        Optional ByVal vstrNetworkServiceName As String = "", _
        Optional ByVal vstrUserid As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber, _
        Optional ByVal vstrDSN As String = "") As Boolean

    Dim blnIsOpeningFormNeeded As Boolean
    
    blnIsOpeningFormNeeded = True

    ' Try to get parameters from Win-registry cache
    
    Set robjUserInputParametersDic = GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(vstrSettingKeyName)

    Select Case True
    
        Case robjUserInputParametersDic Is Nothing, robjUserInputParametersDic.Count = 0
    
            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfOracle(robjUserInputParametersDic, vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber, vstrDSN)
        Case Else
            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache(robjUserInputParametersDic)
    End Select

    IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfOracle = blnIsOpeningFormNeeded
End Function


'''
'''
'''
''' <Argument>robjUserInputParametersDic: Output</Argument>
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrNetworkServiceName: Input</Argument>
''' <Argument>vstrUserid: Input</Argument>
''' <Argument>vstrPassword: Input</Argument>
''' <Argument>vintPortNumber: Input</Argument>
''' <Argument>vstrDSN: Input</Argument>
Private Function IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfOracle(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        Optional ByVal vstrDriverName As String = "", _
        Optional ByVal vstrServerHostName As String = "", _
        Optional ByVal vstrNetworkServiceName As String = "", _
        Optional ByVal vstrUserid As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber, _
        Optional ByVal vstrDSN As String = "") As Boolean


    Dim blnIsOpeningFormNeeded As Boolean
    
    blnIsOpeningFormNeeded = True
    
    If vstrDSN <> "" Then
    
        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToOracle, vstrDSN, vstrUserid, vstrPassword)
    Else
        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfOracle(vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber)
    End If
    
    If vstrPassword <> "" Then
    
        If IsADOConnectionStringEnabled(GetADOConnectionOdbcOracleStringFromInputDic(robjUserInputParametersDic)) Then
        
            blnIsOpeningFormNeeded = False
        End If
    End If

    IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfOracle = blnIsOpeningFormNeeded
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Object creation tests
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCreateADOConStrOfOdbcOracle()

    Dim objADOConStrOfOdbcOracle As ADOConStrOfOdbcOracle, objADOConStrOfDsnlessOracle As ADOConStrOfDsnlessOracle
    
    Set objADOConStrOfOdbcOracle = New ADOConStrOfOdbcOracle
    
    Set objADOConStrOfDsnlessOracle = New ADOConStrOfDsnlessOracle
End Sub


'**---------------------------------------------
'** USetAdoOdbcConStrForOracle form tests
'**---------------------------------------------
'''
''' delete testing SettingKeyName from the registory
'''
Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToOracle()

    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestOracleDSNlessKey"
    
    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestOracleDSNKey"
End Sub


'''
'''
'''
Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForOracleForDSNless()

    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
 
    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfOracle()

    msubSetUserInputParametersDicOfOdbcOracleByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestOracleDSNlessKey"

    If Not blnIsRequestedToCancelAdoConnection Then
    
        If IsADOConnectionStringEnabled(GetADOConStrOfOdbcOracleFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
    
            Debug.Print "Connected to Oracle by ADO"
        End If
    End If
End Sub


'''
''' DSNless Oracle connection
'''
Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForOracleForDSNlessWithLongNotations()


    Dim objUSetAdoOdbcConStrForOracle As USetAdoOdbcConStrForOracle
    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr

    Set objFormStateToSetAdoConStr = New FormStateToSetAdoConStr
    
    With objFormStateToSetAdoConStr
    
        Set .UserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfOracle()       ' A document object of a properties form
    
        .SettingKeyName = "TestOracleDSNlessKey"
    End With
    
    Set objUSetAdoOdbcConStrForOracle = New USetAdoOdbcConStrForOracle
    
    With objUSetAdoOdbcConStrForOracle
    
        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
    
        .Show vbModal
    End With

    If Not objFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection Then

        msubConnectionTestForOdbcOracle objFormStateToSetAdoConStr, True
    End If
End Sub


'''
''' DSN connection to Oracle
'''
Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForOracleForDSN()

    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
 
    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToOracle)

    msubSetUserInputParametersDicOfOdbcOracleByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestOracleDSNKey"

    If Not blnIsRequestedToCancelAdoConnection Then
    
        If IsADOConnectionStringEnabled(GetADOConStrOfOdbcOracleFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
    
            Debug.Print "Connected to Oracle by ADO"
        End If
    End If
End Sub

'''
''' Connection to Oracle with Form
'''
Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForOracleFromSettingCache()


    Dim objUSetAdoOdbcConStrForOracle As USetAdoOdbcConStrForOracle, objFormStateToSetAdoConStr As FormStateToSetAdoConStr
    

    Set objFormStateToSetAdoConStr = New FormStateToSetAdoConStr
    
    With objFormStateToSetAdoConStr
    
        Set .UserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToOracle)       ' A document object of a properties form
    
        .SettingKeyName = ""
    End With

    Set objUSetAdoOdbcConStrForOracle = New USetAdoOdbcConStrForOracle
    
    With objUSetAdoOdbcConStrForOracle
    
        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, False
    
        .Show vbModal
    End With

    If Not objFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection Then
    
        msubConnectionTestForOdbcOracle objFormStateToSetAdoConStr, True
    End If
End Sub



'///////////////////////////////////////////////
'/// Internal functions for sanity tests
'///////////////////////////////////////////////

'''
'''
'''
Private Sub msubConnectionTestForOdbcOracle(ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr, _
        Optional ByVal vblnAllowToWriteAdoConStrInfoSettingIntoWinRegistry As Boolean = False)

    If IsADOConnectionStringEnabled(GetADOConStrOfOdbcOracleFromInputDic(robjFormStateToSetAdoConStr.UserInputParametersDic).GetConnectionString()) Then
    
        Debug.Print "Connected to Oracle by ADO"
    
        If vblnAllowToWriteAdoConStrInfoSettingIntoWinRegistry Then
    
            ' Write setting to Registory
            
            If GetAllowToSaveAdoConStrPasswordInTemporaryCache() Then
            
                UpdateWinRegKeyValuesOfAdoConnectionStringInfoFromAdoConStrFormStateDic robjFormStateToSetAdoConStr
            End If
        End If
    End If
End Sub

