Attribute VB_Name = "CompareOnlySheetsByAdo"
'
'   compare two Excel book about only Worksheet table data, which are not locked by password, using ADODB.connection
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on XlAdoConnector.bas, VariantTypeConversion.bas
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  4/Jun/2023    Kalmclaeyd Tarclanus    Separated from CodeAnalysisVBA.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Using ADO, compare Excel worksheets between two files without opening
'**---------------------------------------------
'''
''' compare two books only sheets, which book is not locked by password
'''
''' This doesn't check auto-shapes on each Worksheet.
'''
''' <Argument>rstrBook01Path: Input</Argument>
''' <Argument>rstrBook02Path: Input</Argument>
''' <Argument>vstrSheetNamesDelimitedByComma: Input</Argument>
''' <Return>Boolean</Return>
Public Function IsDifferentPartBetweenTwoBookSheetsUsingAdo(ByRef rstrBook01Path As String, _
        ByRef rstrBook02Path As String, _
        ByVal vstrSheetNamesDelimitedByComma As String) As Boolean

    Dim strSQL As String, varSheetName As Variant, strSheetName As String, objRSet As ADODB.Recordset
    
    Dim objDT01Col As Collection, objDT02Col As Collection, blnIsDifferent As Boolean
    
    
    blnIsDifferent = False
    
    If vstrSheetNamesDelimitedByComma = "" Then
    
        Debug.Print "!!! Attention: The comparison cannot be able because there is no sheet name information."
    End If
    
    For Each varSheetName In Split(vstrSheetNamesDelimitedByComma, ",")
    
        strSheetName = varSheetName
        
        strSQL = "SELECT * FROM [" & strSheetName & "$]"
        
        Set objDT01Col = Nothing: Set objDT02Col = Nothing
        
        With New XlAdoConnector
        
            .SuppressToShowUpSqlExecutionError = True
        
            .SetInputExcelBookPath rstrBook01Path, vblnBookReadOnly:=True
            
            Set objRSet = .GetRecordset(strSQL)
        
            Set objDT01Col = GetTableCollectionFromRSet(objRSet)
            
            
            .CloseConnection
            
            .SetInputExcelBookPath rstrBook02Path, vblnBookReadOnly:=True
            
            Set objRSet = .GetRecordset(strSQL)
        
            Set objDT02Col = GetTableCollectionFromRSet(objRSet)
        End With
    
        If Not objDT01Col Is Nothing And Not objDT02Col Is Nothing Then
        
            If AreTwoCountOfColumnsOfDTColsExactlyMatched(objDT01Col, objDT02Col) Then
        
                If AreTwoDTColsExactlyMatched(objDT01Col, objDT02Col) Then
                
                    ' matched
                Else
                    blnIsDifferent = True
                End If
            Else
                blnIsDifferent = True
            End If
            
        ElseIf objDT01Col Is Nothing And objDT02Col Is Nothing Then
        
            ' Unknown
        Else
            blnIsDifferent = True
        End If
        
        If blnIsDifferent Then
        
            Exit For
        End If
    Next
    
    IsDifferentPartBetweenTwoBookSheetsUsingAdo = blnIsDifferent
End Function


'**---------------------------------------------
'** Update Excel Book files based on any Worksheet table differences
'**---------------------------------------------
'''
''' Ref. UpdateCopyFilesWithoutDuplicated in FileSysSyncCopy.bas
'''
Public Function UpdateCopyExcelBookFilesOnlyWhenThereAreWorksheetsDifferences(ByVal vstrParentDirectoryPath As String, _
        ByVal vstrSourcePathPart As String, _
        ByVal vstrDestinationPathPart As String, _
        ByVal vstrSheetNamesDelimitedByComma As String, _
        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*.xlsx,*xls", _
        Optional ByVal vblnShowWarningMessageBoxWhenFileSystemDriveTroubles As Boolean = True) As Collection


    Dim objBookPathes As Collection, objCopiedFilePaths As Collection, objDummayLogDTCol As Collection
        

    Set objCopiedFilePaths = New Collection

    If AreBothDrivesReady(vstrSourcePathPart, vstrDestinationPathPart, vblnShowWarningMessageBoxWhenFileSystemDriveTroubles) Then
    
        ' use VBA.Dir
        Set objBookPathes = GetFilePathsUsingVbaDir(vstrParentDirectoryPath, vstrRawWildCardsByDelimitedChar)
    
        Set objCopiedFilePaths = CopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences(objDummayLogDTCol, _
                objBookPathes, _
                vstrSourcePathPart, _
                vstrDestinationPathPart, _
                vstrSheetNamesDelimitedByComma)
    End If
    
    Set UpdateCopyExcelBookFilesOnlyWhenThereAreWorksheetsDifferences = objCopiedFilePaths
End Function


'''
''' ref. CopyFilesWithKeepingDirectoryStructure in FileSysCopyProgressBar.bas
'''
''' <Argument>robjLogDTCol: Output - loggings when checking diff</Argument>
''' <Argument>vobjBookPaths: Input - Excel book full paths</Argument>
''' <Argument>vstrSourceParentDirectoryPathPart: Input</Argument>
''' <Argument>vstrDestinationParentDirectoryPathPart: Input</Argument>
''' <Argument>vstrSheetNamesDelimitedByComma: Input - checking sheet names delimited by comma character</Argument>
''' <Return>Collection: copied file paths</Return>
Public Function CopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences(ByRef robjLogDTCol As Collection, _
        ByVal vobjBookPaths As Collection, _
        ByVal vstrSourceParentDirectoryPathPart As String, _
        ByVal vstrDestinationParentDirectoryPathPart As String, _
        ByVal vstrSheetNamesDelimitedByComma As String) As Collection


    Dim objCopiedFilePaths As Collection, objLogDTCol As Collection, objDoCopySrcToDstDic As Scripting.Dictionary
    
    
    Set objLogDTCol = GetListsOfCopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences(objDoCopySrcToDstDic, _
            vobjBookPaths, _
            vstrSourceParentDirectoryPathPart, _
            vstrDestinationParentDirectoryPathPart, _
            vstrSheetNamesDelimitedByComma)

    Set objCopiedFilePaths = CopyFilesWithSimpleProgressBar(objDoCopySrcToDstDic, New Scripting.FileSystemObject)

    Set robjLogDTCol = objLogDTCol

    Set CopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences = objCopiedFilePaths
End Function


'''
''' ref. GetListsOfCopyFilesWithKeepingDirectoryStructure in FileSysSyncCopy.bas
'''
''' <Argument>robjDoCopySrcToDstDic: Output</Argument>
''' <Argument>vobjBookPaths: Input</Argument>
''' <Argument>vstrSourceParentDirectoryPathPart: Input</Argument>
''' <Argument>vstrDestinationParentDirectoryPathPart: Input</Argument>
''' <Argument>vstrSheetNamesDelimitedByComma: Input - checking sheet names delimited by comma character</Argument>
''' <Return>Collection</Return>
Public Function GetListsOfCopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences(ByRef robjDoCopySrcToDstDic As Scripting.Dictionary, _
        ByVal vobjBookPaths As Collection, _
        ByVal vstrSourceParentDirectoryPathPart As String, _
        ByVal vstrDestinationParentDirectoryPathPart As String, _
        ByVal vstrSheetNamesDelimitedByComma As String) As Collection


    Dim objFS As Scripting.FileSystemObject
    
    Dim varPath As Variant, strSourcePath As String, strDestinationDir As String, strDestinationPath As String
    
    Dim blnDoCopy As Boolean, objLogRowCol As Collection, objLogDTCol As Collection
    
    Dim enmFileCopySituation As FileSyncCopySituation, varCompoundKey As Variant


    Set objFS = New Scripting.FileSystemObject
    
    If robjDoCopySrcToDstDic Is Nothing Then Set robjDoCopySrcToDstDic = New Scripting.Dictionary
    
    Set objLogDTCol = New Collection
    
    For Each varPath In vobjBookPaths
    
        strSourcePath = varPath
    
        If InStr(1, strSourcePath, vstrSourceParentDirectoryPathPart) > 0 Then
    
            With objFS
            
                strDestinationDir = .GetParentFolderName(Replace(strSourcePath, _
                        vstrSourceParentDirectoryPathPart, _
                        vstrDestinationParentDirectoryPathPart))
            
                strDestinationPath = strDestinationDir & "\" & .GetFileName(strSourcePath)
                
                blnDoCopy = mfblnIsNeedToCopyOnlyWhenThereAreWorksheetsDifferences(enmFileCopySituation, _
                        objLogRowCol, _
                        strSourcePath, _
                        strDestinationPath, _
                        vstrSheetNamesDelimitedByComma, _
                        objFS)
                
                If Not objLogRowCol Is Nothing Then
                
                    objLogDTCol.Add objLogRowCol
                End If
            End With
    
            If blnDoCopy Then
                
                ForceToCreateDirectory strDestinationDir, objFS
                
                varCompoundKey = strSourcePath & ";" & CStr(enmFileCopySituation)
                
                robjDoCopySrcToDstDic.Add varCompoundKey, strDestinationDir & "\"
            End If
        End If
    Next

    Set GetListsOfCopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences = objLogDTCol
End Function


'''
''' ref. mfblnIsNeedToCopy in FileSysSyncCopy.bas
'''
''' <Argument>renmFileCopySituation: Output</Argument>
''' <Argument>robjLogRowCol: Output</Argument>
''' <Argument>vstrSourcePath: Input</Argument>
''' <Argument>vstrDestinationPath: Input</Argument>
''' <Argument>vstrSheetNamesDelimitedByComma: Input</Argument>
''' <Argument>vobjFS: Input</Argument>
Private Function mfblnIsNeedToCopyOnlyWhenThereAreWorksheetsDifferences(ByRef renmFileCopySituation As FileSyncCopySituation, _
        ByRef robjLogRowCol As Collection, _
        ByVal vstrSourcePath As String, _
        ByVal vstrDestinationPath As String, _
        ByVal vstrSheetNamesDelimitedByComma As String, _
        ByVal vobjFS As Scripting.FileSystemObject) As Boolean


    Dim blnDoCopy As Boolean, intSecondDiff As Long, intDayDiff As Long
    
    
    Const intEpsilonOfSecond As Long = 5    ' When the Window copy any file in special combination of an internal HDD and an external HDD, each file timestamp are always shifted between one and two about each file timestamp.
    
    blnDoCopy = False
                
    With vobjFS
                
        If .FileExists(vstrDestinationPath) Then
    
            Set robjLogRowCol = Nothing
    
            ' compare only data-tables on sheets. the auto-shapes on each sheet and modified-last-date and office-file-built-in properties are ignored
            
            If Not IsDifferentPartBetweenTwoBookSheetsUsingAdo(vstrSourcePath, vstrDestinationPath, vstrSheetNamesDelimitedByComma) Then
            
                blnDoCopy = False
            Else
                blnDoCopy = True
                
                renmFileCopySituation = UpdateFileToFileSystemSync
            End If
            
            'msubLogOfComparingDateLastModifiedBetweenSourceAndDestination robjLogRowCol, objSourceFile, objDestinationFile, blnDoCopy, intSecondDiff, intDayDiff
        Else
            blnDoCopy = True
            
            renmFileCopySituation = CopyFileNewToFileSystemSync
        End If
    End With

    mfblnIsNeedToCopyOnlyWhenThereAreWorksheetsDifferences = blnDoCopy
End Function


