Attribute VB_Name = "ADOParameters"
'
'   ADO constant parameters definitions, however this is a common library codes in order to localize various captions in this system
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrModuleName As String = "ADOParameters"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Key-Value cache preparation for ADOParameters
'**---------------------------------------------
Private mobjStrKeyValueADOParametersDic As Scripting.Dictionary

Private mblnIsStrKeyValueADOParametersDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.

' prevent duplicated calling
Private mintSemaphoreInitializingStrKeyValueADOParametersDic As Long

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'**---------------------------------------------
'** ADODB.Connection.ConnectionString parameter enumerations
'**---------------------------------------------
'''
''' ConnectionString generation mode
'''
Public Enum ADOConStrGenerationMode

    ODBCDSNConStrType = 1
    
    ODBCOracleWithoutDSNConStrType  ' DSNless connection for Oracle
    
    ODBCPgSQLWithoutDSNConStrType   ' DSNless connection for PostgreSQL
    
    
    OLEDBOracleConStrType
    
    OLEDBPgSQLConStrType
    
    XlBookConnectionType
    
    AccessDBConnectionType
    
    CsvFileConnectionType
End Enum

'''
''' ADO parameters for the OLE-DB connection to Microsoft Access
'''
Public Enum AccessOLEDBProviderType
    
    JetOLEDB_3P51 = 1   ' this stands for Microsoft.Jet.OLEDB.3.5.1
    
    JetOLEDB_4P0 = 2    ' this stands for Microsoft.Jet.OLEDB.4.0
    
    AceOLEDB_12P0 = 3   ' this stands for Microsoft.ACE.OLEDB.12.0
    
    AceOLEDB_16P0 = 4   ' this stands for Microsoft.ACE.OLEDB.16.0
End Enum


'''
''' ADO parameters for the OLE-DB connection to Microsoft Excel
''' ADO connection-string option, IMEX, when open the Excel sheet with ADO, the following constants can be used
'''
Public Enum IMEXMode
    
    NoUseIMEXMode = -1
    
    IMEXExport = 0  ' Export mode
    
    IMEXImport = 1  ' Import mode    「IMEX=1;」  "intermixed" all columns are searved as String type data.
    
    IMEXLink = 2    ' Link mode       Able to use 'UPDATE' SQL command, this is applied to not-opened Excel book file
End Enum



'''
''' This is refered from ADOConStrOfDsn.cls
'''
Public Enum OdbcConnectionDestinationRDBType

    OdbcConnectionDestinationToPostgreSQL = 1
    
    OdbcConnectionDestinationToOracle = 4

    OdbcConnectionDestinationToSQLite3 = 5
End Enum

'''
'''
'''
Public Enum OleDbOrOdbcAdoConnectionDestinationRDBType

    AdoOdbcAndPostgreSQL = OdbcConnectionDestinationRDBType.OdbcConnectionDestinationToPostgreSQL

    AdoOdbcAndOracle = OdbcConnectionDestinationRDBType.OdbcConnectionDestinationToOracle

    AdoOdbcAndSQLite = OdbcConnectionDestinationRDBType.OdbcConnectionDestinationToSQLite3
    
    AdoOleDbAndMsAccessDb = 31  ' "OLE DB Ms Access"
    
    AdoOleDbAndMsExcel = 32 ' "OLE DB Ms Excel"
    
    AdoOleDbAndCsv = 33 ' "OLE DB Csv"
End Enum

'''
'''
'''
Public Enum ShowUpAdoErrorOptionFlag

    NoControlToShowUpAdoSqlErrorFlag = 0

    SuppressToShowUpAdoSqlErrorByFormFlag = 1
    
    SuppressToShowUpAdoSqlErrorByExcelSheetFlag = 2

    SuppressToShowUpAdoSqlErrorByDebugPrintFlag = 4

    SuppressToShowUpAdoSqlErrorFlag = SuppressToShowUpAdoSqlErrorByFormFlag Or SuppressToShowUpAdoSqlErrorByExcelSheetFlag
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub InitializeTextForADOParametersBeforeADOSQLExecution()

    msubInitializeTextForADOParameters
End Sub

'**---------------------------------------------
'** About AccessOLEDBProviderType enumeration
'**---------------------------------------------
'''
'''
'''
Public Function GetAccessOLEDBProviderTypeEnums() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add AccessOLEDBProviderType.JetOLEDB_3P51
    
        .Add AccessOLEDBProviderType.JetOLEDB_4P0
        
        .Add AccessOLEDBProviderType.AceOLEDB_12P0
        
        .Add AccessOLEDBProviderType.AceOLEDB_16P0
    End With

    Set GetAccessOLEDBProviderTypeEnums = objCol
End Function

'''
'''
'''
Public Function GetAccessOLEDBProviderNameFromEnm(ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType) As String
    
    Dim strProviderStr As String
    
    Select Case venmAccessOLEDBProviderType
    
        Case AccessOLEDBProviderType.JetOLEDB_3P51
        
            strProviderStr = "Microsoft.Jet.OLEDB.3.5.1"    ' No use, ISAM error occurs when it is used
            
        Case AccessOLEDBProviderType.JetOLEDB_4P0
        
            strProviderStr = "Microsoft.Jet.OLEDB.4.0"      ' for Office97, Office2000-2003
            
        Case AccessOLEDBProviderType.AceOLEDB_12P0
        
            strProviderStr = "Microsoft.ACE.OLEDB.12.0"     ' for Office2007 or lator (- Office2019)
            
        Case AccessOLEDBProviderType.AceOLEDB_16P0
        
            strProviderStr = "Microsoft.ACE.OLEDB.16.0"     ' This can be used in Excel 2021 of Windows 11 64 bit
    End Select
    
    GetAccessOLEDBProviderNameFromEnm = strProviderStr
End Function

'''
'''
'''
Public Function GetAccessOLEDBProviderEnmFromName(ByVal vstrName As String) As AccessOLEDBProviderType
    
    Dim enmAccessOLEDBProviderType As AccessOLEDBProviderType
    
    Select Case vstrName
    
        Case "Microsoft.Jet.OLEDB.3.5.1"
    
            enmAccessOLEDBProviderType = JetOLEDB_3P51
    
        Case "Microsoft.Jet.OLEDB.4.0"
    
            enmAccessOLEDBProviderType = JetOLEDB_4P0
    
        Case "Microsoft.ACE.OLEDB.12.0"
        
            enmAccessOLEDBProviderType = AceOLEDB_12P0
        
        Case "Microsoft.ACE.OLEDB.16.0"
    
            enmAccessOLEDBProviderType = AceOLEDB_16P0
    End Select
    
    GetAccessOLEDBProviderEnmFromName = enmAccessOLEDBProviderType
End Function

'''
'''
'''
Public Function GetAccessOLEDBProviderDescriptionFromEnm(ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType) As String

    Dim strProviderDescription As String

    Select Case venmAccessOLEDBProviderType
    
        Case AccessOLEDBProviderType.JetOLEDB_3P51
        
            strProviderDescription = "Microsoft Office 3.5 Jet Database Engine OLE DB Provider"    ' No use, ISAM error occurs when it is used
            
        Case AccessOLEDBProviderType.JetOLEDB_4P0
        
            strProviderDescription = "Microsoft Office 4.0 Jet Database Engine OLE DB Provider"      ' for Office97, Office2000-2003
            
        Case AccessOLEDBProviderType.AceOLEDB_12P0
        
            strProviderDescription = "Microsoft Office 12.0 Access Database Engine OLE DB Provider"     ' for Office2007 or lator (- Office2019)
            
        Case AccessOLEDBProviderType.AceOLEDB_16P0
        
            strProviderDescription = "Microsoft Office 16.0 Access Database Engine OLE DB Provider"     ' This can be used in Excel 2021 of Windows 11 64 bit
    End Select

    GetAccessOLEDBProviderDescriptionFromEnm = strProviderDescription
End Function


'**---------------------------------------------
'** Connection String Parts for ADODB
'**---------------------------------------------
'''
'''
'''
Public Function GetAdoIMEXModeEnums() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add IMEXMode.NoUseIMEXMode
        
        .Add IMEXMode.IMEXExport
        
        .Add IMEXMode.IMEXImport
        
        .Add IMEXMode.IMEXLink
    End With
    
    Set GetAdoIMEXModeEnums = objCol
End Function


'''
'''
'''
Public Function GetAdoIMEXModeNameFromEnm(ByRef renmIMEXMode As IMEXMode) As String

    Dim strName As String
    
    Select Case renmIMEXMode
    
        Case IMEXMode.NoUseIMEXMode
        
            strName = "No use IMEX mode"
        
        Case IMEXMode.IMEXExport
        
            strName = "Export mode"
            
        Case IMEXMode.IMEXImport
        
            strName = "Import mode"
            
        Case IMEXMode.IMEXLink
    
            strName = "Link mode"
    End Select

    GetAdoIMEXModeNameFromEnm = strName
End Function

'''
'''
'''
Public Function GetAdoIMEXModeEnmFromName(ByRef rstrName As String) As IMEXMode

    Dim enmIMEXMode As IMEXMode
    
    Select Case rstrName
    
        Case "No use IMEX mode"
    
            enmIMEXMode = NoUseIMEXMode
    
        Case "Export mode"
    
            enmIMEXMode = IMEXExport
     
        Case "Import mode"
    
            enmIMEXMode = IMEXImport
            
        Case "Link mode"
    
            enmIMEXMode = IMEXLink
    End Select

    GetAdoIMEXModeEnmFromName = enmIMEXMode
End Function


'**---------------------------------------------
'** get literal string for each enumeration
'**---------------------------------------------
Public Function GetADOEnumLiteralFromCursorLocation(ByVal venmCursorLocationEnum As ADODB.CursorLocationEnum) As String
    
    Dim strLiteral As String

    Select Case venmCursorLocationEnum
    
        Case ADODB.CursorLocationEnum.adUseServer
            
            strLiteral = "adUseServer"
            
        Case ADODB.CursorLocationEnum.adUseClient
            
            strLiteral = "adUseClient"
    End Select

    GetADOEnumLiteralFromCursorLocation = strLiteral
End Function

Public Function GetADOEnumLiteralFromCommandType(ByVal venmCommandTypeEnum As ADODB.CommandTypeEnum) As String
    
    Dim strLiteral As String
    
    Select Case venmCommandTypeEnum
        
        Case ADODB.CommandTypeEnum.adCmdUnknown
            
            strLiteral = "adCmdUnknown"
        
        Case ADODB.CommandTypeEnum.adCmdText
            
            strLiteral = "adCmdText"
        
        Case ADODB.CommandTypeEnum.adCmdTable
            
            strLiteral = "adCmdTable"
        
        Case ADODB.CommandTypeEnum.adCmdTableDirect
            
            strLiteral = "adCmdTableDirect"
        
        Case ADODB.CommandTypeEnum.adCmdStoredProc
            
            strLiteral = "adCmdStoredProc"
        
        Case ADODB.CommandTypeEnum.adCmdFile
            
            strLiteral = "adCmdFile"
    End Select

    GetADOEnumLiteralFromCommandType = strLiteral
End Function


Public Function GetADOEnumLiteralFromExecuteOption(ByVal venmExecutionOptionEnum As ADOR.ExecuteOptionEnum) As String
    
    Dim strLiteral As String
    
    Select Case venmExecutionOptionEnum
        
        Case ADOR.ExecuteOptionEnum.adAsyncExecute
            
            strLiteral = "adAsyncExecute"
        
        Case ADOR.ExecuteOptionEnum.adAsyncFetch
            
            strLiteral = "adAsyncFetch"
        
        Case ADOR.ExecuteOptionEnum.adAsyncFetchNonBlocking
            
            strLiteral = "adAsyncFetchNonBlocking"
        
        Case ADOR.ExecuteOptionEnum.adExecuteNoRecords
            
            strLiteral = "adExecuteNoRecords"
        
        Case ADOR.ExecuteOptionEnum.adExecuteRecord
            
            strLiteral = "adExecuteRecord"
        
        Case ADOR.ExecuteOptionEnum.adExecuteStream
            
            strLiteral = "adExecuteStream"
        
        Case ADOR.ExecuteOptionEnum.adOptionUnspecified
            
            strLiteral = "adOptionUnspecified"
    End Select

    GetADOEnumLiteralFromExecuteOption = strLiteral
End Function


Public Function GetADOEnumLiteralFromRecordsetCursorType(ByVal venmCursorTypeEnum As ADOR.CursorTypeEnum) As String
    
    Dim strLiteral As String
    
    Select Case venmCursorTypeEnum
        
        Case ADOR.CursorTypeEnum.adOpenForwardOnly
            
            strLiteral = "adOpenForwardOnly"
        
        Case ADOR.CursorTypeEnum.adOpenKeyset
            
            strLiteral = "adOpenKeyset"
        
        Case ADOR.CursorTypeEnum.adOpenDynamic
            
            strLiteral = "adOpenDynamic"
        
        Case ADOR.CursorTypeEnum.adOpenStatic
            
            strLiteral = "adOpenStatic"
        
        Case ADOR.CursorTypeEnum.adOpenUnspecified
            
            strLiteral = "adOpenUnspecified"
    End Select
    
    GetADOEnumLiteralFromRecordsetCursorType = strLiteral
End Function

'''
''' About ADOR.LockTypeEnum
'''
Public Function GetADOEnumLiteralFromRecordsetLockType(ByVal venmLockTypeEnum As ADOR.LockTypeEnum) As String
    
    Dim strLiteral As String

    Select Case venmLockTypeEnum
        
        Case ADOR.LockTypeEnum.adLockReadOnly
            
            strLiteral = "adLockReadOnly"
        
        Case ADOR.LockTypeEnum.adLockPessimistic
            
            strLiteral = "adLockPessimistic"
        
        Case ADOR.LockTypeEnum.adLockOptimistic
            
            strLiteral = "adLockOptimistic"
        
        Case ADOR.LockTypeEnum.adLockBatchOptimistic
            
            strLiteral = "adLockBatchOptimistic"
        
        Case ADOR.LockTypeEnum.adLockUnspecified
            
            strLiteral = "adLockUnspecified"
    End Select

    GetADOEnumLiteralFromRecordsetLockType = strLiteral
End Function

'''
'''
'''
Public Function GetADOEnumLiteralFromObjectStateEnum(ByVal venmObjectStateEnum As ADODB.ObjectStateEnum) As String

    Dim strLiteral As String
    
    Select Case venmObjectStateEnum
    
        Case ADODB.ObjectStateEnum.adStateClosed
        
            strLiteral = "adStateClosed"
        
        Case ADODB.ObjectStateEnum.adStateConnecting
        
            strLiteral = "adStateConnecting"
        
        Case ADODB.ObjectStateEnum.adStateExecuting
        
            strLiteral = "adStateExecuting"
            
        Case ADODB.ObjectStateEnum.adStateFetching
        
            strLiteral = "adStateFetching"
            
        Case ADODB.ObjectStateEnum.adStateOpen
    
            strLiteral = "adStateOpen"
    End Select
    
    GetADOEnumLiteralFromObjectStateEnum = strLiteral
End Function


'**---------------------------------------------
'** ODBC type string conversion
'**---------------------------------------------
'''
'''
'''
Public Function GetDescriptionOfOdbcConnectionDestinationRDBType(ByRef renmOdbcConnectionDestinationRDBType As OdbcConnectionDestinationRDBType) As String

    Dim strDescription As String
    
    Select Case renmOdbcConnectionDestinationRDBType
    
        Case OdbcConnectionDestinationRDBType.OdbcConnectionDestinationToPostgreSQL
        
            strDescription = "ODBC PostgreSQL"
        
        Case OdbcConnectionDestinationRDBType.OdbcConnectionDestinationToOracle
        
            strDescription = "ODBC Oracle"
        
        Case OdbcConnectionDestinationRDBType.OdbcConnectionDestinationToSQLite3
        
            strDescription = "ODBC SQLite"
    End Select
    
    GetDescriptionOfOdbcConnectionDestinationRDBType = strDescription
End Function

'''
'''
'''
Public Function GetEnumOfOdbcConnectionDestinationRDBTypeFromDescription(ByRef rstrDescription As String) As OdbcConnectionDestinationRDBType

    Dim enmOdbcConnectionDestinationRDBType As OdbcConnectionDestinationRDBType
    
    Select Case rstrDescription
    
        Case "ODBC PostgreSQL"
    
            enmOdbcConnectionDestinationRDBType = OdbcConnectionDestinationToPostgreSQL
            
        Case "ODBC Oracle"
    
            enmOdbcConnectionDestinationRDBType = OdbcConnectionDestinationToOracle
            
        Case "ODBC SQLite"
        
            enmOdbcConnectionDestinationRDBType = OdbcConnectionDestinationToSQLite3
    End Select
    
    GetEnumOfOdbcConnectionDestinationRDBTypeFromDescription = enmOdbcConnectionDestinationRDBType
End Function

'''
'''
'''
Public Function GetDescriptionOfOleDbOrOdbcAdoConnectionDestinationRDBType(ByRef renmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType) As String

    Dim strDescription As String
    
    Select Case renmOleDbOrOdbcAdoConnectionDestinationRDBType
    
        Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOdbcAndPostgreSQL, OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOdbcAndOracle, OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOdbcAndSQLite
    
            strDescription = GetDescriptionOfOdbcConnectionDestinationRDBType(CInt(renmOleDbOrOdbcAdoConnectionDestinationRDBType))
    
        Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsAccessDb
    
            strDescription = "OLE DB Ms Access"
        
        Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsExcel
    
            strDescription = "OLE DB Ms Excel"
    
        Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndCsv
        
            strDescription = "OLE DB Csv"
    End Select
    
    GetDescriptionOfOleDbOrOdbcAdoConnectionDestinationRDBType = strDescription
End Function

'''
'''
'''
Public Function GetEnumOfOleDbOrOdbcAdoConnectionDestinationRDBTypeFromDescription(ByRef rstrDescription As String) As OleDbOrOdbcAdoConnectionDestinationRDBType

    Dim enmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType
    
    If InStr(1, rstrDescription, "ODBC") > 0 Then
    
        enmOleDbOrOdbcAdoConnectionDestinationRDBType = CInt(GetEnumOfOdbcConnectionDestinationRDBTypeFromDescription(rstrDescription))
    Else
        Select Case rstrDescription
        
            Case "OLE DB Ms Access"
        
                enmOleDbOrOdbcAdoConnectionDestinationRDBType = AdoOleDbAndMsAccessDb
        
            Case "OLE DB Ms Excel"
        
                enmOleDbOrOdbcAdoConnectionDestinationRDBType = AdoOleDbAndMsExcel
        
            Case "OLE DB Csv"
        
                enmOleDbOrOdbcAdoConnectionDestinationRDBType = AdoOleDbAndCsv
        End Select
    End If
    
    GetEnumOfOleDbOrOdbcAdoConnectionDestinationRDBTypeFromDescription = enmOleDbOrOdbcAdoConnectionDestinationRDBType
End Function

'**---------------------------------------------
'** Key-Value cache preparation for ADOParameters
'**---------------------------------------------
'''
''' get string Value from STR_KEY_ADO_PARAMETERS_SQL_EXECUTION_ELAPSED_TIME
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersSqlExecutionElapsedTime() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersSqlExecutionElapsedTime = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_SQL_EXECUTION_ELAPSED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADO_PARAMETERS_TABLE_RECORD_COUNT
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersTableRecordCount() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersTableRecordCount = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_TABLE_RECORD_COUNT")
End Function

'''
''' get string Value from STR_KEY_ADO_PARAMETERS_TABLE_FIELD_COUNT
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersTableFieldCount() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersTableFieldCount = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_TABLE_FIELD_COUNT")
End Function

'''
''' get string Value from STR_KEY_ADO_PARAMETERS_SQL_EXECUTED_TIME
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersSqlExecutedTime() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersSqlExecutedTime = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_SQL_EXECUTED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADO_PARAMETERS_UNIT_SECOND
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersUnitSecond() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersUnitSecond = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_UNIT_SECOND")
End Function

'''
''' get string Value from STR_KEY_ADO_PARAMETERS_RECORDS_AFFECTED
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersRecordsAffected() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersRecordsAffected = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_RECORDS_AFFECTED")
End Function

'''
''' get string Value from STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_DELETE
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersCountOfSqlDelete() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersCountOfSqlDelete = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_DELETE")
End Function

'''
''' get string Value from STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_UPDATE
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersCountOfSqlUpdate() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersCountOfSqlUpdate = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_UPDATE")
End Function

'''
''' get string Value from STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_INSERT
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersCountOfSqlInsert() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersCountOfSqlInsert = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_INSERT")
End Function

'''
''' get string Value from STR_KEY_ADO_PARAMETERS_DELETING_ELAPSED_TIME
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersDeletingElapsedTime() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersDeletingElapsedTime = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_DELETING_ELAPSED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADO_PARAMETERS_UPDATING_ELAPSED_TIME
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersUpdatingElapsedTime() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersUpdatingElapsedTime = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_UPDATING_ELAPSED_TIME")
End Function

'''
''' get string Value from STR_KEY_ADO_PARAMETERS_INSERTING_ELAPSED_TIME
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Public Function GetTextOfStrKeyAdoParametersInsertingElapsedTime() As String

    If mobjStrKeyValueADOParametersDic Is Nothing Then mblnIsStrKeyValueADOParametersDicInitialized = False

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        msubInitializeTextForADOParameters
    End If

    GetTextOfStrKeyAdoParametersInsertingElapsedTime = mobjStrKeyValueADOParametersDic.Item("STR_KEY_ADO_PARAMETERS_INSERTING_ELAPSED_TIME")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Private Sub msubInitializeTextForADOParameters()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueADOParametersDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForADOParametersByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForADOParametersByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueADOParametersDicInitialized = True
    End If

    Set mobjStrKeyValueADOParametersDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for ADOParameters key-values cache
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Private Sub AddStringKeyValueForADOParametersByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ADO_PARAMETERS_SQL_EXECUTION_ELAPSED_TIME", "SQL execution elapsed time"
        .Add "STR_KEY_ADO_PARAMETERS_TABLE_RECORD_COUNT", "record count"
        .Add "STR_KEY_ADO_PARAMETERS_TABLE_FIELD_COUNT", "field count"
        .Add "STR_KEY_ADO_PARAMETERS_SQL_EXECUTED_TIME", "SQL executed time"
        .Add "STR_KEY_ADO_PARAMETERS_UNIT_SECOND", "[s]"
        .Add "STR_KEY_ADO_PARAMETERS_RECORDS_AFFECTED", "Records affected"
        .Add "STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_DELETE", "Count of deleted records"
        .Add "STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_UPDATE", "Count of updated records"
        .Add "STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_INSERT", "Count of inserted records"
        .Add "STR_KEY_ADO_PARAMETERS_DELETING_ELAPSED_TIME", "Deleting elapsed time"
        .Add "STR_KEY_ADO_PARAMETERS_UPDATING_ELAPSED_TIME", "Updating elapsed time"
        .Add "STR_KEY_ADO_PARAMETERS_INSERTING_ELAPSED_TIME", "Inserting elapsed time"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for ADOParameters key-values cache
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Private Sub AddStringKeyValueForADOParametersByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ADO_PARAMETERS_SQL_EXECUTION_ELAPSED_TIME", "SQL実行経過時間"
        .Add "STR_KEY_ADO_PARAMETERS_TABLE_RECORD_COUNT", "レコード数"
        .Add "STR_KEY_ADO_PARAMETERS_TABLE_FIELD_COUNT", "列数"
        .Add "STR_KEY_ADO_PARAMETERS_SQL_EXECUTED_TIME", "SQL実行の日付時刻"
        .Add "STR_KEY_ADO_PARAMETERS_UNIT_SECOND", "[秒]"
        .Add "STR_KEY_ADO_PARAMETERS_RECORDS_AFFECTED", "変更のあったレコード数"
        .Add "STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_DELETE", "データ削除数(DELETE: Records affected)"
        .Add "STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_UPDATE", "データ更新数(UPDATE: Records affected)"
        .Add "STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_INSERT", "データ追加数(INSERT: Records affected)"
        .Add "STR_KEY_ADO_PARAMETERS_DELETING_ELAPSED_TIME", "削除経過時間"
        .Add "STR_KEY_ADO_PARAMETERS_UPDATING_ELAPSED_TIME", "更新経過時間"
        .Add "STR_KEY_ADO_PARAMETERS_INSERTING_ELAPSED_TIME", "挿入経過時間"
    End With
End Sub

'''
''' Remove Keys for ADOParameters key-values cache
'''
''' automatically-added for ADOParameters string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueADOParameters(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_ADO_PARAMETERS_SQL_EXECUTION_ELAPSED_TIME") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_ADO_PARAMETERS_SQL_EXECUTION_ELAPSED_TIME"
            .Remove "STR_KEY_ADO_PARAMETERS_TABLE_RECORD_COUNT"
            .Remove "STR_KEY_ADO_PARAMETERS_TABLE_FIELD_COUNT"
            .Remove "STR_KEY_ADO_PARAMETERS_SQL_EXECUTED_TIME"
            .Remove "STR_KEY_ADO_PARAMETERS_UNIT_SECOND"
            .Remove "STR_KEY_ADO_PARAMETERS_RECORDS_AFFECTED"
            .Remove "STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_DELETE"
            .Remove "STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_UPDATE"
            .Remove "STR_KEY_ADO_PARAMETERS_COUNT_OF_SQL_INSERT"
            .Remove "STR_KEY_ADO_PARAMETERS_DELETING_ELAPSED_TIME"
            .Remove "STR_KEY_ADO_PARAMETERS_UPDATING_ELAPSED_TIME"
            .Remove "STR_KEY_ADO_PARAMETERS_INSERTING_ELAPSED_TIME"
        End If
    End With
End Sub


