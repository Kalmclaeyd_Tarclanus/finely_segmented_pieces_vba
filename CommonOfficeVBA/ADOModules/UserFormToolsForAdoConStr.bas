Attribute VB_Name = "UserFormToolsForAdoConStr"
'
'   User form support functions for setting ADO Connection string parameters form
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 20/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "UserFormToolsForAdoConStr"


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////



'**---------------------------------------------
'** Key-Value cache preparation for UserFormToolsForAdoConStr
'**---------------------------------------------
Private mobjStrKeyValueUserFormToolsForAdoConStrDic As Scripting.Dictionary

Private mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About Ms Access OLE DB ComboBox
'**---------------------------------------------
'''
'''
'''
Public Function InitializeCombBoxOfAceOleDb(ByRef rcboUsingAccdbOleDb As MSForms.ComboBox) As Scripting.Dictionary

    Dim varAccessOLEDBProviderType As Variant, enmAccessOLEDBProviderType As AccessOLEDBProviderType, objEnumValueToListIndexDic As Scripting.Dictionary, i As Long

    With rcboUsingAccdbOleDb
    
        .Clear
        
        Set objEnumValueToListIndexDic = New Scripting.Dictionary
        
        i = 0
        
        For Each varAccessOLEDBProviderType In GetAccessOLEDBProviderTypeEnums()
        
            enmAccessOLEDBProviderType = varAccessOLEDBProviderType
            
            .AddItem GetAccessOLEDBProviderNameFromEnm(enmAccessOLEDBProviderType)
            
            objEnumValueToListIndexDic.Add enmAccessOLEDBProviderType, i
            
            i = i + 1
        Next
    
        .ListIndex = objEnumValueToListIndexDic.Item(AccessOLEDBProviderType.AceOLEDB_12P0)
    End With
    
    Set InitializeCombBoxOfAceOleDb = objEnumValueToListIndexDic
End Function

'''
'''
'''
Public Function InitializeCombBoxOfAdoIMEXMode(ByRef rcboIMEXMode As MSForms.ComboBox) As Scripting.Dictionary

    Dim varIMEXMode As Variant, enmIMEXMode As IMEXMode, objEnumValueToListIndexDic As Scripting.Dictionary, i As Long

    With rcboIMEXMode
    
        .Clear
    
        Set objEnumValueToListIndexDic = New Scripting.Dictionary
        
        i = 0
    
        For Each varIMEXMode In GetAdoIMEXModeEnums()
    
            enmIMEXMode = varIMEXMode
    
            .AddItem GetAdoIMEXModeNameFromEnm(enmIMEXMode)
            
            objEnumValueToListIndexDic.Add enmIMEXMode, i
            
            i = i + 1
        Next
        
        .ListIndex = objEnumValueToListIndexDic.Item(IMEXMode.NoUseIMEXMode)
    End With
    
    Set InitializeCombBoxOfAdoIMEXMode = objEnumValueToListIndexDic
End Function


'**---------------------------------------------
'** About initializing UI
'**---------------------------------------------
'''
''' For PostgreSQL, Oracle
'''
Public Sub InitializeSavePasswordTemporaryCacheCheckBox(ByRef rchkSavePasswordTemporaryCache As MSForms.CheckBox)

    If IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() Then

        rchkSavePasswordTemporaryCache.Value = GetAllowToSaveAdoConStrPasswordInTemporaryCache()
    Else
        With rchkSavePasswordTemporaryCache
        
            .Value = False
            
            .Enabled = False
        End With
    End If
End Sub

'''
''' For Ms Access
'''
Public Sub InitializeSavePasswordTemporaryCacheCheckBoxForMsAccess(ByRef rchkSavePasswordTemporaryCache As MSForms.CheckBox)

    If IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled() Then

        rchkSavePasswordTemporaryCache.Value = GetAllowToSaveAdoConStrPasswordInTemporaryCache()
    Else
        With rchkSavePasswordTemporaryCache
        
            .Value = False
            
            .Enabled = False
        End With
    End If
End Sub

'''
''' For Ms Excel
'''
Public Sub InitializeSavePasswordTemporaryCacheCheckBoxForMsExcel(ByRef rchkSavePasswordTemporaryCache As MSForms.CheckBox)

    If IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled() Then

        rchkSavePasswordTemporaryCache.Value = GetAllowToSaveAdoConStrPasswordInTemporaryCache()
    Else
        With rchkSavePasswordTemporaryCache
        
            .Value = False
            
            .Enabled = False
        End With
    End If
End Sub

'''
'''
'''
Public Sub InitializeFixedSettingNameModeForEachRDB(ByRef robjSettingKeyNameToUserInputParametersDic As Scripting.Dictionary, _
        ByRef rcboConStrCacheName As MSForms.ComboBox, _
        ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr, _
        ByVal venmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType, _
        ByRef rblnFixedSettingNameMode As Boolean, _
        ByRef rcmdOK As MSForms.CommandButton)


    If rblnFixedSettingNameMode Then
    
        With rcboConStrCacheName
        
            .AddItem robjFormStateToSetAdoConStr.SettingKeyName
            
            .ListIndex = 0
            
            .Locked = True
        End With
    Else
        SetupAndInitializeCboSettingKeyNameUI robjSettingKeyNameToUserInputParametersDic, _
                rcboConStrCacheName, _
                robjFormStateToSetAdoConStr, _
                venmOleDbOrOdbcAdoConnectionDestinationRDBType
        
        If rcboConStrCacheName.ListCount > 0 Or rcboConStrCacheName.Text <> "" Then
        
            rcmdOK.Enabled = True
        Else
            rcmdOK.Enabled = False
        End If
    End If
End Sub




'''
'''
'''
''' <Argument>robjSettingKeyNameToUserInputParametersDic: Output</Argument>
''' <Argument>rcboConStrCacheName: Output</Argument>
''' <Argument>robjFormStateToSetAdoConStr: Input</Argument>
''' <Argument>venmOleDbOrOdbcAdoConnectionDestinationRDBType: Input</Argument>
Public Sub SetupAndInitializeCboSettingKeyNameUI(ByRef robjSettingKeyNameToUserInputParametersDic As Scripting.Dictionary, _
        ByRef rcboConStrCacheName As MSForms.ComboBox, _
        ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr, _
        ByVal venmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType)

    
    Dim varSettingKeyName As Variant

    If robjSettingKeyNameToUserInputParametersDic Is Nothing Then
    
        Set robjSettingKeyNameToUserInputParametersDic = GetFilteredOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey(venmOleDbOrOdbcAdoConnectionDestinationRDBType)
    End If

    If robjFormStateToSetAdoConStr.SettingKeyName <> "" Then

        With robjSettingKeyNameToUserInputParametersDic
        
            If Not .Exists(robjFormStateToSetAdoConStr.SettingKeyName) Then
            
                .Add robjFormStateToSetAdoConStr.SettingKeyName, robjFormStateToSetAdoConStr.UserInputParametersDic
            End If
        End With
    End If
    
    SortDictionaryAboutKeys robjSettingKeyNameToUserInputParametersDic


    With rcboConStrCacheName
        
        .Text = ""
        
        .Clear
    End With
    
    With robjSettingKeyNameToUserInputParametersDic
    
        For Each varSettingKeyName In .Keys
        
            With rcboConStrCacheName
    
                .AddItem varSettingKeyName
            End With
        Next
        
        With rcboConStrCacheName
        
            If .ListCount > 0 Then
            
                If robjFormStateToSetAdoConStr.SettingKeyName <> "" Then
                
                    .Value = robjFormStateToSetAdoConStr.SettingKeyName
                Else
                    .ListIndex = 0
                End If
            End If
        End With
    End With
End Sub

'''
'''
'''
''' <Argument>rcboControl: Input</Argument>
''' <Argument>robjSettingKeyNameToUserInputParametersDic: Input - one-nested Dictionary(Of String, Dictionary(Of String, Variant))</Argument>
''' <Return>Dictionary(Of String, Variant)</Return>
Public Function GetCurrentUserInputParametersDicFromComboBoxAndCacheDic(ByRef rcboControl As MSForms.ComboBox, _
        ByRef robjSettingKeyNameToUserInputParametersDic As Scripting.Dictionary) As Scripting.Dictionary


    Dim strSettingKeyName As String, objCurrentUserInputParametersDic As Scripting.Dictionary

    strSettingKeyName = GetCurrentValueOfComboBoxWithTextEditable(rcboControl)

    With robjSettingKeyNameToUserInputParametersDic
    
        If .Exists(strSettingKeyName) Then
    
            Set objCurrentUserInputParametersDic = .Item(strSettingKeyName)
        Else
            Set objCurrentUserInputParametersDic = New Scripting.Dictionary
        End If
    End With

    Set GetCurrentUserInputParametersDicFromComboBoxAndCacheDic = objCurrentUserInputParametersDic
End Function


'''
'''
'''
Public Function GetCurrentValueOfComboBoxWithTextEditable(ByRef rcboControl As MSForms.ComboBox) As String

    Dim strValue As String
    
    strValue = ""
    
    With rcboControl

        If .ListCount > 0 Then

            strValue = .List(.ListIndex)

        ElseIf .Text <> "" Then
        
            strValue = .Text
        End If
    End With

    GetCurrentValueOfComboBoxWithTextEditable = strValue
End Function




'**---------------------------------------------
'** About UI localization
'**---------------------------------------------
'''
'''
'''
Public Sub LocalizeUIControlOfAdoConStrInfoForAdoConStrLblConStrCacheName(ByRef robjAdoSettingCacheName As MSForms.Label, ByRef robjAdoConnectionStringFrame As MSForms.Frame)

    robjAdoSettingCacheName.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblConStrCacheName()

    robjAdoConnectionStringFrame.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrFraAdoConnectionParameters()
End Sub

'''
'''
'''
Public Sub LocalizeUIControlOfAdoConStrInfoForAdoConStrFraDsnParameters(ByRef robjEnablingDsnFrame As MSForms.Frame, _
        ByRef robjUseDsnOption As MSForms.OptionButton, _
        ByRef robjNoUseDsnOption As MSForms.OptionButton, _
        ByRef robjUseDsnFrame As MSForms.Frame, _
        ByRef robjLabelOfDSN As MSForms.Label)

    robjEnablingDsnFrame.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnType()

    robjUseDsnOption.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrOptUseDsn()
    
    robjNoUseDsnOption.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrOptDsnless()

    robjUseDsnFrame.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnParameters()

    robjLabelOfDSN.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblDataSourceName()
End Sub


'''
'''
'''
Public Sub LocalizeUIControlOfAdoConStrInfoForAdoConStrFraConnectionStringInfoCachingOption(ByRef robjFrame As MSForms.Frame, _
        ByRef robjCheckBoxOfSaveParametersInRegistoryExceptForPassword As MSForms.CheckBox, _
        ByRef robjCheckBoxOfSavePasswordTemporaryCache As MSForms.CheckBox)


    robjCheckBoxOfSavePasswordTemporaryCache.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrChkSavePasswordTemporaryCache()
    
    LocalizeUIControlOfAdoConStrInfoForSaveParametersInRegistoryExceptForPassword robjFrame, robjCheckBoxOfSaveParametersInRegistoryExceptForPassword
End Sub


'''
'''
'''
Public Sub LocalizeUIControlOfAdoConStrInfoForSaveParametersInRegistoryExceptForPassword(ByRef robjFrame As MSForms.Frame, _
        ByRef robjCheckBoxOfSaveParametersInRegistoryExceptForPassword As MSForms.CheckBox)

    robjFrame.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrFraConnectionStringInfoCachingOption()

    robjCheckBoxOfSaveParametersInRegistoryExceptForPassword.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrChkSaveParametersInRegistoryExceptForPassword()
End Sub



'**---------------------------------------------
'** Key-Value cache preparation for UserFormToolsForAdoConStr
'**---------------------------------------------
'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOdbcPostgresqlFormTitle() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrOdbcPostgresqlFormTitle = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_ORACLE_FORM_TITLE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOdbcOracleFormTitle() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrOdbcOracleFormTitle = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_ORACLE_FORM_TITLE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_SQLITE3_FORM_TITLE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOdbcSqlite3FormTitle() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrOdbcSqlite3FormTitle = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_SQLITE3_FORM_TITLE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_ACCESS_FORM_TITLE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOledbMsAccessFormTitle() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrOledbMsAccessFormTitle = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_ACCESS_FORM_TITLE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_EXCEL_FORM_TITLE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOledbMsExcelFormTitle() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrOledbMsExcelFormTitle = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_EXCEL_FORM_TITLE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_CON_STR_CACHE_NAME
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblConStrCacheName() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblConStrCacheName = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_CON_STR_CACHE_NAME")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_ADO_CONNECTION_PARAMETERS
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraAdoConnectionParameters() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrFraAdoConnectionParameters = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_ADO_CONNECTION_PARAMETERS")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_TYPE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnType() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnType = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_TYPE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_USE_DSN
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOptUseDsn() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrOptUseDsn = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_USE_DSN")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_DSNLESS
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOptDsnless() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrOptDsnless = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_DSNLESS")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_PARAMETERS
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnParameters() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnParameters = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_PARAMETERS")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATA_SOURCE_NAME
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblDataSourceName() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblDataSourceName = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATA_SOURCE_NAME")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_PGSQL_PARAMETERS
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessPgsqlParameters() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessPgsqlParameters = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_PGSQL_PARAMETERS")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_ORA_SQL_PARAMETERS
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessOraSqlParameters() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessOraSqlParameters = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_ORA_SQL_PARAMETERS")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_SQLITE_SQL_PARAMETERS
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessSqliteSqlParameters() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessSqliteSqlParameters = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_SQLITE_SQL_PARAMETERS")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_ODBC_DRIVER
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblOdbcDriver() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblOdbcDriver = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_ODBC_DRIVER")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_SERVER_HOST
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblServerHost() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblServerHost = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_SERVER_HOST")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATABASE_NAME
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblDatabaseName() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblDatabaseName = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATABASE_NAME")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_NETWORK_SERVICE_NAME
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblNetworkServiceName() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblNetworkServiceName = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_NETWORK_SERVICE_NAME")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PORT_NO
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblPortNo() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblPortNo = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PORT_NO")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USING_OLE_DB
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblUsingOleDb() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblUsingOleDb = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USING_OLE_DB")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_EXCEL_BOOK
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfExcelBook() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfExcelBook = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_EXCEL_BOOK")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_ACCESS_DB
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfAccessDb() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfAccessDb = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_ACCESS_DB")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_SQLITE_DB
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfSqliteDb() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfSqliteDb = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_SQLITE_DB")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_BROWSE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrCmdBrowse() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrCmdBrowse = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_BROWSE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_NAME
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblUserName() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblUserName = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_NAME")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_PASSWORD
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblUserPassword() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblUserPassword = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_PASSWORD")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_EXCEL_BOOK
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblPasswordForExcelBook() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblPasswordForExcelBook = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_EXCEL_BOOK")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_ACCESS_JET_DB_PASSWORD
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblPasswordForAccessJetDbPassword() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblPasswordForAccessJetDbPassword = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_ACCESS_JET_DB_PASSWORD")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_ADO_CONNECTION_TEST
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrCmdAdoConnectionTest() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrCmdAdoConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_ADO_CONNECTION_TEST")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_READ_ONLY_ADO_CONNECTION
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrChkReadOnlyAdoConnection() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrChkReadOnlyAdoConnection = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_READ_ONLY_ADO_CONNECTION")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_HEADER_COLUMN_TITLES_EXIST
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrChkHeaderColumnTitlesExist() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrChkHeaderColumnTitlesExist = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_HEADER_COLUMN_TITLES_EXIST")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_IMEX_MODE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblImexMode() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrLblImexMode = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_IMEX_MODE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_CONNECTION_STRING_INFO_CACHING_OPTION
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraConnectionStringInfoCachingOption() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrFraConnectionStringInfoCachingOption = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_CONNECTION_STRING_INFO_CACHING_OPTION")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PARAMETERS_IN_REGISTORY_EXCEPT_FOR_PASSWORD
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrChkSaveParametersInRegistoryExceptForPassword() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrChkSaveParametersInRegistoryExceptForPassword = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PARAMETERS_IN_REGISTORY_EXCEPT_FOR_PASSWORD")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PASSWORD_TEMPORARY_CACHE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrChkSavePasswordTemporaryCache() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrChkSavePasswordTemporaryCache = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PASSWORD_TEMPORARY_CACHE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TOOLTIP_CHK_SAVE_PASSWORD_TEMPORARY_CACHE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTooltipChkSavePasswordTemporaryCache() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrTooltipChkSavePasswordTemporaryCache = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TOOLTIP_CHK_SAVE_PASSWORD_TEMPORARY_CACHE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_CSV_ADO_OLE_DB_CONNECTION_TEST
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleCsvAdoOleDbConnectionTest() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrTitleCsvAdoOleDbConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_CSV_ADO_OLE_DB_CONNECTION_TEST")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_ACCESS_DB_ADO_OLE_DB_CONNECTION_TEST
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleMsAccessDbAdoOleDbConnectionTest() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrTitleMsAccessDbAdoOleDbConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_ACCESS_DB_ADO_OLE_DB_CONNECTION_TEST")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_EXCEL_SHEET_ADO_OLE_DB_CONNECTION_TEST
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleMsExcelSheetAdoOleDbConnectionTest() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrTitleMsExcelSheetAdoOleDbConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_EXCEL_SHEET_ADO_OLE_DB_CONNECTION_TEST")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_POSTGRESQL_ADO_ODBC_CONNECTION_TEST
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitlePostgresqlAdoOdbcConnectionTest() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrTitlePostgresqlAdoOdbcConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_POSTGRESQL_ADO_ODBC_CONNECTION_TEST")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_ORACLE_ADO_ODBC_CONNECTION_TEST
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleOracleAdoOdbcConnectionTest() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrTitleOracleAdoOdbcConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_ORACLE_ADO_ODBC_CONNECTION_TEST")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SQLITE_ADO_ODBC_CONNECTION_TEST
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleSqliteAdoOdbcConnectionTest() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrTitleSqliteAdoOdbcConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SQLITE_ADO_ODBC_CONNECTION_TEST")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_SUCCESS
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrMesAdoConnectionSuccess() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrMesAdoConnectionSuccess = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_SUCCESS")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_FAILED
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrMesAdoConnectionFailed() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrMesAdoConnectionFailed = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_FAILED")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_SQLITE_DATABASE_FILE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectSqliteDatabaseFile() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectSqliteDatabaseFile = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_SQLITE_DATABASE_FILE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_SQLITE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrDatabaseSqlite() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrDatabaseSqlite = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_SQLITE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_ACCESS_DATABASE_FILE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectMsAccessDatabaseFile() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectMsAccessDatabaseFile = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_ACCESS_DATABASE_FILE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_MS_ACCESS
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrDatabaseMsAccess() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrDatabaseMsAccess = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_MS_ACCESS")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_EXCEL_BOOK_FILE
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectMsExcelBookFile() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectMsExcelBookFile = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_EXCEL_BOOK_FILE")
End Function

'''
''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MS_EXCEL_BOOK
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Public Function GetTextOfStrKeyUserFormToolsForAdoConStrMsExcelBook() As String

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        msubInitializeTextForUserFormToolsForAdoConStr
    End If

    GetTextOfStrKeyUserFormToolsForAdoConStrMsExcelBook = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MS_EXCEL_BOOK")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Private Sub msubInitializeTextForUserFormToolsForAdoConStr()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForUserFormToolsForAdoConStrByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForUserFormToolsForAdoConStrByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized = True
    End If

    Set mobjStrKeyValueUserFormToolsForAdoConStrDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for UserFormToolsForAdoConStr key-values cache
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Private Sub AddStringKeyValueForUserFormToolsForAdoConStrByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE", "ADO connection string setting for ODBC PostgreSQL"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_ORACLE_FORM_TITLE", "ADO connection string setting for ODBC Oracle"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_SQLITE3_FORM_TITLE", "ADO connection string setting for ODBC SQLite"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_ACCESS_FORM_TITLE", "ADO connection string setting for OLEDB to MS Access DB"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_EXCEL_FORM_TITLE", "ADO connection string setting for OLEDB to MS Excel sheet"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_CON_STR_CACHE_NAME", "Connection setting local name key"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_ADO_CONNECTION_PARAMETERS", "ADO connection parameters"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_TYPE", "Whether use DSN or not"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_USE_DSN", "Use DSN"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_DSNLESS", "DSNless"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_PARAMETERS", "DSN parameters"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATA_SOURCE_NAME", "Data source name(DSN)"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_PGSQL_PARAMETERS", "DSNless PosgreSQL ODBC settings"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_ORA_SQL_PARAMETERS", "DSNless Oracle ODBC settings"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_SQLITE_SQL_PARAMETERS", "DSNless SQLite ODBC settings"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_ODBC_DRIVER", "ODBC driver"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_SERVER_HOST", "Server host name"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATABASE_NAME", "Data base name"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_NETWORK_SERVICE_NAME", "Network service name"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PORT_NO", "Port Number"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USING_OLE_DB", "Using OLE DB interface"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_EXCEL_BOOK", "Path of Excel book"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_ACCESS_DB", "Path of Access DB"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_SQLITE_DB", "Path of SQLite DB"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_BROWSE", "Browse"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_NAME", "User name"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_PASSWORD", "User password"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_EXCEL_BOOK", "Excel book password"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_ACCESS_JET_DB_PASSWORD", "Access JET DB password"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_ADO_CONNECTION_TEST", "ADO connection test"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_READ_ONLY_ADO_CONNECTION", "Read only ADO connection"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_HEADER_COLUMN_TITLES_EXIST", "Header column titles exist"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_IMEX_MODE", "IMEX mode"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_CONNECTION_STRING_INFO_CACHING_OPTION", "Connection string information caching option"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PARAMETERS_IN_REGISTORY_EXCEPT_FOR_PASSWORD", "Save parameters in Windows-registry except for password"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PASSWORD_TEMPORARY_CACHE", "Save password temporary cache variable"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TOOLTIP_CHK_SAVE_PASSWORD_TEMPORARY_CACHE", "The cache password is to be discarded when the Application is closed"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_CSV_ADO_OLE_DB_CONNECTION_TEST", "Csv ADO OLE DB connection test"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_ACCESS_DB_ADO_OLE_DB_CONNECTION_TEST", "Microsoft Access database ADO OLE DB connection test"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_EXCEL_SHEET_ADO_OLE_DB_CONNECTION_TEST", "Microsoft Excel sheet ADO OLE DB connection test"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_POSTGRESQL_ADO_ODBC_CONNECTION_TEST", "PostgreSQL ADO ODBC connection test"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_ORACLE_ADO_ODBC_CONNECTION_TEST", "Oracle ADO ODBC connection test"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SQLITE_ADO_ODBC_CONNECTION_TEST", "SQLite ADO ODBC connection test"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_SUCCESS", "ADO connection had been settled."
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_FAILED", "ADO connection was failed."
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_SQLITE_DATABASE_FILE", "Select a SQLite database file"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_SQLITE", "SQLite database"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_ACCESS_DATABASE_FILE", "Select a Microsoft Access database file"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_MS_ACCESS", "Access database"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_EXCEL_BOOK_FILE", "Select a Excel file"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MS_EXCEL_BOOK", "Excel book"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for UserFormToolsForAdoConStr key-values cache
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Private Sub AddStringKeyValueForUserFormToolsForAdoConStrByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE", "ADO接続文字列設定 - ODBC - PostgreSQL"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_ORACLE_FORM_TITLE", "ADO接続文字列設定 - ODBC - Oracle"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_SQLITE3_FORM_TITLE", "ADO接続文字列設定 - ODBC - SQLite"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_ACCESS_FORM_TITLE", "ADO接続文字列設定 - OLEDB - MS Access DB"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_EXCEL_FORM_TITLE", "ADO接続文字列設定 - OLEDB - MS Excel sheet"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_CON_STR_CACHE_NAME", "接続設定名キー"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_ADO_CONNECTION_PARAMETERS", "ADO接続パラメータ"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_TYPE", "DSN設定 使用・使用しない"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_USE_DSN", "DSN使用"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_DSNLESS", "DSN無し"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_PARAMETERS", "DSN項目"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATA_SOURCE_NAME", "データソース名(DSN)"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_PGSQL_PARAMETERS", "DSN無し PostgreSQL ODBC設定"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_ORA_SQL_PARAMETERS", "DSN無し Oracle ODBC設定"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_SQLITE_SQL_PARAMETERS", "DSN無し SQLite ODBC設定"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_ODBC_DRIVER", "ODBCドライバ"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_SERVER_HOST", "サーバホスト名"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATABASE_NAME", "データベース名"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_NETWORK_SERVICE_NAME", "ネットワークサービス名"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PORT_NO", "ポート番号"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USING_OLE_DB", "使用するOLE DB"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_EXCEL_BOOK", "Excelブックパス"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_ACCESS_DB", "Accessデータベースパス"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_SQLITE_DB", "SQLiteデータベースパス"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_BROWSE", "開く"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_NAME", "ユーザ名"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_PASSWORD", "ユーザパスワード"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_EXCEL_BOOK", "Excelブックパスワード"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_ACCESS_JET_DB_PASSWORD", "Access JETデータベースパスワード"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_ADO_CONNECTION_TEST", "ADO接続テスト"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_READ_ONLY_ADO_CONNECTION", "読み取り専用ADO接続"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_HEADER_COLUMN_TITLES_EXIST", "列見出し有り"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_IMEX_MODE", "IMEXモード"

        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_CONNECTION_STRING_INFO_CACHING_OPTION", "接続設定情報保存オプション"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PARAMETERS_IN_REGISTORY_EXCEPT_FOR_PASSWORD", "パスワードを除いて接続情報をレジストリに保存"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PASSWORD_TEMPORARY_CACHE", "パスワードを一時的に保存"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TOOLTIP_CHK_SAVE_PASSWORD_TEMPORARY_CACHE", "一時的に保存されたパスワードは、アプリケーション終了時に消されます"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_CSV_ADO_OLE_DB_CONNECTION_TEST", "Csv ADO OLE DB接続テスト"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_ACCESS_DB_ADO_OLE_DB_CONNECTION_TEST", "Microsoft Accessデータベース ADO OLE DB接続テスト"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_EXCEL_SHEET_ADO_OLE_DB_CONNECTION_TEST", "Microsoft Excelシート ADO OLE DB接続テスト"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_POSTGRESQL_ADO_ODBC_CONNECTION_TEST", "PostgreSQLデータベース ADO ODBC接続テスト"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_ORACLE_ADO_ODBC_CONNECTION_TEST", "Oracleデータベース ADO ODBC接続テスト"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SQLITE_ADO_ODBC_CONNECTION_TEST", "SQLiteデータベース ADO ODBC接続テスト"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_SUCCESS", "ADO接続しました。"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_FAILED", "ADO接続は失敗しました。"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_SQLITE_DATABASE_FILE", "SQLiteデータベースファイルを選択してください"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_SQLITE", "SQLiteデータベース"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_ACCESS_DATABASE_FILE", "Accessデータベースファイルを選択してください"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_MS_ACCESS", "Accessデータベース"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_EXCEL_BOOK_FILE", "Excelブックファイルを選択してください"
        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MS_EXCEL_BOOK", "Excelブック"
    End With
End Sub

'''
''' Remove Keys for UserFormToolsForAdoConStr key-values cache
'''
''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueUserFormToolsForAdoConStr(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_ORACLE_FORM_TITLE"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_SQLITE3_FORM_TITLE"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_ACCESS_FORM_TITLE"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_EXCEL_FORM_TITLE"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_CON_STR_CACHE_NAME"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_ADO_CONNECTION_PARAMETERS"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_TYPE"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_USE_DSN"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_DSNLESS"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_PARAMETERS"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATA_SOURCE_NAME"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_PGSQL_PARAMETERS"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_ORA_SQL_PARAMETERS"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_SQLITE_SQL_PARAMETERS"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_ODBC_DRIVER"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_SERVER_HOST"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATABASE_NAME"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_NETWORK_SERVICE_NAME"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PORT_NO"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USING_OLE_DB"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_EXCEL_BOOK"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_ACCESS_DB"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_SQLITE_DB"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_BROWSE"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_NAME"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_PASSWORD"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_EXCEL_BOOK"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_ACCESS_JET_DB_PASSWORD"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_ADO_CONNECTION_TEST"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_READ_ONLY_ADO_CONNECTION"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_HEADER_COLUMN_TITLES_EXIST"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_IMEX_MODE"

            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_CONNECTION_STRING_INFO_CACHING_OPTION"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PARAMETERS_IN_REGISTORY_EXCEPT_FOR_PASSWORD"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PASSWORD_TEMPORARY_CACHE"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TOOLTIP_CHK_SAVE_PASSWORD_TEMPORARY_CACHE"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_CSV_ADO_OLE_DB_CONNECTION_TEST"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_ACCESS_DB_ADO_OLE_DB_CONNECTION_TEST"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_EXCEL_SHEET_ADO_OLE_DB_CONNECTION_TEST"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_POSTGRESQL_ADO_ODBC_CONNECTION_TEST"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_ORACLE_ADO_ODBC_CONNECTION_TEST"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SQLITE_ADO_ODBC_CONNECTION_TEST"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_SUCCESS"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_FAILED"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_SQLITE_DATABASE_FILE"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_SQLITE"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_ACCESS_DATABASE_FILE"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_MS_ACCESS"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_EXCEL_BOOK_FILE"
            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MS_EXCEL_BOOK"
        End If
    End With
End Sub


