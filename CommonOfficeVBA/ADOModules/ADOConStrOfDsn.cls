VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOConStrOfDsn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO connection string generator for general RDB with DSN
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on ODBC Data-source-name setting (DSN) at this Windows
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IADOConnectStrGenerator

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** ODBC DSN connection
'**---------------------------------------------
Private mstrDSN As String   ' Data Source Name

Private mstrUID As String   ' User ID

Private mstrPWD As String   ' Password


' Additional information
Private menmOdbcConnectionDestinationRDBType As OdbcConnectionDestinationRDBType

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    ' about ODBC
    mstrDSN = ""
    
    ' Additional information
    menmOdbcConnectionDestinationRDBType = OdbcConnectionDestinationToPostgreSQL
End Sub

'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
Private Function IADOConnectStrGenerator_GetConnectionString() As String

    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForODBCWithDSN()
End Function

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** ODBC DSN connection
'**---------------------------------------------
'''
''' read-only Data Source Name - ODBC
'''
Public Property Get DSN() As String

    DSN = mstrDSN
End Property

'''
''' read-only User Name - ODBC
'''
Public Property Get UserName() As String

    UserName = mstrUID
End Property

'**---------------------------------------------
'** Additional information
'**---------------------------------------------
'''
'''
'''
Public Property Get OdbcConnectionDestinationRDB() As OdbcConnectionDestinationRDBType

    OdbcConnectionDestinationRDB = menmOdbcConnectionDestinationRDBType
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetConnectionString()

    GetConnectionString = GetConnectionStringForODBCWithDSN()
End Function

'''
''' by ODBC DSN connection
'''
Public Function GetConnectionStringForODBCWithDSN() As String

    GetConnectionStringForODBCWithDSN = "DSN=" & mstrDSN & ";UID=" & mstrUID & ";PWD=" & mstrPWD & ";"
End Function

'''
''' using ODBC DataSource setting (DSN)
'''
Public Sub SetODBCConnectionWithDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)

    mstrDSN = vstrDSN   ' Data Source Name
    
    mstrUID = vstrUID   ' User ID
    
    mstrPWD = vstrPWD   ' Password
End Sub

'''
'''
'''
Public Sub SetOdbcConnectionDestinationInformation(ByVal venmOdbcConnectionDestinationRDBType As OdbcConnectionDestinationRDBType)

    menmOdbcConnectionDestinationRDBType = venmOdbcConnectionDestinationRDBType
End Sub




