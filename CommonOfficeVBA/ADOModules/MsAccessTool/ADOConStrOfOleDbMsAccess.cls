VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOConStrOfOleDbMsAccess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO OLE DB connection and tools for Microsoft Access RDB
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on Microsoft Access OLE DB provider, which should has been installed when the Excel has installed.
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IADOConnectStrGenerator

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjCoreGenerator As ADOConStrOfAceOleDb

Private menmAccessOLEDBProviderType As AccessOLEDBProviderType

Private mstrOldTypeDbPassword As String

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjCoreGenerator = New ADOConStrOfAceOleDb
    
    mstrOldTypeDbPassword = ""
End Sub


'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
Private Function IADOConnectStrGenerator_GetConnectionString() As String

    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForMsAccess()
End Function

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** OLE DB connection
'**---------------------------------------------
'''
''' read-only Provider Name for OLE DB
'''
Public Property Get ProviderName() As String

    ProviderName = mobjCoreGenerator.OleDbProviderName
End Property

'**---------------------------------------------
'** Ms Access (.accdb) connection
'**---------------------------------------------
'''
''' read-only Excel book path or ACCESS accdb path
'''
Public Property Get ConnectingFilePath() As String

    ConnectingFilePath = mobjCoreGenerator.ConnectingFilePath
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetConnectionString()

    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
End Function

'''
'''
'''
Public Function GetConnectionStringForMsAccess() As String

    GetConnectionStringForMsAccess = GetADODBConnectionOleDbStringToAccDb(mobjCoreGenerator.ConnectingFilePath, mstrOldTypeDbPassword, menmAccessOLEDBProviderType)
End Function

'**---------------------------------------------
'** Set ADODB Access OLE DB parameters directly
'**---------------------------------------------
'''
''' connect to CSV directory
'''
Public Sub SetMsAccessConnection(ByVal vstrDbPath As String, _
        Optional ByVal vstrOldTypeDbPassword As String = "", _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)

    mobjCoreGenerator.ConnectingFilePath = vstrDbPath
    
    menmAccessOLEDBProviderType = venmAccessOLEDBProviderType
    
    mobjCoreGenerator.OleDbProviderName = GetAccessOLEDBProviderDescriptionFromEnm(menmAccessOLEDBProviderType)
    
    mstrOldTypeDbPassword = vstrOldTypeDbPassword
End Sub

'**---------------------------------------------
'** By a User-form, set ADODB ODBC parameters
'**---------------------------------------------
'''
'''
'''
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrDbPath: Input</Argument>
''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
''' <Argument>vstrOldTypeDbPassword: Input</Argument>
''' <Return>Boolean</Return>
Public Function IsAdoConnectionContinuedAfterOleDbToAccessDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDbPath As String = "", _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vstrOldTypeDbPassword As String = "") As Boolean

    Dim blnIsRequestedToCancelAdoConnection As Boolean
    
    SetOleDbConnectionParametersByAccessOleDbToMsAccessDbForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, vstrDbPath, venmAccessOLEDBProviderType, vstrOldTypeDbPassword
    
    IsAdoConnectionContinuedAfterOleDbToAccessDbConnectionParametersByForm = Not blnIsRequestedToCancelAdoConnection
End Function







