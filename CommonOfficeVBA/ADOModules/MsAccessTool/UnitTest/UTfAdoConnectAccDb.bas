Attribute VB_Name = "UTfAdoConnectAccDb"
'
'   Serving Microsoft Access db serving tests by ADO without Access application
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 15/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = True   ' About ADODB, ADOX

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToCreateAccessDB()

    GetPathAfterCreateSampleAccDb "TestFromVba01.accdb"
End Sub

'''
''' use SQL
'''
Private Sub msubSanityTestToCreateAccessDBBySQL()

    Dim strDbName As String, strDbPath As String, strConnectionString As String
    

    strDbName = "TestFromVba01.accdb"

    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\" & strDbName

    With New Scripting.FileSystemObject
    
        If .FileExists(strDbPath) Then
            
            .DeleteFile strDbPath
        End If
    End With
    
    strConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strDbPath & ";"
    
    msubCreateAccdbSampleTableByUsingSQL strConnectionString
End Sub


'''
''' Whenever this VBA is the 32 bit mode, no error occurs
'''
Private Sub msubSanityTestToCreateAccessDBWithMDB()

    Dim strDbName As String, strDbPath As String, strConnectionString As String
    
#If HAS_REF Then

    Dim objConnection As ADODB.Connection, objCatalog As ADOX.Catalog, strSQL As String
#Else
    Dim objConnection As Object, objCatalog As Object, strSQL As String
#End If


    strDbName = "TestFromVbs01.mdb"

    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\" & strDbName

    With New Scripting.FileSystemObject
    
        If .FileExists(strDbPath) Then
            
            .DeleteFile strDbPath
        End If
    End With
    
    strConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strDbPath & ";"
    
    ' If X64 mode VBA, the following code causes an error.
    
    msubCreateSampleTableByUsingRecordset strConnectionString
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Private Sub ExploreParentFolderOfTemporaryAccessDb()

    ExploreParentFolderWhenOpenedThatIsNothing GetTemporaryAccessDataBaseFilesDirectoryPath()
End Sub



'''
'''
'''
Public Function PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist(ByVal vstrDbName As String) As String

    Dim strDbPath As String

    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\" & vstrDbName

    If Not FileExistsByVbaDir(strDbPath) Then
    
        GetPathAfterCreateSampleAccDb vstrDbName
    End If

    PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist = strDbPath
End Function


'''
'''
'''
Public Function GetPathAfterCreateSampleAccDb(ByVal vstrDbName As String) As String

    Dim strDbPath As String, strConnectionString As String

    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\" & vstrDbName
    
    If FileExistsByVbaDir(strDbPath) Then
        
        VBA.Kill strDbPath
    End If
    
    strConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strDbPath & ";"
    
    msubCreateSampleTableByUsingRecordset strConnectionString

    GetPathAfterCreateSampleAccDb = strDbPath
End Function

'''
'''
'''
Public Function GetTemporaryAccessDataBaseFilesDirectoryPath() As String

    Dim strDir As String

    strDir = GetTemporaryDevelopmentVBARootDir() & "\DataBases"

    ForceToCreateDirectory strDir

    GetTemporaryAccessDataBaseFilesDirectoryPath = strDir
End Function

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubCreateSampleTableByUsingRecordset(ByRef rstrConnectionString As String)

    Dim strTableName As String, strTableFieldTypes As String, strSQL As String
    
#If HAS_REF Then

    Dim objConnection As ADODB.Connection
#Else
    Dim objConnection As Object
#End If

    strTableName = "TestTable"
    
    strTableFieldTypes = "���� varchar(20),�g�� float"
    
    
    With New ADOX.Catalog
    
        ' create Access data-base
    
        ' This cause an error in x64 bit mode VBA
    
        Set objConnection = .Create(rstrConnectionString)
    End With

    strSQL = "create table " & strTableName & " (" & strTableFieldTypes & ");"
    
    ' create a table
    objConnection.Execute strSQL

    
    strSQL = "select * from " & strTableName & ";"
    
    'With CreateObject("ADODB.Recordset")
    
    With New ADODB.Recordset
    
        ' adLockPessimistic := 2, ADODB.CommandTypeEnum.adCmdText := 1
    
        .Open strSQL, objConnection, adOpenForwardOnly, ADODB.LockTypeEnum.adLockPessimistic, adCmdText
        
        ' add some records by ADODB.Recordset interface
        
        .AddNew
        
        .Fields("����").Value = "�� �o�ɑ�": .Fields("�g��").Value = 174.6
        
        '.Update
        
        .AddNew
        
        .Fields(0).Value = "�� �o�ɑ�": .Fields(1).Value = 166.2
        
        '.Update
        
        .AddNew Array("����", "�g��"), Array("���� �o�ɑ�", 151.4)
        
        .Update
        
        .Close
    End With
    
    objConnection.Close
End Sub

'''
'''
'''
Private Sub msubCreateAccdbSampleTableByUsingSQL(ByRef rstrConnectionString As String)

    Dim strTableName As String, strTableFieldTypes As String, strSQL As String

    strTableName = "TestTable"
    
    strTableFieldTypes = "���� varchar(20),�g�� float"
    
    
    With New ADOX.Catalog
    
        ' create Access data-base
    
        ' This cause an error in x64 bit mode VBA
    
        With .Create(rstrConnectionString)
        
            strSQL = "create table " & strTableName & " (" & strTableFieldTypes & ");"
            
            ' create a table
            .Execute strSQL
        
            .BeginTrans
        
            strSQL = "insert into " & strTableName & " (����, �g��)  values('�� �o�ɑ�', 174.6);"
            .Execute strSQL
            
            strSQL = "insert into " & strTableName & " (����, �g��)  values('�� �o�ɑ�', 166.2);"
            .Execute strSQL
            
            strSQL = "insert into " & strTableName & " (����, �g��)  values('���� �o�ɑ�', 151.4);"
            .Execute strSQL
            
            .CommitTrans
            
            .Close
        End With
    End With
End Sub

