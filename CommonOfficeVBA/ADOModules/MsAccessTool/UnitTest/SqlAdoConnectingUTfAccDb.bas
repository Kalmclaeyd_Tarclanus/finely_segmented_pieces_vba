Attribute VB_Name = "SqlAdoConnectingUTfAccDb"
'
'   Sanity test to connect Access database using ADO OLE DB
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
'       which should have been the installed when the Microsoft Office Excel software has been installed.
'       Dependent on UTfAdoConnectAccDb.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri,  4/Aug/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** AccDbAdoConnector
'**---------------------------------------------
'''
''' Create an in-line table from the only SELECT sql inside
'''
Private Sub msubSanityTestToConnectToAccDbAdoConnectorWithSqlInlineVirtualTable()
    
    Dim strSQL As String

    With New AccDbAdoConnector

        ' use ADOX

        .SetMsAccessConnection PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist("TestDbOfAccDbAdoSheetExpander.accdb")

        .AllowToRecordSQLLog = True

        ' Confirm the Recordset of the virtual-table in the SQL without reading the Access database.

        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"

        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub

'''
''' SQL test INSERT INTO
'''
Private Sub msubSanityTestToConnectToAccDbAdoConnectorWithSqlInsert()

    GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert
End Sub

'''
''' SQL test DELETE
'''
Private Sub msubSanityTestToConnectToAccDbAdoConnectorWithSqlDelete()

    Dim strDbPath As String, strSQL As String

    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
    
    With New AccDbAdoConnector
    
        .SetMsAccessConnection strDbPath
    
        strSQL = "DELETE FROM Test_Table WHERE ColText = 'Type2'"
    
        .ExecuteSQL strSQL
        
        .CloseConnection
        
        strSQL = "SELECT * FROM Test_Table"
    
        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub

'''
''' SQL test UPDATE
'''
Private Sub msubSanityTestToConnectToAccDbAdoConnectorWithSqlUpdate()

    Dim strDbPath As String, strSQL As String

    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
    
    With New AccDbAdoConnector
    
        .SetMsAccessConnection strDbPath
    
        strSQL = "UPDATE Test_Table SET Col1 = 31, Col2 = 32 WHERE Col3 = 9 AND ColText = 'Type2'"
    
        .ExecuteSQL strSQL
        
        .CloseConnection
        
        strSQL = "SELECT * FROM Test_Table"
    
        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub


'''
''' SQL test DROP TABLE
'''
Public Sub msubSanityTestToConnectMsAccessDbAndDropTable()

    Dim strSQL As String, strDbPath As String, objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess
    
    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\TestDb02OfAccDbAdoConnector.accdb"

    If FileExistsByVbaDir(strDbPath) Then
        
        VBA.Kill strDbPath
    End If

    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess

    objADOConStrOfOleDbMsAccess.SetMsAccessConnection strDbPath

    With New ADOX.Catalog
    
        With .Create(objADOConStrOfOleDbMsAccess.GetConnectionString())
        
            .Close
        End With
    End With

    With New AccDbAdoConnector
    
        .SetMsAccessConnection strDbPath
        
        
        strSQL = "CREATE TABLE Test_Table (Col1 Integer, Col2 Integer, Col3 Integer, ColText varchar);"
        
        .ExecuteSQL strSQL
        
        .SuppressToShowUpSqlExecutionError = True

        strSQL = "CREATE TABLE Test_Table (Col1 Integer, Col2 Integer, Col3 Integer, ColText varchar);"

        On Error Resume Next

        .ExecuteSQL strSQL

        On Error GoTo 0

        .SuppressToShowUpSqlExecutionError = False
        
        
        strSQL = "DROP TABLE Test_Table"
        
        .ExecuteSQL strSQL
        
        
'        strSQL = "DROP TABLE Test_Table"
'
'        .ExecuteSQL strSQL
        
        .CloseConnection
    End With
End Sub


'**---------------------------------------------
'** About Microsoft Access MSysObjects
'**---------------------------------------------
'''
''' At default .accdb file, MSysObjects is not able to access from ODBC outside Ms Access application
'''
Private Sub msubSanityTestToConnectToAccDbAndGetTableListBySQL()

    Dim strSQL As String, strDbPath As String

    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\TestDb01OfAccDbAdoConnector.accdb"
    
    With New AccDbAdoConnector
    
        .SetMsAccessConnection strDbPath
        
        strSQL = "SELECT MSysObjects.Type, MSysObjects.Name, MSysObjects.Flags From MSysObjects WHERE MSysObjects.Type = 1 AND MSysObjects.Flags = 0 ORDER BY MSysObjects.Type, MSysObjects.Name;"
        
        ' The following occurs an error of the access authority of MSysObjects
        
        DebugCol GetTableCollectionAsDateFromRSet(.GetRecordset(strSQL))
        
        If Err.Number <> 0 Then
                
            Debug.Print Err.Description
        End If
        
        On Error GoTo 0
    End With
End Sub

'**---------------------------------------------
'** Ms Access ADODB.Connection tests
'**---------------------------------------------
'''
''' SQL SELECT INTO
'''
Private Sub msubSanityTestToConnectMsAccessDbAndSelectInto()

    Dim strSQL As String, strDbPath As String, objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess, blnFailedToDeleteAccDb As Boolean
    Dim strConnectionString As String
    Dim objConnection As ADODB.Connection, objRSet As ADODB.Recordset, intRecordsAffected As Long
    
    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\TestDb01.accdb"

    blnFailedToDeleteAccDb = False
    
    If FileExistsByVbaDir(strDbPath) Then
    
        On Error Resume Next
        
        VBA.Kill strDbPath
        
        If Err.Number <> 0 Then
        
            blnFailedToDeleteAccDb = True
        End If
        
        On Error GoTo 0
    End If
    
    If Not blnFailedToDeleteAccDb Then
    
        Set objConnection = mfobjGetConnectionAfterCreateSimpleAdoDbByADOX(strDbPath)
    End If

    With objConnection
    
        .BeginTrans
    
        If blnFailedToDeleteAccDb Then
    
            strSQL = "DROP TABLE Test_Table"
    
            On Error Resume Next: .Execute strSQL: On Error GoTo 0
        End If
    
        strSQL = "CREATE TABLE Test_Table (Col1 Integer, Col2 Integer, Col3 Integer, ColText varchar);"
    
        .Execute strSQL
        
        strSQL = "INSERT INTO Test_Table VALUES (1, 2, 3, 'Type1');"
        
        .Execute strSQL
        
        strSQL = "INSERT INTO Test_Table VALUES (4, 5, 6, 'Type1');"
        
        .Execute strSQL
        
        strSQL = "INSERT INTO Test_Table VALUES (7, 8, 9, 'Type2');"
        
        .Execute strSQL
        
        strSQL = "INSERT INTO Test_Table VALUES (10, 11, 12, 'Type2');"
        
        .Execute strSQL
        
        .CommitTrans
        
        strSQL = "SELECT * FROM Test_Table"
        
        DebugCol GetTableCollectionFromRSet(.Execute(strSQL))
        
        ' Test SELECT INTO
        
        .BeginTrans
        
        strSQL = "SELECT * INTO Test_Table02 FROM Test_Table WHERE ColText = 'Type2'"
        
        intRecordsAffected = -1
        
        Set objRSet = .Execute(strSQL, intRecordsAffected)
        
        If Not objRSet Is Nothing Then
        
            Debug.Print "ADODB.Recordset.State: " & GetADOEnumLiteralFromObjectStateEnum(objRSet.State) & ", ADODB.Connection.State: " & GetADOEnumLiteralFromObjectStateEnum(.State)
            
            If objRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
            
                Debug.Print "ADODB.Recordset has been got and have some data"
            
                Debug.Print "RecordCount: " & CStr(objRSet.RecordCount)
    
                Debug.Print "Fields.Count: " & CStr(objRSet.Fields.Count)
                
                DebugCol GetTableCollectionFromRSet(objRSet)
            Else
                Debug.Print "ADODB.Recordset has been got, but it has been closed."
            End If
        End If
        
        If intRecordsAffected <> -1 Then
        
            Debug.Print "After SELECT INTO, RecordsAffected: " & CStr(intRecordsAffected)
        End If
        
        .CommitTrans
        
        strSQL = "SELECT * FROM Test_Table02"
        
        DebugCol GetTableCollectionFromRSet(.Execute(strSQL))
        
        .Close
    End With
End Sub

'''
'''
'''
Private Function mfobjGetConnectionAfterCreateSimpleAdoDbByADOX(ByVal vstrDbPath As String, Optional ByVal vblnCloseAdoConnectBecauseOfNoUse As Boolean = False) As ADODB.Connection

    Dim objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess, objConnection As ADODB.Connection

    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess

    objADOConStrOfOleDbMsAccess.SetMsAccessConnection vstrDbPath

    With New ADOX.Catalog
    
        Set objConnection = .Create(objADOConStrOfOleDbMsAccess.GetConnectionString())
        
        If vblnCloseAdoConnectBecauseOfNoUse Then
        
            objConnection.Close
            
            Set objConnection = Nothing
        End If
    End With
    
    Set mfobjGetConnectionAfterCreateSimpleAdoDbByADOX = objConnection
End Function


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert() As String

    Dim strSQL As String, strDbPath As String, blnFailedToDeleteAccDb As Boolean
    
    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\TestDb01OfAccDbAdoConnector.accdb"

    blnFailedToDeleteAccDb = False
    
    If FileExistsByVbaDir(strDbPath) Then
        
        On Error Resume Next
        
        VBA.Kill strDbPath
        
        If Err.Number <> 0 Then
        
            blnFailedToDeleteAccDb = True
        End If
        
        On Error GoTo 0
    End If

    If Not blnFailedToDeleteAccDb Then
    
        mfobjGetConnectionAfterCreateSimpleAdoDbByADOX strDbPath, True
    End If

    With New AccDbAdoConnector
    
        .SetMsAccessConnection strDbPath
        
        If blnFailedToDeleteAccDb Then
        
            strSQL = "DROP TABLE Test_Table"
        
            '.SuppressToShowUpSqlExecutionError = True
            
            On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
            
            '.SuppressToShowUpSqlExecutionError = False
        End If
        
        strSQL = "CREATE TABLE Test_Table (Col1 Integer, Col2 Integer, Col3 Integer, ColText varchar);"
        
        On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
        
        .ADOConnectorInterface.CommitTransaction
        
        If .IsSqlExecutionFailed Then
        
            strSQL = "DELETE FROM Test_Table"
            
            .ExecuteSQL strSQL
            
            .ADOConnectorInterface.CommitTransaction
        End If
        
        strSQL = "INSERT INTO Test_Table VALUES (1, 2, 3, 'Type1');"
        
        .ExecuteSQL strSQL
        
        strSQL = "INSERT INTO Test_Table VALUES (4, 5, 6, 'Type1');"
        
        .ExecuteSQL strSQL
        
        strSQL = "INSERT INTO Test_Table VALUES (7, 8, 9, 'Type2');"
        
        .ExecuteSQL strSQL
    
        strSQL = "INSERT INTO Test_Table VALUES (10, 11, 12, 'Type2');"
        
        .ExecuteSQL strSQL
        
        .CloseConnection
        
        strSQL = "SELECT * FROM Test_Table"
        
        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
        
        .CloseConnection
    End With
    
    GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert = strDbPath
End Function




