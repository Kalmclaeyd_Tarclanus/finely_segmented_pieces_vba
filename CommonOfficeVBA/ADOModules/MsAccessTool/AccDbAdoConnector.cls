VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AccDbAdoConnector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   connect to a Microsoft Access database by the ADO Access OLE DB interface
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IADOConnector

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjConnector As ADOConnector: Private mitfConnector As IADOConnector

Private mobjConnectStrGenerator As ADOConStrOfOleDbMsAccess

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjConnector = New ADOConnector: Set mitfConnector = mobjConnector
    
    Set mobjConnectStrGenerator = New ADOConStrOfOleDbMsAccess
End Sub

Private Sub Class_Terminate()

    Me.CloseConnection
    
    Set mitfConnector = Nothing
    
    Set mobjConnector = Nothing
    
    Set mobjConnectStrGenerator = Nothing
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get ADOConnectorInterface() As IADOConnector

    Set ADOConnectorInterface = Me
End Property

'///////////////////////////////////////////////
'/// Properties - connoted ADO connector
'///////////////////////////////////////////////
Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)

    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
End Property
Public Property Get AllowToRecordSQLLog() As Boolean

    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
End Property


Public Property Get SQLExecutionResult() As SQLResult

    Set SQLExecutionResult = IADOConnector_SQLExecutionResult
End Property

Public Property Get IsSqlExecutionFailed() As Boolean

    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
End Property

'///////////////////////////////////////////////
'/// Implemented properties
'///////////////////////////////////////////////
Private Property Let IADOConnector_IsConnected(ByVal vblnIsConnected As Boolean)

    If vblnIsConnected Then
    
        mitfConnector.IsConnected = mfblnConnect()
    Else
        mobjConnector.CloseConnection
        
        mitfConnector.IsConnected = mobjConnector.IsConnected
    End If
End Property
Private Property Get IADOConnector_IsConnected() As Boolean

    IADOConnector_IsConnected = mfblnConnect()
End Property

Private Property Set IADOConnector_ADOConnection(ByVal vobjADOConnection As ADODB.Connection)

    Set mitfConnector.ADOConnection = vobjADOConnection
End Property
Private Property Get IADOConnector_ADOConnection() As ADODB.Connection

    Set IADOConnector_ADOConnection = mitfConnector.ADOConnection
End Property



Private Property Set IADOConnector_SQLExecutionResult(ByVal vobjSQLResult As SQLResult)

    Set mitfConnector.SQLExecutionResult = vobjSQLResult
End Property
Private Property Get IADOConnector_SQLExecutionResult() As SQLResult

    Set IADOConnector_SQLExecutionResult = mitfConnector.SQLExecutionResult
End Property


Private Property Get IADOConnector_CommandTimeout() As Long

    IADOConnector_CommandTimeout = mobjConnector.CommandTimeout
End Property
Private Property Let IADOConnector_CommandTimeout(ByVal vintTimeout As Long)

    mobjConnector.CommandTimeout = vintTimeout
End Property

Private Property Get IADOConnector_SuppressToShowUpSqlExecutionError() As Boolean

    IADOConnector_SuppressToShowUpSqlExecutionError = mitfConnector.SuppressToShowUpSqlExecutionError
End Property
Private Property Let IADOConnector_SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)

    mitfConnector.SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
End Property

Private Property Get IADOConnector_RdbConnectionInformation() As RdbConnectionInformationFlag

    IADOConnector_RdbConnectionInformation = AdoOleDbToMsAccessRdbFlag
End Property
Private Property Let IADOConnector_RdbConnectionInformation(ByVal venmRdbConnectionInformationFlag As RdbConnectionInformationFlag)

    ' Nothing to do
End Property

'///////////////////////////////////////////////
'/// Properties - connoted ADOConnectionSetting
'///////////////////////////////////////////////
Public Property Get ConnectSetting() As ADOConnectionSetting

    Set ConnectSetting = mobjConnector.ConnectSetting
End Property

'''
''' unit is second. [s]
'''
Public Property Let CommandTimeout(ByVal vintTimeout As Long)

    IADOConnector_CommandTimeout = vintTimeout
End Property
Public Property Get CommandTimeout() As Long

    CommandTimeout = IADOConnector_CommandTimeout
End Property


Public Property Get SuppressToShowUpSqlExecutionError() As Boolean

    SuppressToShowUpSqlExecutionError = IADOConnector_SuppressToShowUpSqlExecutionError
End Property
Public Property Let SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)

    IADOConnector_SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
End Property

'///////////////////////////////////////////////
'/// Properties - Access Db connection by ACCESS OLE-DB provider
'///////////////////////////////////////////////
Public Property Get ConnectingFilePath() As String

    ConnectingFilePath = mobjConnectStrGenerator.ConnectingFilePath
End Property


'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
'''
''' overrides
'''
Private Function IADOConnector_GetRecordset(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
    
    Set IADOConnector_GetRecordset = Me.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
End Function

'''
'''
'''
Private Function IADOConnector_ExecuteSQL(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset

    Set IADOConnector_ExecuteSQL = ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
End Function

'''
''' If the ExecuteSQL method had been used at least one time, this execute ADODB.Connect.CommitTrans()
'''
Private Sub IADOConnector_CommitTransaction()

    mitfConnector.CommitTransaction
End Sub

'''
''' If the ExecuteSQL method had been used at least one time, this execute CommitTransaction before execute ADODB.Connect.Close()
'''
Private Sub IADOConnector_CloseConnection()

    Me.CloseConnection
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** set ADO connection string all parameters directly
'**---------------------------------------------
'''
'''
'''
Public Sub SetMsAccessConnection(ByVal vstrDbPath As String, _
        Optional ByVal vstrOldTypeDbPassword As String = "", _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)

    With mobjConnectStrGenerator
    
        If .ConnectingFilePath <> "" And .ConnectingFilePath <> vstrDbPath Then
            
            Me.CloseConnection
        End If
    
        .SetMsAccessConnection vstrDbPath, vstrOldTypeDbPassword, venmAccessOLEDBProviderType
    End With
End Sub

'**---------------------------------------------
'** set ADO connection string parameters by a User-form interface
'**---------------------------------------------
'''
'''
'''
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrDbPath: Input</Argument>
''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
''' <Argument>vstrOldTypeDbPassword: Input</Argument>
''' <Return>Boolean</Return>
Public Function IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDbPath As String = "", _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vstrOldTypeDbPassword As String = "")
        
        
    IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm = mobjConnectStrGenerator.IsAdoConnectionContinuedAfterOleDbToAccessDbConnectionParametersByForm(vstrSettingKeyName, vstrDbPath, venmAccessOLEDBProviderType, vstrOldTypeDbPassword)
End Function

'''
''' Close ADO connection
'''
Public Sub CloseConnection()

    mobjConnector.CloseConnection
End Sub


'**---------------------------------------------
'** Use SQL after the ADO connected
'**---------------------------------------------
'''
''' get recordset object by the Select SQL
'''
Public Function GetRecordset(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
    
    If mfblnConnect() Then
    
        Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
    End If
End Function

'''
''' input SQL, such as INSERT, UPDATE, DELETE
'''
Public Function ExecuteSQL(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
    
    Dim objRSet As ADODB.Recordset
    
    Set objRSet = Nothing
    
    If mfblnConnect() Then
    
        Set objRSet = mobjConnector.ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
    End If
    
    Set ExecuteSQL = objRSet
End Function

'''
'''
'''
Public Function IsConnected() As Boolean

    IsConnected = mfblnConnect()
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
''' connect Excel sheet by ADO
'''
Private Function mfblnConnect() As Boolean

    Dim blnIsConnected As Boolean

    TryToConnectRDBProviderByAdoConnectionString blnIsConnected, mobjConnector, mobjConnectStrGenerator

    mfblnConnect = blnIsConnected
End Function


