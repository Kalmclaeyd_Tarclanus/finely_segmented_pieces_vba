Attribute VB_Name = "ADOConStrToolsForAccDb"
'
'   Generate ADODB connection string to a Ace (.accdb) data base
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
'       which should have been the installed when the Microsoft Office Excel software has been installed.
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetADODBConnectionOleDbStringToAccDb(ByVal vstrDbPath As String, _
        Optional ByVal vstrOldTypeDbPassword As String = "", _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0) As String
        
        
    Dim strConnectionString As String, strProvider As String
    
    strProvider = GetAccessOLEDBProviderNameFromEnm(venmAccessOLEDBProviderType)
    
    strConnectionString = "Provider=" & strProvider & ";Data Source=" & vstrDbPath & ";"
        
    If vstrOldTypeDbPassword <> "" Then
    
        strConnectionString = strConnectionString & "Jet OLEDB:Database Password=" & vstrOldTypeDbPassword & ";"
    End If

    GetADODBConnectionOleDbStringToAccDb = strConnectionString
End Function


'**---------------------------------------------
'** for control USetAdoOleDbConStrForAccdb user-form
'**---------------------------------------------
'''
'''
'''
Public Function GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsAccessDb(Optional ByVal vstrDbPath As String = "", _
        Optional ByVal vstrOldTypeDbPassword As String = "", _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary

    With objDic
    
        .Add mstrAdoDbFilePathKey, vstrDbPath
    
        .Add "OLE_DB_Provider", GetAccessOLEDBProviderNameFromEnm(venmAccessOLEDBProviderType)
        
        If vstrOldTypeDbPassword <> "" Then
    
            .Add mstrTemporaryPdKey, vstrOldTypeDbPassword
        End If
        
        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOleDbOrOdbcAdoConnectionDestinationRDBType(AdoOleDbAndMsAccessDb)
    End With

    Set GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsAccessDb = objDic
End Function

'''
'''
'''
Public Sub UpdateADOConStrOfAccessOleDbToMsAccessDbFromInputDic(ByRef robjADOConStrOfOledbMsAccess As ADOConStrOfOleDbMsAccess, ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String, strOldTypeDbPassword As String, enmAccessOLEDBProviderType As AccessOLEDBProviderType
    
    With robjUserInputParametersDic
    
        strKey = "OLE_DB_Provider"
        
        enmAccessOLEDBProviderType = AceOLEDB_12P0
        
        If .Exists(strKey) Then
        
            enmAccessOLEDBProviderType = GetAccessOLEDBProviderEnmFromName(.Item(strKey))
        End If
        
        strOldTypeDbPassword = ""
        
        If .Exists(mstrTemporaryPdKey) Then
        
            strOldTypeDbPassword = .Item(mstrTemporaryPdKey)
        End If
        
        robjADOConStrOfOledbMsAccess.SetMsAccessConnection .Item(mstrAdoDbFilePathKey), strOldTypeDbPassword, enmAccessOLEDBProviderType
    End With
End Sub

'''
'''
'''
Public Function GetADOConStrOfAccessOleDbToMsAccessDbFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfOleDbMsAccess

    Dim objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess
    
    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess
    
    UpdateADOConStrOfAccessOleDbToMsAccessDbFromInputDic objADOConStrOfOleDbMsAccess, robjUserInputParametersDic
    
    Set GetADOConStrOfAccessOleDbToMsAccessDbFromInputDic = objADOConStrOfOleDbMsAccess
End Function



'**---------------------------------------------
'** ADO connection string parameters solution interfaces by each special form
'**---------------------------------------------
'''
'''
'''
''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
''' <Argument>robjADOConStrOfOledbMsAccess: Output</Argument>
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrDbFilePath: Input</Argument>
''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
''' <Argument>vstrOldTypeDbPassword: Input</Argument>
Public Sub SetOleDbConnectionParametersByAccessOleDbToMsAccessDbForm(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
        ByRef robjADOConStrOfOledbMsAccess As ADOConStrOfOleDbMsAccess, _
        ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDbFilePath As String = "", _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vstrOldTypeDbPassword As String = "")

    Dim objUserInputParametersDic As Scripting.Dictionary, strPassword As String, blnIsOpeningFormNeeded As Boolean
        
    rblnIsRequestedToCancelAdoConnection = False
        
    blnIsOpeningFormNeeded = IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToMsAccessDb(objUserInputParametersDic, vstrSettingKeyName, vstrDbFilePath, venmAccessOLEDBProviderType, vstrOldTypeDbPassword)

    If blnIsOpeningFormNeeded Then
    
        msubSetUserInputParametersDicOfAccessOleDbToMsAccessDbByOpeningFormWithFixedSettingKeyName rblnIsRequestedToCancelAdoConnection, objUserInputParametersDic, vstrSettingKeyName
    End If

    If Not rblnIsRequestedToCancelAdoConnection Then
    
        UpdateADOConStrOfAccessOleDbToMsAccessDbFromInputDic robjADOConStrOfOledbMsAccess, objUserInputParametersDic
    End If
End Sub




'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
''' <Argument>robjUserInputParametersDic: Input - Output</Argument>
''' <Argument>rstrSettingKeyName: Input</Argument>
Private Sub msubSetUserInputParametersDicOfAccessOleDbToMsAccessDbByOpeningFormWithFixedSettingKeyName(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        ByRef rstrSettingKeyName As String)


    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr, objUSetAdoOleDbConStrForAccdb As USetAdoOleDbConStrForAccdb
   
    Set objFormStateToSetAdoConStr = GetFormStateToSetAdoConStrByDefaultParams(robjUserInputParametersDic, rstrSettingKeyName)
   
    Set objUSetAdoOleDbConStrForAccdb = New USetAdoOleDbConStrForAccdb
    
    With objUSetAdoOleDbConStrForAccdb
    
        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
    
        .Show vbModal
    End With

    AfterProcessAdoConStrFormSetting rblnIsRequestedToCancelAdoConnection, robjUserInputParametersDic, objFormStateToSetAdoConStr
End Sub

'''
'''
'''
''' <Argument>robjUserInputParametersDic: Output</Argument>
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrDbFilePath: Input</Argument>
''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
''' <Argument>vstrOldTypeDbPassword: Input</Argument>
Private Function IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToMsAccessDb(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDbFilePath As String = "", _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vstrOldTypeDbPassword As String = "") As Boolean

    Dim blnIsOpeningFormNeeded As Boolean
    
    blnIsOpeningFormNeeded = True

    ' Try to get parameters from Win-registry cache
    
    Set robjUserInputParametersDic = GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(vstrSettingKeyName)

    Select Case True
    
        Case robjUserInputParametersDic Is Nothing, robjUserInputParametersDic.Count = 0
    
            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToMsAccessDb(robjUserInputParametersDic, vstrDbFilePath, venmAccessOLEDBProviderType, vstrOldTypeDbPassword)
        Case Else
            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache(robjUserInputParametersDic)
    End Select

    IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToMsAccessDb = blnIsOpeningFormNeeded
End Function



'''
'''
'''
''' <Argument>robjUserInputParametersDic: Output</Argument>
''' <Argument>vstrDbFilePath: Input</Argument>
''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
''' <Argument>vstrOldTypeDbPassword: Input</Argument>
Private Function IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToMsAccessDb(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        Optional ByVal vstrDbFilePath As String = "", _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vstrOldTypeDbPassword As String = "") As Boolean


    Dim blnIsOpeningFormNeeded As Boolean
    
    blnIsOpeningFormNeeded = True
    
    Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsAccessDb(vstrDbFilePath, vstrOldTypeDbPassword, venmAccessOLEDBProviderType)
    
    If IsADOConnectionStringEnabled(GetADOConStrOfAccessOleDbToMsAccessDbFromInputDic(robjUserInputParametersDic).GetConnectionString()) Then
    
        blnIsOpeningFormNeeded = False
    End If

    IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToMsAccessDb = blnIsOpeningFormNeeded
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' delete testing SettingKeyName from the registory
'''
Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToAccessOleDbWithMsAccessDb()

    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToAccessDbKey"
    
    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToAccessDbWithPasswordKey"
End Sub


'''
'''
'''
Private Sub msubSanityTestToOpenUSetAdoOleDbConStrForAccessOleDbWithMsAccessDb()

    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
 
    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsAccessDb()

    msubSetUserInputParametersDicOfAccessOleDbToMsAccessDbByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestAccOleDbToAccessDbKey"

    If Not blnIsRequestedToCancelAdoConnection Then
    
        If IsADOConnectionStringEnabled(GetADOConStrOfAccessOleDbToMsAccessDbFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
    
            Debug.Print "Connected to Ms Access Db by Ms Access OLE DB using ADO"
        End If
    End If
End Sub

