Attribute VB_Name = "ADOConStrToolsForCSV"
'
'   Generate ADODB connection string to CSV files
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'       Dependent on ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  7/Feb/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' CSV files connection
'''
Public Function GetADODBConnectionOleDbStringForCSVFile(ByVal vstrDirectoryPath As String, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True) As String

    Dim strConnectionString As String
    Dim strProvider As String, strExtendedProperties As String
    
    strProvider = GetAccessOLEDBProviderNameFromEnm(venmAccessOLEDBProviderType)

    Select Case venmAccessOLEDBProviderType
    
        Case AccessOLEDBProviderType.AceOLEDB_12P0, AccessOLEDBProviderType.AceOLEDB_16P0
        
            strExtendedProperties = "Text;HDR="
        
            If vblnHeaderFieldTitleExists Then
            
                strExtendedProperties = strExtendedProperties & "Yes;"
            Else
                strExtendedProperties = strExtendedProperties & "No;"
            End If
        
            strExtendedProperties = strExtendedProperties & "FMT=Delimited;"
        
            strConnectionString = "Provider=" & strProvider & ";" & _
                            "Data Source=" & vstrDirectoryPath & "\;Extended Properties=""" & strExtendedProperties & """"
        
        Case AccessOLEDBProviderType.JetOLEDB_4P0, AccessOLEDBProviderType.JetOLEDB_3P51
    
            strExtendedProperties = "Text;HDR="
        
            If vblnHeaderFieldTitleExists Then
            
                strExtendedProperties = strExtendedProperties & "YES;"
            Else
                strExtendedProperties = strExtendedProperties & "NO;"
            End If
        
            strExtendedProperties = strExtendedProperties & ";FMT=Delimited"
        
            strConnectionString = "Provider=" & strProvider & ";" & _
                            "Data Source=" & vstrDirectoryPath & "\;Extended Properties=""" & strExtendedProperties & """"
    
    End Select

    GetADODBConnectionOleDbStringForCSVFile = strConnectionString
End Function
