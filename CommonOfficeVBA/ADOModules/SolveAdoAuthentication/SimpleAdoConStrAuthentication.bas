Attribute VB_Name = "SimpleAdoConStrAuthentication"
'
'   ADO connection string save and load between a Dictionary object and a Windows-registry key values except for any passwords
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Windows OS
'       Dependent on ConvertDicToWinReg.bas, WinRegGeneral.bas
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 13/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
'**---------------------------------------------
'** Public constants
'**---------------------------------------------
Public Const mstrTemporaryPdKey As String = "Password"

Public Const mstrAdoConnectionTypeAndDestinationKey As String = "ConnectTypeAndDestination"

Public Const mstrAdoDbFilePathKey As String = "DbFilePath"

'**---------------------------------------------
'** Internal private constants
'**---------------------------------------------
Private Const mstrModuleKey As String = "SimpleAdoConStrAuthentication"   ' 1st part of registry sub-key

Private Const mstrSubjectCachingAdoConStrParamsKey As String = "CachingAdoConStrParams"   ' 2nd part of registry sub-key

Private Const mstrModuleNameAndSubjectKeyForAdoConStrCachingMode As String = mstrModuleKey & "\" & mstrSubjectCachingAdoConStrParamsKey


Private Const mstrWinRegValueNameOfAllowToSaveAdoConnectionParams As String = "AllowToSaveAdoConnectionParams"

Private Const mstrWinRegValueNameOfAllowToSaveAdoConStrPasswordTemporarily As String = "AllowToSaveAdoConStrPdTemporarily"

Private Const mstrWinRegValueNameOfAllowToSaveMsExcelLockFilePasswordTemporarily As String = "AllowToSaveMsExcelLockFilePdTemporarily"

Private Const mstrWinRegValueNameOfAllowToSaveMsAccessDbLockFilePasswordTemporarily As String = "AllowToSaveMsAccessDbLockFilePdTemporarily"


Private Const mstrRegKeySecureOfficeCommonVBAAdoConStr As String = "HKEY_CURRENT_USER\Software\VBASecurePosition\OfficeCommonVBA\AdoConStrSettings"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** ADO connection info setting keeper control
'**---------------------------------------------
Private mblnSaveParametersInRegistoryExceptForPassword As Boolean

Private mblnIsTheValue_SaveParametersInRegistoryExceptForPassword_Initialized As Boolean


'**---------------------------------------------
'** About ADO connection password caching
'**---------------------------------------------

Private mblnSaveAdoConStrPasswordInTemporaryCache As Boolean

Private mblnIsTheValue_SaveAdoConStrPasswordInTemporaryCache_Initialized As Boolean

Private mblnIsTheFunction_SaveAdoConStrPasswordInTemporaryCache_Enabled As Boolean



Private mblnSaveMsExcelFileLockPasswordInTemporaryCache As Boolean

Private mblnIsTheValue_SaveMsExcelFileLockPasswordInTemporaryCache_Initialized As Boolean

Private mblnIsTheFunction_SaveMsExcelFileLockPasswordInTemporaryCache_Enabled As Boolean



Private mblnSaveMsAccessDbFileLockPasswordInTemporaryCache As Boolean

Private mblnIsTheValue_SaveMsAccessDbFileLockPasswordInTemporaryCache_Initialized As Boolean

Private mblnIsTheFunction_SaveMsAccessDbFileLockPasswordInTemporaryCache_Enabled As Boolean


'**---------------------------------------------
'** About confirmed no password lock file-paths
'**---------------------------------------------
' cache
Private mobjNoPasswordLockFilePathDic As Scripting.Dictionary


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** ADO connection info setting keeper control
'**---------------------------------------------
'''
'''
'''
Public Sub SetAdoConnectionSaveParametersInRegistoryExceptForPassword(ByVal vblnSaveParametersInRegistoryExceptForPassword As Boolean)

    If mblnSaveParametersInRegistoryExceptForPassword <> vblnSaveParametersInRegistoryExceptForPassword Then
    
        mblnSaveParametersInRegistoryExceptForPassword = vblnSaveParametersInRegistoryExceptForPassword
    
        WriteCurUserRegBoolByModuleNameAndSubjectSubKey mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveAdoConnectionParams, vblnSaveParametersInRegistoryExceptForPassword
    End If
End Sub
Public Function GetAdoConnectionSaveParametersInRegistoryExceptForPassword() As Boolean

    Dim blnReadValue As Boolean

    If Not mblnIsTheValue_SaveParametersInRegistoryExceptForPassword_Initialized Then
    
        mblnSaveParametersInRegistoryExceptForPassword = True
        
        If IsCurUserRegBoolAboutAllowToSaveAdoConnectionParams(blnReadValue) Then
        
            mblnSaveParametersInRegistoryExceptForPassword = blnReadValue
        End If
        
        mblnIsTheValue_SaveParametersInRegistoryExceptForPassword_Initialized = True
    End If

    GetAdoConnectionSaveParametersInRegistoryExceptForPassword = mblnSaveParametersInRegistoryExceptForPassword
End Function

'''
'''
'''
Public Function IsCurUserRegBoolAboutAllowToSaveAdoConnectionParams(ByRef rblnReadValue As Boolean) As Boolean

    Dim blnIsValueRead As Boolean

    blnIsValueRead = False

    If IsCurUserRegBoolReadFromModuleNameAndSubjectSubKey(rblnReadValue, mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveAdoConnectionParams) Then
    
        blnIsValueRead = True
    End If

    IsCurUserRegBoolAboutAllowToSaveAdoConnectionParams = blnIsValueRead
End Function


'**---------------------------------------------
'** About ADO connection password caching
'**---------------------------------------------
'''
'''
'''
''' <Argument>robjUserInputParametersDic: Input-Output</Argument>
''' <Return>Boolean</Return>
Public Function IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache(ByRef robjUserInputParametersDic As Scripting.Dictionary) As Boolean

    Dim blnIsOpeningFormNeeded As Boolean, strPassword As String, strConStrInfoKey As String, enmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType

    blnIsOpeningFormNeeded = True

    If GetAllowToSaveAdoConStrPasswordInTemporaryCache() Then
        
        If IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() Then
        
            ' If the SimplePdAuthenticationAdo.bas had been removed in this VB project, the following doesn't causes a VBA syntax error
            
            strPassword = ""
            
            enmOleDbOrOdbcAdoConnectionDestinationRDBType = GetEnumOfOleDbOrOdbcAdoConnectionDestinationRDBTypeFromDescription(robjUserInputParametersDic.Item(mstrAdoConnectionTypeAndDestinationKey))
            
            Select Case enmOleDbOrOdbcAdoConnectionDestinationRDBType
            
                Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsExcel
                
                    strPassword = GetStringFromCallBackInterfaceAndOneStringArgument("GetLockingBookPasswordByPath", robjUserInputParametersDic.Item(mstrAdoDbFilePathKey))
                    
                Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsAccessDb
                
                    strPassword = GetStringFromCallBackInterfaceAndOneStringArgument("GetLockingAccdbPasswordByPath", robjUserInputParametersDic.Item(mstrAdoDbFilePathKey))
                Case Else
            
                    strConStrInfoKey = GetAdoConStrKeyFromUserInputParametersDic(robjUserInputParametersDic)
                    
                    Debug.Print "At temporary Pd reading [ConStrInfoKey] - " & strConStrInfoKey
                    
                    'strPassword = GetRdbUserPasswordByAdoConStrPartKey(strConStrInfoKey)
                    
                    strPassword = GetStringFromCallBackInterfaceAndOneStringArgument("GetRdbUserPasswordByAdoConStrPartKey", GetAdoConStrKeyFromUserInputParametersDic(robjUserInputParametersDic))
            End Select
            
            If strPassword <> "" Then
                    
                robjUserInputParametersDic.Add mstrTemporaryPdKey, strPassword
                
                blnIsOpeningFormNeeded = False
            End If
        End If
    End If

    IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache = blnIsOpeningFormNeeded
End Function


'''
''' This is applied to ODBC connection for both PostgreSQL, Oracle
'''
Public Function IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() As Boolean

    Dim blnIsToolVBModuleExisted As Boolean, blnReadValue As Boolean

    If Not mblnIsTheValue_SaveAdoConStrPasswordInTemporaryCache_Initialized Then
    
        ' confirm the SimplePdAuthenticationAdo.bas exists
        
        blnIsToolVBModuleExisted = False
        
        On Error Resume Next
        
        blnIsToolVBModuleExisted = Application.Run("IsAdoPdAuthenticationCachingEnabled")
        
        On Error GoTo 0
        
        mblnIsTheFunction_SaveAdoConStrPasswordInTemporaryCache_Enabled = blnIsToolVBModuleExisted
        
        If blnIsToolVBModuleExisted Then
        
            ' read an enabled value from registry
        
            blnReadValue = False
        
            If IsCurUserRegBoolAboutAllowToSaveAdoConStrPasswordTemporarily(blnReadValue) Then
        
                mblnSaveAdoConStrPasswordInTemporaryCache = blnReadValue
            End If
        End If
    
        mblnIsTheValue_SaveAdoConStrPasswordInTemporaryCache_Initialized = True
    End If

    IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled = mblnIsTheFunction_SaveAdoConStrPasswordInTemporaryCache_Enabled
End Function

'''
'''
'''
Public Function IsCurUserRegBoolAboutAllowToSaveAdoConStrPasswordTemporarily(ByRef rblnReadValue As Boolean) As Boolean

    Dim blnIsValueRead As Boolean

    blnIsValueRead = False

    If IsCurUserRegBoolReadFromModuleNameAndSubjectSubKey(rblnReadValue, mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveAdoConStrPasswordTemporarily) Then
    
        blnIsValueRead = True
    End If
    
    IsCurUserRegBoolAboutAllowToSaveAdoConStrPasswordTemporarily = blnIsValueRead
End Function


'''
'''
'''
Public Sub SetAllowToSaveAdoConStrPasswordInTemporaryCache(ByVal vblnSaveAdoConStrPasswordInTemporaryCache As Boolean)

    If IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() Then
    
        If mblnSaveAdoConStrPasswordInTemporaryCache <> vblnSaveAdoConStrPasswordInTemporaryCache Then
        
            mblnSaveAdoConStrPasswordInTemporaryCache = vblnSaveAdoConStrPasswordInTemporaryCache
        
            WriteCurUserRegBoolByModuleNameAndSubjectSubKey mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveAdoConStrPasswordTemporarily, vblnSaveAdoConStrPasswordInTemporaryCache
        End If
    End If
End Sub
Public Function GetAllowToSaveAdoConStrPasswordInTemporaryCache() As Boolean

    Dim blnAllowToSave As Boolean, blnReadValue As Boolean

    blnAllowToSave = False

    If IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() Then
    
        blnReadValue = False
        
        If IsCurUserRegBoolAboutAllowToSaveAdoConStrPasswordTemporarily(blnReadValue) Then
        
            mblnSaveAdoConStrPasswordInTemporaryCache = blnReadValue
        End If
    
        blnAllowToSave = mblnSaveAdoConStrPasswordInTemporaryCache
    End If

    GetAllowToSaveAdoConStrPasswordInTemporaryCache = blnAllowToSave
End Function

'**---------------------------------------------
'** About Microsoft Excel file lock password caching
'**---------------------------------------------
'''
''' This is applied to Excel book OLE DB connection
'''
Public Function IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled() As Boolean

    Dim blnIsToolVBModuleExisted As Boolean, blnReadValue As Boolean

    If Not mblnIsTheValue_SaveMsExcelFileLockPasswordInTemporaryCache_Initialized Then
    
        ' confirm the SimplePdAuthenticationAdo.bas exists
        
        blnIsToolVBModuleExisted = False
        
        On Error Resume Next
        
        blnIsToolVBModuleExisted = Application.Run("IsMsExcelFilePdCachingEnabled")
        
        On Error GoTo 0
        
        mblnIsTheFunction_SaveMsExcelFileLockPasswordInTemporaryCache_Enabled = blnIsToolVBModuleExisted
        
        If blnIsToolVBModuleExisted Then
        
            ' read an enabled value from registry
        
            blnReadValue = False
        
            If IsCurUserRegBoolReadFromModuleNameAndSubjectSubKey(blnReadValue, mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveMsExcelLockFilePasswordTemporarily) Then
        
                mblnSaveMsExcelFileLockPasswordInTemporaryCache = blnReadValue
            End If
        End If
    
        mblnIsTheValue_SaveMsExcelFileLockPasswordInTemporaryCache_Initialized = True
    End If

    IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled = mblnIsTheFunction_SaveMsExcelFileLockPasswordInTemporaryCache_Enabled
End Function


'''
'''
'''
Public Sub SetAllowToSaveMsExcelLockFilePasswordInTemporaryCache(ByVal vblnSaveMsExcelLockFilePasswordInTemporaryCache As Boolean)

    If IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled() Then
    
        If mblnSaveMsExcelFileLockPasswordInTemporaryCache <> vblnSaveMsExcelLockFilePasswordInTemporaryCache Then
        
            mblnSaveMsExcelFileLockPasswordInTemporaryCache = vblnSaveMsExcelLockFilePasswordInTemporaryCache
        
            WriteCurUserRegBoolByModuleNameAndSubjectSubKey mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveMsExcelLockFilePasswordTemporarily, vblnSaveMsExcelLockFilePasswordInTemporaryCache
        End If
    End If
End Sub
Public Function GetAllowToSaveMsExcelLockFilePasswordInTemporaryCache() As Boolean

    Dim blnAllowToSave As Boolean

    blnAllowToSave = False

    If IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled() Then
    
        blnAllowToSave = mblnSaveMsExcelFileLockPasswordInTemporaryCache
    End If

    GetAllowToSaveMsExcelLockFilePasswordInTemporaryCache = blnAllowToSave
End Function



'**---------------------------------------------
'** About Microsoft Access database file lock password caching
'**---------------------------------------------
'''
''' This is applied to Access database OLE DB connection
'''
Public Function IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled() As Boolean

    Dim blnIsToolVBModuleExisted As Boolean, blnReadValue As Boolean

    If Not mblnIsTheValue_SaveMsAccessDbFileLockPasswordInTemporaryCache_Initialized Then
    
        ' confirm the SimplePdAuthenticationAdo.bas exists
        
        blnIsToolVBModuleExisted = False
        
        On Error Resume Next
        
        blnIsToolVBModuleExisted = Application.Run("IsMsAccessDbFilePdCachingEnabled")
        
        On Error GoTo 0
        
        mblnIsTheFunction_SaveMsAccessDbFileLockPasswordInTemporaryCache_Enabled = blnIsToolVBModuleExisted
        
        If blnIsToolVBModuleExisted Then
        
            ' read an enabled value from registry
        
            blnReadValue = False
        
            If IsCurUserRegBoolReadFromModuleNameAndSubjectSubKey(blnReadValue, mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveMsAccessDbLockFilePasswordTemporarily) Then
        
                mblnSaveMsAccessDbFileLockPasswordInTemporaryCache = blnReadValue
            End If
        End If
    
        mblnIsTheValue_SaveMsAccessDbFileLockPasswordInTemporaryCache_Initialized = True
    End If

    IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled = mblnIsTheFunction_SaveMsAccessDbFileLockPasswordInTemporaryCache_Enabled
End Function


'''
'''
'''
Public Sub SetAllowToSaveMsAccessDbLockFilePasswordInTemporaryCache(ByVal vblnSaveMsAccessDbLockFilePasswordInTemporaryCache As Boolean)

    If IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled() Then
    
        If mblnSaveMsAccessDbFileLockPasswordInTemporaryCache <> vblnSaveMsAccessDbLockFilePasswordInTemporaryCache Then
        
            mblnSaveMsAccessDbFileLockPasswordInTemporaryCache = vblnSaveMsAccessDbLockFilePasswordInTemporaryCache
        
            WriteCurUserRegBoolByModuleNameAndSubjectSubKey mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveMsAccessDbLockFilePasswordTemporarily, vblnSaveMsAccessDbLockFilePasswordInTemporaryCache
        End If
    End If
End Sub
Public Function GetAllowToSaveMsAccessDbLockFilePasswordInTemporaryCache() As Boolean

    Dim blnAllowToSave As Boolean

    blnAllowToSave = False

    If IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled() Then
    
        blnAllowToSave = mblnSaveMsAccessDbFileLockPasswordInTemporaryCache
    End If

    GetAllowToSaveMsAccessDbLockFilePasswordInTemporaryCache = blnAllowToSave
End Function

'**---------------------------------------------
'** Write or read from Windows-regisrty using FormStateToSetAdoConStr object
'**---------------------------------------------
'''
'''
'''
Public Function GetFormStateToSetAdoConStrByDefaultParams(ByRef robjUserInputParametersDic As Scripting.Dictionary, ByRef rstrSettingKeyName As String) As FormStateToSetAdoConStr

    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr
    
    Set objFormStateToSetAdoConStr = New FormStateToSetAdoConStr
    
    With objFormStateToSetAdoConStr
    
        Set .UserInputParametersDic = robjUserInputParametersDic
    
        .SettingKeyName = rstrSettingKeyName
    End With
    
    Set GetFormStateToSetAdoConStrByDefaultParams = objFormStateToSetAdoConStr
End Function

'''
'''
'''
''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
''' <Argument>robjUserInputParametersDic: Output</Argument>
''' <Argument>robjFormStateToSetAdoConStr: Input</Argument>
Public Sub AfterProcessAdoConStrFormSetting(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr)

    If Not robjFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection Then

        If GetAllowToSaveAdoConStrPasswordInTemporaryCache() Then
        
            UpdateWinRegKeyValuesOfAdoConnectionStringInfoFromAdoConStrFormStateDic robjFormStateToSetAdoConStr
        End If
        
        Set robjUserInputParametersDic = robjFormStateToSetAdoConStr.UserInputParametersDic
    Else
        rblnIsRequestedToCancelAdoConnection = True
    End If
End Sub


'''
''' write ADO connection string information to a Windows-registry
'''
''' <Argument>robjFormStateToSetAdoConStr: Input</Argument>
Public Sub UpdateWinRegKeyValuesOfAdoConnectionStringInfoFromAdoConStrFormStateDic(ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr)

    With robjFormStateToSetAdoConStr
        
        msubManagePasswordOfUserInputParametersDic .UserInputParametersDic
    
        msubUpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromAdoConStrInfoDic .SettingKeyName, .UserInputParametersDic
    End With
End Sub

'''
'''
'''
Private Sub msubUpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromAdoConStrInfoDic(ByRef rstrSettingKeyName As String, ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim objDic As Scripting.Dictionary

    Set objDic = GetFilteredDicExceptForSpecifiedPluralKeys(robjUserInputParametersDic, mstrTemporaryPdKey)

    UpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromDic mstrRegKeySecureOfficeCommonVBAAdoConStr, rstrSettingKeyName, objDic
End Sub


'**---------------------------------------------
'** Write or read from Windows-regisrty
'**---------------------------------------------
'''
''' write ADO connection string information to a Windows-registry
'''
''' <Argument>rstrSettingKeyName: Input - This is same as Windows-registry child sub-key name</Argument>
''' <Argument>robjUserInputParametersDic: Input</Argument>
Public Sub UpdateWinRegKeyValuesOfAdoConnectionStringInfoFromDic(ByRef rstrSettingKeyName As String, ByRef robjUserInputParametersDic As Scripting.Dictionary)

    UpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromDic mstrRegKeySecureOfficeCommonVBAAdoConStr, rstrSettingKeyName, robjUserInputParametersDic
End Sub

'''
''' delete ADO connection string information from a Windows-registry
'''
''' <Argument>rstrSettingKeyName: Input - This is same as Windows-registry child sub-key name</Argument>
Public Sub DeleteWinRegKeyValuesOfAdoConnectionStringInfo(ByRef rstrSettingKeyName As String)

    DeleteWinRegKeyValuesBySpecifyingChildSubKeyName mstrRegKeySecureOfficeCommonVBAAdoConStr, rstrSettingKeyName
End Sub

'''
''' read ADO connection string information as a Dictionary object
'''
''' <Argument>rstrSettingKeyName: Input - This is same as Windows-registry child sub-key name</Argument>
''' <Return>Dictionary(Of String[ValueName], Variant[Value])</Return>
Public Function GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(ByRef rstrSettingKeyName As String) As Scripting.Dictionary

    Set GetDicOfAdoConnectionStringInfoFromReadingWinRegValues = GetDicBySpecifyingChildSubKeyNameFromReadingWinRegValues(mstrRegKeySecureOfficeCommonVBAAdoConStr, rstrSettingKeyName)
End Function

'''
'''
'''
Public Function GetFilteredOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey(ByVal venmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType) As Scripting.Dictionary

    Dim objFilteredOneNestedDic As Scripting.Dictionary, varSettingKeyName As Variant, objChildDic As Scripting.Dictionary

    Set objFilteredOneNestedDic = New Scripting.Dictionary

    With GetOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey()
    
        For Each varSettingKeyName In .Keys
        
            Set objChildDic = .Item(varSettingKeyName)
            
            With objChildDic
            
                If .Exists(mstrAdoConnectionTypeAndDestinationKey) Then
                
                    If GetEnumOfOleDbOrOdbcAdoConnectionDestinationRDBTypeFromDescription(.Item(mstrAdoConnectionTypeAndDestinationKey)) = venmOleDbOrOdbcAdoConnectionDestinationRDBType Then
                    
                        objFilteredOneNestedDic.Add varSettingKeyName, objChildDic
                    End If
                End If
            End With
        Next
    End With

    Set GetFilteredOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey = objFilteredOneNestedDic
End Function



'''
''' read ADO current all connection settings in the current user Windows-registry
'''
''' <Return>Dictionary(Of String[ChildSubKeyName], Dictionary(Of String[ValueName], Variant[Value]))</Return>
Public Function GetOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey() As Scripting.Dictionary

    Set GetOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey = GetOneNestedDicFromReadingWinRegChiledKeysAndValuesOfEachKey(mstrRegKeySecureOfficeCommonVBAAdoConStr)
End Function


'''
'''
'''
Public Function GetAdoConStrKeyFromUserInputParametersDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As String

    Dim strConStrPartKey As String, varKey As Variant, strKey As String
    
    strConStrPartKey = ""
    
    With robjUserInputParametersDic
    
        For Each varKey In .Keys
        
            strKey = varKey
            
            Select Case strKey
            
                Case mstrAdoConnectionTypeAndDestinationKey, mstrTemporaryPdKey
            
                    ' nothing to do
                    
                Case Else
                
                    If strConStrPartKey <> "" Then
                    
                        strConStrPartKey = strConStrPartKey & ";"
                    End If
            
                    strConStrPartKey = strConStrPartKey & .Item(strKey)
            End Select
        Next
    End With
    
    GetAdoConStrKeyFromUserInputParametersDic = strConStrPartKey
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** Write or read from Windows-regisrty using FormStateToSetAdoConStr object
'**---------------------------------------------
'''
'''
'''
Private Sub msubManagePasswordOfUserInputParametersDic(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim enmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType, strPassword As String, strConStrInfoKey As String, strPath As String

    With robjUserInputParametersDic
    
        If .Exists(mstrTemporaryPdKey) Then
        
            If GetAllowToSaveAdoConStrPasswordInTemporaryCache() Then
            
                ' Temporarily cache password
            
                strPassword = .Item(mstrTemporaryPdKey)
            
                enmOleDbOrOdbcAdoConnectionDestinationRDBType = GetEnumOfOleDbOrOdbcAdoConnectionDestinationRDBTypeFromDescription(.Item(mstrAdoConnectionTypeAndDestinationKey))
            
                Select Case enmOleDbOrOdbcAdoConnectionDestinationRDBType
                
                    Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsExcel
                    
                        If IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled() Then
                        
                            ' If the SimplePdAuthentication.bas had been removed in this VB project, the following doesn't causes a VBA syntax error
                        
                            strPath = .Item(mstrAdoDbFilePathKey)
                        
                            CallBackInterfaceByTwoStringArguments "StorePasswordOfBookTemporarily", strPath, strPassword
                        End If
                    
                    Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsAccessDb
                
                        If IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled() Then
                        
                            ' If the SimplePdAuthentication.bas had been removed in this VB project, the following doesn't causes a VBA syntax error
                        
                            strPath = .Item(mstrAdoDbFilePathKey)
                        
                            CallBackInterfaceByTwoStringArguments "StorePasswordOfAccdbTemporarily", strPath, strPassword
                        End If
                
                    Case Else
                
                        If IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() Then
                        
                            ' If the SimplePdAuthenticationAdo.bas had been removed in this VB project, the following doesn't causes a VBA syntax error
                        
                            strConStrInfoKey = GetAdoConStrKeyFromUserInputParametersDic(robjUserInputParametersDic)
                        
                            Debug.Print "At temporary Pd writing [ConStrInfoKey] - " & strConStrInfoKey
                        
                            'StoreRdbUserPasswordByAdoConStrPartKey strConStrInfoKey, strPassword
                        
                            CallBackInterfaceByTwoStringArguments "StoreRdbUserPasswordByAdoConStrPartKey", GetAdoConStrKeyFromUserInputParametersDic(robjUserInputParametersDic), strPassword
                        End If
                End Select
            End If
        End If
    End With
End Sub



