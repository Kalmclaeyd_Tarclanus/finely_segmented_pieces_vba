Attribute VB_Name = "SimplePdAuthenticationAdo"
'
'   Simply password keeper for some type RDB (relational data-base)
'   If users feel some risks for keeping passwords in VB project variable cache, they can remove this VB code module from this VB project without any VBA syntax error.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  6/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjConnectionInfoToPasswordInfoDic As Scripting.Dictionary ' Dictionary(Of String[file path], String[password])


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
'''
'''
Public Enum RDBInfoCacheType
    
    PostgreSQLInfoCache = 1

    SQLiteInfoCache = 2
    
    OracleInfoCache = 4
    
    DSNConnectionCache = 10
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** ADO connection info setting password keeper control
'**---------------------------------------------
'''
''' This is called from Application.Run
'''
Public Function IsAdoPdAuthenticationCachingEnabled() As Boolean

    IsAdoPdAuthenticationCachingEnabled = True
End Function



'**---------------------------------------------
'** Cache RDB user password temporarily
'**---------------------------------------------
'''
'''
'''
Public Sub ClearCacheTemporaryRdbUserPasswords()

    If Not mobjConnectionInfoToPasswordInfoDic Is Nothing Then
    
        mobjConnectionInfoToPasswordInfoDic.RemoveAll
    
        Set mobjConnectionInfoToPasswordInfoDic = Nothing
    End If
End Sub

'''
'''
'''
Public Function GetRdbUserPasswordByAdoConStrPartKey(ByRef rstrAdoConStrPartKey As String) As String

    Dim strCachePassword As String
    
    strCachePassword = ""

    If Not mobjConnectionInfoToPasswordInfoDic Is Nothing Then
    
        With mobjConnectionInfoToPasswordInfoDic
        
            If .Exists(rstrAdoConStrPartKey) Then
            
                strCachePassword = .Item(rstrAdoConStrPartKey)
            End If
        End With
    End If

    GetRdbUserPasswordByAdoConStrPartKey = strCachePassword
End Function

'''
''' Serve a ADO connection supported RDB password
'''
Public Sub StoreRdbUserPasswordByAdoConStrPartKey(ByRef rstrAdoConStrPartKey As String, ByRef rstrPassword As String)

    If mobjConnectionInfoToPasswordInfoDic Is Nothing Then Set mobjConnectionInfoToPasswordInfoDic = New Scripting.Dictionary

    With mobjConnectionInfoToPasswordInfoDic
    
        If Not .Exists(rstrAdoConStrPartKey) Then
        
            .Add rstrAdoConStrPartKey, rstrPassword
        Else
            .Item(rstrAdoConStrPartKey) = rstrPassword
        End If
    End With
End Sub

