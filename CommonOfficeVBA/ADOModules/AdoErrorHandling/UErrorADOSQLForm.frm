VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UErrorADOSQLForm 
   Caption         =   "UErrorADOSQLForm"
   ClientHeight    =   6450
   ClientLeft      =   50
   ClientTop       =   380
   ClientWidth     =   5790
   OleObjectBlob   =   "UErrorADOSQLForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "UErrorADOSQLForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   ADO SQL error display form
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'       Dependent on ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  8/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "UErrorADOSQLForm"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjErrorADOSQL As ErrorADOSQL

Private mblnSQLExeAsynchronous As Boolean

Private mobjDynamicCacheKeyToDic As Scripting.Dictionary

Private mobjIndexToAxBarBtnEvHdlForTxtClipBoardDic As Scripting.Dictionary

Private mblnIsInitialized As Boolean

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////

Public Property Set CurrentError(ByVal vobjError As ErrorADOSQL)

    Set mobjErrorADOSQL = vobjError
End Property
Public Property Get CurrentError() As ErrorADOSQL

    Set CurrentError = mobjErrorADOSQL
End Property

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub cmdOK_Click()

    Unload Me
End Sub

Private Sub UserForm_Initialize()

    mblnSQLExeAsynchronous = False
    
    Set mobjIndexToAxBarBtnEvHdlForTxtClipBoardDic = New Scripting.Dictionary
End Sub

Private Sub UserForm_Layout()

    If Not mblnIsInitialized Then

        msubInitializeAxBarBtnEvHdlForTxtClipBoardForTextBoxes

        Localize
        
        mblnIsInitialized = True
    End If

    RefreshControls
End Sub

Private Sub UserForm_Terminate()

    mobjIndexToAxBarBtnEvHdlForTxtClipBoardDic.RemoveAll
    
    Set mobjIndexToAxBarBtnEvHdlForTxtClipBoardDic = Nothing

    Set mobjErrorADOSQL = Nothing
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
Public Sub Localize()

    msubLocalize
    
    If mblnSQLExeAsynchronous Then
    
        Me.Caption = mobjDynamicCacheKeyToDic.Item("STR_KEY_ERROR_ADOSQL_ASYNCHRONOUS_SQL_EXECUTION_ERROR")
    Else
        Me.Caption = mobjDynamicCacheKeyToDic.Item("STR_KEY_ERROR_ADOSQL_SYNCHRONOUS_SQL_EXECUTION_ERROR")
    End If
    
End Sub

'''
''' simplified form-show
'''
Public Sub SetAndShowForm(ByVal vobjErrorADOSQL As ErrorADOSQL, Optional ByVal vblnSQLExeAsynchronous As Boolean = False)

    'Me.Localize
    
    Set mobjErrorADOSQL = vobjErrorADOSQL
    
    mblnSQLExeAsynchronous = vblnSQLExeAsynchronous
End Sub

'''
''' refresh log texts
'''
Public Sub RefreshControls()

    Dim objControl As MSForms.Control ', objCommandButton As MSForms.CommandButton
    Dim objTextBox As MSForms.TextBox


    On Error Resume Next
    
    With mobjErrorADOSQL
    
        txtErrorDescription.Text = .GetErrorSummary()
        
        txtSQLElapsedTime.Text = Format(CDbl(.ADOSQLResult.SQLExeElapsedTime) / 1000, "0.000")
        
        txtSQL.Text = .ADOSQLResult.SQL
        
        txtErrOccurDateTime.Text = FormatDateTime(.ADOSQLResult.SQLExeTime, vbLongDate) & ", " & FormatDateTime(.ADOSQLResult.SQLExeTime, vbLongTime)
    
        txtCommandTimeout.Text = Format(CDbl(.LoadedConnectionSetting.CommandTimeout), "0.000")
        
        txtCursorLocation.Text = GetADOEnumLiteralFromCursorLocation(.LoadedConnectionSetting.CursorLocation)
        
        txtCursorLocation.ControlTipText = GetADOEnumDescriptionJPNFromCursorLocation(.LoadedConnectionSetting.CursorLocation)
    
    
        txtExecuteOption.Text = GetADOEnumAllLiteralsFromExecuteOption(.LoadedConnectionSetting.ExecuteOption)
        
        txtExecuteOption.ControlTipText = GetADOEnumAllDescriptionsJPNFromExecuteOption(.LoadedConnectionSetting.ExecuteOption)
        
        If .IsRecordsetExists Then
        
            txtRecordsetCursorType.Text = GetADOEnumLiteralFromRecordsetCursorType(.LoadedConnectionSetting.CursorType)
            
            txtRecordsetCursorType.ControlTipText = GetADOEnumDescriptionJPNFromRecordsetCursorType(.LoadedConnectionSetting.CursorType)
            
            
            txtRecordsetLockType.Text = GetADOEnumLiteralFromRecordsetLockType(.LoadedConnectionSetting.LockType)
            
            txtRecordsetLockType.ControlTipText = GetADOEnumDescriptionJPNFromRecordsetLockType(.LoadedConnectionSetting.LockType)
        Else
            fraRecordsetOpenParam.Visible = False
        End If
        
        cmdOK.Caption = "OK"
    End With

    
    For Each objControl In Me.Controls
        
        If TypeOf objControl Is MSForms.TextBox Then
            
            Set objTextBox = objControl
            
            objTextBox.Locked = True
            'objTextBox.Enabled = False
        End If
    Next

End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubInitializeAxBarBtnEvHdlForTxtClipBoardForTextBoxes()

    Dim objApplication As Object
    Dim objAxBarBtnEvHdlForTxtClipBoard As AxBarBtnEvHdlForTxtClipBoard


    Set objApplication = GetCurrentOfficeFileObject().Application

    With mobjIndexToAxBarBtnEvHdlForTxtClipBoardDic
    
        Set objAxBarBtnEvHdlForTxtClipBoard = New AxBarBtnEvHdlForTxtClipBoard
        
        objAxBarBtnEvHdlForTxtClipBoard.Initialize txtErrorDescription, objApplication, ClipBoardMenuCopyEnabled
        
        .Add 0, objAxBarBtnEvHdlForTxtClipBoard
        
        Set objAxBarBtnEvHdlForTxtClipBoard = New AxBarBtnEvHdlForTxtClipBoard
        
        objAxBarBtnEvHdlForTxtClipBoard.Initialize txtSQL, objApplication, ClipBoardMenuCopyEnabled
        
        .Add 1, objAxBarBtnEvHdlForTxtClipBoard
    End With
End Sub



'**---------------------------------------------
'** Key-Value cache preparation for RClickOnAxCtl
'**---------------------------------------------
'''
'''
'''
Private Sub msubLocalize()

    Dim objKeyToTextDic As Scripting.Dictionary
    
    Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

    If objKeyToTextDic Is Nothing Then
    
        ' If the Key-Value table file is lost, then the default setting is used
    
        Set objKeyToTextDic = New Scripting.Dictionary
    
        Select Case GetCurrentUICaptionLanguageType()
    
            Case CaptionLanguageType.UIJapaneseCaptions
            
                AddStringKeyValueForUErrorADOSQLFormByJapaneseValues objKeyToTextDic
            Case Else
            
                AddStringKeyValueForUErrorADOSQLFormByEnglishValues objKeyToTextDic
        End Select
    End If
    
    If mobjDynamicCacheKeyToDic Is Nothing Then
    
        Set mobjDynamicCacheKeyToDic = New Scripting.Dictionary
        
        mobjDynamicCacheKeyToDic.Add "STR_KEY_ERROR_ADOSQL_SYNCHRONOUS_SQL_EXECUTION_ERROR", objKeyToTextDic.Item("STR_KEY_ERROR_ADOSQL_SYNCHRONOUS_SQL_EXECUTION_ERROR")
        
        mobjDynamicCacheKeyToDic.Add "STR_KEY_ERROR_ADOSQL_ASYNCHRONOUS_SQL_EXECUTION_ERROR", objKeyToTextDic.Item("STR_KEY_ERROR_ADOSQL_ASYNCHRONOUS_SQL_EXECUTION_ERROR")
    End If
    
    AssigningStringValuesForUErrorADOSQLForm objKeyToTextDic
End Sub


'''
''' Assign each target string property from dictionary values
'''
''' automatically-added for UErrorADOSQLForm string-key-value management
Private Sub AssigningStringValuesForUErrorADOSQLForm(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        lblErrorDescription.Caption = .Item("STR_KEY_ERROR_ADOSQL_FORM_LBL_ERROR_DESCRIPTION")
        lblSQLElapsedTime.Caption = .Item("STR_KEY_ERROR_ADOSQL_FORM_LBL_SQL_ELAPSED_TIME")
        lblErrOccurDateTime.Caption = .Item("STR_KEY_ERROR_ADOSQL_FORM_LBL_ERR_OCCUR_DATETIME")
        
        fraADODBConnectionParam.Caption = .Item("STR_KEY_ERROR_ADOSQL_FORM_FRA_ADO_CON_OPTIONS")
        lblCommandTimeout.Caption = .Item("STR_KEY_ERROR_ADOSQL_FORM_LBL_COMMAND_TIMEOUT")
        lblCursorLocation.Caption = .Item("STR_KEY_ERROR_ADOSQL_FORM_LBL_CURSOR_LOCATION")
        lblExecuteOption.Caption = .Item("STR_KEY_ERROR_ADOSQL_FORM_LBL_EXECUTE_OPTION")
        
        fraRecordsetOpenParam.Caption = .Item("STR_KEY_ERROR_ADOSQL_FORM_LBL_RECORDSET_OPEN_PARAM")
        lblCursorType.Caption = .Item("STR_KEY_ERROR_ADOSQL_FORM_LBL_CURSOR_TYPE")
        lblLockType.Caption = .Item("STR_KEY_ERROR_ADOSQL_FORM_LBL_LOCK_TYPE")
    End With
End Sub

'''
''' Add Key-Values which values are in English for UErrorADOSQLForm key-values cache
'''
''' automatically-added for UErrorADOSQLForm string-key-value management
Private Sub AddStringKeyValueForUErrorADOSQLFormByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_ERROR_DESCRIPTION", "Error description"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_SQL_ELAPSED_TIME", "SQL elapsed time"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_ERR_OCCUR_DATETIME", "Error occurred date time"
        
        .Add "STR_KEY_ERROR_ADOSQL_FORM_FRA_ADO_CON_OPTIONS", "ADO connection options"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_COMMAND_TIMEOUT", "Command timeout"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_CURSOR_LOCATION", "Cursor location"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_EXECUTE_OPTION", "Execute options"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_RECORDSET_OPEN_PARAM", "Recordset open options"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_CURSOR_TYPE", "Cursor type"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_LOCK_TYPE", "Lock type"
        .Add "STR_KEY_ERROR_ADOSQL_SYNCHRONOUS_SQL_EXECUTION_ERROR", "SQL execution error : Synchronous"
        .Add "STR_KEY_ERROR_ADOSQL_ASYNCHRONOUS_SQL_EXECUTION_ERROR", "SQL execution error : Asynchronous"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for UErrorADOSQLForm key-values cache
'''
''' automatically-added for UErrorADOSQLForm string-key-value management
Private Sub AddStringKeyValueForUErrorADOSQLFormByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_ERROR_DESCRIPTION", "エラーの説明"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_SQL_ELAPSED_TIME", "SQL実行の経過時間"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_ERR_OCCUR_DATETIME", "エラー発生日時"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_FRA_ADO_CON_OPTIONS", "ADO接続オプション"
        
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_COMMAND_TIMEOUT", "ADOコマンドタイムアウト時間"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_CURSOR_LOCATION", "カーソルサービスの選択"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_EXECUTE_OPTION", "実行オプション"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_RECORDSET_OPEN_PARAM", "レコードセットの開く際のオプション"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_CURSOR_TYPE", "カーソルのタイプ"
        .Add "STR_KEY_ERROR_ADOSQL_FORM_LBL_LOCK_TYPE", "ロックの種類"
        .Add "STR_KEY_ERROR_ADOSQL_SYNCHRONOUS_SQL_EXECUTION_ERROR", "SQL実行時エラー : 同期実行"
        .Add "STR_KEY_ERROR_ADOSQL_ASYNCHRONOUS_SQL_EXECUTION_ERROR", "SQL実行時エラー : 非同期実行"
    End With
End Sub

'''
''' Remove Keys for UErrorADOSQLForm key-values cache
'''
''' automatically-added for UErrorADOSQLForm string-key-value management
Private Sub RemoveKeysOfKeyValueUErrorADOSQLForm(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_ERROR_ADOSQL_FORM_LBL_ERROR_DESCRIPTION") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_ERROR_ADOSQL_FORM_LBL_ERROR_DESCRIPTION"
            .Remove "STR_KEY_ERROR_ADOSQL_FORM_LBL_SQL_ELAPSED_TIME"
            .Remove "STR_KEY_ERROR_ADOSQL_FORM_LBL_ERR_OCCUR_DATETIME"
            
            .Remove "STR_KEY_ERROR_ADOSQL_FORM_FRA_ADO_CON_OPTIONS"
            .Remove "STR_KEY_ERROR_ADOSQL_FORM_LBL_COMMAND_TIMEOUT"
            .Remove "STR_KEY_ERROR_ADOSQL_FORM_LBL_CURSOR_LOCATION"
            .Remove "STR_KEY_ERROR_ADOSQL_FORM_LBL_EXECUTE_OPTION"
            .Remove "STR_KEY_ERROR_ADOSQL_FORM_LBL_RECORDSET_OPEN_PARAM"
            .Remove "STR_KEY_ERROR_ADOSQL_FORM_LBL_CURSOR_TYPE"
            .Remove "STR_KEY_ERROR_ADOSQL_FORM_LBL_LOCK_TYPE"
            .Remove "STR_KEY_ERROR_ADOSQL_SYNCHRONOUS_SQL_EXECUTION_ERROR"
            .Remove "STR_KEY_ERROR_ADOSQL_ASYNCHRONOUS_SQL_EXECUTION_ERROR"
        End If
    End With
End Sub


