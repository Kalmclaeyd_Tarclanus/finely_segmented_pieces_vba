Attribute VB_Name = "ADOFailedInformation"
'
'   ADO error information localized texts, which are refered in UErrorADOSQLForm form object
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  7/Feb/2023    Kalmclaeyd Tarclanus    Separated from ADOParameters
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrModuleName As String = "ADOFailedInformation"

'**---------------------------------------------
'** Key-Value cache preparation for ADOFailedInformation
'**---------------------------------------------
Private mobjStrKeyValueADOFailedInformationDic As Scripting.Dictionary

Private mblnIsStrKeyValueADOFailedInformationDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** get description for each enumeration (Japanese)
'**---------------------------------------------
Public Function GetADOEnumDescriptionJPNFromCursorLocation(ByVal venmCursorLocationEnum As ADODB.CursorLocationEnum) As String
    
    Dim strDescription As String

    Select Case venmCursorLocationEnum
        
        Case ADODB.CursorLocationEnum.adUseServer
        
            strDescription = GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseServer()
            
        Case ADODB.CursorLocationEnum.adUseClient
        
            strDescription = GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseClient()
    End Select

    GetADOEnumDescriptionJPNFromCursorLocation = strDescription
End Function

Public Function GetADOEnumDescriptionJPNFromCommandType(ByVal venmCommandTypeEnum As ADODB.CommandTypeEnum) As String
    
    Dim strDescription As String
    
    Select Case venmCommandTypeEnum
    
        Case ADODB.CommandTypeEnum.adCmdUnknown
        
            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumUnknown()
            
        Case ADODB.CommandTypeEnum.adCmdText
        
            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumText()
            
        Case ADODB.CommandTypeEnum.adCmdTable
        
            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTable()
            
        Case ADODB.CommandTypeEnum.adCmdTableDirect
        
            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTableDirect()
            
        Case ADODB.CommandTypeEnum.adCmdStoredProc
        
            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumStoredProcedure()
            
        Case ADODB.CommandTypeEnum.adCmdFile
        
            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumFile()
    End Select

    GetADOEnumDescriptionJPNFromCommandType = strDescription
End Function

'''
''' this evaluate individual flag
'''
Public Function GetADOEnumDescriptionJPNFromExecuteOption(ByVal venmExecutionOptionEnum As ADOR.ExecuteOptionEnum) As String
    
    Dim strDescription As String
    
    Select Case venmExecutionOptionEnum
    
        Case ADOR.ExecuteOptionEnum.adAsyncExecute
        
            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncExecute()
            
        Case ADOR.ExecuteOptionEnum.adAsyncFetch
        
            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetch()
            
        Case ADOR.ExecuteOptionEnum.adAsyncFetchNonBlocking
        
            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetchNonBlocking()
            
        Case ADOR.ExecuteOptionEnum.adExecuteNoRecords
        
            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteNoRecords()
            
        Case ADOR.ExecuteOptionEnum.adExecuteRecord
        
            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteRecord()
            
        Case ADOR.ExecuteOptionEnum.adExecuteStream
        
            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteStream()
            
        Case ADOR.ExecuteOptionEnum.adOptionUnspecified
        
            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumUnspecified()
    End Select

    GetADOEnumDescriptionJPNFromExecuteOption = strDescription
End Function


Public Function GetADOEnumDescriptionJPNFromRecordsetCursorType(ByVal venmCursorTypeEnum As ADOR.CursorTypeEnum) As String
    
    Dim strDescription As String
    
    Select Case venmCursorTypeEnum
    
        Case ADOR.CursorTypeEnum.adOpenForwardOnly
        
            strDescription = GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenForwardOnly()
            
        Case ADOR.CursorTypeEnum.adOpenKeyset
            
            strDescription = GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenKeyset()
            
        Case ADOR.CursorTypeEnum.adOpenDynamic
            
            strDescription = GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenDynamic()
            
        Case ADOR.CursorTypeEnum.adOpenStatic
            
            strDescription = GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenStatic()
            
        Case ADOR.CursorTypeEnum.adOpenUnspecified
            
            strDescription = GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenUnspecified()
    End Select
    
    GetADOEnumDescriptionJPNFromRecordsetCursorType = strDescription
End Function


Public Function GetADOEnumDescriptionJPNFromRecordsetLockType(ByVal venmLockTypeEnum As ADOR.LockTypeEnum) As String
    
    Dim strDescription As String

    Select Case venmLockTypeEnum
    
        Case ADOR.LockTypeEnum.adLockReadOnly
        
            strDescription = GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockReadOnly()
            
        Case ADOR.LockTypeEnum.adLockPessimistic
            
            strDescription = GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockPessimistic()
            
        Case ADOR.LockTypeEnum.adLockOptimistic
            
            strDescription = GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockOptimistic()
            
        Case ADOR.LockTypeEnum.adLockBatchOptimistic
            
            strDescription = GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockBatchOptimistic()
            
        Case ADOR.LockTypeEnum.adLockUnspecified
            
            strDescription = GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockUnspecified()
    End Select

    GetADOEnumDescriptionJPNFromRecordsetLockType = strDescription
End Function


'**---------------------------------------------
'** get flag literals for each enumeration
'**---------------------------------------------

Public Function GetADOEnumAllLiteralsFromExecuteOption(ByVal venmExecutionOptionEnum As ADOR.ExecuteOptionEnum) As String

    Dim varFlag As Variant
    Dim enmFlag As ADOR.ExecuteOptionEnum
    Dim i As Long

    Dim strLiterals As String, objCol As Collection

    Set objCol = GetFilteredEnumForExecuteOptionFlag(venmExecutionOptionEnum)
    
    i = 1
    
    strLiterals = ""
    
    For Each varFlag In objCol
    
        enmFlag = varFlag
    
        strLiterals = strLiterals & GetADOEnumLiteralFromExecuteOption(enmFlag)
        
        If i < objCol.Count Then
        
            strLiterals = strLiterals & vbCrLf
        End If
        
        i = i + 1
    Next
    
    GetADOEnumAllLiteralsFromExecuteOption = strLiterals
End Function


Public Function GetADOEnumAllDescriptionsJPNFromExecuteOption(ByVal venmExecutionOptionEnum As ADOR.ExecuteOptionEnum) As String

    Dim varFlag As Variant
    Dim enmFlag As ADOR.ExecuteOptionEnum
    Dim i As Long

    Dim strDescriptions As String, objCol As Collection


    Set objCol = GetFilteredEnumForExecuteOptionFlag(venmExecutionOptionEnum)
    
    i = 1
    strDescriptions = ""
    
    For Each varFlag In objCol
    
        enmFlag = varFlag
    
        strDescriptions = strDescriptions & GetADOEnumDescriptionJPNFromExecuteOption(enmFlag)
        
        If i < objCol.Count Then
        
            strDescriptions = strDescriptions & vbCrLf
        End If
        
        i = i + 1
    Next

    GetADOEnumAllDescriptionsJPNFromExecuteOption = strDescriptions
End Function


Public Function GetFilteredEnumForExecuteOptionFlag(ByVal venmExecutionOptionEnum As ADOR.ExecuteOptionEnum) As Collection

    Dim objFilteredCol As Collection
    Dim varFlag As Variant
    Dim enmFlag As ADOR.ExecuteOptionEnum
    
    Set objFilteredCol = New Collection
    
    If venmExecutionOptionEnum = adOptionUnspecified Then
    
        objFilteredCol.Add venmExecutionOptionEnum
    Else
        For Each varFlag In GetAllEnumForExecuteOptionFlag()
        
            enmFlag = varFlag
            
            If (venmExecutionOptionEnum And enmFlag) = enmFlag Then
            
                objFilteredCol.Add enmFlag
            End If
        Next
    End If

    Set GetFilteredEnumForExecuteOptionFlag = objFilteredCol
End Function

Public Function GetAllEnumForExecuteOptionFlag() As Collection
    
    Dim objAllCol As Collection
    
    Set objAllCol = New Collection
    
    With objAllCol
    
        .Add ADOR.ExecuteOptionEnum.adAsyncExecute
        
        .Add ADOR.ExecuteOptionEnum.adAsyncFetch
        
        .Add ADOR.ExecuteOptionEnum.adAsyncFetchNonBlocking
        
        .Add ADOR.ExecuteOptionEnum.adExecuteNoRecords
        
        .Add ADOR.ExecuteOptionEnum.adExecuteRecord
        
        .Add ADOR.ExecuteOptionEnum.adExecuteStream
        
        .Add ADOR.ExecuteOptionEnum.adOptionUnspecified
    End With

    Set GetAllEnumForExecuteOptionFlag = objAllCol
End Function



'**---------------------------------------------
'** Key-Value cache preparation for ADOFailedInformation
'**---------------------------------------------
'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseServer() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseServer = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_CLIENT
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseClient() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseClient = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_CLIENT")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_UNKNOWN
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumUnknown() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumUnknown = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_UNKNOWN")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TEXT
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumText() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumText = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TEXT")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTable() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTable = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE_DIRECT
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTableDirect() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTableDirect = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE_DIRECT")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_FILE
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumFile() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumFile = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_FILE")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_STORED_PROCEDURE
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumStoredProcedure() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumStoredProcedure = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_STORED_PROCEDURE")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_EXECUTE
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncExecute() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncExecute = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_EXECUTE")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetch() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetch = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH_NON_BLOCKING
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetchNonBlocking() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetchNonBlocking = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH_NON_BLOCKING")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_NO_RECORDS
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteNoRecords() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteNoRecords = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_NO_RECORDS")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_RECORD
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteRecord() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteRecord = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_RECORD")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_STREAM
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteStream() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteStream = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_STREAM")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_UNSPECIFIED
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumUnspecified() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumUnspecified = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_UNSPECIFIED")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_FORWARD_ONLY
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenForwardOnly() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenForwardOnly = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_FORWARD_ONLY")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_KEYSET
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenKeyset() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenKeyset = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_KEYSET")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_DYNAMIC
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenDynamic() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenDynamic = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_DYNAMIC")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_STATIC
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenStatic() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenStatic = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_STATIC")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_UNSPECIFIED
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenUnspecified() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenUnspecified = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_UNSPECIFIED")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_READ_ONLY
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockReadOnly() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockReadOnly = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_READ_ONLY")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_PESSIMISTIC
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockPessimistic() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockPessimistic = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_PESSIMISTIC")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_OPTIMISTIC
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockOptimistic() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockOptimistic = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_OPTIMISTIC")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_BATCH_OPTIMISTIC
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockBatchOptimistic() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockBatchOptimistic = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_BATCH_OPTIMISTIC")
End Function

'''
''' get string Value from STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_UNSPECIFIED
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Function GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockUnspecified() As String

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        msubInitializeTextForADOFailedInformation
    End If

    GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockUnspecified = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_UNSPECIFIED")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Sub msubInitializeTextForADOFailedInformation()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForADOFailedInformationByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForADOFailedInformationByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueADOFailedInformationDicInitialized = True
    End If

    Set mobjStrKeyValueADOFailedInformationDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for ADOFailedInformation key-values cache
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Sub AddStringKeyValueForADOFailedInformationByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER", "Default. Uses cursors supplied by the data provider or driver. These cursors are sometimes very flexible and allow for additional sensitivity to changes others make to the data source. However, some features of the The Microsoft Cursor Service for OLE DB, such as disassociated"
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_CLIENT", "Uses client-side cursors supplied by a local cursor library. Local cursor services often will allow many features that driver-supplied cursors may not, so using this setting may provide an advantage with respect to features that will be enabled. For backward compatibility, the synonym adUseClientBatch is also supported."
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_UNKNOWN", "Default. Indicates that the type of command in the CommandText property is not known. When the type of command is not known, ADO will make several attempts to interpret the CommandText"
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TEXT", "Evaluates CommandText as a textual definition of a command or stored procedure call."
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE", "Evaluates CommandText as a table name whose columns are all returned by an internally generated SQL query."
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE_DIRECT", "Evaluates CommandText as a table name whose columns are all returned. Used with Recordset.Open or Requery only. To use the Seek method, the Recordset must be opened with adCmdTableDirect."
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_FILE", "Evaluates CommandText as the file name of a persistently stored Recordset. Used with Recordset.Open or Requery only."
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_STORED_PROCEDURE", "Evaluates CommandText as a stored procedure name."
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_EXECUTE", "Indicates that the command should execute asynchronously. This value cannot be combined with the CommandTypeEnum value adCmdTableDirect."
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH", "Indicates that the remaining rows after the initial quantity specified in the CacheSize property should be retrieved asynchronously."
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH_NON_BLOCKING", "Indicates that the main thread never blocks while retrieving. If the requested row has not been retrieved, the current row automatically moves to the end of the file."
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_NO_RECORDS", "Indicates that the command text is a command or stored procedure that does not return rows (for example, a command that only inserts data). If any rows are retrieved, they are discarded and not returned."
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_RECORD", "Indicates that the CommandText is a command or stored procedure that returns a single row which should be returned as a Record object."
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_STREAM", "Indicates that the results of a command execution should be returned as a stream. adExecuteStream can only be passed as an optional parameter to the Command Execute method."
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_UNSPECIFIED", "Indicates that the command is unspecified."
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_FORWARD_ONLY", "Default. Uses a forward-only cursor. Identical to a static cursor, except that you can only scroll forward through records. This improves performance when you need to make only one pass through a Recordset."
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_KEYSET", "Uses a keyset cursor. Like a dynamic cursor, except that you can't see records that other users add, although records that other users delete are inaccessible from your Recordset. Data changes by other users are still visible."
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_DYNAMIC", "Uses a dynamic cursor. Additions, changes, and deletions by other users are visible, and all types of movement through the Recordset are allowed, except for bookmarks, if the provider doesn't support them."
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_STATIC", "Uses a static cursor, which is a static copy of a set of records that you can use to find data or generate reports. Additions, changes, or deletions by other users are not visible."
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_UNSPECIFIED", "Does not specify the type of cursor."
        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_READ_ONLY", "Indicates read-only records. You cannot alter the data."
        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_PESSIMISTIC", "Indicates pessimistic locking, record by record. The provider does what is necessary to ensure successful editing of the records, usually by locking records at the data source immediately after editing."
        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_OPTIMISTIC", "Indicates optimistic locking, record by record. The provider uses optimistic locking, locking records only when you call the Update method."
        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_BATCH_OPTIMISTIC", "Indicates optimistic batch updates. Required for batch update mode."
        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_UNSPECIFIED", "Does not specify a type of lock. For clones, the clone is created with the same lock type as the original."
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for ADOFailedInformation key-values cache
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Sub AddStringKeyValueForADOFailedInformationByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER", "既定値 - サーバのカーソルを使用"
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_CLIENT", "ADOはCursor Serviceを起動し、取得したデータをクライアント側にコピー、また､そのデータによってRecordCountプロパティにレコード数が設定されます。"
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_UNKNOWN", "既定値. CommandTextプロパティの中のコマンドの種類が不明"
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TEXT", "CommandTextをテキストによるコマンドの定義, SQL文やストアドプロシージャとして評価"
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE", "CommandTextをテーブル名として評価"
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE_DIRECT", "すべての列が返されたテーブル名としてCommandTextを評価します。 レコードセットで使用されます。 OpenまたはRequeryのみ。 Seekメソッドを使用するには、レコードセットをadCmdTableDirectで開く必要があります。"
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_FILE", "永続的に格納されたレコードセットのファイル名としてCommandTextを評価します。 レコードセットで使用されます。OpenまたはRequeryのみ。"
        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_STORED_PROCEDURE", "CommandTextをストアド プロシージャ名として評価"
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_EXECUTE", "コマンドを非同期的に実行する。 CommandTypeEnum値adCmdTableDirectと組み合わせることはできません。"
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH", "CacheSizeプロパティで指定された初期数量の後の残りの行を非同期に取得する"
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH_NON_BLOCKING", "メインスレッドが取得中にブロックされないことを示します。 要求された行が取得されていない場合は、現在の行が自動的にファイルの末尾に移動"
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_NO_RECORDS", "adExecuteNoRecordsは、コマンドまたは接続の Executeメソッドにオプションのパラメーターとしてのみ渡す。コマンドテキストが、行を返さないコマンドまたはストアドプロシージャ (たとえば、データを挿入するコマンド) であることを示します。 取得された行は破棄され、返されません。"
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_RECORD", "CommandTextが、レコードオブジェクトとして返される1つの行を返すコマンドまたはストアドプロシージャであることを示します。"
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_STREAM", "adExecuteStreamはコマンドの Executeメソッドにオプションのパラメーターとしてのみ渡すことができます。コマンドの実行結果をストリームとして返します"
        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_UNSPECIFIED", "コマンドの起動の指定オプション無し"
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_FORWARD_ONLY", "既定値. 順方向専用カーソルを使用します。 レコードをスクロールするだけでスクロールできる点を除いて、静的カーソルと同じです。 これにより、レコードセットのパススルーを1回だけ行う必要がある場合にパフォーマンスが向上します。"
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_KEYSET", "キーセットカーソルを使用します。 動的カーソルと同様に、他のユーザーが追加したレコードは表示されませんが、他のユーザーが削除したレコードにはレコードセットからアクセスできません。 他のユーザーによるデータの変更は引き続き表示されます。"
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_DYNAMIC", "動的カーソルを使用します。 他のユーザーによる追加、変更、および削除が表示され、プロバイダーがサポートしていない場合は、ブックマークを除くすべての種類の移動が許可されます。"
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_STATIC", "静的カーソルを使用します。これは、データの検索やレポートの生成に使用できる一連のレコードの静的なコピーです。 他のユーザーによる追加、変更、または削除は表示されません。"
        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_UNSPECIFIED", "カーソルの種類を指定しません。"
        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_READ_ONLY", "読み取り専用レコードを示します。 データを変更することはできません。"
        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_PESSIMISTIC", "レコード単位の排他的ロック。プロバイダーは、レコードを正常に編集するために必要な処理を実行。通常は、編集するとすぐデータソースのレコードをロック。"
        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_OPTIMISTIC", "レコード単位の共有的ロック。プロバイダーは共有的ロックを使用し、 Updateメソッドを呼び出したときにのみレコードをロックします。"
        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_BATCH_OPTIMISTIC", "共有的バッチ更新を示します。 バッチ更新モードの場合のみ指定できます。"
        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_UNSPECIFIED", "ロックの種類を指定していません。 複製の場合、複製は元と同じロックの種類で作成されます。"
    End With
End Sub

'''
''' Remove Keys for ADOFailedInformation key-values cache
'''
''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueADOFailedInformation(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER"
            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_CLIENT"
            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_UNKNOWN"
            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TEXT"
            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE"
            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE_DIRECT"
            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_FILE"
            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_STORED_PROCEDURE"
            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_EXECUTE"
            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH"
            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH_NON_BLOCKING"
            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_NO_RECORDS"
            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_RECORD"
            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_STREAM"
            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_UNSPECIFIED"
            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_FORWARD_ONLY"
            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_KEYSET"
            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_DYNAMIC"
            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_STATIC"
            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_UNSPECIFIED"
            .Remove "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_READ_ONLY"
            .Remove "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_PESSIMISTIC"
            .Remove "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_OPTIMISTIC"
            .Remove "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_BATCH_OPTIMISTIC"
            .Remove "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_UNSPECIFIED"
        End If
    End With
End Sub



