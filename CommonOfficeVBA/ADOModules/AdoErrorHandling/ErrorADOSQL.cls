VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ErrorADOSQL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO SQL error data-class
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'       Dependent on ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  8/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** an additional information
'**---------------------------------------------
Private mudtLoggedDate As Date

Private mobjLoadedConnectionSetting As LoadedADOConnectionSetting

Private mobjSQLResult As SQLResult

Private mblnIsRecordsetExists As Boolean

'**---------------------------------------------
'** an error contents
'**---------------------------------------------
Private mintErrorNumber As Long

Private mstrErrorDescription As String

Private mstrErrorSource As String


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mblnIsRecordsetExists = False

    mstrErrorSource = ""
End Sub

Private Sub Class_Terminate()

    Set mobjLoadedConnectionSetting = Nothing
    
    Set mobjSQLResult = Nothing
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** an error contents
'**---------------------------------------------
'''
''' read-only error number
'''
Public Property Get ErrorNumber() As Long

    ErrorNumber = mintErrorNumber
End Property

'''
''' read-only error description
'''
Public Property Get ErrorDescription() As String

    ErrorDescription = mstrErrorDescription
End Property

'''
''' read-only error source
'''
Public Property Get ErrorSource() As String

    ErrorSource = mstrErrorSource
End Property

'**---------------------------------------------
'** an additional information
'**---------------------------------------------
'''
''' read-only SQL result
'''
Public Property Get ADOSQLResult() As SQLResult

    Set ADOSQLResult = mobjSQLResult
End Property

'''
''' read-only, this is false when the Recordset is nothing
'''
Public Property Get IsRecordsetExists() As Boolean

    IsRecordsetExists = mobjLoadedConnectionSetting.IsRecordsetExists
End Property

'''
''' read-only Loaded-ConnectionSetting object
'''
Public Property Get LoadedConnectionSetting() As LoadedADOConnectionSetting

    Set LoadedConnectionSetting = mobjLoadedConnectionSetting
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub SetLog(ByVal vobjErr As Object, _
        ByVal vobjSQLResult As SQLResult, _
        ByVal vobjConnection As ADODB.Connection, _
        ByVal vobjOpeningConnectionSetting As ADOConnectionSetting, _
        Optional ByVal vobjRecordset As ADODB.Recordset = Nothing)
    
    
    msubSetErrorAndResult vobjErr, vobjSQLResult
    
    Set mobjLoadedConnectionSetting = New LoadedADOConnectionSetting
    
    mobjLoadedConnectionSetting.SetLoadedParameters vobjConnection, vobjOpeningConnectionSetting, vobjRecordset
End Sub


Public Sub SetLogWithLoadedConnectSetting(ByVal vobjErr As Object, _
        ByVal vobjSQLResult As SQLResult, _
        ByVal vobjLoadedConnectSetting As LoadedADOConnectionSetting)
    
    
    msubSetErrorAndResult vobjErr, vobjSQLResult

    Set mobjLoadedConnectionSetting = vobjLoadedConnectSetting
End Sub


'''
'''
'''
Public Function GetErrorSummary() As String
    
    Dim strMessage As String
    
    strMessage = ""
    
    strMessage = strMessage & "[Number] : " & CStr(Me.ErrorNumber) & vbCrLf

    If Me.ErrorSource <> "" Then
    
        strMessage = strMessage & "[Source] : " & Me.ErrorSource & vbCrLf & vbCrLf
    End If
    
    strMessage = strMessage & Me.ErrorDescription

    GetErrorSummary = strMessage
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSetErrorAndResult(ByVal vobjErr As Object, ByVal vobjSQLResult As SQLResult)
    
    mudtLoggedDate = Now()

    With vobjErr    ' As ADODB.Error...
        
        mintErrorNumber = CLng(.Number)
        
        mstrErrorDescription = .Description
        
        mstrErrorSource = .Source
    End With
    
    Set mobjSQLResult = vobjSQLResult
End Sub


