VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ErrorContentKeeper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   general VBA error data-clas
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'       Independent on ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  8/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mintNumber As Long
Private mstrDescription As String
Private mstrSource As String


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mintNumber = vbObjectError
    
    mstrDescription = ""
    
    mstrSource = ""
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
''' error number
'''
Public Property Get Number() As Long

    Number = mintNumber
End Property
Public Property Let Number(ByVal vintNumber As Long)

    mintNumber = vintNumber
End Property

'''
''' error description
'''
Public Property Get Description() As String

    Description = mstrDescription
End Property
Public Property Let Description(ByVal vstrDescription As String)

    mstrDescription = vstrDescription
End Property

'''
''' error source
'''
Public Property Get Source() As String

    Source = mstrSource
End Property
Public Property Let Source(ByVal vstrSource As String)

    mstrSource = vstrSource
End Property

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////

Public Sub CopyFrom(ByVal vobjErr As Object, Optional vstrAdditionalMetaDataForSource As String = "")
    
    With vobjErr
        
        Me.Number = .Number
        
        Me.Description = .Description
        
        On Error Resume Next
        
        Me.Source = .Source
        
        If vstrAdditionalMetaDataForSource <> "" Then
        
            If Me.Source = "" Then
            
                Me.Source = vstrAdditionalMetaDataForSource
            Else
                Me.Source = vstrAdditionalMetaDataForSource & ", " & Me.Source
            End If
        End If
    End With
End Sub
