Attribute VB_Name = "ADOConStrToolsForExcelBookBase"
'
'   Generate ADODB connection string to a Excel book file
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Despite this is dependent on ADO, it is a common codes for localizing various captions
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  3/Aug/2023    Kalmclaeyd Tarclanus    Separated from ADOConStrToolsForExcelBook.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_EXCEL_REF = False     ' This code-module supports to open the password locked Excel book file (.xlsx, xlsm, xls) in order to connect it by ADO ACE OLE-DB interface


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' ADO parameters for the OLE-DB connection to Microsoft Excel
'''
Public Enum ExcelBookSpecType
    
    XlBookFileType5P0 = 1   ' [Excel 5.0] when connected book is Excel 95
    
    XlBookFileType8P0 = 2   ' [Excel 8.0] when connected book is Excel 97, 2002-2003
    
    XlBookFileType12P0Xml = 3   ' [Excel 12.0 Xml] when connected book is Excel 2007 or lator and Xml type (.xlsx)
    
    XlBookFileType12P0B = 4     ' [Excel 12.0] when connected book is Excel 2007 or lator and binary type (.xlsb)
    
    XlBookFileType12P0Macro = 5 ' [Excel 12.0 Macro] when connected book is Excel 2007 or lator and Macro book type (.xlsm)
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** ADODB connection string generation
'**---------------------------------------------
'''
'''
'''
Public Function GetADODBConnectionOleDbStringToXlBook(ByVal vstrFilePath As String, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal venmExcelBookSpecType As ExcelBookSpecType = ExcelBookSpecType.XlBookFileType12P0Macro, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal vblnReadOnly As Boolean = False, _
        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode) As String
    
    
    Dim strConnectionString As String, strProvider As String, strXlProperties As String
    
    
    strProvider = GetAccessOLEDBProviderNameFromEnm(venmAccessOLEDBProviderType)
    
    strXlProperties = """" & GetExcelBookSpecTypeString(venmExcelBookSpecType) & ";HDR="
    
    If vblnHeaderFieldTitleExists Then
    
        strXlProperties = strXlProperties & "Yes"
    Else
        strXlProperties = strXlProperties & "No"
    End If
    
    Select Case venmIMEXMode
    
        Case IMEXMode.IMEXExport
            
            strXlProperties = strXlProperties & ";IMEX=" & CStr(IMEXMode.IMEXExport)
        Case IMEXMode.IMEXImport
            
            strXlProperties = strXlProperties & ";IMEX=" & CStr(IMEXMode.IMEXImport)
        Case IMEXMode.IMEXLink
            
            strXlProperties = strXlProperties & ";IMEX=" & CStr(IMEXMode.IMEXLink)
    End Select
    
    strXlProperties = strXlProperties & ";ReadOnly=" & CStr(vblnReadOnly)
    
    strXlProperties = strXlProperties & ";"""
    
    
    strConnectionString = "Provider=" & strProvider & ";" & _
                    "Data Source=" & vstrFilePath & ";Extended Properties=" & strXlProperties
    
    GetADODBConnectionOleDbStringToXlBook = strConnectionString
End Function



'''
'''
'''
Public Function GetExcelBookSpecTypeString(ByVal venmExcelBookSpecType As ExcelBookSpecType) As String
    
    Dim strConnectionString As String
    
    Select Case venmExcelBookSpecType
        
        Case ExcelBookSpecType.XlBookFileType5P0
            
            strConnectionString = "Excel 5.0"         ' Excel 95
        
        Case ExcelBookSpecType.XlBookFileType8P0
            
            strConnectionString = "Excel 8.0"         ' Excel 97, 2002-2003
        
        Case ExcelBookSpecType.XlBookFileType12P0Xml
            
            strConnectionString = "Excel 12.0 Xml"    ' Excel 2007 or lator and Xml type (.xlsx)
        
        Case ExcelBookSpecType.XlBookFileType12P0B
            
            strConnectionString = "Excel 12.0"        ' Excel 2007 or lator and binary type (.xlsb)
        
        Case ExcelBookSpecType.XlBookFileType12P0Macro
            
            strConnectionString = "Excel 12.0 Macro"  ' Excel 2007 or lator and Macro book type (.xlsm)
    End Select
    
    GetExcelBookSpecTypeString = strConnectionString
End Function


'**---------------------------------------------
'** Support to connect the password locked Excel books
'**---------------------------------------------
'''
''' The password locked Excel file ADO connection is to be failed before it is not opened.
''' After it is opened, then ADO connection is to be passed.
'''
Public Function OpenPasswordLockedWorkbookByInterface(ByRef rstrBookPath As String, ByRef rstrPassword As String) As Object

#If HAS_EXCEL_REF Then

    Dim objBook As Excel.Workbook, objApplication As Excel.Application
#Else
    Dim objBook As Object, objApplication As Object
#End If

    Set objBook = Nothing

    Set objApplication = GetExcelApplicationIfItIsInstalled()

    If Not objApplication Is Nothing Then

        Set objBook = GetOpenedBook(rstrBookPath, objApplication)
    
        If objBook Is Nothing Then
    
            On Error Resume Next
            
            Set objBook = objApplication.Workbooks.Open(rstrBookPath, Password:=rstrPassword)
            
            On Error GoTo 0
        End If
    End If

    Set OpenPasswordLockedWorkbookByInterface = objBook
End Function



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function GetOpenedBook(ByRef rstrBookPath As String, ByRef robjApplication As Object) As Object

#If HAS_EXCEL_REF Then

    Dim objFS As Scripting.FileSystemObject, objBook As Excel.Workbook, objFoundBook As Excel.Workbook
#Else
    Dim objFS As Scripting.FileSystemObject, objBook As Object, objFoundBook As Object
#End If
    Dim blnIsFound As Boolean

    blnIsFound = False: Set objFoundBook = Nothing

    Set objFS = CreateObject("Scripting.FileSystemObject")

    With objFS
    
        For Each objBook In robjApplication.Workbooks
        
            If StrComp(.GetFileName(rstrBookPath), .GetFileName(objBook.Name)) = 0 Then
            
                Set objFoundBook = objBook
            
                blnIsFound = True
            End If
        Next
    End With

    Set GetOpenedBook = objFoundBook
End Function


'''
''' The following can be called from either Word macro document or PowerPoint macro document
'''
Private Function GetExcelApplicationIfItIsInstalled() As Object

#If HAS_EXCEL_REF Then

    Dim objApplication As Excel.Application
#Else
    Dim objApplication As Object
#End If

    Set objApplication = Nothing

    On Error Resume Next
    
    Set objApplication = GetObject(, "Excel.Application")   ' If a Excel process exists, the Excel.Application object is got as the existed Excel

    If objApplication Is Nothing Then
    
        Set objApplication = CreateObject("Excel.Application")
    End If

    Set GetExcelApplicationIfItIsInstalled = objApplication
End Function


