VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOConStrOfAceOleDbCsv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO connection string generator for ADO-DB connection with CSV files
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
'       which should have been the installed when the Microsoft Office Excel software has been installed.
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 10/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IADOConnectStrGenerator

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjCoreGenerator As ADOConStrOfAceOleDb


Private menmAccessOLEDBProviderType As AccessOLEDBProviderType

Private mstrDirectoryPath As String

Private mblnHeaderFieldTitleExists As Boolean

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjCoreGenerator = New ADOConStrOfAceOleDb
End Sub


'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
Private Function IADOConnectStrGenerator_GetConnectionString() As String

    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForCSVFile
End Function
'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'**---------------------------------------------
'** CSV directory connection by ACCESS OLE-DB provider
'**---------------------------------------------
Public Property Get ConnectingFilePath() As String

    ConnectingFilePath = mobjCoreGenerator.ConnectingFilePath
End Property

'''
''' read-only
'''
Public Property Get ConnectingDirectoryPath() As String

    ConnectingDirectoryPath = mstrDirectoryPath
End Property

Public Property Get HeaderFieldTitleExists() As Boolean

    HeaderFieldTitleExists = mblnHeaderFieldTitleExists
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetConnectionString() As String

    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
End Function

'''
''' CSV files connection
'''
Public Function GetConnectionStringForCSVFile() As String

    GetConnectionStringForCSVFile = GetADODBConnectionOleDbStringForCSVFile(mstrDirectoryPath, menmAccessOLEDBProviderType, mblnHeaderFieldTitleExists)
End Function

'''
''' connect to CSV directory from a CSV file
'''
Public Sub SetCsvFileConnection(ByVal vstrCSVFilePath As String, _
        Optional vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)

    mobjCoreGenerator.ConnectingFilePath = vstrCSVFilePath

    With New Scripting.FileSystemObject
    
        SetCsvDirectoryConnection .GetParentFolderName(vstrCSVFilePath), vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
    End With
End Sub

'''
''' connect to CSV directory
'''
Public Sub SetCsvDirectoryConnection(ByVal vstrCsvDirectoryPath As String, _
        Optional vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)

    mstrDirectoryPath = vstrCsvDirectoryPath
    
    mblnHeaderFieldTitleExists = vblnHeaderFieldTitleExists

    menmAccessOLEDBProviderType = venmAccessOLEDBProviderType
    
    mobjCoreGenerator.OleDbProviderName = GetAccessOLEDBProviderDescriptionFromEnm(menmAccessOLEDBProviderType)
End Sub

