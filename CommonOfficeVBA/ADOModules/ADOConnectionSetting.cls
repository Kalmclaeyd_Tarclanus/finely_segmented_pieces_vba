VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOConnectionSetting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO connection setting parameters data class
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  1/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mintCommandTimeout As Long  ' unit is second

Private menmCommandTypeEnum As ADODB.CommandTypeEnum

Private menmCursorLocationEnum As ADODB.CursorLocationEnum

Private menmCursorTypeEnum As ADOR.CursorTypeEnum   ' RecordSet open parameter

Private menmLockTypeEnum As ADOR.LockTypeEnum   ' RecordSet open parameter

Private menmExecuteOptionEnum As ADOR.ExecuteOptionEnum ' used when Connection.Execute is selected

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mintCommandTimeout = 30 ' ADODB.Connection default value is 30.

    menmCommandTypeEnum = CommandTypeEnum.adCmdUnknown  ' ADO default value
    
    menmCursorLocationEnum = ADODB.CursorLocationEnum.adUseServer    ' ADO default value
    
    ' RecordSet open parameter
    menmCursorTypeEnum = ADOR.CursorTypeEnum.adOpenUnspecified ' ADO default value
    
    ' RecordSet open parameter
    menmLockTypeEnum = ADOR.LockTypeEnum.adLockUnspecified ' ADO default value
    
    ' Asynchronous SQL execution option
    menmExecuteOptionEnum = ADOR.ExecuteOptionEnum.adOptionUnspecified  ' ADO default value
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////

Public Property Get CommandTimeout() As Long

    CommandTimeout = mintCommandTimeout
End Property
Public Property Let CommandTimeout(ByVal vintCommandTimeout As Long)
    ' time unit is second, [s]
    
    mintCommandTimeout = vintCommandTimeout
End Property


Public Property Get CommandType() As ADODB.CommandTypeEnum

    CommandType = menmCommandTypeEnum
End Property
Public Property Let CommandType(ByVal venmCommandTypeEnum As ADODB.CommandTypeEnum)

    ' ADODB.CommandTypeEnum.adCmdUnknown : 8 - Default. Indicates that the type of command in the CommandText property is not known. When the type of command is not known, ADO will make several attempts to interpret the CommandText
    ' ADODB.CommandTypeEnum.adCmdText : 1 - Evaluates CommandText as a textual definition of a command or stored procedure call.
    ' ADODB.CommandTypeEnum.adCmdTable : 2 - Evaluates CommandText as a table name whose columns are all returned by an internally generated SQL query.
    ' ADODB.CommandTypeEnum.adCmdStoredProc : 4 - Evaluates CommandText as a stored procedure name.
    ' ADODB.CommandTypeEnum.adCmdFile : 256 - Evaluates CommandText as the file name of a persistently stored Recordset. Used with Recordset.Open or Requery only.
    ' ADODB.CommandTypeEnum.adCmdTableDirect : 512 - Evaluates CommandText as a table name whose columns are all returned. Used with Recordset.Open or Requery only. To use the Seek method, the Recordset must be opened with adCmdTableDirect.
    ' ADODB.CommandTypeEnum.adCmdUnspecified : -1 - The command is unspecified.
    
    menmCommandTypeEnum = venmCommandTypeEnum
End Property

Public Property Get CursorLocation() As ADODB.CursorLocationEnum

    CursorLocation = menmCursorLocationEnum
End Property
Public Property Let CursorLocation(ByVal venmCursorLocationEnum As ADODB.CursorLocationEnum)

    ' ADODB.CursorLocationEnum.adUseServer : 2  - Default. Uses cursors supplied by the data provider or driver. These cursors are sometimes very flexible and allow for additional sensitivity to changes others make to the data source. However, some features of the The Microsoft Cursor Service for OLE DB, such as disassociated
    ' ADODB.CursorLocationEnum.adUseClient : 3  => Uses client-side cursors supplied by a local cursor library. Local cursor services often will allow many features that driver-supplied cursors may not, so using this setting may provide an advantage with respect to features that will be enabled. For backward compatibility, the synonym adUseClientBatch is also supported.

    menmCursorLocationEnum = venmCursorLocationEnum
End Property

'''
''' specify the type of cursor on RecordSet object
'''
Public Property Get CursorType() As ADOR.CursorTypeEnum

    CursorType = menmCursorTypeEnum
End Property
Public Property Let CursorType(ByVal venmCursorTypeEnum As ADOR.CursorTypeEnum)
    
    ' ADOR.CursorTypeEnum.adOpenForwardOnly : 0 - Default. Uses a forward-only cursor. Identical to a static cursor, except that you can only scroll forward through records. This improves performance when you need to make only one pass through a Recordset.
    ' ADOR.CursorTypeEnum.adOpenDynamic : 2 - Uses a dynamic cursor. Additions, changes, and deletions by other users are visible, and all types of movement through the Recordset are allowed, except for bookmarks, if the provider doesn't support them.
    ' ADOR.CursorTypeEnum.adOpenKeyset : 1 - Uses a keyset cursor. Like a dynamic cursor, except that you can't see records that other users add, although records that other users delete are inaccessible from your Recordset. Data changes by other users are still visible.
    ' ADOR.CursorTypeEnum.adOpenStatic : 3 - Uses a static cursor, which is a static copy of a set of records that you can use to find data or generate reports. Additions, changes, or deletions by other users are not visible.
    ' ADOR.CursorTypeEnum.adOptionUnspecified : -1 - Does not specify the type of cursor.
    
    menmCursorTypeEnum = venmCursorTypeEnum
End Property

'''
''' Specifies the type of lock placed on records during editing.
'''
Public Property Get LockType() As ADOR.LockTypeEnum

    LockType = menmLockTypeEnum
End Property
Public Property Let LockType(ByVal venmLockTypeEnum As ADOR.LockTypeEnum)
    
    ' ADOR.LockTypeEnum.adLockReadOnly : 1 -  Does not specify the type of cursor.
    ' ADOR.LockTypeEnum.adLockPessimistic : 2 - Indicates pessimistic locking, record by record. The provider does what is necessary to ensure successful editing of the records, usually by locking records at the data source immediately after editing.
    ' ADOR.LockTypeEnum.adLockOptimistic : 3 - Indicates optimistic locking, record by record. The provider uses optimistic locking, locking records only when you call the Update method.
    ' ADOR.LockTypeEnum.adLockBatchOptimistic : 4 - Indicates optimistic batch updates. Required for batch update mode.
    ' ADOR.LockTypeEnum.adLockUnspecified : -1 - Does not specify a type of lock. For clones, the clone is created with the same lock type as the original.
    
    menmLockTypeEnum = venmLockTypeEnum
End Property

'''
''' This is flag specification, thus you can specify more than one option
'''
Public Property Get ExecuteOption() As ADOR.ExecuteOptionEnum

    ExecuteOption = menmExecuteOptionEnum
End Property
Public Property Let ExecuteOption(ByVal venmExecuteOptionEnum As ADOR.ExecuteOptionEnum)

    ' ADOR.ExecuteOptionEnum.adOptionUnspecified : -1 - Indicates that the command is unspecified.
    ' ADOR.ExecuteOptionEnum.adAsyncExecute : 0x10 - Indicates that the command should execute asynchronously. This value cannot be combined with the CommandTypeEnum value adCmdTableDirect.
    ' ADOR.ExecuteOptionEnum.adAsyncFetch : 0x20 - Indicates that the remaining rows after the initial quantity specified in the CacheSize property should be retrieved asynchronously.
    ' ADOR.ExecuteOptionEnum.adAsyncFetchNonBlocking : 0x40 - Indicates that the main thread never blocks while retrieving. If the requested row has not been retrieved, the current row automatically moves to the end of the file.
    
    ' ADOR.ExecuteOptionEnum.adExecuteNoRecords : 0x80 - Indicates that the command text is a command or stored procedure that does not return rows (for example, a command that only inserts data). If any rows are retrieved, they are discarded and not returned.
    ' ADOR.ExecuteOptionEnum.adExecuteStream : 0x400 - Indicates that the results of a command execution should be returned as a stream. adExecuteStream can only be passed as an optional parameter to the Command Execute method.
    ' ADOR.ExecuteOptionEnum.adExecuteRecord : 0x800 - Indicates that the CommandText is a command or stored procedure that returns a single row which should be returned as a Record object.

    menmExecuteOptionEnum = venmExecuteOptionEnum
End Property

