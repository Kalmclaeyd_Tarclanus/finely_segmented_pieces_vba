VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOConnector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADODB.Connection class wrapper
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       This class focuses on the visualization of both the ADO error-trapping and SQL execution error-trapping
'       LozalizationADOConnector doesn't support the error trapping visualization , logging of SQL results, and localizing captions of itself
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on SQLGeneral.bas
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  8/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IADOConnector

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjConnect As ADODB.Connection

Private mblnIsBeganTransaction As Boolean

Private mobjConnectSetting As ADOConnectionSetting

Private mblnIsConnected As Boolean

Private mblnAllowToRecordSQLLog As Boolean

Private mobjSQLRes As SQLResult ' Logging

Private mobjLoadedConnectSetting As LoadedADOConnectionSetting


Private mblnSuppressToShowUpSqlExecutionError As Boolean

'**---------------------------------------------
'** SQL Error handling state variables
'**---------------------------------------------
Private mintT1 As Long  ' mili-second

Private mintT2 As Long  ' mili-second

Private mblnSQLFailed As Boolean


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mblnIsConnected = False
    
    mblnAllowToRecordSQLLog = True
    
    Set mobjConnectSetting = New ADOConnectionSetting   ' connection starting parameters
    
    Set mobjLoadedConnectSetting = Nothing  ' log-data
    
    mblnIsBeganTransaction = False
    
    
    mblnSQLFailed = False
    
    mblnSuppressToShowUpSqlExecutionError = False
End Sub

Private Sub Class_Terminate()

    IADOConnector_CloseConnection
    
    Set mobjConnect = Nothing

    Set mobjConnectSetting = Nothing
    
    Set mobjLoadedConnectSetting = Nothing
End Sub


'///////////////////////////////////////////////
'/// Implemented properties
'///////////////////////////////////////////////
Private Property Let IADOConnector_IsConnected(ByVal vblnIsConnected As Boolean)

    mblnIsConnected = vblnIsConnected
End Property
Private Property Get IADOConnector_IsConnected() As Boolean

    IADOConnector_IsConnected = mblnIsConnected
End Property

Private Property Set IADOConnector_ADOConnection(ByVal vobjADOConnection As ADODB.Connection)

    Set mobjConnect = vobjADOConnection
End Property
Private Property Get IADOConnector_ADOConnection() As ADODB.Connection

    Set IADOConnector_ADOConnection = mobjConnect
End Property

Private Property Set IADOConnector_SQLExecutionResult(ByVal vobjSQLResult As SQLResult)

    Set mobjSQLRes = vobjSQLResult
End Property
Private Property Get IADOConnector_SQLExecutionResult() As SQLResult

    Set IADOConnector_SQLExecutionResult = mobjSQLRes
End Property

'''
''' unit is second. [s]
'''
Private Property Let IADOConnector_CommandTimeout(ByVal vintTimeout As Long)

    Me.CommandTimeout = vintTimeout
End Property
Private Property Get IADOConnector_CommandTimeout() As Long

    IADOConnector_CommandTimeout = Me.CommandTimeout
End Property


Private Property Get IADOConnector_SuppressToShowUpSqlExecutionError() As Boolean

    IADOConnector_SuppressToShowUpSqlExecutionError = mblnSuppressToShowUpSqlExecutionError
End Property
Private Property Let IADOConnector_SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)

    mblnSuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
End Property

Private Property Get IADOConnector_RdbConnectionInformation() As RdbConnectionInformationFlag

    ' The following is imperfect, the perfect implementation is written in each individual ADO connecting class including this class
    
    IADOConnector_RdbConnectionInformation = RdbConnectionInformationFlag.UseDatabaseMsAdoObjectToConnectRdbFlag
End Property
Private Property Let IADOConnector_RdbConnectionInformation(ByVal venmRdbConnectionInformationFlag As RdbConnectionInformationFlag)

    ' Nothing to do
End Property

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Let IsConnected(ByVal vblnIsConnected As Boolean)

    IADOConnector_IsConnected = vblnIsConnected
End Property
Public Property Get IsConnected() As Boolean

    IsConnected = IADOConnector_IsConnected
End Property

Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecordSQLLog As Boolean)

    mblnAllowToRecordSQLLog = vblnAllowToRecordSQLLog
End Property
Public Property Get AllowToRecordSQLLog() As Boolean

    AllowToRecordSQLLog = mblnAllowToRecordSQLLog
End Property


Public Property Set ADOConnection(ByVal vobjADOConnection As ADODB.Connection)

    Set IADOConnector_ADOConnection = vobjADOConnection
End Property
Public Property Get ADOConnection() As ADODB.Connection

    Set ADOConnection = IADOConnector_ADOConnection
End Property


Public Property Set SQLExecutionResult(ByVal vobjSQLResult As SQLResult)

    Set mobjSQLRes = vobjSQLResult
End Property
Public Property Get SQLExecutionResult() As SQLResult

    Set SQLExecutionResult = mobjSQLRes
End Property

Public Property Get IsSqlExecutionFailed() As Boolean

    IsSqlExecutionFailed = mblnSQLFailed
End Property

'///////////////////////////////////////////////
'/// Properties - connoted ADOConnectionSetting
'///////////////////////////////////////////////
Public Property Get ConnectSetting() As ADOConnectionSetting

    Set ConnectSetting = mobjConnectSetting
End Property

'''
''' unit is second. [s]
'''
Public Property Let CommandTimeout(ByVal vintTimeout As Long)

    mobjConnectSetting.CommandTimeout = vintTimeout
End Property
Public Property Get CommandTimeout() As Long

    CommandTimeout = mobjConnectSetting.CommandTimeout
End Property

'''
''' read-only loaded ConnectSetting()
'''
Public Property Get LoadedConnectSetting() As LoadedADOConnectionSetting

    Set LoadedConnectSetting = mobjLoadedConnectSetting
End Property


'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
'''
''' get recordset object by the SELECT SQL
'''
Private Function IADOConnector_GetRecordset(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset

    Dim objRSet As ADODB.Recordset
    
    
    Set objRSet = Nothing
    
    mblnSQLFailed = False
    
    If mblnIsConnected Then
    
        SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectBeforeExecuteSQL mobjSQLRes, mobjLoadedConnectSetting, mintT1, mblnAllowToRecordSQLLog, vstrSQL
    
        Set objRSet = New ADODB.Recordset
        
        With objRSet
        
            .Source = vstrSQL: .ActiveConnection = mobjConnect
            
            If mobjConnect.State <> ADODB.ObjectStateEnum.adStateOpen Then
            
                Debug.Print "Attention: current ADODB.Conection state - " & GetADOEnumLiteralFromObjectStateEnum(mobjConnect.State)
            End If
            
            On Error Resume Next
            
            .Open
            
            If Err.Number <> 0 Then
            
                msubCreateErrorContentAndHandleItWithShowingErrorOptions Err, mblnSuppressToShowUpSqlExecutionError, venmShowUpAdoErrorOptionFlag, "<ADOConnector.GetRecordset>"
                
                mblnSQLFailed = True: Err.Clear: On Error GoTo 0
            End If
            
            'msubConfirmCurrentConnectionConditionsByDebugPrint mobjConnect, objRSet
            
            On Error GoTo 0
            
        End With
        
        If Not mblnSQLFailed And mblnAllowToRecordSQLLog Then
        
            SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectAfterExecuteSQL mobjSQLRes, mobjLoadedConnectSetting, mintT1, mintT2, mblnAllowToRecordSQLLog, mobjConnect, mobjConnectSetting, objRSet
        End If
    End If

    Set IADOConnector_GetRecordset = objRSet
End Function

'''
''' input SQL, such as INSERT, UPDATE, DELETE
'''
Private Function IADOConnector_ExecuteSQL(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset

    Dim intRecordsAffected As Long, udtCurrent As Date, strCurrentDateTime As String ', objErrorContent As ErrorContentKeeper
    Dim objCommand As ADODB.Command, objRSet As ADODB.Recordset
    
    Set objRSet = Nothing
    
    If mblnIsConnected Then
        
        mblnSQLFailed = False
        
        With mobjConnect
        
            SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectBeforeExecuteSQL mobjSQLRes, mobjLoadedConnectSetting, mintT1, mblnAllowToRecordSQLLog, vstrSQL

            If Not mblnIsBeganTransaction Then
            
                .BeginTrans
                
                mblnIsBeganTransaction = True
            End If
            
            intRecordsAffected = -1
            
            On Error Resume Next
            
            Set objRSet = .Execute(vstrSQL, intRecordsAffected)
            
            If Err.Number <> 0 Then
            
                msubCreateErrorContentAndHandleItWithShowingErrorOptions Err, mblnSuppressToShowUpSqlExecutionError, venmShowUpAdoErrorOptionFlag, "<ADOConnector.ExecuteSQL>"
                
                mblnSQLFailed = True: Err.Clear: On Error GoTo 0
            End If
            
            'msubConfirmCurrentConnectionConditionsByDebugPrint mobjConnect, objRSet
            
            On Error GoTo 0
            
            If Me.AllowToRecordSQLLog Then
            
                SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectAfterExecuteSQL mobjSQLRes, mobjLoadedConnectSetting, mintT1, mintT2, mblnAllowToRecordSQLLog, mobjConnect, mobjConnectSetting, objRSet, intRecordsAffected

                DebugLoggingOfSQLCommand vstrSQL, mobjSQLRes, intRecordsAffected
            End If
        End With
    End If
    
    Set IADOConnector_ExecuteSQL = objRSet
End Function

'''
'''
'''
Private Sub msubConfirmCurrentConnectionConditionsByDebugPrint(ByRef robjConnect As ADODB.Connection, ByRef robjRSet As ADODB.Recordset)

    Dim objCommand As ADODB.Command

    If Not robjConnect Is Nothing Then
    
        With robjConnect
        
            Debug.Print "ADODB.Connection CursorLocation : " & GetADOEnumLiteralFromCursorLocation(.CursorLocation)
        End With
    End If
    
    If Not robjRSet Is Nothing Then
            
        With robjRSet
            
            If .State <> ADODB.ObjectStateEnum.adStateClosed Then
            
                Debug.Print "Recordset CursorType : " & GetADOEnumLiteralFromRecordsetCursorType(.CursorType)
        
                Debug.Print "Recordset LockType : " & GetADOEnumLiteralFromRecordsetLockType(.LockType)
            
                If Not .ActiveCommand Is Nothing Then
                
                    Set objCommand = .ActiveCommand
                    
                    Debug.Print "Recordset CommandType : " & GetADOEnumLiteralFromCommandType(objCommand.CommandType)
                End If
            End If
        End With
    End If
End Sub


'''
''' If the ExecuteSQL method had been used at least one time, this execute ADODB.Connect.CommitTrans()
'''
Private Sub IADOConnector_CommitTransaction()

    If Not mobjConnect Is Nothing Then
        
        If mblnIsConnected Then
            
            If mblnIsBeganTransaction Then
            
                With mobjConnect
                
                    If .State <> ADODB.ObjectStateEnum.adStateClosed Then
                
                        .CommitTrans
                    
                        mblnIsBeganTransaction = False
                    End If
                End With
            End If
        End If
    End If
End Sub

'''
''' If the ExecuteSQL method had been used at least one time, this execute CommitTransaction before execute ADODB.Connect.Close()
'''
Private Sub IADOConnector_CloseConnection()

    If Not mobjConnect Is Nothing Then
        
        If mblnIsConnected Then
            
            On Error Resume Next
            
            IADOConnector_CommitTransaction
            
            On Error GoTo 0
            
            With mobjConnect
            
                If .State <> ADODB.ObjectStateEnum.adStateClosed Then
                
                    On Error Resume Next
                    
                    .Close
                    
                    On Error GoTo 0
                End If
            End With
           
            mblnIsConnected = False
        End If
    End If
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' get recordset object by the Select SQL
'''
Public Function GetRecordset(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
    
    Set GetRecordset = IADOConnector_GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
End Function


'''
''' input SQL, such as INSERT, UPDATE, DELETE
'''
Public Function ExecuteSQL(ByVal vstrSQL As String, _
        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
    
    Set ExecuteSQL = IADOConnector_ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
End Function

'''
''' Close ADO connection
'''
Public Sub CloseConnection()
    
    IADOConnector_CloseConnection
End Sub


'///////////////////////////////////////////////
'/// Internal Functions
'///////////////////////////////////////////////

'''
'''
'''
Private Sub msubCreateErrorContentAndHandleItWithShowingErrorOptions(ByRef robjErr As Object, _
        ByVal vblnSuppressToShowUpSqlExecutionError As Boolean, _
        ByRef renmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag, _
        Optional ByVal vstrAdditionalMetaDataForSource As String = "")
        
    If Not vblnSuppressToShowUpSqlExecutionError Then
    
        If (renmShowUpAdoErrorOptionFlag And SuppressToShowUpAdoSqlErrorFlag) = 0 Then
        
            msubCreateErrorContentAndHandleIt robjErr, renmShowUpAdoErrorOptionFlag, "<ADOConnector.ExecuteSQL>"
        Else
            If (renmShowUpAdoErrorOptionFlag And SuppressToShowUpAdoSqlErrorByDebugPrintFlag) = 0 Then
        
                Debug.Print "ADODB.Connection error: " & robjErr.Description
            End If
        End If
    Else
        If (renmShowUpAdoErrorOptionFlag And SuppressToShowUpAdoSqlErrorByDebugPrintFlag) = 0 Then
        
            Debug.Print "ADODB.Connection error: " & robjErr.Description
        End If
    End If
End Sub

'''
'''
'''
Private Sub msubCreateErrorContentAndHandleIt(ByRef robjErr As Object, _
        ByRef renmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag, _
        Optional ByVal vstrAdditionalMetaDataForSource As String = "")

    Dim objErrorContent As ErrorContentKeeper

    Set objErrorContent = New ErrorContentKeeper
    
    objErrorContent.CopyFrom robjErr, vstrAdditionalMetaDataForSource
    
    msubSQLErrorHandling objErrorContent, renmShowUpAdoErrorOptionFlag, Nothing
End Sub



'''
''' Catch the SQL execution error
'''
Private Sub msubSQLErrorHandling(ByVal vobjErrorContent As ErrorContentKeeper, _
        ByRef renmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag, _
        Optional ByVal vobjRecordset As ADODB.Recordset = Nothing)

    HandleAdoSQlError mobjSQLRes, mintT1, mintT2, mblnAllowToRecordSQLLog, mobjLoadedConnectSetting, mobjConnect, mobjConnectSetting, vobjErrorContent, renmShowUpAdoErrorOptionFlag, vobjRecordset
End Sub



