Attribute VB_Name = "ADOConnectionUtilities"
'
'   ADO connection string generation and connection tests,
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  7/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** general connection tests
'**---------------------------------------------
'''
''' general ADO connection-string test
'''
''' <Argument>vstrConnectionString: Input</Argument>
''' <Return>Boolean</Return>
Public Function IsADOConnectionStringEnabled(ByVal vstrConnectionString As String) As Boolean

    Dim blnRet As Boolean, objConnect As ADODB.Connection, intErrNumber As Long
    
    
    blnRet = False
    
    intErrNumber = 0

    Set objConnect = New ADODB.Connection
    
    On Error GoTo GetErrData
    
    With objConnect
        
        .ConnectionString = vstrConnectionString
        
        .Open
    End With

GetErrData:

    If Err.Number <> 0 Then
        
        Debug.Print "Failed to connect a RDB by ADODB, Err.Number = " & Err.Number & ", Description - " & Err.Description
    End If
    
    intErrNumber = Err.Number

    On Error GoTo Filnally

Filnally:
    If intErrNumber = 0 Then
        
        blnRet = True
        
        objConnect.Close
    End If
    
    Set objConnect = Nothing
    
    IsADOConnectionStringEnabled = blnRet
End Function


'''
'''
'''
''' <Argument>DSN: Data Source Name, this is defined by ODBC (32 bit) administrator</Argument>
''' <Argument>UID: User ID</Argument>
''' <Argument>PWD: Password</Argument>
Public Function DoesODBCDSNSettingExists(ByVal vstrDSN As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String) As Boolean
    
    Dim strConnectionString As String
    
    strConnectionString = "DSN=" & vstrDSN & ";UID=" & vstrUID & ";PWD=" & vstrPWD & ";"
    
    DoesODBCDSNSettingExists = IsADOConnectionStringEnabled(strConnectionString)
End Function

'**---------------------------------------------
'** ADO connection test and passing message
'**---------------------------------------------
'''
'''
'''
Public Sub AdoVariousTypeConnectionTestAndShowMessageBox(ByRef rstrAdoConnectionString As String, _
        ByVal venmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType)


    Dim strTitle As String

    strTitle = GetAdoConnectionTestMessageBoxTitleFromOleDbOrOdbcAdoConnectionDestinationRDBType(venmOleDbOrOdbcAdoConnectionDestinationRDBType)
    
    If IsADOConnectionStringEnabled(rstrAdoConnectionString) Then
    
        MsgBox GetTextOfStrKeyUserFormToolsForAdoConStrMesAdoConnectionSuccess(), vbInformation Or vbOKOnly, strTitle
    Else
        MsgBox GetTextOfStrKeyUserFormToolsForAdoConStrMesAdoConnectionFailed(), vbCritical Or vbOKOnly, strTitle
    End If
End Sub

'''
'''
'''
Public Function GetAdoConnectionTestMessageBoxTitleFromOleDbOrOdbcAdoConnectionDestinationRDBType(ByRef renmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType) As String

    Dim strTitle As String
    
    Select Case renmOleDbOrOdbcAdoConnectionDestinationRDBType
    
        Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndCsv

            strTitle = GetTextOfStrKeyUserFormToolsForAdoConStrTitleCsvAdoOleDbConnectionTest()

        Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsAccessDb
        
            strTitle = GetTextOfStrKeyUserFormToolsForAdoConStrTitleMsAccessDbAdoOleDbConnectionTest()
        
        Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsExcel
        
            strTitle = GetTextOfStrKeyUserFormToolsForAdoConStrTitleMsExcelSheetAdoOleDbConnectionTest()
        
        Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOdbcAndPostgreSQL
        
            strTitle = GetTextOfStrKeyUserFormToolsForAdoConStrTitlePostgresqlAdoOdbcConnectionTest()
        
        Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOdbcAndOracle
        
            strTitle = GetTextOfStrKeyUserFormToolsForAdoConStrTitleOracleAdoOdbcConnectionTest()
        
        Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOdbcAndSQLite

            strTitle = GetTextOfStrKeyUserFormToolsForAdoConStrTitleSqliteAdoOdbcConnectionTest()
    End Select

    GetAdoConnectionTestMessageBoxTitleFromOleDbOrOdbcAdoConnectionDestinationRDBType = strTitle
End Function

'**---------------------------------------------
'** for control USetAdoOdbcConStrForPgSql user-form, USetAdoOdbcConStrForOracle user-form
'**---------------------------------------------
'''
'''
'''
Public Function GetInitialUserInputParametersDicForAdoDSNConnection(Optional ByVal venmOdbcConnectionDestinationRDBType As OdbcConnectionDestinationRDBType = OdbcConnectionDestinationRDBType.OdbcConnectionDestinationToPostgreSQL, _
        Optional ByVal vstrDSN As String = "", _
        Optional ByVal vstrUserName As String = "", _
        Optional ByVal vstrPassword As String = "") As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary

    With objDic
    
        .Add "UseDSN", True
    
        .Add "DSN", vstrDSN
    
        If vstrUserName <> "" Then
    
            .Add "UserName", vstrUserName
        End If
    
        If vstrPassword <> "" Then
        
            .Add mstrTemporaryPdKey, vstrPassword
        End If
        
        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOdbcConnectionDestinationRDBType(venmOdbcConnectionDestinationRDBType)
    End With

    Set GetInitialUserInputParametersDicForAdoDSNConnection = objDic
End Function

'''
'''
'''
Public Sub UpdateADOConStrOfDSNFromInputDic(ByRef robjADOConStrOfDsn As ADOConStrOfDsn, ByRef robjUserInputParametersDic As Scripting.Dictionary)

    With robjUserInputParametersDic
    
        robjADOConStrOfDsn.SetODBCConnectionWithDSN .Item("DSN"), .Item("UserName"), .Item(mstrTemporaryPdKey)
    End With
End Sub


'''
'''
'''
Public Function GetADOConStrOfDSNFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfDsn

    Dim objADOConStrOfDsn As ADOConStrOfDsn
    
    Set objADOConStrOfDsn = New ADOConStrOfDsn
    
    UpdateADOConStrOfDSNFromInputDic objADOConStrOfDsn, robjUserInputParametersDic

    Set GetADOConStrOfDSNFromInputDic = objADOConStrOfDsn
End Function

