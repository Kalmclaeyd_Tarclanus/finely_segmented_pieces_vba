VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LoadedADOConnectionSetting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO SQL SELECT result parameters
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjConnectionSetting As ADOConnectionSetting

Private mblnIsRecordsetExists As Boolean

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjConnectionSetting = New ADOConnectionSetting
    
    mblnIsRecordsetExists = False
End Sub



'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
''' read-only, this is false when the Recordset is nothing
'''
Public Property Get IsRecordsetExists() As Boolean

    IsRecordsetExists = mblnIsRecordsetExists
End Property


'///////////////////////////////////////////////
'/// Properties - connoted ADOConnectionSetting
'///////////////////////////////////////////////
Public Property Get CommandTimeout() As Long

    CommandTimeout = mobjConnectionSetting.CommandTimeout
End Property
Public Property Let CommandTimeout(ByVal vintCommandTimeout As Long)
    ' time unit is second, [s]
    
   mobjConnectionSetting.CommandTimeout = vintCommandTimeout
End Property


Public Property Get CommandType() As ADODB.CommandTypeEnum

    CommandType = mobjConnectionSetting.CommandType
End Property
Public Property Let CommandType(ByVal venmCommandTypeEnum As ADODB.CommandTypeEnum)

    mobjConnectionSetting.CommandType = venmCommandTypeEnum
End Property

Public Property Get CursorLocation() As ADODB.CursorLocationEnum

    CursorLocation = mobjConnectionSetting.CursorLocation
End Property
Public Property Let CursorLocation(ByVal venmCursorLocationEnum As ADODB.CursorLocationEnum)

    mobjConnectionSetting.CursorLocation = venmCursorLocationEnum
End Property

'''
''' specify the type of cursor on RecordSet object
'''
Public Property Get CursorType() As ADOR.CursorTypeEnum

    CursorType = mobjConnectionSetting.CursorType
End Property
Public Property Let CursorType(ByVal venmCursorTypeEnum As ADOR.CursorTypeEnum)

    mobjConnectionSetting.CursorType = venmCursorTypeEnum
End Property

'''
''' Specifies the type of lock placed on records during editing.
'''
Public Property Get LockType() As ADOR.LockTypeEnum

    LockType = mobjConnectionSetting.LockType
End Property
Public Property Let LockType(ByVal venmLockTypeEnum As ADOR.LockTypeEnum)

    mobjConnectionSetting.LockType = venmLockTypeEnum
End Property

'''
''' This is flag specification, thus you can specify more than one option
'''
Public Property Get ExecuteOption() As ADOR.ExecuteOptionEnum

    ExecuteOption = mobjConnectionSetting.ExecuteOption
End Property
Public Property Let ExecuteOption(ByVal venmExecuteOptionEnum As ADOR.ExecuteOptionEnum)

    mobjConnectionSetting.ExecuteOption = venmExecuteOptionEnum
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub SetLoadedParameters(ByVal vobjConnection As ADODB.Connection, ByVal vobjOpeningConnectionSetting As ADOConnectionSetting, Optional vobjRecordset As ADODB.Recordset = Nothing)
    
    Dim objCommand As ADODB.Command
    
    With mobjConnectionSetting
        
        .CommandTimeout = vobjConnection.CommandTimeout
        
        .CursorLocation = vobjConnection.CursorLocation
        
        Set objCommand = Nothing
        
        If Not vobjRecordset Is Nothing Then
        
            If vobjRecordset.State <> ADODB.ObjectStateEnum.adStateClosed Then
        
                Set objCommand = vobjRecordset.ActiveCommand
            
                If Not objCommand Is Nothing Then
                
                    .CommandType = objCommand.CommandType
                End If
            End If
        End If
        
        If objCommand Is Nothing Then
        
            .CommandType = vobjOpeningConnectionSetting.CommandType
        End If
        
        .ExecuteOption = vobjOpeningConnectionSetting.ExecuteOption
        
        If Not vobjRecordset Is Nothing Then
        
            If vobjRecordset.State <> ADODB.ObjectStateEnum.adStateClosed Then
            
                mblnIsRecordsetExists = True
            
                .CursorType = vobjRecordset.CursorType
                
                .LockType = vobjRecordset.LockType
            End If
        Else
            mblnIsRecordsetExists = False
        End If
    End With
End Sub
