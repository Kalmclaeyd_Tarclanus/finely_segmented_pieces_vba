Attribute VB_Name = "ADOConStrToolsForExcelBook"
'
'   Generate ADODB connection string to a Excel book file
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADOConStrOfAceOleDbXl.cls
'       Despite this is dependent on ADO, it is a common codes for localizing various captions
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  7/Feb/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator.cls
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** for control USetAdoOleDbConStrForAccdb user-form
'**---------------------------------------------
'''
'''
'''
Public Function GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsExcelSheet(Optional ByVal vstrBookPath As String = "", _
        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal vblnBookReadOnly As Boolean = False, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vstrBookLockPassword As String = "") As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary

    With objDic
    
        .Add mstrAdoDbFilePathKey, vstrBookPath
    
        .Add "OLE_DB_Provider", GetAccessOLEDBProviderNameFromEnm(venmAccessOLEDBProviderType)
        
        .Add "HeaderColumnTitleExists", vblnHeaderFieldTitleExists
        
        .Add "BookReadOnly", vblnBookReadOnly
        
        If vstrBookLockPassword <> "" Then
    
            .Add mstrTemporaryPdKey, vstrBookLockPassword
        End If
        
        If venmIMEXMode <> NoUseIMEXMode Then
        
            .Add "IMEX", venmIMEXMode
        End If
        
        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOleDbOrOdbcAdoConnectionDestinationRDBType(AdoOleDbAndMsExcel)
    End With

    Set GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsExcelSheet = objDic
End Function


'''
'''
'''
Public Sub UpdateADOConStrOfAccessOleDbToExcelSheetFromInputDic(ByRef robjADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String, enmIMEXMode As IMEXMode, blnHeaderFieldTitleExists As Boolean, blnBookReadOnly As Boolean, enmAccessOLEDBProviderType As AccessOLEDBProviderType
    
    With robjUserInputParametersDic
    
        strKey = "OLE_DB_Provider"
        
        enmAccessOLEDBProviderType = AceOLEDB_12P0
        
        If .Exists(strKey) Then
        
            enmAccessOLEDBProviderType = GetAccessOLEDBProviderEnmFromName(.Item(strKey))
        End If
        
        
        enmIMEXMode = NoUseIMEXMode
        
        strKey = "IMEX"
        
        If .Exists(strKey) Then
        
            enmIMEXMode = GetAdoIMEXModeEnmFromName(.Item(strKey))
        End If
        
        
        blnHeaderFieldTitleExists = True
        
        strKey = "HeaderColumnTitleExists"
        
        If .Exists(strKey) Then
        
            blnHeaderFieldTitleExists = CBool(.Item(strKey))
        End If
        
        blnBookReadOnly = False
        
        strKey = "BookReadOnly"
        
        If .Exists(strKey) Then
        
            blnBookReadOnly = CBool(.Item(strKey))
        End If
        
        robjADOConStrOfAceOledbXl.SetExcelBookConnectionInDetails .Item(mstrAdoDbFilePathKey), enmIMEXMode, blnHeaderFieldTitleExists, blnBookReadOnly, enmAccessOLEDBProviderType
    End With
End Sub


'''
'''
'''
Public Function GetADOConStrOfAccessOleDbToExcelSheetFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfAceOleDbXl

    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
    
    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
    
    UpdateADOConStrOfAccessOleDbToExcelSheetFromInputDic objADOConStrOfAceOledbXl, robjUserInputParametersDic
    
    Set GetADOConStrOfAccessOleDbToExcelSheetFromInputDic = objADOConStrOfAceOledbXl
End Function



'**---------------------------------------------
'** ADO connection string parameters solution interfaces by each special form
'**---------------------------------------------
'''
'''
'''
''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
''' <Argument>robjADOConStrOfAceOledbXl: Output</Argument>
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrBookPath: Input</Argument>
''' <Argument>venmIMEXMode: Input</Argument>
''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
''' <Argument>vblnBookReadOnly: Input</Argument>
''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
''' <Argument>vstrBookLockPassword: Input</Argument>
Public Sub SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
        ByRef robjADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, _
        ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrBookPath As String = "", _
        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal vblnBookReadOnly As Boolean = False, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vstrBookLockPassword As String = "", _
        Optional ByVal vblnAllowToOpenAdoConnectingExcelBookWhenItIsLockedByPassword As Boolean = False)

    Dim objUserInputParametersDic As Scripting.Dictionary, strPassword As String, blnIsOpeningFormNeeded As Boolean
        
    rblnIsRequestedToCancelAdoConnection = False
        
    blnIsOpeningFormNeeded = IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToExcelSheet(objUserInputParametersDic, vstrSettingKeyName, vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmAccessOLEDBProviderType, vstrBookLockPassword)

    If blnIsOpeningFormNeeded Then
    
        msubSetUserInputParametersDicOfAccessOleDbToExcelSheetByOpeningFormWithFixedSettingKeyName rblnIsRequestedToCancelAdoConnection, objUserInputParametersDic, vstrSettingKeyName, vblnAllowToOpenAdoConnectingExcelBookWhenItIsLockedByPassword
    Else
        If vblnAllowToOpenAdoConnectingExcelBookWhenItIsLockedByPassword Then
        
            msubOpenPasswordLockedExcelBookIfItIsNeeded objUserInputParametersDic
        End If
    End If

    If Not rblnIsRequestedToCancelAdoConnection Then
    
        UpdateADOConStrOfAccessOleDbToExcelSheetFromInputDic robjADOConStrOfAceOledbXl, objUserInputParametersDic
    End If
End Sub




'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
''' <Argument>robjUserInputParametersDic: Input - Output</Argument>
''' <Argument>rstrSettingKeyName: Input</Argument>
Private Sub msubSetUserInputParametersDicOfAccessOleDbToExcelSheetByOpeningFormWithFixedSettingKeyName(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        ByRef rstrSettingKeyName As String, _
        Optional ByVal vblnAllowToOpenAdoConnectingExcelBookWhenItIsLockedByPassword As Boolean = False)


    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr, objUSetAdoOleDbConStrForXlBook As USetAdoOleDbConStrForXlBook
   
    Set objFormStateToSetAdoConStr = GetFormStateToSetAdoConStrByDefaultParams(robjUserInputParametersDic, rstrSettingKeyName)
   
    Set objUSetAdoOleDbConStrForXlBook = New USetAdoOleDbConStrForXlBook
    
    With objUSetAdoOleDbConStrForXlBook
    
        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
    
        .Show vbModal
    End With
    
    If vblnAllowToOpenAdoConnectingExcelBookWhenItIsLockedByPassword Then
    
        msubOpenPasswordLockedExcelBookIfItIsNeeded robjUserInputParametersDic
    End If

    AfterProcessAdoConStrFormSetting rblnIsRequestedToCancelAdoConnection, robjUserInputParametersDic, objFormStateToSetAdoConStr
End Sub

'''
'''
'''
Private Sub msubOpenPasswordLockedExcelBookIfItIsNeeded(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim objBook As Object
    
    With robjUserInputParametersDic
    
        If .Exists(mstrTemporaryPdKey) Then

            If .Item(mstrTemporaryPdKey) <> "" Then
            
                Set objBook = GetMsExcelWorkbookByInterface(.Item(mstrAdoDbFilePathKey), False, vstrPassword:=.Item(mstrTemporaryPdKey))
            End If
        End If
    End With
End Sub


'''
'''
'''
''' <Argument>robjUserInputParametersDic: Output</Argument>
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrBookPath: Input</Argument>
''' <Argument>venmIMEXMode: Input</Argument>
''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
''' <Argument>vblnBookReadOnly: Input</Argument>
''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
''' <Argument>vstrBookLockPassword: Input</Argument>
Private Function IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToExcelSheet(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrBookPath As String = "", _
        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal vblnBookReadOnly As Boolean = False, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vstrBookLockPassword As String = "") As Boolean

    Dim blnIsOpeningFormNeeded As Boolean
    
    blnIsOpeningFormNeeded = True

    ' Try to get parameters from Win-registry cache
    
    Set robjUserInputParametersDic = GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(vstrSettingKeyName)

    Select Case True
    
        Case robjUserInputParametersDic Is Nothing, robjUserInputParametersDic.Count = 0
    
            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToExcelSheet(robjUserInputParametersDic, vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmAccessOLEDBProviderType, vstrBookLockPassword)
        Case Else
            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache(robjUserInputParametersDic)
    End Select

    IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToExcelSheet = blnIsOpeningFormNeeded
End Function



'''
'''
'''
''' <Argument>robjUserInputParametersDic: Output</Argument>
''' <Argument>vstrBookPath: Input</Argument>
''' <Argument>venmIMEXMode: Input</Argument>
''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
''' <Argument>vblnBookReadOnly: Input</Argument>
''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
''' <Argument>vstrBookLockPassword: Input</Argument>
Private Function IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToExcelSheet(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        Optional ByVal vstrBookPath As String = "", _
        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal vblnBookReadOnly As Boolean = False, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal vstrBookLockPassword As String = "") As Boolean


    Dim blnIsOpeningFormNeeded As Boolean, objBook As Object
    
    blnIsOpeningFormNeeded = True
    
    Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsExcelSheet(vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmAccessOLEDBProviderType, vstrBookLockPassword)
    
    If vstrBookLockPassword <> "" And vstrBookPath <> "" Then
    
        ' When the Excel book file is locked by a password, the ADO connection is able after the Excel book is opened
    
        Set objBook = GetMsExcelWorkbookByInterface(vstrBookPath, False, vstrPassword:=vstrBookLockPassword)
    End If
    
    If IsADOConnectionStringEnabled(GetADOConStrOfAccessOleDbToExcelSheetFromInputDic(robjUserInputParametersDic).GetConnectionString()) Then
    
        blnIsOpeningFormNeeded = False
    End If

    IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToExcelSheet = blnIsOpeningFormNeeded
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' delete testing SettingKeyName from the registory
'''
Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToAccessOleDbWithExcelSheet()

    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelKey"
    
    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelWithPasswordKey"
End Sub


'''
'''
'''
Private Sub msubSanityTestToOpenUSetAdoOleDbConStrForAccessOleDbWithExcelSheet()

    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
 
    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsExcelSheet()

    msubSetUserInputParametersDicOfAccessOleDbToExcelSheetByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestAccOleDbToExcelKey"

    If Not blnIsRequestedToCancelAdoConnection Then
    
        If IsADOConnectionStringEnabled(GetADOConStrOfAccessOleDbToExcelSheetFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
    
            Debug.Print "Connected to Excel sheet by Ms Access OLE DB using ADO"
        End If
    End If
End Sub
