Attribute VB_Name = "ADOConnectingInClasses"
'
'   ADO connection support-processes for ADOConnector.cls
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Windows OS
'       Dependent on ADOConnector.cls, CurrentUserDomainUtility.bas
'       Dependent on interfaces of IADOConnectStrGenerator.cls, IADOConnector.cls
'       Dependent on ErrorADOSQL.cls, ErrorContentKeeper.cls
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Private WinAPI Definitions
'///////////////////////////////////////////////
#If VBA7 Then
    ' For 64 bit version windows, declare timeGetTime
    Private Declare PtrSafe Function timeGetTime Lib "winmm.dll" () As Long
#Else
    ' For 32 bit version windows, declare timeGetTime
    Private Declare Function timeGetTime Lib "winmm.dll" () As Long
#End If


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** ADO connecting for various provider
'**---------------------------------------------
'''
'''
'''
Public Sub TryToConnectRDBProviderByAdoConnectionString(ByRef rblnIsConnected As Boolean, ByRef ritfConnection As IADOConnector, ByRef ritfADOConnectStrGenerator As IADOConnectStrGenerator)

    rblnIsConnected = False

    If Not ritfConnection.IsConnected Then
    
        InitializeTextForADOParametersBeforeADOSQLExecution
        
        Set ritfConnection.ADOConnection = New ADODB.Connection
        
        With ritfConnection.ADOConnection
        
            .ConnectionString = ritfADOConnectStrGenerator.GetConnectionString ' strConnection
            
            If ritfConnection.CommandTimeout > 0 And .CommandTimeout <> ritfConnection.CommandTimeout Then
                
                ' Set the ADODB.Connection.CommandTimeout property
                
                .CommandTimeout = ritfConnection.CommandTimeout
            End If
            
            .Open
        End With
    
        ritfConnection.IsConnected = True
    End If

    rblnIsConnected = ritfConnection.IsConnected
End Sub


'**---------------------------------------------
'** ADO Recordset sheet expansion support tools
'**---------------------------------------------
'''
'''
'''
Public Sub DetectRecordsetRecordsCountState(ByRef rblnSupportedPropertyRecordCount As Boolean, ByRef robjSQLResult As SQLResult, ByRef robjRSet As ADODB.Recordset)

    If robjRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then

        If robjRSet.RecordCount = -1 Then
            
            rblnSupportedPropertyRecordCount = False
        Else
            rblnSupportedPropertyRecordCount = True
            
            If Not robjRSet.EOF Then
            
                robjSQLResult.TableRecordCount = robjRSet.RecordCount
            End If
        End If
    Else
        rblnSupportedPropertyRecordCount = False
    End If
End Sub

'''
'''
'''
Public Sub GetFieldCountsAndStartToMeasureTimeOfExpandingToSheet(ByRef robjSQLResult As SQLResult, _
        ByRef robjRSet As ADODB.Recordset, _
        ByRef rintExcelSheetOutputTime1 As Long)

    If robjRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
    
        robjSQLResult.TableFieldCount = robjRSet.Fields.Count
    End If

    rintExcelSheetOutputTime1 = timeGetTime()
End Sub

'''
'''
'''
Public Sub GetMeasureTimeOfExpandedRecordsetToSheet(ByRef robjSQLResult As SQLResult, _
        ByRef rintExcelSheetOutputTime1 As Long, _
        ByRef rintExcelSheetOutputTime2 As Long)


    rintExcelSheetOutputTime2 = timeGetTime()
        
    robjSQLResult.ExcelSheetOutputElapsedTime = rintExcelSheetOutputTime2 - rintExcelSheetOutputTime1
End Sub

'**---------------------------------------------
'** logging around SQL execution
'**---------------------------------------------
'''
'''
'''
''' <Argument>robjSQLResult: Output</Argument>
''' <Argument>robjLoadedADOConnectionSetting: Output</Argument>
''' <Argument>rintT1: Output</Argument>
''' <Argument>rblnAllowToRecordSQLLog: Input</Argument>
''' <Argument>rstrSQL: Input</Argument>
Public Sub SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectBeforeExecuteSQL(ByRef robjSQLResult As SQLResult, _
        ByRef robjLoadedADOConnectionSetting As LoadedADOConnectionSetting, _
        ByRef rintT1 As Long, _
        ByRef rblnAllowToRecordSQLLog As Boolean, _
        ByRef rstrSQL As String)


    If rblnAllowToRecordSQLLog Then
    
        Set robjSQLResult = New SQLResult
        
        Set robjLoadedADOConnectionSetting = New LoadedADOConnectionSetting
        
        robjSQLResult.SQL = rstrSQL

        rintT1 = timeGetTime()
    Else
        Set robjSQLResult = Nothing
        
        Set robjLoadedADOConnectionSetting = Nothing
    End If
End Sub


'''
'''
'''
''' <Argument>robjSQLResult: Output</Argument>
''' <Argument>robjLoadedADOConnectionSetting: Output</Argument>
''' <Argument>rintT1: Input</Argument>
''' <Argument>rintT2: Output</Argument>
''' <Argument>rblnAllowToRecordSQLLog: Input</Argument>
''' <Argument>robjConnection: Input</Argument>
''' <Argument>robjADOConnectionSetting: Input</Argument>
''' <Argument>robjRSet: Input</Argument>
''' <Argument>vintRecordsAffected: Input</Argument>
Public Sub SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectAfterExecuteSQL(ByRef robjSQLResult As SQLResult, _
        ByRef robjLoadedADOConnectionSetting As LoadedADOConnectionSetting, _
        ByRef rintT1 As Long, _
        ByRef rintT2 As Long, _
        ByRef rblnAllowToRecordSQLLog As Boolean, _
        ByRef robjConnection As ADODB.Connection, _
        ByRef robjADOConnectionSetting As ADOConnectionSetting, _
        ByRef robjRSet As ADODB.Recordset, _
        Optional ByVal vintRecordsAffected As Long = -1)


    LoggingTimesAndOthersAfterExecuteSQL robjSQLResult, rintT1, rintT2, rblnAllowToRecordSQLLog, vintRecordsAffected

    LoggingLoadedADOConnectionSettingAfterExecuteSQL robjSQLResult, robjLoadedADOConnectionSetting, rblnAllowToRecordSQLLog, robjConnection, robjADOConnectionSetting, robjRSet
End Sub

'''
'''
'''
''' <Argument>robjSQLResult: Input-Output</Argument>
''' <Argument>rintT1: Input</Argument>
''' <Argument>rintT2: Output</Argument>
''' <Argument>rblnAllowToRecordSQLLog: Input</Argument>
''' <Argument>vintRecordsAffected: Input</Argument>
Public Sub LoggingTimesAndOthersAfterExecuteSQL(ByRef robjSQLResult As SQLResult, _
        ByRef rintT1 As Long, _
        ByRef rintT2 As Long, _
        ByRef rblnAllowToRecordSQLLog As Boolean, _
        Optional ByVal vintRecordsAffected As Long = -1)


    If rblnAllowToRecordSQLLog Then
    
        rintT2 = timeGetTime()
                
        With robjSQLResult
        
            .SQLExeTime = Now()
            
            .SQLExeElapsedTime = rintT2 - rintT1
            
            If vintRecordsAffected >= 0 Then
            
                .RecordsAffected = vintRecordsAffected
            End If
        End With
        
        KeepSQLResultToSharedCurrentUserDomain robjSQLResult
    End If
End Sub


'''
'''
'''
Public Sub LoggingLoadedADOConnectionSettingAfterExecuteSQL(ByRef robjSQLResult As SQLResult, _
        ByRef robjLoadedADOConnectionSetting As LoadedADOConnectionSetting, _
        ByRef rblnAllowToRecordSQLLog As Boolean, _
        ByRef robjConnection As ADODB.Connection, _
        ByRef robjADOConnectionSetting As ADOConnectionSetting, _
        ByRef robjRSet As ADODB.Recordset)


    If rblnAllowToRecordSQLLog Then
    
        robjLoadedADOConnectionSetting.SetLoadedParameters robjConnection, robjADOConnectionSetting, robjRSet
        
        robjSQLResult.SetSQLOptionalLog robjLoadedADOConnectionSetting
    End If
End Sub

'**---------------------------------------------
'** SQL command logging, UPDATE, INSERT, DELETE
'**---------------------------------------------
'''
'''
'''
Public Sub DebugLoggingOfSQLCommand(ByRef rstrSQL As String, ByRef robjSQLResult As SQLResult, ByRef rintRecordsAffected As Long)

    Dim udtCurrent As Date, strCurrentDateTime As String


    udtCurrent = robjSQLResult.SQLExeTime
    
    strCurrentDateTime = FormatDateTime(udtCurrent, vbLongDate) & ", " & FormatDateTime(udtCurrent, vbLongTime)
    
    Select Case GetSQLSyntaxType(rstrSQL)
    
        Case RdbSqlClassification.RdbSqlDeleteType
            
            Debug.Print GetTextOfStrKeyAdoParametersCountOfSqlDelete() & ": " & CStr(rintRecordsAffected) & " - " & strCurrentDateTime
            
            Debug.Print GetTextOfStrKeyAdoParametersDeletingElapsedTime() & ": " & Format(CDbl(robjSQLResult.SQLExeElapsedTime) / 1000, "0.000") & " " & GetTextOfStrKeyAdoParametersUnitSecond()
        
        Case RdbSqlClassification.RdbSqlUpdateType
            
            Debug.Print GetTextOfStrKeyAdoParametersCountOfSqlUpdate() & ": "; CStr(rintRecordsAffected) & " - " & strCurrentDateTime
            
            Debug.Print GetTextOfStrKeyAdoParametersUpdatingElapsedTime() & ": " & Format(CDbl(robjSQLResult.SQLExeElapsedTime) / 1000, "0.000") & " " & GetTextOfStrKeyAdoParametersUnitSecond()
        
        Case RdbSqlClassification.RdbSqlInsertType
            
            Debug.Print GetTextOfStrKeyAdoParametersCountOfSqlInsert() & ": " & CStr(rintRecordsAffected) & " - " & strCurrentDateTime
            
            Debug.Print GetTextOfStrKeyAdoParametersInsertingElapsedTime() & ": " & Format(CDbl(robjSQLResult.SQLExeElapsedTime) / 1000, "0.000") & " " & GetTextOfStrKeyAdoParametersUnitSecond()
    End Select
End Sub


'**---------------------------------------------
'** ADO SQL error trapping
'**---------------------------------------------
'''
''' Catch the SQL execution error
'''
Public Sub HandleAdoSQlError(ByRef robjSQLResult As SQLResult, _
        ByRef rintT1 As Long, _
        ByRef rintT2 As Long, _
        ByRef rblnAllowToRecordSQLLog As Boolean, _
        ByRef robjLoadedADOConnectionSetting As LoadedADOConnectionSetting, _
        ByRef robjConnection As ADODB.Connection, _
        ByRef robjADOConnectionSetting As ADOConnectionSetting, _
        ByVal vobjErrorContent As ErrorContentKeeper, _
        ByRef renmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag, _
        Optional ByVal vobjRecordset As ADODB.Recordset = Nothing)

    Dim objErrorADOSQL As ErrorADOSQL
    
    
    Debug.Print "[Err.Source] :" & vobjErrorContent.Source
                
                
    LoggingTimesAndOthersAfterExecuteSQL robjSQLResult, rintT1, rintT2, rblnAllowToRecordSQLLog
    
    Set objErrorADOSQL = GetErrorAdoSqlObject(vobjErrorContent, robjSQLResult, robjLoadedADOConnectionSetting, robjConnection, robjADOConnectionSetting, vobjRecordset)
    
    If (renmShowUpAdoErrorOptionFlag And SuppressToShowUpAdoSqlErrorByExcelSheetFlag) = 0 Then
    
        ShowErrorADOExecutedSQLExcelSheetIfExcelIsInstalled objErrorADOSQL, rblnAllowToRecordSQLLog
    End If
    
    If (renmShowUpAdoErrorOptionFlag And SuppressToShowUpAdoSqlErrorByFormFlag) = 0 Then
    
        ShowErrorADOExecutedSQLForm objErrorADOSQL
    End If
End Sub
'''
'''
'''
Public Function GetErrorAdoSqlObject(ByRef robjErrorContent As ErrorContentKeeper, _
        ByRef robjSQLResult As SQLResult, _
        ByRef robjLoadedADOConnectionSetting As LoadedADOConnectionSetting, _
        ByRef robjConnection As ADODB.Connection, _
        ByRef robjADOConnectionSetting As ADOConnectionSetting, _
        ByRef robjRSet As ADODB.Recordset) As ErrorADOSQL


    Dim objErrorADOSQL As ErrorADOSQL

    Set objErrorADOSQL = New ErrorADOSQL

    robjLoadedADOConnectionSetting.SetLoadedParameters robjConnection, robjADOConnectionSetting, robjRSet
    
    objErrorADOSQL.SetLogWithLoadedConnectSetting robjErrorContent, robjSQLResult, robjLoadedADOConnectionSetting
    
    Set GetErrorAdoSqlObject = objErrorADOSQL
End Function

'''
''' Show ADO SQL error contents by a Excel sheet, if the Excel is installed and the GenerateADOSQLExecutionErrorLogSheet operation module is loaded.
'''
Public Sub ShowErrorADOExecutedSQLExcelSheetIfExcelIsInstalled(ByRef robjErrorADOSQL As ErrorADOSQL, ByRef rblnAllowToRecordSQLLog As Boolean)

    If rblnAllowToRecordSQLLog Then

        'GenerateADOSQLExecutionErrorLogSheet robjErrorADOSQL

        On Error Resume Next

        ' When this VB project doesn't include GenerateADOSQLExecutionErrorLogSheet operation, the following will be ignored.
        
        ' One of merits is that doesn't cause any VBA compile errors although the Excel hadn't been installed.

        CallBackInterfaceBySingleArguments "GenerateADOSQLExecutionErrorLogSheet", robjErrorADOSQL
        
        On Error GoTo 0
    End If
End Sub

'''
''' Show ADO SQL error contents by user-form
'''
Public Sub ShowErrorADOExecutedSQLForm(ByRef robjErrorADOSQL As ErrorADOSQL, Optional ByVal vblnSQLExecuteAsynchronous As Boolean = False)

    Dim objErrorADOSQLForm As UErrorADOSQLForm: Set objErrorADOSQLForm = New UErrorADOSQLForm
    
    With objErrorADOSQLForm
    
        .SetAndShowForm robjErrorADOSQL, vblnSQLExecuteAsynchronous
        
        .Show
    End With
End Sub


