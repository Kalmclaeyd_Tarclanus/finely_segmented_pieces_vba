Attribute VB_Name = "DataTableADOIn"
'
'   get data-table collection object or dictionary object from ADODB.Recordset which either SQL query or SQL cammand is used without any SQL error.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  4/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' return Collection(Of String) from Recordset instance after SQL SELECT
'''
Public Function GetTableCollectionFromRSet(ByVal vobjRSet As ADODB.Recordset) As Collection
    
    Dim objCol As Collection, objChildCol As Collection, intColumn As Long

    Set objCol = New Collection
    
    With vobjRSet
        
        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
        
            If .Fields.Count = 1 Then
            
                Do While Not .EOF
                    
                    objCol.Add .Fields.Item(0).Value
                    
                    .MoveNext
                Loop
            Else
                Do While Not .EOF
                    
                    Set objChildCol = New Collection
                    
                    For intColumn = 0 To .Fields.Count - 1
                        
                        objChildCol.Add .Fields.Item(intColumn).Value
                    Next
                    
                    objCol.Add objChildCol
                    
                    .MoveNext
                Loop
            End If
        Else
            Debug.Print "The ADODB.Recordset has been already closed at the GetTableCollectionFromRSet function."
        End If
    End With

    Set GetTableCollectionFromRSet = objCol
End Function


'''
''' return Collection(Of Date) from Recordset instance after SQL SELECT
'''
Public Function GetTableCollectionAsDateFromRSet(ByVal vobjRSet As ADODB.Recordset) As Collection
    
    Dim objCol As Collection, objChildCol As Collection, intColumn As Long, varValue As Variant

    Set objCol = New Collection
    
    With vobjRSet
        
        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
        
            If .Fields.Count = 1 Then
            
                Do While Not .EOF
                    
                    varValue = .Fields.Item(0).Value
                    
                    If IsDate(varValue) Then
                    
                        objCol.Add CDate(varValue)
                    Else
                    
                        objCol.Add varValue
                    End If
                    
                    .MoveNext
                Loop
            Else
                Do While Not .EOF
                    
                    Set objChildCol = New Collection
                    
                    For intColumn = 0 To .Fields.Count - 1
                    
                        varValue = .Fields.Item(intColumn).Value
                    
                        If IsDate(varValue) Then
                    
                            objChildCol.Add CDate(varValue)
                        Else
                        
                            objChildCol.Add varValue
                        End If
                        'objChildCol.Add .Fields.Item(intColumn).Value
                    Next
                    
                    objCol.Add objChildCol
                    
                    .MoveNext
                Loop
            End If
        Else
            Debug.Print "The ADODB.Recordset has been already closed at the GetTableCollectionAsDateFromRSet function."
        End If
    End With

    Set GetTableCollectionAsDateFromRSet = objCol
End Function

'''
'''
'''
''' <Argument>vobjRSet: a Recordset instance after SQL SELECT execution</Argument>
''' <Argument>vblnOmitDuplicatedKeys: Default is false, </Argument>
''' <Return>Dictionary(Of String, String) from a Recordset instance after SQL SELECT execution</Return>
Public Function GetTableDictionaryFromRSet(ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnOmitDuplicatedKeys As Boolean = False) As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary

    StoreTableIntoDictionaryFromRSet objDic, vobjRSet, vblnOmitDuplicatedKeys

    Set GetTableDictionaryFromRSet = objDic
End Function


'''
'''
'''
''' <Argument>robjDic: Output Dictionary(Of Key, Value) or Dictionary(Of Key, Collection(Of Value)) - key is 1st column value of Recordset</Argument>
''' <Argument>vobjRSet: Input - a Recordset instance after SQL SELECT execution, which has to include more than one column</Argument>
''' <Argument>vblnOmitDuplicatedKeys: Input - Default is false, </Argument>
Public Sub StoreTableIntoDictionaryFromRSet(ByRef robjDic As Scripting.Dictionary, ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnOmitDuplicatedKeys As Boolean = False)

    Dim objRowCol As Collection, intColumn As Long
    Dim varKey As Variant
    
    If robjDic Is Nothing Then Set robjDic = New Scripting.Dictionary
    
    With vobjRSet
    
        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
    
            If .Fields.Count > 2 Then
            
                Do While Not .EOF
                    
                    Set objRowCol = New Collection
                    
                    For intColumn = 1 To .Fields.Count - 1
                    
                        objRowCol.Add .Fields.Item(intColumn).Value  ' From the second column on, these column values are stored to Collection
                    Next
                    
                    If vblnOmitDuplicatedKeys Then
                    
                        With robjDic
                        
                            varKey = vobjRSet.Fields.Item(0)    ' The first column value is key
                            
                            If Not .Exists(varKey) Then
                            
                                .Add varKey, objRowCol
                            End If
                        End With
                    Else
                        robjDic.Add .Fields.Item(0).Value, objRowCol ' The first column value is key
                    End If
                    
                    .MoveNext
                Loop
            Else
                Do While Not .EOF
                    
                    If vblnOmitDuplicatedKeys Then
                    
                        With robjDic
                        
                            varKey = vobjRSet.Fields.Item(0)
                            
                            If Not .Exists(varKey) Then
                            
                                .Add varKey, vobjRSet.Fields.Item(1).Value
                            End If
                        End With
                    Else
                        robjDic.Add .Fields.Item(0).Value, .Fields.Item(1).Value
                    End If
                    
                    .MoveNext
                Loop
            End If
        Else
            Debug.Print "The ADODB.Recordset has been already closed at the StoreTableIntoDictionaryFromRSet function."
        End If
    End With
End Sub


'''
'''
'''
''' <Argument>vobjRSet: a Recordset instance after SQL SELECT execution</Argument>
''' <Argument>vblnOmitDuplicatedKeys: Default is false, </Argument>
''' <Return>Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Value)) or Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Collection(Of Value))) from a Recordset instance after SQL SELECT execution</Return>
Public Function GetTableOneNestedDictionaryFromRSet(ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnOmitDuplicatedKeys As Boolean = False) As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary

    StoreTableIntoOneNestedDictionaryFromRSet objDic, vobjRSet, vblnOmitDuplicatedKeys

    Set GetTableOneNestedDictionaryFromRSet = objDic
End Function


'''
'''
'''
''' <Argument>robjDic: Output Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Value)) or Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Collection(Of Value))) - key is 1st column value of Recordset</Argument>
''' <Argument>vobjRSet: Input - a Recordset instance after SQL SELECT execution, which has to include more than one column</Argument>
''' <Argument>vblnOmitDuplicatedKeys: Input - Default is false, </Argument>
Public Sub StoreTableIntoOneNestedDictionaryFromRSet(ByRef robjDic As Scripting.Dictionary, ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnOmitAfterSecondDuplicatedKeys As Boolean = False)

    Dim objChildDic As Scripting.Dictionary, objRowCol As Collection, intColumn As Long
    Dim var1stKey As Variant, var2ndKey As Variant, varValue As Variant
    
    If robjDic Is Nothing Then Set robjDic = New Scripting.Dictionary
    
    With vobjRSet
    
        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
    
            If .Fields.Count > 3 Then
            
                Do While Not .EOF
                    
                    Set objRowCol = New Collection
                    
                    For intColumn = 2 To .Fields.Count - 1
                    
                        objRowCol.Add .Fields.Item(intColumn).Value  ' From the third column on, these column values are stored to Collection
                    Next
                    
                    var1stKey = .Fields.Item(0)
                    
                    var2ndKey = .Fields.Item(1)
                    
                    With robjDic
                    
                        If Not .Exists(var1stKey) Then
                        
                            Set objChildDic = New Scripting.Dictionary
                        
                            .Add var1stKey, objChildDic
                        Else
                        
                            Set objChildDic = .Item(var1stKey)
                        End If
                    End With
                    
                    If vblnOmitAfterSecondDuplicatedKeys Then
                    
                        With objChildDic
                        
                            If Not .Exists(var2ndKey) Then
                            
                                .Add var2ndKey, objRowCol
                            End If
                        End With
                    Else
                        objChildDic.Add var2ndKey, objRowCol ' The first column value is key
                    End If
                    
                    .MoveNext
                Loop
            Else
                Do While Not .EOF
                    
                    var1stKey = .Fields.Item(0)
                    
                    var2ndKey = .Fields.Item(1)
                    
                    varValue = .Fields.Item(2)
                    
                    With robjDic
                    
                        If Not .Exists(var1stKey) Then
                        
                            Set objChildDic = New Scripting.Dictionary
                        
                            .Add var1stKey, objChildDic
                        Else
                        
                            Set objChildDic = .Item(var1stKey)
                        End If
                    End With
                    
                    If vblnOmitAfterSecondDuplicatedKeys Then
                    
                        With objChildDic
                        
                            If Not .Exists(var2ndKey) Then
                            
                                .Add var2ndKey, varValue
                            End If
                        End With
                    Else
                        objChildDic.Add var2ndKey, varValue
                    End If
                    
                    .MoveNext
                Loop
            End If
        Else
            Debug.Print "The ADODB.Recordset has been already closed at the StoreTableIntoOneNestedDictionaryFromRSet function."
        End If
    End With
End Sub


'''
'''
'''
''' <Argument>vobjRSet: a Recordset instance after SQL SELECT execution</Argument>
''' <Argument>vblnOmitDuplicatedKeys: Default is false, </Argument>
''' <Return>Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Value)) or Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Collection(Of Value))) from a Recordset instance after SQL SELECT execution</Return>
Public Function GetTableOneNestedDictionaryFromRSetWithSpecialConversion(ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnOmitDuplicatedKeys As Boolean = False) As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary

    StoreTableIntoOneNestedDictionaryFromRSetWithSpecialConversion objDic, vobjRSet, vblnOmitDuplicatedKeys

    Set GetTableOneNestedDictionaryFromRSetWithSpecialConversion = objDic
End Function


'''
'''
'''
''' <Argument>robjDic: Output Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Value)) or Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Collection(Of Value))) - key is 1st column value of Recordset</Argument>
''' <Argument>vobjRSet: Input - a Recordset instance after SQL SELECT execution, which has to include more than one column</Argument>
''' <Argument>vblnOmitDuplicatedKeys: Input - Default is false, </Argument>
Public Sub StoreTableIntoOneNestedDictionaryFromRSetWithSpecialConversion(ByRef robjDic As Scripting.Dictionary, _
        ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vblnOmitAfterSecondDuplicatedKeys As Boolean = False)

    Dim objChildDic As Scripting.Dictionary, objRowCol As Collection, intColumn As Long
    Dim var1stValue As Variant, var2ndKey As Variant, varValue As Variant
    
    Dim strCurrent1stKey As Variant
    
    If robjDic Is Nothing Then Set robjDic = New Scripting.Dictionary
    
    strCurrent1stKey = ""
    
    With vobjRSet
    
        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
    
            If .Fields.Count > 3 Then
            
                Do While Not .EOF
                    
                    Set objRowCol = New Collection
                    
                    For intColumn = 2 To .Fields.Count - 1
                    
                        objRowCol.Add .Fields.Item(intColumn).Value  ' From the third column on, these column values are stored to Collection
                    Next
                    
                    var1stValue = .Fields.Item(0)
                    
                    If var1stValue <> "" Then
                    
                        strCurrent1stKey = var1stValue
                    End If
                    
                    var2ndKey = .Fields.Item(1)
                    
                    With robjDic
                    
                        If Not .Exists(strCurrent1stKey) Then
                        
                            Set objChildDic = New Scripting.Dictionary
                        
                            .Add strCurrent1stKey, objChildDic
                        Else
                        
                            Set objChildDic = .Item(strCurrent1stKey)
                        End If
                    End With
                    
                    If vblnOmitAfterSecondDuplicatedKeys Then
                    
                        With objChildDic
                        
                            If Not .Exists(var2ndKey) Then
                            
                                .Add var2ndKey, objRowCol
                            End If
                        End With
                    Else
                        objChildDic.Add var2ndKey, objRowCol ' The first column value is key
                    End If
                    
                    .MoveNext
                Loop
            Else
                Do While Not .EOF
                    
                    var1stValue = .Fields.Item(0)
                    
                    If var1stValue <> "" Then
                    
                        strCurrent1stKey = var1stValue
                    End If
                    
                    var2ndKey = .Fields.Item(1)
                    
                    varValue = .Fields.Item(2)
                    
                    With robjDic
                    
                        If Not .Exists(strCurrent1stKey) Then
                        
                            Set objChildDic = New Scripting.Dictionary
                        
                            .Add strCurrent1stKey, objChildDic
                        Else
                            Set objChildDic = .Item(strCurrent1stKey)
                        End If
                    End With
                    
                    If vblnOmitAfterSecondDuplicatedKeys Then
                    
                        With objChildDic
                        
                            If Not .Exists(var2ndKey) Then
                            
                                .Add var2ndKey, varValue
                            End If
                        End With
                    Else
                        objChildDic.Add var2ndKey, varValue
                    End If
                    
                    .MoveNext
                Loop
            End If
        Else
            Debug.Print "The ADODB.Recordset has been already closed at the StoreTableIntoOneNestedDictionaryFromRSetWithSpecialConversion function."
        End If
    End With
End Sub




'''
''' return Dictionary(Of String, Boolean) from Recordset instance after SQL SELECT
'''
''' Not 0 (for example 1) to True, 0 to False
'''
Public Function GetTableDictionaryAsValuesAreBooleanFromRSet(ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vblnOmitDuplicatedKeys As Boolean = False) As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, objRowCol As Collection, intColumn As Long
    Dim varKey As Variant, blnValue As Boolean
    
    Set objDic = New Scripting.Dictionary

    With vobjRSet
    
        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
    
            If .Fields.Count > 2 Then
            
                Do While Not .EOF
                    
                    Set objRowCol = New Collection
                    
                    For intColumn = 1 To .Fields.Count - 1
                    
                        blnValue = False
                        
                        If Not IsObject(.Fields.Item(intColumn).Value) Then
                        
                            If IsNumeric(.Fields.Item(intColumn).Value) Then
                                
                                If .Fields.Item(intColumn).Value <> 0 Then
                                
                                    blnValue = True
                                End If
                            End If
                        End If
                    
                        objRowCol.Add blnValue
                    Next
                    
                    If vblnOmitDuplicatedKeys Then
                    
                        With objDic
                        
                            varKey = vobjRSet.Fields.Item(0)
                            
                            If Not .Exists(varKey) Then
                            
                                .Add varKey, objRowCol
                            End If
                        End With
                    Else
                        objDic.Add .Fields.Item(0).Value, objRowCol
                    End If
                    
                    .MoveNext
                Loop
            Else
                Do While Not .EOF
                    
                    If vblnOmitDuplicatedKeys Then
                    
                        With objDic
                            
                            varKey = vobjRSet.Fields.Item(0)
                            
                            If Not .Exists(varKey) Then
                            
                                blnValue = False
                        
                                If Not IsObject(.Fields.Item(1).Value) Then
                                
                                    If IsNumeric(.Fields.Item(1).Value) Then
                                        
                                        If .Fields.Item(1).Value <> 0 Then
                                            blnValue = True
                                        End If
                                        
                                    End If
                                End If
                            
                                .Add varKey, blnValue
                                
                            End If
                        End With
                    Else
                        blnValue = False
                        
                        If Not IsObject(.Fields.Item(1).Value) Then
                        
                            If IsNumeric(.Fields.Item(1).Value) Then
                                
                                If .Fields.Item(1).Value <> 0 Then
                                
                                    blnValue = True
                                End If
                                
                            End If
                        End If
                    
                        objDic.Add .Fields.Item(0).Value, blnValue
                    End If
                    
                    .MoveNext
                Loop
            End If
        Else
            Debug.Print "The ADODB.Recordset has been already closed at the GetTableDictionaryAsValuesAreBooleanFromRSet function."
        End If
    End With

    Set GetTableDictionaryAsValuesAreBooleanFromRSet = objDic
End Function


'''
'''
'''
Public Sub GetTableDictionaryAndRemainedCollectionFromRSet(ByRef robjDTDic As Scripting.Dictionary, ByRef robjRemainedDTCol As Collection, ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnAddKeyValueForFirstItemAboutMoreThanOneItem As Boolean = False)

    Dim objKeyToColDic As Scripting.Dictionary, objValuesCol As Collection
    Dim objRowCol As Collection, intColumn As Long
    Dim varKey As Variant, varItem As Variant, objChildCol As Collection, varRowItem As Variant
    
    Set objKeyToColDic = New Scripting.Dictionary

    ' serve the index 0 of Recordset.Fields.Item as dictionary key first key to values col
    With vobjRSet
    
        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
    
            If .Fields.Count > 2 Then
            
                Do While Not .EOF
                
                    varKey = .Fields.Item(0).Value  ' The first column is key
                    
                    Set objRowCol = New Collection
                    
                    For intColumn = 1 To .Fields.Count - 1
                        
                        objRowCol.Add .Fields.Item(intColumn).Value ' From the second column on, these column are stored to Collection
                    Next
                    
                    With objKeyToColDic
                        
                        If Not .Exists(varKey) Then
                            
                            Set objValuesCol = New Collection
                            
                            objValuesCol.Add objRowCol
                            
                            .Add varKey, objValuesCol
                        Else
                            Set objValuesCol = .Item(varKey)
                        
                            objValuesCol.Add objRowCol
                            
                            Set .Item(varKey) = objValuesCol
                        End If
                    End With
                    
                    .MoveNext
                Loop
            Else
                Do While Not .EOF
                    
                    varKey = .Fields.Item(0).Value  ' The first column is key
                    varItem = .Fields.Item(1).Value ' The second column is value
                    
                    With objKeyToColDic
                        
                        If Not .Exists(varKey) Then
                            
                            Set objValuesCol = New Collection
                            
                            objValuesCol.Add varItem
                            
                            .Add varKey, objValuesCol
                        Else
                            Set objValuesCol = .Item(varKey)
                        
                            objValuesCol.Add varItem
                            
                            Set .Item(varKey) = objValuesCol
                        End If
                    
                    End With
                    
                    .MoveNext
                Loop
            End If
        Else
            Debug.Print "The ADODB.Recordset has been already closed at the GetTableDictionaryAndRemainedCollectionFromRSet function."
        End If
    End With


    Set robjDTDic = New Scripting.Dictionary
    
    Set robjRemainedDTCol = New Collection

    ' separate the first key values-col for a main dictionay and a remained col
    With objKeyToColDic
    
        For Each varKey In .Keys
        
            Set objValuesCol = .Item(varKey)
        
            If objValuesCol.Count = 1 Then
            
                If IsObject(objValuesCol.Item(1)) Then
                    
                    Set varItem = objValuesCol.Item(1)
                Else
                    varItem = objValuesCol.Item(1)
                End If
            
                robjDTDic.Add varKey, varItem
            Else
                For Each varItem In objValuesCol
                    
                    Set objChildCol = New Collection
                    
                    objChildCol.Add varKey
                    
                    If IsObject(varItem) Then
                    
                        Set objRowCol = varItem
                        
                        For Each varRowItem In objRowCol
                            
                            objChildCol.Add varRowItem
                        Next
                    Else
                        objChildCol.Add varRowItem
                    End If
                    
                    robjRemainedDTCol.Add objChildCol
                Next
            
                If vblnAddKeyValueForFirstItemAboutMoreThanOneItem Then
                    
                    If Not robjDTDic.Exists(varKey) Then
                        
                        robjDTDic.Add varKey, objValuesCol.Item(1)
                    End If
                End If
            End If
        Next
    End With
End Sub


