VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} USetAdoOleDbConStrForXlBook 
   Caption         =   "USetAdoOleDbConStrForXlBook"
   ClientHeight    =   7840
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   5640
   OleObjectBlob   =   "USetAdoOleDbConStrForXlBook.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "USetAdoOleDbConStrForXlBook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   ADO Connection string parameters form for Microsoft Excel sheet by a OLE DB interface
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both ADO and Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 14/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "USetAdoOleDbConStrForXlBook"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

' User-form document object
Private mobjFormStateToSetAdoConStr As FormStateToSetAdoConStr


' user-form input result cache
Private mobjCurrentUserInputParametersDic As Scripting.Dictionary

' When FixedSettingNameMode is false, then the following is read from Registry
Private mobjSettingKeyNameToUserInputParametersDic As Scripting.Dictionary  ' Registry cache


'**---------------------------------------------
'** Form state control
'**---------------------------------------------
Private mblnFixedSettingNameMode As Boolean


Private mblnIsFormInitialized As Boolean

Private mintInitializing As Long


Private mobjAccessOLEDBProviderTypeToListIndexDic As Scripting.Dictionary

Private mobjIMEXModeToListIndexDic As Scripting.Dictionary


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub UserForm_Initialize()

    mblnFixedSettingNameMode = False

    mblnIsFormInitialized = False
End Sub

'''
'''
'''
Private Sub UserForm_Layout()

    If Not mblnIsFormInitialized Then
    
        Set mobjAccessOLEDBProviderTypeToListIndexDic = InitializeCombBoxOfAceOleDb(cboUsingAccdbOleDb)
    
        Set mobjIMEXModeToListIndexDic = InitializeCombBoxOfAdoIMEXMode(cboIMEXMode)
    
        msubClearAll
        
        GetCurrentFormPositionFromUserReg mstrModuleName, Me, Application.Name
    
        msubInitializePasswordTextBox
        
        msubInitializeFixedSettingNameMode
        
        msubInitializeCachingConStrParamsOptions
        
        msubLocalize
    
        msubRefresh
    
    
        msubInitializeFirstControlFocus
        
        mblnIsFormInitialized = True
    End If
End Sub

'''
'''
'''
Private Sub UserForm_QueryClose(ByRef rintCancel As Integer, ByRef rintCloseMode As Integer)

    ' When an user clicks the cancel X button, then rintCloseMode becomes False

    If VBA.VbQueryClose.vbFormControlMenu = rintCloseMode Then
    
        mobjFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection = True
    Else
        msubUpdateFormStateToSetAdoConStr
    End If
    
    SetCurrentFormPositionToUserReg mstrModuleName, Me, Application.Name
End Sub

'''
'''
'''
Private Sub cmdOK_Click()

    VBA.Global.Unload Me
End Sub

'''
'''
'''
Private Sub chkSaveParametersInRegistoryExceptForPassword_Change()

    If mintInitializing <= 0 Then
    
        SetAdoConnectionSaveParametersInRegistoryExceptForPassword chkSaveParametersInRegistoryExceptForPassword.Value
    End If
End Sub

'''
'''
'''
Private Sub chkSavePasswordTemporaryCache_Change()

    If mintInitializing <= 0 Then

        SetAllowToSaveAdoConStrPasswordInTemporaryCache chkSavePasswordTemporaryCache.Value
    End If
End Sub


'''
'''
'''
Private Sub cmdAdoConnectionTest_Click()

    Dim strConStr As String
    
    msubUpdateInputDicOfFormState
    
    msubOpenPasswordLockedExcelBookIfThereIsAPassword mobjCurrentUserInputParametersDic
    
    strConStr = GetADOConStrOfAccessOleDbToExcelSheetFromInputDic(mobjCurrentUserInputParametersDic).GetConnectionString()
    
    AdoVariousTypeConnectionTestAndShowMessageBox strConStr, AdoOleDbAndMsExcel
End Sub

'''
'''
'''
Private Sub txtPasswordForExcelBook_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    Dim strConStr As String, objBook As Excel.Workbook

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyReturn Then
    
            msubUpdateInputDicOfFormState
            
            msubOpenPasswordLockedExcelBookIfThereIsAPassword mobjCurrentUserInputParametersDic
            
            strConStr = GetADOConStrOfAccessOleDbToExcelSheetFromInputDic(mobjCurrentUserInputParametersDic).GetConnectionString()
            
            If IsADOConnectionStringEnabled(strConStr) Then
    
                cmdOK_Click
            End If
        ElseIf venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
            
            msubCloseFormToCancel
        End If
    End If
End Sub


Private Sub txtFullPathOfExcelBook_KeyDown(ByVal venmKeyCode As MSForms.ReturnInteger, ByVal vintShift As Integer)

    If mintInitializing <= 0 Then
    
        If venmKeyCode = VBA.KeyCodeConstants.vbKeyEscape Then
        
            msubCloseFormToCancel
        End If
    End If
End Sub

'''
'''
'''
Private Sub msubOpenPasswordLockedExcelBookIfThereIsAPassword(ByRef robjCurrentUserInputParametersDic As Scripting.Dictionary)

    Dim objBook As Object

    With robjCurrentUserInputParametersDic
    
        If .Exists(mstrTemporaryPdKey) Then
    
            If .Item(mstrTemporaryPdKey) <> "" Then
            
                ' When the Excel book file is locked by a password, the ADO connection is able after the Excel book is opened
            
                Set objBook = GetMsExcelWorkbookByInterface(.Item(mstrAdoDbFilePathKey), False, vstrPassword:=.Item(mstrTemporaryPdKey))
            End If
        End If
    End With
End Sub


'''
'''
'''
Private Sub cmdBrowse_Click()

    Dim strInitialPath As String, strPath As String
    
    strInitialPath = txtFullPathOfExcelBook.Text
    
    If Not FileExistsByVbaDir(strInitialPath) Then
    
        strInitialPath = GetParentDirectoryPathOfCurrentOfficeFile()
    End If
    
    strPath = GetFilePathByApplicationFileDialog(GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectMsExcelBookFile(), strInitialPath, GetTextOfStrKeyUserFormToolsForAdoConStrMsExcelBook() & ",*.xlsx;*.xlsm;*.xls", "", True, "BrowseExcelSheetDirPath")
    
    If strPath <> "" Then
    
        mintInitializing = mintInitializing + 1
    
        txtFullPathOfExcelBook.Text = strPath
    
        mintInitializing = mintInitializing - 1
    End If
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vobjFormStateToSetAdoConStr: Input</Argument>
''' <Argument>vblnFixedSettingNameMode: Input</Argument>
Public Sub SetInitialFormStateToSetAdoConStr(ByVal vobjFormStateToSetAdoConStr As FormStateToSetAdoConStr, _
        Optional ByVal vblnFixedSettingNameMode As Boolean = True)

    Set mobjFormStateToSetAdoConStr = vobjFormStateToSetAdoConStr

    With mobjFormStateToSetAdoConStr
    
        Set mobjCurrentUserInputParametersDic = .UserInputParametersDic
    End With

    mblnFixedSettingNameMode = vblnFixedSettingNameMode
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubLocalize()

    Me.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrOledbMsExcelFormTitle()

    LocalizeUIControlOfAdoConStrInfoForAdoConStrLblConStrCacheName lblConStrCacheName, fraAdoConnectionParameters

    
    msubLocalizeOleDbAceToExcelSheetSettings
    

    cmdAdoConnectionTest.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrCmdAdoConnectionTest()

    ' About fraConnectionStringInfoCachingOption
    LocalizeUIControlOfAdoConStrInfoForAdoConStrFraConnectionStringInfoCachingOption fraConnectionStringInfoCachingOption, chkSaveParametersInRegistoryExceptForPassword, chkSavePasswordTemporaryCache
End Sub

'''
'''
'''
Private Sub msubLocalizeOleDbAceToExcelSheetSettings()

    lblFullPathOfExcelBook.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfExcelBook()

    cmdBrowse.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrCmdBrowse()
    
    lblUsingOleDb.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblUsingOleDb()

    chkReadOnlyAdoConnection.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrChkReadOnlyAdoConnection()

    chkHeaderColumnTitlesExist.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrChkHeaderColumnTitlesExist()

    lblIMEXMode.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblImexMode()

    lblPasswordForExcelBook.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblPasswordForExcelBook()
End Sub


'**---------------------------------------------
'** Update mobjFormStateToSetAdoOdbcConStr from UI
'**---------------------------------------------
'''
'''
'''
Private Sub msubUpdateFormStateToSetAdoConStr()

    Dim objUIInputResultDic As Scripting.Dictionary

    If Not mblnFixedSettingNameMode Then
    
        mobjFormStateToSetAdoConStr.SettingKeyName = GetCurrentSettingKeyNameFromComboBox
    End If
    
    msubUpdateInputDicOfFormState
End Sub

'''
'''
'''
Private Function GetCurrentSettingKeyNameFromComboBox() As String

    GetCurrentSettingKeyNameFromComboBox = GetCurrentValueOfComboBoxWithTextEditable(cboConStrCacheName)
End Function


'''
'''
'''
Private Sub msubUpdateInputDicOfFormState()

    msubUpdateInputDicFromUI mobjCurrentUserInputParametersDic
End Sub


'''
''' Update objUserInputParametersDic from UI
'''
Private Sub msubUpdateInputDicFromUI(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String, enmIMEXMode As IMEXMode, strValue As String

    With robjUserInputParametersDic

        strKey = mstrAdoDbFilePathKey: strValue = txtFullPathOfExcelBook.Text
        
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        strKey = mstrTemporaryPdKey: strValue = txtPasswordForExcelBook.Text
        
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        With cboUsingAccdbOleDb
        
            strKey = "OLE_DB_Provider": strValue = .List(.ListIndex)
        End With

        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        strKey = "HeaderColumnTitleExists":  strValue = CInt(chkHeaderColumnTitlesExist.Value)
        
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        strKey = "BookReadOnly":  strValue = CInt(chkReadOnlyAdoConnection.Value)
        
        If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        
        With cboIMEXMode
        
            strValue = .List(.ListIndex)
            
            enmIMEXMode = GetAdoIMEXModeEnmFromName(strValue)
        End With
        
        strKey = "IMEX"
        
        If enmIMEXMode <> NoUseIMEXMode Then
            
            If .Exists(strKey) Then .Item(strKey) = strValue Else .Add strKey, strValue
        Else
            If .Exists(strKey) Then .Remove strKey
        End If
    End With
End Sub


'**---------------------------------------------
'** Refresh UI
'**---------------------------------------------
'''
'''
'''
Private Sub msubRefresh()

    mintInitializing = mintInitializing + 1

    msubRefreshFixedSettingNameMode

    msubRefreshExcelSheetConnectionParameters mobjCurrentUserInputParametersDic

    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubRefreshFixedSettingNameMode()

    If Not mblnFixedSettingNameMode Then
    
        msubRefreshCurrentSettingKeyAndUserInputParametersDic
    End If
End Sub

'''
'''
'''
Private Sub msubRefreshCurrentSettingKeyAndUserInputParametersDic()

    Set mobjCurrentUserInputParametersDic = GetCurrentUserInputParametersDicFromComboBoxAndCacheDic(cboConStrCacheName, mobjSettingKeyNameToUserInputParametersDic)
End Sub

'''
'''
'''
Private Sub msubRefreshExcelSheetConnectionParameters(ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String

    With robjUserInputParametersDic
    
        If .Exists(mstrAdoDbFilePathKey) Then
        
            txtFullPathOfExcelBook.Text = .Item(mstrAdoDbFilePathKey)
        End If
    
        strKey = "OLE_DB_Provider"
    
        If .Exists(strKey) Then
        
            cboUsingAccdbOleDb.ListIndex = mobjAccessOLEDBProviderTypeToListIndexDic.Item(GetAccessOLEDBProviderEnmFromName(.Item(strKey)))
        End If
    
        strKey = "HeaderColumnTitleExists"
        
        If .Exists(strKey) Then
        
            chkHeaderColumnTitlesExist.Value = CBool(.Item(strKey))
        End If
        
        strKey = "BookReadOnly"
        
        If .Exists(strKey) Then
        
            chkReadOnlyAdoConnection.Value = CBool(.Item(strKey))
        End If
        
        strKey = "IMEX"
        
        If .Exists(strKey) Then
        
            cboIMEXMode.ListIndex = mobjIMEXModeToListIndexDic.Item(GetAdoIMEXModeEnmFromName(.Item(strKey)))
        End If
        
        If .Exists(mstrTemporaryPdKey) Then
        
            txtPasswordForExcelBook.Text = .Item(mstrTemporaryPdKey)
        End If
    End With
End Sub


'''
'''
'''
Private Sub msubClearAll()

    msubClearSettingKeyName
    
    msubClearBookPathAndPassword
End Sub

'''
'''
'''
Private Sub msubClearSettingKeyName()
    
    mintInitializing = mintInitializing + 1
    
    With cboConStrCacheName
    
        .Clear
    End With
    
    mintInitializing = mintInitializing - 1
End Sub


'''
'''
'''
Private Sub msubClearBookPathAndPassword()

    mintInitializing = mintInitializing + 1
    
    txtFullPathOfExcelBook.Text = ""
    
    txtPasswordForExcelBook.Text = ""
    
    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubInitializePasswordTextBox()

    With txtPasswordForExcelBook
    
        .IMEMode = fmIMEModeAlpha
    
        .PasswordChar = "*"
    End With
End Sub


'''
'''
'''
Private Sub msubInitializeFixedSettingNameMode()

    mintInitializing = mintInitializing + 1

    InitializeFixedSettingNameModeForEachRDB mobjSettingKeyNameToUserInputParametersDic, cboConStrCacheName, mobjFormStateToSetAdoConStr, AdoOleDbAndMsExcel, mblnFixedSettingNameMode, cmdOK
    
    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubInitializeCachingConStrParamsOptions()

    mintInitializing = mintInitializing + 1

    chkSaveParametersInRegistoryExceptForPassword.Value = GetAdoConnectionSaveParametersInRegistoryExceptForPassword()

    InitializeSavePasswordTemporaryCacheCheckBoxForMsExcel chkSavePasswordTemporaryCache

    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubInitializeFirstControlFocus()

    mintInitializing = mintInitializing + 1
    
    If txtFullPathOfExcelBook.Text <> "" Then
    
        txtPasswordForExcelBook.SetFocus
    Else
        txtFullPathOfExcelBook.SetFocus
    End If
    
    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub msubCloseFormToCancel()

    mobjFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection = True
    
    cmdOK_Click
End Sub


