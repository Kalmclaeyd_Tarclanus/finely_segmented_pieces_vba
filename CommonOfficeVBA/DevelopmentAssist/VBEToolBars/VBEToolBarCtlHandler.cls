VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "VBEToolBarCtlHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Toolbar VBE click menu
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Toolbar VBE click menu
'
'   Dependency Abstract:
'       Dependent on VBIDE
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 17/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private WithEvents mobjToolBarEvents As VBIDE.CommandBarEvents
Attribute mobjToolBarEvents.VB_VarHelpID = -1
Private mobjToolBarButton As Office.CommandBarButton

Private menmVBEToolBarMenuItemTrType As VBEToolBarMenuItemTrType

Private Const mintFaceID As Long = 272


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub Class_Terminate()

    Set mobjToolBarEvents = Nothing
    
    Set mobjToolBarButton = Nothing
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vobjApplication: Either Excel.Application, Word.Application, or PowerPoint.Application</Argument>
Public Sub AddToolBarButton(ByVal venmVBEToolBarMenuItemTrType As VBEToolBarMenuItemTrType, ByRef robjToolBar As Office.CommandBar, ByVal vobjApplication As Excel.Application)

    Dim strCaption As String

    menmVBEToolBarMenuItemTrType = venmVBEToolBarMenuItemTrType
    
    strCaption = GetCaptionTextFromVBEToolBarMenuItemTrType(venmVBEToolBarMenuItemTrType)
    

    ' Add a menu
    Set mobjToolBarButton = robjToolBar.Controls.Add(Office.MsoControlType.msoControlButton, , , , True)
    
    With mobjToolBarButton
    
        .Caption = strCaption
        
        .TooltipText = strCaption
        
        .FaceID = mintFaceID
    End With
    
    ' Create the command bar events
    
    Set mobjToolBarEvents = vobjApplication.VBE.Events.CommandBarEvents(mobjToolBarButton)
End Sub

'''
'''
'''
Private Sub mobjToolBarEvents_Click(ByVal CommandBarControl As Object, handled As Boolean, CancelDefault As Boolean)

    Debug.Print "called mobjToolBarEvents_Click"

    Select Case menmVBEToolBarMenuItemTrType

        Case VBEToolBarMenuItemTrType.ToolBarResetCodePaneRightClickMenusOnVBE
        
            ResetTrLocalCodePaneLocalCommandBarButtons
        
        Case VBEToolBarMenuItemTrType.ToolBarOpenOpenCodePaneSwitchMultiLstFormForSingleVBProject
        
        
        Case VBEToolBarMenuItemTrType.ToolBarClearImmediateWindow


    End Select

End Sub
