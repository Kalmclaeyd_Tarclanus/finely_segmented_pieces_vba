Attribute VB_Name = "VBEToolBarsSetup"
'
'   Set up VBIDE tool-bars tools
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on VBIDE
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 16/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "VBEToolBarsSetup"


Private Const mstrToolbarTitle As String = "TrToolBar"


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////

Public Enum VBEToolBarMenuItemTrType

    ToolBarResetCodePaneRightClickMenusOnVBE = 1
    
    ToolBarOpenOpenCodePaneSwitchMultiLstFormForSingleVBProject
    
    ToolBarClearImmediateWindow
End Enum


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjToolBarHandler As VBEToolBarCtlHandler


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToVBEToolBarsSetup()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "RClickOnCodePane,RClickCtlCodePaneHandler,VBEToolBarCtlHandler"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Add or Delete toolbar menu items on VBE
'**---------------------------------------------
'''
'''
'''
Public Sub AddTrToolBarToCurrentVBE()

    Dim objApplication As Object '
    Dim objToolBarCtr As VBEToolBarCtlHandler
    
    ' DeleteTrToolBar
    
    Set objApplication = GetCurrentOfficeFileObject().Application
    
    DeleteCommandBarIfItExists objApplication, mstrToolbarTitle
    
    With objApplication.VBE.CommandBars.Add(mstrToolbarTitle, msoBarTop, , True)
    
    'With objApplication.VBE.CommandBars.Add(mstrToolbarTitle, msoBarRight, , True)
        
        .Visible = True

    End With

    Set objToolBarCtr = New VBEToolBarCtlHandler
    
    Set mobjToolBarHandler = objToolBarCtr
    
    With objApplication.VBE
    
        objToolBarCtr.AddToolBarButton ToolBarResetCodePaneRightClickMenusOnVBE, .CommandBars(mstrToolbarTitle), objApplication
    End With
End Sub


'''
'''
'''
Public Function GetCaptionTextFromVBEToolBarMenuItemTrType(ByVal venmVBEToolBarMenuItemTrType As VBEToolBarMenuItemTrType) As String

    Dim strCaption As String

    strCaption = ""
    
    Select Case venmVBEToolBarMenuItemTrType
    
        Case VBEToolBarMenuItemTrType.ToolBarResetCodePaneRightClickMenusOnVBE
        
            strCaption = "Reset the right-click menus on VBE"
            
        Case VBEToolBarMenuItemTrType.ToolBarOpenOpenCodePaneSwitchMultiLstFormForSingleVBProject
        
            strCaption = "Open switch code-pane window"
            
        Case VBEToolBarMenuItemTrType.ToolBarClearImmediateWindow
        
            strCaption = "Clear immediate window"
    End Select
    
    GetCaptionTextFromVBEToolBarMenuItemTrType = strCaption
End Function
