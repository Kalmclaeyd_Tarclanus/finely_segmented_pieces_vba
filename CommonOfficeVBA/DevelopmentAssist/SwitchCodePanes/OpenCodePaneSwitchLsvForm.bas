Attribute VB_Name = "OpenCodePaneSwitchLsvForm"
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       First idea; After the integrated codes are divided into AddIns, show more than one this window and each window shows codes of each VB project.
'       In this case, the idea can keeps the smallest width.
'       Second idea; For all codes searving in all AddIns and Macrobooks, one code-pane switch form shows all.
'       In this case, the list items should be restricted to current opened modules.
'
'   Dependency Abstract:
'       This is dependent on both the MSComctlLib reference and the MSForms reference
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon,  6/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation.
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjCodePaneSwitchSingleLsvForm As CodePaneSwitchSingleLsvForm

Private mobjProjectNameToCodePaneSwitchMultiLsvFormDic As Scripting.Dictionary  ' by ListView



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' This is also a sanity test
'''
Public Sub OpenCodePaneSwitchSingleLsvForm()

    Set mobjCodePaneSwitchSingleLsvForm = New CodePaneSwitchSingleLsvForm

    With mobjCodePaneSwitchSingleLsvForm
        
        .Show vbModeless
    End With
End Sub

'''
'''
'''
Public Sub OpenCodePaneSwitchMultiListViewsFormForAllVBProjectsInCurrentVBE()

    Dim objVBProject As VBIDE.VBProject, objCodePaneSwitchMultiLsvForm As CodePaneSwitchMultiLsvForm
    
    Dim varFileName As Variant, strFileName As String, objFileNameToVBProjectDic As Scripting.Dictionary
    
    Dim objApplication As Object


    If mobjProjectNameToCodePaneSwitchMultiLsvFormDic Is Nothing Then
    
        Set mobjProjectNameToCodePaneSwitchMultiLsvFormDic = New Scripting.Dictionary
    End If
    
    Set objApplication = GetCurrentOfficeFileObject().Application
    
    Set objFileNameToVBProjectDic = GetVBProjectFileNameToVBProjectDic()
    
    
    ' If any VB projects, which no code pane is opened, don't show Form
    
    With GetOpenedIDECodePaneOfFileNameClassifiedTypeNameModuleDic()
    
        For Each varFileName In .Keys
        
            strFileName = varFileName
            
            Set objVBProject = objFileNameToVBProjectDic.Item(strFileName)
        
            Set objCodePaneSwitchMultiLsvForm = New CodePaneSwitchMultiLsvForm
    
            With objCodePaneSwitchMultiLsvForm
                
                .Initialize objVBProject, strFileName, objApplication
            
                .Show vbModeless
            End With
        
            With mobjProjectNameToCodePaneSwitchMultiLsvFormDic
            
                If Not .Exists(objVBProject.Name) Then
                
                    .Add objVBProject.Name, objCodePaneSwitchMultiLsvForm
                Else
                
                    Set .Item(objVBProject.Name) = objCodePaneSwitchMultiLsvForm
                End If
            End With
        Next
    End With
End Sub


