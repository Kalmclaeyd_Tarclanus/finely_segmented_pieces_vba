VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AxCtlLstCodeModulePaneHdl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ActiveX control ListBox code module-pane handler, which is the MSForms.ListBox double-click event handler to open a VBA code pane.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'       Dependent on VBIDE, MSForms.UserForm, MSForms.ListBox
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 29/Aug/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private WithEvents mobjListBox As MSForms.ListBox
Attribute mobjListBox.VB_VarHelpID = -1

Private mobjVBProject As VBIDE.VBProject

Private mstrFileName As String  ' If the office file isn't saved at least one, then the VBProject.fileName returns null string. this prevents such causing error

'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub Class_Terminate()

    Set mobjListBox = Nothing
    
    Set mobjVBProject = Nothing
End Sub


Private Sub mobjListBox_DblClick(ByVal Cancel As MSForms.ReturnBoolean)

    'Debug.Print "ListBox double-clicked"
    
    msubShowLatestOneVBAModuleCodePane
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
Public Sub Initialize(ByVal vobjListBox As MSForms.ListBox, _
        ByVal vobjVBProject As VBIDE.VBProject, _
        ByRef rstrFileName As String)


    Set mobjListBox = vobjListBox
    
    Set mobjVBProject = vobjVBProject
    
    mstrFileName = rstrFileName
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
Private Sub msubShowVBAModuleCodePaneOfSelectedItems()

    ShowIDECodePaneFromPluralModuleNamesAndVBProjectObject mfobjGetSelectedItems(), _
            mobjVBProject, _
            mstrFileName
End Sub

Private Sub msubShowLatestOneVBAModuleCodePane()

    With mobjListBox
    
        ShowIDECodePaneFromModuleNameAndVBProjectObject .List(.ListIndex), _
                mobjVBProject, _
                mstrFileName
    End With
End Sub


Private Function mfobjGetSelectedItems() As Collection

    Dim i As Long, objCol As Collection, strText As String

    Set objCol = New Collection

    With mobjListBox
    
        For i = 0 To .ListCount - 1
    
            If .Selected(i) Then
            
                strText = .List(i)
                
                objCol.Add strText
            End If
        Next
    
    End With

    Set mfobjGetSelectedItems = objCol
End Function
