VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AxCtlLstRClickCodePaneMenuHdl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   AxtiveX Control ListBox Right-Click menu handler
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is dependent on the MSForms reference
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  2/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private WithEvents mobjListBox As MSForms.ListBox
Attribute mobjListBox.VB_VarHelpID = -1

Private mobjKeyToAxBarBtnEvHdlForSwitchLstPaneDic As Scripting.Dictionary

Private mobjApplication As Object    ' Excel.Application, Word.Application, or PowerPoint.Application

Private mstrCommandBarName As String

Private mobjVBProject As VBIDE.VBProject

Private mstrFileName As String

Private mobjListBoxes As Collection

Private mobjParentForm As Object

'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mstrCommandBarName = "UserDefinedAdditionalBar"

    Set mobjKeyToAxBarBtnEvHdlForSwitchLstPaneDic = New Scripting.Dictionary
End Sub

Private Sub Class_Terminate()

    If Not mobjKeyToAxBarBtnEvHdlForSwitchLstPaneDic Is Nothing Then
    
        mobjKeyToAxBarBtnEvHdlForSwitchLstPaneDic.RemoveAll
    End If
    
    Set mobjKeyToAxBarBtnEvHdlForSwitchLstPaneDic = Nothing
End Sub

'''
''' Right click event handler
'''
Private Sub mobjListBox_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal x As Single, ByVal y As Single)

    Dim i As Long
    Dim objAxBarBtnEvHdlForSwitchLstPane As AxBarBtnEvHdlForSwitchLstPane, objIndexToRClickSwitchCodePaneMenuTypeDic As Scripting.Dictionary, varIndex As Variant
    Dim enmRClickSwitchCodePaneMenuType As RClickSwitchCodePaneMenuType
    
    If Button <> 2 Then
    
        Exit Sub
    End If

    Set objIndexToRClickSwitchCodePaneMenuTypeDic = New Scripting.Dictionary
    
    With objIndexToRClickSwitchCodePaneMenuTypeDic
    
        .Add 0, RClickSwitchCodePaneMenuType.TileSelectedWindowsHorizontallyMenu
        .Add 1, RClickSwitchCodePaneMenuType.CascadeCodePanesMenu
        .Add 2, RClickSwitchCodePaneMenuType.TileSelectedBothCodesAndUFDesignerHorizontallyMenu
        .Add 3, RClickSwitchCodePaneMenuType.CloseSelectedCodePanesMenu
    End With

    If mobjKeyToAxBarBtnEvHdlForSwitchLstPaneDic Is Nothing Then Set mobjKeyToAxBarBtnEvHdlForSwitchLstPaneDic = New Scripting.Dictionary

    DeleteCommandBarIfItExists mobjApplication, mstrCommandBarName

    With mobjApplication.CommandBars.Add(Name:=mstrCommandBarName, Position:=Office.MsoBarPosition.msoBarPopup)
     
        For Each varIndex In objIndexToRClickSwitchCodePaneMenuTypeDic.Keys
        
            enmRClickSwitchCodePaneMenuType = objIndexToRClickSwitchCodePaneMenuTypeDic.Item(varIndex)
       
            i = varIndex
        
            Set objAxBarBtnEvHdlForSwitchLstPane = New AxBarBtnEvHdlForSwitchLstPane
        
            objAxBarBtnEvHdlForSwitchLstPane.Initialize i, .Controls.Add(Office.MsoControlType.msoControlButton), enmRClickSwitchCodePaneMenuType, mobjListBoxes, mobjVBProject, mstrFileName, mobjParentForm
        
            msubSetUpKeyToAxBarBtnEvHdlForSwitchLstPaneDic i, objAxBarBtnEvHdlForSwitchLstPane
        Next
        
        .ShowPopup
        
        .Delete
    End With

End Sub




Private Sub msubSetUpKeyToAxBarBtnEvHdlForSwitchLstPaneDic(ByVal vintIndex As Long, ByRef robjAxBarBtnEvHdlForSwitchLstPane As AxBarBtnEvHdlForSwitchLstPane)

    With mobjKeyToAxBarBtnEvHdlForSwitchLstPaneDic
        
        If Not .Exists(vintIndex) Then
        
            .Add vintIndex, robjAxBarBtnEvHdlForSwitchLstPane
        Else
            Set .Item(vintIndex) = robjAxBarBtnEvHdlForSwitchLstPane
        End If
    End With
End Sub



'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Property Get ListBoxControl() As MSForms.ListBox

    Set ListBoxControl = mobjListBox
End Property


Public Property Get CommandBarName() As String
    CommandBarName = mstrCommandBarName
End Property



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
Public Sub Initialize(ByRef robjListBox As MSForms.ListBox, ByVal vobjApplication As Object, ByVal vobjVBProject As VBIDE.VBProject, ByVal vstrFileName As String, ByVal vobjListBoxes As Collection, ByVal vobjParentForm As Object, Optional ByVal vstrCommandBarName As String = "")

    Set mobjListBox = robjListBox
    
    Set mobjApplication = vobjApplication
    
    Set mobjVBProject = vobjVBProject
    
    mstrFileName = vstrFileName
    
    Set mobjListBoxes = vobjListBoxes
    
    Set mobjParentForm = vobjParentForm
    
    If vstrCommandBarName <> "" Then
    
        mstrCommandBarName = vstrCommandBarName
    End If
End Sub



