Attribute VB_Name = "OperateVBIDEWindowCommon"
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on VBIDE
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 20/Feb/2023    Kalmclaeyd Tarclanus    Separated from OperateVBIDECodePanes.bas
'

Option Explicit


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
'''
'''
Public Enum VBCodePaneType

    OnlyVBCodeAtWindow
    
    UserFormDesignerAtWindow
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' Detect the both the code-pane and the User form designer, when the User form module is selected
'''
''' <Argument>rstrModuleName: Output</Argument>
''' <Argument>rstrOfficeFileName: Output</Argument>
''' <Argument>renmVBCodePaneType: Output</Argument>
''' <Argument>vstrWindowCaption: Input</Argument>
''' <Argument>robjVBProjectToVBProjectFileNameDic: Input</Argument>
Public Sub GetModuleNameAndOfficeFileNameAndVBAWindowPaneTypeFromVBProjects(ByRef rstrModuleName As String, _
        ByRef rstrOfficeFileName As String, _
        ByRef renmVBCodePaneType As VBCodePaneType, _
        ByVal vstrWindowCaption As String, _
        ByRef robjVBProjectToVBProjectFileNameDic As Scripting.Dictionary)

    Dim strWindowType As String, intHyphenPos As Long, strCaptionArray() As String, strModuleNameWithParen As String
    
    Dim intParenPos As Long, blnIsUnknown As Boolean, varVBProject As Variant, objVBProject As VBIDE.VBProject, objVBComponent As VBIDE.VBComponent


    blnIsUnknown = True

    intParenPos = InStr(1, vstrWindowCaption, "(")
    
    If intParenPos > 0 Then
    
        strWindowType = Trim(Mid(vstrWindowCaption, intParenPos + 1, Len(vstrWindowCaption) - intParenPos - 1))
    
        intHyphenPos = InStr(1, vstrWindowCaption, "-")
    
        If intHyphenPos > 0 Then
    
            strCaptionArray = Split(vstrWindowCaption, "-")
                        
            rstrOfficeFileName = Trim(strCaptionArray(0))
            
            strModuleNameWithParen = Trim(strCaptionArray(1))
            
            'intParenPos = InStr(1, strModuleNameWithParen, "(")
            
            rstrModuleName = Trim(Left(strModuleNameWithParen, InStr(1, strModuleNameWithParen, "(") - 1))
        Else
            rstrModuleName = Trim(Left(vstrWindowCaption, intParenPos - 1))
    
            
            With robjVBProjectToVBProjectFileNameDic
            
                For Each varVBProject In .Keys
            
                    Set objVBProject = varVBProject
            
                    With objVBProject
                    
                        Set objVBComponent = Nothing
                        
                        On Error Resume Next
                    
                        Set objVBComponent = .VBComponents.Item(rstrModuleName)
                        
                        On Error GoTo 0
                    End With
                    
                    If Not objVBComponent Is Nothing Then
                    
                        rstrOfficeFileName = .Item(varVBProject)
                        
                        Exit For
                    End If
                Next
            End With
        End If
    
        Select Case strWindowType
        
            Case "UserForm"
                ' UserForm desinger
            
                renmVBCodePaneType = UserFormDesignerAtWindow
            
            Case GetTextOfStrKeyOperateVbideCodePanesCode()
                ' Code pane
            
                renmVBCodePaneType = OnlyVBCodeAtWindow
            
            Case Else
            
                blnIsUnknown = True
                
                Debug.Assert False
            
        End Select
    Else
        rstrModuleName = ""
        
        rstrOfficeFileName = ""
    End If
End Sub


'''
''' Extract the both of the module name and office file name including macro-programs from parsing the window title text of the CodePane object
'''
''' <Argument>rstrModuleName: output - module name</Argument>
''' <Argument>rstrOfficeFileName: output - office file name, which kind of file is either Excel, Word, or PowerPoint</Argument>
''' <Argument>vstrWindowCaption: input - CodePane window title text</Argument>
Public Sub GetModuleNameAndOfficeFileNameFromVBIDECodePane(ByRef rstrModuleName As String, _
        ByRef rstrOfficeFileName As String, _
        ByVal vstrWindowCaption As String, ByRef robjVBE As VBIDE.VBE)

    Dim strCaptionArray() As String, strModuleNameWithParen As String, strWindowType As String
    
    Dim intParenPos As Long, intHyphenPos As Long, objVBProject As VBIDE.VBProject, objVBComponent As VBIDE.VBComponent
    
    
    intHyphenPos = InStr(1, vstrWindowCaption, "-")

    If intHyphenPos > 0 Then
    
        strCaptionArray = Split(vstrWindowCaption, "-")
                    
        rstrOfficeFileName = Trim(strCaptionArray(0))
        
        strModuleNameWithParen = Trim(strCaptionArray(1))
        
        intParenPos = InStr(1, strModuleNameWithParen, "(")
        
        If intParenPos > 0 Then
        
            rstrModuleName = Trim(Left(strModuleNameWithParen, intParenPos - 1))
            
            'strWindowType = Trim(Mid(strModuleNameWithParen, intParenPos + 1, Len(strModuleNameWithParen) - intParenPos - 1))
            
            'Debug.Print strWindowType
        Else
            rstrModuleName = strModuleNameWithParen
        End If
    Else
    
        strModuleNameWithParen = vstrWindowCaption
    
        intParenPos = InStr(1, strModuleNameWithParen, "(")
        
        If intParenPos > 0 Then
        
            rstrModuleName = Trim(Left(strModuleNameWithParen, intParenPos - 1))
        Else
            rstrModuleName = strModuleNameWithParen
        End If
        
        For Each objVBProject In robjVBE.VBProjects
        
            If IsVBProjectSavedAtLeastOne(objVBProject) Then
            
                With objVBProject
                
                    Set objVBComponent = Nothing
                    
                    On Error Resume Next
                
                    Set objVBComponent = .VBComponents.Item(rstrModuleName)
                    
                    On Error GoTo 0
                    
                    If Not objVBComponent Is Nothing Then
                    
                        rstrOfficeFileName = GetFileNameWithoutUsingFileSystemObject(.fileName)
                        
                        Exit For
                    End If
                End With
            End If
        Next
    End If
End Sub


'''
'''
'''
Public Function IsVBProjectSavedAtLeastOne(ByRef robjVBProject As VBIDE.VBProject) As Boolean

    Dim strPath As String, blnIsSaved As Boolean
    
    strPath = ""
    
    blnIsSaved = False
        
    On Error Resume Next
    
    ' the fileName property returns void string when the new office file is not saved yet.
    
    strPath = robjVBProject.fileName

    On Error GoTo 0
    
    If strPath <> "" Then
    
        blnIsSaved = True
    End If
    
    IsVBProjectSavedAtLeastOne = blnIsSaved
End Function

'**---------------------------------------------
'** Adjust both the size and the position of Contols specially, for CodePaneSwitchMultiLsvForm, CodePaneSwitchMultiLstForm
'**---------------------------------------------
'''
'''
'''
''' <Argument>robjOrderToLabelControlDic: Input</Argument>
''' <Argument>robjOrderToListControlDic: Input</Argument>
''' <Argument>robjComponentTypeToCountOfModulesDic: Input</Argument>
''' <Argument>robjForm: Input</Argument>
''' <Argument>vblnIsListViewControl: Input</Argument>
Public Sub AdjustListControlsForSwitchCodePanesForm(ByRef robjOrderToLabelControlDic As Scripting.Dictionary, _
        ByRef robjOrderToListControlDic As Scripting.Dictionary, _
        ByRef robjComponentTypeToCountOfModulesDic As Scripting.Dictionary, _
        ByRef robjForm As Object, _
        Optional ByVal vblnIsListViewControl As Boolean = True)


    Dim enmComponentType As VBIDE.vbext_ComponentType, varComponentType As Variant
    Dim objLabelControl As Object, objListControl As Object
    
    Dim intLabelTop As Long, intListTop As Long, i As Long, intLabelLeft As Long, intListLeft As Long, intCurrentHeight As Long, intCountOfListItems As Long
    Dim intMaximumOfItemsOfAdjustingHeightRange As Long
    
    Const intSpace As Long = 6
    
    
    intLabelLeft = 9
    
    intListLeft = 6
    
    intMaximumOfItemsOfAdjustingHeightRange = 7
    
    With robjOrderToLabelControlDic
    
        intLabelTop = 6
        
        intListTop = 18
    
        For Each varComponentType In .Keys
        
            enmComponentType = varComponentType
        
            Set objLabelControl = .Item(enmComponentType)
            
            Set objListControl = robjOrderToListControlDic.Item(enmComponentType)
        
            intCountOfListItems = robjComponentTypeToCountOfModulesDic.Item(enmComponentType)
            
            If InStr(1, objListControl.Name, "Standard") > 0 Then
            
                intMaximumOfItemsOfAdjustingHeightRange = 14
            Else
                intMaximumOfItemsOfAdjustingHeightRange = 17 ' 7
            End If
            
            'Sleep 100
            
            AdjustHeightByCountOfItemsWithFixedMinimum objListControl, intCountOfListItems, intMaximumOfItemsOfAdjustingHeightRange, vblnIsListViewControl
        
            With objListControl
            
                'Debug.Print .Name & ", Visible state : " & CStr(.Visible)
            
                '.Visible = True
            
                intCurrentHeight = .Height
            
        
                ' Avoid the issue among MSForms and MSComctlLib.ListView
                robjForm.AdjustListControlPos objListControl, intListLeft, intListTop

'                Of course, the following code is clearly simple. but via Object type interface, the ListControl, which is either ListView or ListBox, position updating problem often occur....
'
'                .Top = intListTop ' -> this is not updated sometimes.
'
'                'Interaction.DoEvents
'
'                .Left = intListLeft ' -> this is not updated sometimes.
'
'                Interaction.DoEvents
            End With
            
            With objLabelControl
            
                intCurrentHeight = intCurrentHeight + .Height
            
                .Top = intLabelTop: .Left = intLabelLeft
            End With
        
            intListTop = intListTop + intCurrentHeight + intSpace
            
            intLabelTop = intLabelTop + intCurrentHeight + intSpace
        Next
    End With
End Sub


'''
'''
'''
Public Sub GetTwoTypeDicsAboutClassifiedTypeToModuleNameAndComponentTypeToCountOfModules(ByRef robjClassifiedTypeToModuleNameDic As Scripting.Dictionary, _
        ByRef robjComponentTypeToCountOfModulesDic As Scripting.Dictionary, _
        ByVal vstrFileName As String)


    Dim objOpenedFileNameClassifiedTypeModuleDic As Scripting.Dictionary

    Set objOpenedFileNameClassifiedTypeModuleDic = GetOpenedIDECodePaneOfFileNameClassifiedTypeModuleDic()

    With objOpenedFileNameClassifiedTypeModuleDic
    
        If .Exists(vstrFileName) Then
        
            Set robjClassifiedTypeToModuleNameDic = .Item(vstrFileName)
        Else
            Set robjClassifiedTypeToModuleNameDic = Nothing
        End If
    End With
    
    Set robjComponentTypeToCountOfModulesDic = GetComponentTypeToCountOfModulesDic(robjClassifiedTypeToModuleNameDic)
End Sub



