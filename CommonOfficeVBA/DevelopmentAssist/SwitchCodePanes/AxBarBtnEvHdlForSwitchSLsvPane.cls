VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AxBarBtnEvHdlForSwitchSLsvPane"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ActiveX CommandBarButton control Event handler for switching CodePane by a single ListView, which is the MSComctlLib.ListView right-click event handler to open a VBA code pane.
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'       Dependent on VBIDE, MSForms.UserForm, MSComctlLib.ListView
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  7/Sun/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private WithEvents mobjCommandButton As Office.CommandBarButton
Attribute mobjCommandButton.VB_VarHelpID = -1

Private mintIndex As Long

Private menmRClickSwitchCodePaneMenuType As RClickSwitchCodePaneMenuType

Private mobjListView As MSComctlLib.ListView

Private mobjParentForm As Object


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub mobjCommandButton_Click(ByVal Ctrl As Office.CommandBarButton, CancelDefault As Boolean)

    Select Case menmRClickSwitchCodePaneMenuType
    
        Case RClickSwitchCodePaneMenuType.TileSelectedWindowsHorizontallyMenu
        
            
            TileSelectedWindowsHorizontallyWithClosingBothObjectBrowserAndUFDesigner GetAllSelectedModuleNameAndFileNameOfListViews(mobjListView)
        
        Case RClickSwitchCodePaneMenuType.CascadeCodePanesMenu
    
            CascadeCodePanesAfterNomalizeAllPanes
        
        Case RClickSwitchCodePaneMenuType.TileSelectedBothCodesAndUFDesignerHorizontallyMenu
        
            TileSelectedBothCodePanesAndUFDeignersHorizontallyWithClosingObjectBrowser GetAllSelectedModuleNameAndFileNameOfListViews(mobjListView)
        
        Case RClickSwitchCodePaneMenuType.CloseSelectedCodePanesMenu
        
            CloseSelectedBothCodePanesAndUserFormDesigners GetAllSelectedModuleNameAndFileNameOfListViews(mobjListView)
            
            'CloseSelectedCodePanes GetAllSelectedModuleNameAndFileNameOfListViews(mobjListView)
            
            mobjParentForm.RefreshOpenedModuleNames
    End Select
End Sub


Private Sub Class_Terminate()

    Set mobjCommandButton = Nothing
    
    Set mobjListView = Nothing
    
    Set mobjParentForm = Nothing
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub Initialize(ByVal vintIndex As Long, ByRef robjCommandButton As Office.CommandBarButton, ByVal venmRClickSwitchCodePaneMenuType As RClickSwitchCodePaneMenuType, ByVal vobjListView As MSComctlLib.ListView, ByVal vobjParentForm As Object)
 
    mintIndex = vintIndex
 
    Set mobjCommandButton = robjCommandButton
    
    menmRClickSwitchCodePaneMenuType = venmRClickSwitchCodePaneMenuType
    
    Set mobjListView = vobjListView
    
    SetUpRClickCommandButtonCaptionsFromRClickSwitchCodePaneMenuType robjCommandButton, menmRClickSwitchCodePaneMenuType
    
    Set mobjParentForm = vobjParentForm
End Sub




