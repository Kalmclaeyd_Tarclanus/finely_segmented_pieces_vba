Attribute VB_Name = "CodePaneSwitchLsvUtilities"
'
'   Utilities for CodePaneSwitchMultiLsvForm using ListView
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on VBIDE, MSForms.UserForm, MSComctlLib.ListView
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat,  4/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub InitializeProcedureModuleNameListView(ByRef rlsvNames As MSComctlLib.ListView)

    Dim strColumnName As String
    
    InitalizeListViewForDataTableView rlsvNames
    
    strColumnName = "Module name"
    
    With rlsvNames.ColumnHeaders
    
        .Clear
        
        .Add key:=strColumnName, Text:=strColumnName, Width:=132
    End With
End Sub


Private Sub RefreshProcedureModuleNameListViewByCol(ByRef rlsvNames As MSComctlLib.ListView, ByVal vobjNames As Collection)

    Dim varName As Variant, strName As String

    With rlsvNames
    
        .ListItems.Clear
        
        For Each varName In vobjNames
    
            strName = varName
    
            .ListItems.Add key:=strName, Text:=strName
        Next
    End With
End Sub


'''
'''
'''
Public Sub RefreshEachModuleTypeListViewesFromClassifiedTypeToModuleNameDic(ByRef rlsvProcedureNames As MSComctlLib.ListView, ByRef rlsvClassNames As MSComctlLib.ListView, ByRef rlsvFormNames As MSComctlLib.ListView, ByRef rlsvObjectModuleNames As MSComctlLib.ListView, ByVal vobjClassifiedTypeToModuleNameDic As Scripting.Dictionary)

    Dim enmComponentType As GeneralVBComponentType, varComponentType As Variant
    Dim objObjectModuleNames As Collection


    With vobjClassifiedTypeToModuleNameDic
        
        For Each varComponentType In .Keys

            enmComponentType = varComponentType

            Select Case enmComponentType
            
                Case GeneralVBComponentType.GStdModule
                
                    RefreshProcedureModuleNameListViewByCol rlsvProcedureNames, .Item(enmComponentType)
                
                Case GeneralVBComponentType.GClassModule
                
                    RefreshProcedureModuleNameListViewByCol rlsvClassNames, .Item(enmComponentType)
                    
                Case GeneralVBComponentType.GMSForm
                
                    RefreshProcedureModuleNameListViewByCol rlsvFormNames, .Item(enmComponentType)
                
                Case GeneralVBComponentType.GVbaObjectModule, GeneralVBComponentType.GVbaBookObjectModule, GeneralVBComponentType.GVbaSheetObjectModule
            
                    If objObjectModuleNames Is Nothing Then Set objObjectModuleNames = New Collection
                    
                    UnionDoubleCollectionsToSingle objObjectModuleNames, .Item(enmComponentType)
            End Select

        Next
    End With
    
    If Not objObjectModuleNames Is Nothing Then
    
        RefreshProcedureModuleNameListViewByCol rlsvObjectModuleNames, objObjectModuleNames
    End If
    
End Sub

