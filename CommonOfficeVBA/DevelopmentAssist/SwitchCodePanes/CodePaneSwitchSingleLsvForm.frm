VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} CodePaneSwitchSingleLsvForm 
   Caption         =   "CodePaneSwitchLsvForm"
   ClientHeight    =   2740
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   1930
   OleObjectBlob   =   "CodePaneSwitchSingleLsvForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "CodePaneSwitchSingleLsvForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   VB project code-panes selecting form by a single ListView control
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'       Dependent on VBIDE, MSForms.UserForm, MSComctlLib.ListView
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri,  3/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "CodePaneSwitchSingleLsvForm"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mstrCurrentVBProjectName As String

'**---------------------------------------------
'** Semaphore parameter cache within user-control objects initializing
'**---------------------------------------------
Private mblnIsInitialized As Boolean

'**---------------------------------------------
'** Form layout-size cache
'**---------------------------------------------
Private mudtListViewDefaultSize As ResizingAxControlSize

'**---------------------------------------------
'** Right click menu control cache
'**---------------------------------------------
Private mobjAxCtlSLsvRClickCodePaneMenuHdl As AxCtlSLsvRClickCodePaneMenuHdl


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub UserForm_Initialize()

    msubInitializeCodePaneSwitchListView
    
    mblnIsInitialized = False
    
    mstrCurrentVBProjectName = GetCurrentVBProjectName()
End Sub

Private Sub UserForm_Terminate()

    Set mobjAxCtlSLsvRClickCodePaneMenuHdl = Nothing
End Sub

Private Sub UserForm_Layout()

    'Debug.Print "called Layout event handler"
    If Not mblnIsInitialized Then
    
        msubInitializeListViewRightClickMenuHandlers
    
        GetCurrentFormPositionFromUserReg mstrModuleName, Me
        
        GetCurrentFormSizeFromUserReg mstrModuleName, Me

        msubLocalize
        
        mblnIsInitialized = True
    End If


    msubRefreshCodePaneSwitchListView
    
    ModifyFormStyle Me
End Sub

'''
'''
'''
Private Sub UserForm_Resize()

    AdjustControlSizeAlongWithFormBottomRight mudtListViewDefaultSize, Me, lsvVBModuleCodePanes
End Sub

Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)

    SetCurrentFormPositionToUserReg mstrModuleName, Me
    
    SetCurrentFormSizeToUserReg mstrModuleName, Me
End Sub


Private Sub UserForm_DblClick(ByVal Cancel As MSForms.ReturnBoolean)

    msubRefreshCodePaneSwitchListView
    
    GetCurrentOfficeFileObject().Application.VBE.MainWindow.SetFocus
End Sub


'''
'''
'''
Private Sub lsvVBModuleCodePanes_DblClick()

    Dim strModuleName As String, strFileName As String, objDic As Scripting.Dictionary
    
    msubGetModuleNameAndFileNameFromSingleLsvView strModuleName, strFileName
    
    Set objDic = GetTextToTextDicFromLineDelimitedChar(strModuleName & "," & strFileName)
    
    ShowIDECodePaneFromModuleNameToFileNameDic objDic
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' Interface
'''
Public Sub RefreshOpenedModuleNames()

    msubRefreshCodePaneSwitchListView
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubInitializeListViewRightClickMenuHandlers()

    Set mobjAxCtlSLsvRClickCodePaneMenuHdl = New AxCtlSLsvRClickCodePaneMenuHdl

    mobjAxCtlSLsvRClickCodePaneMenuHdl.Initialize lsvVBModuleCodePanes, _
            GetCurrentOfficeFileObject().Application, _
            Me, _
            mstrCurrentVBProjectName & "SingleListViewModulesCommandBar"
End Sub


Private Sub msubLocalize()

    Me.Caption = GetTextOfStrKeyCodePaneSwitchListUtilitiesFormCaptionSwitchCodePanes()
End Sub


'''
'''
'''
Private Sub msubGetModuleNameAndFileNameFromSingleLsvView(ByRef rstrModuleName As String, ByRef rstrFileName As String)

    Dim objRowCol As Collection
    
    Set objRowCol = GetSingleDataTableColFromCurrentSelectedListView(lsvVBModuleCodePanes).Item(1)

    rstrModuleName = objRowCol.Item(1)
    
    rstrFileName = objRowCol.Item(4)
End Sub


'''
'''
'''
Private Sub msubRefreshCodePaneSwitchListView()

    RefreshListViewRecordsForDataTableCol lsvVBModuleCodePanes, GetDataTableColOfOpenedIDECodePaneModuleClassifiedTypeVBProjectInfo()
End Sub


'''
'''
'''
Private Sub msubInitializeCodePaneSwitchListView()

    InitalizeListViewForDataTableView lsvVBModuleCodePanes

    GetDefaultResizingAxControlSize mudtListViewDefaultSize, Me, lsvVBModuleCodePanes
    
    msubInitiazlieListViewColumns
End Sub

'''
'''
'''
Private Sub msubInitiazlieListViewColumns()

    Dim objFieldTitlesCol As Collection, objFieldTitleToColumnWidthDic As Scripting.Dictionary
    
    GetFieldTitlesLsvInfoForModuleClassifiedTypeVBProject objFieldTitlesCol, objFieldTitleToColumnWidthDic

    RefreshListViewFieldsForDataTableCol lsvVBModuleCodePanes, objFieldTitlesCol, objFieldTitleToColumnWidthDic
End Sub


Private Function GetCurrentVBProjectName() As String

    GetCurrentVBProjectName = GetCurrentOfficeFileObject().VBProject.Name
End Function

