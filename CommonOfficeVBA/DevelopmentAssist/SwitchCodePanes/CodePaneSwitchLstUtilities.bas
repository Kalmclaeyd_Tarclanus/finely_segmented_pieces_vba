Attribute VB_Name = "CodePaneSwitchLstUtilities"
'
'   Utilities for CodePaneSwitchMultiLstForm
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'       Dependent on VBIDE, MSForms.UserForm, MSForms.ListBox
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 29/Aug/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private Const mstrModuleName As String = "CodePaneSwitchLstUtilities"

'**---------------------------------------------
'** Key-Value cache preparation for CodePaneSwitchLstUtilities
'**---------------------------------------------
Private mobjStrKeyValueCodePaneSwitchLstUtilitiesDic As Scripting.Dictionary

Private mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub LocalizeLabelForVBAModuleSwitchForm(ByRef rlblProcedureNames As MSForms.Label, ByRef rlblClassNames As MSForms.Label, ByRef rlblFormNames As MSForms.Label, ByRef rlblObjectModuleNames As MSForms.Label)

    rlblProcedureNames.Caption = GetTextOfStrKeyCodePaneSwitchListUtilitiesLblStandardModules()
    rlblClassNames.Caption = GetTextOfStrKeyCodePaneSwitchListUtilitiesLblClassModules()
    rlblFormNames.Caption = GetTextOfStrKeyCodePaneSwitchListUtilitiesLblUserFormModules()
    rlblObjectModuleNames.Caption = GetTextOfStrKeyCodePaneSwitchListUtilitiesLblObjectModules()
End Sub



Public Sub InitializeListBoxForModuleNames(ByRef rlstNames As MSForms.ListBox)

    With rlstNames
    
        .MultiSelect = MSForms.fmMultiSelect.fmMultiSelectMulti
    End With
End Sub


Private Sub RefreshProcedureModuleNameListBoxByCol(ByRef rlstNames As MSForms.ListBox, ByVal vobjNames As Collection)

    Dim varName As Variant, strName As String

    With rlstNames
    
        .Clear
        
        For Each varName In vobjNames
    
            strName = varName
    
            .AddItem strName
        Next
    End With
End Sub


'''
'''
'''
Public Sub RefreshEachModuleTypeListBoxesFromClassifiedTypeToModuleNameDic(ByRef rlstProcedureNames As MSForms.ListBox, ByRef rlstClassNames As MSForms.ListBox, ByRef rlstFormNames As MSForms.ListBox, ByRef rlstObjectModuleNames As MSForms.ListBox, ByVal vobjClassifiedTypeToModuleNameDic As Scripting.Dictionary)

    Dim enmComponentType As GeneralVBComponentType, varComponentType As Variant
    
    Dim objObjectModuleNames As Collection

    With vobjClassifiedTypeToModuleNameDic
        
        For Each varComponentType In .Keys

            enmComponentType = varComponentType

            Select Case enmComponentType
            
                Case GeneralVBComponentType.GStdModule
                
                    RefreshProcedureModuleNameListBoxByCol rlstProcedureNames, .Item(enmComponentType)
                
                Case GeneralVBComponentType.GClassModule
                
                    RefreshProcedureModuleNameListBoxByCol rlstClassNames, .Item(enmComponentType)
                    
                Case GeneralVBComponentType.GMSForm
                
                    RefreshProcedureModuleNameListBoxByCol rlstFormNames, .Item(enmComponentType)
                
                Case GeneralVBComponentType.GVbaObjectModule, GeneralVBComponentType.GVbaBookObjectModule, GeneralVBComponentType.GVbaSheetObjectModule
            
                    If objObjectModuleNames Is Nothing Then Set objObjectModuleNames = New Collection
                    
                    UnionDoubleCollectionsToSingle objObjectModuleNames, .Item(enmComponentType)
            End Select

        Next
    End With
    
    If Not objObjectModuleNames Is Nothing Then
    
        RefreshProcedureModuleNameListBoxByCol rlstObjectModuleNames, objObjectModuleNames
    End If
    
End Sub


'**---------------------------------------------
'** Key-Value cache preparation for CodePaneSwitchLstUtilities
'**---------------------------------------------
'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_STANDARD_MODULES
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Private Function GetTextOfStrKeyCodePaneSwitchListUtilitiesLblStandardModules() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesLblStandardModules = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_STANDARD_MODULES")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_CLASS_MODULES
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Private Function GetTextOfStrKeyCodePaneSwitchListUtilitiesLblClassModules() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesLblClassModules = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_CLASS_MODULES")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_USER_FORM_MODULES
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Private Function GetTextOfStrKeyCodePaneSwitchListUtilitiesLblUserFormModules() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesLblUserFormModules = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_USER_FORM_MODULES")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_OBJECT_MODULES
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Private Function GetTextOfStrKeyCodePaneSwitchListUtilitiesLblObjectModules() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesLblObjectModules = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_OBJECT_MODULES")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_FORM_CAPTION_SWITCH_CODE_PANES
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Public Function GetTextOfStrKeyCodePaneSwitchListUtilitiesFormCaptionSwitchCodePanes() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesFormCaptionSwitchCodePanes = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_FORM_CAPTION_SWITCH_CODE_PANES")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_TILE_SELECTED_PANES
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Public Function GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionTileSelectedPanes() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionTileSelectedPanes = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_TILE_SELECTED_PANES")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_TILE_SELECTED_PANES
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Public Function GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextTileSelectedPanes() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextTileSelectedPanes = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_TILE_SELECTED_PANES")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_CASCADE_ALL_CODE_PANES
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Public Function GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionCascadeAllCodePanes() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionCascadeAllCodePanes = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_CASCADE_ALL_CODE_PANES")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_CASCADE_ALL_CODE_PANES
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Public Function GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextCascadeAllCodePanes() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextCascadeAllCodePanes = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_CASCADE_ALL_CODE_PANES")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_CLOSE_SELECTED_PANES
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Public Function GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionCloseSelectedPanes() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionCloseSelectedPanes = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_CLOSE_SELECTED_PANES")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_CLOSE_SELECTED_PANES
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Public Function GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextCloseSelectedPanes() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextCloseSelectedPanes = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_CLOSE_SELECTED_PANES")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_TILE_BOTH_PANES_AND_UFDESIGNER
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Public Function GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionTileBothPanesAndUfdesigner() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionTileBothPanesAndUfdesigner = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_TILE_BOTH_PANES_AND_UFDESIGNER")
End Function

'''
''' get string Value from STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_TILE_BOTH_PANES_AND_UFDESIGNER
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Public Function GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextTileBothPanesAndUfdesigner() As String

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        msubInitializeTextForCodePaneSwitchLstUtilities
    End If

    GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextTileBothPanesAndUfdesigner = mobjStrKeyValueCodePaneSwitchLstUtilitiesDic.Item("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_TILE_BOTH_PANES_AND_UFDESIGNER")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Private Sub msubInitializeTextForCodePaneSwitchLstUtilities()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForCodePaneSwitchLstUtilitiesByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForCodePaneSwitchLstUtilitiesByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueCodePaneSwitchLstUtilitiesDicInitialized = True
    End If

    Set mobjStrKeyValueCodePaneSwitchLstUtilitiesDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for CodePaneSwitchLstUtilities key-values cache
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Private Sub AddStringKeyValueForCodePaneSwitchLstUtilitiesByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_STANDARD_MODULES", "Standard modules"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_CLASS_MODULES", "Classes"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_USER_FORM_MODULES", "User forms"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_OBJECT_MODULES", "Object modules"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_FORM_CAPTION_SWITCH_CODE_PANES", "Switch code panes"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_TILE_SELECTED_PANES", "Tiles selected code-panes"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_TILE_SELECTED_PANES", "Tiles selected code-panes horizontally with closing UserForm desinger windows"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_CASCADE_ALL_CODE_PANES", "Cascade all code-panes"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_CASCADE_ALL_CODE_PANES", "Cascade all code-panes after normalize all minimized code-panes"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_CLOSE_SELECTED_PANES", "Close selected code-panes"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_CLOSE_SELECTED_PANES", "Close selected code-panes"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_TILE_BOTH_PANES_AND_UFDESIGNER", "Tiles selected both code-panes and User form designers"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_TILE_BOTH_PANES_AND_UFDESIGNER", "Tiles selected both code-panes and User form designers"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for CodePaneSwitchLstUtilities key-values cache
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Private Sub AddStringKeyValueForCodePaneSwitchLstUtilitiesByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_STANDARD_MODULES", "標準モジュール"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_CLASS_MODULES", "クラス"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_USER_FORM_MODULES", "ユーザーフォーム"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_OBJECT_MODULES", "オブジェクトモジュール"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_FORM_CAPTION_SWITCH_CODE_PANES", "コードウィンドウ切り替え"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_TILE_SELECTED_PANES", "選択ウィンドウのみ左右に並べて表示"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_TILE_SELECTED_PANES", "フォームデザイナはすべて閉じて、選択したウィンドウのみ左右に並べて表示"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_CASCADE_ALL_CODE_PANES", "全てのウィンドウを重ねて表示"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_CASCADE_ALL_CODE_PANES", "最小化されたすべてのウィンドウをアクティブにしてから、全てのウィンドウを重ねて表示"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_CLOSE_SELECTED_PANES", "選択ウィンドウを閉じる"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_CLOSE_SELECTED_PANES", "選択ウィンドウを閉じる"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_TILE_BOTH_PANES_AND_UFDESIGNER", "選択ウィンドウのみコードウィンドウとフォームデザイナを左右に並べて表示"
        .Add "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_TILE_BOTH_PANES_AND_UFDESIGNER", "選択ウィンドウのみコードウィンドウとフォームデザイナを左右に並べて表示"
    End With
End Sub

'''
''' Remove Keys for CodePaneSwitchLstUtilities key-values cache
'''
''' automatically-added for CodePaneSwitchLstUtilities string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueCodePaneSwitchLstUtilities(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_STANDARD_MODULES") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_STANDARD_MODULES"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_CLASS_MODULES"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_USER_FORM_MODULES"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_LBL_OBJECT_MODULES"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_FORM_CAPTION_SWITCH_CODE_PANES"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_TILE_SELECTED_PANES"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_TILE_SELECTED_PANES"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_CASCADE_ALL_CODE_PANES"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_CASCADE_ALL_CODE_PANES"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_CLOSE_SELECTED_PANES"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_CLOSE_SELECTED_PANES"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_CAPTION_TILE_BOTH_PANES_AND_UFDESIGNER"
            .Remove "STR_KEY_CODE_PANE_SWITCH_LIST_UTILITIES_RCLICK_TOOLTIPTEXT_TILE_BOTH_PANES_AND_UFDESIGNER"
        End If
    End With
End Sub

