Attribute VB_Name = "OperateVBIDECodePanes"
'
'   control VBIDE code-pane, which is independent of this VBA meta-structure
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on VBIDE
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  6/Jul/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
#Else
    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
#End If


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////

Public Enum DetectIDEWindowMode

    AllDetectInSpecifiedVBE
    
    OnlyIncludedModulesInSpecifiedVBProject
    
    ExcludedModulesFromSpecifiedVBProject
End Enum

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "OperateVBIDECodePanes"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Key-Value cache preparation for OperateVBIDECodePanes
'**---------------------------------------------
Private mobjStrKeyValueOperateVBIDECodePanesDic As Scripting.Dictionary

Private mblnIsStrKeyValueOperateVBIDECodePanesDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


' cache
Private mobjCacheModuleNameToVBComponentType As Scripting.Dictionary ' Dictionary(Of String[Module name], Of VBIDE.vbext_ComponentType)


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToOperateVBIDECodePanes()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
 
    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "CodePaneSwitchSingleLsvForm,ModifyFormsByWinAPI,DataTableListBox,DataTableListView,AxCtlLstCodeModulePaneHdl,OpenCodePaneSwitchForm"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Tests to send control-keys to VBIDE
'**---------------------------------------------
'''
''' The VBIDE window has to be active, when you need for this
'''
Public Sub ClearImmediateWindowBySendingControlKeyToVBE()

    Dim objVBE As VBIDE.VBE
    
    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

    objVBE.MainWindow.SetFocus

    SendKeys "^g"

    Sleep 10
    
    SendKeys "^a{DEL}"
End Sub

'**---------------------------------------------
'** Change WindowState about this VBE
'**---------------------------------------------
'''
'''
'''
Public Sub CloseSelectedCodePanes(ByVal vobjModuleNameToFileNameDic As Scripting.Dictionary)

    Dim objVBE As VBIDE.VBE, objCodePane As VBIDE.CodePane, blnIsToBeClosed As Boolean
    Dim strModuleName As String, strProjectFileName As String

    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

    For Each objCodePane In objVBE.CodePanes
        
        blnIsToBeClosed = mfblnIsCodePaneEqualsToBothModuleNameAndFileName(objCodePane, vobjModuleNameToFileNameDic)
    
        If blnIsToBeClosed Then
        
            objCodePane.Window.Close
        End If
    Next
End Sub

'''
'''
'''
Public Sub CloseSelectedBothCodePanesAndUserFormDesigners(ByVal vobjModuleNameToFileNameDic As Scripting.Dictionary)

    Dim objVBE As VBIDE.VBE, objCodePane As VBIDE.CodePane, blnIsToBeClosed As Boolean
    Dim strModuleName As String, strProjectFileName As String, enmVBCodePaneType As VBCodePaneType
    Dim objVBProjectToVBProjectFileNameDic As Scripting.Dictionary, objWindow As VBIDE.Window

    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

    Set objVBProjectToVBProjectFileNameDic = GetVBProjectToVBProjectFileNameDic()

    For Each objWindow In objVBE.Windows
    
        GetModuleNameAndOfficeFileNameAndVBAWindowPaneTypeFromVBProjects strModuleName, strProjectFileName, enmVBCodePaneType, objWindow.Caption, objVBProjectToVBProjectFileNameDic
        
        If strModuleName <> "" Then
        
            blnIsToBeClosed = False
            
            ' This is either the code pane or the User form designer window
            
            With vobjModuleNameToFileNameDic
            
                If .Exists(strModuleName) Then
                
                    If StrComp(strProjectFileName, .Item(strModuleName)) = 0 Then
                    
                        blnIsToBeClosed = True
                    End If
                End If
            End With
            
            If blnIsToBeClosed Then
                
                objWindow.Close
            
                Sleep 10
            
                'objWindow.Visible = False  ' In this case, each Window continue to exists in the VBE.Windows cache
            End If
        End If
    Next
End Sub

'''
'''
'''
Public Sub CascadeCodePanesAfterNomalizeAllPanes()

    Dim objVBE As VBIDE.VBE

    Set objVBE = GetCurrentOfficeFileObject().Application.VBE
    
    objVBE.MainWindow.SetFocus

    msubNormilizeAllCodePanes
    
    msubCascadeVBEWindows
End Sub

'''
'''
'''
Public Sub TileSelectedWindowsHorizontallyWithClosingBothObjectBrowserAndUFDesigner(ByVal vobjModuleNameToFileNameDic As Scripting.Dictionary)

    Dim objVBE As VBIDE.VBE

    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

'    objVBE.MainWindow.SetFocus
'
'    Sleep 10

    msubCloseObjectBrowserWindow objVBE
    
    msubCloseUserFormDesignViewWindow objVBE

    msubMinimizeCodePanesExceptSelectedModules vobjModuleNameToFileNameDic

    objVBE.MainWindow.SetFocus
    
    'Sleep 10

    msubTileVBEWindowsHorizontally
End Sub


'''
'''
'''
Public Sub TileSelectedBothCodePanesAndUFDeignersHorizontallyWithClosingObjectBrowser(ByVal vobjModuleNameToFileNameDic As Scripting.Dictionary)

    Dim objVBE As VBIDE.VBE

    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

'    objVBE.MainWindow.SetFocus
'
'    Sleep 10

    msubCloseObjectBrowserWindow objVBE
    
    msubOpenCodePaneAndUFDesignerWithMinimizingExcludedModules vobjModuleNameToFileNameDic
    

    objVBE.MainWindow.SetFocus
    
    Sleep 10

    msubTileVBEWindowsHorizontally
End Sub



'''
'''
'''
Private Sub msubMinimizeCodePanesExceptSelectedModules(ByVal vobjModuleNameToFileNameDic As Scripting.Dictionary)

    Dim objVBE As VBIDE.VBE, objCodePane As VBIDE.CodePane, blnIsMinimized As Boolean
    Dim strModuleName As String, strProjectFileName As String

    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

    For Each objCodePane In objVBE.CodePanes
        
        blnIsMinimized = Not mfblnIsCodePaneEqualsToBothModuleNameAndFileName(objCodePane, vobjModuleNameToFileNameDic)
    
        With objCodePane.Window
        
            If blnIsMinimized Then
            
                If .WindowState <> vbext_ws_Minimize Then
                
                    .WindowState = vbext_ws_Minimize
                End If
            Else
                If .WindowState <> vbext_ws_Normal Then
                
                    .WindowState = vbext_ws_Normal
                End If
            End If
        End With
    Next
End Sub

'''
'''
'''
Private Sub msubOpenCodePaneAndUFDesignerWithMinimizingExcludedModules(ByVal vobjModuleNameToFileNameDic As Scripting.Dictionary, Optional ByVal vblnIsCodePaneVisible As Boolean = True, Optional ByVal vblnIsUserFormDesingerVisible As Boolean = True)


    Dim objVBE As VBIDE.VBE, objWindow As VBIDE.Window, blnIsMinimized As Boolean
    Dim varModuleName As Variant, strModuleName As String, strProjectFileName As String, enmVBCodePaneType As VBCodePaneType
    Dim objCacheDic As Scripting.Dictionary, varCompoundKey As Variant, strKeys() As String, objVBProjectFileNameToVBProjectDic As Scripting.Dictionary
    Dim objVBProjectToVBProjectFileNameDic As Scripting.Dictionary
    Dim objVBProject As VBIDE.VBProject, objVBComponent As VBIDE.VBComponent, strFileName As String


    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

    Set objCacheDic = New Scripting.Dictionary
    
    Set objVBProjectFileNameToVBProjectDic = GetVBProjectFileNameToVBProjectDic()
    
    Set objVBProjectToVBProjectFileNameDic = GetVBProjectToVBProjectFileNameDic()
    
    With vobjModuleNameToFileNameDic
    
        For Each varModuleName In .Keys
        
            strFileName = .Item(varModuleName)
        
            objCacheDic.Add varModuleName & ";" & CStr(VBCodePaneType.OnlyVBCodeAtWindow) & ";" & strFileName, 0
            
            Set objVBProject = objVBProjectFileNameToVBProjectDic.Item(strFileName)
            
            Set objVBComponent = objVBProject.VBComponents.Item(varModuleName)
            
            If objVBComponent.Type = vbext_ct_MSForm Then
            
                objCacheDic.Add varModuleName & ";" & CStr(VBCodePaneType.UserFormDesignerAtWindow) & ";" & strFileName, 0
            End If
        Next
    End With

    For Each objWindow In objVBE.Windows
        
        GetModuleNameAndOfficeFileNameAndVBAWindowPaneTypeFromVBProjects strModuleName, strProjectFileName, enmVBCodePaneType, objWindow.Caption, objVBProjectToVBProjectFileNameDic
        
        If mstrModuleName <> "" Then
        
            ' This is either the code pane or the User form designer window
        
            blnIsMinimized = True
            
            With vobjModuleNameToFileNameDic
            
                If .Exists(strModuleName) Then
                
                    strFileName = .Item(strModuleName)
                
                    If StrComp(strProjectFileName, strFileName) = 0 Then
                    
                        objCacheDic.Remove strModuleName & ";" & CStr(enmVBCodePaneType) & ";" & strFileName
                    
                        blnIsMinimized = False
                    End If
                End If
            End With
            
            If Not vblnIsCodePaneVisible Then
             
                If Not blnIsMinimized And (enmVBCodePaneType = OnlyVBCodeAtWindow) Then
            
                    blnIsMinimized = True
                End If
            End If
            
            If Not vblnIsUserFormDesingerVisible Then
                
                If Not blnIsMinimized And (enmVBCodePaneType = UserFormDesignerAtWindow) Then
            
                    blnIsMinimized = True
                End If
            End If
        
            With objWindow
            
                If blnIsMinimized Then
                
                    If .WindowState <> vbext_ws_Minimize Then
                    
                        .WindowState = vbext_ws_Minimize
                    End If
                Else
                    If .WindowState <> vbext_ws_Normal Then
                    
                        .WindowState = vbext_ws_Normal
                    End If
                End If
            End With
        End If
    Next

    If objCacheDic.Count > 0 Then
    
        With objCacheDic
        
            For Each varCompoundKey In .Keys
            
                strKeys = Split(varCompoundKey, ";")
                
                strModuleName = strKeys(0)
                
                enmVBCodePaneType = CInt(strKeys(1))
                
                strFileName = strKeys(2)
            
                Select Case enmVBCodePaneType
                
                    Case VBCodePaneType.OnlyVBCodeAtWindow
                    
                        If vblnIsCodePaneVisible Then
                        
                            Set objVBProject = objVBProjectFileNameToVBProjectDic.Item(strFileName)
                            
                            Set objVBComponent = objVBProject.VBComponents.Item(strModuleName)
                            
                            objVBComponent.CodeModule.CodePane.Show
                        End If
                    
                    Case VBCodePaneType.UserFormDesignerAtWindow
                    
                        If vblnIsUserFormDesingerVisible Then
                        
                            Set objVBProject = objVBProjectFileNameToVBProjectDic.Item(strFileName)
                            
                            Set objVBComponent = objVBProject.VBComponents.Item(strModuleName)
                            
                            objVBComponent.Activate ' Show User Form desinger
                        End If
                End Select
            Next
        End With
    End If
End Sub


'''
'''
'''
Private Sub msubMinimizeCodePanesAndDesignerExceptSelectedModules(ByVal vobjModuleNameToFileNameDic As Scripting.Dictionary)

    Dim objVBE As VBIDE.VBE, objWindow As VBIDE.Window, blnIsMinimized As Boolean
    Dim strModuleName As String, strProjectFileName As String, enmVBCodePaneType As VBCodePaneType
    
    Dim objVBProjectToVBProjectFileNameDic As Scripting.Dictionary


    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

    Set objVBProjectToVBProjectFileNameDic = GetInverseDictionaryAsDictionaryFromValueToKeyWithoutDuplicatedValues(GetVBProjectNameToVBProjectDic(objVBE))

    For Each objWindow In objVBE.Windows
        
        GetModuleNameAndOfficeFileNameAndVBAWindowPaneTypeFromVBProjects strModuleName, strProjectFileName, enmVBCodePaneType, objWindow.Caption, objVBProjectToVBProjectFileNameDic
        
        If mstrModuleName <> "" Then
        
            ' This is either the code pane or the User form designer window
        
            blnIsMinimized = True
            
            With vobjModuleNameToFileNameDic
            
                If .Exists(strModuleName) Then
                
                    If StrComp(strProjectFileName, .Item(strModuleName)) = 0 Then
                    
                        blnIsMinimized = False
                    End If
                End If
            End With
            
            With objWindow
            
                If blnIsMinimized Then
                
                    If .WindowState <> vbext_ws_Minimize Then
                    
                        .WindowState = vbext_ws_Minimize
                    End If
                Else
                    If .WindowState <> vbext_ws_Normal Then
                    
                        .WindowState = vbext_ws_Normal
                    End If
                End If
            End With
        End If
    Next
End Sub


'''
'''
'''
Private Function mfblnIsCodePaneEqualsToBothModuleNameAndFileName(ByRef robjCodePane As VBIDE.CodePane, ByRef robjModuleNameToFileNameDic As Scripting.Dictionary) As Boolean

    Dim strModuleName As String, strProjectFileName As String, blnIsEqualsTo As Boolean
    
    blnIsEqualsTo = False
    
    With robjCodePane
    
        GetModuleNameAndOfficeFileNameFromVBIDECodePane strModuleName, strProjectFileName, .Window.Caption, .VBE
    End With
    
    With robjModuleNameToFileNameDic
    
        If .Exists(strModuleName) Then
        
            If StrComp(.Item(strModuleName), strProjectFileName) = 0 Then
                
                blnIsEqualsTo = True
            End If
        End If
    End With

    mfblnIsCodePaneEqualsToBothModuleNameAndFileName = blnIsEqualsTo
End Function



Private Sub msubCloseObjectBrowserWindow(Optional ByVal vobjVBE As VBIDE.VBE = Nothing)

    Dim objVBE As VBIDE.VBE, objWindow As VBIDE.Window
    
    If Not vobjVBE Is Nothing Then
    
        Set objVBE = vobjVBE
    Else
        Set objVBE = GetCurrentOfficeFileObject().Application.VBE
    End If

    For Each objWindow In objVBE.Windows
    
        ' You must prepare the proper localized window caption of the Object Browser for each language
        
        If StrComp(objWindow.Caption, GetTextOfStrKeyOperateVbideCodePanesObjectBrowser()) = 0 Then
        
            ' objWindow.WindowState = vbext_ws_Minimize
            
            objWindow.Visible = False
            
            Exit For
        End If
    Next
End Sub


Private Sub msubCloseUserFormDesignViewWindow(Optional ByVal vobjVBE As VBIDE.VBE = Nothing)

    Dim objVBE As VBIDE.VBE, objWindow As VBIDE.Window
    
    If Not vobjVBE Is Nothing Then
    
        Set objVBE = vobjVBE
    Else
        Set objVBE = GetCurrentOfficeFileObject().Application.VBE
    End If

    For Each objWindow In objVBE.Windows
    
        With objWindow
        
            If IsUserFormDesignViewWindow(.Caption) Then
            
                ' .WindowState = vbext_ws_Minimize
                
                .Visible = False
            End If
        End With
    Next
End Sub

'''
'''
'''
Private Function IsUserFormDesignViewWindow(ByVal rstrWindowCaption As String) As Boolean

    Dim intLeftParenPos As Long, blnIsDesignView As Boolean, strInfoWithInParens As String
    
    blnIsDesignView = False
    
    intLeftParenPos = InStrRev(rstrWindowCaption, "(")
    
    If intLeftParenPos > 0 Then
    
        strInfoWithInParens = Mid(rstrWindowCaption, intLeftParenPos + 1, Len(rstrWindowCaption) - intLeftParenPos - 1)
        
        'Debug.Print strInfoWithInParens
        
        If StrComp(strInfoWithInParens, "UserForm") = 0 Then
        
            blnIsDesignView = True
        End If
    End If

    IsUserFormDesignViewWindow = blnIsDesignView
End Function


Private Sub msubTileVBEWindowsHorizontally()

    SendKeys "%WV"
End Sub

Private Sub msubCascadeVBEWindows()

    SendKeys "%WC"
End Sub


Private Sub msubSanityTestToMinimizeActiveCodePane()

    Dim objVBE As VBIDE.VBE
    
    Set objVBE = GetCurrentOfficeFileObject().Application.VBE
    
    objVBE.ActiveWindow.WindowState = vbext_ws_Minimize
End Sub

'''
'''
'''
Private Sub msubSanityTestToMinimizeCodePanesExceptActiveCodePane()

    Dim objCodePane As VBIDE.CodePane, objActivePane As VBIDE.CodePane, objVBE As VBIDE.VBE
    
    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

    Set objActivePane = objVBE.ActiveCodePane

    For Each objCodePane In objVBE.CodePanes
    
        If Not objCodePane Is objActivePane Then
        
            objCodePane.Window.WindowState = vbext_ws_Minimize
        End If
    Next
End Sub

'''
'''
'''
Private Sub msubNormilizeAllCodePanes()

    Dim objCodePane As VBIDE.CodePane, objActivePane As VBIDE.CodePane, objVBE As VBIDE.VBE
    
    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

    Set objActivePane = objVBE.ActiveCodePane

    For Each objCodePane In objVBE.CodePanes
    
        If objCodePane.Window.WindowState = vbext_ws_Minimize Then
        
            objCodePane.Window.WindowState = vbext_ws_Normal
        End If
    Next
End Sub


'''
'''
'''
''' <Return>String: Module name</Return>
Public Function GetActiveIDECodePaneModuleName(ByVal vobjVBProject As VBIDE.VBProject, ByRef rstrFileName As String) As String

    Dim strModuleName As String, strFileName As String
    Dim strThisProjectModuleName As String
    
    strThisProjectModuleName = ""
    
    With vobjVBProject.VBE
    
        GetModuleNameAndOfficeFileNameFromVBIDECodePane strModuleName, strFileName, .ActiveCodePane.Window.Caption, vobjVBProject.VBE
    End With
    
    ' If the office file isn't saved at least one, then the VBProject.fileName returns null string. this prevents such causing error
    
    If StrComp(rstrFileName, strFileName) = 0 Then
    
        strThisProjectModuleName = strModuleName
    End If
    
    GetActiveIDECodePaneModuleName = strThisProjectModuleName
End Function


'**---------------------------------------------
'** Operate VBE code-panes
'**---------------------------------------------
'''
'''
'''
Public Sub ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma(ByRef rstrModuleNamesDelimitedByComma As String)

    ShowIDECodePaneFromModuleNamesOfThisVBProject GetColFromLineDelimitedChar(rstrModuleNamesDelimitedByComma)
End Sub

'''
'''
'''
Public Sub ShowIDECodePaneFromModuleNamesOfThisVBProject(ByVal vobjModuleNames As Collection)

    ShowIDECodePaneFromPluralModuleNamesAndVBProjectObject vobjModuleNames, GetThisOfficeFileVBProject(), GetCurrentOfficeFileObject().Name
End Sub

'''
''' general open VB code-panes for any VBProject
'''
Public Sub ShowIDECodePaneFromModuleNameToFileNameDic(ByVal vobjModuleNameToFileNameDic As Scripting.Dictionary, Optional ByVal vobjVBE As VBIDE.VBE = Nothing)

    Dim varName As Variant, strName As String, strFileName As String, strKey As String
    Dim objProjectFileNameToVBProjectDic As Scripting.Dictionary
    Dim objVBE As VBIDE.VBE, objCodePane As VBIDE.CodePane, objVBProject As VBIDE.VBProject
    
    If Not vobjVBE Is Nothing Then
    
        Set objVBE = vobjVBE
    Else
        Set objVBE = GetCurrentOfficeFileObject().Application.VBE
    End If
    
    
    Set objProjectFileNameToVBProjectDic = GetVBProjectFileNameToVBProjectDic()
    
    With GetOpenedIDECodePnameModuleAndFileNameToCodePaneDic(objVBE)
    
        For Each varName In vobjModuleNameToFileNameDic.Keys
        
            strName = varName
            
            strFileName = vobjModuleNameToFileNameDic.Item(varName)
            
            strKey = strName & ";" & strFileName
            
            If .Exists(strKey) Then
            
                Set objCodePane = .Item(strKey)
                
                Set objVBE.ActiveCodePane = objCodePane
            Else
                Set objVBProject = objProjectFileNameToVBProjectDic.Item(strFileName)
            
                msubShowCodePaneFromModuleNameAndVBProject strName, objVBProject
            End If
        Next
    End With
End Sub


'''
'''
'''
Public Sub ShowIDECodePaneFromModuleNameAndVBProjectName(ByVal vstrModuleName As String, ByVal vstrVBProjectName As String)

    Dim objVBProject As VBIDE.VBProject
    
    Set objVBProject = GetVBProjectFromVBProjectNameInCurrentVBE(vstrVBProjectName)

    ' About the following, the file name is new and isn't saved yet, the error occur

    ShowIDECodePaneFromModuleNameAndVBProjectObject vstrModuleName, _
            objVBProject, _
            GetFileNameFromPathByVbaDir(objVBProject.fileName)
End Sub


'''
'''
'''
Public Sub ShowIDECodePaneFromPluralModuleNamesAndVBProjectObject(ByVal vobjModuleNames As Collection, ByVal vobjVBProject As VBIDE.VBProject, ByRef rstrFileName As String)

    Dim strKey As String, varModuleName As Variant, strModuleName As String, strVBProjectFileName As String, objCodePane As VBIDE.CodePane


    msubCheckExistenseOfSpecifiedModuleNames vobjModuleNames, vobjVBProject

    With GetOpenedIDECodePnameModuleAndFileNameToCodePaneDic(vobjVBProject.VBE)
        
        For Each varModuleName In vobjModuleNames
        
            strModuleName = varModuleName
        
            If strModuleName <> GetActiveIDECodePaneModuleName(vobjVBProject, rstrFileName) Then
                
                strKey = strModuleName & ";" & rstrFileName
            
                If .Exists(strKey) Then
                
                    Set objCodePane = .Item(strKey)
                    
                    Set vobjVBProject.VBE.ActiveCodePane = objCodePane
                Else
                    msubShowCodePaneFromModuleNameAndVBProject strModuleName, vobjVBProject
                End If
            Else
                vobjVBProject.VBE.MainWindow.SetFocus
            End If
        Next
    End With
End Sub


'''
'''
'''
Private Sub msubCheckExistenseOfSpecifiedModuleNames(ByRef robjModuleNames As Collection, ByRef robjVBProject As VBIDE.VBProject)

    Dim varModuleName As Variant, objVBComponent As VBIDE.VBComponent
    Dim objNotIncludedNames As Collection, strLog As String
    
    For Each varModuleName In robjModuleNames
    
        Set objVBComponent = Nothing
    
        On Error Resume Next
        
        Set objVBComponent = robjVBProject.VBComponents.Item(varModuleName)
    
        On Error GoTo 0
    
        If objVBComponent Is Nothing Then
    
            If objNotIncludedNames Is Nothing Then Set objNotIncludedNames = New Collection
            
            objNotIncludedNames.Add varModuleName
        End If
    Next

    If Not objNotIncludedNames Is Nothing Then
    
        strLog = "The following VBComponent isn't included at this VB project" & vbNewLine & GetLineTextFromCol(objNotIncludedNames)
    
        Debug.Print strLog
        
        MsgBox strLog, vbInformation Or vbOKOnly, "No existed VB code modules"
    End If
End Sub


'''
'''
'''
Public Sub ShowIDECodePaneFromModuleNameAndVBProjectObject(ByVal vstrModuleName As String, _
        ByVal vobjVBProject As VBIDE.VBProject, _
        ByRef rstrFileName As String)


    Dim strKey As String, objCodePane As VBIDE.CodePane

    If vstrModuleName <> GetActiveIDECodePaneModuleName(vobjVBProject, rstrFileName) Then
    
        With GetOpenedIDECodePnameModuleAndFileNameToCodePaneDic(vobjVBProject.VBE)
        
            strKey = vstrModuleName & ";" & rstrFileName
        
            If .Exists(strKey) Then
            
                Set objCodePane = .Item(strKey)
                
                Set vobjVBProject.VBE.ActiveCodePane = objCodePane
            Else
                msubShowCodePaneFromModuleNameAndVBProject vstrModuleName, vobjVBProject
            End If
        End With
    Else
        vobjVBProject.VBE.MainWindow.SetFocus
    End If
End Sub


'''
'''
'''
Private Sub msubShowCodePaneFromModuleNameAndVBProject(ByVal vstrModuleName As String, _
        ByVal vobjVBProject As VBIDE.VBProject)

    Dim objVBComponent As VBIDE.VBComponent

    On Error Resume Next

    Set objVBComponent = vobjVBProject.VBComponents.Item(vstrModuleName)
    
    On Error GoTo 0
    
    If Not objVBComponent Is Nothing Then
    
        objVBComponent.CodeModule.CodePane.Show
    End If
End Sub


'''
'''
'''
''' <Return>Dictionary(Of Scripting[ModuleName,FileName], Of CodePane)</Return>
Public Function GetOpenedIDECodePnameModuleAndFileNameToCodePaneDic(Optional ByVal vobjVBE As VBIDE.VBE) As Scripting.Dictionary


    Dim objVBE As VBIDE.VBE, objCodePane As VBIDE.CodePane, strKey As String, objDic As Scripting.Dictionary
    Dim strModuleName As String, strProjectFileName As String
    
    If Not vobjVBE Is Nothing Then
    
        Set objVBE = vobjVBE
    Else
        Set objVBE = GetCurrentOfficeFileObject().Application.VBE
    End If
    
    Set objDic = New Scripting.Dictionary
    
    For Each objCodePane In objVBE.CodePanes
    
        With objCodePane.Window
        
            GetModuleNameAndOfficeFileNameFromVBIDECodePane strModuleName, strProjectFileName, .Caption, objVBE
            
            If strModuleName <> "" And strProjectFileName <> "" Then
            
                strKey = strModuleName & ";" & strProjectFileName
                
                With objDic
                    
                    If Not .Exists(strKey) Then
                    
                        .Add strKey, objCodePane
                    End If
                End With
            End If
        End With
    Next

    Set GetOpenedIDECodePnameModuleAndFileNameToCodePaneDic = objDic
End Function



'''
'''
'''
''' <Argument>vobjVBProject: </Argument>
''' <Argument>venmDetectIDEWindowMode: </Argument>
''' <Argument>venmSettingIDEWindowDictionaryValueType: </Argument>
''' <Return>Scripting.Dictionary(Of String[Project Name], Collection(Of String) )</Return>
Public Function GetOpenedIDECodePaneModuleNameToProjectFileNamesDic(ByVal vobjVBProject As VBIDE.VBProject, Optional ByVal venmDetectIDEWindowMode As DetectIDEWindowMode = DetectIDEWindowMode.OnlyIncludedModulesInSpecifiedVBProject) As Scripting.Dictionary


    Dim objDic As Scripting.Dictionary, objProjectFileNameCol As Collection
    Dim objCodePane As VBIDE.CodePane
    Dim strProjectFileName As String, strModuleName As String
    
    Dim strInputVBProjectFileName As String, blnAllowToAdd As Boolean
    
    
    Set objDic = New Scripting.Dictionary
    
    With vobjVBProject
    
        strInputVBProjectFileName = GetFileNameWithoutUsingFileSystemObject(.fileName)
        
        With .VBE
        
            For Each objCodePane In .CodePanes
            
                blnAllowToAdd = False
            
                With objCodePane.Window
                
                    GetModuleNameAndOfficeFileNameFromVBIDECodePane strModuleName, strProjectFileName, .Caption, .VBE
                
                    Select Case venmDetectIDEWindowMode
                    
                        Case DetectIDEWindowMode.ExcludedModulesFromSpecifiedVBProject
                        
                            If StrComp(strInputVBProjectFileName, strProjectFileName) <> 0 Then
                            
                                blnAllowToAdd = True
                            End If
                        
                        Case DetectIDEWindowMode.OnlyIncludedModulesInSpecifiedVBProject
                        
                            If StrComp(strInputVBProjectFileName, strProjectFileName) = 0 Then
                            
                                blnAllowToAdd = True
                            End If
                            
                        Case DetectIDEWindowMode.AllDetectInSpecifiedVBE
                    
                            blnAllowToAdd = True
                    End Select
                
                    If blnAllowToAdd Then
                        
                        With objDic
                        
                            If Not .Exists(strModuleName) Then
                            
                                Set objProjectFileNameCol = New Collection
                                
                                objProjectFileNameCol.Add strProjectFileName
                                
                                .Add strModuleName, objProjectFileNameCol
                            Else
                                Set objProjectFileNameCol = .Item(strModuleName)
                            
                                objProjectFileNameCol.Add strProjectFileName
                            
                                Set .Item(strModuleName) = objProjectFileNameCol
                            End If
                        End With
                    End If
                End With
            Next
        End With
    End With

    Set GetOpenedIDECodePaneModuleNameToProjectFileNamesDic = objDic
End Function


'''
'''
'''
Public Sub GetFieldTitlesLsvInfoForModuleClassifiedTypeVBProject(ByRef robjFieldTitles As Collection, ByRef robjFieldTitleToColumnWidthDic As Scripting.Dictionary)

    Set robjFieldTitles = GetColFromLineDelimitedChar("Module name,Classified name,VB project name,File name")
    
    Set robjFieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Module name,120,Classified name,40,VB project name,50,File name,50")
End Sub


'''
'''
'''
Public Function GetDataTableColOfOpenedIDECodePaneModuleClassifiedTypeVBProjectInfo() As Collection

    Dim objCol As Collection, objRowCol As Collection
    Dim objOpenedIDECodePaneModuleNameAndInfoDic As Scripting.Dictionary, objModuleNameToVBProjectNameDic As Scripting.Dictionary
    Dim varClassifiedName As Variant, strClassifiedName As String, varModuleName As Variant, strModuleName As String, strVBProjectName As String
    Dim strNames() As String, strFileName As String

    Set objOpenedIDECodePaneModuleNameAndInfoDic = GetOpenedIDECodePaneOfModuleClassifiedTypeVBProjectDic()


    Set objCol = New Collection

    With objOpenedIDECodePaneModuleNameAndInfoDic
    
        For Each varClassifiedName In .Keys
        
            strClassifiedName = varClassifiedName
            
            Set objModuleNameToVBProjectNameDic = .Item(varClassifiedName)
        
            With objModuleNameToVBProjectNameDic
            
                For Each varModuleName In .Keys
                
                    strModuleName = varModuleName
                
                    strNames = Split(.Item(varModuleName), ";")
                
                    strVBProjectName = strNames(0)
                    
                    strFileName = strNames(1)
                    
                    Set objRowCol = New Collection
                    
                    With objRowCol
                    
                        .Add strModuleName
                        
                        .Add strClassifiedName
                        
                        .Add strVBProjectName
                        
                        .Add strFileName
                    End With
                    
                    objCol.Add objRowCol
                Next
            End With
        Next
    End With

    Set GetDataTableColOfOpenedIDECodePaneModuleClassifiedTypeVBProjectInfo = objCol
End Function


'''
''' supports plural opened VBProject objects
'''
''' <Return>Dictionary(Of String[FileName], Of Dictionary(Of Long[GeneralVBComponentType], Collection(Of String[ModuleName])))</Return>
Public Function GetOpenedIDECodePaneOfFileNameClassifiedTypeModuleDic() As Scripting.Dictionary

    Dim objApplication As Object    ' Excel.Application, Word.Application, or PowerPoint.Application
    Dim objCodePane As VBIDE.CodePane, enmComponentType As VBIDE.vbext_ComponentType
    Dim objFileNameToVBProjectDic As Scripting.Dictionary, objVBProject As VBIDE.VBProject, strModuleName As String, strProjectFileName As String
    Dim enmGeneralVBComponentType As GeneralVBComponentType, varGeneralVBComponentType As Variant
    Dim objDic As Scripting.Dictionary, objClassifiedTypeToModuleNameDic As Scripting.Dictionary
    
    
    Set objApplication = GetCurrentOfficeFileObject().Application
    
    Set objFileNameToVBProjectDic = GetVBProjectFileNameToVBProjectDic()
    
    Set objDic = New Scripting.Dictionary
    
    For Each objCodePane In objApplication.VBE.CodePanes
    
        With objCodePane.Window
                
            ' With parsing the code-pane window title caption, get the module anem and the office file name
            GetModuleNameAndOfficeFileNameFromVBIDECodePane strModuleName, strProjectFileName, .Caption, .VBE
            
            Set objVBProject = objFileNameToVBProjectDic.Item(strProjectFileName)
            
            enmGeneralVBComponentType = GetVBComponentExType(strModuleName, objVBProject)
            
            With objDic
            
                If Not .Exists(strProjectFileName) Then
                
                    Set objClassifiedTypeToModuleNameDic = New Scripting.Dictionary
                    
                    msubAddClassifiedTypeToModuleNameIntoDic objClassifiedTypeToModuleNameDic, enmGeneralVBComponentType, strModuleName
                    
                    .Add strProjectFileName, objClassifiedTypeToModuleNameDic
                Else
                    Set objClassifiedTypeToModuleNameDic = .Item(strProjectFileName)
                
                    msubAddClassifiedTypeToModuleNameIntoDic objClassifiedTypeToModuleNameDic, enmGeneralVBComponentType, strModuleName
                    
                    Set .Item(strProjectFileName) = objClassifiedTypeToModuleNameDic
                End If
            End With
        End With
    Next
    
    ' sort it about ModuleName
    With objDic
        
        For Each varGeneralVBComponentType In .Keys
        
            SortDictionaryAboutValuesCollection .Item(varGeneralVBComponentType)
        Next
    End With
    
    ' sort it about FileName
    SortDictionaryAboutKeys objDic

    Set GetOpenedIDECodePaneOfFileNameClassifiedTypeModuleDic = objDic
End Function


'''
''' supports plural opened VBProject objects
'''
''' <Return>Dictionary(Of String[FileName], Of Dictionary(Of String[ClassifiedName], Collection(Of String[ModuleName])))</Return>
Public Function GetOpenedIDECodePaneOfFileNameClassifiedTypeNameModuleDic() As Scripting.Dictionary

    Dim objApplication As Object    ' Excel.Application, Word.Application, or PowerPoint.Application
    Dim objCodePane As VBIDE.CodePane, enmComponentType As VBIDE.vbext_ComponentType
    Dim objFileNameToVBProjectDic As Scripting.Dictionary, objVBProject As VBIDE.VBProject, strModuleName As String, strProjectFileName As String
    Dim strClassifiedName As String
    Dim objDic As Scripting.Dictionary, objClassifiedNameToModuleNameDic As Scripting.Dictionary, varFileName As Variant
    
    
    Set objApplication = GetCurrentOfficeFileObject().Application
    
    Set objFileNameToVBProjectDic = GetVBProjectFileNameToVBProjectDic()
    
    Set objDic = New Scripting.Dictionary
    
    For Each objCodePane In objApplication.VBE.CodePanes
    
        With objCodePane.Window
                
            ' With parsing the code-pane window title caption, get the module anem and the office file name
            GetModuleNameAndOfficeFileNameFromVBIDECodePane strModuleName, strProjectFileName, .Caption, .VBE
            
            Set objVBProject = objFileNameToVBProjectDic.Item(strProjectFileName)
            
            strClassifiedName = GetVBComponentTypeClassifiedName(strModuleName, objVBProject)
            
            With objDic
            
                If Not .Exists(strProjectFileName) Then
                
                    Set objClassifiedNameToModuleNameDic = New Scripting.Dictionary
                    
                    msubAddClassifiedNameToModuleNameIntoDic objClassifiedNameToModuleNameDic, strClassifiedName, strModuleName
                    
                    .Add strProjectFileName, objClassifiedNameToModuleNameDic
                Else
                    Set objClassifiedNameToModuleNameDic = .Item(strProjectFileName)
                
                    msubAddClassifiedNameToModuleNameIntoDic objClassifiedNameToModuleNameDic, strClassifiedName, strModuleName
                    
                    Set .Item(strProjectFileName) = objClassifiedNameToModuleNameDic
                End If
            End With
        End With
    Next
    
    ' sort it about ModuleName
    With objDic
        
        For Each varFileName In .Keys
        
            SortDictionaryAboutValuesCollection .Item(varFileName)
        Next
    End With
    
    ' sort it about FileName
    SortDictionaryAboutKeys objDic

    Set GetOpenedIDECodePaneOfFileNameClassifiedTypeNameModuleDic = objDic
End Function

'''
''' supports plural opened VBProject objects
'''
''' <Return>Dictionary(Of String[ClassifiedName], Of Dictionary(Of String[ModuleName], String[VBProject.Name;FileName]))</Return>
Public Function GetOpenedIDECodePaneOfModuleClassifiedTypeVBProjectDic() As Scripting.Dictionary

    Dim objApplication As Object    ' Excel.Application, Word.Application, or PowerPoint.Application
    Dim objCodePane As VBIDE.CodePane, enmComponentType As VBIDE.vbext_ComponentType
    Dim objFileNameToVBProjectDic As Scripting.Dictionary, objVBProject As VBIDE.VBProject, strModuleName As String, strProjectFileName As String
    Dim strClassifiedName As String, objDic As Scripting.Dictionary, objModuleNameToVBProjectNameDic As Scripting.Dictionary
    
    
    Set objApplication = GetCurrentOfficeFileObject().Application
    
    Set objFileNameToVBProjectDic = GetVBProjectFileNameToVBProjectDic()
    
    Set objDic = New Scripting.Dictionary
    
    For Each objCodePane In objApplication.VBE.CodePanes
    
        With objCodePane.Window
                
            ' With parsing the code-pane window title caption, get the module anem and the office file name
            GetModuleNameAndOfficeFileNameFromVBIDECodePane strModuleName, strProjectFileName, .Caption, .VBE
            
            Set objVBProject = objFileNameToVBProjectDic.Item(strProjectFileName)
            
            strClassifiedName = GetVBComponentTypeClassifiedName(strModuleName, objVBProject)
            
            With objDic
            
                If Not .Exists(strClassifiedName) Then
                
                    Set objModuleNameToVBProjectNameDic = New Scripting.Dictionary
                    
                    msubAddModuleNameToVBProjectNameAndFileNameIntoDic objModuleNameToVBProjectNameDic, strModuleName, objVBProject.Name & ";" & strProjectFileName
                    
                    .Add strClassifiedName, objModuleNameToVBProjectNameDic
                Else
                    Set objModuleNameToVBProjectNameDic = .Item(strClassifiedName)
                
                    msubAddModuleNameToVBProjectNameAndFileNameIntoDic objModuleNameToVBProjectNameDic, strModuleName, objVBProject.Name & ";" & strProjectFileName
                    
                    Set .Item(strClassifiedName) = objModuleNameToVBProjectNameDic
                End If
            End With
        End With
    Next
    
    ' sort it about ModuleName
    SortDictionaryAboutValuesChildDictionaryKeys objDic
    
    ' sort it about ClassifiedName
    SortDictionaryAboutKeys objDic

    Set GetOpenedIDECodePaneOfModuleClassifiedTypeVBProjectDic = objDic
End Function



'''
''' get this current VBIDE windows state as the DataTable collection
'''
Public Function GetVBIDECodePaneWindowInfoDataTableCol(Optional ByVal vblnExcludeDuplicatedItems As Boolean = True) As Collection

    Dim objVBE As VBIDE.VBE, objWindow As VBIDE.Window, enmVBCodePaneType As VBCodePaneType, strVBCodePaneTypeName As String, blnContinue As Boolean
    Dim strModuleName As String, strProjectFileName As String
    Dim objVBProjectFileNameToVBProjectDic As Scripting.Dictionary
    Dim objVBProjectToVBProjectFileNameDic As Scripting.Dictionary, objDTCol As Collection, objRowCol As Collection, objUniqueKeysDic As Scripting.Dictionary
    Dim strKey As String
    Dim objTmpDic As Scripting.Dictionary, objGeneralComponentTypeToLocalizedNameDic As Scripting.Dictionary
    Dim strComponentClassifiedType As String


    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

    Set objDTCol = New Collection

    Set objGeneralComponentTypeToLocalizedNameDic = GetGeneralComponentTypeToLocalizedNameDic()

    Set objVBProjectFileNameToVBProjectDic = GetVBProjectFileNameToVBProjectDic()

    Set objVBProjectToVBProjectFileNameDic = GetVBProjectToVBProjectFileNameDic()

    For Each objWindow In objVBE.Windows
    
        GetModuleNameAndOfficeFileNameAndVBAWindowPaneTypeFromVBProjects strModuleName, strProjectFileName, enmVBCodePaneType, objWindow.Caption, objVBProjectToVBProjectFileNameDic
        
        If strModuleName <> "" Then
        
            blnContinue = True
        
            strVBCodePaneTypeName = GetTextFromVBCodePaneType(enmVBCodePaneType)
        
            strComponentClassifiedType = objGeneralComponentTypeToLocalizedNameDic.Item(GetVBComponentExType(strModuleName, objVBProjectFileNameToVBProjectDic.Item(strProjectFileName)))
        
            strKey = strProjectFileName & ";" & strComponentClassifiedType & ";" & strModuleName & ";" & strVBCodePaneTypeName
            
            If vblnExcludeDuplicatedItems Then
            
                If objUniqueKeysDic Is Nothing Then Set objUniqueKeysDic = New Scripting.Dictionary
            
                With objUniqueKeysDic
                
                    If Not .Exists(strKey) Then
                    
                        .Add strKey, 0
                    Else
                        blnContinue = False
                    End If
                End With
            End If
        
            If blnContinue Then
        
                If objTmpDic Is Nothing Then Set objTmpDic = New Scripting.Dictionary
        
                Set objRowCol = New Collection
                
                With objRowCol
                
                    .Add strModuleName
                    
                    .Add strProjectFileName
                
                    .Add strVBCodePaneTypeName
                    
                    .Add strComponentClassifiedType
                
                    .Add objVBProjectFileNameToVBProjectDic.Item(strProjectFileName).Name
                End With
                
                objTmpDic.Add strKey, objRowCol
                
                'objDTCol.Add objRowCol
            End If
        End If
    Next
    
    ' Sort
    If Not objTmpDic Is Nothing Then
    
        If objTmpDic.Count > 0 Then
        
            SortDictionaryAboutKeys objTmpDic
            
            Set objDTCol = GetEnumeratorValuesColFromDic(objTmpDic)
        End If
    End If

    Set GetVBIDECodePaneWindowInfoDataTableCol = objDTCol
End Function



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function GetTextFromVBCodePaneType(ByVal venmVBCodePaneType As VBCodePaneType) As String

    Dim strText As String
    
    strText = ""

    Select Case venmVBCodePaneType
    
        Case VBCodePaneType.OnlyVBCodeAtWindow
        
            strText = "VBCodePane"
        
        Case VBCodePaneType.UserFormDesignerAtWindow
        
            strText = "UserFormDesigner"
    End Select

    GetTextFromVBCodePaneType = strText
End Function

'''
'''
'''
Private Sub msubAddClassifiedTypeToModuleNameIntoDic(ByRef robjDic As Scripting.Dictionary, _
        ByRef renmGeneralVBComponentType As GeneralVBComponentType, _
        ByRef rstrModuleName As String)


    Dim objChildCol As Collection, strItem As String

    With robjDic
    
        If Not .Exists(renmGeneralVBComponentType) Then
        
            Set objChildCol = New Collection
            
            objChildCol.Add rstrModuleName, key:=rstrModuleName
        
            .Add renmGeneralVBComponentType, objChildCol
        Else
            Set objChildCol = .Item(renmGeneralVBComponentType)
            
            strItem = ""
            
            On Error Resume Next
            
            strItem = objChildCol.Item(rstrModuleName)
            
            On Error GoTo 0
            
            If strItem = "" Then
            
                objChildCol.Add rstrModuleName, key:=rstrModuleName
                
                Set .Item(renmGeneralVBComponentType) = objChildCol
            End If
        End If
    End With
End Sub


'''
'''
'''
Private Sub msubAddClassifiedNameToModuleNameIntoDic(ByRef robjDic As Scripting.Dictionary, _
        ByRef rstrClassifiedName As String, _
        ByRef rstrModuleName As String)

    Dim objChildCol As Collection, strItem As String

    With robjDic
    
        If Not .Exists(rstrClassifiedName) Then
        
            Set objChildCol = New Collection
            
            objChildCol.Add rstrModuleName, key:=rstrModuleName
        
            .Add rstrClassifiedName, objChildCol
        Else
            Set objChildCol = .Item(rstrClassifiedName)
            
            strItem = ""
            
            On Error Resume Next
            
            strItem = objChildCol.Item(rstrModuleName)
            
            On Error GoTo 0
            
            If strItem = "" Then
            
                objChildCol.Add rstrModuleName, key:=rstrModuleName
                
                Set .Item(rstrClassifiedName) = objChildCol
            End If
        End If
    End With
End Sub


'''
'''
'''
Private Sub msubAddModuleNameToVBProjectNameAndFileNameIntoDic(ByRef robjDic As Scripting.Dictionary, _
        ByRef rstrModuleName As String, _
        ByRef rstrVBProjectNameAndFileName As String)

    With robjDic
    
        If Not .Exists(rstrModuleName) Then
        
            .Add rstrModuleName, rstrVBProjectNameAndFileName
        End If
    End With
End Sub

'''
''' Use this when only the VBProjects of the specified project name are needed
'''
Public Function GetVBProjectNameToVBProjectDicFromCurrentVBE() As Scripting.Dictionary

    Set GetVBProjectNameToVBProjectDicFromCurrentVBE = GetVBProjectNameToVBProjectDic(GetCurrentOfficeFileObject().Application.VBE)
End Function


'''
'''
'''
Private Function GetVBProjectNameToVBProjectDic(ByVal vobjVBE As VBIDE.VBE) As Scripting.Dictionary

    Dim objVBProject As VBIDE.VBProject, objDic As Scripting.Dictionary, objFileNameCol As Collection
    Dim objDuplicatedProjectNameToFileNameDic As Scripting.Dictionary, objFS As Scripting.FileSystemObject
    
    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        For Each objVBProject In vobjVBE.VBProjects
        
            If IsVBProjectSavedAtLeastOne(objVBProject) Then
            
                If Not .Exists(objVBProject.Name) Then
                
                    .Add objVBProject.Name, objVBProject
                Else
                
                    If objFS Is Nothing Then Set objFS = New Scripting.FileSystemObject
                
                    If objDuplicatedProjectNameToFileNameDic Is Nothing Then
                    
                        Set objDuplicatedProjectNameToFileNameDic = New Scripting.Dictionary
                    End If
                
                    With objDuplicatedProjectNameToFileNameDic
                    
                        If Not .Exists(objVBProject.Name) Then
                        
                            Set objFileNameCol = New Collection
                            
                            With objFileNameCol
                            
                                .Add objFS.GetFileName(objDic.Item(objVBProject.Name).fileName)
                                
                                .Add objFS.GetFileName(objVBProject.fileName)
                            End With
                        
                            .Add objVBProject.Name, objFileNameCol
                        Else
                            Set objFileNameCol = .Item(objVBProject.Name)
                            
                            objFileNameCol.Add objFS.GetFileName(objVBProject.fileName)
                            
                            Set .Item(objVBProject.Name) = objFileNameCol
                        End If
                    End With
                End If
            End If
        Next
    End With

    If Not objDuplicatedProjectNameToFileNameDic Is Nothing Then
    
        MsgBox mfstrGetLogOfDuplicatedVBProjectNames(objDuplicatedProjectNameToFileNameDic), vbOKOnly Or vbExclamation, "Duplicated VB project name..."
    End If

    Set GetVBProjectNameToVBProjectDic = objDic
End Function

'''
'''
'''
Private Function GetVBProjectFileNameToVBProjectDicLimitedBySavedAtLeastOne(ByVal vobjVBE As VBIDE.VBE) As Scripting.Dictionary

    Dim objVBProject As VBIDE.VBProject, objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary
    
    For Each objVBProject In vobjVBE.VBProjects
    
        If IsVBProjectSavedAtLeastOne(objVBProject) Then
    
            objDic.Add GetFileNameWithoutUsingFileSystemObject(objVBProject.fileName), objVBProject
        End If
    Next

    Set GetVBProjectFileNameToVBProjectDicLimitedBySavedAtLeastOne = objDic
End Function




'''
'''
'''
Private Function GetVBProjectFromVBProjectNameInCurrentVBE(ByVal vstrVBProjectName As String) As VBIDE.VBProject

    Dim objApplication As Object    ' either Excel.Application, Word.Application, or PowerPoint.Application
    
    Set objApplication = GetCurrentOfficeFileObject().Application

    Set GetVBProjectFromVBProjectNameInCurrentVBE = GetVBProjectFromVBProjectNameInVBE(vstrVBProjectName, objApplication.VBE)
End Function

'''
'''
'''
Private Function GetVBProjectFromVBProjectNameInVBE(ByVal vstrVBProjectName As String, ByVal vobjVBE As VBIDE.VBE) As VBIDE.VBProject

    Dim objVBProject As VBIDE.VBProject, objFoundVBProject As VBIDE.VBProject
    
    Set objFoundVBProject = Nothing

    With New Scripting.FileSystemObject
    
        For Each objVBProject In vobjVBE.VBProjects
        
            If IsVBProjectSavedAtLeastOne(objVBProject) Then
            
                If StrComp(objVBProject.Name, vstrVBProjectName) = 0 Then
                
                    Set objFoundVBProject = objVBProject
                
                    Exit For
                End If
            End If
        Next
    End With

    Set GetVBProjectFromVBProjectNameInVBE = objFoundVBProject
End Function



'''
'''
'''
Private Function mfenmGetModuleNameToExtentionType(ByVal vobjVBProject As VBIDE.VBProject, ByVal vstrModuleName As String) As VBIDE.vbext_ComponentType

    Dim objVBComponent As VBIDE.VBComponent, enmComponentType As VBIDE.vbext_ComponentType

    If mobjCacheModuleNameToVBComponentType Is Nothing Then Set mobjCacheModuleNameToVBComponentType = New Scripting.Dictionary
    
    With mobjCacheModuleNameToVBComponentType
    
        If Not .Exists(vstrModuleName) Then
        
            On Error Resume Next
            
            Set objVBComponent = vobjVBProject.VBComponents.Item(vstrModuleName)
            
            On Error GoTo 0
        
            If Not objVBComponent Is Nothing Then
            
                enmComponentType = objVBComponent.Type
                
                .Add vstrModuleName, enmComponentType
            Else
                enmComponentType = 0
            
                Err.Raise 1, "Failed to get VBIDE.VBComponent instance", "Error: getting VBComponent"
            End If
        Else
            enmComponentType = .Item(vstrModuleName)
        End If
    End With

    mfenmGetModuleNameToExtentionType = enmComponentType
End Function


'**---------------------------------------------
'** File path utilities
'**---------------------------------------------
'''
''' Using this is slightly faster than using FileSystemObject. When the system serves the huge count of paths, it should be used.
'''
Public Function GetFileNameWithoutUsingFileSystemObject(ByVal vstrPath As String) As String

    Dim intPositionOfBackSlash As Long, intPositionOfColon As Long
    
    Dim strFileName As String, strPathSeparator As String
    
    
    strFileName = ""
    
    ' PowerPoint.Application has no PathSeparator property
    
    On Error Resume Next
    
    strPathSeparator = ""
    
    strPathSeparator = VBA.CallByName(Application, "PathSeparator", VbGet)
    
    If strPathSeparator = "" Then
    
        strPathSeparator = "\"
    End If
    
    On Error GoTo 0
    
    
    intPositionOfBackSlash = InStrRev(vstrPath, strPathSeparator)
    
    intPositionOfColon = InStrRev(vstrPath, ":")
    
    Select Case vstrPath
    
        Case ".", ".."
    
            ' directory , or parent-directory
        Case Else
        
            If intPositionOfBackSlash > 0 Then
            
                strFileName = Right(vstrPath, Len(vstrPath) - intPositionOfBackSlash)
                
            ElseIf intPositionOfColon > 0 Then
                ' drive-name
                
            Else
                ' file name
                strFileName = vstrPath
            End If
    End Select

    GetFileNameWithoutUsingFileSystemObject = strFileName
End Function

'**---------------------------------------------
'** log tools
'**---------------------------------------------
'''
''' get log only when more than one Macro enable office file exists which have the same VB project name.
'''
Private Function mfstrGetLogOfDuplicatedVBProjectNames(ByVal vobjDuplicatedProjectNameToFileNameDic As Scripting.Dictionary) As String

    Dim strLog As String, varProjectName As Variant, varFileNameCol As Variant, varFileName As Variant
    
    strLog = "Duplicated VB project name VBA files:" & vbNewLine & vbNewLine
    
    With vobjDuplicatedProjectNameToFileNameDic
    
        For Each varProjectName In .Keys
        
            strLog = strLog & "A VB project name: " & varProjectName & vbNewLine & vbNewLine
            
            For Each varFileName In .Item(varProjectName)
            
                strLog = strLog & "File name: " & varFileName & vbNewLine
            Next
        Next
    End With

    mfstrGetLogOfDuplicatedVBProjectNames = strLog
End Function


'**---------------------------------------------
'** Key-Value cache preparation for OperateVBIDECodePanes
'**---------------------------------------------
'''
''' get string Value from STR_KEY_OPERATE_VBIDE_CODE_PANES_OBJECT_BROWSER
'''
''' automatically-added for OperateVBIDECodePanes string-key-value management for standard module and class module
Private Function GetTextOfStrKeyOperateVbideCodePanesObjectBrowser() As String

    If Not mblnIsStrKeyValueOperateVBIDECodePanesDicInitialized Then

        msubInitializeTextForOperateVBIDECodePanes
    End If

    GetTextOfStrKeyOperateVbideCodePanesObjectBrowser = mobjStrKeyValueOperateVBIDECodePanesDic.Item("STR_KEY_OPERATE_VBIDE_CODE_PANES_OBJECT_BROWSER")
End Function

'''
''' get string Value from STR_KEY_OPERATE_VBIDE_CODE_PANES_CODE
'''
''' automatically-added for OperateVBIDECodePanes string-key-value management for standard module and class module
Public Function GetTextOfStrKeyOperateVbideCodePanesCode() As String

    If Not mblnIsStrKeyValueOperateVBIDECodePanesDicInitialized Then

        msubInitializeTextForOperateVBIDECodePanes
    End If

    GetTextOfStrKeyOperateVbideCodePanesCode = mobjStrKeyValueOperateVBIDECodePanesDic.Item("STR_KEY_OPERATE_VBIDE_CODE_PANES_CODE")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for OperateVBIDECodePanes string-key-value management for standard module and class module
Private Sub msubInitializeTextForOperateVBIDECodePanes()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueOperateVBIDECodePanesDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForOperateVBIDECodePanesByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForOperateVBIDECodePanesByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueOperateVBIDECodePanesDicInitialized = True
    End If

    Set mobjStrKeyValueOperateVBIDECodePanesDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for OperateVBIDECodePanes key-values cache
'''
''' automatically-added for OperateVBIDECodePanes string-key-value management for standard module and class module
Private Sub AddStringKeyValueForOperateVBIDECodePanesByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_OPERATE_VBIDE_CODE_PANES_OBJECT_BROWSER", "Object Browser"
        .Add "STR_KEY_OPERATE_VBIDE_CODE_PANES_CODE", "Code"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for OperateVBIDECodePanes key-values cache
'''
''' automatically-added for OperateVBIDECodePanes string-key-value management for standard module and class module
Private Sub AddStringKeyValueForOperateVBIDECodePanesByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_OPERATE_VBIDE_CODE_PANES_OBJECT_BROWSER", "オブジェクト ブラウザー"
        .Add "STR_KEY_OPERATE_VBIDE_CODE_PANES_CODE", "コード"
    End With
End Sub

'''
''' Remove Keys for OperateVBIDECodePanes key-values cache
'''
''' automatically-added for OperateVBIDECodePanes string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueOperateVBIDECodePanes(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_OPERATE_VBIDE_CODE_PANES_OBJECT_BROWSER") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_OPERATE_VBIDE_CODE_PANES_OBJECT_BROWSER"
            .Remove "STR_KEY_OPERATE_VBIDE_CODE_PANES_CODE"
        End If
    End With
End Sub


'///////////////////////////////////////////////
'/// Sanity tests dependent on users' visual checks
'///////////////////////////////////////////////
'**---------------------------------------------
'** Change WindowState about this VBE
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToTileSelectedWindowsHorizontallyWithClosingObjectBrowser()

    Dim objDic As Scripting.Dictionary, strFileName As String
    
    strFileName = GetCurrentOfficeFileObject().Name
    
    Set objDic = GetTextToTextDicFromLineDelimitedChar("OperateVBIDECodePanes," & strFileName & ",OperateVBIDECodePanesForXl," & strFileName)

    ShowIDECodePaneFromModuleNameToFileNameDic objDic

    TileSelectedWindowsHorizontallyWithClosingBothObjectBrowserAndUFDesigner objDic
End Sub

'''
'''
'''
Private Sub msubTestOfVBProject()

    With GetCurrentOfficeFileObject().VBProject
    
        Debug.Print .Name
        
        Debug.Print .fileName
        
        Debug.Print GetFileNameWithoutUsingFileSystemObject(.fileName)
    End With
End Sub

'''
''' open all UserForm designer
'''
Private Sub msubSanityTestToOpenAllUserFormDesignerPane()

    Dim objVBComponent As VBIDE.VBComponent, objVBProject As VBIDE.VBProject
    
    Set objVBProject = GetCurrentOfficeFileObject().VBProject
    
    For Each objVBComponent In objVBProject.VBComponents
    
        If objVBComponent.Type = vbext_ct_MSForm Then
    
            objVBComponent.Activate ' Open a user-form desinger
        End If
    Next

End Sub

'''
'''
'''
Private Sub msubSanityTestToCloseAllUserFormDesignerPane()

    Dim objVBE As VBIDE.VBE, objWindow As VBIDE.Window, strModuleName As String, strProjectFileName As String, enmVBCodePaneType As VBCodePaneType
    Dim objVBProjectToVBProjectFileNameDic As Scripting.Dictionary


    Set objVBE = GetCurrentOfficeFileObject().Application.VBE

    Set objVBProjectToVBProjectFileNameDic = GetVBProjectToVBProjectFileNameDic()

    For Each objWindow In objVBE.Windows
    
        GetModuleNameAndOfficeFileNameAndVBAWindowPaneTypeFromVBProjects strModuleName, strProjectFileName, enmVBCodePaneType, objWindow.Caption, objVBProjectToVBProjectFileNameDic
        
        If strModuleName <> "" And enmVBCodePaneType = UserFormDesignerAtWindow Then
        
            objWindow.Close
        
            'objWindow.Visible = False  ' In this case, each Window continue to exists in the VBE.Windows cache
        End If
    Next
End Sub

'''
''' Local dependent...
'''
Private Sub msubSanityTestToOpenSpecifiedUserFormDesignerPane()

    Dim objDic As Scripting.Dictionary, strFileName As String
    
    strFileName = GetCurrentOfficeFileObject().Name
    
    Set objDic = New Scripting.Dictionary

    With objDic
    
        .Add "CountUpProgressForm", strFileName
        '.Add "CodePaneSwitchSingleLsvForm", strFileName
    End With

    msubOpenCodePaneAndUFDesignerWithMinimizingExcludedModules objDic

End Sub


'''
'''
'''
Private Sub msubSanityTestOfConfirmUserFormDesignerPane()

    Dim objVBE As VBIDE.VBE, objWindow As VBIDE.Window
    Dim strModuleName As String, strProjectFileName As String
   
    Set objVBE = GetCurrentOfficeFileObject().Application.VBE
    
    For Each objWindow In objVBE.Windows
    
        With objWindow
        
            If IsUserFormDesignViewWindow(.Caption) Then
        
                GetModuleNameAndOfficeFileNameFromVBIDECodePane strModuleName, strProjectFileName, .Caption, objVBE
            
                Debug.Print "Module Name: " & strModuleName
            End If
        End With
    Next
End Sub

