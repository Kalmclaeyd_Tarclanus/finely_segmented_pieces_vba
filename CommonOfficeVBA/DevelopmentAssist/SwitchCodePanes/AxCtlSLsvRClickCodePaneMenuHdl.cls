VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AxCtlSLsvRClickCodePaneMenuHdl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   AxtiveX Control a single ListView Right-Click menu handler
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is dependent on both the MSComctlLib reference and the MSForms reference
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Feb,  7/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private WithEvents mobjListView As MSComctlLib.ListView
Attribute mobjListView.VB_VarHelpID = -1

Private mobjKeyToAxBarBtnEvHdlForSwitchSLsvPaneDic As Scripting.Dictionary

Private mobjApplication As Object    ' Excel.Application, Word.Application, or PowerPoint.Application

Private mstrCommandBarName As String

Private mobjParentForm As Object


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mstrCommandBarName = "UserDefinedAdditionalBar"
    
    Set mobjKeyToAxBarBtnEvHdlForSwitchSLsvPaneDic = New Scripting.Dictionary
End Sub

Private Sub Class_Terminate()

    Set mobjListView = Nothing

    If Not mobjKeyToAxBarBtnEvHdlForSwitchSLsvPaneDic Is Nothing Then
    
        mobjKeyToAxBarBtnEvHdlForSwitchSLsvPaneDic.RemoveAll
    End If
    
    Set mobjKeyToAxBarBtnEvHdlForSwitchSLsvPaneDic = Nothing
End Sub


'''
''' Right click event handler
'''
Private Sub mobjListView_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal x As stdole.OLE_XPOS_PIXELS, ByVal y As stdole.OLE_YPOS_PIXELS)

    Dim i As Long
    Dim objAxBarBtnEvHdlForSwitchSLsvPane As AxBarBtnEvHdlForSwitchSLsvPane, objIndexToRClickSwitchCodePaneMenuTypeDic As Scripting.Dictionary, varIndex As Variant
    Dim enmRClickSwitchCodePaneMenuType As RClickSwitchCodePaneMenuType
    
    If Button <> 2 Then
    
        Exit Sub
    End If

    Set objIndexToRClickSwitchCodePaneMenuTypeDic = New Scripting.Dictionary
    
    With objIndexToRClickSwitchCodePaneMenuTypeDic
    
        .Add 0, RClickSwitchCodePaneMenuType.TileSelectedWindowsHorizontallyMenu
        .Add 1, RClickSwitchCodePaneMenuType.CascadeCodePanesMenu
        .Add 2, RClickSwitchCodePaneMenuType.TileSelectedBothCodesAndUFDesignerHorizontallyMenu
        .Add 3, RClickSwitchCodePaneMenuType.CloseSelectedCodePanesMenu
    End With

    If mobjKeyToAxBarBtnEvHdlForSwitchSLsvPaneDic Is Nothing Then Set mobjKeyToAxBarBtnEvHdlForSwitchSLsvPaneDic = New Scripting.Dictionary

    DeleteCommandBarIfItExists mobjApplication, mstrCommandBarName

    With mobjApplication.CommandBars.Add(Name:=mstrCommandBarName, Position:=Office.MsoBarPosition.msoBarPopup)
     
        For Each varIndex In objIndexToRClickSwitchCodePaneMenuTypeDic.Keys
        
            enmRClickSwitchCodePaneMenuType = objIndexToRClickSwitchCodePaneMenuTypeDic.Item(varIndex)
       
            i = varIndex
        
            Set objAxBarBtnEvHdlForSwitchSLsvPane = New AxBarBtnEvHdlForSwitchSLsvPane
        
            objAxBarBtnEvHdlForSwitchSLsvPane.Initialize i, .Controls.Add(Office.MsoControlType.msoControlButton), enmRClickSwitchCodePaneMenuType, mobjListView, mobjParentForm
        
            msubSetUpKeyToAxBarBtnEvHdlForSwitchSLsvPaneDic i, objAxBarBtnEvHdlForSwitchSLsvPane
        Next
        
        .ShowPopup
        
        .Delete
    End With

End Sub



Private Sub msubSetUpKeyToAxBarBtnEvHdlForSwitchSLsvPaneDic(ByVal vintIndex As Long, ByRef robjAxBarBtnEvHdlForSwitchSLsvPane As AxBarBtnEvHdlForSwitchSLsvPane)

    With mobjKeyToAxBarBtnEvHdlForSwitchSLsvPaneDic
        
        If Not .Exists(vintIndex) Then
        
            .Add vintIndex, robjAxBarBtnEvHdlForSwitchSLsvPane
        Else
            Set .Item(vintIndex) = robjAxBarBtnEvHdlForSwitchSLsvPane
        End If
    End With
End Sub



'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Get ListViewControl() As MSComctlLib.ListView

    Set ListViewControl = mobjListView
End Property


Public Property Get CommandBarName() As String
    CommandBarName = mstrCommandBarName
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
Public Sub Initialize(ByRef robjListView As MSComctlLib.ListView, ByVal vobjApplication As Object, ByVal vobjParentForm As Object, Optional ByVal vstrCommandBarName As String = "")

    Set mobjListView = robjListView
    
    Set mobjApplication = vobjApplication
    
    Set mobjParentForm = vobjParentForm
    
    If vstrCommandBarName <> "" Then
    
        mstrCommandBarName = vstrCommandBarName
    End If
End Sub

