Attribute VB_Name = "OpenCodePaneSwitchForm"
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       First idea; After the integrated codes are divided into AddIns, show more than one this window and each window shows codes of each VB project.
'       In this case, the idea can keeps the smallest width.
'       Second idea; For all codes searving in all AddIns and Macrobooks, one code-pane switch form shows all.
'       In this case, the list items should be restricted to current opened modules.
'
'   Dependency Abstract:
'       This is dependent on the MSForms reference
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun, 15/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation.
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
' Cache
Private mobjProjectNameToCodePaneSwitchMultiLstFormDic As Scripting.Dictionary  ' by ListBox


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
Public Enum ResultOfCreatingObjectInBookModule

    FailedToCreateInBookModule = 0
    
    PassedSuccessfullyCreatingObjectInBookModule = 1
End Enum

'''
'''
'''
Public Enum RClickSwitchCodePaneMenuType

    TileSelectedWindowsHorizontallyMenu
    
    CascadeCodePanesMenu
    
    TileSelectedBothCodesAndUFDesignerHorizontallyMenu
    
    CloseSelectedCodePanesMenu
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Right click menu CommandBarButton setup by RClickSwitchCodePaneMenuType
'**---------------------------------------------
Public Sub SetUpRClickCommandButtonCaptionsFromRClickSwitchCodePaneMenuType(ByRef robjCommandBarButton As Office.CommandBarButton, _
        ByVal venmRClickSwitchCodePaneMenuType As RClickSwitchCodePaneMenuType)


    Select Case venmRClickSwitchCodePaneMenuType
    
        Case RClickSwitchCodePaneMenuType.TileSelectedWindowsHorizontallyMenu   ' In this case, User-form designers are to be closed
        
            With robjCommandBarButton
        
                .Caption = GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionTileSelectedPanes()
                
                .TooltipText = GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextTileSelectedPanes()
            End With
        
        Case RClickSwitchCodePaneMenuType.CascadeCodePanesMenu
    
            With robjCommandBarButton
        
                .Caption = GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionCascadeAllCodePanes()
                
                .TooltipText = GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextCascadeAllCodePanes()
            End With
        
        Case RClickSwitchCodePaneMenuType.TileSelectedBothCodesAndUFDesignerHorizontallyMenu    ' In this case, User-form designers are to be opened
        
            With robjCommandBarButton
        
                .Caption = GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionTileBothPanesAndUfdesigner()
                
                .TooltipText = GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextTileBothPanesAndUfdesigner()
            End With
        
        Case RClickSwitchCodePaneMenuType.CloseSelectedCodePanesMenu
        
            With robjCommandBarButton
            
                .Caption = GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickCaptionCloseSelectedPanes()
            
                .TooltipText = GetTextOfStrKeyCodePaneSwitchListUtilitiesRclickTooltiptextCloseSelectedPanes()
            End With
    End Select
End Sub


'**---------------------------------------------
'** Open forms
'**---------------------------------------------
'''
'''
'''
Public Sub OpenCodePaneSwitchMultiListBoxesFormForAllVBProjectsInCurrentVBE()

    Dim objVBProject As VBIDE.VBProject, objCodePaneSwitchMultiLstForm As CodePaneSwitchMultiLstForm
    Dim varFileName As Variant, strFileName As String, objFileNameToVBProjectDic As Scripting.Dictionary
    Dim objApplication As Object

    If mobjProjectNameToCodePaneSwitchMultiLstFormDic Is Nothing Then
    
        Set mobjProjectNameToCodePaneSwitchMultiLstFormDic = New Scripting.Dictionary
    End If
    
    Set objFileNameToVBProjectDic = GetVBProjectFileNameToVBProjectDic()
    
    Set objApplication = GetCurrentOfficeFileObject().Application
    
    ' If any VB projects, which no code pane is opened, don't show Form
    
    With GetOpenedIDECodePaneOfFileNameClassifiedTypeNameModuleDic()
    
        For Each varFileName In .Keys
        
            strFileName = varFileName
            
            Set objVBProject = objFileNameToVBProjectDic.Item(strFileName)
        
            Set objCodePaneSwitchMultiLstForm = New CodePaneSwitchMultiLstForm
    
            With objCodePaneSwitchMultiLstForm
                
                .Initialize objVBProject, strFileName, objApplication
            
                .Show vbModeless
            End With
        
            With mobjProjectNameToCodePaneSwitchMultiLstFormDic
            
                If Not .Exists(objVBProject.Name) Then
                
                    .Add objVBProject.Name, objCodePaneSwitchMultiLstForm
                Else
                    Set .Item(objVBProject.Name) = objCodePaneSwitchMultiLstForm
                End If
            End With
        Next
    End With
End Sub


'''
'''   In the case of some AddIns are opened, or other macro-books are opened
'''
Public Sub OpenCodePaneSwitchMultiLstFormForSingleVBProject()

    Dim objVBProject As VBIDE.VBProject, objCodePaneSwitchMultiLstForm As CodePaneSwitchMultiLstForm

    Dim objApplication As Object, strFileName As String
    

    If mobjProjectNameToCodePaneSwitchMultiLstFormDic Is Nothing Then
    
        Set mobjProjectNameToCodePaneSwitchMultiLstFormDic = New Scripting.Dictionary
    End If

    With GetCurrentOfficeFileObject()
    
        strFileName = .Name
        
        Set objVBProject = .VBProject
        
        Set objApplication = .Application
    End With

    Set objCodePaneSwitchMultiLstForm = New CodePaneSwitchMultiLstForm
    
    With objCodePaneSwitchMultiLstForm
        
        .Initialize objVBProject, strFileName, objApplication
    
        .Show vbModeless
    End With
    
    With mobjProjectNameToCodePaneSwitchMultiLstFormDic
    
        If Not .Exists(objVBProject.Name) Then
        
            .Add objVBProject.Name, objCodePaneSwitchMultiLstForm
        Else
            Set .Item(objVBProject.Name) = objCodePaneSwitchMultiLstForm
        End If
    End With
End Sub
