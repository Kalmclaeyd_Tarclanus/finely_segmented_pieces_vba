VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} CodePaneSwitchMultiLsvForm 
   Caption         =   "CodePaneSwitchMultiLsvForm"
   ClientHeight    =   5770
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   3010
   OleObjectBlob   =   "CodePaneSwitchMultiLsvForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "CodePaneSwitchMultiLsvForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   VB project code-pane selecting form by multi ListView controls
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'       Dependent on VBIDE, MSForms.UserForm, MSComctlLib.ListView
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat,  4/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then
    ' For 64 bit version windows, declare timeGetTime
    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
#Else
    ' For 32 bit version windows, declare timeGetTime
    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
#End If

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "CodePaneSwitchMultiLsvForm"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjVBProject As VBIDE.VBProject

Private mstrFileName As String  ' If the office file isn't saved at least one, then the VBProject.fileName returns null string. this prevents such causing error

Private mobjApplication As Object

'**---------------------------------------------
'** Form window order control
'**---------------------------------------------
Private mobjFormTopMostEnabledCtlHdr As FormTopMostEnabledCtlHdr

'**---------------------------------------------
'** Semaphore parameter cache within user-control objects initializing
'**---------------------------------------------
Private mintInitializing As Long

Private mblnIsInitialized As Boolean

'**---------------------------------------------
'** Form layout-size cache
'**---------------------------------------------
Private mudtLsvStandardModulesDefaultSize As ResizingAxControlSize

Private mudtLsvClassModulesDefaultSize As ResizingAxControlSize

Private mudtLsvFormModulesDefaultSize As ResizingAxControlSize

Private mudtLsvObjectModulesDefaultSize As ResizingAxControlSize

'**---------------------------------------------
'** List-views cache to control by outside
'**---------------------------------------------
Private mobjListViews As Collection

'**---------------------------------------------
'** Right click menu control cache
'**---------------------------------------------
Private mobjKeyToAxCtlLsvCodeModulePaneHdlDic As Scripting.Dictionary

Private mobjKeyToAxCtlLsvRClickCodePaneMenuHdlDic As Scripting.Dictionary


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub UserForm_Initialize()

    mintInitializing = 0

    mblnIsInitialized = False
    
    InitializeFormTopMostEnabledControlHandler mobjFormTopMostEnabledCtlHdr, Me, True
    
    msubInitiazliResizingControlsParameters
End Sub

Private Sub UserForm_Terminate()

    Set mobjFormTopMostEnabledCtlHdr = Nothing

    Set mobjVBProject = Nothing

    Set mobjApplication = Nothing


    mobjKeyToAxCtlLsvRClickCodePaneMenuHdlDic.RemoveAll
    
    Set mobjKeyToAxCtlLsvRClickCodePaneMenuHdlDic = Nothing

    msubTerminateCodeModuleListPaneHandlers
End Sub

Private Sub UserForm_Layout()

    If Not mblnIsInitialized Then
    
        msubLocalize
        
        msuInitializeListViews
        
        msubInitializeCodeModuleListPaneHandlers
        
        mblnIsInitialized = True
    End If

    RefreshOpenedModuleNames

    ModifyFormStyle Me
End Sub

Private Sub UserForm_Resize()

    msubAdjustWidthOfControls
End Sub

Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)

    SetCurrentFormPositionToUserReg mstrModuleName, Me, mobjVBProject.Name
    
    SetCurrentFormSizeToUserReg mstrModuleName, Me, mobjVBProject.Name
End Sub



'''
''' Refresh code-file lists
'''
Private Sub UserForm_DblClick(ByVal Cancel As MSForms.ReturnBoolean)

    RefreshOpenedModuleNames
    
    GetCurrentOfficeFileObject().Application.VBE.MainWindow.SetFocus
End Sub

'''
''' Remove selections from all ListViews
'''
Private Sub UserForm_Click()

    RemoveAllSelectionsOfListViews mfobjGetModuleNamesListViewCol()
End Sub
'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vobjVBProject</Argument>
''' <Argument>rstrFileName</Argument>
''' <Argument>vobjApplication</Argument>
Public Function Initialize(ByVal vobjVBProject As VBIDE.VBProject, _
        ByRef rstrFileName As String, _
        ByVal vobjApplication As Object)


    Set mobjVBProject = vobjVBProject
    
    mstrFileName = rstrFileName
    
    Set mobjApplication = vobjApplication
    
    GetCurrentFormPositionFromUserReg mstrModuleName, Me, mobjVBProject.Name

    GetCurrentFormSizeFromUserReg mstrModuleName, Me, mobjVBProject.Name
End Function

'''
'''
'''
''' <Argument>vobjObject: For example, Excel.Workbook, Word.Document, PowerPoint.Presentation</Argument>
Public Sub InitializeByOfficeFileObject(ByVal vobjObject As Object)

    With vobjObject
    
        Me.Initialize .VBProject, .Name, .Application
    End With
End Sub

'''
''' interface for AxBarBtnEvHdlForSwitchLsvPane
'''
Public Sub CloseFormOfRefreshOpenedModuleNames()

    Unload Me
End Sub

'''
'''
'''
Public Sub RefreshOpenedModuleNames()

    Dim objClassifiedTypeToModuleNameDic As Scripting.Dictionary, objComponentTypeToCountOfModulesDic As Scripting.Dictionary
    Dim objOrderToLabelControlDic As Scripting.Dictionary, objOrderToListControlDic As Scripting.Dictionary
    
    GetTwoTypeDicsAboutClassifiedTypeToModuleNameAndComponentTypeToCountOfModules objClassifiedTypeToModuleNameDic, objComponentTypeToCountOfModulesDic, mstrFileName
    
    If Not objClassifiedTypeToModuleNameDic Is Nothing Then
    
        msubSetAllVisibleList
        
        RefreshEachModuleTypeListViewesFromClassifiedTypeToModuleNameDic lsvStandardModules, lsvClassModules, lsvFormModules, lsvObjectModules, objClassifiedTypeToModuleNameDic
        
        msubGetAlignOrdersAndControlVisiblePropertyForList objOrderToLabelControlDic, objOrderToListControlDic, objClassifiedTypeToModuleNameDic
    
        AdjustListControlsForSwitchCodePanesForm objOrderToLabelControlDic, objOrderToListControlDic, objComponentTypeToCountOfModulesDic, Me, True
    
        ' Remove all selections state
        RemoveAllSelectionsOfListViews mfobjGetModuleNamesListViewCol()
    Else
        Unload Me
    End If
End Sub

'''
''' Avoiding the Excel ListView position change error. The ListView control position is not updated rarely when the Object type interface is used
'''
Public Sub AdjustListControlPos(ByRef robjListControl As Object, ByRef rintLeft As Long, ByRef rintTop As Long)

    If robjListControl Is lsvStandardModules Then
    
        With lsvStandardModules
        
            .Left = rintLeft: .Top = rintTop
            
            Interaction.DoEvents: Interaction.DoEvents: Interaction.DoEvents
        End With
    
    ElseIf robjListControl Is lsvClassModules Then
    
        With lsvClassModules
        
            .Left = rintLeft: .Top = rintTop
            
            Interaction.DoEvents: Interaction.DoEvents: Interaction.DoEvents
        End With
    
    ElseIf robjListControl Is lsvFormModules Then
    
        With lsvFormModules
        
            .Left = rintLeft: .Top = rintTop
            
            Interaction.DoEvents: Interaction.DoEvents: Interaction.DoEvents
        End With
    
    ElseIf robjListControl Is lsvObjectModules Then
    
        With lsvObjectModules
        
            .Left = rintLeft: .Top = rintTop
            
            Interaction.DoEvents: Interaction.DoEvents: Interaction.DoEvents
        End With
    End If
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>Dictionary(Of Long[GeneralVBComponentType], Of String[ModuleName])</Argument>
''' <Return>Collection(Of Long[VBIDE.vbext_ComponentType])</Return>
Private Sub msubGetAlignOrdersAndControlVisiblePropertyForList(ByRef robjOrderToLabelControlDic As Scripting.Dictionary, ByRef robjOrderToListControlDic As Scripting.Dictionary, ByVal vobjClassifiedTypeToModuleNameDic As Scripting.Dictionary)

    Set robjOrderToLabelControlDic = New Scripting.Dictionary
    Set robjOrderToListControlDic = New Scripting.Dictionary

    With vobjClassifiedTypeToModuleNameDic
    
        If .Exists(GeneralVBComponentType.GStdModule) Then
        
            lblStandardModules.Visible = True: lsvStandardModules.Visible = True
            
            robjOrderToLabelControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_StdModule, lblStandardModules
            
            robjOrderToListControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_StdModule, lsvStandardModules
            
        Else
            lblStandardModules.Visible = False: lsvStandardModules.Visible = False
        End If
    
        If .Exists(GeneralVBComponentType.GClassModule) Then
        
            lblClassModules.Visible = True: lsvClassModules.Visible = True
        
            robjOrderToLabelControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_ClassModule, lblClassModules
            
            robjOrderToListControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_ClassModule, lsvClassModules
            
        Else
            lblClassModules.Visible = False: lsvClassModules.Visible = False
        End If
    
        If .Exists(GeneralVBComponentType.GMSForm) Then
        
            lblFormModules.Visible = True: lsvFormModules.Visible = True
        
            robjOrderToLabelControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_MSForm, lblFormModules
            
            robjOrderToListControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_MSForm, lsvFormModules
        
        Else
            lblFormModules.Visible = False: lsvFormModules.Visible = False
        End If
        
        
        If .Exists(GeneralVBComponentType.GVbaObjectModule) Or .Exists(GeneralVBComponentType.GVbaBookObjectModule) Or .Exists(GeneralVBComponentType.GVbaSheetObjectModule) Then
        
            lblObjectModules.Visible = True: lsvObjectModules.Visible = True
            
            robjOrderToLabelControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_Document, lblObjectModules
            
            robjOrderToListControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_Document, lsvObjectModules
            
        Else
            lblObjectModules.Visible = False: lsvObjectModules.Visible = False
        End If
    End With
End Sub


'''
'''
'''
Private Sub msubLocalize()

    Dim objFS As Scripting.FileSystemObject
    
    
    If Not mobjVBProject Is Nothing Then
    
        Me.Caption = mobjVBProject.Name & " - " & GetTextOfStrKeyCodePaneSwitchListUtilitiesFormCaptionSwitchCodePanes() & " ( " & mstrFileName & " )"
    Else
        Me.Caption = GetTextOfStrKeyCodePaneSwitchListUtilitiesFormCaptionSwitchCodePanes()
    End If

    LocalizeLabelForVBAModuleSwitchForm lblStandardModules, lblClassModules, lblFormModules, lblObjectModules
End Sub


'''
'''
'''
Private Sub msubInitializeCodeModuleListPaneHandlers()

    Dim varListView As Variant, objListView As MSComctlLib.ListView
    Dim objAxCtlLsvCodeModulePaneHdl As AxCtlLsvCodeModulePaneHdl
    Dim objAxCtlLsvRClickCodePaneMenuHdl As AxCtlLsvRClickCodePaneMenuHdl
    Dim i As Long, objListViews As Collection, strFileNamePart As String
    
    Set mobjKeyToAxCtlLsvCodeModulePaneHdlDic = New Scripting.Dictionary
    
    Set mobjKeyToAxCtlLsvRClickCodePaneMenuHdlDic = New Scripting.Dictionary
    
    Set objListViews = mfobjGetModuleNamesListViewCol()
    
    strFileNamePart = Replace(mstrFileName, ".", "")
    
    i = 0
    
    For Each varListView In objListViews

        Set objListView = varListView
        
        Set objAxCtlLsvCodeModulePaneHdl = New AxCtlLsvCodeModulePaneHdl
        
        objAxCtlLsvCodeModulePaneHdl.Initialize objListView, mobjVBProject, mstrFileName
    
        mobjKeyToAxCtlLsvCodeModulePaneHdlDic.Add i, objAxCtlLsvCodeModulePaneHdl
    
    
        Set objAxCtlLsvRClickCodePaneMenuHdl = New AxCtlLsvRClickCodePaneMenuHdl
        
        objAxCtlLsvRClickCodePaneMenuHdl.Initialize objListView, mobjApplication, mobjVBProject, mstrFileName, mobjListViews, Me, strFileNamePart & "RightClickSwitchCodePaneListViewMenu" & Format(i, "00")
    
        mobjKeyToAxCtlLsvRClickCodePaneMenuHdlDic.Add i, objAxCtlLsvRClickCodePaneMenuHdl
        
        i = i + 1
    Next
End Sub

'''
'''
'''
Private Function mfobjGetModuleNamesListViewCol() As Collection

    If mobjListViews Is Nothing Then
    
        Set mobjListViews = New Collection
        
        With mobjListViews
        
            .Add lsvStandardModules
            
            .Add lsvClassModules
            
            .Add lsvFormModules
            
            .Add lsvObjectModules
        End With
    End If

    Set mfobjGetModuleNamesListViewCol = mobjListViews
End Function


Private Sub msubSetAllVisibleList()

    ' Before refresh list, each control needs to visible

    Dim varListView As Variant, objListView As Object
    
    For Each varListView In mobjListViews
    
        Set objListView = varListView
        
        With objListView
        
            If Not .Visible Then
            
                .Visible = True
                '.Enabled = True
            End If
        End With
    Next
End Sub


'''
'''
'''
Private Sub msubTerminateCodeModuleListPaneHandlers()

    mobjKeyToAxCtlLsvCodeModulePaneHdlDic.RemoveAll
    
    Set mobjKeyToAxCtlLsvCodeModulePaneHdlDic = Nothing
End Sub


Private Sub msuInitializeListViews()

    InitializeProcedureModuleNameListView lsvStandardModules

    InitializeProcedureModuleNameListView lsvClassModules
    
    InitializeProcedureModuleNameListView lsvFormModules
    
    InitializeProcedureModuleNameListView lsvObjectModules
End Sub


'''
'''
'''
Private Sub msubInitiazliResizingControlsParameters()

    GetDefaultResizingAxControlSize mudtLsvStandardModulesDefaultSize, Me, lsvStandardModules
    
    GetDefaultResizingAxControlSize mudtLsvClassModulesDefaultSize, Me, lsvClassModules
    
    GetDefaultResizingAxControlSize mudtLsvFormModulesDefaultSize, Me, lsvFormModules
    
    GetDefaultResizingAxControlSize mudtLsvObjectModulesDefaultSize, Me, lsvObjectModules
End Sub

'''
'''
'''
Private Sub msubAdjustWidthOfControls()

    AdjustControlWidthAlongWithFormBottomRight mudtLsvStandardModulesDefaultSize, Me, lsvStandardModules
    
    AdjustControlWidthAlongWithFormBottomRight mudtLsvClassModulesDefaultSize, Me, lsvClassModules
    
    AdjustControlWidthAlongWithFormBottomRight mudtLsvFormModulesDefaultSize, Me, lsvFormModules
    
    AdjustControlWidthAlongWithFormBottomRight mudtLsvObjectModulesDefaultSize, Me, lsvObjectModules
End Sub

