VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} CodePaneSwitchMultiLstForm 
   Caption         =   "CodePaneSwitchMultiLstForm"
   ClientHeight    =   6180
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   3140
   OleObjectBlob   =   "CodePaneSwitchMultiLstForm.frx":0000
End
Attribute VB_Name = "CodePaneSwitchMultiLstForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   VB project code-pane selecting form by multi ListBox controls
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'       Dependent on VBIDE, MSForms.UserForm, MSForms.ListBox
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 29/Aug/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "CodePaneSwitchMultiLstForm"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjVBProject As VBIDE.VBProject

Private mstrFileName As String  ' If the office file isn't saved at least one, then the VBProject.fileName returns null string. this prevents such causing error

Private mobjApplication As Object

'**---------------------------------------------
'** Form window order control
'**---------------------------------------------
Private mobjFormTopMostEnabledCtlHdr As FormTopMostEnabledCtlHdr

'**---------------------------------------------
'** Semaphore parameter cache within user-control objects initializing
'**---------------------------------------------
Private mintInitializing As Long

Private mblnIsInitialized As Boolean

'**---------------------------------------------
'** Form layout-size cache
'**---------------------------------------------
Private mudtLstStandardModulesDefaultSize As ResizingAxControlSize
Private mudtLstClassModulesDefaultSize As ResizingAxControlSize
Private mudtLstFormModulesDefaultSize As ResizingAxControlSize
Private mudtLstObjectModulesDefaultSize As ResizingAxControlSize

'**---------------------------------------------
'** List-boxes cache to control by outside
'**---------------------------------------------
Private mobjListBoxes As Collection

'**---------------------------------------------
'** Right click menu control cache
'**---------------------------------------------
Private mobjKeyToAxCtlLstCodeModulePaneHdlDic As Scripting.Dictionary

Private mobjKeyToAxCtlLstRClickCodePaneMenuHdlDic As Scripting.Dictionary


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub UserForm_Initialize()

    mintInitializing = 0

    mblnIsInitialized = False
    
    InitializeFormTopMostEnabledControlHandler mobjFormTopMostEnabledCtlHdr, Me, True
    
    msubInitiazliResizingControlsParameters
End Sub

Private Sub UserForm_Terminate()

    mobjKeyToAxCtlLstRClickCodePaneMenuHdlDic.RemoveAll
    
    Set mobjKeyToAxCtlLstRClickCodePaneMenuHdlDic = Nothing

    msubTerminateCodeModuleListPaneHandlers
End Sub

'''
'''
'''
Private Sub UserForm_Layout()

    If Not mblnIsInitialized Then
    
        msubLocalize
        
        msubInitializeCodeModuleListPaneHandlers
    
        mblnIsInitialized = True
    End If

    RefreshOpenedModuleNames
    
    ModifyFormStyle Me
End Sub

Private Sub UserForm_Resize()

    msubAdjustWidthOfControls
End Sub

Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)

    SetCurrentFormPositionToUserReg mstrModuleName, Me, mobjVBProject.Name
    
    SetCurrentFormSizeToUserReg mstrModuleName, Me, mobjVBProject.Name
End Sub

'''
''' Refresh code-file lists
'''
Private Sub UserForm_DblClick(ByVal Cancel As MSForms.ReturnBoolean)

    RefreshOpenedModuleNames
    
    GetCurrentOfficeFileObject().Application.VBE.MainWindow.SetFocus
End Sub

'''
''' Remove selections from all ListBoxes
'''
Private Sub UserForm_Click()

    RemoveAllSelectionsOfListBoxes mfobjGetModuleNamesListBoxCol()
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Get VBProjectName() As String

    VBProjectName = mobjVBProject.Name
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vobjVBProject</Argument>
''' <Argument>rstrFileName</Argument>
''' <Argument>vobjApplication</Argument>
Public Sub Initialize(ByVal vobjVBProject As VBIDE.VBProject, _
        ByRef rstrFileName As String, _
        ByVal vobjApplication As Object)

    Set mobjVBProject = vobjVBProject
    
    
    mstrFileName = rstrFileName
    
    Set mobjApplication = vobjApplication
    
    GetCurrentFormPositionFromUserReg mstrModuleName, Me, mobjVBProject.Name
    
    GetCurrentFormSizeFromUserReg mstrModuleName, Me, mobjVBProject.Name
End Sub

'''
'''
'''
''' <Argument>vobjObject: For example, Excel.Workbook, Word.Document, PowerPoint.Presentation</Argument>
Public Sub InitializeByOfficeFileObject(ByVal vobjObject As Object)

    With vobjObject
    
        Me.Initialize .VBProject, .Name, .Application
    End With
End Sub


'''
''' interface for AxBarBtnEvHdlForSwitchLstPane
'''
Public Sub CloseFormOfRefreshOpenedModuleNames()

    Unload Me
End Sub

'''
''' Interface
'''
Public Sub RefreshOpenedModuleNames()

    Dim objClassifiedTypeToModuleNameDic As Scripting.Dictionary, objComponentTypeToCountOfModulesDic As Scripting.Dictionary
    Dim objOrderToLabelControlDic As Scripting.Dictionary, objOrderToListControlDic As Scripting.Dictionary
    
    GetTwoTypeDicsAboutClassifiedTypeToModuleNameAndComponentTypeToCountOfModules objClassifiedTypeToModuleNameDic, objComponentTypeToCountOfModulesDic, mstrFileName
    
    If Not objClassifiedTypeToModuleNameDic Is Nothing Then
    
        msubSetAllVisibleList
        
        RefreshEachModuleTypeListBoxesFromClassifiedTypeToModuleNameDic lstStandardModules, lstClassModules, lstFormModules, lstObjectModules, objClassifiedTypeToModuleNameDic
        
        msubGetAlignOrdersAndControlVisiblePropertyForList objOrderToLabelControlDic, objOrderToListControlDic, objClassifiedTypeToModuleNameDic
    
        AdjustListControlsForSwitchCodePanesForm objOrderToLabelControlDic, objOrderToListControlDic, objComponentTypeToCountOfModulesDic, Me, False
    Else
    
        Unload Me
    End If
End Sub


'''
''' Avoiding the Excel ListView position change error. The ListBox control position is not updated rarely when the Object type interface is used
'''
Public Sub AdjustListControlPos(ByRef robjListControl As Object, ByRef rintLeft As Long, ByRef rintTop As Long)

    If robjListControl Is lstStandardModules Then
    
        With lstStandardModules
        
            .Left = rintLeft
            .Top = rintTop
            
            Interaction.DoEvents: Interaction.DoEvents: Interaction.DoEvents
        End With
    
    ElseIf robjListControl Is lstClassModules Then
    
        With lstClassModules
        
            .Left = rintLeft
            .Top = rintTop
            
            Interaction.DoEvents: Interaction.DoEvents: Interaction.DoEvents
        End With
    
    ElseIf robjListControl Is lstFormModules Then
    
        With lstFormModules
        
            .Left = rintLeft
            .Top = rintTop
            
            Interaction.DoEvents: Interaction.DoEvents: Interaction.DoEvents
        End With
    
    ElseIf robjListControl Is lstObjectModules Then
    
        With lstObjectModules
        
            .Left = rintLeft
            .Top = rintTop
            
            Interaction.DoEvents: Interaction.DoEvents: Interaction.DoEvents
        End With
    End If
End Sub


'//////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////



'''
'''
'''
''' <Argument>Dictionary(Of Long[GeneralVBComponentType], Of String[ModuleName])</Argument>
''' <Return>Collection(Of Long[VBIDE.vbext_ComponentType])</Return>
Private Sub msubGetAlignOrdersAndControlVisiblePropertyForList(ByRef robjOrderToLabelControlDic As Scripting.Dictionary, ByRef robjOrderToListControlDic As Scripting.Dictionary, ByVal vobjClassifiedTypeToModuleNameDic As Scripting.Dictionary)

    Set robjOrderToLabelControlDic = New Scripting.Dictionary
    Set robjOrderToListControlDic = New Scripting.Dictionary

    With vobjClassifiedTypeToModuleNameDic
    
        If .Exists(GeneralVBComponentType.GStdModule) Then
        
            lblStandardModules.Visible = True
            lstStandardModules.Visible = True
            
            robjOrderToLabelControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_StdModule, lblStandardModules
            robjOrderToListControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_StdModule, lstStandardModules
            
        Else
            lblStandardModules.Visible = False
            lstStandardModules.Visible = False
        End If
    
        If .Exists(GeneralVBComponentType.GClassModule) Then
        
            lblClassModules.Visible = True
            lstClassModules.Visible = True
        
            robjOrderToLabelControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_ClassModule, lblClassModules
            robjOrderToListControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_ClassModule, lstClassModules
            
        Else
            lblClassModules.Visible = False
            lstClassModules.Visible = False
        End If
    
        If .Exists(GeneralVBComponentType.GMSForm) Then
        
            lblFormModules.Visible = True
            lstFormModules.Visible = True
        
            robjOrderToLabelControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_MSForm, lblFormModules
            robjOrderToListControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_MSForm, lstFormModules
        
        Else
            lblFormModules.Visible = False
            lstFormModules.Visible = False
        End If
        
        
        If .Exists(GeneralVBComponentType.GVbaObjectModule) Or .Exists(GeneralVBComponentType.GVbaBookObjectModule) Or .Exists(GeneralVBComponentType.GVbaSheetObjectModule) Then
        
            lblObjectModules.Visible = True
            lstObjectModules.Visible = True
            
            robjOrderToLabelControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_Document, lblObjectModules
            robjOrderToListControlDic.Add VBIDE.vbext_ComponentType.vbext_ct_Document, lstObjectModules
            
        Else
            lblObjectModules.Visible = False
            lstObjectModules.Visible = False
        End If
    
    End With

End Sub



Private Sub msubLocalize()

    Dim objFS As Scripting.FileSystemObject
    
    If Not mobjVBProject Is Nothing Then
    
        Set objFS = New Scripting.FileSystemObject
        
        Me.Caption = mobjVBProject.Name & " - " & GetTextOfStrKeyCodePaneSwitchListUtilitiesFormCaptionSwitchCodePanes() & " ( " & mstrFileName & " )"
    Else
        Me.Caption = GetTextOfStrKeyCodePaneSwitchListUtilitiesFormCaptionSwitchCodePanes()
    End If

    LocalizeLabelForVBAModuleSwitchForm lblStandardModules, lblClassModules, lblFormModules, lblObjectModules
End Sub

'''
'''
'''
Private Sub msubInitializeCodeModuleListPaneHandlers()

    Dim varListBox As Variant, objListBox As MSForms.ListBox
    Dim objAxCtlLstCodeModulePaneHdl As AxCtlLstCodeModulePaneHdl
    Dim objAxCtlLstRClickCodePaneMenuHdl As AxCtlLstRClickCodePaneMenuHdl
    Dim i As Long, objListBoxes As Collection, strFileNamePart As String
    
    Set mobjKeyToAxCtlLstCodeModulePaneHdlDic = New Scripting.Dictionary
    
    Set mobjKeyToAxCtlLstRClickCodePaneMenuHdlDic = New Scripting.Dictionary
    
    Set objListBoxes = mfobjGetModuleNamesListBoxCol()
    
    strFileNamePart = Replace(mstrFileName, ".", "")
    
    i = 0
    For Each varListBox In objListBoxes

        Set objListBox = varListBox
        
        Set objAxCtlLstCodeModulePaneHdl = New AxCtlLstCodeModulePaneHdl
        
        objAxCtlLstCodeModulePaneHdl.Initialize objListBox, mobjVBProject, mstrFileName
    
        mobjKeyToAxCtlLstCodeModulePaneHdlDic.Add i, objAxCtlLstCodeModulePaneHdl
    
    
        Set objAxCtlLstRClickCodePaneMenuHdl = New AxCtlLstRClickCodePaneMenuHdl
        
        objAxCtlLstRClickCodePaneMenuHdl.Initialize objListBox, mobjApplication, mobjVBProject, mstrFileName, mobjListBoxes, Me, strFileNamePart & "RightClickSwitchCodePaneListBoxMenu" & Format(i, "00")
    
        mobjKeyToAxCtlLstRClickCodePaneMenuHdlDic.Add i, objAxCtlLstRClickCodePaneMenuHdl
        
        i = i + 1
    Next
End Sub


Private Function mfobjGetModuleNamesListBoxCol() As Collection

    If mobjListBoxes Is Nothing Then
    
        Set mobjListBoxes = New Collection
    
        With mobjListBoxes
        
            .Add lstStandardModules
            .Add lstClassModules
            .Add lstFormModules
            .Add lstObjectModules
        End With
    End If

    Set mfobjGetModuleNamesListBoxCol = mobjListBoxes
End Function


Private Sub msubSetAllVisibleList()

    ' Before refresh list, each control needs to visible

    Dim varListBox As Variant, objListBox As Object
    
    For Each varListBox In mobjListBoxes
    
        Set objListBox = varListBox
        
        With objListBox
        
            If Not .Visible Then
            
                .Visible = True
            End If
        End With
    Next
End Sub

'''
'''
'''
Private Sub msubTerminateCodeModuleListPaneHandlers()

    If Not mobjKeyToAxCtlLstCodeModulePaneHdlDic Is Nothing Then
    
    mobjKeyToAxCtlLstCodeModulePaneHdlDic.RemoveAll
    End If
    
    If Not mobjKeyToAxCtlLstRClickCodePaneMenuHdlDic Is Nothing Then
    
        mobjKeyToAxCtlLstRClickCodePaneMenuHdlDic.RemoveAll
    End If
    
    Set mobjKeyToAxCtlLstCodeModulePaneHdlDic = Nothing
    
    Set mobjKeyToAxCtlLstRClickCodePaneMenuHdlDic = Nothing
End Sub

'''
'''
'''
Private Sub msubInitiazliResizingControlsParameters()

    GetDefaultResizingAxControlSize mudtLstStandardModulesDefaultSize, Me, lstStandardModules
    GetDefaultResizingAxControlSize mudtLstClassModulesDefaultSize, Me, lstClassModules
    GetDefaultResizingAxControlSize mudtLstFormModulesDefaultSize, Me, lstFormModules
    GetDefaultResizingAxControlSize mudtLstObjectModulesDefaultSize, Me, lstObjectModules
End Sub

'''
'''
'''
Private Sub msubAdjustWidthOfControls()

    AdjustControlWidthAlongWithFormBottomRight mudtLstStandardModulesDefaultSize, Me, lstStandardModules
    AdjustControlWidthAlongWithFormBottomRight mudtLstClassModulesDefaultSize, Me, lstClassModules
    AdjustControlWidthAlongWithFormBottomRight mudtLstFormModulesDefaultSize, Me, lstFormModules
    AdjustControlWidthAlongWithFormBottomRight mudtLstObjectModulesDefaultSize, Me, lstObjectModules
End Sub


