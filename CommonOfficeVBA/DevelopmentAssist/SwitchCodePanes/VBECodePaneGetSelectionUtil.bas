Attribute VB_Name = "VBECodePaneGetSelectionUtil"
'
'   Serve VBIDE code pane information
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on VBIDE
'       Dependent on GetCurrentOfficeFileObject() method of the InterfaceCall.bas
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 20/Feb/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vobjSelectedModuleNames: Input</Argument>
''' <Argument>vobjVBProject: Input</Argument>
''' <Return>Dictionary</Return>
Public Function GetSelectedModuleNameToFileNameDicBySpecifiedVBProject(ByVal vobjSelectedModuleNames As Collection, _
        ByVal vobjVBProject As VBIDE.VBProject) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, strFileName As String, varModuleName As Variant
    
    
    strFileName = GetFileNameWithoutUsingFileSystemObject(vobjVBProject.fileName)
    
    Set objDic = New Scripting.Dictionary
    
    For Each varModuleName In vobjSelectedModuleNames
    
        objDic.Add varModuleName, strFileName
    Next

    Set GetSelectedModuleNameToFileNameDicBySpecifiedVBProject = objDic
End Function
'''
'''
'''
Public Sub CollectAModuleVBAFunctionBasicInfoFromVBECurrentSelection()

    Dim objVBProject As VBIDE.VBProject, objCodePane As VBIDE.CodePane, strCurrentOfficeFileName As String
    
    Dim intStartLine As Long, intStartColumn As Long, intEndLine As Long, intEndColumn As Long
    
    Dim strModuleName As String, strCodePaneWindowFileName As String, objVBComponent As VBIDE.VBComponent
    
    Dim objDataTableCol As Collection
    
    
    GetCurrentVBECursorInfo objCodePane, _
            strCurrentOfficeFileName, _
            objVBProject, _
            intStartLine, _
            intStartColumn, _
            intEndLine, _
            intEndColumn
    
    
    If Not objCodePane Is Nothing Then
    
        GetModuleNameAndOfficeFileNameFromVBIDECodePane strModuleName, _
                strCodePaneWindowFileName, _
                objCodePane.Window.Caption, _
                objCodePane.VBE
    
        If StrComp(strCodePaneWindowFileName, strCodePaneWindowFileName) = 0 Then
        
            Set objVBComponent = objVBProject.VBComponents.Item(strModuleName)
            
            Set objDataTableCol = New Collection
            
            CollectAModuleVBAFunctionBasicInfoFromLineNumber objDataTableCol, objVBProject, objVBComponent, intStartLine
            
            DebugCol objDataTableCol
        End If
    End If
End Sub


'''
''' get current CodePane.GetSelection info
'''
''' <Argument>robjCodePane: Input</Argument>
''' <Argument>rstrCurrentOfficeFileName: Output</Argument>
''' <Argument>robjVBProject: Output</Argument>
''' <Argument>rintStartLine: Output</Argument>
''' <Argument>rintStartColumn: Output</Argument>
''' <Argument>rintEndLine: Output</Argument>
''' <Argument>rintEndColumn: Output</Argument>
Public Sub GetCurrentVBECursorInfo(ByRef robjCodePane As VBIDE.CodePane, _
        ByRef rstrCurrentOfficeFileName As String, _
        ByRef robjVBProject As VBIDE.VBProject, _
        ByRef rintStartLine As Long, _
        ByRef rintStartColumn As Long, _
        ByRef rintEndLine As Long, _
        ByRef rintEndColumn As Long)


    Dim objVBE As VBIDE.VBE, objVBComponent As VBIDE.VBComponent
    
    With GetCurrentOfficeFileObject()
    
        Set robjVBProject = .VBProject
        
        rstrCurrentOfficeFileName = .Name
    End With
    
    Set objVBE = robjVBProject.VBE
    
    With objVBE
    
        If Not .ActiveCodePane Is Nothing Then
        
            Set robjCodePane = .ActiveCodePane
            
            robjCodePane.GetSelection rintStartLine, rintStartColumn, rintEndLine, rintEndColumn
            
            Debug.Print "VBE-Current line number: " & CStr(rintStartLine) & ", " & CStr(rintStartColumn) & ", " & CStr(rintEndLine) & ", " & CStr(rintEndColumn)
        Else
            Set robjCodePane = Nothing
        
            Debug.Print "Any CodePane is not opened."
        End If
    End With
End Sub


'''
'''
'''
''' <Argument>robjDataTableCol: Output</Argument>
''' <Argument>vobjVBProject: Input</Argument>
''' <Argument>vobjVBComponent: Input</Argument>
''' <Argument>rintFunctionDefinitionRegionLine: Input</Argument>
Public Sub CollectAModuleVBAFunctionBasicInfoFromLineNumber(ByRef robjDataTableCol As Collection, _
        ByVal vobjVBProject As VBIDE.VBProject, _
        ByVal vobjVBComponent As VBIDE.VBComponent, _
        ByRef rintFunctionDefinitionRegionLine As Long)


    Dim enmReceivedProcKind As VBIDE.vbext_ProcKind
    
    Dim intProcBodyLine As Long, intProcStartLine As Long, intProcCountLines As Long
    
    Dim objRowCol As Collection
    
    Dim strProcName As String
    

    With vobjVBComponent.CodeModule
    
        strProcName = .ProcOfLine(rintFunctionDefinitionRegionLine, enmReceivedProcKind)
    
        If strProcName <> "" Then
    
            intProcStartLine = .ProcStartLine(strProcName, enmReceivedProcKind)
            
            intProcBodyLine = .ProcBodyLine(strProcName, enmReceivedProcKind)
            
            intProcCountLines = .ProcCountLines(strProcName, enmReceivedProcKind)
            
            
            Set objRowCol = New Collection
            
            With objRowCol
            
                .Add vobjVBProject.Name
            
                .Add vobjVBComponent.Name
            
                .Add strProcName
                
                .Add GetKindFullNameOfVBAProcedure(enmReceivedProcKind)
                
                
                .Add intProcStartLine
                
                .Add intProcBodyLine
                
                .Add intProcStartLine + intProcCountLines - 1
            End With
            
            robjDataTableCol.Add objRowCol
        End If
    End With
End Sub



