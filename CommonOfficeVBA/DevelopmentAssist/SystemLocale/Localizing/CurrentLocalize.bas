Attribute VB_Name = "CurrentLocalize"
'
'   Design simply to localize text with dependent on ADO
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 16/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
'**---------------------------------------------
'** paths characters
'**---------------------------------------------
'
Private Const mstrKeyToTextTableSubDirectoryName As String = "LocalizeByKeyValues"

Private Const mstrPrefixOfKeyValueDataTable = "KeyValuesAbout"


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

' Cache, I have a concern that this aggregates the all Key-Value texts in this static variable....
' If new some designing problem occur, I investigate to redesign the architecture.

Private mobjModuleNameToKeyToTextDic As Scripting.Dictionary    ' Dictionary(Of String[ModuleName], Dictionary(Of String[Key], String[Text]))


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToCurrentLocalize()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
  
    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "MetaMetaLocalize,CurrentLocaleUtility,ADOParameters,ADOConStrToolsForExcelBook,LocalizationADOConnector"
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** get localized values from outside Key-Value table as a Excel-book
'**---------------------------------------------
'''
''' This is dependent on both the static mobjModuleNameToKeyToTextDic of this module CurrentLocalize and the current UI language code page.
'''
''' If the Key-Value table Excel book isn't found in this Windows file system, return Nothing and need for some substitute processes.
'''
Public Function GetKeyToTextForLocalizingEachModule(ByVal vstrModuleName As String) As Scripting.Dictionary

    Dim objModuleKeyToTextDic As Scripting.Dictionary, strInputPath As String

    If mobjModuleNameToKeyToTextDic Is Nothing Then
    
        Set mobjModuleNameToKeyToTextDic = New Scripting.Dictionary
    End If

    Set GetKeyToTextForLocalizingEachModule = Nothing
    
    With mobjModuleNameToKeyToTextDic
    
        If .Exists(vstrModuleName) Then
        
            Set objModuleKeyToTextDic = .Item(vstrModuleName)
        Else
        
            strInputPath = GetKeyValuesVbaLocalizationBookPathForEachModule(vstrModuleName)
            
            With New Scripting.FileSystemObject
        
                If .FileExists(strInputPath) Then
        
                    Set objModuleKeyToTextDic = GetKeyToTextByLoadingFromDataTable(vstrModuleName, GetCurrentUICaptionLanguageType())
                
                    mobjModuleNameToKeyToTextDic.Add vstrModuleName, objModuleKeyToTextDic
                End If
            End With
        End If
    End With

    Set GetKeyToTextForLocalizingEachModule = objModuleKeyToTextDic
End Function

'**---------------------------------------------
'** get dictionary by using ADO to external tables such as Excel.Workbook.
'**---------------------------------------------
'''
'''
'''
Public Function GetKeyToTextByLoadingFromDataTable(ByVal vstrModuleName As String, _
        Optional ByVal venmCaptionLanguageType As CaptionLanguageType = CaptionLanguageType.UIDefaultEnglishCaptions) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary
    
    StoreLocalizeKeyToTextIntoDictionaryByLoadingFromDataTable objDic, vstrModuleName, venmCaptionLanguageType
    
    Set GetKeyToTextByLoadingFromDataTable = objDic
End Function


'''
'''
'''
''' <Argument>robjDic: Output</Argument>
''' <Argument>vstrModuleName: Input</Argument>
''' <Return>venmCaptionLanguageType: Input</Return>
Public Sub StoreLocalizeKeyToTextIntoDictionaryByLoadingFromDataTable(ByRef robjDic As Scripting.Dictionary, _
        ByVal vstrModuleName As String, _
        Optional ByVal venmCaptionLanguageType As CaptionLanguageType = CaptionLanguageType.UIDefaultEnglishCaptions)

    Dim strInputPath As String, strSQL As String

    If robjDic Is Nothing Then Set robjDic = New Scripting.Dictionary

    strInputPath = GetKeyValuesVbaLocalizationBookPathForEachModule(vstrModuleName)

    With New LocalizationADOConnector
    
        .SetInputExcelBookPath strInputPath, vblnBookReadOnly:=True
    
        strSQL = "SELECT StringKey, " & GetLocalizingColumnNameByCaptionLanguageType(venmCaptionLanguageType) & " From [KeyValuesForLocalizing$] WHERE StringKey IS NOT NULL"
    
        msubStoreTableIntoDictionaryFromRSet robjDic, .GetRecordset(strSQL)
    End With
End Sub


'''
''' Attention! use duplicated codes for keeping this module dependency, this original is in DataTableADOIn.bas
'''
''' <Argument>robjDic: Output Dictionary(Of Key, Value) or Dictionary(Of Key, Collection(Of Value)) - key is 1st column value of Recordset</Argument>
''' <Argument>vobjRSet: Input - a Recordset instance after SQL SELECT execution, which has to include more than one column</Argument>
''' <Argument>vblnOmitDuplicatedKeys: Input - Default is false, </Argument>
Private Sub msubStoreTableIntoDictionaryFromRSet(ByRef robjDic As Scripting.Dictionary, _
        ByVal vobjRSet As ADODB.Recordset, _
        Optional ByVal vblnOmitDuplicatedKeys As Boolean = False)


    Dim objRowCol As Collection, intColumn As Long
    
    Dim varKey As Variant
    
    
    If robjDic Is Nothing Then Set robjDic = New Scripting.Dictionary
    
    With vobjRSet
    
        If .Fields.Count > 2 Then
        
            Do While Not .EOF
                
                Set objRowCol = New Collection
                
                For intColumn = 1 To .Fields.Count - 1
                
                    objRowCol.Add .Fields.Item(intColumn).Value  ' From the second column on, these column values are stored to Collection
                Next
                
                If vblnOmitDuplicatedKeys Then
                
                    With robjDic
                    
                        varKey = vobjRSet.Fields.Item(0)    ' The first column value is key
                        
                        If Not .Exists(varKey) Then
                        
                            .Add varKey, objRowCol
                        End If
                    End With
                Else
                    robjDic.Add .Fields.Item(0).Value, objRowCol ' The first column value is key
                End If
                
                .MoveNext
            Loop
        Else
            Do While Not .EOF
                
                If vblnOmitDuplicatedKeys Then
                
                    With robjDic
                    
                        varKey = vobjRSet.Fields.Item(0)
                        
                        If Not .Exists(varKey) Then
                        
                            .Add varKey, vobjRSet.Fields.Item(1).Value
                        End If
                    End With
                Else
                    robjDic.Add .Fields.Item(0).Value, .Fields.Item(1).Value
                End If
                
                .MoveNext
            Loop
        End If
    End With
End Sub


'**---------------------------------------------
'** Solute paths of the outside Key-Value table
'**---------------------------------------------
'''
''' This should be reinvestigated after the AddIn macro-book separation
'''
Public Function GetKeyValuesVbaLocalizationBookPathForEachModule(ByVal vstrModuleName As String) As String

    Dim strBookFileName As String, strDir As String, strPath As String
    
    strBookFileName = mstrPrefixOfKeyValueDataTable & vstrModuleName & ".xlsx"
    
    strDir = GetCurrentOfficeFileObject().Path & "\" & mstrKeyToTextTableSubDirectoryName
    
    With New Scripting.FileSystemObject
    
        If Not .FolderExists(strDir) Then
        
            .CreateFolder strDir
        End If
    End With
    
    strPath = strDir & "\" & strBookFileName
    
    GetKeyValuesVbaLocalizationBookPathForEachModule = strPath
End Function

'**---------------------------------------------
'** get SQL column name
'**---------------------------------------------
'''
'''
'''
Public Function GetLocalizingColumnNameByCaptionLanguageType(ByVal venmCaptionLanguageType As CaptionLanguageType) As String

    Dim strColumnName As String
    
    strColumnName = ""
    
    Select Case venmCaptionLanguageType
    
        Case CaptionLanguageType.UIDefaultEnglishCaptions
    
            strColumnName = "EnglishText"
    
        Case CaptionLanguageType.UIJapaneseCaptions
        
            strColumnName = "JapaneseText"
    End Select

    GetLocalizingColumnNameByCaptionLanguageType = strColumnName
End Function



