VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LocalizationADOConnector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADODB.Connection class wrapper for VBA application multi-language localization
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       ADOConnector focus on the visualization of both the ADO error-trapping and SQL execution error-trapping
'       LozalizationADOConnector doesn't support the error trapping visualization and localize captions of itself
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  7/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjConnect As ADODB.Connection

Private mblnIsConnected As Boolean


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mblnIsConnected = False
End Sub

Private Sub Class_Terminate()

    Me.CloseConnection
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Get IsConnected() As Boolean

    IsConnected = mblnIsConnected
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' set input Excel book path, which is limited as the file is lator Excel 2007 version and a '.xlsx' extension
'''
Public Sub SetInputExcelBookPath(ByVal vstrBookPath As String, _
        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
        Optional ByVal vblnBookReadOnly As Boolean = False, _
        Optional ByVal vblnIsConnectedToOffice95 As Boolean = False)
    
    mblnIsConnected = mfblnConnectToExcelBook(vstrBookPath, AccessOLEDBProviderType.AceOLEDB_12P0, ExcelBookSpecType.XlBookFileType12P0Macro, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmIMEXMode)
End Sub


'''
''' get recordset object by the Select SQL
'''
Public Function GetRecordset(ByVal vstrSQL As String) As ADODB.Recordset
    
    Dim objRSet As ADODB.Recordset, objCommand As ADODB.Command
    
    Set objRSet = Nothing
    
    If mblnIsConnected Then
    
        Set objRSet = New ADODB.Recordset
        
        With objRSet
        
            .Source = vstrSQL
            
            .ActiveConnection = mobjConnect
            
            On Error Resume Next
            
            .Open
            
            On Error GoTo 0
        End With
    End If

    Set GetRecordset = objRSet
End Function


'''
''' Close ADO connection
'''
Public Sub CloseConnection()
    
    If Not mobjConnect Is Nothing Then
        
        If mblnIsConnected Then
            
            With mobjConnect
                
                If .State <> ADODB.adStateClosed Then
                
                    On Error Resume Next
                    
                    .Close
                    
                    On Error GoTo 0
                End If
            End With
           
            mblnIsConnected = False
        End If
    End If
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
''' connect Excel sheet by ADO
'''
Private Function mfblnConnectToExcelBook(ByVal vstrFilePath As String, _
        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
        Optional ByVal venmExcelBookSpecType As ExcelBookSpecType = ExcelBookSpecType.XlBookFileType12P0Macro, _
        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, Optional ByVal vblnReadOnly As Boolean = False, _
        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode) As Boolean
    
    mfblnConnectToExcelBook = False

    If Not mblnIsConnected Then
    
        Set mobjConnect = New ADODB.Connection
        
        With mobjConnect
        
            .ConnectionString = GetADODBConnectionOleDbStringToXlBook(vstrFilePath, venmAccessOLEDBProviderType, venmExcelBookSpecType, vblnHeaderFieldTitleExists, vblnReadOnly, venmIMEXMode)
            
            .Open
        End With
    
        mblnIsConnected = True
    End If

    mfblnConnectToExcelBook = mblnIsConnected
End Function

