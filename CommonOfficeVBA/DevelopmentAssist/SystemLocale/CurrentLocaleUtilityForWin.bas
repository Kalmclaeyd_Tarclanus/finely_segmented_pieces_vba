Attribute VB_Name = "CurrentLocaleUtilityForWin"
'
'   Solute the current Locale from Windows API
'   This is unusable for MAC OS
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 16/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function GetLocaleInfo Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long) As Long
#Else
    Private Declare Function GetLocaleInfo Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long) As Long
#End If

Private Const LOCALE_SYSTEM_DEFAULT = 2048

Private Const LOCALE_SENGCOUNTRY = &H1002


'///////////////////////////////////////////////
'/// Operation
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetCurrentUICaptionLanguageTypeFromWinAPI() As CaptionLanguageType

    Dim enmCaptionLanguageType As CaptionLanguageType
    Dim strBuf As String * 256, strLangText As String
    
    
    GetLocaleInfo LOCALE_SYSTEM_DEFAULT, LOCALE_SENGCOUNTRY, strBuf, 256
    
    strLangText = Trim(Replace(strBuf, ChrW(0), ""))
    
    'DumpStringToCodeInteger strLangText
    
    Select Case strLangText
    
        Case "Japan"
    
            enmCaptionLanguageType = UIJapaneseCaptions
            
        Case Else
        
            enmCaptionLanguageType = UIDefaultEnglishCaptions
    End Select
    
    GetCurrentUICaptionLanguageTypeFromWinAPI = enmCaptionLanguageType
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
Private Sub msubSanityTestForGetLocaleInfoAPI()
    
    Dim strBuf As String * 256

    GetLocaleInfo LOCALE_SYSTEM_DEFAULT, LOCALE_SENGCOUNTRY, strBuf, 256
    
    Debug.Print Trim(Replace(strBuf, ChrW(0), ""))
End Sub

Private Sub msubSanityTestOfGetCurrentUICaptionLanguageTypeFromWinAPI()

    Debug.Print GetCurrentUICaptionLanguageTypeFromWinAPI()

End Sub

