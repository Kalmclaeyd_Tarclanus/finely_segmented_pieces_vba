Attribute VB_Name = "CurrentLocaleUtility"
'
'   Solute the current Locale from Application.LanguageSettings properties
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 16/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
'''
'''
Public Enum CaptionLanguageType

    UIDefaultEnglishCaptions
    
    UIJapaneseCaptions
End Enum


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private menmCurrentCaptionLanguageType As CaptionLanguageType

Private mblnIsCurrentCaptionLanguageTypeInitialized As Boolean




'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetCurrentUICaptionLanguageType() As CaptionLanguageType

    If Not mblnIsCurrentCaptionLanguageTypeInitialized Then
    
        msubSetupCurrentCaptionLanguageType
        
        mblnIsCurrentCaptionLanguageTypeInitialized = True
    End If

    Select Case True
     
        Case FeatureToForceLocaleToUseJapanese
    
            menmCurrentCaptionLanguageType = UIJapaneseCaptions
            
        Case FeatureToForceLocaleToUseEnglish
        
            menmCurrentCaptionLanguageType = UIDefaultEnglishCaptions
    End Select

    GetCurrentUICaptionLanguageType = menmCurrentCaptionLanguageType
End Function




'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////


Private Sub msubSetupCurrentCaptionLanguageType()

    Dim intLanguageID As Long, intErrorCode As Long
    Dim enmCaptionLanguageType As CaptionLanguageType
    
    intLanguageID = GetCurrentLanguageCodePageInThisUserInterface(intErrorCode)


    If intErrorCode = 0 Then
    
        enmCaptionLanguageType = mfenmGetCurrentCaptionLanguageType(intLanguageID)
    
    Else
        ' Try to use Windows API
        enmCaptionLanguageType = GetCurrentUICaptionLanguageTypeFromWinAPI()
    End If

    menmCurrentCaptionLanguageType = enmCaptionLanguageType
End Sub


'''
'''
'''
Private Function GetCurrentLanguageCodePageInThisUserInterface(ByRef rintErrorCode As Long) As Long

    Dim objApplication As Object    ' Either Excel.Application, Word.Application, or PowerPoint.Application
    
    Set objApplication = GetCurrentOfficeFileObject().Application

    With objApplication
        
        On Error Resume Next
        
        GetCurrentLanguageCodePageInThisUserInterface = .LanguageSettings.LanguageID(msoLanguageIDUI)
        
        If Err.Number <> 0 Then
        
            rintErrorCode = Err.Number
        End If
        
        On Error GoTo 0
    
    End With
End Function

'''
'''
'''
Private Function mfenmGetCurrentCaptionLanguageType(ByVal vintLanguageID As Long) As CaptionLanguageType

    Dim enmCaptionLanguageType As CaptionLanguageType
    
    Select Case vintLanguageID

        Case 1041
        
            enmCaptionLanguageType = UIJapaneseCaptions
            
        Case Else
        
            enmCaptionLanguageType = UIDefaultEnglishCaptions
    End Select

    mfenmGetCurrentCaptionLanguageType = enmCaptionLanguageType
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToCurrentRawCodePage()

    Debug.Print Application.LanguageSettings.LanguageID(msoLanguageIDUI)    ' 1041 means the Japanese environment
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetCurrentLanguageCodePageInThisUserInterface()
    
    Dim intErrorCode As Long

    intErrorCode = 0

    Debug.Print GetCurrentLanguageCodePageInThisUserInterface(intErrorCode)
    
    If intErrorCode <> 0 Then
    
        Debug.Print "Error code : " & intErrorCode
    End If
End Sub
