Attribute VB_Name = "OperateWinMerge"
'
'   Utilities for the installed 'WinMerge'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and 'WinMerge'
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  1/Jun/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleKey As String = "OperateWinMerge"   ' 1st part of registry sub-key

Private Const mstrSubjectWinMergePathKey As String = "WinMergePath"   ' 2nd part of registry sub-key, recommend local hard drive

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Check whether the WinMergeU.exe is installed or not
'**---------------------------------------------
Private mblnIsWinMergeUInstallationChecked As Boolean    ' VBA default value is false

Private mblnIsWinMergeUInstalledInThisComputer As Boolean                ' VBA default value is false


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About WinMerge
'**---------------------------------------------
'''
''' compare two source-files using the WinMerge
'''
Public Sub CompareTwoSourceFilesByWinMerge(ByVal vstrSrc01Path As String, ByVal vstrSrc02Path As String, Optional ByVal vstrOptionalArguments As String = "")
   
    With New IWshRuntimeLibrary.WshShell
        
        .Run """" & mfstrGetWinMergePath() & """ " & vstrSrc01Path & " " & vstrSrc02Path & " " & vstrOptionalArguments
    End With
End Sub


'///////////////////////////////////////////////
'/// Operations - a UNC path portability solution
'///////////////////////////////////////////////
'**---------------------------------------------
'** set user-specified path
'**---------------------------------------------
'''
''' Individual-Set-Registy
'''
Public Sub ISRInOperateWinMergeOfWinMergePath(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectWinMergePathKey, vstrTargetPath
End Sub
Public Sub IDelRInOperateWinMergeOfWinMergePath()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectWinMergePathKey
End Sub


Public Function IsWinMergeUInstalledInThisComputer() As Boolean

    Dim strPath As String

    If Not mblnIsWinMergeUInstallationChecked Then
    
        strPath = mfstrGetWinMergePath()
    End If

    IsWinMergeUInstalledInThisComputer = mblnIsWinMergeUInstalledInThisComputer
End Function


'**---------------------------------------------
'** solute path generaly
'**---------------------------------------------
'''
''' search path from registry CURRENT_USER
'''
Private Function mfstrGetWinMergePath() As String

    Dim objSubPaths As Collection, strTargetPath As String, strModuleNameAndSubjectKey As String
    
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectWinMergePathKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If Not mblnIsWinMergeUInstallationChecked Then

        If strTargetPath = "" Then
            
            Set objSubPaths = New Collection
            
            With objSubPaths
            
                ' typical install paths of WinMerge
                
                .Add ":\Program Files\WinMerge\WinMergeU.exe"
                
                .Add ":\Program Files (x86)\WinMerge\WinMergeU.exe"
            End With
            
            strTargetPath = SearchSpecifiedFileFromSpecifiedSubPathsAndSetRegistryIfFound(strModuleNameAndSubjectKey, objSubPaths)
        End If
    
        If strTargetPath <> "" Then
        
             mblnIsWinMergeUInstalledInThisComputer = True
        End If
    
        mblnIsWinMergeUInstallationChecked = True
    End If
    
    mfstrGetWinMergePath = strTargetPath
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfGetWinMergePath()

    Debug.Print mfstrGetWinMergePath()
End Sub

'''
'''
'''
Private Sub msubSanityTestToDeleteImportantDirectories()

    IDelRInOperateWinMergeOfWinMergePath
End Sub

