Attribute VB_Name = "OperateWinGit"
'
'   Utilities for the installed 'Git for Windows'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and 'Git for Windows'
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  1/Jun/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleKey As String = "OperateWinGit"   ' 1st part of registry sub-key

Private Const mstrSubjectInstalledGitPathKey As String = "GitForWindows"   ' 2nd part of registry sub-key, recommend local hard drive

Private Const mstrSubjectGitRepositoryRootPathKey As String = "GitRepositoryRoot"    ' 2nd part of registry sub-key, recommend local hard drive, for example D:\GitRepo


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Check whether the Git.exe is installed or not
'**---------------------------------------------
Private mblnIsGitForWindowsInstallationChecked As Boolean    ' VBA default value is false

Private mblnIsGitForWindowsInstalledInThisComputer As Boolean                ' VBA default value is false


' cache
Private mobjCacheGitSettingsDic As Scripting.Dictionary

Private Const mstrGetSettingGlobalKey As String = "GitGlobal"

Private Const mstrGetSettingSystemKey As String = "GitSystem"

Private Const mstrGetSettingLocalKey As String = "GitLocal"

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' open Cmd.exe
'''
Public Sub OpenCmdToManageGitRepositoryThisComputerRootPath()

    OpenCmdWithSpecifyingBothWorkingDirectoryAndCmdWindowTitleText GetGitRepositoryRootPath(), _
            "Manage Git for common setting"
End Sub

'''
''' open Cmd.exe for gpg.exe (GnuPG, which should be also installed with the Git for Windows installation)
'''
Public Sub OpenCmdToManageGnuPGForGit()

    OpenCmdWithSpecifyingBothWorkingDirectoryAndCmdWindowTitleText GetGPGInstalledDirectoryPath(), _
            "Manage GnuPG for common setting"
    
    
    ' sample
    
    ' confirm the signature after git commit
    ' git log --show-signature -1
    
    ' gpg --clearsign
    
End Sub

'''
''' Confirm gpg keys after create keys
'''
Public Sub SendGnuPGKeyList01WithOutputtingImmediateWindow()

    SendGitCommandWithOutputtingLogToVBIDEImmediateWindow "gpg --list-keys", _
            GetGPGInstalledDirectoryPath()
End Sub

'''
''' Confirm gpg keys after create keys
'''
Public Sub SendGnuPGKeyList02WithOutputtingImmediateWindow()

    SendGitCommandWithOutputtingLogToVBIDEImmediateWindow "gpg --list-secret-keys --keyid-format LONG", _
            GetGPGInstalledDirectoryPath()
End Sub


'''
''' git config --global user.signingkey <Sample: 3BB5C424371567CA3>
'''
Public Sub SendGitCommandGnuPGSigningWithOutputtingImmediateWindow(ByRef rstrGpgSigningKey As String)

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git config --global user.signingkey " & rstrGpgSigningKey
End Sub


'''
'''
'''
Public Sub SendGnuPGCommandWithOutputtingImmediateWindow(ByRef rstrCommand As String)

    SendGitCommandWithOutputtingLogToVBIDEImmediateWindow rstrCommand, _
            GetGPGInstalledDirectoryPath()
End Sub


'**---------------------------------------------
'** About staging
'**---------------------------------------------
'''
'''
'''
Public Sub SendGitCommitCommand(ByVal vstrRepositoryDirPath As String, _
        Optional ByVal vstrMessage As String = "")

    SendGitCommandWithOutputtingLogToVBIDEImmediateWindow GetGitCommitCommand(vstrMessage), _
            vstrRepositoryDirPath
End Sub

'''
'''
'''
Public Function GetGitCommitCommand(Optional ByVal vstrMessage As String = "", _
        Optional ByVal vblnDoSignatureCommit As Boolean = False) As String

    Dim strCommand As String

    If vstrMessage <> "" Then
    
        If vblnDoSignatureCommit Then
    
            strCommand = "git commit -S -m """ & vstrMessage & """"
        Else
            strCommand = "git commit -m """ & vstrMessage & """"
        End If
    Else
        If vblnDoSignatureCommit Then
        
            strCommand = "git commit -S"
        Else
            strCommand = "git commit"
        End If
    End If

    GetGitCommitCommand = strCommand
End Function

'**---------------------------------------------
'** About get Git registered file information
'**---------------------------------------------

'''
''' Use 'git ls-tree' command
'''
Public Function GetGitRegisteredFiles(ByVal vstrGitRepositoryDirectoryPath As String) As Collection

    Dim strCommand As String, strStdOut As String, strStdErr As String, varDataLines As Variant, strLine As String, i As Long
    Dim objGitRegisteredCol As Collection

    ' use git ls-tree command
    strCommand = "git ls-tree -r --name-only HEAD"
    
    strStdOut = ExecuteWinCmdWithGettingStdErr(strCommand, strStdErr, vstrGitRepositoryDirectoryPath)

    If strStdOut = "" And strStdErr <> "" Then
    
        Debug.Print "git command error: " & strStdErr
    End If

    Set objGitRegisteredCol = New Collection
    
    GetLinesArrayFromOneStringText varDataLines, strStdOut
    
    For i = UBound(varDataLines) To LBound(varDataLines) Step -1
        
        strLine = Trim(varDataLines(i))

        If strLine <> "" Then
        
            objGitRegisteredCol.Add strLine
        End If
    Next

    Set GetGitRegisteredFiles = objGitRegisteredCol
End Function


'''
''' Use 'git log' command
'''
Public Function GetGitRegisteredFilesFromGitLog(ByVal vstrGitRepositoryDirectoryPath As String) As Collection

    Dim strCommand As String, strStdOut As String, strStdErr As String, varDataLines As Variant, strLine As String, i As Long

    Dim objHistoryHeaderRegExp As VBScript_RegExp_55.RegExp, objFileNameKeys As Scripting.Dictionary, objGitRegisteredCol As Collection
    
    Const strGitAdd As String = "A" & vbTab
    
    Const strGitRemove As String = "D" & vbTab

    ' use git log command
    strCommand = "git log --name-status --pretty=oneline"
    
    strStdOut = ExecuteWinCmdWithGettingStdErr(strCommand, strStdErr, vstrGitRepositoryDirectoryPath)

    Set objGitRegisteredCol = New Collection

    If strStdErr = "" Then
    
        Set objHistoryHeaderRegExp = GetRegExpObjectFromGeneralCacheAfterSetting("[a-z0-9]{40}")
    
        GetLinesArrayFromOneStringText varDataLines, strStdOut
    
        ' trace reversely
        For i = UBound(varDataLines) To LBound(varDataLines) Step -1
        
            strLine = varDataLines(i)
        
            If strLine <> "" And Not objHistoryHeaderRegExp.Test(strLine) Then
            
                If objFileNameKeys Is Nothing Then Set objFileNameKeys = New Scripting.Dictionary
            
                If InStr(1, strLine, strGitAdd) > 0 Then
                
                    strLine = Replace(strLine, strGitAdd, "")
                    
                    ' Add
                    
                    With objFileNameKeys
                    
                        If Not .Exists(strLine) Then
                        
                            .Add strLine, 1
                        End If
                    End With
                
                ElseIf InStr(1, strLine, strGitRemove) > 0 Then
                
                    strLine = Replace(strLine, strGitRemove, "")
                
                    ' Delete
                    
                    With objFileNameKeys
                    
                        If .Exists(strLine) Then
                        
                            .Remove strLine
                        End If
                    End With
                End If
            End If
        Next
        
        SortDictionaryAboutKeys objFileNameKeys
        
        Set objGitRegisteredCol = GetEnumeratorKeysColFromDic(objFileNameKeys)
    End If
    
    Set GetGitRegisteredFilesFromGitLog = objGitRegisteredCol
End Function


'**---------------------------------------------
'** About a first repository creation
'**---------------------------------------------
'''
''' If you use Norton 360, you should have to set the permission of the Script-Control in Microsoft Office VBA
'''
Public Function IsLocalGitRepositoryExists(ByVal vstrDirectoryPath As String) As Boolean

    Dim blnExists As Boolean
    
    Dim strCommand As String, strStdOut As String, strStdErr As String


    strCommand = "git status"
    
    strStdOut = ExecuteWinCmdWithGettingStdErr(strCommand, strStdErr, vstrDirectoryPath)

    If strStdErr <> "" Then
    
        If InStr(1, strStdErr, "not a git repository") > 0 Then
        
            blnExists = False
        End If
    End If
    If strStdOut <> "" Then
    
        If InStr(1, strStdOut, "On branch") > 0 Then
        
            blnExists = True
        End If
    End If
    
    IsLocalGitRepositoryExists = blnExists
End Function

'''
'''
'''
Public Function GetGitVersion() As String

    Dim strDirPath As String, strStdOut As String
    
    strDirPath = GetMicrosoftWindowsCurrentUserRootDirectoryPath()

    strStdOut = ExecuteWinCmd("git --version", strDirPath)

    'Debug.Print strStdOut
    
    GetGitVersion = Trim(Replace(strStdOut, "git version", ""))
End Function


'''
'''
'''
Public Function GetGPGInstalledDirectoryPath() As String

    Dim strGitExePath As String, strTmpDir As String

    strGitExePath = mfstrGetGitForWindowsPath()

    GetParentDirectoryPathToArgByVbaDir strTmpDir, strGitExePath
    
    GetParentDicrectoryPathToArgFromCurrentExistedDirectoryPathByVbaDir strTmpDir, strTmpDir

    GetGPGInstalledDirectoryPath = strTmpDir & "\usr\bin"
End Function

'''
'''
'''
Public Function GetGPGInstalledPath()

    GetGPGInstalledPath = GetGPGInstalledDirectoryPath() & "\gpg.exe"
End Function

'''
''' get gpg.exe version
'''
Public Function GetGPGVersionInGit() As String

    Dim strStdOut As String, strGPGPathInGit As String
    Dim varLines As Variant, i As Long, strVersion
    
    strGPGPathInGit = GetGPGInstalledDirectoryPath()

    'Debug.Print strGPGPathInGit
    
    strStdOut = ExecuteWinCmd("gpg --version", strGPGPathInGit)

    'Debug.Print strStdOut

    GetLinesArrayFromOneStringText varLines, strStdOut

    For i = LBound(varLines) To UBound(varLines)
    
        If InStr(1, varLines(i), "gpg (GnuPG)") > 0 Then
        
            strVersion = Trim(Replace(varLines(i), "gpg (GnuPG)", ""))
        
            Exit For
        End If
    Next

    GetGPGVersionInGit = strVersion
End Function

'''
'''
'''
Public Sub CreateLocalGitRepository(ByVal vstrDirectoryPath As String)

    Dim strCommand As String, strStdOut As String, strStdErr As String

    If Not IsLocalGitRepositoryExists(vstrDirectoryPath) Then
    
        strCommand = "git init"
        
        strStdOut = ExecuteWinCmdWithGettingStdErr(strCommand, strStdErr, vstrDirectoryPath)
        
        If strStdErr <> "" Then
        
            Debug.Print "Error: " & strStdErr
            
            Debug.Assert False
        Else
            Debug.Print strStdOut
        End If
    End If
End Sub

'''
'''
'''
Public Sub DeleteLocalGitRepository(ByVal vstrDirectoryPath As String)

    Dim strCommand As String, strStdOut As String, strStdErr As String

    If IsLocalGitRepositoryExists(vstrDirectoryPath) Then
    
        strCommand = "rmdir /S /Q .git"    ' delete a git repository in the case of Cmd.exe of Windows
        
        strStdOut = ExecuteWinCmdWithGettingStdErr(strCommand, strStdErr, vstrDirectoryPath)
        
        If strStdErr <> "" Then
        
            Debug.Print "Error: " & strStdErr
            
            Debug.Assert False
        End If
    End If
End Sub


'**---------------------------------------------
'** About this machine local Git user information
'**---------------------------------------------
'''
'''
'''
Public Sub SendGitCommandWithOutputtingLogToTextFile(ByVal vstrCommand As String, _
        ByVal vstrRepositoryDirPath As String, _
        Optional ByVal vstrCmdLogTextFilePath As String = "")


    Dim strLogText As String, strLogPath As String
    
    
    SendGitCommandAndGetLogText strLogText, vstrCommand, vstrRepositoryDirPath

    If vstrCmdLogTextFilePath <> "" Then
    
        strLogPath = vstrCmdLogTextFilePath
    Else
        strLogPath = GetTmpCompareRootPathOriginal() & "\EachVBprojExportLog\GitCommandExecutedLog.txt"
    End If

    ForceToCreateDirectoryByVbaDir GetParentDirectoryPathByVbaDir(strLogPath)

    DebugPrintToFullPathTextFile strLogText, strLogPath
End Sub


'''
'''
'''
Public Sub SendGitCommandWithOutputtingLogToVBIDEImmediateWindow(ByVal vstrCommand As String, _
        ByVal vstrRepositoryDirPath As String)

    Dim strLogText As String
    
    SendGitCommandAndGetLogText strLogText, vstrCommand, vstrRepositoryDirPath
    
    Debug.Print strLogText
    
    msubActivateWindowOfThisVBE
End Sub

'''
'''
'''
Public Sub SendGitCommandAndGetLogText(ByRef rstrLogText As String, _
        ByVal vstrCommand As String, _
        ByVal vstrRepositoryDirPath As String)


    Dim strStdOut As String, strStdErr As String
    
    strStdOut = ExecuteWinCmdWithGettingStdErr(vstrCommand, strStdErr, vstrRepositoryDirPath)
    
    msubGetCmdExecutionLogTextForGitCommand rstrLogText, strStdOut, strStdErr, vstrRepositoryDirPath
End Sub

'''
'''
'''
''' <Argument>rstrOutputLogText: Output</Argument>
''' <Argument>rstrStdOut: Input</Argument>
''' <Argument>rstrStdErr: Input</Argument>
''' <Argument>rstrGitRepositoryDirectoryPath: Input</Argument>
Public Sub msubGetCmdExecutionLogTextForGitCommand(ByRef rstrOutputLogText As String, _
        ByRef rstrStdOut As String, _
        ByRef rstrStdErr As String, _
        ByRef rstrGitRepositoryDirectoryPath As String)


    rstrOutputLogText = rstrOutputLogText & "Shell cmd.exe working directory path: " & rstrGitRepositoryDirectoryPath & vbNewLine
    
    If rstrStdErr <> "" Then
    
        If rstrOutputLogText <> "" Then
        
            rstrOutputLogText = rstrOutputLogText & vbNewLine
        End If
    
        rstrOutputLogText = rstrOutputLogText & "Cmd Standard Error: " & rstrStdErr
    End If
    
    If rstrStdOut <> "" Then
    
        If rstrOutputLogText <> "" Then
        
            rstrOutputLogText = rstrOutputLogText & vbNewLine
        End If
        
        rstrOutputLogText = rstrOutputLogText & "StdOut: " & rstrStdOut
    End If
End Sub


'''
'''
'''
Public Sub SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow(ByRef rstrCommand As String)

    SendGitCommandWithOutputtingLogToVBIDEImmediateWindow rstrCommand, _
            GetTmpTestingVirtualGitRepositoryRootPathOriginal()
End Sub

'''
'''
'''
Public Sub SendGitCommandAtTestingDirectoryWithOutputtingTextLogFile(ByRef rstrCommand As String)

    SendGitCommandWithOutputtingLogToTextFile rstrCommand, _
            GetTmpTestingVirtualGitRepositoryRootPathOriginal()
End Sub

'''
'''
'''
Public Function GetParameterDicFromGitConfig(ByVal vstrCommand As String, _
        ByVal vstrRepositoryDirPath As String) As Scripting.Dictionary

    Dim strLog As String, varLine As Variant, objDic As Scripting.Dictionary
    
    Dim strSourceText As String, strKey As String, strValue As String
    
    
    Set objDic = New Scripting.Dictionary
    
    strLog = ExecuteWinCmd(vstrCommand, vstrRepositoryDirPath)

    msubActivateWindowOfThisVBE

    For Each varLine In Split(strLog, vbLf) ' Not vbCrLf
    
        strSourceText = varLine
    
        If IsDelimiterCharFoundAndThenGetLeftPartAndRightPartByDelimiting(strKey, strValue, strSourceText, "=") Then
        
            objDic.Add strKey, strValue
        End If
    Next

    Set GetParameterDicFromGitConfig = objDic
End Function

'**---------------------------------------------
'** Managing git settings cache
'**---------------------------------------------
'''
'''
'''
Public Function IsSignatureCommitEnabled(Optional ByVal vstrGitLocalRepositoryDirectoryPath As String = "") As Boolean


    Dim objGlobalParamDic As Scripting.Dictionary, objLocalParamDic As Scripting.Dictionary

    Dim strGPGSignCommitEnablingKey As String, blnLocalGPGSignSettingExists As Boolean
    
    Dim strGPGProgramPathKey As String, blnLocalGPGProgramPathExists As Boolean
    
    Dim blnIsGPGSignEnabled As Boolean, blnGPGProgramExists As Boolean


    blnLocalGPGSignSettingExists = False: blnLocalGPGProgramPathExists = False

    strGPGSignCommitEnablingKey = "commit.gpgsign"
    
    strGPGProgramPathKey = "gpg.program"

    blnIsGPGSignEnabled = False

    If vstrGitLocalRepositoryDirectoryPath <> "" Then

        Set objLocalParamDic = GetCurrentUserLocalGitSettingDic(vstrGitLocalRepositoryDirectoryPath)
        
        With objLocalParamDic
        
            If .Exists(strGPGProgramPathKey) Then
            
                blnLocalGPGProgramPathExists = True
                
                blnGPGProgramExists = .Item(strGPGProgramPathKey)
            End If
        
            If .Exists(strGPGSignCommitEnablingKey) Then
            
                blnLocalGPGSignSettingExists = True
                
                blnIsGPGSignEnabled = .Item(strGPGSignCommitEnablingKey)
            End If
        End With
    End If

    If Not (blnLocalGPGSignSettingExists And blnLocalGPGProgramPathExists And blnGPGProgramExists) Then
    
        Set objGlobalParamDic = GetCurrentUserGlobalGitSettingDic()
    
        With objGlobalParamDic
        
            If Not blnLocalGPGProgramPathExists Then
            
                If .Exists(strGPGProgramPathKey) Then
                
                    blnGPGProgramExists = (.Item(strGPGProgramPathKey) <> "")
                Else
                    blnGPGProgramExists = False
                End If
            End If
            
            If Not blnLocalGPGSignSettingExists Then
            
                If .Exists(strGPGSignCommitEnablingKey) Then
                
                    blnIsGPGSignEnabled = (.Item(strGPGSignCommitEnablingKey) <> "")
                Else
                    blnIsGPGSignEnabled = False
                End If
            End If
        End With
    End If
    
    If Not blnGPGProgramExists Then
    
        blnIsGPGSignEnabled = False
    End If
    
    IsSignatureCommitEnabled = blnIsGPGSignEnabled
End Function


'''
'''
'''
Public Sub ClearCacheOfGitSettingsOfCurrentUser()

    If Not mobjCacheGitSettingsDic Is Nothing Then
    
        mobjCacheGitSettingsDic.RemoveAll
    
        Set mobjCacheGitSettingsDic = Nothing
    End If
End Sub

'''
'''
'''
Public Function GetCurrentUserGlobalGitSettingDic() As Scripting.Dictionary

    Dim objSettingDic As Scripting.Dictionary

    If mobjCacheGitSettingsDic Is Nothing Then Set mobjCacheGitSettingsDic = New Scripting.Dictionary
    
    With mobjCacheGitSettingsDic
    
        If Not .Exists(mstrGetSettingGlobalKey) Then
        
            msubReadGitSettingsOfGlobal
        End If
        
        Set objSettingDic = .Item(mstrGetSettingGlobalKey)
    End With

    Set GetCurrentUserGlobalGitSettingDic = objSettingDic
End Function

'''
'''
'''
Public Function GetCurrentUserSystemGitSettingDic() As Scripting.Dictionary

    Dim objSettingDic As Scripting.Dictionary

    If mobjCacheGitSettingsDic Is Nothing Then Set mobjCacheGitSettingsDic = New Scripting.Dictionary

    With mobjCacheGitSettingsDic
    
        If Not .Exists(mstrGetSettingSystemKey) Then
        
            msubReadGitSettingsOfSystem
        End If
        
        Set objSettingDic = .Item(mstrGetSettingSystemKey)
    End With

    Set GetCurrentUserSystemGitSettingDic = objSettingDic
End Function

'''
'''
'''
Public Function GetCurrentUserLocalGitSettingDic(ByVal vstrLocalGitRepositoryDirectoryPath As String) As Scripting.Dictionary

    Dim strKey As String, objSettingDic As Scripting.Dictionary

    If mobjCacheGitSettingsDic Is Nothing Then Set mobjCacheGitSettingsDic = New Scripting.Dictionary

    strKey = mstrGetSettingLocalKey & "_" & vstrLocalGitRepositoryDirectoryPath

    With mobjCacheGitSettingsDic
    
        If Not .Exists(strKey) Then
        
            msubReadGitSettingsOfLocal strKey, vstrLocalGitRepositoryDirectoryPath
        End If
        
        Set objSettingDic = .Item(strKey)
    End With

    Set GetCurrentUserLocalGitSettingDic = objSettingDic
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubReadGitSettingsOfGlobal()

    Dim objParameterDic As Scripting.Dictionary

    Set objParameterDic = GetParameterDicFromGitConfig("git config --global --list", mfstrGetGitRepositoryRootDirectoryPathOrSomthingExisted())

    mobjCacheGitSettingsDic.Add mstrGetSettingGlobalKey, objParameterDic
End Sub

'''
'''
'''
Private Sub msubReadGitSettingsOfSystem()

    Dim objParameterDic As Scripting.Dictionary

    Set objParameterDic = GetParameterDicFromGitConfig("git config --system --list", mfstrGetGitRepositoryRootDirectoryPathOrSomthingExisted())

    mobjCacheGitSettingsDic.Add mstrGetSettingGlobalKey, objParameterDic
End Sub

'''
'''
'''
Private Sub msubReadGitSettingsOfLocal(ByRef rstrKey As String, ByRef rstrLocalGitRepositoryDirectoryPath As String)

    Dim objParameterDic As Scripting.Dictionary

    Set objParameterDic = GetParameterDicFromGitConfig("git config --local --list", rstrLocalGitRepositoryDirectoryPath)

    mobjCacheGitSettingsDic.Add rstrKey, objParameterDic
End Sub

'''
'''
'''
Private Function mfstrGetGitRepositoryRootDirectoryPathOrSomthingExisted() As String

    Dim strDir As String
    
    strDir = GetGitRepositoryRootPath()

    If Not DirectoryExistsByVbaDir(strDir) Then
    
        strDir = GetMicrosoftWindowsSpecialDirectoryPath("MyDocuments")
    End If

    mfstrGetGitRepositoryRootDirectoryPathOrSomthingExisted = strDir
End Function

'''
'''
'''
Private Sub msubActivateWindowOfThisVBE()

    Interaction.AppActivate "Microsoft Visual Basic"
End Sub

'**---------------------------------------------
'** Solve paths for testing this meta-struct git repository path
'**---------------------------------------------
'''
''' solute testing only-files repository path
'''
''' testing synchronizing files from GetTmpTestingFileRepositoryRootPathOriginal() to GetTmpTestingVirtualGitRepositoryRootPathOriginal()
'''
''' and in GetTmpTestingVirtualGitRepositoryRootPathOriginal(), operate git Commit command for testing
'''
Public Function GetTmpTestingVirtualGitRepositoryRootPathOriginal() As String

    Dim strDir As String: strDir = GetTemporaryCodesBaseDir() & "\TmpTestingVirtualGitRepo"
    
    ForceToCreateDirectory strDir

    GetTmpTestingVirtualGitRepositoryRootPathOriginal = strDir
End Function

'''
''' After confirming the testing-directory, next use the following path
'''
''' for testing open-source publication VBA codes, .bas, .cls, .frm, .frx
'''
Public Function GetTmpVBAThisMetaStructureMasterGitRepositoryRootPath(Optional ByVal vstrSnakeCaseProjectName As String = "") As String

    Dim strRightPathPart As String
    
    If vstrSnakeCaseProjectName <> "" Then
    
        strRightPathPart = "\MStruct\" & vstrSnakeCaseProjectName
    Else
        strRightPathPart = "\MStruct"
    End If

    GetTmpVBAThisMetaStructureMasterGitRepositoryRootPath = GetTmpTestingVirtualGitRepositoryRootPathOriginal() & strRightPathPart
End Function

'''
''' For git uploading (git push) test
'''
Private Sub msubExploreTmpVBAThisMetaStructureMasterGitParentRepositoryRootPath()

    Dim strDir As String: strDir = GetTmpVBAThisMetaStructureMasterGitRepositoryRootPath()

    ForceToCreateDirectoryByVbaDir strDir

    If DirectoryExistsByVbaDir(strDir) Then ExploreFolderWhenOpenedThatIsNothing strDir Else Debug.Print "Directory is not found: " & strDir
    
    OpenCmdWithSpecifyingBothWorkingDirectoryAndCmdWindowTitleText strDir, "Manage testing Git meta-structure Git"
End Sub


'///////////////////////////////////////////////
'/// Operations - a UNC path portability solution
'///////////////////////////////////////////////
'**---------------------------------------------
'** set user-specified path
'**---------------------------------------------
'''
''' Individual-Set-Registy
'''
Public Sub ISRInOperateGitForWindowsOfInstalledGitForWindowsPath(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectInstalledGitPathKey, vstrTargetPath
End Sub
Public Sub IDelRInOperateGitForWindowsOfInstalledGitForWindowsPath()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectInstalledGitPathKey
End Sub

Public Sub ISRInOperateGitForWindowsOfGitRepositoryRootPath(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectGitRepositoryRootPathKey, vstrTargetPath
End Sub
Public Sub IDelRInOperateGitForWindowsOfGitRepositoryRootPath()

    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectGitRepositoryRootPathKey
End Sub


Public Function IsGitForWindowsInstalledInThisComputer() As Boolean

    Dim strPath As String

    If Not mblnIsGitForWindowsInstallationChecked Then
    
        strPath = mfstrGetGitForWindowsPath()
    End If

    IsGitForWindowsInstalledInThisComputer = mblnIsGitForWindowsInstalledInThisComputer
End Function


'**---------------------------------------------
'** solute path generaly
'**---------------------------------------------
'''
''' search path from registry CURRENT_USER
'''
Private Function mfstrGetGitForWindowsPath() As String

    Dim objSubPaths As Collection, strTargetPath As String, strModuleNameAndSubjectKey As String
    
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectInstalledGitPathKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If Not mblnIsGitForWindowsInstallationChecked Then

        If strTargetPath = "" Then
            
            Set objSubPaths = New Collection
            
            With objSubPaths
            
                ' typical install paths of GitForWindows
                
                .Add ":\Program Files\Git\bin\git.exe"
                
                .Add ":\Program Files (x86)\Git\bin\git.exe"
            End With
            
            strTargetPath = SearchSpecifiedFileFromSpecifiedSubPathsAndSetRegistryIfFound(strModuleNameAndSubjectKey, objSubPaths)
        End If
    
        If strTargetPath <> "" Then
        
             mblnIsGitForWindowsInstalledInThisComputer = True
        End If
    
        mblnIsGitForWindowsInstallationChecked = True
    End If
    
    mfstrGetGitForWindowsPath = strTargetPath
End Function

'''
'''
'''
Public Function GetVBAEachVBProjectGitRepositoryRootPath() As String

    GetVBAEachVBProjectGitRepositoryRootPath = GetGitRepositoryRootPath() & "\DevVBA\EachVBproj"
End Function

'''
''' After confirming the testing-directory, next use the following path
'''
''' for open-source publication VBA codes, .bas, .cls, .frm, .frx
'''
Public Function GetVBAThisMetaStructureMasterGitRepositoryRootPath(Optional ByVal vstrSnakeCaseProjectName As String = "") As String

    Dim strRightPathPart As String
    
    If vstrSnakeCaseProjectName <> "" Then
        
        strRightPathPart = "\DevVBA\MStruct\" & vstrSnakeCaseProjectName
    Else
        strRightPathPart = "\DevVBA\MStruct"
    End If

    GetVBAThisMetaStructureMasterGitRepositoryRootPath = GetGitRepositoryRootPath() & strRightPathPart
End Function


'''
''' for VBA codes, .bas, .cls, .frm, .frx
'''
Public Function GetVBAGitRepositoryRootPath() As String

    GetVBAGitRepositoryRootPath = GetGitRepositoryRootPath() & "\DevVBA"
End Function

'''
''' for VBScript codes, .wsf, .vbs
'''
Public Function GetVBScriptGitRepositoryRootPath() As String

    GetVBScriptGitRepositoryRootPath = GetGitRepositoryRootPath() & "\DevVBScript"
End Function

'''
''' common by all programming languages
'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetGitRepositoryRootPath() As String

    Dim strModuleNameAndSubjectKey As String, strTargetPath As String
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectGitRepositoryRootPathKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)

    If strTargetPath = "" Then
    
        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForGitRepositoryRootPath())
    End If
    
    GetGitRepositoryRootPath = strTargetPath
End Function

Private Function mfobjGetRegParamOfStorageUNCPathColForGitRepositoryRootPath() As Collection
    
    Dim objCol As Collection, strSubDirectoryPath As String
    
    
    Set objCol = Nothing
    
    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForGitRepositoryRootPath")
    
    If objCol Is Nothing Then   ' If interface cannot be soluted
        
        With New WshNetwork
            
            strSubDirectoryPath = "VBAWorks\" & .UserName & "\GitRepo"
        End With
    
        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCDirectoryPath(StoragePathSearchingFlag.NetWorkOrLocalAsDirectory, strSubDirectoryPath, strSubDirectoryPath))
    End If
    
    Set mfobjGetRegParamOfStorageUNCPathColForGitRepositoryRootPath = objCol
End Function


'**---------------------------------------------
'** About Git relative path
'**---------------------------------------------
'''
''' About 'Git for Windows' installed path
'''
Private Sub msubSanityTestToGetGitForWindowsPath()

    Debug.Print mfstrGetGitForWindowsPath()
End Sub


'''
''' About Git repository root path of this computer
'''
Private Sub msubSanityTestToGetGitRepositoryRootPaths()

    Debug.Print GetGitRepositoryRootPath()
    
    Debug.Print GetVBAGitRepositoryRootPath()
    
    Debug.Print GetVBAThisMetaStructureMasterGitRepositoryRootPath("git_public_main_project")
    
    Debug.Print GetVBAEachVBProjectGitRepositoryRootPath()
    
    Debug.Print GetTmpTestingVirtualGitRepositoryRootPathOriginal()
End Sub


