Attribute VB_Name = "OperateWinGitForVbaAutoCommit"
'
'   Utilities for operating git commit VBA project automatically by using the installed 'Git for Windows'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and 'Git for Windows'
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 29/Jul/2023    Kalmclaeyd Tarclanus    Separated from OperateWinGit.bas
'

Option Explicit


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
Public Enum TwoFilesDifferenceCompareMethod

    CompareTwoFilesDifferencesAboutText ' simply text differences
    
    CompareTwoFilesDifferencesAboutTextWithoutSpecifiedIgnoredConditions    ' a little compricated

    CompareTwoNoPasswordLockedExcelBookFilesDifferencesAboutOnlySheetTablesUsingAdo ' dependent on ADO connection
End Enum

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
'**---------------------------------------------
'** Temporary cmd.exe batch file name
'**---------------------------------------------
Private Const mstrSynchronizeFilesAndGitRepoBatchFileName As String = "SynchronizeFilesAndGit.bat"

Private Const mstrRemoveAllFilesFromGitRepoBatchFileName As String = "RemoveAllFilesFromGit.bat"


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Fixed set files git-push tools
'**---------------------------------------------
'''
''' register special files to a git-repository for example LICENSE, .gitattributes
'''
''' <Argument>robjSourceFileFullPathToDestinationDirectoryPathDic: Input</Argument>
''' <Argument>vstrGitRepositoryRootPath: Input</Argument>
''' <Argument>vblnOnlyCreateBatchFile: Input</Argument>
''' <Argument>vstrHeaderCommitMessageTitle: Input</Argument>
Public Sub AddGitRepositoryForSpecifiedFiles(ByRef robjSourceFileFullPathToDestinationDirectoryPathDic As Scripting.Dictionary, _
        ByVal vstrGitRepositoryRootPath As String, _
        Optional ByVal vblnOnlyCreateBatchFile As Boolean = False, _
        Optional ByVal vstrHeaderCommitMessageTitle As String = "")


    Dim strSrcPath As String, strDstPath As String, varSrcPath As Variant, strDir As String
    
    Dim objNeedToUpdateSrcToDstDic As Scripting.Dictionary, objNeedToAddNewSrcKeysDic As Scripting.Dictionary, objDummyNeedToRemoveDstKeysDic As Scripting.Dictionary
    
    
    Set objNeedToUpdateSrcToDstDic = New Scripting.Dictionary: Set objNeedToAddNewSrcKeysDic = New Scripting.Dictionary
    
    Set objDummyNeedToRemoveDstKeysDic = New Scripting.Dictionary
    
    With robjSourceFileFullPathToDestinationDirectoryPathDic
    
        For Each varSrcPath In .Keys
        
            strSrcPath = varSrcPath
        
            strDstPath = GetDirectoryPathWithAddingLastBackSlashIfThePathHasNoIt(.Item(varSrcPath)) & GetFileNameFromPathByVbaDir(strSrcPath)
        
            If Not FileExistsByVbaDir(strDstPath) Then
            
                objNeedToUpdateSrcToDstDic.Add strSrcPath, strDstPath
            Else
                Select Case LCase(GetExtensionNameByVbaDir(strSrcPath))
                
                    Case "gitattributes", "txt", ""
                    
                        ' text compare
                        If IsDifferentBetweenTextFilesByFileSystemObject(strSrcPath, strDstPath) Then
                        
                            objNeedToUpdateSrcToDstDic.Add strSrcPath, strDstPath
                        End If
                        
                    Case "pptx", "ppt", "docx", "doc", "xlsx", "xls"
                    
                        ' last modified date compare
                    
                        If IsDifferentBetweenFilesAboutDateLastModified(strSrcPath, strDstPath) Then
                        
                            objNeedToUpdateSrcToDstDic.Add strSrcPath, strDstPath
                        End If
                    
                    Case Else
            
                        ' text compare
                        If IsDifferentBetweenTextFilesByFileSystemObject(strSrcPath, strDstPath) Then
                        
                            objNeedToUpdateSrcToDstDic.Add strSrcPath, strDstPath
                        End If
                End Select
            End If
        Next
    
    End With

    If mfblnIsAtLeastOneCommitFileExists(objNeedToUpdateSrcToDstDic, _
            objNeedToAddNewSrcKeysDic, _
            objDummyNeedToRemoveDstKeysDic) Then
    
    
        msubGitCommitBasedOnDifferencesInfoDics objNeedToUpdateSrcToDstDic, _
                objNeedToAddNewSrcKeysDic, _
                objDummyNeedToRemoveDstKeysDic, _
                vstrGitRepositoryRootPath, _
                vblnOnlyCreateBatchFile, _
                vstrHeaderCommitMessageTitle
    End If
End Sub




'**---------------------------------------------
'** About comparing two repository root path
'**---------------------------------------------
'''
'''
'''
''' <Argument>robjRelativePathKeys: Input</Argument>
''' <Argument>vstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vstrExcludedFileExtensionsDelimitedByComma: Input</Argument>
''' <Argument>vstrExtensionPairsDelimitedByComma: Input</Argument>
''' <Argument>vstrHeaderCommitMessageTitle: Input</Argument>
Public Sub RemoveSpecifiedAllVBProjectCodesFromOnlyFileRepoToGitRepoByPath(ByRef robjRelativePathKeys As Scripting.Dictionary, _
        ByVal vstrGitRepositoryRootDirectoryPath As String, _
        Optional ByVal vblnOnlyCreateBatchFile As Boolean = False, _
        Optional ByVal vstrExcludedFileExtensionsDelimitedByComma As String = "frx", _
        Optional ByVal vstrExtensionPairsDelimitedByComma As String = "frm;frx", _
        Optional ByVal vstrHeaderCommitMessageTitle As String = "")


    Dim objNeedToRemoveDstKeysDic As Scripting.Dictionary
    
    msubGetGitAllRemovingFileListDictionaries objNeedToRemoveDstKeysDic, _
            robjRelativePathKeys, _
            vstrGitRepositoryRootDirectoryPath, _
            vstrExcludedFileExtensionsDelimitedByComma

    If objNeedToRemoveDstKeysDic.Count > 0 Then

        msubGitCommitAboutOnlyRemoveBasedOnDifferencesInfoDics objNeedToRemoveDstKeysDic, _
                vstrGitRepositoryRootDirectoryPath, _
                vblnOnlyCreateBatchFile, _
                vstrExtensionPairsDelimitedByComma, _
                vstrHeaderCommitMessageTitle
    End If
End Sub


'''
'''
'''
''' <Argument>robjRelativePathKeys: Input</Argument>
''' <Argument>vstrOnlyFileRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vstrManagingFileWildCardsDelimitedByComma: Input - for example, vstrManagingFileWildCardsDelimitedByComma is "*.bas,*.cls,*.frm,*.frx" or "*.txt", or "*.vbs,*.bat"</Argument>
''' <Argument>vblnOnlyCreateBatchFile: Input</Argument>
''' <Argument>vstrExcludedFileExtensionsDelimitedByComma: Input</Argument>
''' <Argument>vstrExtensionPairsDelimitedByComma: Input</Argument>
''' <Argument>venmTwoFilesDifferenceCompareMethod: Input</Argument>
''' <Argument>vobjIgnoreRegExpPatternsDic: Input</Argument>
''' <Argument>vstrSheetNamesDelimitedByComma: Input</Argument>
''' <Argument>vblnAllowToSearchToRemovingFilesFromGitRepositoryDirectory: Input</Argument>
''' <Argument>vstrHeaderCommitMessageTitle: Input</Argument>
Public Sub SynchronizeVBProjectCodesFromOnlyFileRepoToGitRepoByPath(ByRef robjRelativePathKeys As Scripting.Dictionary, _
        ByVal vstrOnlyFileRepositoryRootDirectoryPath As String, _
        ByVal vstrGitRepositoryRootDirectoryPath As String, _
        ByVal vstrManagingFileWildCardsDelimitedByComma As String, _
        Optional ByVal vblnOnlyCreateBatchFile As Boolean = False, _
        Optional ByVal vstrExcludedFileExtensionsDelimitedByComma As String = "frx", _
        Optional ByVal vstrExtensionPairsDelimitedByComma As String = "frm;frx", _
        Optional ByVal venmTwoFilesDifferenceCompareMethod As TwoFilesDifferenceCompareMethod = TwoFilesDifferenceCompareMethod.CompareTwoFilesDifferencesAboutText, _
        Optional ByVal vobjIgnoreRegExpPatternsDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrSheetNamesDelimitedByComma As String = "", _
        Optional ByVal vblnAllowToSearchToRemovingFilesFromGitRepositoryDirectory As Boolean = True, _
        Optional ByVal vstrHeaderCommitMessageTitle As String = "")

    
    Dim objNeedToUpdateSrcToDstDic As Scripting.Dictionary, objNeedToAddNewSrcKeysDic As Scripting.Dictionary, objNeedToRemoveDstKeysDic As Scripting.Dictionary
    
    
    msubGetGitOperationNeededFileListDictionaries objNeedToUpdateSrcToDstDic, _
            objNeedToAddNewSrcKeysDic, _
            objNeedToRemoveDstKeysDic, _
            robjRelativePathKeys, _
            vstrOnlyFileRepositoryRootDirectoryPath, _
            vstrGitRepositoryRootDirectoryPath, _
            vstrManagingFileWildCardsDelimitedByComma, _
            vstrExcludedFileExtensionsDelimitedByComma, _
            venmTwoFilesDifferenceCompareMethod, _
            vobjIgnoreRegExpPatternsDic, _
            vstrSheetNamesDelimitedByComma, _
            vblnAllowToSearchToRemovingFilesFromGitRepositoryDirectory
    
    
    If mfblnIsAtLeastOneCommitFileExists(objNeedToUpdateSrcToDstDic, objNeedToAddNewSrcKeysDic, objNeedToRemoveDstKeysDic) Then
    
        msubGitCommitBasedOnDifferencesInfoDics objNeedToUpdateSrcToDstDic, _
                objNeedToAddNewSrcKeysDic, _
                objNeedToRemoveDstKeysDic, _
                vstrGitRepositoryRootDirectoryPath, _
                vblnOnlyCreateBatchFile, _
                vstrExtensionPairsDelimitedByComma, _
                vstrHeaderCommitMessageTitle
    End If
End Sub

'''
'''
'''
''' <Argument>robjNeedToUpdateSrcToDstDic: Input</Argument>
''' <Argument>robjNeedToAddNewSrcKeysDic: Input</Argument>
''' <Argument>robjNeedToRemoveDstKeysDic: Input</Argument>
''' <Return>Boolean</Return>
Private Function mfblnIsAtLeastOneCommitFileExists(ByRef robjNeedToUpdateSrcToDstDic As Scripting.Dictionary, _
        ByRef robjNeedToAddNewSrcKeysDic As Scripting.Dictionary, _
        ByRef robjNeedToRemoveDstKeysDic As Scripting.Dictionary) As Boolean

    Dim blnIsAtLeastOneCommitFileExists As Boolean

    blnIsAtLeastOneCommitFileExists = False
    
    Select Case True
    
        Case IsAtLeastOneDictionaryKeyValueExists(robjNeedToUpdateSrcToDstDic), _
                IsAtLeastOneDictionaryKeyValueExists(robjNeedToAddNewSrcKeysDic), _
                IsAtLeastOneDictionaryKeyValueExists(robjNeedToRemoveDstKeysDic)
    
            blnIsAtLeastOneCommitFileExists = True
    End Select
    
    mfblnIsAtLeastOneCommitFileExists = blnIsAtLeastOneCommitFileExists
End Function

'''
''' remove only
'''
''' <Argument>robjNeedToRemoveDstKeysDic: Input</Argument>
''' <Argument>vstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vblnOnlyCreateBatchFile: Input</Argument>
''' <Argument>vstrExtensionPairsDelimitedByComma: Input</Argument>
''' <Argument>vstrHeaderCommitMessageTitle: Input</Argument>
Private Sub msubGitCommitAboutOnlyRemoveBasedOnDifferencesInfoDics(ByRef robjNeedToRemoveDstKeysDic As Scripting.Dictionary, _
        ByVal vstrGitRepositoryRootDirectoryPath As String, _
        Optional ByVal vblnOnlyCreateBatchFile As Boolean = False, _
        Optional ByVal vstrExtensionPairsDelimitedByComma As String = "frm;frx", _
        Optional ByVal vstrHeaderCommitMessageTitle As String = "")


    Dim strCommitCommentOfRemove As String, strCommitMessage As String, strCommandLines As String

    If IsLocalGitRepositoryExists(vstrGitRepositoryRootDirectoryPath) Then

        msubAddCommandLinesAboutRemovingGitRepositoryFiles strCommandLines, _
                strCommitCommentOfRemove, _
                robjNeedToRemoveDstKeysDic, _
                vstrGitRepositoryRootDirectoryPath, _
                vstrExtensionPairsDelimitedByComma

        strCommitMessage = mfstrGetCommitMainMessage("", _
                "", _
                strCommitCommentOfRemove, _
                vstrHeaderCommitMessageTitle)

        ' add Git commit command
        msubAddCommandLinesAboutGitCommit strCommandLines, _
                strCommitMessage, _
                vstrGitRepositoryRootDirectoryPath
    
        
        msubGitCommitByCmdBatcheFileOrOnlyCreateBatcheFile strCommandLines, _
                mstrRemoveAllFilesFromGitRepoBatchFileName, _
                vstrGitRepositoryRootDirectoryPath, _
                vblnOnlyCreateBatchFile
    End If
End Sub

'''
'''
'''
''' <Argument>robjNeedToUpdateSrcToDstDic: Input</Argument>
''' <Argument>robjNeedToAddNewSrcKeysDic: Input</Argument>
''' <Argument>robjNeedToRemoveDstKeysDic: Input</Argument>
''' <Argument>vstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vblnOnlyCreateBatchFile: Input</Argument>
''' <Argument>vstrExtensionPairsDelimitedByComma: Input</Argument>
''' <Argument>vstrHeaderCommitMessageTitle: Input</Argument>
Private Sub msubGitCommitBasedOnDifferencesInfoDics(ByRef robjNeedToUpdateSrcToDstDic As Scripting.Dictionary, _
        ByRef robjNeedToAddNewSrcKeysDic As Scripting.Dictionary, _
        ByRef robjNeedToRemoveDstKeysDic As Scripting.Dictionary, _
        ByVal vstrGitRepositoryRootDirectoryPath As String, _
        Optional ByVal vblnOnlyCreateBatchFile As Boolean = False, _
        Optional ByVal vstrExtensionPairsDelimitedByComma As String = "frm;frx", _
        Optional ByVal vstrHeaderCommitMessageTitle As String = "")


    Dim blnIsThisTimeFirstCommit As Boolean
    
    Dim strCommitCommentOfNewAdd As String, strCommitCommentOfUpdate As String, strCommitCommentOfRemove As String
    
    Dim strCommitMessage As String, strCommandLines As String, strPromptMessage As String, intCountOfAdd As Long, intCountOfRemove As Long
    

    blnIsThisTimeFirstCommit = IsThisGitRepositoryDirectoryFirstCommitAndIfYesThenInitilizeItAsGitRepository(vstrGitRepositoryRootDirectoryPath)
        
    strCommandLines = ""

 
    msubAddCommandLinesAboutEcho strCommandLines, _
            mfstrGetGitAutoCommitPromptMessage(robjNeedToUpdateSrcToDstDic, _
                robjNeedToAddNewSrcKeysDic, _
                robjNeedToRemoveDstKeysDic)

    msubAddCommandLinesAboutEchoEnabling strCommandLines, False
    
    
    msubAddCommandLinesAboutMoveCurrentDirectory strCommandLines, _
            vstrGitRepositoryRootDirectoryPath


    msubAddCommandLinesAboutAddingNew strCommandLines, _
            strCommitCommentOfNewAdd, _
            robjNeedToAddNewSrcKeysDic, _
            vstrGitRepositoryRootDirectoryPath, _
            vstrExtensionPairsDelimitedByComma


    msubAddCommandLinesAboutUpdatingGitRepositoryFiles strCommandLines, _
            strCommitCommentOfUpdate, _
            robjNeedToUpdateSrcToDstDic, _
            vstrGitRepositoryRootDirectoryPath, _
            vstrExtensionPairsDelimitedByComma

    msubAddCommandLinesAboutRemovingGitRepositoryFiles strCommandLines, _
            strCommitCommentOfRemove, _
            robjNeedToRemoveDstKeysDic, _
            vstrGitRepositoryRootDirectoryPath, _
            vstrExtensionPairsDelimitedByComma


    strCommandLines = strCommandLines & vbNewLine & vbNewLine

    msubAddCommandLinesAboutEchoEnabling strCommandLines, True

    strCommitMessage = mfstrGetCommitIntegratedMessage(strCommitCommentOfNewAdd, _
            strCommitCommentOfUpdate, _
            strCommitCommentOfRemove, _
            blnIsThisTimeFirstCommit, _
            vstrHeaderCommitMessageTitle)

    msubAddCommandLinesAboutGitCommit strCommandLines, _
            strCommitMessage, _
            vstrGitRepositoryRootDirectoryPath


    msubGitCommitByCmdBatcheFileOrOnlyCreateBatcheFile strCommandLines, _
            mstrSynchronizeFilesAndGitRepoBatchFileName, _
            vstrGitRepositoryRootDirectoryPath, _
            vblnOnlyCreateBatchFile
End Sub

'''
'''
'''
''' <Argument>robjNeedToUpdateSrcToDstDic: Input</Argument>
''' <Argument>robjNeedToAddNewSrcKeysDic: Input</Argument>
''' <Argument>robjNeedToRemoveDstKeysDic: Input</Argument>
''' <Return>String</Return>
Private Function mfstrGetGitAutoCommitPromptMessage(ByRef robjNeedToUpdateSrcToDstDic As Scripting.Dictionary, _
        ByRef robjNeedToAddNewSrcKeysDic As Scripting.Dictionary, _
        ByRef robjNeedToRemoveDstKeysDic As Scripting.Dictionary) As String

    Dim strPromptMessage As String, intCountOfAdd As Long, intCountOfRemove As Long

    strPromptMessage = ""
    
    intCountOfAdd = robjNeedToUpdateSrcToDstDic.Count + robjNeedToAddNewSrcKeysDic.Count
    
    If intCountOfAdd > 0 Then
    
        strPromptMessage = strPromptMessage & "Count of git-add: " & CStr(intCountOfAdd)
    End If
    
    intCountOfRemove = robjNeedToRemoveDstKeysDic.Count
    
    If intCountOfRemove > 0 Then
    
        If strPromptMessage <> "" Then
        
            strPromptMessage = strPromptMessage & ", "
        End If
    
        strPromptMessage = strPromptMessage & "Count of git-remove: " & CStr(intCountOfRemove)
    End If

    mfstrGetGitAutoCommitPromptMessage = strPromptMessage
End Function



'''
'''
'''
''' <Argument>rstrCommandLines: Output</Argument>
''' <Argument>vstrMessage: Input</Argument>
''' <Argument>vblnHideEchoCommand: Input</Argument>
Private Sub msubAddCommandLinesAboutEcho(ByRef rstrCommandLines As String, _
        ByVal vstrMessage As String, _
        Optional ByVal vblnHideEchoCommand As Boolean = True)

    Dim strCommand As String
    
    strCommand = ""
    
    If vblnHideEchoCommand Then
    
        strCommand = strCommand & "@"
    End If
    
    strCommand = strCommand & "echo " & vstrMessage
    
    rstrCommandLines = rstrCommandLines & strCommand & vbNewLine & vbNewLine
End Sub


'''
'''
'''
''' <Argument>rstrCommandLines: Output</Argument>
''' <Argument>vblnEchoOn: Input</Argument>
''' <Argument>vblnHideEchoCommand: Input</Argument>
Private Sub msubAddCommandLinesAboutEchoEnabling(ByRef rstrCommandLines As String, _
        ByVal vblnEchoOn As Boolean, _
        Optional ByVal vblnHideEchoCommand As Boolean = True)

    Dim strCommand As String
    
    strCommand = ""
    
    If vblnHideEchoCommand Then
    
        strCommand = strCommand & "@"
    End If
    
    If vblnEchoOn Then
    
        strCommand = strCommand & "echo on"
    Else
        strCommand = strCommand & "echo off"
    End If
    
    rstrCommandLines = rstrCommandLines & strCommand & vbNewLine & vbNewLine
End Sub

'''
'''
'''
''' <Argument>rstrCommandLines: Output</Argument>
''' <Argument>vstrDir: Input</Argument>
Private Sub msubAddCommandLinesAboutMoveCurrentDirectory(ByRef rstrCommandLines As String, _
        ByVal vstrDir As String)

    With New Scripting.FileSystemObject
    
        rstrCommandLines = rstrCommandLines & .GetDriveName(vstrDir) & vbNewLine & vbNewLine
    End With
    
    rstrCommandLines = rstrCommandLines & "cd " & vstrDir & vbNewLine & vbNewLine
End Sub


'''
'''
'''
''' <Argument>rstrCommandLines: Input</Argument>
''' <Argument>vstrBatchFileName: Input</Argument>
''' <Argument>vstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vblnOnlyCreateBatchFile: Input</Argument>
Private Sub msubGitCommitByCmdBatcheFileOrOnlyCreateBatcheFile(ByRef rstrCommandLines As String, _
        ByVal vstrBatchFileName As String, _
        ByVal vstrGitRepositoryRootDirectoryPath As String, _
        Optional ByVal vblnOnlyCreateBatchFile As Boolean = False)

    If vblnOnlyCreateBatchFile Then

        CreateTemporaryBatchFileByFSO vstrGitRepositoryRootDirectoryPath, vstrBatchFileName, rstrCommandLines
        
        ExploreFolderWhenOpenedThatIsNothing vstrGitRepositoryRootDirectoryPath
    Else
        With New DoubleStopWatch
    
            .MeasureStart
        
            Debug.Print "Before start to git-commit at " & Format(Now(), "ddd, yyyy/m/d hh:mm AM/PM")
         
            ' execute git commit after git add
         
            ExecuteWinCmdBatchFileByRun vstrGitRepositoryRootDirectoryPath, vstrBatchFileName, _
                    rstrCommandLines
        
            'ExecuteWinCmdBatchFileWithGettingStdErr vstrGitRepositoryRootDirectoryPath, vstrBatchFileName, rstrCommandLines
            
            .MeasureInterval
            
            Debug.Print "Git commit elapsed time: " & .ElapsedTimeByString
        End With
    End If
End Sub


'''
'''
'''
''' <Argument>rstrCommandLines: Output</Argument>
''' <Argument>rstrCommitMessage: Input</Argument>
''' <Argument>rstrGitRepositoryRootDirectoryPath: Input</Argument>
Private Sub msubAddCommandLinesAboutGitCommit(ByRef rstrCommandLines As String, _
        ByRef rstrCommitMessage As String, _
        ByRef rstrGitRepositoryRootDirectoryPath As String)


    Dim blnDoSignatureCommit As Boolean

    blnDoSignatureCommit = IsSignatureCommitEnabled(rstrGitRepositoryRootDirectoryPath)
    
    ' add Git commit command
    
    If blnDoSignatureCommit Then
    
        msubAddCmdCommandWithCmdComment rstrCommandLines, _
                GetColFromLineDelimitedChar(GetGitCommitCommand(rstrCommitMessage, blnDoSignatureCommit), vbNewLine), _
                "Do signed-commit Git"
    Else
        msubAddCmdCommandWithCmdComment rstrCommandLines, _
                GetColFromLineDelimitedChar(GetGitCommitCommand(rstrCommitMessage, blnDoSignatureCommit), vbNewLine), _
                "Do commit Git"
    End If
End Sub

'''
'''
'''
''' <Argument>rstrCommitCommentOfNewAdd: Input</Argument>
''' <Argument>rstrCommitCommentOfUpdate: Input</Argument>
''' <Argument>rstrCommitCommentOfRemove: Input</Argument>
''' <Argument>rblnIsThisTimeFirstCommit: Input</Argument>
''' <Argument>vstrHeaderCommitMessageTitle: Input</Argument>
Private Function mfstrGetCommitIntegratedMessage(ByRef rstrCommitCommentOfNewAdd As String, _
        ByRef rstrCommitCommentOfUpdate As String, _
        ByRef rstrCommitCommentOfRemove As String, _
        ByRef rblnIsThisTimeFirstCommit As Boolean, _
        Optional ByVal vstrHeaderCommitMessageTitle As String = "")

    Dim strCommitMessage As String
    
    strCommitMessage = mfstrGetCommitMainMessage(rstrCommitCommentOfNewAdd, _
                rstrCommitCommentOfUpdate, _
                rstrCommitCommentOfRemove, _
                vstrHeaderCommitMessageTitle)
    
    If rblnIsThisTimeFirstCommit Then
    
        strCommitMessage = "This is the first commit. - " & vbNewLine & strCommitMessage
    End If

    mfstrGetCommitIntegratedMessage = strCommitMessage
End Function

'''
'''
'''
''' <Argument>rstrCommandLines: Output</Argument>
''' <Argument>rstrCommitCommentOfNewAdd: Output</Argument>
''' <Argument>robjNeedToAddNewSrcKeysDic: Input</Argument>
''' <Argument>vstrGitRepositoryRootDirectoryPath: Input</Argument>
Private Sub msubAddCommandLinesAboutAddingNew(ByRef rstrCommandLines As String, _
        ByRef rstrCommitCommentOfNewAdd As String, _
        ByRef robjNeedToAddNewSrcKeysDic As Scripting.Dictionary, _
        ByVal vstrGitRepositoryRootDirectoryPath As String, _
        Optional ByVal vstrExtensionPairsDelimitedByComma As String = "frm;frx")

    Dim objGitCommands As Collection

    rstrCommitCommentOfNewAdd = ""
    
    If robjNeedToAddNewSrcKeysDic.Count > 0 Then
    
        Set objGitCommands = GetGitAddCommandsAndAddNewFilesToGitRepository(robjNeedToAddNewSrcKeysDic, _
                vstrGitRepositoryRootDirectoryPath, _
                vstrExtensionPairsDelimitedByComma)
        
        rstrCommitCommentOfNewAdd = "Count of new added files: " & CStr(objGitCommands.Count)
        
        msubAddCmdCommandWithCmdComment rstrCommandLines, objGitCommands, rstrCommitCommentOfNewAdd
    End If
End Sub

'''
'''
'''
''' <Argument>rstrCommandLines: Output</Argument>
''' <Argument>rstrCommitCommentOfUpdate: Output</Argument>
''' <Argument>robjNeedToUpdateSrcToDstDic: Input</Argument>
''' <Argument>vstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vstrExtensionPairsDelimitedByComma</Argument>
Private Sub msubAddCommandLinesAboutUpdatingGitRepositoryFiles(ByRef rstrCommandLines As String, _
        ByRef rstrCommitCommentOfUpdate As String, _
        ByRef robjNeedToUpdateSrcToDstDic As Scripting.Dictionary, _
        ByVal vstrGitRepositoryRootDirectoryPath As String, _
        Optional ByVal vstrExtensionPairsDelimitedByComma As String = "frm;frx")

    Dim objGitCommands As Collection
    
    rstrCommitCommentOfUpdate = ""

    If robjNeedToUpdateSrcToDstDic.Count > 0 Then
    
        Set objGitCommands = GetGitAddCommandsAndAddNewFilesToGitRepository(robjNeedToUpdateSrcToDstDic, _
                vstrGitRepositoryRootDirectoryPath, _
                vstrExtensionPairsDelimitedByComma)
    
        rstrCommitCommentOfUpdate = "Count of updated files: " & CStr(objGitCommands.Count)
        
        msubAddCmdCommandWithCmdComment rstrCommandLines, objGitCommands, rstrCommitCommentOfUpdate
    End If
End Sub

'''
'''
'''
''' <Argument>rstrCommandLines: Output</Argument>
''' <Argument>rstrCommitCommentOfRemove: Output</Argument>
''' <Argument>robjNeedToRemoveDstKeysDic: Input</Argument>
''' <Argument>vstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vstrExtensionPairsDelimitedByComma</Argument>
Private Sub msubAddCommandLinesAboutRemovingGitRepositoryFiles(ByRef rstrCommandLines As String, _
        ByRef rstrCommitCommentOfRemove As String, _
        ByRef robjNeedToRemoveDstKeysDic As Scripting.Dictionary, _
        ByVal vstrGitRepositoryRootDirectoryPath As String, _
        Optional ByVal vstrExtensionPairsDelimitedByComma As String = "frm;frx")

    Dim objGitCommands As Collection
    
    rstrCommitCommentOfRemove = ""

    If robjNeedToRemoveDstKeysDic.Count > 0 Then
    
        Set objGitCommands = GetGitRemoveCommandAndRemoveFilesFromGitRepository(robjNeedToRemoveDstKeysDic, _
                vstrGitRepositoryRootDirectoryPath, _
                vstrExtensionPairsDelimitedByComma)
    
        rstrCommitCommentOfRemove = "Count of removed files: " & CStr(objGitCommands.Count)
        
        msubAddCmdCommandWithCmdComment rstrCommandLines, objGitCommands, rstrCommitCommentOfRemove
    End If
End Sub


'''
'''
'''
Private Function IsThisGitRepositoryDirectoryFirstCommitAndIfYesThenInitilizeItAsGitRepository(ByVal vstrGitRepositoryRootDirectoryPath As String) As Boolean

    Dim blnIsThisTimeFirstCommit As Boolean

    blnIsThisTimeFirstCommit = False
    
    If Not IsLocalGitRepositoryExists(vstrGitRepositoryRootDirectoryPath) Then
    
        CreateLocalGitRepository vstrGitRepositoryRootDirectoryPath
        
        blnIsThisTimeFirstCommit = True
    End If

    IsThisGitRepositoryDirectoryFirstCommitAndIfYesThenInitilizeItAsGitRepository = blnIsThisTimeFirstCommit
End Function


'''
'''
'''
''' <Argument>rstrCommandLines: Output</Argument>
''' <Argument>robjCmdCommands: Input</Argument>
''' <Argument>vstrCmdComment: Input</Argument>
Private Sub msubAddCmdCommandWithCmdComment(ByRef rstrCommandLines As String, _
        ByRef robjCmdCommands As Collection, _
        Optional ByVal vstrCmdComment As String = "")

    Dim varCommand As String
    
    
    If rstrCommandLines <> "" Then
    
        rstrCommandLines = rstrCommandLines & vbNewLine & vbNewLine
    End If
    
    If vstrCmdComment <> "" Then
    
        rstrCommandLines = rstrCommandLines & "rem " & vstrCmdComment
    End If
    
    GetBatchCommandStringFromCommandCol rstrCommandLines, robjCmdCommands
End Sub

'''
'''
'''
''' <Argument>rstrCommitCommentOfNewAdd: Input</Argument>
''' <Argument>rstrCommitCommentOfUpdate: Input</Argument>
''' <Argument>rstrCommitCommentOfRemove: Input</Argument>
''' <Argument>vstrHeaderCommitMessageTitle: Input</Argument>
''' <Return>String</Return>
Private Function mfstrGetCommitMainMessage(ByRef rstrCommitCommentOfNewAdd As String, _
        ByRef rstrCommitCommentOfUpdate As String, _
        ByRef rstrCommitCommentOfRemove As String, _
        Optional ByVal vstrHeaderCommitMessageTitle As String = "") As String


    Dim strMessage As String

    strMessage = ""

    If vstrHeaderCommitMessageTitle <> "" Then
    
        strMessage = strMessage & vstrHeaderCommitMessageTitle & vbNewLine
    End If

    If rstrCommitCommentOfNewAdd <> "" Then
    
        strMessage = strMessage & rstrCommitCommentOfNewAdd
    End If

    If rstrCommitCommentOfUpdate <> "" Then
    
        If strMessage <> "" Then
        
            strMessage = strMessage & ", "
        End If
    
        strMessage = strMessage & rstrCommitCommentOfUpdate
    End If
    
    If rstrCommitCommentOfRemove <> "" Then
    
        If strMessage <> "" Then
        
            strMessage = strMessage & ", "
        End If
    
        strMessage = strMessage & rstrCommitCommentOfRemove
    End If

    mfstrGetCommitMainMessage = strMessage
End Function


'''
'''
'''
Private Function GetGitAddCommandsAndAddNewFilesToGitRepository(ByRef robjNeedToAddNewSrcKeysDic As Scripting.Dictionary, _
        ByRef rstrGitRepositoryRootDirectoryPath As String, _
        Optional ByVal vstrExtensionPairsDelimitedByComma As String = "frm;frx") As Collection


    Dim varSrcPath As Variant, strSrcPath As String, strDstPath As String, strOtherSrcPath As String, strOtherDstPath As String, strShellCommand As String
    
    Dim strRelativePath As String, objCommands As Collection, varCommand As Variant, strBatchCommands As String
    
    Dim strExtension As String
    
    Dim objExtensionPairDic As Scripting.Dictionary, blnIsFileExtensionPairNeededToExpand As Boolean
    
    
    blnIsFileExtensionPairNeededToExpand = False
    
    If vstrExtensionPairsDelimitedByComma <> "" Then
    
        blnIsFileExtensionPairNeededToExpand = True
    
        Set objExtensionPairDic = GetTextToTextDicFromTwoTypeLineDelimitedChars(vstrExtensionPairsDelimitedByComma)
    End If

    Set objCommands = New Collection

    With robjNeedToAddNewSrcKeysDic
        
        For Each varSrcPath In .Keys

            strSrcPath = varSrcPath

            strDstPath = .Item(varSrcPath)

            FileCopySrcToDstForSynchronizingPathByVba strSrcPath, strDstPath
            
            objCommands.Add GetCmdShellGitAddFileCommand(strDstPath, rstrGitRepositoryRootDirectoryPath)
            
            If blnIsFileExtensionPairNeededToExpand Then
            
                strExtension = GetExtensionNameByVbaDir(strSrcPath)
                
                With objExtensionPairDic
                
                    If .Exists(strExtension) Then
                
                        strOtherSrcPath = GetPathByChagingFileExtension(strSrcPath, .Item(strExtension))
                        
                        strOtherDstPath = GetPathByChagingFileExtension(strDstPath, .Item(strExtension))
                        
                        
                        FileCopySrcToDstForSynchronizingPathByVba strOtherSrcPath, strOtherDstPath
                        
                        objCommands.Add GetCmdShellGitAddFileCommand(strOtherDstPath, rstrGitRepositoryRootDirectoryPath)
                    End If
                End With
            End If
        Next
        
        Set GetGitAddCommandsAndAddNewFilesToGitRepository = objCommands
    End With
End Function

'''
'''
'''
''' <Argument>robjNeedToRemoveDstKeysDic: Input</Argument>
''' <Argument>rstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vstrExtensionPairsDelimitedByComma: Input</Argument>
''' <Return>Collection: git commands Collection(Of String)</Return>
Private Function GetGitRemoveCommandAndRemoveFilesFromGitRepository(ByRef robjNeedToRemoveDstKeysDic As Scripting.Dictionary, _
        ByRef rstrGitRepositoryRootDirectoryPath As String, _
        Optional ByVal vstrExtensionPairsDelimitedByComma As String = "frm;frx") As Collection


    Dim varDstPath As Variant, strDstPath As String, strOtherDstPath As String, strShellCommand As String
    
    Dim strRelativePath As String, objCommands As Collection, varCommand As Variant, strBatchCommands As String, strExtension As String
    
    Dim objExtensionPairDic As Scripting.Dictionary, blnIsFileExtensionPairNeededToExpand As Boolean
    
    
    If vstrExtensionPairsDelimitedByComma <> "" Then
    
        blnIsFileExtensionPairNeededToExpand = True
    
        Set objExtensionPairDic = GetTextToTextDicFromTwoTypeLineDelimitedChars(vstrExtensionPairsDelimitedByComma)
    End If

    Set objCommands = New Collection

    With robjNeedToRemoveDstKeysDic
        
        For Each varDstPath In .Keys

            strDstPath = varDstPath

            If FileExistsByVbaDir(strDstPath) Then: VBA.Kill strDstPath
            
            objCommands.Add GetCmdShellGitRemoveFileCommand(strDstPath, rstrGitRepositoryRootDirectoryPath)
            
            If blnIsFileExtensionPairNeededToExpand Then
            
                strExtension = GetExtensionNameByVbaDir(strDstPath)
                
                With objExtensionPairDic
                
                    If .Exists(strExtension) Then
                    
                        strOtherDstPath = GetPathByChagingFileExtension(strDstPath, .Item(strExtension))
                        
                        If FileExistsByVbaDir(strOtherDstPath) Then: VBA.Kill strOtherDstPath
                        
                        objCommands.Add GetCmdShellGitRemoveFileCommand(strOtherDstPath, rstrGitRepositoryRootDirectoryPath)
                    End If
                End With
            End If
        Next
        
        Set GetGitRemoveCommandAndRemoveFilesFromGitRepository = objCommands
    End With
End Function


'''
'''
'''
Private Function GetCmdShellGitAddFileCommand(ByRef rstrFullPath As String, _
        ByRef rstrLeftSideDictionaryPath As String) As String

    Dim strRelativePath As String, strShellCommand As String

    strRelativePath = GetShellRelativePath(rstrFullPath, rstrLeftSideDictionaryPath)
    
    strShellCommand = "git add " & strRelativePath

    GetCmdShellGitAddFileCommand = strShellCommand
End Function

'''
'''
'''
Private Function GetCmdShellGitRemoveFileCommand(ByRef rstrFullPath As String, _
        ByRef rstrLeftSideDictionaryPath As String) As String

    Dim strRelativePath As String, strShellCommand As String

    strRelativePath = GetShellRelativePath(rstrFullPath, rstrLeftSideDictionaryPath)
    
    strShellCommand = "git rm " & strRelativePath

    GetCmdShellGitRemoveFileCommand = strShellCommand
End Function



'''
''' use VBA.FileCopy
'''
Public Sub FileCopySrcToDstForSynchronizingWithRelativePathByVba(ByRef rvarRelativePath As Variant, _
        ByRef rstrSrcRootDirectoryPath As String, _
        ByRef rstrDstRootDirectoryPath As String)


    Dim strSrcPath As String, strDstPath As String

    strSrcPath = rstrSrcRootDirectoryPath & "\" & rvarRelativePath
    
    strDstPath = rstrDstRootDirectoryPath & "\" & rvarRelativePath
    
    FileCopySrcToDstForSynchronizingPathByVba strSrcPath, strDstPath
End Sub

'''
''' use VBA.FileCopy
'''
Public Sub FileCopySrcToDstForSynchronizingPathByVba(ByRef rstrSrcPath As String, ByRef rstrDstPath As String)

    Dim strDstDir As String
    
    GetParentDirectoryPathToArgByVbaDir strDstDir, rstrDstPath
    
    ForceToCreateDirectoryByVbaDir strDstDir
    
    VBA.FileCopy rstrSrcPath, rstrDstPath
End Sub


'''
'''
'''
''' <Argument>robjNeedToRemoveDstKeysDic: Output</Argument>
''' <Argument>robjRelativePathKeys: Input</Argument>
''' <Argument>rstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vstrExcludedFileExtensionsDelimitedByComma: Input</Argument>
Private Sub msubGetGitAllRemovingFileListDictionaries(ByRef robjNeedToRemoveDstKeysDic As Scripting.Dictionary, _
        ByRef robjRelativePathKeys As Scripting.Dictionary, _
        ByRef rstrGitRepositoryRootDirectoryPath As String, _
        Optional ByVal vstrExcludedFileExtensionsDelimitedByComma As String = "frx")


    Dim strSrcPath As String, strDstPath As String, varRelativePath As Variant, blnNeedToFilterByFileExtension As Boolean, blnContinue As Boolean
    Dim objExcludedExtensionsRegExp As VBScript_RegExp_55.RegExp, blnSrcFileExists As Boolean, blnDstFileExists As Boolean, strExtension As String
    
    
    msubGetExcludedExtensionsRegExp objExcludedExtensionsRegExp, blnNeedToFilterByFileExtension, vstrExcludedFileExtensionsDelimitedByComma
    
    
    Set robjNeedToRemoveDstKeysDic = New Scripting.Dictionary
    
    With robjRelativePathKeys
    
        For Each varRelativePath In .Keys
        
            strDstPath = rstrGitRepositoryRootDirectoryPath & "\" & varRelativePath
        
            blnContinue = True
            
            If blnNeedToFilterByFileExtension Then
            
                strExtension = GetExtensionNameByVbaDir(strDstPath)
        
                ' the .frx file is to be also processed with the corresponding .frm file after the this procedure.
        
                blnContinue = Not objExcludedExtensionsRegExp.Test(strExtension)
            End If
        
            If blnContinue Then
            
                If FileExistsByVbaDir(strDstPath) Then
                
                    ' need to remove the destination side file from Git repository
                
                    robjNeedToRemoveDstKeysDic.Add strDstPath, 0
                End If
            End If
        Next
    End With
End Sub

'''
'''
'''
''' <Argument>robjNeedToUpdateSrcToDstDic: Output</Argument>
''' <Argument>robjNeedToAddNewSrcKeysDic: Output</Argument>
''' <Argument>robjNeedToRemoveDstKeysDic: Output</Argument>
''' <Argument>robjRelativePathKeys: Input</Argument>
''' <Argument>rstrOnlyFileRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>rstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vstrManagingFileWildCardsDelimitedByComma: Input - for example, vstrManagingFileWildCardsDelimitedByComma is "*.bas,*.cls,*.frm,*.frx" or "*.txt", or "*.vbs,*.bat"</Argument>
''' <Argument>vstrExcludedFileExtensionsDelimitedByComma: Input</Argument>
''' <Argument>venmTwoFilesDifferenceCompareMethod: Input</Argument>
''' <Argument>vobjIgnoreRegExpPatternsDic: Input</Argument>
''' <Argument>vstrSheetNamesDelimitedByComma: Input</Argument>
''' <Argument>vblnAllowToSearchToRemovingFilesFromGitRepositoryDirectory: Input</Argument>
Private Sub msubGetGitOperationNeededFileListDictionaries(ByRef robjNeedToUpdateSrcToDstDic As Scripting.Dictionary, _
        ByRef robjNeedToAddNewSrcKeysDic As Scripting.Dictionary, _
        ByRef robjNeedToRemoveDstKeysDic As Scripting.Dictionary, _
        ByRef robjRelativePathKeys As Scripting.Dictionary, _
        ByRef rstrOnlyFileRepositoryRootDirectoryPath As String, _
        ByRef rstrGitRepositoryRootDirectoryPath As String, _
        ByVal vstrManagingFileWildCardsDelimitedByComma As String, _
        Optional ByVal vstrExcludedFileExtensionsDelimitedByComma As String = "frx", _
        Optional ByVal venmTwoFilesDifferenceCompareMethod As TwoFilesDifferenceCompareMethod = TwoFilesDifferenceCompareMethod.CompareTwoFilesDifferencesAboutText, _
        Optional ByVal vobjIgnoreRegExpPatternsDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrSheetNamesDelimitedByComma As String = "", _
        Optional ByVal vblnAllowToSearchToRemovingFilesFromGitRepositoryDirectory As Boolean = True)


    Dim blnNeedToFilterByFileExtension As Boolean, objExcludedExtensionsRegExp As VBScript_RegExp_55.RegExp
    
    
    msubGetExcludedExtensionsRegExp objExcludedExtensionsRegExp, _
            blnNeedToFilterByFileExtension, _
            vstrExcludedFileExtensionsDelimitedByComma
    
    
    Set robjNeedToUpdateSrcToDstDic = New Scripting.Dictionary: Set robjNeedToAddNewSrcKeysDic = New Scripting.Dictionary: Set robjNeedToRemoveDstKeysDic = New Scripting.Dictionary
    
    ' Update by forward
    
    msubGetGitOperationNeededFileListFromOnlyFileRepositorySide robjNeedToUpdateSrcToDstDic, _
            robjNeedToAddNewSrcKeysDic, _
            robjNeedToRemoveDstKeysDic, _
            robjRelativePathKeys, _
            rstrOnlyFileRepositoryRootDirectoryPath, _
            rstrGitRepositoryRootDirectoryPath, _
            blnNeedToFilterByFileExtension, _
            objExcludedExtensionsRegExp, _
            venmTwoFilesDifferenceCompareMethod, _
            vobjIgnoreRegExpPatternsDic, _
            vstrSheetNamesDelimitedByComma
    
    
    ' Update by backward
    
    If vblnAllowToSearchToRemovingFilesFromGitRepositoryDirectory Then
    
        ' for example, vstrManagingFileWildCardsDelimitedByComma is "*.bas,*.cls,*.frm,*.frx" or "*.txt", or "*.vbs,*.bat"
    
        msubGetGitOperationNeededFileListFromGitRepositorySide robjNeedToRemoveDstKeysDic, _
                robjRelativePathKeys, _
                rstrOnlyFileRepositoryRootDirectoryPath, _
                rstrGitRepositoryRootDirectoryPath, _
                vstrManagingFileWildCardsDelimitedByComma
    End If
End Sub

'''
''' check by backward (from GitRepository files)
'''
''' <Argument>robjNeedToRemoveDstKeysDic: Output</Argument>
''' <Argument>robjRelativePathKeys: Input</Argument>
''' <Argument>rstrOnlyFileRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>rstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>vstrManagingFileWildCardsDelimitedByComma: Input</Argument>
Private Sub msubGetGitOperationNeededFileListFromGitRepositorySide(ByRef robjNeedToRemoveDstKeysDic As Scripting.Dictionary, _
        ByRef robjRelativePathKeys As Scripting.Dictionary, _
        ByRef rstrOnlyFileRepositoryRootDirectoryPath As String, _
        ByRef rstrGitRepositoryRootDirectoryPath As String, _
        ByVal vstrManagingFileWildCardsDelimitedByComma As String)


    Dim varRelativePath As Variant, strPath As String, strDir As String, varDir As Variant, varPath As Variant, strRelativePath As String, objDirKeysDic As Scripting.Dictionary
    
    Dim strDstPath As String
    
    Dim objGitRepoRelativePathKeys As Scripting.Dictionary
    
    Dim objDummyCommonKeyDic01 As Scripting.Dictionary, objDummyCommonKeyDic02 As Scripting.Dictionary
    
    Dim objDummyIndependentKeyDic01 As Scripting.Dictionary, objToRemoveRelativeKeysDic  As Scripting.Dictionary
    
    Dim objFilePaths As Collection
    
    
    Set objDirKeysDic = New Scripting.Dictionary
    
    With robjRelativePathKeys
    
        For Each varRelativePath In .Keys
        
            strPath = rstrGitRepositoryRootDirectoryPath & "\" & varRelativePath
        
            strDir = GetParentDirectoryPathByVbaDir(strPath)
            
            If DirectoryExistsByVbaDir(strDir) Then
            
                With objDirKeysDic
                
                    If Not .Exists(strDir) Then
                    
                        .Add strDir, 0
                    End If
                End With
            End If
        Next
        
        Set objGitRepoRelativePathKeys = New Scripting.Dictionary
        
        For Each varDir In objDirKeysDic.Keys
        
            strDir = varDir
        
            Set objFilePaths = GetFilePathsUsingVbaDir(strDir, _
                    vstrManagingFileWildCardsDelimitedByComma, _
                    "FullPath", _
                    0, _
                    0)
            
            For Each varPath In objFilePaths
            
                strRelativePath = Replace(varPath, rstrGitRepositoryRootDirectoryPath & "\", "")
                
                objGitRepoRelativePathKeys.Add strRelativePath, 0
            Next
        Next
        
        
        CompareKeysOfTwoDictionaries objDummyCommonKeyDic01, _
                objDummyCommonKeyDic02, _
                objDummyIndependentKeyDic01, _
                objToRemoveRelativeKeysDic, _
                robjRelativePathKeys, _
                objGitRepoRelativePathKeys
        
        
        If Not objToRemoveRelativeKeysDic Is Nothing Then
        
            If objToRemoveRelativeKeysDic.Count > 0 Then
        
                For Each varRelativePath In objToRemoveRelativeKeysDic.Keys
                
                    ' need to remove the destination side file from Git repository
    
                    strDstPath = rstrGitRepositoryRootDirectoryPath & "\" & varRelativePath
    
                    robjNeedToRemoveDstKeysDic.Add strDstPath, 0
                Next
            End If
        End If
    End With
End Sub


'''
''' check by forward (from OnlyFileRepository and that RelativePathKeys)
'''
''' <Argument>robjNeedToUpdateSrcToDstDic: Output</Argument>
''' <Argument>robjNeedToAddNewSrcKeysDic: Output</Argument>
''' <Argument>robjNeedToRemoveDstKeysDic: Output</Argument>
''' <Argument>robjRelativePathKeys: Input</Argument>
''' <Argument>rstrOnlyFileRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>rstrGitRepositoryRootDirectoryPath: Input</Argument>
''' <Argument>rblnNeedToFilterByFileExtension: Input</Argument>
''' <Argument>robjExcludedExtensionsRegExp: Input</Argument>
''' <Argument>venmTwoFilesDifferenceCompareMethod: Input</Argument>
''' <Argument>vobjIgnoreRegExpPatternsDic: Input</Argument>
''' <Argument>vstrSheetNamesDelimitedByComma: Input</Argument>
Private Sub msubGetGitOperationNeededFileListFromOnlyFileRepositorySide(ByRef robjNeedToUpdateSrcToDstDic As Scripting.Dictionary, _
        ByRef robjNeedToAddNewSrcKeysDic As Scripting.Dictionary, _
        ByRef robjNeedToRemoveDstKeysDic As Scripting.Dictionary, _
        ByRef robjRelativePathKeys As Scripting.Dictionary, _
        ByRef rstrOnlyFileRepositoryRootDirectoryPath As String, _
        ByRef rstrGitRepositoryRootDirectoryPath As String, _
        ByRef rblnNeedToFilterByFileExtension As Boolean, _
        ByRef robjExcludedExtensionsRegExp As VBScript_RegExp_55.RegExp, _
        Optional ByVal venmTwoFilesDifferenceCompareMethod As TwoFilesDifferenceCompareMethod = TwoFilesDifferenceCompareMethod.CompareTwoFilesDifferencesAboutText, _
        Optional ByVal vobjIgnoreRegExpPatternsDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrSheetNamesDelimitedByComma As String = "")


    Dim strSrcPath As String, strDstPath As String, varRelativePath As Variant, blnContinue As Boolean
    
    Dim strExtension As String
    

    With robjRelativePathKeys
    
        For Each varRelativePath In .Keys
        
            strSrcPath = rstrOnlyFileRepositoryRootDirectoryPath & "\" & varRelativePath
            
            strDstPath = rstrGitRepositoryRootDirectoryPath & "\" & varRelativePath
        
            blnContinue = True
            
            If rblnNeedToFilterByFileExtension Then
            
                strExtension = GetExtensionNameByVbaDir(strSrcPath)
        
                ' the .frx file is to be also processed with the corresponding .frm file after the this procedure.
        
                blnContinue = Not robjExcludedExtensionsRegExp.Test(strExtension)
            End If
        
            If blnContinue Then
            
                msubGetFileSynchonizingClassifiedList robjNeedToUpdateSrcToDstDic, _
                        robjNeedToAddNewSrcKeysDic, _
                        robjNeedToRemoveDstKeysDic, _
                        strSrcPath, _
                        strDstPath, _
                        venmTwoFilesDifferenceCompareMethod, _
                        vobjIgnoreRegExpPatternsDic, _
                        vstrSheetNamesDelimitedByComma
            End If
        Next
    End With
End Sub


'''
'''
'''
''' <Argument>robjNeedToUpdateSrcToDstDic: Output</Argument>
''' <Argument>robjNeedToAddNewSrcKeysDic: Output</Argument>
''' <Argument>robjNeedToRemoveDstKeysDic: Output</Argument>
''' <Argument>rstrSrcPath: Input</Argument>
''' <Argument>rstrDstPath: Input</Argument>
''' <Argument>renmTwoFilesDifferenceCompareMethod: Input</Argument>
''' <Argument>vobjIgnoreRegExpPatternsDic: Input</Argument>
''' <Argument>vstrSheetNamesDelimitedByComma: Input</Argument>
Private Sub msubGetFileSynchonizingClassifiedList(ByRef robjNeedToUpdateSrcToDstDic As Scripting.Dictionary, _
        ByRef robjNeedToAddNewSrcKeysDic As Scripting.Dictionary, _
        ByRef robjNeedToRemoveDstKeysDic As Scripting.Dictionary, _
        ByRef rstrSrcPath As String, _
        ByRef rstrDstPath As String, _
        ByRef renmTwoFilesDifferenceCompareMethod As TwoFilesDifferenceCompareMethod, _
        Optional ByVal vobjIgnoreRegExpPatternsDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrSheetNamesDelimitedByComma As String = "")

    Dim blnSrcFileExists As Boolean, blnDstFileExists As Boolean

    blnSrcFileExists = FileExistsByVbaDir(rstrSrcPath)
                
    blnDstFileExists = FileExistsByVbaDir(rstrDstPath)

    If blnSrcFileExists And blnDstFileExists Then
    
        ' Compare the source-code files between src-only file repository and dst Git repository
        
        If IsDifferentBetweenTwoFilesBySpecifiedMethod(rstrSrcPath, _
                rstrDstPath, _
                renmTwoFilesDifferenceCompareMethod, _
                vobjIgnoreRegExpPatternsDic, _
                vstrSheetNamesDelimitedByComma) Then
        
            ' need to update the Git repository
        
            robjNeedToUpdateSrcToDstDic.Add rstrSrcPath, rstrDstPath
        Else
            ' Nothing to do
        End If
    
    ElseIf blnSrcFileExists And Not blnDstFileExists Then
    
        ' need to register new source side file into the Git repository
    
        robjNeedToAddNewSrcKeysDic.Add rstrSrcPath, rstrDstPath
    
    ElseIf Not blnSrcFileExists And blnDstFileExists Then
    
        ' need to remove the destination side file from Git repository
    
        robjNeedToRemoveDstKeysDic.Add rstrDstPath, 0
    End If
End Sub

'''
'''
'''
Public Function IsDifferentBetweenTwoFilesBySpecifiedMethod(ByRef rstrSrcPath As String, _
        ByRef rstrDstPath As String, _
        ByRef renmTwoFilesDifferenceCompareMethod As TwoFilesDifferenceCompareMethod, _
        Optional ByVal vobjIgnoreRegExpPatternsDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vstrSheetNamesDelimitedByComma As String = "") As Boolean

    Dim blnIsTwoFilesDifferent As Boolean

    Select Case renmTwoFilesDifferenceCompareMethod
    
        Case TwoFilesDifferenceCompareMethod.CompareTwoFilesDifferencesAboutText
        
            blnIsTwoFilesDifferent = IsDifferentBetweenTextFilesByFileSystemObject(rstrSrcPath, rstrDstPath)
        
        Case TwoFilesDifferenceCompareMethod.CompareTwoFilesDifferencesAboutTextWithoutSpecifiedIgnoredConditions
        
            blnIsTwoFilesDifferent = IsDifferentBetweenTextFilesWithSpecifiedIgnoreRegExpPatternLinesByFileSystemObject(rstrSrcPath, rstrDstPath, vobjIgnoreRegExpPatternsDic)
        
        Case TwoFilesDifferenceCompareMethod.CompareTwoNoPasswordLockedExcelBookFilesDifferencesAboutOnlySheetTablesUsingAdo
        
            blnIsTwoFilesDifferent = IsDifferentPartBetweenTwoBookSheetsUsingAdo(rstrSrcPath, rstrDstPath, vstrSheetNamesDelimitedByComma)
    End Select

    IsDifferentBetweenTwoFilesBySpecifiedMethod = blnIsTwoFilesDifferent
End Function

'''
'''
'''
''' <Argument>robjExcludedExtensionsRegExp: Output</Argument>
''' <Argument>rblnNeedToFilterByFileExtension: Output</Argument>
''' <Argument>rstrExcludedFileExtensionsDelimitedByComma: Input</Argument>
Private Sub msubGetExcludedExtensionsRegExp(ByRef robjExcludedExtensionsRegExp As VBScript_RegExp_55.RegExp, _
        ByRef rblnNeedToFilterByFileExtension As Boolean, _
        ByRef rstrExcludedFileExtensionsDelimitedByComma As String)

    rblnNeedToFilterByFileExtension = False
    
    If rstrExcludedFileExtensionsDelimitedByComma <> "" Then
    
        Set robjExcludedExtensionsRegExp = GetRegExpObjectFromGeneralCacheAfterSetting(Replace(rstrExcludedFileExtensionsDelimitedByComma, ",", "|"), True)
        
        rblnNeedToFilterByFileExtension = True
    End If
End Sub




