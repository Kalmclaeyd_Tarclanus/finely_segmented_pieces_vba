Attribute VB_Name = "UTfOperateWinGit"
'
'   Sanity tests for 'Git for Windows'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Windows OS and 'Git for Windows'
'       Dependent on Excel
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri,  2/Jun/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToUTfOperateWinGit()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", _
            "OperateWinGit,OperateWinGitForXl,OperateWinGitByMetaStruct,OperateWinGitEachVBProject"
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** About Git common settings
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCheckGitUserName()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git config user.name"
End Sub

'''
'''
'''
Private Sub msubSanityTestToCheckGitUserEmailAddress()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git config user.email"
End Sub


Private Sub msubSanityTestToSetGitTestUserName()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git config --global user.name ""Test your name"""
End Sub
Private Sub msubSanityTestToDeleteGitUserName()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git config --global user.name """""
End Sub

Private Sub msubSanityTestToSetGitTestUserEmailAddress()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git config --global user.email ""Test_e_mail@Test_example.com"""
End Sub
Private Sub msubSanityTestToDeleteGitUserEmailAddress()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git config --global user.email """""
End Sub

'''
''' Git server setting. If you are only local user, then the settig is ordinarily nothing.
'''
Private Sub msubSanityTestToCheckGitConfigSystemStatus()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git config --system --list"
End Sub

'''
''' Git config --local --list
'''
Private Sub msubSanityTestToCheckGitConfigLocalList()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git config --local --list"
End Sub

'''
''' Git config --global --list
'''
Private Sub msubSanityTestToCheckGitConfigGlobalList()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git config --global --list"
End Sub


'''
''' Git log output test, If no source-text is registered, error text will appear.
'''
Private Sub msubSanityTestToOutputLogTestOfCurrentGitSetting()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git -c log.date=relative log -n 2"
End Sub

'''
''' edit Git config by specified editor
'''
Private Sub msubSanityTestToEditGitConfig()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git config --local --edit"
End Sub


'''
''' Git config --global --list
'''
Private Sub msubSanityTestToGetParameterDicFromGitConfig()

    Dim objDic As Scripting.Dictionary

    Set objDic = GetParameterDicFromGitConfig("git config --global --list", GetTmpTestingVirtualGitRepositoryRootPathOriginal())
    
    DebugDic objDic
End Sub


'**---------------------------------------------
'** About Git repository creation
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCheckGitRepositoryStatus()

    SendGitCommandAtTestingDirectoryWithOutputtingImmediateWindow "git status"
End Sub


'''
'''
'''
Private Sub msubSanityTestToCreateGitRepository()

    Dim strDir As String, strCommand As String, strLog As String
    
    strDir = GetTmpTestingVirtualGitRepositoryRootPathOriginal()
    
    If Not IsLocalGitRepositoryExists(strDir) Then
    
        strCommand = "git init"
        
        SendGitCommandAndGetLogText strLog, strCommand, strDir
        
        Debug.Print strLog
    Else
        Debug.Print "The Git repository has already existed at " & strDir
    End If
End Sub


'''
'''
'''
Private Sub msubSanityTestToDeleteGitRepository()

    Dim strDir As String, strCommand As String, strReceivedLog As String
    
    strDir = GetTmpTestingVirtualGitRepositoryRootPathOriginal()
    
    If IsLocalGitRepositoryExists(strDir) Then
    
        strCommand = "rmdir /S /Q .git"    ' delete a git repository in the case of Cmd.exe of Windows
        
        SendGitCommandAndGetLogText strReceivedLog, strCommand, strDir
        
        Debug.Print strReceivedLog
    Else
        Debug.Print "The Git repository doesn't exist at " & strDir & " yet"
    End If
End Sub


'''
'''
'''
Private Sub msubSanityTestToCheckGitRepositoryStatus02()

    Debug.Print IsLocalGitRepositoryExists(GetTmpTestingVirtualGitRepositoryRootPathOriginal())
End Sub

'''
''' create a testing Git repository
'''
Private Sub msubSanityTestToCreateGitRepository02()

    CreateLocalGitRepository GetTmpTestingVirtualGitRepositoryRootPathOriginal()
End Sub

'''
''' delete the testing Git repository
'''
Private Sub msubSanityTestToDeleteGitRepository02()

    DeleteLocalGitRepository GetTmpTestingVirtualGitRepositoryRootPathOriginal()
End Sub


'**---------------------------------------------
'** About Git relative path
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToGetGitRepositoryRootPath()

    Debug.Print "Git testing local repository root: ", GetTmpTestingVirtualGitRepositoryRootPathOriginal()

    Debug.Print "Git testing local this meta-structure project repository root: ", _
            GetTmpVBAThisMetaStructureMasterGitRepositoryRootPath("finely_segmented_pieces_vba") & vbNewLine

    Debug.Print "Git public project root: ", GetGitRepositoryRootPath()
End Sub

