VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "RClickCtlCodePaneHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Aggregate the control codes of adding VBE right click menu
'
'   Dependency Abstract:
'       Dependent on VBIDE
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 16/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private WithEvents mobjCommandBarEvents As VBIDE.CommandBarEvents
Attribute mobjCommandBarEvents.VB_VarHelpID = -1
Private mobjCommandBarButton As Office.CommandBarButton

'Private Const CommbandBarTitle As String = "Test title"

Private Const mintFaceID As Long = 612

Private menmRClickCodePaneMenuItemTrType As RClickCodePaneMenuItemTrType


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub Class_Terminate()

    Set mobjCommandBarEvents = Nothing
    
    Set mobjCommandBarButton = Nothing
End Sub


Private Sub mobjCommandBarEvents_Click(ByVal CommandBarControl As Object, handled As Boolean, CancelDefault As Boolean)


    'Debug.Print "Selected CommandBar button name: "; CommandBarControl.Caption

    ExecuteByRClickCodePaneMenuItemTrType menmRClickCodePaneMenuItemTrType
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Get RClickCodePaneMenuItem() As RClickCodePaneMenuItemTrType

    RClickCodePaneMenuItem = menmRClickCodePaneMenuItemTrType
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vobjApplication: Either Excel.Application, Word.Application, or PowerPoint.Application</Argument>
Public Sub AddCommandBarButton(ByVal venmRClickCodePaneMenuItemTrType As RClickCodePaneMenuItemTrType, ByRef robjCommandBar As Office.CommandBar, rintPos As Long, ByVal vobjApplication As Object)

    Dim strCaption As String

    menmRClickCodePaneMenuItemTrType = venmRClickCodePaneMenuItemTrType
    
    strCaption = GetCaptionTextFromRClickCodePaneMenuItemTrType(venmRClickCodePaneMenuItemTrType)
    
    ' If the command bar has already existed, then delete it
    On Error Resume Next
    
    robjCommandBar.Controls.Item(strCaption).Delete
    
    On Error GoTo 0

    ' Add a menu
    Set mobjCommandBarButton = robjCommandBar.Controls.Add(Office.MsoControlType.msoControlButton, , , rintPos, True)
    
    With mobjCommandBarButton
    
        .Caption = GetCaptionTextFromRClickCodePaneMenuItemTrType(venmRClickCodePaneMenuItemTrType)
        
        .TooltipText = GetTooltipTextFromRClickCodePaneMenuItemTrType(venmRClickCodePaneMenuItemTrType)
        
        .FaceID = mintFaceID
    End With
    
    ' Create the command bar events
    
    Set mobjCommandBarEvents = vobjApplication.VBE.Events.CommandBarEvents(mobjCommandBarButton)
End Sub


Public Sub DeleteCommandBarButton()

    mobjCommandBarButton.Delete
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////




