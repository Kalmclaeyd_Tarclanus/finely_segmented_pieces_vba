Attribute VB_Name = "RClickOnCodePane"
'
'   Richt-click context menu tools for VBE code-panes
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Control RClickCtlHandler class instances for each VBE(Visual Basic Editor) code panes
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 16/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' Right-click menu item type
''' Tr means the Tarclanus local selection items
'''
Public Enum RClickCodePaneMenuItemTrType

    RClickClearImmediateWindow = 1
    
    RClickOpenOpenCodePaneSwitchMultiLstFormForSingleVBProject
End Enum


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "RClickOnCodePane"


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

' Scripting.Dictionary(Of RClickMenuItemTrType, Of Office.CommandBarButton)
Public mobjRClickMenuKeyToRClickCtlHandlerDic As Scripting.Dictionary


Private mblnIsRClickCodePaneMenuAdded As Boolean

'**---------------------------------------------
'** Key-Value cache preparation for RClickOnCodePane
'**---------------------------------------------
Private mobjStrKeyValueRClickOnCodePaneDic As Scripting.Dictionary

Private mblnIsStrKeyValueRClickOnCodePaneDicInitialized As Boolean


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToRClickOnCodePane()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "RClickCtlCodePaneHandler,VBEToolBarCtlHandler,VBEToolBarsSetup"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Add or Delete right-click menu items on Code-panes on VBE
'**---------------------------------------------
'''
''' for example, it is called from Workbook_Open
'''
Public Sub AddTrLocalCodePaneLocalCommandBarButtonsOnlyOnce()

    If Not mblnIsRClickCodePaneMenuAdded Then
    
        AddAllTrLocalCodePaneLocalCommandBarButtons
    
        mblnIsRClickCodePaneMenuAdded = True
    End If
End Sub

'''
''' Reset implementation, because the event-handler objects are often to be released unexpectively.
'''
Public Sub ResetTrLocalCodePaneLocalCommandBarButtons()

    DeleteAllCodePaneRClickMenuItems

    AddAllTrLocalCodePaneLocalCommandBarButtons
    
    mblnIsRClickCodePaneMenuAdded = True
End Sub


Private Sub AddAllTrLocalCodePaneLocalCommandBarButtons()

    AddTrLocalCodePaneLocalCommandBarButton RClickClearImmediateWindow
    
    AddTrLocalCodePaneLocalCommandBarButton RClickOpenOpenCodePaneSwitchMultiLstFormForSingleVBProject
End Sub

Private Sub DeleteAllCodePaneRClickMenuItems()

    msubDeleteCodePaneRClickMenuItemFromDic RClickClearImmediateWindow
    
    msubDeleteCodePaneRClickMenuItemFromDic RClickOpenOpenCodePaneSwitchMultiLstFormForSingleVBProject
End Sub

'**---------------------------------------------
'** execute to start macro programs
'**---------------------------------------------
'''
'''
'''
Public Sub ExecuteByRClickCodePaneMenuItemTrType(ByVal venmRClickCodePaneMenuItemTrType As RClickCodePaneMenuItemTrType)

    Select Case venmRClickCodePaneMenuItemTrType
    
        Case RClickCodePaneMenuItemTrType.RClickClearImmediateWindow
        
            ClearImmediateWindowBySendingControlKeyToVBE
        
        Case RClickCodePaneMenuItemTrType.RClickOpenOpenCodePaneSwitchMultiLstFormForSingleVBProject
    
            On Error Resume Next
            
            Application.Run "OpenCodePaneSwitchMultiListViewsFormForAllVBProjectsInCurrentVBE"
            
            On Error GoTo 0
    End Select
End Sub


'**---------------------------------------------
'** add and delete for right-click CommandBar buttons
'**---------------------------------------------
'''
'''
'''
''' <Argument>venmRClickCodePaneMenuItemTrType: Input</Argument>
Public Sub AddTrLocalCodePaneLocalCommandBarButton(ByVal venmRClickCodePaneMenuItemTrType As RClickCodePaneMenuItemTrType)

    Dim objApplication As Excel.Application ' Object
    
    Dim objRClickCtlCodePaneHandler As RClickCtlCodePaneHandler
    
    Const mstrCommandBarName As String = "Code Window"
    
    
    Set objRClickCtlCodePaneHandler = New RClickCtlCodePaneHandler
    
    Set objApplication = GetCurrentOfficeFileObject().Application
    
    With objApplication.VBE
    
        objRClickCtlCodePaneHandler.AddCommandBarButton venmRClickCodePaneMenuItemTrType, _
                .CommandBars(mstrCommandBarName), _
                1, _
                objApplication
    End With
    
    'objRClickCtlCodePaneHandler.DeleteCommandBarButton

    msubAddCodePaneRClickMenuItemToDic venmRClickCodePaneMenuItemTrType, objRClickCtlCodePaneHandler
End Sub


'''
'''
'''
''' <Argument>venmRClickCodePaneMenuItemTrType: Input</Argument>
''' <Argument>vobjRClickCtlCodePaneHandler: Input</Argument>
Private Sub msubAddCodePaneRClickMenuItemToDic(ByVal venmRClickCodePaneMenuItemTrType As RClickCodePaneMenuItemTrType, _
        ByVal vobjRClickCtlCodePaneHandler As RClickCtlCodePaneHandler)

    If mobjRClickMenuKeyToRClickCtlHandlerDic Is Nothing Then
    
        Set mobjRClickMenuKeyToRClickCtlHandlerDic = New Scripting.Dictionary
    End If
    
    With mobjRClickMenuKeyToRClickCtlHandlerDic
    
        If Not .Exists(venmRClickCodePaneMenuItemTrType) Then
        
            .Add venmRClickCodePaneMenuItemTrType, vobjRClickCtlCodePaneHandler
        End If
    End With
End Sub

'''
'''
'''
''' <Argument>venmRClickCodePaneMenuItemTrType: Input</Argument>
Private Sub msubDeleteCodePaneRClickMenuItemFromDic(ByVal venmRClickCodePaneMenuItemTrType As RClickCodePaneMenuItemTrType)

    Dim objRClickCtlCodePaneHandler As RClickCtlCodePaneHandler
    
    If Not mobjRClickMenuKeyToRClickCtlHandlerDic Is Nothing Then
    
        With mobjRClickMenuKeyToRClickCtlHandlerDic
        
            If .Exists(venmRClickCodePaneMenuItemTrType) Then
            
                Set objRClickCtlCodePaneHandler = .Item(venmRClickCodePaneMenuItemTrType)
                
                objRClickCtlCodePaneHandler.DeleteCommandBarButton
                
                .Remove venmRClickCodePaneMenuItemTrType
                
                Set objRClickCtlCodePaneHandler = Nothing
            End If
        End With
    End If
End Sub

'**---------------------------------------------
'** Get text about RClickCodePaneMenuItemTrType enumeration
'**---------------------------------------------
'''
'''
'''
Public Function GetCaptionTextFromRClickCodePaneMenuItemTrType(ByVal venmRClickCodePaneMenuItemTrType As RClickCodePaneMenuItemTrType) As String

    Dim strCaption As String

    strCaption = ""

    Select Case venmRClickCodePaneMenuItemTrType
    
        Case RClickCodePaneMenuItemTrType.RClickClearImmediateWindow
        
            strCaption = GetTextOfStrKeyRclickMenuClearImmediateWindow()
        
        Case RClickCodePaneMenuItemTrType.RClickOpenOpenCodePaneSwitchMultiLstFormForSingleVBProject
        
            strCaption = GetTextOfStrKeyRclickMenuOpenCodeSwitchForm()
    End Select

    GetCaptionTextFromRClickCodePaneMenuItemTrType = strCaption

End Function



Public Function GetTooltipTextFromRClickCodePaneMenuItemTrType(ByVal venmRClickCodePaneMenuItemTrType As RClickCodePaneMenuItemTrType) As String

    Dim strCaption As String

    strCaption = ""

    Select Case venmRClickCodePaneMenuItemTrType
    
        Case RClickCodePaneMenuItemTrType.RClickClearImmediateWindow
        
            'strCaption = "Clear Immediate Window all texts"
            
            strCaption = GetTextOfStrKeyRclickMenuClearImmediateWindow()
        
        Case RClickCodePaneMenuItemTrType.RClickOpenOpenCodePaneSwitchMultiLstFormForSingleVBProject
        
            'strCaption = "Open form to switch VB Editor code-panes"
    
            strCaption = GetTextOfStrKeyRclickMenuOpenCodeSwitchForm()
    End Select

    GetTooltipTextFromRClickCodePaneMenuItemTrType = strCaption
End Function

'**---------------------------------------------
'** Key-Value cache preparation for RClickOnCodePane
'**---------------------------------------------
'''
''' get string Value from STR_KEY_RCLICK_MENU_CLEAR_IMMEDIATE_WINDOW
'''
''' automatically-added for RClickOnCodePane string-key-value management for standard module and class module
Public Function GetTextOfStrKeyRclickMenuClearImmediateWindow() As String

    If Not mblnIsStrKeyValueRClickOnCodePaneDicInitialized Then

        msubInitializeTextForRClickOnCodePane
    End If

    GetTextOfStrKeyRclickMenuClearImmediateWindow = mobjStrKeyValueRClickOnCodePaneDic.Item("STR_KEY_RCLICK_MENU_CLEAR_IMMEDIATE_WINDOW")
End Function

'''
''' get string Value from STR_KEY_RCLICK_MENU_OPEN_CODE_SWITCH_FORM
'''
''' automatically-added for RClickOnCodePane string-key-value management for standard module and class module
Public Function GetTextOfStrKeyRclickMenuOpenCodeSwitchForm() As String

    If Not mblnIsStrKeyValueRClickOnCodePaneDicInitialized Then

        msubInitializeTextForRClickOnCodePane
    End If

    GetTextOfStrKeyRclickMenuOpenCodeSwitchForm = mobjStrKeyValueRClickOnCodePaneDic.Item("STR_KEY_RCLICK_MENU_OPEN_CODE_SWITCH_FORM")
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** Key-Value cache preparation for RClickOnCodePane
'**---------------------------------------------
'''
''' Initialize Key-Values which values
'''
''' automatically-added for RClickOnCodePane string-key-value management for standard module and class module
Private Sub msubInitializeTextForRClickOnCodePane()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueRClickOnCodePaneDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForRClickOnCodePaneByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForRClickOnCodePaneByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueRClickOnCodePaneDicInitialized = True
    End If

    Set mobjStrKeyValueRClickOnCodePaneDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for RClickOnCodePane key-values cache
'''
''' automatically-added for RClickOnCodePane string-key-value management for standard module and class module
Private Sub AddStringKeyValueForRClickOnCodePaneByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_RCLICK_MENU_CLEAR_IMMEDIATE_WINDOW", "Clear Immediate Window"
        .Add "STR_KEY_RCLICK_MENU_OPEN_CODE_SWITCH_FORM", "Open Code-pane switch form"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for RClickOnCodePane key-values cache
'''
''' automatically-added for RClickOnCodePane string-key-value management for standard module and class module
Private Sub AddStringKeyValueForRClickOnCodePaneByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_RCLICK_MENU_CLEAR_IMMEDIATE_WINDOW", "イミディエイトウィンドウをクリア"
        .Add "STR_KEY_RCLICK_MENU_OPEN_CODE_SWITCH_FORM", "コード切り替えウィンドウを開く"
    End With
End Sub

'''
''' Remove Keys for RClickOnCodePane key-values cache
'''
''' automatically-added for RClickOnCodePane string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueRClickOnCodePane(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_RCLICK_MENU_CLEAR_IMMEDIATE_WINDOW") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_RCLICK_MENU_CLEAR_IMMEDIATE_WINDOW"
            .Remove "STR_KEY_RCLICK_MENU_OPEN_CODE_SWITCH_FORM"
        End If
    End With
End Sub



