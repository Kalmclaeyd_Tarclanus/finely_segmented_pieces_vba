Attribute VB_Name = "CodeVBIDECommon"
'
'   Base common simple utilities for VBIDE ActiveX components
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on VBIDE
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  4/Jan/2023    Kalmclaeyd Tarclanus    Separated from CodeAnalysisVBACommon.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' Extension of VBIDE.vbext_ComponentType
'''
Public Enum GeneralVBComponentType

    GStdModule = VBIDE.vbext_ComponentType.vbext_ct_StdModule
    
    GClassModule = VBIDE.vbext_ComponentType.vbext_ct_ClassModule
    
    GMSForm = VBIDE.vbext_ComponentType.vbext_ct_MSForm
    
    GActiveXDesigner = VBIDE.vbext_ComponentType.vbext_ct_ActiveXDesigner
    
    GVbaObjectModule = VBIDE.vbext_ComponentType.vbext_ct_Document
    
    
    GVbaSheetObjectModule = VBIDE.vbext_ComponentType.vbext_ct_Document + 1
    
    GVbaBookObjectModule = VBIDE.vbext_ComponentType.vbext_ct_Document + 2
End Enum

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjGComponentTypeToLocalizedNameDic As Scripting.Dictionary



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** VBA component internal unique key definitions
'**---------------------------------------------
'''
''' unique VBA module code key
'''
Public Function GetVBAModuleKey(ByRef rstrFileName As String, ByRef rstrModuleName As String) As String

    ' The file name is always unique in a VBIDE process in any Office file application VBA project.
    ' But the VBProject.Name, (or 'VB project name'), are allowed that more than one of same VB project name exists in a single VBA process.
    ' For example, the VBProject.Name of new office file is always the 'VBAProject'.
    ' Therefore, the VBProject.Name is not proper as a unique key.

    GetVBAModuleKey = rstrFileName & ";" & rstrModuleName
End Function

'''
''' unique VBA function name key in a VBA module code
'''
Public Function GetVBAFunctionKey(ByRef rstrFunctionName As String, ByRef renmProcKind As VBIDE.vbext_ProcKind) As String

    GetVBAFunctionKey = rstrFunctionName & ";" & CStr(renmProcKind)
End Function

'''
''' inverse of GetVBAFunctionKey
'''
Public Sub GetVBAProcNameAndProcKindFromVBAFunctionKey(ByRef rstrProcName As String, ByRef renmProcKind As VBIDE.vbext_ProcKind, ByRef rvarVBAFunctionKey As Variant)

    Dim strInfos() As String
    
    strInfos = Split(rvarVBAFunctionKey, ";")
    
    rstrProcName = strInfos(0)
    
    renmProcKind = CInt(strInfos(1))
End Sub


'''
''' simple interface
'''
''' <Argument>rstrOfficeFileName: Output</Argument>
''' <Argument>robjVBComponent: Output</Argument>
''' <Argument>robjOfficeFile: Input</Argument>
''' <Argument>rstrModuleName: Input</Argument>
Public Sub GetFileNameAndVBComponentFromOfficeFileAndModuleName(ByRef rstrOfficeFileName As String, _
        ByRef robjVBComponent As VBIDE.VBComponent, _
        ByRef robjOfficeFile As Object, _
        ByRef rstrModuleName As String)

    With robjOfficeFile
    
        Set robjVBComponent = .VBProject.VBComponents.Item(rstrModuleName)
    
        rstrOfficeFileName = .Name
    End With
End Sub

'''
'''
'''
Public Function GetModuleNameToExtensionDicExceptForDocumentComponent(ByRef robjVBProject As VBIDE.VBProject) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, objComponent As VBIDE.VBComponent
    
    Set objDic = New Scripting.Dictionary
    
    For Each objComponent In robjVBProject.VBComponents
    
        If objComponent.Type <> vbext_ct_Document Then
    
            objDic.Add objComponent.Name, GetExtensionFromVBAComponentType(objComponent.Type)
        End If
    Next
    
    SortDictionaryAboutKeys objDic
    
    Set GetModuleNameToExtensionDicExceptForDocumentComponent = objDic
End Function

'**---------------------------------------------
'** For control Switch-code-panes form
'**---------------------------------------------
'''
'''
'''
Public Function GetComponentTypeToCountOfModulesDic(ByRef robjClassifiedTypeToModuleNameDic As Scripting.Dictionary) As Scripting.Dictionary

    Dim enmExType As GeneralVBComponentType, varExType As Variant, objDic As Scripting.Dictionary
    Dim enmType As VBIDE.vbext_ComponentType
    Dim intCountOfObjectModuleType As Long

    Set objDic = New Scripting.Dictionary

    With robjClassifiedTypeToModuleNameDic
    
        intCountOfObjectModuleType = 0
    
        For Each varExType In .Keys
        
            enmExType = varExType
            
            Select Case enmExType
            
                Case GStdModule
                
                    objDic.Add VBIDE.vbext_ComponentType.vbext_ct_StdModule, .Item(varExType).Count
                
                Case GClassModule
            
                    objDic.Add VBIDE.vbext_ComponentType.vbext_ct_ClassModule, .Item(varExType).Count
                
                Case GMSForm
                
                    objDic.Add VBIDE.vbext_ComponentType.vbext_ct_MSForm, .Item(varExType).Count
                
                Case GActiveXDesigner
                
                    objDic.Add VBIDE.vbext_ComponentType.vbext_ct_ActiveXDesigner, .Item(varExType).Count
                
                Case GVbaObjectModule, GVbaSheetObjectModule, GVbaBookObjectModule
                
                    intCountOfObjectModuleType = intCountOfObjectModuleType + .Item(varExType).Count
            End Select
        Next
        
    End With

    If intCountOfObjectModuleType > 0 Then
        
        objDic.Add VBIDE.vbext_ComponentType.vbext_ct_Document, intCountOfObjectModuleType
    End If

    Set GetComponentTypeToCountOfModulesDic = objDic
End Function


'**---------------------------------------------
'** VB component detailed type name
'**---------------------------------------------
Public Function GetGeneralComponentTypeToLocalizedNameDic() As Scripting.Dictionary

    If mobjGComponentTypeToLocalizedNameDic Is Nothing Then
    
        Set mobjGComponentTypeToLocalizedNameDic = New Scripting.Dictionary
        
        With mobjGComponentTypeToLocalizedNameDic
        
            .Add GeneralVBComponentType.GStdModule, "Standard"
        
            .Add GeneralVBComponentType.GClassModule, "Class"
        
            .Add GeneralVBComponentType.GMSForm, "UserForm"
            
            .Add GeneralVBComponentType.GActiveXDesigner, "ActiveXDesigner"
            
            .Add GeneralVBComponentType.GVbaObjectModule, "ObjectModule"
            
            .Add GeneralVBComponentType.GVbaSheetObjectModule, "SheetObjectModule"
            
            .Add GeneralVBComponentType.GVbaBookObjectModule, "BookObjectModule"
        End With
    End If
    
    Set GetGeneralComponentTypeToLocalizedNameDic = mobjGComponentTypeToLocalizedNameDic
End Function


'**---------------------------------------------
'** VBIDE utitlities, which are common in Excel, Access, Word, PowerPoint, and Outlook
'**---------------------------------------------
'''
''' get extension name with dot
'''
Public Function GetDotExtensionFromVBAComponentType(ByVal venmComponentType As VBIDE.vbext_ComponentType) As String
    
    Dim strExtension As String

    strExtension = ""

    Select Case venmComponentType
    
        Case vbext_ct_StdModule     ' 1
        
            strExtension = ".bas"
            
        Case vbext_ct_ClassModule   ' 2
        
            strExtension = ".cls"
            
        Case vbext_ct_MSForm        ' 3
        
            strExtension = ".frm"
            
        Case vbext_ct_ActiveXDesigner   ' 11
        
            strExtension = ".cls"
            
        Case vbext_ct_Document      ' 100
        
            strExtension = ".cls"   ' Worksheet object class module, Workbook object class module
    End Select

    GetDotExtensionFromVBAComponentType = strExtension
End Function

'''
''' get extension name
'''
Public Function GetExtensionFromVBAComponentType(ByVal venmComponentType As VBIDE.vbext_ComponentType) As String
    
    Dim strExtension As String

    strExtension = ""

    Select Case venmComponentType
    
        Case vbext_ct_StdModule
        
            strExtension = "bas"
            
        Case vbext_ct_ClassModule
        
            strExtension = "cls"
            
        Case vbext_ct_MSForm
        
            strExtension = "frm"
            
        Case vbext_ct_ActiveXDesigner
        
            strExtension = "cls"
            
        Case vbext_ct_Document
        
            strExtension = "cls"   ' Worksheet object class module, Workbook object class module
    End Select

    GetExtensionFromVBAComponentType = strExtension
End Function


'''
'''
'''
Public Function GetVBComponentTypeClassifiedName(ByRef rstrModuleName As String, ByRef robjVBProject As VBIDE.VBProject) As String

    Dim objExTypeToNameDic As Scripting.Dictionary
    
    Set objExTypeToNameDic = GetGeneralComponentTypeToLocalizedNameDic()
    
    With GetGeneralComponentTypeToLocalizedNameDic()
    
        GetVBComponentTypeClassifiedName = objExTypeToNameDic.Item(GetVBComponentExType(rstrModuleName, robjVBProject))
    End With
End Function

'''
'''
'''
Public Function GetVBComponentExType(ByRef rstrModuleName As String, ByRef robjVBProject As VBIDE.VBProject) As GeneralVBComponentType

    Dim enmComponentType As VBIDE.vbext_ComponentType
    Dim objVBComponent As VBIDE.VBComponent, objExTypeToNameDic As Scripting.Dictionary
    
    Dim enmGeneralVBComponentType As GeneralVBComponentType
    
    
    On Error Resume Next
    
    Set objVBComponent = robjVBProject.VBComponents.Item(rstrModuleName)
    
    On Error GoTo 0

    If Not objVBComponent Is Nothing Then
    
        enmComponentType = objVBComponent.Type
        
        Select Case enmComponentType
        
            Case vbext_ct_StdModule     ' 1
            
                enmGeneralVBComponentType = GeneralVBComponentType.GStdModule
                
            Case vbext_ct_ClassModule   ' 2
            
                enmGeneralVBComponentType = GeneralVBComponentType.GClassModule
                
            Case vbext_ct_MSForm        ' 3
            
                enmGeneralVBComponentType = GeneralVBComponentType.GMSForm
                
            Case vbext_ct_ActiveXDesigner   ' 11
            
                enmGeneralVBComponentType = GeneralVBComponentType.GActiveXDesigner
                
            Case vbext_ct_Document      ' 100
            
                If InStr(1, LCase(rstrModuleName), "sheet") > 0 Then
                
                    enmGeneralVBComponentType = GeneralVBComponentType.GVbaSheetObjectModule
                    
                ElseIf InStr(1, LCase(rstrModuleName), "book") > 0 Then
                
                    enmGeneralVBComponentType = GeneralVBComponentType.GVbaBookObjectModule
                Else
            
                    enmGeneralVBComponentType = GeneralVBComponentType.GVbaObjectModule
                End If
        End Select
    End If
    
    GetVBComponentExType = enmGeneralVBComponentType
End Function


'''
'''
'''
Public Function GetKindFullNameOfVBAProcedure(ByVal venmProcKind As VBIDE.vbext_ProcKind) As String
    
    Dim strText As String
    
    Select Case venmProcKind
    
        Case VBIDE.vbext_ProcKind.vbext_pk_Proc
        
            strText = "Procedure"
            
        Case VBIDE.vbext_ProcKind.vbext_pk_Get
        
            strText = "Property Get"
            
        Case VBIDE.vbext_ProcKind.vbext_pk_Let
        
            strText = "Property Let"
            
        Case VBIDE.vbext_ProcKind.vbext_pk_Set
        
            strText = "Property Set"
            
        Case Else
        
            strText = ""
    End Select

    GetKindFullNameOfVBAProcedure = strText
End Function


'''
''' get a dictionary object key from ProcKind enumeration type
'''
Public Function GetVBAProcedureKindSuffixKey(ByVal venmProcKind As VBIDE.vbext_ProcKind) As String
    
    Dim strSuffix As String
    
    Select Case venmProcKind
        
        Case VBIDE.vbext_ProcKind.vbext_pk_Proc
        
            strSuffix = ""
            
        Case VBIDE.vbext_ProcKind.vbext_pk_Get
        
            strSuffix = "_Get"
            
        Case VBIDE.vbext_ProcKind.vbext_pk_Let
        
            strSuffix = "_Let"
            
        Case VBIDE.vbext_ProcKind.vbext_pk_Set
        
            strSuffix = "_Set"
            
        Case Else
        
            strSuffix = ""
    End Select

    GetVBAProcedureKindSuffixKey = strSuffix
End Function


'**---------------------------------------------
'** solute VBA module file name strings simply
'**---------------------------------------------
'''
'''
'''
Public Function GetVbaModuleFileNamesStringDelimitedComma(ByVal vobjVBProject As VBIDE.VBProject, ByVal vobjModuleNames As Collection) As String

    Dim strLine As String, varName As Variant, strName As String, objVBComponent As VBIDE.VBComponent
    Dim i As Long, strFileName As String
    
    strLine = ""
    
    If Not vobjModuleNames Is Nothing Then
    
        If vobjModuleNames.Count > 0 Then
        
            i = 1
        
            For Each varName In vobjModuleNames
    
                strName = varName
                
                Set objVBComponent = Nothing
                
                Set objVBComponent = vobjVBProject.VBComponents.Item(strName)
    
                If Not objVBComponent Is Nothing Then
                
                    strFileName = strName & GetDotExtensionFromVBAComponentType(objVBComponent.Type)
                
                    If i < vobjModuleNames.Count Then
                    
                        strLine = strLine & strFileName & ", "
                    Else
                        strLine = strLine & strFileName
                    End If
                End If
    
                i = i + 1
            Next
        End If
    End If
    
    GetVbaModuleFileNamesStringDelimitedComma = strLine
End Function

'''
'''
'''
Public Function GetVbaModuleFileNamesStringDelimitedCommaWithLeftTagDelimitingColon(ByVal vobjVBProject As VBIDE.VBProject, ByVal vobjModuleNames As Collection) As String

    Dim strLine As String
    
    strLine = ""
    
    If Not vobjModuleNames Is Nothing Then
    
        If vobjModuleNames.Count > 0 Then
        
            If vobjModuleNames.Count = 1 Then
                
                strLine = "Module file name: "
            Else
                strLine = "Module file names: "
            End If
        
            strLine = strLine & GetVbaModuleFileNamesStringDelimitedComma(vobjVBProject, vobjModuleNames)
        End If
    End If

    GetVbaModuleFileNamesStringDelimitedCommaWithLeftTagDelimitingColon = strLine
End Function



