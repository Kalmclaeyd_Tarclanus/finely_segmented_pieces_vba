Attribute VB_Name = "CurrentPrinterUtility"
'
'   Collect this computer registered printers
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on either Windows OS or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 20/May/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = False

#Const HAS_ACCESS_REF = False


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToLoadRegisteredPrinterInfoFromWMI()

    Dim objCol As Collection
    
    LoadThisComputerRegisteredPrinterInfoFromWMI objCol
    
    DebugCol objCol
End Sub

'''
'''
'''
Private Sub msubSanityTestToLoadRegisteredPrinterInfoFromMSAccess()

    Dim objCol As Collection
    
    LoadThisComputerRegisteredPrinterInfoFromMSAccess objCol
    
    DebugCol objCol
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub LoadThisComputerRegisteredPrinterInfoFromWMI(ByRef robjDTCol As Collection)

    Dim objRowCol As Collection

#If HAS_REF Then

    Dim objLocator As WbemScripting.SWbemLocator, objService As WbemScripting.SWbemServicesEx, objClassSet As WbemScripting.SWbemObjectSet, objItem As SWbemObjectEx
    
    Set objLocator = New WbemScripting.SWbemLocator
#Else
    Dim objLocator As Object, objService As Object, objClassSet As Object, objItem As Object
    
    ' From WMI object, connect to the local
    Set objLocator = CreateObject("WbemScripting.SWbemLocator")
#End If


    If robjDTCol Is Nothing Then Set robjDTCol = New Collection

    Set objService = objLocator.ConnectServer

    ' query by WQL
    Set objClassSet = objService.ExecQuery("Select * From Win32_Printer")

    For Each objItem In objClassSet
    
        Set objRowCol = New Collection
    
        With objItem
        
            objRowCol.Add .Caption
        
            objRowCol.Add .DriverName
            
            objRowCol.Add .PortName
            
            objRowCol.Add .Default
        End With
        
        robjDTCol.Add objRowCol
    Next
End Sub

'''
''' Need for Microsoft Access application installation
'''
Public Sub LoadThisComputerRegisteredPrinterInfoFromMSAccess(ByRef robjDTCol As Collection)

    Dim strCount As String, strMsg As String, objRowCol As Collection
    
#If HAS_ACCESS_REF Then

    Dim objApplication As Access.Application, objPrinter As Access.Printer
     
    Set objApplication = New Access.Application
#Else
    Dim objApplication As Object, objPrinter As Object
     
    Set objApplication = CreateObject("Access.Application")
#End If

    If IsTheSpecifiedOfficeApplicationInstalledInThisComputer("Access") Then

        If robjDTCol Is Nothing Then Set robjDTCol = New Collection
    
        With objApplication
        
            If .Printers.Count > 0 Then
            
                ' Enumerate printer system properties.
                
                For Each objPrinter In .Printers
                
                    Set objRowCol = New Collection
                
                    With objPrinter
                        
                        objRowCol.Add .DeviceName
                        
                        objRowCol.Add .DriverName
                        
                        objRowCol.Add .Port
                    End With
                    
                    robjDTCol.Add objRowCol
                Next
            Else
                MsgBox "No printers are installed.", vbOKOnly Or vbCritical
            End If
        End With
    Else
        MsgBox "Microsoft Access isn't installed yet." & vbNewLine & "Installed printers are unknown." & vbNewLine & vbNewLine & "You should use a WMI method", vbOKOnly Or vbCritical
    End If
End Sub



