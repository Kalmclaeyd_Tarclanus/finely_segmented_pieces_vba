Attribute VB_Name = "CurrentUserDrivesUtility"
'
'   Collect this computer disk drives information
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       Dependent on Scripting library
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 24/May/2022    Kalmclaeyd Tarclanus    Improved, for example, added IsDriveOfPathStringHasSomeError()
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Conversion for drive enumerations
'**---------------------------------------------
'''
'''
'''
Public Function GetDriveTypeNameFromDriveTypeConst(ByVal venmDriveTypeConst As Scripting.DriveTypeConst) As String
    
    Dim strName As String
    
    Select Case venmDriveTypeConst
    
        Case 0  ' Scripting.DriveTypeConst.Unknown     ' 0
            strName = "Unknown"
            
        Case 1  ' Scripting.DriveTypeConst.Removable   ' 1
            strName = "Removable-Disk"
            
        Case 2  ' Scripting.DriveTypeConst.Fixed       ' 2
            strName = "Local-Hard-Drive"    ' Fixed
            
        Case 3  ' Scripting.DriveTypeConst.Remote      ' 3
            strName = "Network-Drive"
            
        Case 4  ' Scripting.DriveTypeConst.CDRom       ' 4
            strName = "CD-ROM"
            
        Case 5  ' Scripting.DriveTypeConst.RamDisk     ' 5
            strName = "RAM-Disk"
    
    End Select

    GetDriveTypeNameFromDriveTypeConst = strName
End Function

'**---------------------------------------------
'** Search either dirctory or file path based on connected current file-system drives
'**---------------------------------------------
'''
'''
'''
Public Function GetDirectoryPathBySpecifingDriveVolumeName(ByVal venmDriveType As Scripting.DriveTypeConst, ByVal vstrVolumeName As String, ByVal vstrSubRightPath As String) As String

    Dim strDrivePath As String, strDir As String
    
    strDir = ""
    
    strDrivePath = FindWindowsDrivePath(venmDriveType, vstrVolumeName)

    If strDrivePath <> "" Then
    
        strDir = strDrivePath & vstrSubRightPath
    Else
        MsgBox mfstrGetSpecifiedVolumeNameDriveNotFoundMessage(venmDriveType, vstrVolumeName), vbExclamation Or vbOKOnly, "Disk volume [" & vstrVolumeName & "] is not found"
    End If

    GetDirectoryPathBySpecifingDriveVolumeName = strDir
End Function


'''
''' search the active local drives with assigning the sub-path and write it to Registry
'''
''' for example, search an already installed program file such as WinMergeU.exe
'''
Public Function SearchSpecifiedFileFromSpecifiedSubPathsAndSetRegistryIfFound(ByVal vstrModuleNameAndSubjectKey As String, ByVal vobjSubPaths As Collection) As String
    
    Dim strTargetPath As String, varDriveLetter As Variant, strDriveLetter As String
    Dim varSubPath As Variant, strSubPath As String, blnFound As Boolean
    
    strTargetPath = ""
    
    With New Scripting.FileSystemObject
    
        blnFound = False
        
        For Each varSubPath In vobjSubPaths
        
            strSubPath = varSubPath
            
            For Each varDriveLetter In GetLocalHardDriveLettersInThisPC()
            
                strDriveLetter = varDriveLetter
        
                strTargetPath = strDriveLetter & strSubPath
                
                If .FileExists(strTargetPath) Then
                
                     blnFound = True
                     
                     Exit For
                End If
            Next
        
            If blnFound Then Exit For
        Next
        
        If blnFound Then
        
            SetUNCPathToCurUserReg vstrModuleNameAndSubjectKey, strTargetPath
        Else
            strTargetPath = ""
        End If
    End With

    SearchSpecifiedFileFromSpecifiedSubPathsAndSetRegistryIfFound = strTargetPath
End Function


'''
''' search the active local hard-drives by VBA.Dir and write it to Registry
'''
''' for example, search an already installed program file such as sqlite3.exe
'''
Public Function SearchSpecifiedFileFromCurrentHardDrivesAndSetRegistryIfFound(ByVal vstrModuleNameAndSubjectKey As String, _
        ByVal vstrFileName As String, _
        Optional ByVal vstrAvoidingPathStringPartDelimitedByComma As String = "", _
        Optional ByVal vintMinimumHierarchicalOrderToSearch As Long = 0, _
        Optional ByVal vintMaximumHierarchicalOrderToSearch As Long = 2) As String
    
    Dim strTargetPath As String, objAvoidingPathParts As Collection, varAvoidingPart As Variant, blnFound As Boolean
    Dim objFoundPaths As Collection, blnIsFilteringNeed As Boolean, varPath As Variant
    
    
    blnFound = False
    
    strTargetPath = ""
    
    Set objFoundPaths = GetFilePathsForAllHarddrivesUsingVbaDir(vstrFileName, vintMinimumHierarchicalOrderToSearch, vintMaximumHierarchicalOrderToSearch)
    
    If objFoundPaths.Count > 0 Then
    
        If objFoundPaths.Count = 1 Then
        
            strTargetPath = objFoundPaths.Item(1)
            
            blnFound = True
        Else
            If vstrAvoidingPathStringPartDelimitedByComma <> "" Then
            
                Set objAvoidingPathParts = GetColFromLineDelimitedChar(vstrAvoidingPathStringPartDelimitedByComma)
            
                blnIsFilteringNeed = True
            Else
                blnIsFilteringNeed = False
            End If
        
            For Each varPath In objFoundPaths
            
                If Not blnIsFilteringNeed Then
                
                    strTargetPath = varPath
            
                    blnFound = True
                    
                    Exit For
                Else
                    blnFound = True
                
                    For Each varAvoidingPart In objAvoidingPathParts
                
                        If InStr(1, varPath, varAvoidingPart) > 0 Then
                        
                            blnFound = False
                            
                            Exit For
                        End If
                    Next
                    
                    If blnFound Then
                    
                        strTargetPath = varPath
                    
                        Exit For
                    End If
                End If
            Next
            
            If strTargetPath = "" Then
            
                strTargetPath = objFoundPaths.Item(1)
                
                blnFound = True
            End If
        End If
    End If
    
    If blnFound Then
    
        SetUNCPathToCurUserReg vstrModuleNameAndSubjectKey, strTargetPath
    Else
        strTargetPath = ""
    End If

    SearchSpecifiedFileFromCurrentHardDrivesAndSetRegistryIfFound = strTargetPath
End Function



'**---------------------------------------------
'** Check this computer drive ready
'**---------------------------------------------
'''
'''
'''
Public Sub CheckDriveExistence(ByVal vstrPath As String, Optional ByVal vblnShowMessageBox As Boolean = True)

    Dim objDrive As Scripting.Drive
    
    With New Scripting.FileSystemObject
    
        On Error Resume Next
        
        Set objDrive = .GetDrive(.GetDriveName(vstrPath))
    
        On Error GoTo 0
    
        If Not objDrive Is Nothing Then
        
            If Not objDrive.IsReady Then
            
                If vblnShowMessageBox Then MsgBox "The Drive " & .GetDriveName(vstrPath) & ": is not ready.", vbCritical Or vbOKOnly, "This computer Drive existence check"
            End If
        Else
            If vblnShowMessageBox Then MsgBox "The Drive " & .GetDriveName(vstrPath) & ": is not found.", vbCritical Or vbOKOnly, "This computer Drive existence check"
        End If
    End With
End Sub


'''
'''
'''
Public Function IsDriveReady(ByVal vstrPath As String, Optional ByVal vblnShowMessageBox As Boolean = True) As Boolean

    Dim objDrive As Scripting.Drive, blnIsReady As Boolean

    blnIsReady = False

    Set objDrive = Nothing

    With New Scripting.FileSystemObject
    
        On Error Resume Next
        
        Set objDrive = .GetDrive(.GetDriveName(vstrPath))
        
        On Error GoTo 0
    
        If Not objDrive Is Nothing Then
        
            With objDrive
            
                If .IsReady Then
                
                    blnIsReady = True
                Else
                    If vblnShowMessageBox Then
                    
                        MsgBox GetDriveIsNotReadyMessage(objDrive), vbOKOnly Or vbExclamation
                    End If
                End If
            End With
        Else
            MsgBox "Drive " & .GetDriveName(vstrPath) & " is not exist. (Path: " & vstrPath & ")", vbOKOnly Or vbExclamation
        End If
    End With

    IsDriveReady = blnIsReady
End Function

'**---------------------------------------------
'** Check ready for plural drives
'**---------------------------------------------
'''
'''
'''
Public Function AreBothDrivesReady(ByVal vstrSourcePathPart As String, ByVal vstrDestinationPathPart As String, Optional ByVal vblnShowWarningMessageBox As Boolean = True) As Boolean

    AreBothDrivesReady = AreAllDrivesReady(GetColFromLineDelimitedChar(vstrSourcePathPart & "," & vstrDestinationPathPart), vblnShowWarningMessageBox)
End Function

'''
'''
'''
Public Function AreAllDrivesReady(ByVal vobjPathCol As Collection, Optional ByVal vblnShowWarningMessageBox As Boolean = True) As Boolean

    Dim varPath As Variant, strPath As String, blnAreAllReady As Boolean
    
    blnAreAllReady = True
    
    For Each varPath In vobjPathCol
    
        strPath = varPath
    
        If Not IsDriveReady(strPath, vblnShowWarningMessageBox) Then
        
            blnAreAllReady = False
        End If
    
        If Not blnAreAllReady Then
        
            Exit For
        End If
    Next
    
    AreAllDrivesReady = blnAreAllReady
End Function


'''
'''
'''
Private Function GetDriveIsNotReadyMessage(ByVal vobjDrive As Scripting.Drive) As String

    Dim strMessage As String
    
    strMessage = ""
    
    With vobjDrive
    
        strMessage = "Drive " & .DriveLetter & ": is NOT ready." & vbNewLine & "This drive type is " & GetDriveTypeNameFromDriveTypeConst(.DriveType)
    
        Select Case .DriveType
        
            Case 3  ' Scripting.DriveTypeConst.Remote      ' 3 Network
            
                strMessage = strMessage & vbNewLine & .ShareName
            
            Case 2, 1  ' Scripting.DriveTypeConst.Fixed       ' 2 Local-Hard drive 1 Removable
            
                strMessage = strMessage & vbNewLine & "Share name is " & .VolumeName
        End Select
    End With

    GetDriveIsNotReadyMessage = strMessage
End Function


'**---------------------------------------------
'** About check file-system drive state
'**---------------------------------------------
'''
'''
'''
Public Function IsDriveOfPathStringHasSomeError(ByRef rstrFilePath As String, _
        ByRef robjFS As Scripting.FileSystemObject, _
        Optional ByVal vblnAllowToShowMsgBoxOfError As Boolean = True) As Boolean

    Dim blnIsDriveHasSomeError As Boolean, strLog As String

    blnIsDriveHasSomeError = False
    
    With robjFS   ' check drive enabled state, for example, it may be BitLocker protected
    
        If .DriveExists(.GetDriveName(rstrFilePath)) Then
    
            With .GetDrive(.GetDriveName(rstrFilePath))
            
                If Not .IsReady Then
                
                    If vblnAllowToShowMsgBoxOfError Then
                    
                        strLog = "The " & robjFS.GetDriveName(rstrFilePath) & " drive is not enabled, but it exists." & vbNewLine & "Drive type: " & GetDriveTypeNameFromDriveTypeConst(.DriveType)
                    
                        msubAddDriveAdditionalInformationWhenItIsNotReady strLog, .DriveType
                    
                        strLog = strLog & vbNewLine & vbNewLine & "The " & robjFS.GetFileName(rstrFilePath) & " file can not be opened."
                    
                        MsgBox strLog, vbExclamation Or vbOKOnly
                    End If
                    
                    blnIsDriveHasSomeError = True
                End If
            End With
        Else
        
            If vblnAllowToShowMsgBoxOfError Then
            
                MsgBox "The " & robjFS.GetDriveName(rstrFilePath) & " drive doesn't exist.", vbExclamation Or vbOKOnly
            End If
            
            blnIsDriveHasSomeError = True
        End If
    End With
    
    IsDriveOfPathStringHasSomeError = blnIsDriveHasSomeError
End Function

'''
'''
'''
Public Function IsAtLeastOneRemovableDiskConnected() As Boolean

    Dim objCol As Collection, blnIsConnected As Boolean
    
    Set objCol = GetSpecifiedDriveLettersInThisPC(Removable)
    
    If objCol Is Nothing Then
    
        blnIsConnected = False
    Else
        blnIsConnected = True
    End If

    IsAtLeastOneRemovableDiskConnected = blnIsConnected
End Function


'**---------------------------------------------
'** About drive letter
'**---------------------------------------------
'''
'''
'''
Public Function FindWindowsDrivePath(ByVal venmDriveTypeConst As Scripting.DriveTypeConst, ByVal vstrVolumeName As String) As String

    Dim strPath As String, objDrive As Scripting.Drive, blnContinueToProcess As Boolean
    
    strPath = ""
    
    With New Scripting.FileSystemObject
    
        For Each objDrive In .Drives
        
            With objDrive
            
                If .IsReady Then
                
                    If .DriveType = venmDriveTypeConst Then
                    
                        If InStr(1, UCase(.VolumeName), UCase(vstrVolumeName)) > 0 Then
                        
                            strPath = .Path
                    
                            Exit For
                        End If
                    End If
                End If
            End With
        Next
    End With

    FindWindowsDrivePath = strPath
End Function

'''
'''
'''
Public Function GetDrivePathFromVolumeName(ByVal vstrVolumeName As String) As String

    Dim strPath As String, objDrive As Scripting.Drive
    
    strPath = ""
    
    With New Scripting.FileSystemObject
    
        For Each objDrive In .Drives
    
            With objDrive
            
                If .IsReady Then
                
                    If StrComp(LCase(.VolumeName), LCase(vstrVolumeName)) = 0 Then
                    
                        strPath = .Path
                    End If
                End If
            End With
        Next
    End With

    GetDrivePathFromVolumeName = strPath
End Function

'''
''' get this system drive letter
'''
Public Function GetThisLocalWindowsSystemDriveLetter() As String

    With New Scripting.FileSystemObject
    
        GetThisLocalWindowsSystemDriveLetter = Left(.GetSpecialFolder(SystemFolder), 1)
    End With
End Function


'''
'''
'''
Public Function GetLocalHardDriveLettersInThisPC() As Collection
    
    Set GetLocalHardDriveLettersInThisPC = GetSpecifiedDriveLettersInThisPC(Fixed)
End Function

'''
''' get such as USB drive, SD card drive
'''
Public Function GetLocalRemovableDriveLettersInThisPC() As Collection
    
    Set GetLocalRemovableDriveLettersInThisPC = GetSpecifiedDriveLettersInThisPC(Removable)
End Function

'**---------------------------------------------
'** Find enabled drives based on file-system drive size
'**---------------------------------------------
'''
'''
'''
Public Function GetMaxSizeLocalHardDriveLetterPath() As String
    
    Dim objDrive As Scripting.Drive, i As Long
    Dim dblMaxTotalSize As Double, strMaxTotalSizeDriveLetter As String
    
    Const intGB As Long = 1073741824 ' is equals to 1024 * 1024 * 1024
    
    i = 1
    strMaxTotalSizeDriveLetter = ""
    
    With New Scripting.FileSystemObject
    
        For Each objDrive In .Drives
        
            With objDrive
            
                If .IsReady And .DriveType = Fixed Then
                    
                    If i = 1 Then
                    
                        dblMaxTotalSize = CDbl(.TotalSize) / intGB
                        
                        strMaxTotalSizeDriveLetter = .DriveLetter
                    Else
                        If dblMaxTotalSize < (CDbl(.TotalSize) / intGB) Then
                        
                            dblMaxTotalSize = CDbl(.TotalSize) / intGB
                            
                            strMaxTotalSizeDriveLetter = .DriveLetter
                        End If
                    End If
                    
                    i = i + 1
                End If
            End With
        Next
    End With

    GetMaxSizeLocalHardDriveLetterPath = strMaxTotalSizeDriveLetter
End Function

'''
'''
'''
Public Function GetWorkingLocalHardDriveLetterPriorToExcludingSystemDrive() As String

    GetWorkingLocalHardDriveLetterPriorToExcludingSystemDrive = GetWorkingLocalHardDriveLetterPriorToExcludingSystemDriveAndOtherConditions(Nothing, Nothing)
End Function

'''
''' serch maximum size local hard drive without the system-drive. If there is the only system-drive, return the system-drive letter
'''
Public Function GetWorkingLocalHardDriveLetterPriorToExcludingSystemDriveAndOtherConditions(Optional ByVal vobjExcludeSearchingHardDriveVolumeNameDic As Scripting.Dictionary = Nothing, _
        Optional ByVal vobjExcludeSearchingHardDriveLetterDic As Scripting.Dictionary = Nothing) As String

    Dim strMaxTotalSizeDriveLetter As String, dblMaxTotalSize As Double
    
    Dim strThisSystemDriveLetter As String, intFoundLocalHardDrive As Long, i As Long
    
    Dim objDrive As Scripting.Drive, blnContinue As Boolean
    
    Dim strReturnDriveLetter As String
    
    ' In ordinary computers, the system drive is the 'C' drive
    
    strThisSystemDriveLetter = GetThisLocalWindowsSystemDriveLetter()


    Const intGB As Long = 1073741824 ' is equals to 1024 * 1024 * 1024

    intFoundLocalHardDrive = 0
    
    strReturnDriveLetter = ""
    
    i = 1
    
    strMaxTotalSizeDriveLetter = ""
    
    With New Scripting.FileSystemObject
    
        For Each objDrive In .Drives
        
            With objDrive
            
                If .IsReady Then
                
                    If .DriveType = Fixed And CDbl(.TotalSize) / intGB > 5# Then
                    
                        If strThisSystemDriveLetter <> .DriveLetter Then
                            
                            blnContinue = True
                            
                            If Not vobjExcludeSearchingHardDriveVolumeNameDic Is Nothing Then
                            
                                If vobjExcludeSearchingHardDriveVolumeNameDic.Exists(.VolumeName) Then
                                
                                    blnContinue = False
                                End If
                            End If
                            
                            If Not vobjExcludeSearchingHardDriveLetterDic Is Nothing Then
                            
                                If vobjExcludeSearchingHardDriveLetterDic.Exists(.DriveLetter) Then
                                
                                    blnContinue = False
                                End If
                            End If
                            
                            
                            If blnContinue Then
                            
                                If i = 1 Then
                                
                                    dblMaxTotalSize = CDbl(.TotalSize) / intGB
                                    
                                    strMaxTotalSizeDriveLetter = .DriveLetter
                                Else
                                    If dblMaxTotalSize < (CDbl(.TotalSize) / intGB) Then
                                    
                                        dblMaxTotalSize = CDbl(.TotalSize) / intGB
                                        
                                        strMaxTotalSizeDriveLetter = .DriveLetter
                                    End If
                                End If
                                
                                i = i + 1
                                
                                intFoundLocalHardDrive = intFoundLocalHardDrive + 1
                            End If
                        End If
                    End If
                End If
            End With
        Next
    End With
    
    If intFoundLocalHardDrive > 0 Then
    
        strReturnDriveLetter = strMaxTotalSizeDriveLetter
    Else
        strReturnDriveLetter = strThisSystemDriveLetter
    End If
    
    GetWorkingLocalHardDriveLetterPriorToExcludingSystemDriveAndOtherConditions = strReturnDriveLetter
End Function

'**---------------------------------------------
'** Get drive information
'**---------------------------------------------
'''
'''
'''
Public Function GetThisComputerReadyDiskDrivesInfoDTCol() As Collection
    
    Dim objDrive As Scripting.Drive, objCol As Collection, objRowCol As Collection

    Const intGB As Long = 1073741824 ' is 1024 * 1024 * 1024

    Set objCol = New Collection
    
    With New Scripting.FileSystemObject
    
        For Each objDrive In .Drives
            
            With objDrive
                
                If .IsReady Then
                
                    Set objRowCol = New Collection
                
                    objRowCol.Add .Path  ' .DriveLetter
                    
                    objRowCol.Add GetDriveTypeNameFromDriveTypeConst(.DriveType)
                    
                    objRowCol.Add .FileSystem
                    
                    
                    objRowCol.Add CDbl(.TotalSize) / intGB
                    
                    objRowCol.Add CDbl(.AvailableSpace) / intGB
                    
                    objRowCol.Add CDbl(.FreeSpace) / intGB
                    
                    
                    objRowCol.Add .VolumeName
                    
                    objRowCol.Add .ShareName
                    
                    
                    objCol.Add objRowCol
                End If
            End With
        Next
    End With

    Set GetThisComputerReadyDiskDrivesInfoDTCol = objCol
End Function

'''
'''
'''
Public Function GetThisComputerDiskDrivesInfoDTCol() As Collection
    
    Dim objDrive As Scripting.Drive, objCol As Collection, objRowCol As Collection

    Const intGB As Long = 1073741824 ' is 1024 * 1024 * 1024

    Set objCol = New Collection
    
    With New Scripting.FileSystemObject
    
        For Each objDrive In .Drives
            
            With objDrive
                
                Set objRowCol = New Collection
                
                objRowCol.Add .Path  ' .DriveLetter
                
                objRowCol.Add .IsReady
                
                objRowCol.Add GetDriveTypeNameFromDriveTypeConst(.DriveType)
                
                If .IsReady Then
                
                    objRowCol.Add .FileSystem
                
                    objRowCol.Add CDbl(.TotalSize) / intGB
                    
                    objRowCol.Add CDbl(.AvailableSpace) / intGB
                    
                    objRowCol.Add CDbl(.FreeSpace) / intGB
                    
                    objRowCol.Add .VolumeName
                Else
                
                    objRowCol.Add Empty
                
                    objRowCol.Add Empty: objRowCol.Add Empty: objRowCol.Add Empty
                    
                    objRowCol.Add Empty
                End If
                
                objRowCol.Add .ShareName
                
                objCol.Add objRowCol
            End With
        Next
    End With

    Set GetThisComputerDiskDrivesInfoDTCol = objCol
End Function

'''
'''
'''
Public Function GetThisComputerDiskDrivesSearchLog(ByVal vobjDataTableCol As Collection, ByRef robjDecorationTagValueSettings As Collection, ByRef robjDecorationTargetTextSettings As Collection) As String
    
    Dim intRowsCount As Long, intColumnsCount As Long, strLoggedTimeStamp As String, strSearchLog As String
    Dim objNetwork As WshNetwork
    
    Set objNetwork = New WshNetwork

    
    strLoggedTimeStamp = Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
    
    strSearchLog = "This computer drives information" & vbNewLine
    
    strSearchLog = strSearchLog & "Logged time: " & strLoggedTimeStamp & vbNewLine
    
    strSearchLog = strSearchLog & "Count of enabled drives: " & CStr(vobjDataTableCol.Count) & vbNewLine
    
    strSearchLog = strSearchLog & "This system drive letter: " & GetThisLocalWindowsSystemDriveLetter() & ":" & vbNewLine
    
    With objNetwork
    
        strSearchLog = strSearchLog & "Computer name: " & .ComputerName & vbNewLine
        
        strSearchLog = strSearchLog & "User name: " & .UserName & vbNewLine
        
        strSearchLog = strSearchLog & "Domain name: " & .UserDomain
    End With
    
    msubSetupShapeTextDecorationSettingOfDrivesInfo robjDecorationTagValueSettings, robjDecorationTargetTextSettings
    
    GetThisComputerDiskDrivesSearchLog = strSearchLog
End Function

'''
'''
'''
Private Sub msubSetupShapeTextDecorationSettingOfDrivesInfo(ByRef robjDecorationTagValueSettings As Collection, ByRef robjDecorationTargetTextSettings As Collection)

    Set robjDecorationTagValueSettings = New Collection
    
    With robjDecorationTagValueSettings
    
        .Add "Logged time:;" & vbNewLine & ";11;trRGBWhiteFontColor; "
        
        .Add "Count of enabled drives:;" & vbNewLine & ";13;trRGBWhiteFontColor; "
    
        .Add "This system drive letter:;" & vbNewLine & ";13;trRGBPaleGreenFontColor; "
    
        .Add "Computer name:;" & vbNewLine & ";11;trRGBWhiteFontColor; "
        
        .Add "User name:;" & vbNewLine & ";11;trRGBWhiteFontColor; "
        
        .Add "Domain name:;" & vbNewLine & ";11;trRGBWhiteFontColor; "
    End With
    
    Set robjDecorationTargetTextSettings = New Collection
    
    With robjDecorationTargetTextSettings
    
        .Add "This computer drives information;15;trRGBPaleYellowFontColor"
    End With
End Sub


'''
'''
'''
Public Function GetDriveInfoLog(ByVal vstrPath As String, ByRef robjDecorationTagValueSettings As Collection, Optional ByVal vstrPrefixSpaces As String = "") As String

    Dim strLog As String, objDrive As Scripting.Drive
    
    strLog = ""
    
    With New Scripting.FileSystemObject
    
        Set objDrive = .GetDrive(.GetDriveName(vstrPath))
    
        strLog = strLog & vstrPrefixSpaces & "Drive letter: " & objDrive.DriveLetter & vbNewLine
        
        strLog = strLog & vstrPrefixSpaces & "Drive type: " & GetDriveTypeNameFromDriveTypeConst(objDrive.DriveType) & vbNewLine
        
        Select Case objDrive.DriveType
        
            Case Remote
            
                strLog = strLog & vstrPrefixSpaces & "Network share name: " & objDrive.ShareName
            
            Case Else
            
                strLog = strLog & vstrPrefixSpaces & "Volume name: " & objDrive.VolumeName
        End Select
    End With

    Set robjDecorationTagValueSettings = New Collection
    
    With robjDecorationTagValueSettings
    
        .Add "Drive letter:;" & vbNewLine & ";11;trRGBWhiteFontColor; ;MultiTags"
        
        .Add "Drive type:;" & vbNewLine & ";11;trRGBWhiteFontColor; ;MultiTags"
        
        .Add "Network share name:;" & vbNewLine & ";11;trRGBWhiteFontColor; ;MultiTags"
        
        .Add "Volume name:;" & vbNewLine & ";11;trRGBWhiteFontColor; ;MultiTags"
    End With

    GetDriveInfoLog = strLog
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function GetSpecifiedDriveLettersInThisPC(ByVal venmDriveTypeConst As Scripting.DriveTypeConst) As Collection
    
    Dim objDrive As Scripting.Drive, objDriveLetters As Collection

    With New Scripting.FileSystemObject

        For Each objDrive In .Drives
        
            With objDrive
                
                If .IsReady And .DriveType = venmDriveTypeConst Then
                    
                    If objDriveLetters Is Nothing Then Set objDriveLetters = New Collection
                    
                    objDriveLetters.Add objDrive.DriveLetter
                End If
            End With
        Next
    End With

    Set GetSpecifiedDriveLettersInThisPC = objDriveLetters
End Function


'''
'''
'''
Private Function mfstrGetSpecifiedVolumeNameDriveNotFoundMessage(ByVal venmDriveType As Scripting.DriveTypeConst, ByVal vstrVolumeName As String) As String

    Dim strLog As String
    
    strLog = "The " & GetDriveTypeNameFromDriveTypeConst(venmDriveType) & " volume name [" & vstrVolumeName & "] is not found." & vbNewLine
    
    msubAddDriveAdditionalInformationWhenItIsNotReady strLog, venmDriveType
    
    mfstrGetSpecifiedVolumeNameDriveNotFoundMessage = strLog
End Function

'''
'''
'''
Private Function msubAddDriveAdditionalInformationWhenItIsNotReady(ByRef rstrLog As String, ByRef renmDriveType As Scripting.DriveTypeConst)

    Select Case renmDriveType
    
        Case 1, 2
            
            ' Scripting.DriveTypeConst.Fixed       ' 2
            ' Scripting.DriveTypeConst.Removable   ' 1
            
            rstrLog = rstrLog & vbNewLine & "If you set the BitLocker into the drive and it is not unlocked, then the file-path should had not been found."
    
        Case 3  ' Scripting.DriveTypeConst.Remote      ' 3
        
            rstrLog = rstrLog & vbNewLine & "Please confirm the network connection current state."
            
        Case 4  ' Scripting.DriveTypeConst.CDRom       ' 4
    
            rstrLog = rstrLog & vbNewLine & "Please confirm that some disc is inserted or it is enabled."
    End Select
End Function

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** About drive letter
'**---------------------------------------------
Private Sub msubSanityTestToGetMaxSizeLocalHardDriveLetterPath()

    Debug.Print "The drive letter of the maximum size: [" & GetMaxSizeLocalHardDriveLetterPath() & ":]"
End Sub

Private Sub msubSanityTestToGetThisLocalWindowsSystemDriveLetter()

    Debug.Print GetThisLocalWindowsSystemDriveLetter()   ' in this local PC
End Sub

'''
''' get drive letters about SSD, HDD
'''
Private Sub msubSanityToGetLocalHardDriveLettersInThisPC()

    DebugDumpColAsString GetLocalHardDriveLettersInThisPC()

End Sub

'''
''' get drive letters about USB memory or SD card
'''
Private Sub msubSanityTestToGetLocalRemovableDriveLettersInThisPC()

    If IsAtLeastOneRemovableDiskConnected() Then
    
        DebugDumpColAsString GetLocalRemovableDriveLettersInThisPC()
    Else
        Debug.Print "Not found removable disk"
    End If
End Sub

'**---------------------------------------------
'** About sub-folders below a root drive
'**---------------------------------------------
'''
''' FileSystemObject.GetFolder behavior test
'''
Private Sub msubSanityTestToGetDriveRootSubFolder01()

    Dim objDir As Scripting.Folder, objDrive As Scripting.Drive, strDrivePath As String, varDriveLetter As Variant

    With New Scripting.FileSystemObject
    
        Set objDir = .GetFolder(GetThisLocalWindowsSystemDriveLetter() & ":")

        ' The following is not a proper behavior, but it's a specification
        
        Debug.Print objDir.Path

        
        Set objDrive = .GetDrive(GetThisLocalWindowsSystemDriveLetter() & ":")

        Set objDir = objDrive.RootFolder

        ' The following is a proper hehavior
        Debug.Print objDir.Path
        
        
        For Each varDriveLetter In GetLocalHardDriveLettersInThisPC()
    
            strDrivePath = varDriveLetter & ":"
        
            Set objDir = .GetFolder(strDrivePath)
        
            ' When the drive letter is only the system-drive, the .GetFolder() method behavior is changed.
            Debug.Print objDir.Path
        Next
    End With
End Sub


