VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CurrentUserDomain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   This computer relative domain information data class
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       Dependent on ADOConnectionUtilities.bas
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mstrCurrentUserName As String

Private mstrCurrentNodePCName As String

Private mstrIPv4Address As String

Private mstrMACAddress As String


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    msubLoadFromThisComputer
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
''' read-only current log-ins user name
'''
Public Property Get CurrentUserName() As String

    CurrentUserName = mstrCurrentUserName
End Property

'''
''' read-only current log-ins user name without domain-name of the Active-Directory domain service
'''
Public Property Get CurrentUserNameWithoutDomainName() As String

    Dim strName As String, intP1 As Long
    
    strName = mstrCurrentUserName

    intP1 = InStr(1, strName, "\")
    
    If intP1 > 0 Then
    
        strName = Mid(strName, intP1 + 1, Len(strName) - intP1)
    End If

    CurrentUserNameWithoutDomainName = strName
End Property


'''
''' read-only this computer host name
'''
Public Property Get CurrentNodePCName() As String

    CurrentNodePCName = mstrCurrentNodePCName
End Property

'''
''' read-only IPv4 address
'''
Public Property Get IPv4Address() As String

    IPv4Address = mstrIPv4Address
End Property

'''
''' read-only MAC address
'''
Public Property Get MACAddress() As String

    MACAddress = mstrMACAddress
End Property



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubLoadFromThisComputer()

    LoadCurrentUserDomainFromThisComputer mstrCurrentUserName, mstrCurrentNodePCName, mstrIPv4Address, mstrMACAddress
End Sub

