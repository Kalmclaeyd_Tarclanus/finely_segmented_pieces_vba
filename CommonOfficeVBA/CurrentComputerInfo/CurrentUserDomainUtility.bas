Attribute VB_Name = "CurrentUserDomainUtility"
'
'   Collect this computer relative domain information
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 13/Jun/2022    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = True


'///////////////////////////////////////////////
'/// Private WinAPI Definitions
'///////////////////////////////////////////////
#If VBA7 Then
    ' For 64 bit version windows, declare timeGetTime
    Private Declare PtrSafe Function timeGetTime Lib "winmm.dll" () As Long
#Else
    ' For 32 bit version windows, declare timeGetTime
    Private Declare Function timeGetTime Lib "winmm.dll" () As Long
#End If

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Public mobjSharedCurrentUserDomain As CurrentUserDomain

'**---------------------------------------------
'** FEATURE FLAG
'**---------------------------------------------
Public Const mblnFEATURE_FLAG_PREVENT_SPECIAL_CLIENT_USER_LOG As Boolean = True


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetCurrentUserAndThisComputerInfoDic() As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Dim strUserName As String, strNodeComputerName As String, strIPv4Address As String, strMACAddress As String
    
    
    LoadCurrentUserDomainFromThisComputer strUserName, strNodeComputerName, strIPv4Address, strMACAddress
    
    Set objDic = New Scripting.Dictionary

    With objDic
    
        .Add "This computer name", strNodeComputerName
    
        .Add "Current user", strUserName
    
        .Add "This computer IPv4 address", strIPv4Address
        
        .Add "This computer MAC address", strMACAddress
    End With

    Set GetCurrentUserAndThisComputerInfoDic = objDic
End Function

'''
'''
'''
''' <Argument>rstrCurrentUserName: Output</Argument>
''' <Argument>rstrCurrentNodePCName: Output</Argument>
''' <Argument>rstrIPv4Address: Output</Argument>
''' <Argument>rstrMACAddress: Output</Argument>
Public Sub LoadCurrentUserDomainFromThisComputer(ByRef rstrCurrentUserName As String, _
            ByRef rstrCurrentNodePCName As String, _
            ByRef rstrIPv4Address As String, _
            ByRef rstrMACAddress As String)

    Dim intT1 As Long, intT2 As Long
    
    intT1 = timeGetTime

    msubLoadCurrentUserNameFromThisComputer rstrCurrentUserName, rstrCurrentNodePCName
    
    msubLoadIPv4AddressFromThisComputer rstrIPv4Address, rstrMACAddress

    intT2 = timeGetTime

    Debug.Print "Current User Info Get Elapsed time: " & Format(CDbl(intT2 - intT1) / 1000#, "0.000") & "[s]"
End Sub


'''
'''
'''
''' <Argument>rstrCurrentUserName: Output</Argument>
''' <Argument>rstrCurrentNodePCName: Output</Argument>
Private Sub msubLoadCurrentUserNameFromThisComputer(ByRef rstrCurrentUserName As String, _
        ByRef rstrCurrentNodePCName As String)

    Dim strUserDomain As String
    
#If HAS_REF Then

    Dim objNetwork As IWshRuntimeLibrary.WshNetwork
#Else
    Dim objNetwork As Object
#End If

    ' From WScript.Network object
    Set objNetwork = CreateObject("WScript.Network")
    
    strUserDomain = ""
    
    strUserDomain = objNetwork.UserDomain
    
    If strUserDomain <> "" Then
    
        rstrCurrentUserName = strUserDomain & "\" & objNetwork.UserName
    Else
        rstrCurrentUserName = objNetwork.UserName
    End If
    
    rstrCurrentNodePCName = objNetwork.ComputerName
End Sub

'''
'''
'''
''' <Argument>rstrIPv4Address: Output</Argument>
''' <Argument>rstrMACAddress: Output</Argument>
Private Sub msubLoadIPv4AddressFromThisComputer(ByRef rstrIPv4Address As String, _
        ByRef rstrMACAddress As String)

    Dim i As Long
    
#If HAS_REF Then

    Dim objLocator As WbemScripting.SWbemLocator, objService As WbemScripting.SWbemServicesEx, objClassSet As WbemScripting.SWbemObjectSet, objItem As WbemScripting.SWbemObjectEx
    
    Set objLocator = New WbemScripting.SWbemLocator
#Else
    Dim objLocator As Object, objService As Object, objClassSet As Object, objItem As Object
    
    ' From WMI object, connect to the local
    Set objLocator = CreateObject("WbemScripting.SWbemLocator")
#End If

    Set objService = objLocator.ConnectServer

    ' query by WQL
    Set objClassSet = objService.ExecQuery("Select * From Win32_NetworkAdapterConfiguration")

    rstrIPv4Address = ""
    
    For Each objItem In objClassSet
    
        With objItem
        
            If .IPEnabled Then
    
                If UBound(.IPAddress) > 0 Then
                
                    For i = 0 To UBound(.IPAddress)
                    
                        rstrIPv4Address = rstrIPv4Address & .IPAddress(i)
                        
                        If i < UBound(.IPAddress) Then
                        
                            rstrIPv4Address = rstrIPv4Address & ";"
                        End If
                    Next
                Else
                    rstrIPv4Address = .IPAddress(0)
                End If
                
                rstrMACAddress = Replace(.MACAddress, ":", "-")
            End If
        End With
    Next
End Sub

'''
'''
'''
Public Sub GetCurrentLoginUserNameAndIPvAddress(ByRef rstrUserName As String, ByRef rstrIPv4Address As String)

    If mobjSharedCurrentUserDomain Is Nothing Then
    
        Set mobjSharedCurrentUserDomain = New CurrentUserDomain
    End If

    With mobjSharedCurrentUserDomain
    
        rstrUserName = .CurrentUserNameWithoutDomainName
        
        rstrIPv4Address = .IPv4Address
    End With
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToLoadCurrentUserDomainFromThisComputer()

    Dim strUserName As String, strNodeComputerName As String, strIPv4Address As String, strMACAddress As String
    
    LoadCurrentUserDomainFromThisComputer strUserName, strNodeComputerName, strIPv4Address, strMACAddress


    Debug.Print "Current user name: " & strUserName

    Debug.Print "Current this computer name: " & strNodeComputerName
    
    Debug.Print "Current this computer IPv4 address: " & strIPv4Address
    
    Debug.Print "Current this computer MAC address: " & strMACAddress
End Sub

'''
'''
'''
Private Sub msubSanityTestOfCurrentUserNameWithoutDomainName()

    If mobjSharedCurrentUserDomain Is Nothing Then
    
        Set mobjSharedCurrentUserDomain = New CurrentUserDomain
    End If

    Debug.Print mobjSharedCurrentUserDomain.CurrentUserNameWithoutDomainName
End Sub

