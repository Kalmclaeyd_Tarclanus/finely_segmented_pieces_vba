Attribute VB_Name = "CreateRandomSequence"
'
'   get random sorted sequences
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 10/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetAlphabetsCombinationsColWithoutDuplicating(ByVal vintCountOfAlphabetsCombinations As Long, Optional ByVal vintStringLength As Long = 2, Optional ByVal vstrFirstCharacter As String = "A", Optional ByVal vstrLastCharacter As String = "Z", Optional ByVal vobjDefaultAlphabets As Collection = Nothing) As Collection

    Dim objPreventDuplicatingDic As Scripting.Dictionary, varAlphabets As Variant, i As Long, intMax As Long
    Dim objCol As Collection
    
    Set objCol = New Collection
    Set objPreventDuplicatingDic = New Scripting.Dictionary
    
    If Not vobjDefaultAlphabets Is Nothing Then
    
        For Each varAlphabets In vobjDefaultAlphabets
        
            objPreventDuplicatingDic.Add CStr(varAlphabets), 0
            
            objCol.Add CStr(varAlphabets)
        Next
    End If
    
    intMax = vintCountOfAlphabetsCombinations - objPreventDuplicatingDic.Count
    
    
    For i = 1 To intMax
    
        objCol.Add GetUpperCaseAlphabetRandomString(vintStringLength, "A", "Z", objPreventDuplicatingDic)
    Next

    Set GetAlphabetsCombinationsColWithoutDuplicating = objCol
End Function


'''
'''
''' This includes 'ReDim Preserve'
'''
''' Forked from https://dailyrecords.blog/archives/13489
Public Sub GetRandomIntegerArrayWithoutDuplicating(ByRef rvarOutputs() As Long, ByVal vintMinInteger As Long, ByVal vintMaxInteger As Long)
    
    If vintMinInteger < vintMaxInteger Then
     
        Dim intInputArray() As Long, i As Long
        Dim intArrayLength As Long, intRandomIndexOfArray As Long
        
        intArrayLength = vintMaxInteger - vintMinInteger + 1
        
        ReDim intInputArray(1 To intArrayLength)
        ReDim rvarOutputs(1 To intArrayLength)
        
        For i = 1 To intArrayLength
            
            intInputArray(i) = vintMinInteger + i - 1
        Next
         
        VBA.Randomize
         
        
        For i = 1 To UBound(intInputArray)
            
            intRandomIndexOfArray = Int((UBound(intInputArray) - LBound(intInputArray) + 1) * VBA.Math.Rnd() + LBound(intInputArray))
            
            ' Output
            rvarOutputs(i) = intInputArray(intRandomIndexOfArray)
            
            intInputArray(intRandomIndexOfArray) = intInputArray(UBound(intInputArray))
            
            ' delete last item from the array
            ReDim Preserve intInputArray(1 To UBound(intInputArray) - 1)
            
            If UBound(intInputArray) = 1 Then
            
                ' Output
                rvarOutputs(i + 1) = intInputArray(1)
                
                Exit Sub
            End If
        Next

    Else
    
        ' error
    End If
    
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfGetRandomIntegerArrayWithoutDuplicating()

    Dim intRandomIntegerArray() As Long

    GetRandomIntegerArrayWithoutDuplicating intRandomIntegerArray, 6, 21
    
    DebugDumpArrayWithDelimited intRandomIntegerArray
End Sub
