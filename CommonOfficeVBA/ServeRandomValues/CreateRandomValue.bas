Attribute VB_Name = "CreateRandomValue"
'
'   get random values, which dependent on VBA.Math library
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 10/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetRandomInteger(ByVal vintMinNumber As Long, ByVal vintMaxNumber As Long) As Long

    GetRandomInteger = Int((vintMaxNumber - vintMinNumber + 1) * VBA.Math.Rnd() + vintMinNumber)
End Function

'''
'''
'''
Public Function GetRandomIntegerByDigits(ByVal vintDigits As Long) As Long

    Dim intValue As Long
    
    'intValue = Int(Excel.WorksheetFunction.Power(10, vintDigits)) - 1
    intValue = Int(10 ^ vintDigits) - 1
    
    GetRandomIntegerByDigits = GetRandomInteger(1, intValue)
End Function

'''
'''
'''
Public Function GetRandomRealNumber(ByVal vdblMinNumber As Double, ByVal vdblMaxNumber As Double) As Double

    ' Note: The accurate vdblMaxNumber cannot be got, and the accurate vdblMinNumber rarely can be got

    GetRandomRealNumber = (vdblMaxNumber - vdblMinNumber) * VBA.Math.Rnd() + vdblMinNumber
End Function


'''
''' confirm VBA syntax of ^
'''
Public Function Get10ToTheXthPowerForConfirmVBASyntax(ByVal x As Long)

    Get10ToTheXthPowerForConfirmVBASyntax = 10 ^ x
End Function

'''
'''
'''
Public Function GetUpperCaseAlphabetRandomString(Optional ByVal vintStringLength As Long = 2, Optional ByVal vstrFirstCharacter As String = "A", Optional ByVal vstrLastCharacter As String = "Z", Optional ByVal vobjPreventDuplicatingDic As Scripting.Dictionary = Nothing) As String

    Dim j As Long
    Dim strRandomAlphabets As String, blnPreventDuplicating As Boolean
    Dim intTryCounter As Long
    Const intTryMax As Long = 300
    
    blnPreventDuplicating = False
    
    If Not vobjPreventDuplicatingDic Is Nothing Then
    
        blnPreventDuplicating = True
    End If
    
    intTryCounter = 1
    Do
        strRandomAlphabets = ""
        
        For j = 1 To vintStringLength
        
            strRandomAlphabets = strRandomAlphabets & GetUpperCaseAlphabetCharacter(vstrFirstCharacter, vstrLastCharacter) ' VBA.Chr(Int(intLastCode - intFirstCode) * VBA.Math.Rnd() + intFirstCode)
        Next
        
        If blnPreventDuplicating Then
        
            With vobjPreventDuplicatingDic
            
                If Not .Exists(strRandomAlphabets) Then
                
                    .Add strRandomAlphabets, 0
                    
                    Exit Do
                End If
            End With
        End If
        
        intTryCounter = intTryCounter + 1
        
    Loop While blnPreventDuplicating Or (intTryCounter <= intTryMax)
    
    GetUpperCaseAlphabetRandomString = strRandomAlphabets
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function GetUpperCaseAlphabetCharacter(Optional ByVal vstrFirstCharacter As String = "A", Optional ByVal vstrLastCharacter As String = "Z") As String
    
    Dim intFirstCode As Long, intLastCode As Long
    Dim intRandomCode As Long
    
    intFirstCode = VBA.Strings.Asc(vstrFirstCharacter)
    
    intLastCode = VBA.Strings.Asc(vstrLastCharacter)
    
    
    intRandomCode = Int((intLastCode - intFirstCode + 1) * VBA.Math.Rnd() + intFirstCode)

    GetUpperCaseAlphabetCharacter = VBA.Strings.Chr(intRandomCode)
End Function

'''
''' wrong sample
'''
Private Function mfstrGetUpperCaseAlphabetCharacterOther02(Optional ByVal vstrFirstCharacter As String = "A", Optional ByVal vstrLastCharacter As String = "Z") As String
    
    Dim intFirstCode As Long, intLastCode As Long
    Dim intRandomCode As Long
    
    intFirstCode = VBA.Strings.Asc(vstrFirstCharacter)
    
    intLastCode = VBA.Strings.Asc(vstrLastCharacter)
    
    ' The following calculation is wrong
    intRandomCode = Int((intLastCode - intFirstCode) * VBA.Math.Rnd() + intFirstCode)

    mfstrGetUpperCaseAlphabetCharacterOther02 = VBA.Strings.Chr(intRandomCode)
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfGetRandomRealNumber()

    
'    msubSanityTestOfGetRandomRealNumberForConfirming 10000, -6, 8
'
'    msubSanityTestOfGetRandomRealNumberForConfirming 10000, 12, 8956


' 10000000

    msubSanityTestOfGetRandomRealNumberForConfirming 10000000, 12, 8956

End Sub

'''
'''
'''
Private Sub msubSanityTestOfGetRandomRealNumberForConfirming(ByVal vintTryMax As Long, ByVal vdblMinNumber As Double, ByVal vdblMaxNumber)

    Dim i As Long, dblMinThreshold As Double, dblMaxThreshold As Double, dblRandomValue As Double
    Dim intCountOfSmall As Long, intCountOfBig As Long, intErrorCount As Long, intCountOfEqualsToMin As Long, intCountOfEqualsToMax As Long
    
    
    dblMinThreshold = vdblMinNumber + (vdblMaxNumber - vdblMinNumber) / 1000#
    
    dblMaxThreshold = vdblMaxNumber - (vdblMaxNumber - vdblMinNumber) / 1000#

    intCountOfSmall = 0
    
    intCountOfBig = 0
    
    intErrorCount = 0
    
    intCountOfEqualsToMin = 0: intCountOfEqualsToMax = 0
    
    For i = 1 To vintTryMax
    
        dblRandomValue = GetRandomRealNumber(vdblMinNumber, vdblMaxNumber)
    
        If dblRandomValue < dblMinThreshold Then
        
            intCountOfSmall = intCountOfSmall + 1
            
            If dblRandomValue = vdblMinNumber Then
            
                intCountOfEqualsToMin = intCountOfEqualsToMin + 1
            
            ElseIf dblRandomValue < vdblMinNumber Then
            
                intErrorCount = intErrorCount + 1
            
                Err.Raise 1
            End If
        
        ElseIf dblRandomValue > dblMaxThreshold Then
        
            intCountOfBig = intCountOfBig + 1
        
            If dblRandomValue = vdblMaxNumber Then
        
            intCountOfEqualsToMax = intCountOfEqualsToMax + 1
        
            ElseIf dblRandomValue > vdblMaxNumber Then
            
                intErrorCount = intErrorCount + 1
            
                Err.Raise 2
            End If
        End If
    Next

    Debug.Print "The " & CStr(vintTryMax) & " are tried;"
    
    Debug.Print CStr(intCountOfSmall) & ", " & CStr(intCountOfBig) & ", " & CStr(intCountOfEqualsToMin) & ", " & CStr(intCountOfEqualsToMax)

    If intErrorCount > 0 Then
    
        MsgBox "GetRandomRealNumber has functional error", vbOKOnly Or vbCritical
    End If
End Sub


'''
'''
Private Sub msubSanityTestOfGetUpperCaseAlphabetCharacter()

    msubSanityTestOfGetUpperCaseAlphabetCharacterForConfirming 1000
End Sub


'''
'''
'''
Private Sub msubSanityTestOfGetUpperCaseAlphabetCharacterForConfirming(ByVal vintTryMax As Long, Optional ByVal vstrFirstCharacter As String = "A", Optional ByVal vstrLastCharacter As String = "Z")
    
    Dim strChar As String, i As Long
    Dim intFirstCode As Long, intLastCode As Long, strIrregalSmallCharacter As String, strIrregalBigCharacter As String
    Dim intGetRandomCode As Long
    Dim intCountOfGettingFirstChar01 As Long, intCountOfGettingLastChar01 As Long, intCountOfGettingFirstChar02 As Long, intCountOfGettingLastChar02 As Long
    

    intFirstCode = VBA.Asc(vstrFirstCharacter)
    
    intLastCode = VBA.Asc(vstrLastCharacter)
    
    
    strIrregalSmallCharacter = Chr(intFirstCode - 1)
    
    strIrregalBigCharacter = Chr(intLastCode + 1)

    intCountOfGettingFirstChar01 = 0: intCountOfGettingFirstChar02 = 0
    
    intCountOfGettingLastChar01 = 0: intCountOfGettingLastChar02 = 0

    For i = 1 To vintTryMax
    
        intGetRandomCode = VBA.Asc(GetUpperCaseAlphabetCharacter(vstrFirstCharacter, vstrLastCharacter))
    
        If intGetRandomCode = intFirstCode Then
    
            'Debug.Print "About GetUpperCaseAlphabetCharacter get first char: " & vstrFirstCharacter
            
            intCountOfGettingFirstChar01 = intCountOfGettingFirstChar01 + 1
    
        ElseIf intGetRandomCode = intLastCode Then
    
            'Debug.Print "About GetUpperCaseAlphabetCharacter get last char: " & vstrLastCharacter
    
            intCountOfGettingLastChar01 = intCountOfGettingLastChar01 + 1
    
        ElseIf intGetRandomCode < intFirstCode Then
        
            Debug.Print "About GetUpperCaseAlphabetCharacter occur exceeding lower value error"
        
            Err.Raise 1
        ElseIf intGetRandomCode > intLastCode Then
        
            Debug.Print "About GetUpperCaseAlphabetCharacter occur exceeding upper value error"
        
            Err.Raise 2
        End If
        
        
        intGetRandomCode = VBA.Asc(mfstrGetUpperCaseAlphabetCharacterOther02(vstrFirstCharacter, vstrLastCharacter))
    
        If intGetRandomCode = intFirstCode Then
    
            'Debug.Print "About mfstrGetUpperCaseAlphabetCharacterOther02 get first char: " & vstrFirstCharacter
    
            intCountOfGettingFirstChar02 = intCountOfGettingFirstChar02 + 1
    
        ElseIf intGetRandomCode = intLastCode Then
    
            'Debug.Print "About mfstrGetUpperCaseAlphabetCharacterOther02 get last char: " & vstrLastCharacter
    
            intCountOfGettingLastChar02 = intCountOfGettingLastChar02 + 1
    
        ElseIf intGetRandomCode < intFirstCode Then
        
            Debug.Print "About mfstrGetUpperCaseAlphabetCharacterOther02 occur exceeding lower value error"
        
            Err.Raise 3
        ElseIf intGetRandomCode > intLastCode Then
        
            Debug.Print "About mfstrGetUpperCaseAlphabetCharacterOther02 occur exceeding upper value error"
        
            Err.Raise 4
        End If
    Next
    
    Debug.Print CStr(intCountOfGettingFirstChar01) & ", " & CStr(intCountOfGettingLastChar01) & ", " & CStr(intCountOfGettingFirstChar02) & ", " & CStr(intCountOfGettingLastChar02)
End Sub


'''
'''
'''
Private Sub msubSanityTestOfVBARnd()

    ' The following doesn't get often 0 value of Rnd()
    'msubSanityTestOfVBARndForConfirmingPublishedSpecification 1000000

    ' The following gets often one time of the zero value of Rnd()
    msubSanityTestOfVBARndForConfirmingPublishedSpecification 10000000
End Sub

'''
''' inspect VBA.Rnd()
'''
Private Sub msubSanityTestOfVBARndForConfirmingPublishedSpecification(ByVal vintTryMax As Long)
    
    Dim sngValue As Single, i As Long, strZeroHitsLog As String
    Dim intSmallCounter As Long, intBigCounter As Long, intEqualToZeroCounter As Long
    
    intEqualToZeroCounter = 0
    
    intSmallCounter = 0
    
    intBigCounter = 0

    For i = 1 To vintTryMax
    
        sngValue = VBA.Math.Rnd()
        
        If sngValue = 0 Or sngValue = 0# Or sngValue = 0& Then
            ' it satisfy the VBA.Rnd() specification
        
            'Debug.Print "Try count: " & CStr(i) & ", Rnd() outputs 0.0"
            
            strZeroHitsLog = ""
            
            If sngValue = 0 Then
            
                strZeroHitsLog = strZeroHitsLog & "Hits ""= 0""" & vbNewLine
            End If
            If sngValue = 0# Then
            
                strZeroHitsLog = strZeroHitsLog & "Hits ""= 0#""" & vbNewLine
            End If
            If sngValue = 0& Then
            
                strZeroHitsLog = strZeroHitsLog & "Hits ""= 0&""" & vbNewLine
            End If
            
            MsgBox strZeroHitsLog, vbOKOnly, "Compare Operator syntax test when VBA.Rnd() returns 0"
            
            intEqualToZeroCounter = intEqualToZeroCounter + 1
            
            'Stop
            
        ElseIf sngValue < 0# Then
            
            ' it seems an error
            
            Err.Raise 3
            
        ElseIf sngValue = 1# Then
            ' it seems an error from the VBA.Rnd() specification
        
            Debug.Print "Try count: " & CStr(i) & ", ERROR: Rnd() outputs 1.0"
        
            Stop
        
            Err.Raise 1
        ElseIf sngValue > 1# Then
        
            ' it seems that an error
            Err.Raise 2
        ElseIf sngValue < 0.001 Then
            ' it satisfy the VBA.Rnd() specification
        
            'Debug.Print "Try count: " & CStr(i) & ", Rnd() outputs " & CStr(sngValue) & " , which is nearly equals to 0 (Small)"
            
            intSmallCounter = intSmallCounter + 1
            
        ElseIf sngValue > 0.999 Then
            ' it satisfy the VBA.Rnd() specification
        
            'Debug.Print "Try count: " & CStr(i) & ", Rnd() outputs " & CStr(sngValue) & " , which is nearly equals to 1 (Big)"
            
            intBigCounter = intBigCounter + 1
        End If
    Next
    
    Debug.Print "The " & CStr(vintTryMax) & " are tried;"
    
    Debug.Print "Count of small values: " & CStr(intSmallCounter) & ", Count of big values: " & CStr(intBigCounter)
    
    If intEqualToZeroCounter > 0 Then
        
        MsgBox "Count of perfect 0: " & CStr(intEqualToZeroCounter), vbInformation Or vbOKOnly, "Getting perfect 0"
        
        Debug.Print "Count of perfect 0: " & CStr(intEqualToZeroCounter)
    End If
    
End Sub


'''
'''
'''
Private Sub msubSanityTestOfGet10ToTheXthPowerForConfirmVBASyntax()
    
    Dim i As Long
    
    For i = 2 To 8
        
        Debug.Print CStr(Get10ToTheXthPowerForConfirmVBASyntax(i))
    Next
End Sub

Private Sub msubSanityTestOfGetRandomIntegerArrayIncludingMinusNumberWithoutDuplicating()

    Dim intRandomIntegerArray() As Long

    GetRandomIntegerArrayWithoutDuplicating intRandomIntegerArray, -10, 9
    
    DebugDumpArrayWithDelimited intRandomIntegerArray
End Sub


Private Sub msubSanityTestOfGetUpperCaseAlphabetRandomStringWithoutDuplicating()

    Dim objPreventDuplicatingDic As Scripting.Dictionary, i As Long, objCol As Collection
    

    Set objPreventDuplicatingDic = New Scripting.Dictionary
    
    Set objCol = New Collection
    
    For i = 1 To 12
    
        objCol.Add GetUpperCaseAlphabetRandomString(5, "C", "W", vobjPreventDuplicatingDic:=objPreventDuplicatingDic)
    Next
    
    DebugDumpColAsString objCol
End Sub

