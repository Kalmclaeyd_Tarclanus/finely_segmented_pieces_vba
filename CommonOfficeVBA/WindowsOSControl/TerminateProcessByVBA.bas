Attribute VB_Name = "TerminateProcessByVBA"
'
'   Terminate Windows executing process
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 26/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
' if Microsoft WMI Scripting V1.2 Library is refered
#Const HAS_WMI_REF = False


'///////////////////////////////////////////////
'/// Windows API Declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function CreateToolhelp32Snapshot Lib "kernel32.dll" (ByVal Flags As LongPtr, ByVal ProcessID As Long) As LongPtr
    Private Declare PtrSafe Function Process32First Lib "kernel32.dll" (ByVal lngHandleSnapshot As LongPtr, ByRef ProcessEntry As PROCESSENTRY32) As Long
    Private Declare PtrSafe Function Process32Next Lib "kernel32.dll" (ByVal lngHandleSnapshot As LongPtr, ByRef ProcessEntry As PROCESSENTRY32) As Long

    Private Declare PtrSafe Function OpenProcess Lib "kernel32.dll" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As LongPtr
    Private Declare PtrSafe Function CloseHandle Lib "kernel32.dll" (ByVal hObject As LongPtr) As Long
    Private Declare PtrSafe Function TerminateProcess Lib "kernel32.dll" (ByVal hProcess As LongPtr, ByVal uExitCode As Long) As Long

    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
#Else

    Private Declare Function CreateToolhelp32Snapshot Lib "kernel32.dll" (ByVal Flags As Long, ByVal ProcessID As Long) As Long
    Private Declare Function Process32First Lib "kernel32.dll" (ByVal lngHandleSnapshot As Long, ByRef ProcessEntry As PROCESSENTRY32) As Long
    Private Declare Function Process32Next Lib "kernel32.dll" (ByVal lngHandleSnapshot As Long, ByRef ProcessEntry As PROCESSENTRY32) As Long

    Private Declare Function OpenProcess Lib "kernel32.dll" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
    Private Declare Function CloseHandle Lib "kernel32.dll" (ByVal hObject As Long) As Long
    Private Declare Function TerminateProcess Lib "kernel32.dll" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
    
    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
#End If


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const SYNCHRONIZE       As Long = &H100000
Private Const PROCESS_TERMINATE As Long = &H1

Private Const TH32CS_SNAPPROCESS As Long = &H2
Private Const SIZEOF_PROCESSENTRY32 As Long = 296

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private Type PROCESSENTRY32
    Size As Long
    RefCount As Long
    ProcessID As Long
    HeapID As Long
    ModuleID As Long
    ThreadCount As Long
    ParentPriority As Long
    BasePriority As Long
    Flags As Long
    'fileName As String * 256
    fileName As String * 260
End Type
 

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' Terminate process using Windows API
'''
''' Attention: In 64bit VBA, this has some problems.
'''
Public Sub TerminateProcessByProcessProgramName(ByVal vstrProcessProgramName As String)
    
#If VBA7 Then
    Dim intProcessEnumerateHandle As LongPtr
#Else
    Dim intProcessEnumerateHandle As Long
#End If
    
    Dim intBoolValue As Long
    Dim stdProcessEntry As PROCESSENTRY32, strCurrentFileName As String
     
    intProcessEnumerateHandle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, &O0)   ' In 64bit Windows, this has no problem.
    
    Debug.Print GetDllErrorMessage()
    
    stdProcessEntry.Size = SIZEOF_PROCESSENTRY32
    
    intBoolValue = Process32First(intProcessEnumerateHandle, stdProcessEntry)   ' In 64bit windows, this [Process32First] is not supported
    
    Debug.Print GetDllErrorMessage()
    
    'DebugOutProcessProgramName stdProcessEntry
    
    Do While intBoolValue
    
        strCurrentFileName = Left(stdProcessEntry.fileName, InStr(stdProcessEntry.fileName, vbNullChar) - 1)     ' In 64bit windows, this [Process32Next] is not supported
        
        If strCurrentFileName = vstrProcessProgramName Then
        
            TerminateProcessByProcessID stdProcessEntry.ProcessID
        End If
        
        intBoolValue = Process32Next(intProcessEnumerateHandle, stdProcessEntry)
    Loop
End Sub


'''
'''
'''
Private Sub DebugOutProcessProgramName(ByRef rstdProcessEntry As PROCESSENTRY32)

    Debug.Print "PROCESSENTRY32.fileName = " & rstdProcessEntry.fileName
    
    Debug.Print "PROCESSENTRY32.ProcessID = " & rstdProcessEntry.ProcessID
End Sub


'''
'''
'''
Public Sub TerminateProcessByProcessID(ByVal vintProcessID As Long)

#If VBA7 Then
    Dim intProcessHandle As LongPtr
#Else
    Dim intProcessHandle As Long
#End If

    intProcessHandle = OpenProcess(SYNCHRONIZE Or PROCESS_TERMINATE, 0&, vintProcessID)
            
    TerminateProcess intProcessHandle, 0
    
    CloseHandle intProcessHandle
End Sub


'''
''' Terminate process using WMI
'''
Public Sub TerminateProcessByWMI(ByVal vstrProcessName As String)
    
#If HAS_WMI_REF Then
    'Dim objWMI As WbemScripting.SWbemLocator
    Dim objWMIService As WbemScripting.SWbemServices, objQLObjects As WbemScripting.SWbemObjectSet, objUnknownObject As Object, objQLExObject As WbemScripting.SWbemObjectEx
    
    With New WbemScripting.SWbemLocator ' objWMI
    
        With .ConnectServer ' objWMIService
    
            For Each objUnknownObject In .ExecQuery("Select * From Win32_Process Where Caption='" & vstrProcessName & "'")
        
                'Debug.Print TypeName(objUnknownObject)
                If TypeOf objUnknownObject Is WbemScripting.SWbemObjectEx Then
                    
                    Set objQLExObject = objUnknownObject
                    
                    objQLExObject.Terminate ' it is hidden method at Object Browser
                End If
            Next
        End With
    End With
    
#Else
    'Dim objWMI As Object
    Dim objWMIService As Object, objQLObjects As Object, objUnknownObject As Object
    
    Set objWMIService = CreateObject("WbemScripting.SWbemLocator").ConnectServer
    
    Set objQLObjects = objWMIService.ExecQuery("Select * From Win32_Process Where Caption='" & vstrProcessName & "'")
    
    For Each objUnknownObject In objQLObjects
    
        objUnknownObject.Terminate
    Next
#End If
    
    
End Sub


'''
'''
'''
Public Sub TerminateProcessByShell(ByVal vstrProcessName As String)
    
    With CreateObject("WScript.Shell")
    
        ' Abort the specified process, which execute the TASKKILL command in the invisible Command-prompt
        
        .Run "cmd /c taskkill /f /im " & vstrProcessName & "*", 0, True
    End With
End Sub



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' Start Notepad.exe and Terminate it's process
'''
Private Sub msubSanityTestOfNotpadAutoClose()

    Dim intProcessID As Long
    
#If VBA7 Then
    Dim intProcessHandle As LongPtr
#Else
    Dim intProcessHandle As Long
#End If
 
    intProcessID = CLng(VBA.Shell("Notepad", vbNormalFocus))
 
    ' Wait for 1.2 second
    Sleep 1200
 
    ' get the process handle
    intProcessHandle = OpenProcess(SYNCHRONIZE Or PROCESS_TERMINATE, 0&, intProcessID)
 
    ' abort the notepad.exe
    TerminateProcess intProcessHandle, 0&
 
    ' close the process handle
    CloseHandle intProcessHandle
End Sub

'''
''' Attention: The 64 bit VBA doesn't support the following code...
'''
Private Sub msubSanityTestToTerminateProcessByProcessProgramName()

    'TerminateProcessByProcessProgramName "notepad.exe"
    
    TerminateProcessByProcessProgramName "Notepad.exe"
End Sub

'''
'''
'''
Private Sub msubSanityTestOfTerminateProcessByWMI()

    ' TerminateProcessByWMI "iexplore.exe"

    TerminateProcessByWMI "Notepad.exe"
End Sub

'''
''' Attention: If you use the Norton 360 software, it may prevent the following execution.
'''
Private Sub msubSanityTestOfTerminateProcessByShell()

    'TerminateProcessByShell "iexplore.exe"

    TerminateProcessByShell "Notepad.exe"
End Sub
