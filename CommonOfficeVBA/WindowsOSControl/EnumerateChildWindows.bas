Attribute VB_Name = "EnumerateChildWindows"
'
'   List up Ms Office application UserForm HWnd
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun,  6/Mar/2022    Mr. korezooo            A part of the idea has been disclosed at https://qiita.com/korezooo/items/770629327cf64bc39db8
'       Mon, 23/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'       Sat, 13/Jul/2024    Kalmclaeyd Tarclanus    Changed this standard module name from EnumerateOfficeChildWindows.bas to EnumerateChildWindows.bas

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const MSOfficeUserFormClassName As String = "ThunderDFrame"


Private mobjSearchingWindowTextRegExp As VBScript_RegExp_55.RegExp

Private mobjSearchingClassNameRegExp As VBScript_RegExp_55.RegExp

Private mobjSearchingProcessFullPathRegExp As VBScript_RegExp_55.RegExp


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** WinAPI private constants
'**---------------------------------------------
#If VBA7 Then
    Private Declare PtrSafe Function EnumWindows Lib "user32.dll" (ByVal lpEnumFunc As LongPtr, ByRef lParam() As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function EnumChildWindows Lib "user32.dll" (ByVal hWndParent As LongPtr, ByVal lpEnumFunc As LongPtr, ByRef lParam() As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function GetParent Lib "user32" (ByVal hwnd As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function IsWindowVisible Lib "user32" (ByVal hwnd As LongPtr) As Long
#Else
    Private Declare Function EnumWindows Lib "user32.dll" (ByVal lpEnumFunc As Long, ByRef lParam() As Long) As Long
    
    Private Declare Function EnumChildWindows Lib "user32.dll" (ByVal hWndParent As Long, ByVal lpEnumFunc As Long, ByRef lParam() As Long) As Long
    
    Private Declare Function GetParent Lib "user32" (ByVal hwnd As Long) As Long
    
    Private Declare Function IsWindowVisible Lib "user32" (ByVal hWnd As Long) As Long
#End If

'**---------------------------------------------
'** variables for IsModelessUserFormExists
'**---------------------------------------------
Private mstrSpecifiedCaption As String

#If VBA7 Then
    
    Private mintFoundHwnd As LongPtr
#Else
    Private mintFoundHwnd As Long
#End If


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
'''
'''
Public Enum TopLevelWindowHWndInfoSortType

    NoSortAboutTopLevelHWndInfo

    SortInfoByTopLevelHWndValue
    
    SortInfoByTopLevelHWndWindowText
    
    SortInfoByTopLevelHWndClassName
End Enum

'''
'''
'''
Public Enum HWndInfoAddingOptionType

    NoAddingInfoAboutHWnd = 0

    AddWindowTextInfoFromHWnd = &H1
    
    AddClassNameInfoFromHWnd = &H2
    
    AddBothWindowTextAndClassNameInfoFromHWnd = AddWindowTextInfoFromHWnd Or AddClassNameInfoFromHWnd
End Enum


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfIsModelessUserFormExists()

    Debug.Print IsModelessUserFormExists("Switch code panes")
End Sub


'**---------------------------------------------
'** general test
'**---------------------------------------------
'''
'''
'''
Public Sub msubSanityTestOfEnumeratingChildWindowWithSpacifyingParentExcelHwnd()

#If VBA7 Then
    Dim intHWnd As LongPtr, intExcelHWnd As LongPtr
    Dim intChildWindowHwnds() As LongPtr ' child window hWnd buffer
#Else
    Dim intHWnd As Long, intExcelHWnd As Long
    Dim intChildWindowHwnds() As Long ' child window hWnd buffer
#End If

    Dim i As Long, strTitle As String, strClassName As String, objExceptTitleKeys As Scripting.Dictionary
    
    
    Set objExceptTitleKeys = GetExceptionWindowTitleKeysDic()

    ' initialize default buffer
    ReDim intChildWindowHwnds(0)
    
    'intHWnd = 0
    intExcelHWnd = GetThisApplicationHWnd()

    Debug.Print GetWindowTextVBAString(intExcelHWnd)

    EnumChildWindows intExcelHWnd, AddressOf EnumChildProc, intChildWindowHwnds

    For i = LBound(intChildWindowHwnds) To UBound(intChildWindowHwnds)
    
        strTitle = GetWindowTextVBAString(intChildWindowHwnds(i))
    
        strClassName = GetClassNameVBAString(intChildWindowHwnds(i))
    
        Debug.Print strTitle & ", " & strClassName
    Next
End Sub


Public Sub msubSanityTestOfEnumeratingChildWindowWithoutSpacifyingParent()

#If VBA7 Then
    Dim intHWnd As LongPtr
    Dim intChildWindowHwnds() As LongPtr ' child window hWnd buffer
#Else
    Dim intHWnd As Long
    Dim intChildWindowHwnds() As Long ' child window hWnd buffer
#End If

    Dim varHWnd As Variant, i As Long
    Dim strTitle As String, strClassName As String, objExceptTitleKeys As Scripting.Dictionary
    
    Set objExceptTitleKeys = GetExceptionWindowTitleKeysDic()

    ' initialize default buffer
    ReDim intChildWindowHwnds(0)

    Call EnumChildWindows(0, AddressOf EnumChildProc, intChildWindowHwnds)

    For i = LBound(intChildWindowHwnds) To UBound(intChildWindowHwnds)
    
        strTitle = GetWindowTextVBAString(intChildWindowHwnds(i))
    
        strClassName = GetClassNameVBAString(intChildWindowHwnds(i))
    
        Debug.Print strTitle & ", " & strClassName
    Next
End Sub

'''
'''
'''
Public Sub msubSanityTestOfTopLevelHwnd()

    DebugCol GetTopHWndWindowTextAndClassNameAndItOfParentIfExistsDTCol()
End Sub

'''
''' check opened Microsoft office UserForm windows
'''
Public Sub msubSanityTestOfTopLevelHwndAboutMSOfficeUserForm()

    DebugCol GetTopHWndWindowTextAndClassNameAndItOfParentIfExistsDTCol(MSOfficeUserFormClassName, "")
End Sub

'''
'''
'''
Public Sub msubSanityTestToGetChildHwndRecursively()

    Dim objDTCol As Collection
    
    'Set objDTCol = GetChildHwndRecursively("XLMAIN|wndclass_desked_gsk")
    
    Set objDTCol = GetChildHwndRecursively("XLMAIN")

    DebugCol objDTCol
End Sub

'''
'''
'''
Public Sub msubSanityTestToGetChildHWndAndSomeInfoRecursively()

    Dim objDTCol As Collection
    
    Set objDTCol = GetChildHWndAndSomeInfoRecursively(AddBothWindowTextAndClassNameInfoFromHWnd, "XLMAIN")

    DebugCol objDTCol
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetChildHWndAndSomeInfoRecursively(Optional ByVal venmHWndInfoAddingOptionType As HWndInfoAddingOptionType = HWndInfoAddingOptionType.AddBothWindowTextAndClassNameInfoFromHWnd, _
        Optional ByVal vstrTopHWndClassNameRegExpPattern As String = "", _
        Optional ByVal vstrTopHWndWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrTopHWndProcessFullPathRegExpPattern As String = "", _
        Optional ByVal venmTopLevelWindowHWndInfoSortType As TopLevelWindowHWndInfoSortType = TopLevelWindowHWndInfoSortType.SortInfoByTopLevelHWndClassName, _
        Optional ByVal vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively As Boolean = False) As Collection

    Dim objChildHWndDTCol As Collection, varChildHWndDTCol As Variant, varChildHWndRowCol As Variant, objChildHWndRowCol As Collection, varHWnd As Variant, strHWnd As String
    
    Dim blnNeedToGetWindowText As Boolean, blnNeedToGetClassName As Boolean, strWindowText As String, strClassName As String
    
    Dim objDTCol As Collection, objRowCol As Collection
    
    Dim objHWndToWindowTextDic As Scripting.Dictionary, objHWndToClassNameDic As Scripting.Dictionary
    
#If VBA7 Then

    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If
    
    Set objDTCol = New Collection
    
    Set objChildHWndDTCol = GetChildHwndRecursively(vstrTopHWndClassNameRegExpPattern, vstrTopHWndWindowTitleRegExpPattern, vstrTopHWndProcessFullPathRegExpPattern, venmTopLevelWindowHWndInfoSortType, True, vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively)

    If objChildHWndDTCol.Count > 0 Then
    
        If venmHWndInfoAddingOptionType <> NoAddingInfoAboutHWnd Then
    
            blnNeedToGetWindowText = (venmHWndInfoAddingOptionType And AddWindowTextInfoFromHWnd) = HWndInfoAddingOptionType.AddWindowTextInfoFromHWnd
        
            If blnNeedToGetWindowText Then Set objHWndToWindowTextDic = New Scripting.Dictionary
            
        
            blnNeedToGetClassName = (venmHWndInfoAddingOptionType And AddClassNameInfoFromHWnd) = HWndInfoAddingOptionType.AddClassNameInfoFromHWnd
        
            If blnNeedToGetClassName Then Set objHWndToClassNameDic = New Scripting.Dictionary
        
        
            For Each varChildHWndDTCol In objChildHWndDTCol
            
                Set objChildHWndRowCol = varChildHWndDTCol
            
                Set objRowCol = New Collection
            
                For Each varHWnd In objChildHWndRowCol
            
                    If varHWnd = "" Then
                    
                        objRowCol.Add ""
                        
                        If blnNeedToGetWindowText Then objRowCol.Add ""
                        
                        If blnNeedToGetClassName Then objRowCol.Add ""
                    Else
                        intHWnd = varHWnd
                        
                        strHWnd = CStr(varHWnd)
                        
                        objRowCol.Add intHWnd
                        
                        If blnNeedToGetWindowText Then
                        
                            With objHWndToWindowTextDic
                            
                                If .Exists(strHWnd) Then
                                
                                    strWindowText = .Item(strHWnd)
                                Else
                                    strWindowText = GetWindowTextVBAString(intHWnd)
                                    
                                    .Add strHWnd, strWindowText
                                End If
                            End With
                            
                            objRowCol.Add strWindowText
                        End If
                        
                        If blnNeedToGetClassName Then
                        
                            With objHWndToClassNameDic
                        
                                If .Exists(strHWnd) Then
                                
                                    strClassName = .Item(strHWnd)
                                Else
                                    strClassName = GetClassNameVBAString(intHWnd)
                                    
                                    .Add strHWnd, strClassName
                                End If
                            End With
                            
                            objRowCol.Add strClassName
                        End If
                    End If
                Next
                
                objDTCol.Add objRowCol
            Next
        Else
            Set objDTCol = objChildHWndDTCol
        End If
    End If


    Set GetChildHWndAndSomeInfoRecursively = objDTCol
End Function



'''
'''
'''
Public Function GetChildHwndRecursively(Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal venmTopLevelWindowHWndInfoSortType As TopLevelWindowHWndInfoSortType = TopLevelWindowHWndInfoSortType.SortInfoByTopLevelHWndClassName, _
        Optional ByVal vblnAllowToRealignRowColSizeOfChildWndDTCol As Boolean = True, _
        Optional ByVal vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively As Boolean = False)
        
    
    Dim objDummyTopHwndWindowTextAndClassNameAndItOfParentIfExistsDTCol As Collection
    
    Dim objChildHWndDTCol As Collection
    
    GetChildHwndRecursivelyWithTopLevelInfo objDummyTopHwndWindowTextAndClassNameAndItOfParentIfExistsDTCol, objChildHWndDTCol, vstrClassNameRegExpPattern, vstrWindowTitleRegExpPattern, vstrProcessFullPathRegExpPattern, venmTopLevelWindowHWndInfoSortType, vblnAllowToRealignRowColSizeOfChildWndDTCol, vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively
    
    Set GetChildHwndRecursively = objChildHWndDTCol
End Function

'''
'''
'''
Public Sub GetChildHwndRecursivelyWithTopLevelInfo(ByRef robjTopHwndWindowTextAndClassNameAndItOfParentIfExistsDTCol As Collection, _
        ByRef robjChildWndDTCol As Collection, _
        Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal venmTopLevelWindowHWndInfoSortType As TopLevelWindowHWndInfoSortType = TopLevelWindowHWndInfoSortType.SortInfoByTopLevelHWndClassName, _
        Optional ByVal vblnAllowToRealignRowColSizeOfChildWndDTCol As Boolean = True, _
        Optional ByVal vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively As Boolean = False, _
        Optional ByVal vblnWindowTitleIgnoreCase As Boolean = False)

    Dim objTopLevelHWndDTCol As Collection, objChildHWndDTCol As Collection

    Dim varTopLevelHWndRowCol As Variant, objTopLevelHWndRowCol As Collection

    Dim objHWndRowCol As Collection, intMaxHierarchicalOrder As Long

    Dim objTmpDTCol As Collection, varRowCol As Variant, objRowCol As Collection, i As Long

#If VBA7 Then

    Dim intTopLevelHWnd As LongPtr
#Else
    Dim intTopLevelHWnd As Long
#End If

    Set objTopLevelHWndDTCol = GetTopHWndWindowTextAndClassNameAndItOfParentIfExistsDTCol(vstrClassNameRegExpPattern, vstrWindowTitleRegExpPattern, vstrProcessFullPathRegExpPattern, True, venmTopLevelWindowHWndInfoSortType, vblnWindowTitleIgnoreCase)

    Set objChildHWndDTCol = New Collection


    If Not objTopLevelHWndDTCol Is Nothing Then

        If objTopLevelHWndDTCol.Count > 0 Then
        
            intMaxHierarchicalOrder = 0
        
            For Each varTopLevelHWndRowCol In objTopLevelHWndDTCol
        
                Set objTopLevelHWndRowCol = varTopLevelHWndRowCol
        
                intTopLevelHWnd = objTopLevelHWndRowCol.Item(1)
        
                Set objHWndRowCol = New Collection
                
                objHWndRowCol.Add intTopLevelHWnd
                
                
                Select Case True
                
                    Case Not vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively, "" <> GetWindowTextVBAString(intTopLevelHWnd)
                
                        objChildHWndDTCol.Add objHWndRowCol
                End Select
                
                msubLoadChildHWndCol objChildHWndDTCol, objHWndRowCol, 1, intMaxHierarchicalOrder, vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively
            Next
        End If
    
    
        If vblnAllowToRealignRowColSizeOfChildWndDTCol Then
        
            Set objTmpDTCol = New Collection
        
            For Each varRowCol In objChildHWndDTCol
        
                Set objRowCol = varRowCol
                
                If objRowCol.Count < (intMaxHierarchicalOrder + 1) Then
                
                    For i = objRowCol.Count To intMaxHierarchicalOrder
                    
                        objRowCol.Add ""
                    Next
                End If
        
                objTmpDTCol.Add objRowCol
            Next
            
            Set objChildHWndDTCol = objTmpDTCol
        End If
    End If


    Set robjChildWndDTCol = objChildHWndDTCol

    Set robjTopHwndWindowTextAndClassNameAndItOfParentIfExistsDTCol = objTopLevelHWndDTCol
End Sub

'''
'''
'''
#If VBA7 Then
Public Function GetFirstTopHWndFromClassNameAndWindowTextAndProcessFullPath(Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal vblnWindowTitleIgnoreCase As Boolean = False) As LongPtr

    Dim intHWnd As LongPtr
#Else
Public Function GetFirstTopHWndFromClassNameAndWindowTextAndProcessFullPath(Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal vblnWindowTitleIgnoreCase As Boolean = False) As Long

    Dim intHWnd As Long
#End If

    Dim objDTCol As Collection, varRowCol As Variant, objRowCol As Collection


    Set objDTCol = GetTopHWndDTColOfFromClassNameAndWindowTextAndProcessFullPath(vstrClassNameRegExpPattern, vstrWindowTitleRegExpPattern, vstrProcessFullPathRegExpPattern, vblnWindowTitleIgnoreCase)
    
    If Not objDTCol Is Nothing Then
    
        If objDTCol.Count > 0 Then
        
            For Each varRowCol In objDTCol
            
                Set objRowCol = varRowCol
                
                intHWnd = objRowCol.Item(1)
                
                If intHWnd > 0 Then
                    
                    Exit For
                End If
            Next
        End If
    End If
    
   GetFirstTopHWndFromClassNameAndWindowTextAndProcessFullPath = intHWnd
End Function

'''
'''
'''
Public Function GetVisibleWindowTextToHWndDic(Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal vblnWindowTitleIgnoreCase As Boolean = False) As Scripting.Dictionary
        

    Set GetVisibleWindowTextToHWndDic = GetWindowTextToHWndDic(vstrClassNameRegExpPattern, vstrWindowTitleRegExpPattern, vstrProcessFullPathRegExpPattern, vblnWindowTitleIgnoreCase, True, True)
End Function

'''
'''
'''
Public Function GetWindowTextToHWndDic(Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal vblnWindowTitleIgnoreCase As Boolean = False, _
        Optional ByVal vblnAllowToFileterByWindowVisibleState As Boolean = False, _
        Optional ByVal vblnSpecifiedWindowVisibleState As Boolean = True) As Scripting.Dictionary

    Dim objDTCol As Collection, objDic As Scripting.Dictionary
    
    Dim varRowCol As Variant, objRowCol As Collection, strWindowText As String, blnAllowToAdd As Boolean
    
#If VBA7 Then

    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    Set objDTCol = GetTopHWndDTColOfFromClassNameAndWindowTextAndProcessFullPath(vstrClassNameRegExpPattern, vstrWindowTitleRegExpPattern, vstrProcessFullPathRegExpPattern, vblnWindowTitleIgnoreCase)
    
    If Not objDTCol Is Nothing Then
    
        If objDTCol.Count > 0 Then
        
            Set objDic = New Scripting.Dictionary
            
            For Each varRowCol In objDTCol
            
                Set objRowCol = varRowCol
                
                intHWnd = objRowCol.Item(1)
                
                blnAllowToAdd = True
                
                
                If vblnAllowToFileterByWindowVisibleState Then
                
                    If vblnSpecifiedWindowVisibleState <> CBool(IsWindowVisible(intHWnd)) Then
                
                        blnAllowToAdd = False
                    End If
                End If
                
                If blnAllowToAdd Then
                
                    strWindowText = objRowCol.Item(3)
                    
                    If strWindowText = "" Then
                    
                        blnAllowToAdd = False
                    End If
                    
                    If blnAllowToAdd Then
                    
                        With objDic
                        
                            .Add strWindowText, CStr(intHWnd)
                        End With
                    End If
                End If
            Next
        End If
    End If
    
    If objDic Is Nothing Then
    
        Set objDic = New Scripting.Dictionary
    End If
    
    Set GetWindowTextToHWndDic = objDic
End Function


'''
'''
'''
Public Function GetTopHWndDTColOfFromClassNameAndWindowTextAndProcessFullPath(Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal vblnWindowTitleIgnoreCase As Boolean = False) As Collection

    Dim objDTCol As Collection

    Set objDTCol = GetTopHWndWindowTextAndClassNameAndItOfParentIfExistsDTCol(vstrClassNameRegExpPattern, vstrWindowTitleRegExpPattern, vstrProcessFullPathRegExpPattern, True, SortInfoByTopLevelHWndWindowText, vblnWindowTitleIgnoreCase)

    Set GetTopHWndDTColOfFromClassNameAndWindowTextAndProcessFullPath = objDTCol
End Function

'''
'''
'''
Public Function GetTopHWndWindowTextAndClassNameAndItOfParentIfExistsDTCol(Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal vblnAllowToAddHWndValueAndProcessIDAndPathToRowData As Boolean = False, _
        Optional ByVal venmTopLevelWindowHWndInfoSortType As TopLevelWindowHWndInfoSortType = TopLevelWindowHWndInfoSortType.SortInfoByTopLevelHWndClassName, _
        Optional ByVal vblnWindowTitleIgnoreCase As Boolean = False) As Collection

#If VBA7 Then
    Dim intHWnd As LongPtr, intExcelHWnd As LongPtr
    
    Dim intWindowHwnds() As LongPtr ' window hWnd buffer
    
    Dim intParentHwnd As LongPtr
#Else
    Dim intHWnd As Long, intExcelHWnd As Long
    
    Dim intWindowHwnds() As Long ' window hWnd buffer
    
    Dim intParentHwnd As Long
#End If
    Dim i As Long, strWindowTitle As String, strClassName As String, objDTCol As Collection, objRowCol As Collection
    
    Dim blnNeedToSearchProcessFullPath As Boolean
    
    Dim blnAllowToRow As Boolean, intProcessID As Long, strProcessModulePath As String


    If vstrClassNameRegExpPattern <> "" Then
    
        SetupRegExpForSearchingTopLevelHWnd mobjSearchingClassNameRegExp, vstrClassNameRegExpPattern, False, False
    End If

    If vstrWindowTitleRegExpPattern <> "" Then
    
        SetupRegExpForSearchingTopLevelHWnd mobjSearchingWindowTextRegExp, vstrWindowTitleRegExpPattern, vblnWindowTitleIgnoreCase, False
    End If

    blnNeedToSearchProcessFullPath = False
    
    If vstrProcessFullPathRegExpPattern <> "" Then
    
        ' Since search a path, the following IgnoreCase is true
    
        SetupRegExpForSearchingTopLevelHWnd mobjSearchingProcessFullPathRegExp, vstrProcessFullPathRegExpPattern, True, False
        
        blnNeedToSearchProcessFullPath = True
    End If

    ' initialize default buffer
    ReDim intWindowHwnds(0)

    EnumWindows AddressOf EnumTopLevelProc, intWindowHwnds

    Set objDTCol = New Collection

    For i = LBound(intWindowHwnds) To UBound(intWindowHwnds)
    
        If intWindowHwnds(i) > 0 Then
    
            strWindowTitle = GetWindowTextVBAString(intWindowHwnds(i))
            
            strClassName = GetClassNameVBAString(intWindowHwnds(i))
            
            If vblnAllowToAddHWndValueAndProcessIDAndPathToRowData Or blnNeedToSearchProcessFullPath Then
            
                strProcessModulePath = GetProcessFullPathVBAStringAndProcessID(intWindowHwnds(i), intProcessID)
            End If
            
            blnAllowToRow = True
            
            If vstrClassNameRegExpPattern <> "" Then
            
                If Not mobjSearchingClassNameRegExp.Test(strClassName) Then
                
                    blnAllowToRow = False
                End If
            End If
            
            If blnAllowToRow Then
            
                If vstrWindowTitleRegExpPattern <> "" Then
                
                    If Not mobjSearchingWindowTextRegExp.Test(strWindowTitle) Then
                    
                        blnAllowToRow = False
                    End If
                End If
            End If
            
            If blnAllowToRow Then
            
                If vstrProcessFullPathRegExpPattern <> "" Then
                
                    If Not mobjSearchingProcessFullPathRegExp.Test(strProcessModulePath) Then
                    
                        blnAllowToRow = False
                    End If
                End If
            End If
            
            If blnAllowToRow Then
            
                Set objRowCol = New Collection
            
                With objRowCol
                
                    If vblnAllowToAddHWndValueAndProcessIDAndPathToRowData Then
                    
                        .Add intWindowHwnds(i)
                        
                        .Add intProcessID
                    End If
                    
                    .Add strWindowTitle
                    
                    .Add strClassName
                    
                    If vblnAllowToAddHWndValueAndProcessIDAndPathToRowData Then
                    
                        .Add strProcessModulePath
                    End If
                End With
                'Debug.Print strWindowTitle & ", " & strClassName
                
                intParentHwnd = GetParent(intWindowHwnds(i))
                
                If intParentHwnd <> 0 Then
                
    '                Debug.Print "Parent window title: " & GetWindowTextVBAString(intParentHwnd)
    '                Debug.Print "Parent class name: " & GetClassNameVBAString(intParentHwnd)
                    
                    With objRowCol
                    
                        If vblnAllowToAddHWndValueAndProcessIDAndPathToRowData Then
                        
                            .Add intParentHwnd
                        End If
                    
                        .Add GetWindowTextVBAString(intParentHwnd)
                        
                        .Add GetClassNameVBAString(intParentHwnd)
                    End With
                Else
                    With objRowCol
                    
                        If vblnAllowToAddHWndValueAndProcessIDAndPathToRowData Then
                        
                            .Add ""
                        End If
                    
                        .Add "": .Add ""
                    End With
                End If
                
                objDTCol.Add objRowCol
            End If
        End If
    Next
    
    
    If venmTopLevelWindowHWndInfoSortType <> NoSortAboutTopLevelHWndInfo Then
    
        If vblnAllowToAddHWndValueAndProcessIDAndPathToRowData Then
        
            Select Case venmTopLevelWindowHWndInfoSortType
            
                Case TopLevelWindowHWndInfoSortType.SortInfoByTopLevelHWndValue
            
                    SortDataTableCollectionBySpecifiedOneColumn objDTCol, 1
            
            
                Case TopLevelWindowHWndInfoSortType.SortInfoByTopLevelHWndWindowText
            
                    SortDataTableCollectionBySpecifiedOneColumn objDTCol, 3
            
                Case TopLevelWindowHWndInfoSortType.SortInfoByTopLevelHWndClassName
        
                    SortDataTableCollectionBySpecifiedOneColumn objDTCol, 4
            End Select
        Else
        
            Select Case venmTopLevelWindowHWndInfoSortType
            
                Case TopLevelWindowHWndInfoSortType.SortInfoByTopLevelHWndWindowText
            
                    SortDataTableCollectionBySpecifiedOneColumn objDTCol, 1
            
                Case TopLevelWindowHWndInfoSortType.SortInfoByTopLevelHWndClassName
        
                    SortDataTableCollectionBySpecifiedOneColumn objDTCol, 2
            End Select
        End If
    End If
    
    Set GetTopHWndWindowTextAndClassNameAndItOfParentIfExistsDTCol = objDTCol
End Function


'''
''' This is tested on only Excel. this uses Windows API
'''
Public Function IsModelessUserFormExists(ByVal vstrWindowCaption As String) As Boolean

    Dim blnIsExisted As Boolean
    
#If VBA7 Then
    Dim intDummyHwnd() As LongPtr
#Else
    Dim intDummyHwnd() As Long
#End If
    ReDim intDummyHwnd(0)

    blnIsExisted = False

    mintFoundHwnd = 0
    
    mstrSpecifiedCaption = vstrWindowCaption


    EnumWindows AddressOf EnumMSUserFormTopLevelProc, intDummyHwnd

    If mintFoundHwnd <> 0 Then
    
        blnIsExisted = True
    End If

    IsModelessUserFormExists = blnIsExisted
End Function

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** About searching child Window hWnd recursively
'**---------------------------------------------
'''
''' recursive-call
'''
Private Sub msubLoadChildHWndCol(ByRef robjChildHWndDTCol As Collection, _
        ByRef robjParentHWndRowCol As Collection, _
        ByVal intHierarchicalOrder As Long, _
        ByRef rintMaxHierarchicalOrder As Long, _
        Optional ByVal vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively As Boolean = False)

    Dim varParentHWndArray() As Variant, i As Long, j As Long
    
    Dim objHWndRowCol As Collection
    
#If VBA7 Then

    Dim intCurrentParentHWnd As LongPtr, intChildWindowHwnds() As LongPtr
#Else
    Dim intCurrentParentHWnd As Long, intChildWindowHwnds() As Long
#End If
    
    With robjParentHWndRowCol
    
        ReDim varParentHWndArray(robjParentHWndRowCol.Count - 1)
    
        For i = 1 To .Count
        
            varParentHWndArray(i - 1) = .Item(i)
            
            If i = .Count Then
            
                intCurrentParentHWnd = .Item(i)
            End If
        Next
    End With

    ' initialize default buffer
    ReDim intChildWindowHwnds(0)

    EnumChildWindows intCurrentParentHWnd, AddressOf EnumChildProc, intChildWindowHwnds

    For i = LBound(intChildWindowHwnds) To UBound(intChildWindowHwnds)
    
        If intChildWindowHwnds(i) > 0 Then
    
            If rintMaxHierarchicalOrder < intHierarchicalOrder Then
            
                rintMaxHierarchicalOrder = intHierarchicalOrder
            End If
    
            Set objHWndRowCol = New Collection
            
            With objHWndRowCol
                
                For j = 0 To robjParentHWndRowCol.Count - 1
            
                    .Add varParentHWndArray(j)
                Next
            
                .Add intChildWindowHwnds(i)
            End With
            
            
            Select Case True
            
                Case Not vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively, "" <> GetWindowTextVBAString(intChildWindowHwnds(i))
            
                    robjChildHWndDTCol.Add objHWndRowCol
            End Select
            
            ' recursive-call
            msubLoadChildHWndCol robjChildHWndDTCol, objHWndRowCol, intHierarchicalOrder + 1, rintMaxHierarchicalOrder, vblnAllowToCutNoWindowTextHWndWhenFindingChildHWndRecursively
        End If
    Next
End Sub


'**---------------------------------------------
'** About searching hWnd of VBA MSForms.UserForm
'**---------------------------------------------
'''
'''
'''
#If VBA7 Then

Private Function EnumMSUserFormTopLevelProc(ByVal hwnd As LongPtr) As Long

#Else

Private Function EnumMSUserFormTopLevelProc(ByVal hwnd As Long) As Long
#End If

    Dim strClassName As String, strWindowTitle As String, blnContinueToEnumerate As Boolean

    blnContinueToEnumerate = True

    strClassName = GetClassNameVBAString(hwnd)
    
    If StrComp(strClassName, MSOfficeUserFormClassName) = 0 Then
    
        If mstrSpecifiedCaption <> "" Then
    
            strWindowTitle = GetWindowTextVBAString(hwnd)
    
            If StrComp(strWindowTitle, mstrSpecifiedCaption) = 0 Then
            
                mintFoundHwnd = hwnd
                
                blnContinueToEnumerate = False
            End If
        End If
    End If
    
    EnumMSUserFormTopLevelProc = blnContinueToEnumerate ' continue to enumerate
End Function


'''
'''
'''
Private Function GetExceptionWindowTitleKeysDic() As Scripting.Dictionary

    Dim objExceptTitleKeys As Scripting.Dictionary
    
    Set objExceptTitleKeys = New Scripting.Dictionary
    
    With objExceptTitleKeys
    
        .Add "Default IME", 0
        .Add "MSCTFIME UI", 0
        .Add "Program Manager", 0
        .Add "Task Host Window", 0
        .Add "Task Host", 0
        .Add "ccSvcHst", 0
        .Add "NvSvc", 0
        .Add "WatchThread", 0
        .Add "Network Flyout", 0
        .Add "HardwareMonitorWindow", 0
    End With

    Set GetExceptionWindowTitleKeysDic = objExceptTitleKeys
End Function



'**---------------------------------------------
'** call back functions for both EnumWindows and EnumChildWindows
'**---------------------------------------------
'''
''' testing function - call back function - enumerate child window
'''
#If VBA7 Then

Private Function EnumChildProc(ByVal hwnd As LongPtr, ByRef lParam() As LongPtr) As Long
#Else

Private Function EnumChildProc(ByVal hwnd As Long, ByRef lParam() As Long) As Long
#End If

    ReDim Preserve lParam(UBound(lParam) + 1) ' expand the buffer which stores new child hWnd
    
    lParam(UBound(lParam)) = hwnd
    
    EnumChildProc = True ' continue to enumerate
End Function

'''
''' testing function
'''
#If VBA7 Then

Private Function EnumTopLevelProc(ByVal hwnd As LongPtr, ByRef lParam() As LongPtr) As Long
#Else

Private Function EnumTopLevelProc(ByVal hwnd As Long, ByRef lParam() As Long) As Long
#End If

    ReDim Preserve lParam(UBound(lParam) + 1) ' expand the buffer which stores new top-level hWnd
    
    lParam(UBound(lParam)) = hwnd
    
    EnumTopLevelProc = True ' continue to enumerate
End Function
