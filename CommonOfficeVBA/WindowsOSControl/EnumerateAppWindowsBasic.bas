Attribute VB_Name = "EnumerateAppWindowsBasic"
'
'   Wrapper operations for both GetClassName and GetWindowText
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 19/Jan/2023    Kalmclaeyd Tarclanus    Modified
'

Option Explicit

'///////////////////////////////////////////////
'/// Windows API Declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function EnumWindows Lib "user32.dll" (ByVal lpEnumFunc As LongPtr, ByVal lParam As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function EnumChildWindows Lib "user32.dll" (ByVal hWndParent As LongPtr, ByVal lpEnumFunc As LongPtr, ByVal lParam As LongPtr) As LongPtr
    
    
    Private Declare PtrSafe Function GetClassName Lib "user32.dll" Alias "GetClassNameA" (ByVal hwnd As LongPtr, ByVal lpClassName As String, ByVal nMaxCount As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function GetWindowText Lib "user32.dll" Alias "GetWindowTextA" (ByVal hwnd As LongPtr, ByVal lpString As String, ByVal nMaxCount As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function GetModuleFileName Lib "kernel32" Alias "GetModuleFileNameA" (ByVal hModule As LongPtr, ByVal lpFileName As String, ByVal nSize As Long) As Long
    
    Private Declare PtrSafe Function QueryFullProcessImageName Lib "kernel32" Alias "QueryFullProcessImageNameA" (ByVal hProcess As LongPtr, ByVal dwFlags As Long, ByVal lpExeName As String, ByRef lpdwSize As Long) As Long
    
    
    Private Declare PtrSafe Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As LongPtr, lpdwProcessId As Long) As Long
    
    Private Declare PtrSafe Function GetCurrentProcessId Lib "kernel32" () As Long
    
    
    Private Declare PtrSafe Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As LongPtr
    
    Private Declare PtrSafe Function CloseHandle Lib "kernel32" (ByVal hObject As LongPtr) As Long
#Else
    Private Declare Function EnumWindows Lib "user32.dll" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
    
    Private Declare Function EnumChildWindows Lib "user32.dll" (ByVal hWndParent As Long, ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
    
    
    Private Declare Function GetClassName Lib "user32.dll" Alias "GetClassNameA" (ByVal hWnd As Long, ByVal lpClassName As String, ByVal nMaxCount As Long) As Long
    
    Private Declare Function GetWindowText Lib "user32.dll" Alias "GetWindowTextA" (ByVal hWnd As Long, ByVal lpString As String, ByVal nMaxCount As Long) As Long
    
    Private Declare Function GetModuleFileName Lib "kernel32" Alias "GetModuleFileNameA" (ByVal hModule As Long, ByVal lpFileName As String, ByVal nSize As Long) As Long
    
    Private Declare Function QueryFullProcessImageName Lib "kernel32" Alias "QueryFullProcessImageNameA" (ByVal hProcess As Long, ByVal dwFlags As Long, ByVal lpExeName As String, ByRef lpdwSize As Long) As Long
    
    
    Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, lpdwProcessId As Long) As Long
    
    Private Declare Function GetCurrentProcessId Lib "kernel32" () As Long
    
    
    Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
    
    Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
#End If

'**---------------------------------------------
'** class name of Windows programs
'**---------------------------------------------
Private Const WINDOW_CLASS_EXCEL = "XLMAIN" ' Excel Application
Private Const WINDOW_CLASS_VBE = "wndclass_desked_gsk"  ' Visual Basic Editor - VBIDE
Private Const WINDOW_CALSS_EXPLORER = "CabinetWClass"

Private Const WINDOW_CLASS_MS_WORD = "OpusApp" ' Word Application

Private Const CHILD_CLASS_EXCELWINDOW = "EXCEL7"
Private Const CHILD_CLASS_VBE = "VbaWindow"

'**---------------------------------------------
'** OpenProcess flag values - Process Access Security
'**---------------------------------------------
Private Const PROCESS_ALL_ACCESS As Long = &H1F0FFF ' (STANDARD_RIGHTS_REQUIRED (0x000F0000L) |SYNCHRONIZE (0x00100000L) |0xFFFF)

Private Const PROCESS_QUERY_INFORMATION As Long = &H400

Private Const PROCESS_QUERY_LIMITED_INFORMATION As Long = &H1000

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
#If HAS_REF Then
    Private mobjFoundParentHWndToLParamDic As Scripting.Dictionary
    
    Private mobjFoundChildHWndToParentHWndDic As Scripting.Dictionary
#Else
    Private mobjFoundParentHWndToLParamDic As Object  'Dictionary
    
    Private mobjFoundChildHWndToParentHWndDic As Object   'Dictionary
#End If

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About getting various information from each hWnd
'**---------------------------------------------
'''
''' get the ClassName of the hWnd
'''
Public Function GetClassNameVBAString(ByRef rvarHWnd As Variant) As String

#If VBA7 Then
    Dim intReturn As LongPtr
#Else
    Dim intReturn As Long
#End If
    Dim strClassBuff As String * 128
    
    intReturn = GetClassName(rvarHWnd, strClassBuff, Len(strClassBuff))
    
    GetClassNameVBAString = Left(strClassBuff, InStr(strClassBuff, vbNullChar) - 1)
End Function

'''
''' get the Window title text of the hWnd
'''
Public Function GetWindowTextVBAString(ByRef rvarHWnd As Variant) As String

#If VBA7 Then
    Dim intReturn As LongPtr
#Else
    Dim intReturn As Long
#End If

    Dim strTextBuff As String * 516
    
    intReturn = GetWindowText(rvarHWnd, strTextBuff, Len(strTextBuff))
    
    GetWindowTextVBAString = Left(strTextBuff, InStr(strTextBuff, vbNullChar) - 1)
End Function

'''
'''
'''
Public Function GetProcessFullPathVBAString(ByRef rvarHWnd As Variant) As String

    Dim intProcessIDDummy As Long
    
    GetProcessFullPathVBAString = GetProcessFullPathVBAStringAndProcessID(rvarHWnd, intProcessIDDummy)
End Function

'''
'''
'''
Public Function GetProcessFullPathVBAStringAndProcessID(ByRef rvarHWnd As Variant, ByRef rintProcessID As Long) As String

    Dim intReturn As Long, strPath As String
    
    Dim strTextBuff As String * 2048

    Dim intProcessID As Long, intThreadID As Long

#If VBA7 Then
    Dim intHWnd As LongPtr, intProcessHandle As LongPtr
#Else
    Dim intHWnd As Long, intProcessHandle As Long
#End If
    
    strPath = ""

    intHWnd = rvarHWnd

    intThreadID = GetWindowThreadProcessId(intHWnd, intProcessID)

    intProcessHandle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, 1, intProcessID)
    
    'intProcessHandle = OpenProcess(PROCESS_QUERY_INFORMATION, 1, intProcessID)

    If intProcessHandle <> 0 Then
    
        intReturn = QueryFullProcessImageName(intProcessHandle, 0, strTextBuff, Len(strTextBuff))
        
        If intReturn = 0 Then   ' Error occurs
        
            Debug.Print "When QueryFullProcessImageName is executed, " & GetDllErrorMessage(Err.LastDllError)
        Else
            'Debug.Print Left(strTextBuff, InStr(strTextBuff, vbNullChar) - 1)
            
            strPath = Left(strTextBuff, InStr(strTextBuff, vbNullChar) - 1)
        End If
        
        CloseHandle intProcessHandle
    End If

    ' return
    
    rintProcessID = intProcessID

    GetProcessFullPathVBAStringAndProcessID = strPath
End Function


'**---------------------------------------------
'** About process ID and hWnd
'**---------------------------------------------
'''
'''
'''
Public Function GetProcessIDToTopLevelHWndDic(ByVal vstrParentClassName As String) As Scripting.Dictionary

    Dim varHWndArray As Variant, varHWnd As Variant, objDic As Scripting.Dictionary
    Dim intProcessID As Long, intThreadID As Long

#If VBA7 Then

    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    Set objDic = New Scripting.Dictionary

    varHWndArray = EnumFindWindows(vstrParentClassName)

    If LBound(varHWndArray) <= UBound(varHWndArray) Then
    
        For Each varHWnd In varHWndArray
    
            intHWnd = varHWnd
            
            If intHWnd > 0 Then
            
                intThreadID = GetWindowThreadProcessId(intHWnd, intProcessID)
            
                If intProcessID > 0 Then
            
                    With objDic
            
                        If Not .Exists(intProcessID) Then
                    
                            .Add intProcessID, "" & intHWnd
                        End If
                    End With
                End If
            End If
        Next
    End If

    Set GetProcessIDToTopLevelHWndDic = objDic
End Function

'''
'''
'''
Public Function GetTopLevelHWndToProcessIDDic(ByVal vstrParentClassName As String) As Scripting.Dictionary

    Dim varHWndArray As Variant, varHWnd As Variant, objDic As Scripting.Dictionary
    Dim intProcessID As Long, intThreadID As Long, strHWnd As String

#If VBA7 Then

    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    Set objDic = New Scripting.Dictionary

    varHWndArray = EnumFindWindows(vstrParentClassName)

    If LBound(varHWndArray) <= UBound(varHWndArray) Then
    
        For Each varHWnd In varHWndArray
    
            intHWnd = varHWnd
            
            If intHWnd > 0 Then
            
                intThreadID = GetWindowThreadProcessId(intHWnd, intProcessID)
            
                strHWnd = "" & intHWnd
            
                If intProcessID > 0 Then
            
                    With objDic
            
                        If Not .Exists(strHWnd) Then
                    
                            .Add strHWnd, intProcessID
                        End If
                    End With
                End If
            End If
        Next
    End If

    Set GetTopLevelHWndToProcessIDDic = objDic
End Function



'**---------------------------------------------
'** About enumerate current hWnd
'**---------------------------------------------
'''
''' Get top-level Window handle list, which has no parent window
'''
''' <Argument>vstrParentClassName: parent ClassName by filtering Like operator, this is optional.</Argument>
''' <Argument>vstrParentWindowText: parent Window title-bar text by filtering Like operator, this is optional.</Argument>
''' <Return>array of Variant/Variant(0 to #) - array of the parent Window handles</Return>
'''
''' <Attention>
'''  If the both of the parent ClassName and parent the Window title-bar text are specifed, the logical product of the both string evaluations is operated
'''  The both arguments are either null string or omitted, this returns to False.
'''  If you need for all parent Window handles, specify "*".
''' </Attention>
Public Function EnumFindWindows(Optional vstrParentClassName As Variant, Optional vstrParentWindowText As Variant) As Variant
    
#If VBA7 Then
    Dim intReturn As LongPtr
#Else
    Dim intReturn As Long
#End If
    Dim varHWnd As Variant
    
#If HAS_REF Then
    Dim objDic As Scripting.Dictionary
    
    Set mobjFoundParentHWndToLParamDic = New Scripting.Dictionary: Set objDic = New Scripting.Dictionary
#Else
    Dim objDic As Object
    
    Set mobjFoundParentHWndToLParamDic = CreateObject("Scripting.Dictionary"): Set objDic = CreateObject("Scripting.Dictionary")
#End If
    
    ' set the enumerated window-handles into mobjFoundParentHWndToLParamDic
    intReturn = EnumWindows(AddressOf CallbackEnumWindowsProc, ByVal 0&)
    
    
    With mobjFoundParentHWndToLParamDic
    
        For Each varHWnd In .Keys
        
            If IsEitherOrBothOfClassNameAndWindowTextDetected(varHWnd, vstrParentClassName, vstrParentWindowText) Then
            
                objDic.Add "" & varHWnd, .Item("" & varHWnd)
                
    '            Debug.Print hWnd, GetClassNameVBAString(hWnd), GetWindowTextVBAString(hWnd)
            End If
        Next
    End With
    
    Set mobjFoundParentHWndToLParamDic = objDic
    
    'EnumFindWindows = mobjFoundParentHWndToLParamDic.Values
    
    EnumFindWindows = mobjFoundParentHWndToLParamDic.Keys
End Function

'''
''' Get child Window handle list of the specified parent Window
'''
''' <Argument>vstrParentClassName: parent ClassName by filtering Like operator, this is optional.</Argument>
''' <Argument>vstrParentWindowText: parent Window title-bar text by filtering Like operator, this is optional.</Argument>
''' <Argument>vstrChildClassName: child ClassName by filtering Like operator, this is optional.</Argument>
''' <Argument>vstrChildWindowText: child Window title-bar text by filtering Like operator, this is optional.</Argument>
''' <Return>array of Variant/Variant(0 to #): array of window handles such as the child or the grandchild</Return>
'''
''' <Attention>
'''  If the both of the parent ClassName and parent the Window title-bar text are specifed, the logical product of the both string evaluations is operated
'''  The both arguments are either null string or omitted, this returns to False.
'''  If you need for all parent Window handles, specify "*".
''' </Attention>
Public Function EnumFindChildWindows(Optional vstrParentClassName As Variant, _
        Optional vstrParentWindowText As Variant, _
        Optional vstrChildClassName As Variant, _
        Optional vstrChildWindowText As Variant) As Variant

    ' cache clear
#If HAS_REF Then
    Set mobjFoundParentHWndToLParamDic = New Scripting.Dictionary
    
    Set mobjFoundChildHWndToParentHWndDic = New Scripting.Dictionary
#Else
    Set mobjFoundParentHWndToLParamDic = CreateObject("Scripting.Dictionary")
    
    Set mobjFoundChildHWndToParentHWndDic = CreateObject("Scripting.Dictionary")
#End If

#If VBA7 Then
    Dim intReturn As LongPtr
#Else
    Dim intReturn As Long
#End If

    Dim objDic As Object, varHWnd As Variant
    
    ' Enumerate parent Window handles
    ' Store the results with mobjFoundParentHWndToLParamDic
    intReturn = EnumWindows(AddressOf CallbackEnumWindowsProc, ByVal 0&)
    
    ' filter the found windows with either parent ClassName or the parent Window title-bar text
    Set objDic = CreateObject("Scripting.Dictionary")
    
    With mobjFoundParentHWndToLParamDic
    
        For Each varHWnd In .Keys
        
            If IsEitherOrBothOfClassNameAndWindowTextDetected(varHWnd, vstrParentClassName, vstrParentWindowText) Then
                
                objDic.Add "" & varHWnd, .Item("" & varHWnd)
                
    '            Debug.Print hWnd, GetClassNameVBAString(hWnd), GetWindowTextVBAString(hWnd)
            End If
        Next
    End With
    
    Set mobjFoundParentHWndToLParamDic = objDic
    
    ' enumerate child windows
    With mobjFoundParentHWndToLParamDic
    
        For Each varHWnd In .Keys
        
            ' set lparam to parent hWnd
            intReturn = EnumChildWindows(varHWnd, AddressOf CallbackEnumChildWindowsProc, varHWnd)
        Next
    End With
    
    ' filter the found child windows with either child ClassName or the child Window title-bar text
    Set objDic = CreateObject("Scripting.Dictionary")
    
    With mobjFoundChildHWndToParentHWndDic
    
        For Each varHWnd In .Keys
            
            If IsEitherOrBothOfClassNameAndWindowTextDetected(varHWnd, vstrChildClassName, vstrChildWindowText) Then
                
                objDic.Add "" & varHWnd, .Item("" & varHWnd)
    '            Debug.Print hWnd, GetClassNameVBAString(hWnd), GetWindowTextVBAString(hWnd)
            End If
        Next
    End With
    
    Set mobjFoundChildHWndToParentHWndDic = objDic
    
    EnumFindChildWindows = mobjFoundChildHWndToParentHWndDic.Keys
End Function



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
#If VBA7 Then
    '''
    ''' Enumerate root windows, call-back function
    '''
    Private Function CallbackEnumWindowsProc(ByVal hwnd As LongPtr, ByVal lParam As LongPtr) As LongPtr
    
        ' lParam is second argument of EnumWindows()
    
        mobjFoundParentHWndToLParamDic.Add "" & hwnd, lParam
        
        CallbackEnumWindowsProc = True
    End Function
    
    '''
    ''' Enumerate child windows, call-back function
    '''
    Private Function CallbackEnumChildWindowsProc(ByVal hWndChild As LongPtr, ByVal lParam As LongPtr) As LongPtr
        
        mobjFoundChildHWndToParentHWndDic.Add "" & hWndChild, lParam
        
        CallbackEnumChildWindowsProc = True
    End Function
#Else

    Private Function CallbackEnumWindowsProc(ByVal hwnd As Long, ByVal lParam As Long) As Long
    
        mobjFoundParentHWndToLParamDic.Add "" & hwnd, lParam
        
        CallbackEnumWindowsProc = True
    End Function
    
    Private Function CallbackEnumChildWindowsProc(ByVal hWndChild As Long, ByVal lParam As Long) As Long
        
        mobjFoundChildHWndToParentHWndDic.Add "" & hWndChild, lParam
        
        CallbackEnumChildWindowsProc = True
    End Function
#End If


'''
''' Filter the Window handle hWnd by either the ClassName Like operating or th Window bar text Like operating , or the logical product of the both texts
'''
''' <Argument>hWnd:        Window handle</Argument>
''' <Argument>rstrClassName:  ClassName for filtering Like operating. this is optional.</Argument>
''' <Argument>rstrWindowText:  Window bar text for filtering Like operating. this is optional.</Argument>
''' <Return>Boolean: is the condition satisfied</Return>
'''
''' <Attention>
'''  If the both of the ClassName and the Window title-bar text are specifed, the logical product of the both string evaluations is operated
'''  The both arguments are either null string or omitted, this returns to False.
''' </Attention>
'''
Private Function IsEitherOrBothOfClassNameAndWindowTextDetected(ByRef rvarHWnd As Variant, Optional rstrClassName As Variant, Optional rstrWindowText As Variant) As Boolean
    
    Dim blnIsClassNameDetected As Boolean, blnIsWindowTextDetected As Boolean
    
    blnIsClassNameDetected = False
    blnIsWindowTextDetected = False
    
    IsEitherOrBothOfClassNameAndWindowTextDetected = False
    
    If Not IsMissing(rstrClassName) Then
    
        If rstrClassName <> "" Then
            
            blnIsClassNameDetected = (GetClassNameVBAString(rvarHWnd) Like rstrClassName)
        End If
    End If
    If Not IsMissing(rstrWindowText) Then
    
        If rstrWindowText <> "" Then
            
            blnIsWindowTextDetected = (GetWindowTextVBAString(rvarHWnd) Like rstrWindowText)
        End If
    End If
    
    
    If Not IsMissing(rstrClassName) And Not IsMissing(rstrWindowText) Then
    
        ' Both ClassName and WindowText are detected
    
        IsEitherOrBothOfClassNameAndWindowTextDetected = blnIsClassNameDetected And blnIsWindowTextDetected
        
    ElseIf Not IsMissing(rstrClassName) Then
    
        ' Only ClassName is detected
    
        IsEitherOrBothOfClassNameAndWindowTextDetected = blnIsClassNameDetected
        
    ElseIf Not IsMissing(rstrWindowText) Then
    
        ' Only WindowText is detected
    
        IsEitherOrBothOfClassNameAndWindowTextDetected = blnIsWindowTextDetected
    End If
End Function


'''
'''
'''
Private Function FindTopLevelWindowsFilteredBy(Optional vstrParentClassName As Variant, Optional vstrParentWindowText As Variant, Optional vstrChildClassName As Variant, Optional vstrChildWindowText As Variant) As Scripting.Dictionary

    ' cache clear
#If HAS_REF Then
    Set mobjFoundParentHWndToLParamDic = New Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary
#Else
    Set mobjFoundParentHWndToLParamDic = CreateObject("Scripting.Dictionary")
    
    Dim objDic As Object
#End If

#If VBA7 Then
    Dim intReturn As LongPtr
#Else
    Dim intReturn As Long
#End If

    Dim varHWnd As Variant
    
    ' enumerate parent windows, the enumerated results are put into mobjFoundParentHWndToLParamDic
    intReturn = EnumWindows(AddressOf CallbackEnumWindowsProc, ByVal 0&)
    
    Set objDic = CreateObject("Scripting.Dictionary")
    
    ' The lparam is 0&, when EnumWindows is used, the second argument becomes lparam
    With mobjFoundParentHWndToLParamDic
        
        For Each varHWnd In .Keys
        
            ' filtering by ClassName and WindowText
            If IsEitherOrBothOfClassNameAndWindowTextDetected(varHWnd, vstrParentClassName, vstrParentWindowText) Then
                
                objDic.Add "" & varHWnd, .Item("" & varHWnd)
                
    '            Debug.Print hWnd, GetClassNameVBAString(hWnd), GetWindowTextVBAString(hWnd)
            End If
        Next
    End With
    
    Set mobjFoundParentHWndToLParamDic = objDic

    Set FindTopLevelWindowsFilteredBy = objDic
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetModuleFileName()

#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    intHWnd = GetThisApplicationHWnd()

    Debug.Print GetProcessFullPathVBAString(intHWnd)
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetProcessIDToTopLevelHWndDicAboutMsExcel()

    Dim objDic As Scripting.Dictionary

    'Set objDic = GetProcessIDToTopLevelHWndDic(WINDOW_CLASS_EXCEL)

    Set objDic = GetTopLevelHWndToProcessIDDic(WINDOW_CLASS_EXCEL)
    
    
    Debug.Print "HWnd(Ms Excel)", "ProcessID"
    
    DebugDic objDic
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetProcessIDToTopLevelHWndDicAboutMsWord()

    Dim objDic As Scripting.Dictionary

'    Set objDic = GetProcessIDToTopLevelHWndDic(WINDOW_CLASS_MS_WORD)

    Set objDic = GetTopLevelHWndToProcessIDDic(WINDOW_CLASS_MS_WORD)


    Debug.Print "HWnd(Ms Word)", "ProcessID"
    
    DebugDic objDic
End Sub


'''
'''
'''
Private Sub msubSanityTestToChildWindowsOfExcel()

    Dim varHWndArray As Variant, varHWnd As Variant

#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If
    'Dim varSafeExcelWindowArray As Variant
    
    varHWndArray = EnumFindChildWindows(vstrParentClassName:=WINDOW_CLASS_EXCEL, vstrChildClassName:=CHILD_CLASS_EXCELWINDOW)
    
    'varHWndArray = EnumFindChildWindows(vstrParentClassName:=WINDOW_CLASS_EXCEL)

    For Each varHWnd In mobjFoundChildHWndToParentHWndDic.Keys
    
        intHWnd = varHWnd
        
        Debug.Print GetWindowTextVBAString(intHWnd)
    Next
End Sub


