Attribute VB_Name = "EnumerateAppWindows"
'
'   List up Top-level hWnd windows at the current Windows OS
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Jan, 19/Jan/2023    Kalmclaeyd Tarclanus    Added GetNotepadHwndToWindowCaptionDic operation
'

Option Explicit

'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function EnumWindows Lib "user32.dll" (ByVal lpEnumFunc As LongPtr, ByVal lParam As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function EnumChildWindows Lib "user32.dll" (ByVal hWndParent As LongPtr, ByVal lpEnumFunc As LongPtr, ByVal lParam As LongPtr) As LongPtr
    
    
    Private Declare PtrSafe Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As LongPtr, ByVal Msg As LongPtr, ByVal wParam As LongPtr, lParam As Any) As LongPtr
    
    Private Declare PtrSafe Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As LongPtr
    
    Private Declare PtrSafe Function ObjectFromLresult Lib "oleacc" (ByVal lResult As LongPtr, riid As Any, ByVal wParam As LongPtr, ppvObject As Any) As LongPtr
    
    Private Declare PtrSafe Function IsWindow Lib "user32" (ByVal hwnd As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function IsWindowVisible Lib "user32" (ByVal hwnd As LongPtr) As Long
    
    
    Private Declare PtrSafe Function SetForegroundWindow Lib "user32" (ByVal hwnd As LongPtr) As Long

    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
    
    
    Private Type PROCESS_INFORMATION
    
        hProcess As LongPtr
        
        hThread As LongPtr
        
        dwProcessId As Long
        
        dwThreadId As Long
    End Type
    
#Else
    Private Declare Function EnumWindows Lib "user32.dll" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
    
    Private Declare Function EnumChildWindows Lib "user32.dll" (ByVal hWndParent As Long, ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
    
    
    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, lParam As Any) As Long
    
    Private Declare Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As Long
    
    Private Declare Function ObjectFromLresult Lib "oleacc" (ByVal lResult As Long, riid As Any, ByVal wParam As Long, ppvObject As Any) As Long
    
    Private Declare Function IsWindow Lib "user32" (ByVal hWnd As Long) As Long
    
    Private Declare Function IsWindowVisible Lib "user32" (ByVal hWnd As Long) As Long
    

    Private Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long

    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
    
    
    Private Type PROCESS_INFORMATION

        hProcess As Long
        
        hThread As Long
        
        dwProcessId As Long
        
        dwThreadId As Long
    End Type
#End If


'**---------------------------------------------
'** About controling foregrownd window
'**---------------------------------------------
'''
''' Open new window after it is closed if it exists
'''
Public Enum PreparingForegroundNewWindowType

    ForegroundNewWindowIsA_SakuraEditor_Type
    
    ForegroundNewWindowIsA_Notepad_Type
    
    ForegroundNewWindowIsA_Wordpad_Type     ' Wordpad is to be abolished from Windows11 24H2
End Enum

'''
''' Find existed window
'''
Public Enum ActivatingForegroundExistedWindowType


    ActivateExistedWindowIsA_Notepad_Type   ' In Japanese, メモ帳

    ActivateExistedWindowIsA_Wordpad_Type   ' In Japanese, ワードパッド

    ActivateExistedWindowIsA_MsWord_Type
End Enum


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Filtering by each window title text
'**---------------------------------------------
Private mobjFoundHWndToWindowCaptionDic As Scripting.Dictionary ' cache

Private mobjCacheWindowCaptionToHWndDic As Scripting.Dictionary

Private mobjSearchingRegExp As VBScript_RegExp_55.RegExp

'**---------------------------------------------
'** Filtering by each class name
'**---------------------------------------------
Private Const WINDOW_CLASS_MS_WORD = "OpusApp" ' Word Application

Private Const mstrNotepadClassName As String = "Notepad"

Private Const mstrCommandPromptWindowClassName As String = "CASCADIA_HOSTING_WINDOW_CLASS,ConsoleWindowClass" ' cmd.exe

Private Const mstrVBEWindowClassName = "wndclass_desked_gsk"  ' Visual Basic Editor


Private mstrCurrentFilteringClassNameDelimitedByComma As String

Private mobjCacheCurrentClassNameHwndToWindowCaptionDic As Scripting.Dictionary  ' Dictionary(Of LongPtr[hWnd], Of String[Window text])

'**---------------------------------------------
'** List visible all up, which has Window-title text
'**---------------------------------------------
Private mobjWindowTextAllDTCol As Collection

Private mobjChildWindowTextAllDTCol As Collection


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** specify a target Keystroke window with creating new Windows process
'**---------------------------------------------
'''
'''
'''
Public Function IsAForegroundNewWindowPreparedForKeystroke(ByVal venmPreparingForegroundWindowType As PreparingForegroundNewWindowType) As Boolean

    Dim blnIsPrepared As Boolean: blnIsPrepared = False

    Select Case venmPreparingForegroundWindowType
    
        Case PreparingForegroundNewWindowType.ForegroundNewWindowIsA_SakuraEditor_Type
        
            blnIsPrepared = Is_SakuraEditor_PreparedForKeystrokeInputs()
            
        Case PreparingForegroundNewWindowType.ForegroundNewWindowIsA_Notepad_Type
        
            blnIsPrepared = Is_Notepad_PreparedForKeystrokeInputs()
        
        Case PreparingForegroundNewWindowType.ForegroundNewWindowIsA_Wordpad_Type   ' Wordpad is to be abolished from Windows11 24H2
        
            blnIsPrepared = Is_Wordpad_PreparedForKeystrokeInputs()
    End Select
    
    IsAForegroundNewWindowPreparedForKeystroke = blnIsPrepared
End Function

'''
'''
'''
Public Function Is_SakuraEditor_PreparedForKeystrokeInputs() As Boolean

    Dim blnIsPrepared As Boolean: blnIsPrepared = False
    
    CloseAllSakuraEditorsIfItIsStarted

    OpenNewSakuraEditor
    
    blnIsPrepared = IsSpecifiedWindowSetForegroundByIncludingWindowCaptionPartOldType(" - sakura 2")
    
    Is_SakuraEditor_PreparedForKeystrokeInputs = blnIsPrepared
End Function

'''
'''
'''
Public Function Is_Notepad_PreparedForKeystrokeInputs() As Boolean

    Dim blnIsPrepared As Boolean: blnIsPrepared = False
    
    CloseWindowsNotepadIfItIsStarted

    On Error GoTo Err_Handler

    OpenNotepadAndSetItForegroundWindow
    
    blnIsPrepared = True
    
Err_Handler:
    
    Is_Notepad_PreparedForKeystrokeInputs = blnIsPrepared
End Function

'''
''' Wordpad is to be abolished from Windows11 24H2
'''
Public Function Is_Wordpad_PreparedForKeystrokeInputs() As Boolean

    Dim blnIsPrepared As Boolean: blnIsPrepared = False
    
#If VBA7 Then

    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    intHWnd = GetHWndWithOpeningWordpad()

    If intHWnd > 0 Then
    
        blnIsPrepared = True
    End If

    Is_Wordpad_PreparedForKeystrokeInputs = blnIsPrepared
End Function

'**---------------------------------------------
'** specify a target Keystroke window with finding an existed Windows process
'**---------------------------------------------
'''
'''
'''
#If VBA7 Then

Public Function GetHWndAfterActivatingExistedWindowForKeystroke(ByVal venmActivatingForegroundExistedWindowType As ActivatingForegroundExistedWindowType) As LongPtr

    Dim intHWnd As LongPtr
#Else

Public Function GetHWndAfterActivatingExistedWindowForKeystroke(ByVal venmActivatingForegroundExistedWindowType As ActivatingForegroundExistedWindowType) As Long

    Dim intHWnd As Long
#End If
    

    Select Case venmActivatingForegroundExistedWindowType
    
        Case ActivatingForegroundExistedWindowType.ActivateExistedWindowIsA_Notepad_Type
        
            intHWnd = GetHWndAfterActivatingExisted_Notepad_ForKeystrokeInputs()
            
        Case ActivatingForegroundExistedWindowType.ActivateExistedWindowIsA_Wordpad_Type    ' Wordpad is to be abolished from Windows11 24H2
        
            intHWnd = GetHWndAfterActivatingExisted_Wordpad_ForKeystrokeInputs()
        
        Case ActivatingForegroundExistedWindowType.ActivateExistedWindowIsA_MsWord_Type
        
            intHWnd = GetHWndAfterActivatingExisted_Word_ForKeystrokeInputs()
    End Select
    
    GetHWndAfterActivatingExistedWindowForKeystroke = intHWnd
End Function


'''
'''
'''
#If VBA7 Then
Public Function GetHWndAfterActivatingExisted_Notepad_ForKeystrokeInputs() As LongPtr

    Dim intHWnd As LongPtr
#Else
Public Function GetHWndAfterActivatingExisted_Notepad_ForKeystrokeInputs() As Long

    Dim intHWnd As Long
#End If

    intHWnd = GetHWndAfterExistedWindowIsSetForeground("Notepad", "", "Notepad.exe")
    
    GetHWndAfterActivatingExisted_Notepad_ForKeystrokeInputs = intHWnd
End Function

'''
'''
'''
#If VBA7 Then
Public Function GetHWndAfterActivatingExisted_Wordpad_ForKeystrokeInputs() As LongPtr

    Dim intHWnd As LongPtr
#Else
Public Function GetHWndAfterActivatingExisted_Wordpad_ForKeystrokeInputs() As Long

    Dim intHWnd As Long
#End If

    intHWnd = GetHWndAfterExistedWindowIsSetForeground("WordPadClass", "", "wordpad.exe")
    
    GetHWndAfterActivatingExisted_Wordpad_ForKeystrokeInputs = intHWnd
End Function

'''
'''
'''
#If VBA7 Then
Public Function GetHWndAfterActivatingExisted_Word_ForKeystrokeInputs() As LongPtr

    Dim intHWnd As LongPtr
#Else
Public Function GetHWndAfterActivatingExisted_Word_ForKeystrokeInputs() As Long

    Dim intHWnd As Long
#End If

    intHWnd = GetHWndAfterExistedWindowIsSetForeground("OpusApp", "Word", "WINWORD.EXE")
    
    GetHWndAfterActivatingExisted_Word_ForKeystrokeInputs = intHWnd
End Function

'**---------------------------------------------
'** Control specified window order
'**---------------------------------------------
'''
'''
'''
#If VBA7 Then
Public Function GetHWndAfterExistedWindowIsSetForeground(Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal vblnWindowTitleIgnoreCase As Boolean = False) As LongPtr

    Dim intHWnd As LongPtr
#Else
Public Function GetHWndAfterExistedWindowIsSetForeground(Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal vblnWindowTitleIgnoreCase As Boolean = False) As Long

    Dim intHWnd As Long
#End If

    Dim intRet As Long

    intHWnd = 0

    intHWnd = GetFirstTopHWndFromClassNameAndWindowTextAndProcessFullPath(vstrClassNameRegExpPattern, vstrWindowTitleRegExpPattern, vstrProcessFullPathRegExpPattern, vblnWindowTitleIgnoreCase)

    If intHWnd > 0 Then

        intRet = SetForegroundWindow(intHWnd)
                        
        Interaction.DoEvents
        
        Sleep 50
    End If

    GetHWndAfterExistedWindowIsSetForeground = intHWnd
End Function


'''
'''
'''
Public Function IsSpecifiedWindowSetForegroundByClassNameAndWindowTextAndProcessName(Optional ByVal vstrClassNameRegExpPattern As String = "", _
        Optional ByVal vstrWindowTitleRegExpPattern As String = "", _
        Optional ByVal vstrProcessFullPathRegExpPattern As String = "", _
        Optional ByVal vblnWindowTitleIgnoreCase As Boolean = False) As Boolean

    Dim varWindowCaption As Variant, intRet As Long, blnIsTheWindowFound As Boolean

#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    blnIsTheWindowFound = False

    intHWnd = GetFirstTopHWndFromClassNameAndWindowTextAndProcessFullPath(vstrClassNameRegExpPattern, vstrWindowTitleRegExpPattern, vstrProcessFullPathRegExpPattern, vblnWindowTitleIgnoreCase)

    If intHWnd > 0 Then
    
        intRet = SetForegroundWindow(intHWnd)
                        
        Interaction.DoEvents
        
        Sleep 50
        
        blnIsTheWindowFound = True
    End If
    
    IsSpecifiedWindowSetForegroundByClassNameAndWindowTextAndProcessName = blnIsTheWindowFound
End Function

'''
'''
'''
Public Function IsSpecifiedWindowSetForegroundByIncludingWindowCaptionPartOldType(ByVal vstrWindowCaption As String, _
        Optional ByVal vblnEnablingIgnoreCase As Boolean = False) As Boolean

    Dim varWindowCaption As Variant, intRet As Long, blnIsTheWindowFound As Boolean

#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    blnIsTheWindowFound = False

    With GetCurrentWindowCaptionToHWndDic(vstrWindowTextRegExpPattern:=vstrWindowCaption, vblnIgnoreCase:=vblnEnablingIgnoreCase)
    
        If .Count > 0 Then
    
            For Each varWindowCaption In .Keys
            
                intHWnd = .Item(varWindowCaption)
            
                intRet = SetForegroundWindow(intHWnd)
                        
                Interaction.DoEvents
                
                Sleep 50
                
                blnIsTheWindowFound = True
            Next
        End If
    End With
    
    IsSpecifiedWindowSetForegroundByIncludingWindowCaptionPartOldType = blnIsTheWindowFound
End Function

'**---------------------------------------------
'** About Windows HWnd detections
'**---------------------------------------------
'''
'''
'''
Public Sub FindCurrentProcessAndChildWindowsInfo(ByRef robjTopLevelHWndToWindowTextDic As Scripting.Dictionary, _
        ByRef robjChildWindowClassNameInfoCol As Collection, _
        ByVal vstrParentClassName As String)


#If VBA7 Then
    Dim intReturn As LongPtr, intHWnd As LongPtr
#Else
    Dim intReturn As Long, intHWnd As Long
#End If

    Dim objDic As Scripting.Dictionary, varHWnd As Variant, objCol As Collection

    Set mobjCacheCurrentClassNameHwndToWindowCaptionDic = New Scripting.Dictionary
    
    mstrCurrentFilteringClassNameDelimitedByComma = vstrParentClassName
    
    intReturn = EnumWindows(AddressOf EnumerateSpecifiedClassNameWindowTextAndHWnd, ByVal 0&)

    Set objDic = mobjCacheCurrentClassNameHwndToWindowCaptionDic

    ' DebugDic objDic

    SortDictionaryAboutKeys objDic

    ' Enumerate child windows
    
    Set mobjChildWindowTextAllDTCol = New Collection
    
    With objDic
    
        For Each varHWnd In .Keys
        
            intHWnd = varHWnd
        
            ' set lparam to parent hWnd
            intReturn = EnumChildWindows(intHWnd, AddressOf EnumChildWindowTextAndClassNameAndHWnd, intHWnd)
        Next
    End With
    
'    DebugCol objCol

    ' Outputs
    
    Set robjTopLevelHWndToWindowTextDic = objDic
    
    Set robjChildWindowClassNameInfoCol = mobjChildWindowTextAllDTCol
End Sub



'''
'''
'''
Public Function GetCurrentWindowCaptionToHWndDic(Optional ByVal vstrWindowTextRegExpPattern As String = "", _
        Optional ByVal vblnIgnoreCase As Boolean = False) As Scripting.Dictionary


    Set GetCurrentWindowCaptionToHWndDic = GetVisibleWindowTextToHWndDic("", vstrWindowTextRegExpPattern, "", vblnIgnoreCase)


'#If VBA7 Then
'    Dim intReturn As LongPtr
'#Else
'    Dim intReturn As Long
'#End If
'
'    msubInitializeCacheWindowCaptionToHWndDic
'
'    msubSetupSearchingRegExp vstrWindowTextRegExpPattern, vblnIgnoreCase
'
'    intReturn = EnumWindows(AddressOf EnumerateVisibleWindowTextAndHWnd, ByVal 0&)
'
'    Set GetCurrentWindowCaptionToHWndDic = mobjCacheWindowCaptionToHWndDic
End Function





'''
''' About notepad.exe
'''
Public Function GetNotepadHwndToWindowCaptionDic() As Scripting.Dictionary

#If VBA7 Then
    Dim intReturn As LongPtr
#Else
    Dim intReturn As Long
#End If

    Set mobjCacheCurrentClassNameHwndToWindowCaptionDic = New Scripting.Dictionary
    
    mstrCurrentFilteringClassNameDelimitedByComma = mstrNotepadClassName
    
    intReturn = EnumWindows(AddressOf EnumerateWindowVisibleSpecifiedClassNameWindowTextAndHWnd, ByVal 0&)

    Set GetNotepadHwndToWindowCaptionDic = mobjCacheCurrentClassNameHwndToWindowCaptionDic
End Function

'''
''' About cmd.exe
'''
Public Function GetCmdHwndToWindowCaptionDic() As Scripting.Dictionary

#If VBA7 Then
    Dim intReturn As LongPtr
#Else
    Dim intReturn As Long
#End If

    Set mobjCacheCurrentClassNameHwndToWindowCaptionDic = New Scripting.Dictionary
    
    mstrCurrentFilteringClassNameDelimitedByComma = mstrCommandPromptWindowClassName
    
    intReturn = EnumWindows(AddressOf EnumerateWindowVisibleSpecifiedClassNameWindowTextAndHWnd, ByVal 0&)

    Set GetCmdHwndToWindowCaptionDic = mobjCacheCurrentClassNameHwndToWindowCaptionDic
End Function

'''
''' About Visual Basic Editor
'''
Public Function GetVBEHwndToWindowCaptionDic() As Scripting.Dictionary

#If VBA7 Then
    Dim intReturn As LongPtr
#Else
    Dim intReturn As Long
#End If

    Set mobjCacheCurrentClassNameHwndToWindowCaptionDic = New Scripting.Dictionary
    
    mstrCurrentFilteringClassNameDelimitedByComma = mstrVBEWindowClassName
    
    intReturn = EnumWindows(AddressOf EnumerateWindowVisibleSpecifiedClassNameWindowTextAndHWnd, ByVal 0&)

    Set GetVBEHwndToWindowCaptionDic = mobjCacheCurrentClassNameHwndToWindowCaptionDic
End Function


'''
''' all top-level windows hWnd, Window-title, and class-name
'''
Public Function GetAllTopLevelWindowsHWndInfoCol() As Collection

#If VBA7 Then
    Dim intReturn As LongPtr
#Else
    Dim intReturn As Long
#End If

    Set mobjWindowTextAllDTCol = New Collection
    
    intReturn = EnumWindows(AddressOf EnumWindowsVisibleOfAllWindowTextAndClassNameAndHWnd, ByVal 0&)

    Set GetAllTopLevelWindowsHWndInfoCol = mobjWindowTextAllDTCol
End Function

'///////////////////////////////////////////////
'/// Call back inernal functions
'///////////////////////////////////////////////

'''
''' Narrowing class name and store the Window caption text and hWnd
'''
#If VBA7 Then

Private Function EnumerateSpecifiedClassNameWindowTextAndHWnd(ByVal vintHWnd As LongPtr, ByVal vintlParam As LongPtr) As LongPtr
#Else

Private Function EnumerateSpecifiedClassNameWindowTextAndHWnd(ByVal vintHWnd As Long, ByVal vintlParam As Long) As Long
#End If
    Dim strText As String, strClassName As String, objHwndDic As Scripting.Dictionary
    Dim strHWnd As String, blnIsClassNameMatched As Boolean, varClassName As Variant

    strText = GetWindowTextVBAString(vintHWnd)

    strClassName = GetClassNameVBAString(vintHWnd)
    
    blnIsClassNameMatched = False
    
    For Each varClassName In Split(mstrCurrentFilteringClassNameDelimitedByComma, ",")
    
        If StrComp(UCase(strClassName), UCase(varClassName)) = 0 Then
        
            blnIsClassNameMatched = True
        End If
    Next
    
    If blnIsClassNameMatched Then
    
        With mobjCacheCurrentClassNameHwndToWindowCaptionDic
        
            strHWnd = "" & vintHWnd
        
            If Not .Exists(strHWnd) Then
            
                .Add strHWnd, strText
            End If
        End With
    End If

    EnumerateSpecifiedClassNameWindowTextAndHWnd = True
End Function


'''
''' call back test function  - enumerate top-level windows and add the HWnd and captions
'''
#If VBA7 Then

Private Function EnumChildWindowTextAndClassNameAndHWnd(ByVal vintHWnd As LongPtr, ByVal vintlParam As LongPtr) As LongPtr
    
#Else

Private Function EnumChildWindowTextAndClassNameAndHWnd(ByVal vintHWnd As Long, ByVal vintlParam As Long) As Long

#End If
    Dim strText As String, strClassName As String, objRowCol As Collection
  
    strText = GetWindowTextVBAString(vintHWnd)

    strClassName = GetClassNameVBAString(vintHWnd)

    Set objRowCol = New Collection
    
    With objRowCol
    
        .Add strText
    
        .Add strClassName
        
        .Add "" & vintHWnd
    End With

    mobjChildWindowTextAllDTCol.Add objRowCol
    
    EnumChildWindowTextAndClassNameAndHWnd = True
End Function


#If VBA7 Then
    '''
    ''' Narrowing specified class name and store the Window caption text and hWnd
    '''
    Private Function EnumerateWindowVisibleSpecifiedClassNameWindowTextAndHWnd(ByVal vintHWnd As LongPtr, ByVal vintlParam As LongPtr) As LongPtr
        
        Dim strText As String, strClassName As String, objHwndDic As Scripting.Dictionary
        Dim strHWnd As String, blnIsClassNameMatched As Boolean, varClassName As Variant
    
        If CBool(IsWindowVisible(vintHWnd)) Then
            
            strText = GetWindowTextVBAString(vintHWnd)
    
            strClassName = GetClassNameVBAString(vintHWnd)
            
            blnIsClassNameMatched = False
            
            For Each varClassName In Split(mstrCurrentFilteringClassNameDelimitedByComma, ",")
            
                If StrComp(UCase(strClassName), UCase(varClassName)) = 0 Then
                
                    blnIsClassNameMatched = True
                End If
            Next
            
            If blnIsClassNameMatched Then
            
                With mobjCacheCurrentClassNameHwndToWindowCaptionDic
                
                    strHWnd = "" & vintHWnd
                
                    If Not .Exists(strHWnd) Then
                    
                        .Add strHWnd, strText
                    End If
                End With
            End If
        End If
    
        EnumerateWindowVisibleSpecifiedClassNameWindowTextAndHWnd = True
    End Function
    
    '''
    ''' Narrowing string Window caption text by RegExp and store the Window caption text and hWnd
    '''
    Private Function EnumerateVisibleWindowTextAndHWnd(ByVal vintHWnd As LongPtr, ByVal vintlParam As LongPtr) As LongPtr
        
        Dim strText As String
    
        If CBool(IsWindowVisible(vintHWnd)) Then
            
            strText = GetWindowTextVBAString(vintHWnd)
    
            If strText <> "" Then
            
                With mobjCacheWindowCaptionToHWndDic
                
                    If Not mobjSearchingRegExp Is Nothing Then
                    
                        If mobjSearchingRegExp.Test(strText) Then
                        
                            If Not .Exists(strText) Then
                            
                                .Add strText, "" & vintHWnd
                            End If
                        End If
                    Else
                        If Not .Exists(strText) Then
                        
                            .Add strText, "" & vintHWnd
                        End If
                    End If
                End With
            End If
        End If
        
        EnumerateVisibleWindowTextAndHWnd = True
    End Function
    
    
    '''
    ''' call back test function  - enumerate top-level windows and add the HWnd and captions
    '''
    Private Function EnumWindowsVisibleTextSpecifiedCaptions(ByVal vintHWnd As LongPtr, ByVal vintlParam As LongPtr) As LongPtr
        
        Dim strText As String
    
        If CBool(IsWindowVisible(vintHWnd)) Then
            
            strText = GetWindowTextVBAString(vintHWnd)
    
            If strText <> "" Then
            
                mobjFoundHWndToWindowCaptionDic.Add "" & vintHWnd, strText
            End If
        End If
        
        EnumWindowsVisibleTextSpecifiedCaptions = True
    End Function
    
    '''
    ''' call back test function  - enumerate top-level windows and add the HWnd and captions
    '''
    Private Function EnumWindowsVisibleOfAllWindowTextAndClassNameAndHWnd(ByVal vintHWnd As LongPtr, ByVal vintlParam As LongPtr) As LongPtr
        
        Dim strText As String, strClassName As String, objRowCol As Collection
    
        If CBool(IsWindowVisible(vintHWnd)) Then
            
            strText = GetWindowTextVBAString(vintHWnd)
    
            If strText <> "" Then
            
                strClassName = GetClassNameVBAString(vintHWnd)
            
                Set objRowCol = New Collection
                
                With objRowCol
                
                    .Add strText
                
                    .Add strClassName
                    
                    .Add "" & vintHWnd
                End With
            
                mobjWindowTextAllDTCol.Add objRowCol
            End If
        End If
        
        EnumWindowsVisibleOfAllWindowTextAndClassNameAndHWnd = True
    End Function
#Else
    '''
    ''' Narrowing Notepad.exe class name and store the Window caption text and hWnd
    '''
    Private Function EnumerateWindowVisibleSpecifiedClassNameWindowTextAndHWnd(ByVal vintHWnd As Long, ByVal vintlParam As Long) As Long
        
        Dim strText As String, strClassName As String, objHwndDic As Scripting.Dictionary
        Dim strHWnd As String
    
        If CBool(IsWindowVisible(vintHWnd)) Then
            
            strText = GetWindowTextVBAString(vintHWnd)
    
            strClassName = GetClassNameVBAString(vintHWnd)
            
            If StrComp(UCase(strClassName), UCase(mstrCurrentFilteringClassNameDelimitedByComma)) = 0 Then
            
                With mobjCacheCurrentClassNameHwndToWindowCaptionDic
                
                    strHWnd = "" & vintHWnd
                
                    If Not .Exists(strHWnd) Then
                    
                        .Add strHWnd, strText
                    End If
                End With
            End If
        End If
    
        EnumerateWindowVisibleSpecifiedClassNameWindowTextAndHWnd = True
    End Function
    
    '''
    ''' Narrowing string Window caption text by RegExp and store the Window caption text and hWnd
    ''' for Excel 2003 or before
    '''
    Private Function EnumerateVisibleWindowTextAndHWnd(ByVal vintHWnd As Long, ByVal vintlParam As Long) As Long
        
        Dim strText As String
    
        If CBool(IsWindowVisible(vintHWnd)) Then
            
            strText = GetWindowTextVBAString(vintHWnd)
    
            If strText <> "" Then
            
                If Not mobjSearchingRegExp Is Nothing Then
                
                    If mobjSearchingRegExp.Test(strText) Then
                    
                        mobjCacheWindowCaptionToHWndDic.Add strText, "" & vintHWnd
                    
                    End If
                Else
                    mobjCacheWindowCaptionToHWndDic.Add strText, "" & vintHWnd
                End If
            End If
        End If
        
        EnumerateVisibleWindowTextAndHWnd = True
    End Function
    
    
    '''
    ''' for Excel 2003 or before, call back test function  - enumerate top-level windows and add the HWnd and captions
    '''
    Private Function EnumWindowsVisibleTextSpecifiedCaptions(ByVal vintHWnd As Long, ByVal vintlParam As Long) As Long
        
        Dim strText As String
    
        If CBool(IsWindowVisible(vintHWnd)) Then
            
            strText = GetWindowTextVBAString(vintHWnd)
    
            If strText <> "" Then
            
                mobjFoundHWndToWindowCaptionDic.Add "" & vintHWnd, strText
            End If
        End If
        
        EnumWindowsVisibleTextSpecifiedCaptions = True
    End Function

    '''
    ''' for Excel 2003 or before, call back test function  - enumerate top-level windows and add the HWnd and captions
    '''
    Private Function EnumWindowsVisibleOfAllWindowTextAndClassNameAndHWnd(ByVal vintHWnd As Long, ByVal vintlParam As Long) As Long
        
        Dim strText As String, strClassName As String, objRowCol As Collection
    
        If CBool(IsWindowVisible(vintHWnd)) Then
            
            strText = GetWindowTextVBAString(vintHWnd)
    
            If strText <> "" Then
            
                strClassName = GetClassNameVBAString(vintHWnd)
            
                Set objRowCol = New Collection
                
                With objRowCol
                
                    .Add strText
                
                    .Add strClassName
                    
                    .Add "" & vintHWnd
                End With
            
                mobjWindowTextAllDTCol.Add objRowCol
            End If
        End If
        
        EnumWindowsVisibleOfAllWindowTextAndClassNameAndHWnd = True
    End Function
#End If

'///////////////////////////////////////////////
'/// Inernal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubInitializeFoundHWndToWindowCaptionDic()

    If mobjFoundHWndToWindowCaptionDic Is Nothing Then
    
        Set mobjFoundHWndToWindowCaptionDic = New Scripting.Dictionary
    Else
        mobjFoundHWndToWindowCaptionDic.RemoveAll
    End If
End Sub

'''
'''
'''
Private Sub msubInitializeCacheWindowCaptionToHWndDic()

    If mobjCacheWindowCaptionToHWndDic Is Nothing Then
    
        Set mobjCacheWindowCaptionToHWndDic = New Scripting.Dictionary
    Else
        mobjCacheWindowCaptionToHWndDic.RemoveAll
    End If
End Sub

'''
'''
'''
Public Sub SetupRegExpForSearchingTopLevelHWnd(ByRef robjRegExp As VBScript_RegExp_55.RegExp, _
        ByVal vstrNewPattern As String, _
        ByVal vblnIgnoreCase As Boolean, _
        ByVal vblnGlobal As Boolean)


    If vstrNewPattern = "" Then
    
        If Not robjRegExp Is Nothing Then Set robjRegExp = Nothing
    Else
        If robjRegExp Is Nothing Then
        
            Set robjRegExp = New VBScript_RegExp_55.RegExp
        End If

        With robjRegExp
        
            If .Pattern <> vstrNewPattern Then
            
                .Pattern = vstrNewPattern
            End If
        
            If .IgnoreCase <> vblnIgnoreCase Then
            
                .IgnoreCase = vblnIgnoreCase
            End If
            
            If .Global <> vblnGlobal Then
            
                .Global = vblnGlobal
            End If
        End With
    End If
End Sub


'''
''' set up the standard-module private variable RegExp object
'''
Private Sub msubSetupSearchingRegExp(ByVal vstrNewPattern As String, Optional ByVal vblnIgnoreCase As Boolean = False)

    SetupRegExpForSearchingTopLevelHWnd mobjSearchingRegExp, vstrNewPattern, vblnIgnoreCase, False
End Sub

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** About Microsoft Word
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToFindWindowsHWndInfoAboutMsWord()

    Dim objDic As Scripting.Dictionary, varHWnd As Variant, objCol As Collection

    FindCurrentProcessAndChildWindowsInfo objDic, objCol, WINDOW_CLASS_MS_WORD

    Set objDic = mobjCacheCurrentClassNameHwndToWindowCaptionDic

    DebugDic objDic

    Set objCol = mobjChildWindowTextAllDTCol

    Debug.Print "Child windows hWnd information: "

    DebugCol objCol
End Sub


'**---------------------------------------------
'** About general applications
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToGetAllTopLevelWindowsHWndInfoCol()

    DebugCol GetAllTopLevelWindowsHWndInfoCol()
End Sub


'''
''' seach cmd.exe, whose process are running
'''
Private Sub msubSanityTestToGetCurrentWindowCaptionToHWndDicForCmd()

    DebugDic GetCurrentWindowCaptionToHWndDic(vstrWindowTextRegExpPattern:="cmd|コマンド プロンプト")
End Sub

'''
''' search sakura.exe Version 2, whose process are running
'''
Private Sub msubSanityTestToGetCurrentWindowCaptionToHWndDicForSakuraEditor()

    DebugDic GetCurrentWindowCaptionToHWndDic(vstrWindowTextRegExpPattern:=" - sakura 2")
End Sub

'''
''' for all
'''
Private Sub msubSanityTestToGetCurrentWindowCaptionToHWndDicForAll()

    DebugDic GetCurrentWindowCaptionToHWndDic()
End Sub

'''
''' Enumerate only notepad.exe
'''
Private Sub msubSanityTestToGetNotepadWindowCaptionToHWndDic()

    DebugDic GetNotepadHwndToWindowCaptionDic()
End Sub


'''
''' Enumerate only cmd.exe, In Windows 11 (x64) the class name is CASCADIA_HOSTING_WINDOW_CLASS
'''
Private Sub msubSanityTestToGetCmdHwndToWindowCaptionDic()

    DebugDic GetCmdHwndToWindowCaptionDic()
End Sub

'''
''' Enumerate only Visual Basic Editor
'''
Private Sub msubSanityTestToGetVBEHwndToWindowCaptionDic()

    DebugDic GetVBEHwndToWindowCaptionDic()
End Sub


'''
'''
'''
Private Sub SanityTestOfEnumerateAllTopLevelWindows()

#If VBA7 Then
    Dim intReturn As LongPtr
#Else
    Dim intReturn As Long
#End If
    Dim varHWnd As Variant

    msubInitializeFoundHWndToWindowCaptionDic

    intReturn = EnumWindows(AddressOf EnumWindowsVisibleTextSpecifiedCaptions, ByVal 0&)

    msubDebugPrintFoundHWndToWindowCaptionDic
End Sub

'''
'''
'''
Private Sub msubDebugPrintFoundHWndToWindowCaptionDic()

    Dim varHWnd As Variant
    
    With mobjFoundHWndToWindowCaptionDic
    
        If .Count > 0 Then
        
            For Each varHWnd In .Keys
            
                Debug.Print "Specified Window-caption of the visible vindow: " & .Item(varHWnd)
            Next
        End If
    End With
End Sub


