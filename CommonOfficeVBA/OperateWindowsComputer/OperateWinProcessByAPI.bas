Attribute VB_Name = "OperateWinProcessByAPI"
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 18/Jan/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Windows API Declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As LongPtr, lpdwProcessId As Long) As Long

    Private Declare PtrSafe Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As LongPtr, ByVal dwMilliseconds As Long) As Long
    Private Declare PtrSafe Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As LongPtr
    Private Declare PtrSafe Function CloseHandle Lib "kernel32" (ByVal hObject As LongPtr) As Long

    Private Declare PtrSafe Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As LongPtr
    Private Declare PtrSafe Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As LongPtr, ByVal lpString As String, ByVal cch As Long) As Long
    
    ' It seems that this is not supported at Windows 11 64bit environment.
    Private Declare PtrSafe Function SetWindowText Lib "user32" Alias "SetWindowTextA" (ByVal hwnd As LongPtr, ByVal lpString As String) As Long

    Private Declare PtrSafe Function MoveWindow Lib "user32" (ByVal hwnd As LongPtr, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal bRepaint As Long) As Long

    Private Declare PtrSafe Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As LongPtr, ByVal Msg As LongPtr, ByVal wParam As LongPtr, lParam As Any) As LongPtr

    Private Declare PtrSafe Function SetForegroundWindow Lib "user32" (ByVal hwnd As LongPtr) As Long

    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
#Else
    
    Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hWnd As Long, lpdwProcessId As Long) As Long
    
    Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
    Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
    Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
    
    Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
    
    Private Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long) As Long
    Private Declare Function SetWindowText Lib "user32" Alias "SetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String) As Long
    
    Private Declare Function MoveWindow Lib "user32" (ByVal hwnd As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal bRepaint As Long) As Long
    
    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, lParam As Any) As Long
    
    Private Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long
    
    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
#End If

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const SYNCHRONIZE       As Long = &H100000
Private Const PROCESS_TERMINATE As Long = &H1


Private Const PROCESS_ALL_ACCESS As Long = &H1F0FFF

Private Const INFINITE As Long = &HFFFF


Private Const WM_SYSCOMMAND = &H112

Private Const SC_MINIMIZE As Long = &HF020&     '0xF020　Minimize window
Private Const SC_MAXIMIZE As Long = &HF030&     '0xF030　Maximize window
Private Const SC_CLOSE As Long = &HF060&


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** cmd.exe windows control by API
'**---------------------------------------------
'''
'''
'''
Public Sub MoveVisibleCmdWindowToHiddenPosition()

    Dim varHWnd As Variant
    
#If VBA7 Then
    Dim intHWnd As LongPtr, intReturn As LongPtr
#Else
    Dim intHWnd As Long, intReturn As Long
#End If

    With GetCmdHwndToWindowCaptionDic()

        For Each varHWnd In .Keys
        
            intHWnd = varHWnd
        
            intReturn = MoveWindow(intHWnd, 0, 0, 1, 1, True)
        Next
    End With
End Sub

'''
'''
'''
Public Sub MinimizeVisibleCmdWindow()

    Dim varHWnd As Variant
    
#If VBA7 Then
    Dim intHWnd As LongPtr, intReturn As LongPtr
#Else
    Dim intHWnd As Long, intReturn As Long
#End If

    With GetCmdHwndToWindowCaptionDic()

        For Each varHWnd In .Keys
        
            intHWnd = varHWnd
        
            intReturn = SendMessage(intHWnd, WM_SYSCOMMAND, SC_MINIMIZE, 0)
        Next
    End With
End Sub


'**---------------------------------------------
'** Extend VBA.Shell
'**---------------------------------------------
'''
''' Wait for finishing the command using Windows API after the VBA.Shell execution
'''
Public Sub VBAShellExecutionWithWaiting(ByVal vstrCommand As String)

    WaitForSingleObjectByProcessID VBA.Shell(vstrCommand)
End Sub


'''
''' wait for closing the found Windows processes
'''
Public Sub WaitForClosingMatchedWindowTitleStringProcessIfItExists(ByVal vstrWindowTitleMatchRegExpPattern As String, Optional ByVal vblnRegExpPatternIgnoreCase As Boolean = False)
    
    Dim varKey As Variant, varHWnd As Variant, intProcessID As Long
    
#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    With GetCurrentWindowCaptionToHWndDic(vstrWindowTitleMatchRegExpPattern, vblnRegExpPatternIgnoreCase)
    
        If .Count > 0 Then
            
            For Each varKey In .Keys
            
                intHWnd = .Item(varKey)
            
                intProcessID = 0
            
                GetWindowThreadProcessId intHWnd, intProcessID
                
                If intProcessID <> 0 Then
                    
                    WaitForSingleObjectByProcessID intProcessID
                End If
            Next
        End If
    End With

End Sub

'''
'''
'''
Public Sub WaitForSingleObjectByProcessID(ByVal vintProcessID As Long)
    
#If VBA7 Then
    Dim intProcessHandle As LongPtr
#Else
    Dim intProcessHandle As Long
#End If

    intProcessHandle = OpenProcess(PROCESS_ALL_ACCESS, 0, vintProcessID)
    
    If intProcessHandle <> 0 Then
        
        WaitForSingleObject intProcessHandle, INFINITE
        
        CloseHandle intProcessHandle
    End If
End Sub



'''
''' close the found Windows processes
'''
Public Sub CloseMatchedWindowTitleStringProcessIfItExists(ByVal vstrWindowTitleMatchRegExpPattern As String, _
        Optional ByVal vblnRegExpPatternIgnoreCase As Boolean = False)
    
    Dim varKey As Variant, intProcessID As Long

    With GetCurrentWindowCaptionToHWndDic(vstrWindowTitleMatchRegExpPattern, vblnRegExpPatternIgnoreCase)
    
        For Each varKey In .Keys
        
            CloseApplicationProcessFromWindowHandle .Item(varKey)
        Next
    End With
End Sub


'''
'''
'''
Public Sub CloseApplicationProcessFromWindowHandle(ByVal vvarHWnd As Variant)

    Dim intProcessID As Long
    
#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    intHWnd = vvarHWnd
        
    intProcessID = 0

    GetWindowThreadProcessId intHWnd, intProcessID
    
    If intProcessID <> 0 Then
    
        TerminateProcessByProcessID intProcessID
    End If
End Sub


'///////////////////////////////////////////////
'/// Inernal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrCreateSampleBatFile(ByVal vintTimeoutSecond As Long) As String
    
    Dim strBatPath As String
    
    Const strWindowTitle As String = "cmd WindowTitle"
    
    With New IWshRuntimeLibrary.WshShell

        Debug.Print .SpecialFolders("MyDocuments")
        
        strBatPath = .SpecialFolders("MyDocuments") & "\WindowTitleChange.bat"
    End With
    
    With New Scripting.FileSystemObject
    
        With .CreateTextFile(strBatPath, True)
        
            .WriteLine "Title " & strWindowTitle
            
            .WriteLine "timeout /t " & CStr(vintTimeoutSecond) & " /nobreak >nul"
            
            '.WriteLine "Pause"
        
            .Close
        End With
    End With

    mfstrCreateSampleBatFile = strBatPath
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Shell(cmd.exe) running tests
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestOfShellExecutionWithWatingByFirstWshShellRunNextRetryVBAShell()

    If Not FeatureOfWshShellRunDisabling Then
    
        ChangeWshShellRunDisablingFeatureDisabled False
    End If

    ' If the Norton security software prevented starting cmd.exe, in order to continue to test
    ' you can permit to execute cmd.exe temporarily by changing the Norton Script-Conrol option

    ShellExecutionWithWatingByFirstWshShellRunNextRetryVBAShell "cmd.exe " & mfstrCreateSampleBatFile(5)

    If FeatureOfWshShellRunDisabling Then
    
        ChangeWshShellRunDisablingFeatureDisabled False
    End If
End Sub

'''
'''
'''
Private Sub msubSanityTestToStartCmdShell()

    ' If the Norton security software prevented starting cmd.exe, in order to continue to test
    ' you have to permit cmd.exe execution by changing the Norton Script-Conrol option

    VBA.Shell "cmd.exe"
End Sub

'''
'''
'''
Private Sub msubSanityTestOfVBAShellExecutionWithWaiting()
    
    Dim strBatPath As String, strCommand As String
    
    strBatPath = mfstrCreateSampleBatFile(4)
    
    strCommand = strBatPath


    Debug.Print "Started at " & Format(Now(), "yyyy/m/d hh:mm:ss")

    VBAShellExecutionWithWaiting strCommand

    Debug.Print "Finished at " & Format(Now(), "yyyy/m/d hh:mm:ss")

End Sub

'''
''' testing WaitForClosingMatchedWindowTitleStringProcessIfItExists
'''
Private Sub msubSanityTestToWaitForClosingMatchedWindowTitleStringProcessIfItExists()
    
    Dim strBatPath As String
    
    strBatPath = mfstrCreateSampleBatFile(5)
    
    Debug.Print "Started at " & Format(Now(), "yyyy/m/d hh:mm:ss")
    
    VBA.Shell strBatPath    ' If you use Norton 360, then it prevent executing Cmd.exe. And you will have to permit the Norton 360 to execute
    
'    DoEvents: DoEvents: DoEvents
'
    Sleep 500
    
    DebugDic GetCurrentWindowCaptionToHWndDic(vstrWindowTextRegExpPattern:="cmd")
    
    WaitForClosingMatchedWindowTitleStringProcessIfItExists "Title"
    
    Debug.Print "Finished at " & Format(Now(), "yyyy/m/d hh:mm:ss")
    
End Sub


'''
''' Sample of waiting for the Command-prompt exiting
'''
''' modified from https://www.moug.net/tech/exvba/0150034.html
'''
Private Sub msubSanityTestToRunAndWait()
    
    Dim intProcessID As Long                ' Task ID

#If VBA7 Then
    Dim intProcessHandle As LongPtr
#Else
    Dim intProcessHandle As Long
#End If

    ' execut an outside program
    
    intProcessID = VBA.Shell("cmd.exe", vbMinimizedFocus)
    
    Debug.Print "Started at " & Format(Now(), "yyyy/m/d hh:mm:ss")
    
    intProcessHandle = OpenProcess(PROCESS_ALL_ACCESS, 0, intProcessID)
    
    If intProcessHandle <> 0 Then
        
        WaitForSingleObject intProcessHandle, INFINITE
        
        CloseHandle intProcessHandle
        
        Debug.Print "Finished at " & Format(Now(), "yyyy/m/d hh:mm:ss")
    End If
End Sub

'''
'''
'''
#If VBA7 Then

Public Function GetHWndWithOpeningNotePad() As LongPtr

#Else
Public Function GetHWndWithOpeningNotePad() As Long
#End If

    Dim intProcessID As Long

    intProcessID = CLng(VBA.Shell("Notepad", vbNormalFocus))
    
    GetHWndWithOpeningNotePad = FindWindow("notepad", vbNullString)
End Function

'''
'''
'''
Public Sub OpenNotepadAndSetItForegroundWindow()

    Dim intRet As Long

#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    intHWnd = GetHWndWithOpeningNotePad()

    intRet = SetForegroundWindow(intHWnd)

    Interaction.DoEvents
    
    Sleep 500   ' 1500
End Sub

'''
''' Wordpad is to be abolished from Windows11 24H2
'''
#If VBA7 Then

Public Function GetHWndWithOpeningWordpad() As LongPtr

#Else
Public Function GetHWndWithOpeningWordpad() As Long
#End If

    Dim intProcessID As Long, intFoundProcessID As Long, intThreadID As Long
    
    Dim strCmd As String, strDir As String, varWindowCaption As Variant
    
    Dim blnIsTheWindowFound As Boolean
    
#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    strDir = "C:\Program Files\Windows NT\Accessories\"
    
    strCmd = strDir & "wordpad.exe"
    
    intProcessID = CLng(VBA.Shell(strCmd, vbNormalFocus))

    blnIsTheWindowFound = False

    With GetCurrentWindowCaptionToHWndDic(vstrWindowTextRegExpPattern:="ワードパッド|wordpad", vblnIgnoreCase:=True)
    
        If .Count > 0 Then
    
            For Each varWindowCaption In .Keys
            
                intHWnd = .Item(varWindowCaption)
            
                intThreadID = GetWindowThreadProcessId(intHWnd, intFoundProcessID)
            
                If intProcessID = intFoundProcessID Then
            
                    blnIsTheWindowFound = True
                
                    Exit For
                End If
            Next
        End If
    End With
    
    GetHWndWithOpeningWordpad = intHWnd
End Function



'''
''' SetWindowText is not supported at the Windows 11 (64 bit Windows)
'''
Private Sub msubSanityTestToOpenNotepad()

    Dim intProcessID As Long
    Dim strWindowText As String * 500
    Dim strCaption As String
    
#If VBA7 Then
    Dim intProcessHandle As LongPtr, intHWnd As LongPtr
#Else
    Dim intProcessHandle As Long, intHWnd As Long
#End If
 
    intProcessID = CLng(VBA.Shell("Notepad", vbNormalFocus))

    'Sleep 1200

    ' get the process handle
    intProcessHandle = OpenProcess(SYNCHRONIZE, 0&, intProcessID)
    
    intHWnd = FindWindow("notepad", vbNullString)
    
    'GetWindowText intProcessHandle, ByVal strWindowText, Len(strWindowText)
    
    'Sleep 1200
    Sleep 300
    
    GetWindowText intHWnd, strWindowText, Len(strWindowText)
    
    Debug.Print "Process handle : " & CStr(intProcessHandle)
    Debug.Print "Window handle : " & CStr(intHWnd)
    
    Debug.Print "Get window text : " & strWindowText
    
    strCaption = "New notepad"
    
    'strWindowText = "Test Characters notepad" & Chr(0)
    'strWindowText = "Test Characters notepad" & Chr(0) & Chr(0)
    
    SetWindowText intHWnd, strCaption
    
    CloseHandle intProcessHandle
End Sub

