Attribute VB_Name = "OperateWinShell"
'
'   Utility to use Windows shell
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       Dependent on WshShell
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 31/May/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit


'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = True

'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then
    ' For 64 bit version windows, declare timeGetTime
    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
#Else
    ' For 32 bit version windows, declare timeGetTime
    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
#End If


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** WshShell.Exec tools
'**---------------------------------------------
'''
''' this is dependent on the IWshRuntimeLibrary.WshShell object execution
'''
Public Function ExecuteWinCmd(ByVal vstrCommand As String, _
        Optional ByVal vstrWorkingDirectoryPath As String = "") As String

    Dim strStdErr As String

    ExecuteWinCmd = ExecuteWinCmdWithGettingStdErr(vstrCommand, strStdErr, vstrWorkingDirectoryPath)
End Function
'''
'''
'''
''' <Argument>vstrCommand: Input</Argument>
''' <Argument>rstrStdErr: Output</Argument>
''' <Argument>vstrWorkingDirectoryPath: Input</Argument>
''' <Return>String: cmd.exe StdOut</Return>
Public Function ExecuteWinCmdWithGettingStdErr(ByVal vstrCommand As String, _
        ByRef rstrStdErr As String, _
        Optional ByVal vstrWorkingDirectoryPath As String = "") As String

    Dim strStdErr As String

#If HAS_REF Then

    Dim objWshShell As IWshRuntimeLibrary.WshShell, objWshExec As IWshRuntimeLibrary.WshExec
    
    Set objWshShell = New IWshRuntimeLibrary.WshShell
#Else

    Dim objWshShell As Object, objWshExec As Object
    
    Set objWshShell = CreateObject("WScript.Shell")
#End If
    
    strStdErr = ""
    
    With objWshShell
    
        If vstrWorkingDirectoryPath <> "" Then
        
            .CurrentDirectory = vstrWorkingDirectoryPath
        End If
    
        Set objWshExec = .Exec("%ComSpec% /c" & vstrCommand)
        
        'Set objWshExec = .Exec("%ComSpec% /k """ & vstrCommand & """")
        
        With objWshExec
        
            Do While .Status = 0
                
                Sleep 50
                
                Interaction.DoEvents
            Loop
            
            strStdErr = .StdErr.ReadAll
            
            If strStdErr <> "" Then
            
                rstrStdErr = strStdErr
            End If
            
            ExecuteWinCmdWithGettingStdErr = .StdOut.ReadAll
        End With
    End With
End Function

'''
'''
'''
Public Sub ExecuteWinCmdByRunMethod(ByVal vstrCommand As String, Optional ByVal vstrWorkingDirectoryPath As String = "")

#If HAS_REF Then
    
    With New IWshRuntimeLibrary.WshShell
#Else
    With CreateObject("WScript.Shell")
#End If
        If vstrWorkingDirectoryPath <> "" Then
        
            .CurrentDirectory = vstrWorkingDirectoryPath
        End If
    
        .Run vstrCommand, WshNormalFocus, True
    End With
End Sub

'''
'''
'''
Public Function GetPathWithSurroundingDoubleQuotationsIfItsAreNeeded(ByRef rstrPath As String) As String

    If InStr(1, rstrPath, " ") > 0 Then
    
        GetPathWithSurroundingDoubleQuotationsIfItsAreNeeded = """" & rstrPath & """"
    Else
        GetPathWithSurroundingDoubleQuotationsIfItsAreNeeded = rstrPath
    End If
End Function


'**---------------------------------------------
'** Shell running solution (counter measure when WshShell.Run is to be failed because of Windows security setting)
'**---------------------------------------------
'''
''' At first, use WshShell, if it is failed then the command is retried by the VBA.Shell
'''
Public Sub ShellExecutionWithWatingByFirstWshShellRunNextRetryVBAShell(ByVal vstrCommand As String, Optional ByVal vblnWaitOnReturn As Boolean = True)
    
    On Error GoTo ErrWshShellError
        
    If Not FeatureOfWshShellRunDisabling Then   ' for unit-test logic
        
#If HAS_REF Then

        With New IWshRuntimeLibrary.WshShell
#Else
        With CreateObject("WScript.Shell")
#End If

            .Run vstrCommand, 8, vblnWaitOnReturn
        End With
    Else
        Err.Raise -101, "ShellExecutionWithWatingByFirstWshShellRunNextRetryVBAShell", "It seems disabling WshShell.Run"
    End If
    
ErrWshShellError:
    
    If Err.Number <> 0 Then
    
        Debug.Print mfstrGetErrorLogByFailureOfWshShellRun(FeatureOfWshShellRunDisabling, vstrCommand, Err.Description)
        
        Err.Clear
    
        If vblnWaitOnReturn Then
        
            ' use VBA.Shell
        
            VBAShellExecutionWithWaiting vstrCommand
        Else
            VBA.Shell vstrCommand
        End If
    End If
    
    On Error GoTo 0

End Sub


'''
''' After failing WshShell.Run execution, retry to command by VBA.Shell
'''
Public Sub ShellExectionWithoutWatingByFirstWshShellRunNextRetryVBAShell(ByVal vstrCommand As String)

    ShellExecutionWithWatingByFirstWshShellRunNextRetryVBAShell vstrCommand, False
End Sub


'''
'''
'''
Private Function mfstrGetErrorLogByFailureOfWshShellRun(ByVal vblnFeatureOfWshShellRunDisabling As Boolean, ByVal vstrCommand As String, ByVal vstrErrorDescription As String) As String
    
    Dim strErrorLog As String
    
    If vblnFeatureOfWshShellRunDisabling Then
    
        strErrorLog = "By feature disabling, failed to WshShell.Run method ," & vstrErrorDescription & ", Command:[" & vstrCommand & "]"
    Else
        strErrorLog = "Failed to WshShell.Run method ," & vstrErrorDescription & ", Command:[" & vstrCommand & "]"
    End If

    mfstrGetErrorLogByFailureOfWshShellRun = strErrorLog
End Function

'**---------------------------------------------
'** Create and execute temporary batch file
'**---------------------------------------------
'''
'''
'''
Public Function ExecuteWinCmdBatchFileWithGettingStdErr(ByRef rstrDirectoryPath As String, _
        ByRef rstrBatchFileName As String, _
        ByRef rstrCommandString As String) As String

    Dim strCommand As String, strStdErr As String, strStdOut As String, objFS As Scripting.FileSystemObject, strLog As String
    
    Set objFS = New Scripting.FileSystemObject
    
    CreateTemporaryBatchFileByFSO rstrDirectoryPath, rstrBatchFileName, rstrCommandString, objFS

    strCommand = rstrBatchFileName

    Sleep 50

    strStdOut = ExecuteWinCmdWithGettingStdErr(strCommand, strStdErr, rstrDirectoryPath)

    Sleep 50

    objFS.DeleteFile rstrDirectoryPath & "\" & rstrBatchFileName

    strLog = ""
    
    msubGetCmdExecutionLogTextForGeneral strLog, strStdOut, strStdErr

    ExecuteWinCmdBatchFileWithGettingStdErr = strLog
End Function


'''
'''
'''
''' <Argument>rstrDirectoryPath: Input</Argument>
''' <Argument>rstrBatchFileName: Input</Argument>
''' <Argument>rstrCommandString: Input</Argument>
Public Sub ExecuteWinCmdBatchFileByRun(ByRef rstrDirectoryPath As String, _
        ByRef rstrBatchFileName As String, _
        ByRef rstrCommandString As String)

    Dim strCommand As String, strStdErr As String, strStdOut As String, objFS As Scripting.FileSystemObject, strLog As String
    
    
    Set objFS = New Scripting.FileSystemObject
    
    CreateTemporaryBatchFileByFSO rstrDirectoryPath, rstrBatchFileName, rstrCommandString, objFS

    strCommand = rstrBatchFileName

    Sleep 50

    ExecuteWinCmdByRunMethod strCommand, rstrDirectoryPath

    Sleep 50

    objFS.DeleteFile rstrDirectoryPath & "\" & rstrBatchFileName
End Sub


'''
'''
'''
''' <Argument>rstrDirectoryPath: Input</Argument>
''' <Argument>rstrBatchFileName: Input</Argument>
''' <Argument>rstrCommandString: Input</Argument>
''' <Argument>vobjFS: Input</Argument>
Public Sub CreateTemporaryBatchFileByFSO(ByRef rstrDirectoryPath As String, _
        ByRef rstrBatchFileName As String, _
        ByRef rstrCommandString As String, _
        Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing)

    Dim strBatchFilePath As String, objFS As Scripting.FileSystemObject
    
    
    strBatchFilePath = rstrDirectoryPath & "\" & rstrBatchFileName

    If Not vobjFS Is Nothing Then
    
        Set objFS = vobjFS
    Else
        Set objFS = New Scripting.FileSystemObject
    End If

    With objFS
    
        If .FileExists(strBatchFilePath) Then
        
            .DeleteFile strBatchFilePath, True
        End If
    
        If Not .FolderExists(rstrDirectoryPath) Then
        
            ForceToCreateDirectory rstrDirectoryPath, objFS
        End If
        
        With .CreateTextFile(strBatchFilePath, True)
        
            .Write rstrCommandString
        
            .Close
        End With
    End With
End Sub


'''
'''
'''
Public Sub GetBatchCommandStringFromCommandCol(ByRef rstrBatchCommandString As String, _
        ByRef robjCommands As Collection)

    Dim varCommand As Variant   ' cmd.exe shell command

    For Each varCommand In robjCommands
    
        If rstrBatchCommandString <> "" Then
        
            rstrBatchCommandString = rstrBatchCommandString & vbNewLine
        End If
    
        rstrBatchCommandString = rstrBatchCommandString & varCommand
    Next
End Sub


'**---------------------------------------------
'** Open cmd.exe shell
'**---------------------------------------------
'''
'''
'''
Public Sub OpenCmdWithSpecifyingBothWorkingDirectoryAndCmdWindowTitleText(ByVal vstrWorkingDirectoryPath As String, _
        Optional ByVal vstrWindowTitleText As String = "", _
        Optional ByVal vstrAddingCmdEnvironmentPath As String = "")

    Dim strContinuousCommand As String, strCommand As String

    strContinuousCommand = ""
    
    If vstrWindowTitleText <> "" Then
    
        strContinuousCommand = strContinuousCommand & "title " & vstrWindowTitleText
    End If
    
    If strContinuousCommand <> "" Then
    
        strContinuousCommand = strContinuousCommand & " && "
    End If
    
    With New Scripting.FileSystemObject
    
        strContinuousCommand = strContinuousCommand & .GetDriveName(vstrWorkingDirectoryPath) & " && cd " & vstrWorkingDirectoryPath
    End With

    If vstrAddingCmdEnvironmentPath <> "" Then
    
        If strContinuousCommand <> "" Then
        
            strContinuousCommand = strContinuousCommand & " && "
        End If
    
        strContinuousCommand = strContinuousCommand & "SET PATH=%PATH%;" & vstrAddingCmdEnvironmentPath
    End If

    strCommand = "cmd.exe /k """ & strContinuousCommand & """"
    
    VBA.Shell strCommand, vbNormalFocus
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>rstrLogText: Output</Argument>
''' <Argument>rstrStdOut: Input</Argument>
''' <Argument>rstrStdErr: Input</Argument>
Private Sub msubGetCmdExecutionLogTextForGeneral(ByRef rstrLogText As String, _
        ByRef rstrStdOut As String, _
        ByRef rstrStdErr As String)

    If rstrStdErr <> "" Then
    
        rstrLogText = rstrLogText & rstrStdErr
    End If

    If rstrStdOut <> "" Then
    
        If rstrLogText <> "" Then
            
            rstrLogText = rstrLogText & vbNewLine
        End If
        
        rstrLogText = rstrLogText & rstrStdOut
    End If
End Sub

