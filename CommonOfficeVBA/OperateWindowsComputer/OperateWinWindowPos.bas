Attribute VB_Name = "OperateWinWindowPos"
'
'   Set a specified window position by Windows API
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  1/Jun/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then

    Private Declare PtrSafe Function SetForegroundWindow Lib "user32" (ByVal hwnd As LongPtr) As Long

    Private Declare PtrSafe Function GetForegroundWindow Lib "user32" () As LongPtr

    Private Declare PtrSafe Function SetWindowPos Lib "user32" (ByVal hwnd As LongPtr, ByVal hWndInsertAfter As LongPtr, ByVal x As LongPtr, ByVal y As LongPtr, ByVal cx As LongPtr, ByVal cy As LongPtr, ByVal uFlags As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As LongPtr
    
    Private Declare PtrSafe Function GetWindowInfo Lib "user32" (ByVal hwnd As LongPtr, ByRef rstdWindowInfo As WindowInfoType) As Boolean
    
    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
#Else
    Private Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long

    Private Declare Function GetForegroundWindow Lib "user32" Alias "GetForegroundWindow" () As Long

    Private Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
    
    Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
    
    Private Declare Function GetWindowInfo Lib "user32" (ByVal hWnd As Long, ByRef rstdWindowInfo As WindowInfoType) As Boolean
    
    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
#End If

Private Const SWP_VISIBLE = &H40 ' SWP_SHOWWINDOW - Displays the window.
Private Const SWP_NOMOVE = &H2  ' Retains the current position (ignores X and Y parameters).
Private Const SWP_NOSIZE = &H1  ' Retains the current size (ignores the cx and cy parameters).

Private Const HWND_TOP = 0  ' Places the window at the top of the Z order.
Private Const HWND_BOTTOM = 1   ' Places the window at the bottom of the Z order.
Private Const HWND_TopMost = -1 ' Places the window above all non-topmost windows.
Private Const HWND_RemoveTopMost = -2   ' HWND_NOTOPMOST - Places the window above all non-topmost windows (that is, behind all topmost windows).


Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type WindowInfoType
    cbSize As Long              ' The size of the structure, in bytes. The caller must set this member to sizeof(WINDOWINFO)
    rcWindow As RECT            ' The coordinates of the window.
    rcClient As RECT            ' The coordinates of the client area.
    dwStyle As Long             ' The window styles. For a table of window styles, see Window Styles.
    dwExStyle As Long           ' The extended window styles. For a table of extended window styles, see Extended Window
    dwWindowStatus As Long      ' The window status. If this member is WS_ACTIVECAPTION (0x0001), the window is active. Otherwise, this member is zero.
    cxWindowBorders As Long     ' The width of the window border, in pixels.
    cyWindowBorders As Long     ' The height of the window border, in pixels.
    atomWindowType As Integer   ' The window class atom (see RegisterClass).
    wCreatorVersion As Integer  ' The Windows version of the application that created the window.
End Type

'''
''' The following are the window styles. After the window has been created, these styles cannot be modified, except as noted.
'''
Private Enum WINDOWS_STYLES
    WS_BORDER = &H800000        ' The window has a thin-line border.
    WS_CAPTION = &HC00000       ' Allow to include the window title bar (includes the WS_BORDER style).
    WS_CHILD = &H40000000       ' The window is a child window. A window with this style cannot have a menu bar. This style cannot be used with the WS_POPUP style.
    WS_CLIPCHILDREN = &H2000000 ' Same as the WS_CHILD style.
    WS_CLIPSIBLINGS = &H4000000 ' Excludes the area occupied by child windows when drawing occurs within the parent window. This style is used when creating the parent window.
    WS_DISABLED = &H8000000     ' The window is initially disabled. A disabled window cannot receive input from the user. To change this after a window has been created, use the EnableWindow function.
    WS_DLGFRAME = &H400000      ' The window has a border of a style typically used with dialog boxes. A window with this style cannot have a title bar.
    WS_HSCROLL = &H100000       ' The window has a horizontal scroll bar.
    WS_MINIMIZE = &H20000000    ' The window is initially minimized. Same as the WS_ICONIC style.
    WS_MAXIMIZE = &H1000000     ' The window is initially maximized.
    WS_MAXIMIZEBOX = &H10000    ' At the right-top of the window, allow to include the maximize button, which is combined with the WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be included.
    WS_MINIMIZEBOX = &H20000    ' At the right-top of the window, allow to include the minimize button. Cannot be combined with the WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be specified.
    WS_OVERLAPPED = &H0         ' The window is an overlapped window. An overlapped window has a title bar and a border. Same as the WS_TILED style.
    WS_POPUP = &H80000000       ' The window is a pop-up window. This style cannot be used with the WS_CHILD style.
    WS_SIZEBOX = &H40000        ' The window has a sizing border. Same as the WS_THICKFRAME style.
    WS_SYSMENU = &H80000        ' Create the control menu boxes with the window style. The window has a window menu on its title bar. The WS_CAPTION style must also be specified.
    WS_VISIBLE = &H10000000     ' The window is initially visible. This style can be turned on and off by using the ShowWindow or SetWindowPos function.
    WS_VSCROLL = &H200000       ' The window has a vertical scroll bar.
End Enum

'''
''' he following are the extended window styles.
'''
Private Enum EXTENDED_WINDOWS_STYLES
    WS_EX_CLIENTEDGE = &H200    ' The window has a border with a sunken edge.
    WS_EX_CONTEXTHELP = &H400   ' The title bar of the window includes a question mark. When the user clicks the question mark, the cursor changes to a question mark with a pointer. If the user then clicks a child window, the child receives a WM_HELP message. The child window should pass the message to the parent window procedure, which should call the WinHelp function using the HELP_WM_HELP command. The Help application displays a pop-up window that typically contains help for the child window.
    WS_EX_DLGMODALFRAME = &H1   ' Add a double border with the extended windows style (Dialog modal frame). It is necessary to add the title-bar. The window has a double border; the window can, optionally, be created with a title bar by specifying the WS_CAPTION style in the dwStyle parameter.
    WS_EX_NOACTIVATE = &H8000000 ' A top-level window created with this style does not become the foreground window when the user clicks it. The system does not bring this window to the foreground when the user minimizes or closes the foreground window. The window should not be activated through programmatic access or via keyboard navigation by accessible technology, such as Narrator. To activate the window, use the SetActiveWindow or SetForegroundWindow function. The window does not appear on the taskbar by default. To force the window to appear on the taskbar, use the WS_EX_APPWINDOW style.
    WS_EX_TOPMOST = &H8         ' The window should be placed above all non-topmost windows and should stay above them, even when the window is deactivated. To add or remove this style, use the SetWindowPos function.
    WS_EX_TRANSPARENT = &H20    ' The window should not be painted until siblings beneath the window (that were created by the same thread) have been painted. The window appears transparent because the bits of underlying sibling windows have already been painted. To achieve transparency without these restrictions, use the SetWindowRgn function.
End Enum


Private Const mstrCommandPromptWindowClassName As String = "CASCADIA_HOSTING_WINDOW_CLASS,ConsoleWindowClass" ' cmd.exe

Private Const mstrVBEWindowClassName = "wndclass_desked_gsk"  ' Visual Basic Editor

Private Const mstrEXCELWindowClassName = "XLMAIN" ' Excel Application



'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
Private Sub OpenVBECodePaneRelativeToOperateWinWindowPos()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "FormTopMostEnabledCtlHdr,EnumerateAppWindows,EnumerateAppWindowsBasic"
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' If you need to control VBE UI by SendKey, the following should have helped you
'''
Public Sub SetWindowForegroundAboutVBE()

    Dim varHWnd As Variant, strClassName As String, intRet As Long
    
#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    intHWnd = GetForegroundWindow()

    If intHWnd > 0 Then

        strClassName = GetClassNameVBAString(intHWnd)

        ' If the foreground window is a Visual Basic Editor, then do nothing.

        If mstrVBEWindowClassName <> strClassName Then

            With GetVBEHwndToWindowCaptionDic()
            
                For Each varHWnd In .Keys
                
                    intHWnd = varHWnd
                
                    intRet = SetForegroundWindow(intHWnd)
                    
                    Interaction.DoEvents
                    
                    Sleep 50
                    
                    If intRet = 0 Then
                    
                        Debug.Print "Failed to SetForegroundWindow VBE"
                    End If
                Next
            End With
        End If
    End If
End Sub




