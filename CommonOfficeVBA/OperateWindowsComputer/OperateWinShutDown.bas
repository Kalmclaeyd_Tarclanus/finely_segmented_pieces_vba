Attribute VB_Name = "OperateWinShutDown"
'
'   Utility for Windows shut-down and restart
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       Dependent on InterfaceCall.bas
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 31/May/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then
    ' For 64 bit version windows, declare timeGetTime
    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
#Else
    ' For 32 bit version windows, declare timeGetTime
    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
#End If


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** This PC shut down tool
'**---------------------------------------------
'''
''' shut this Windows computer down with logging regardless of this user account lock state
'''
Public Sub ShutdownThisWindowsWithLoggingRegardlessOfAccountLock(Optional ByVal vstrShutdownTypeSuffixComment As String = "")
    
    msubWriteShutdownLog vstrShutdownTypeSuffixComment
    
    ' Next following option setting of the shutdown.exe execution, then this Windows will shut down.
    msubShutdownWithSaveThisBook 100, " /s /f /t 20"
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrGetLogForShutdown(Optional ByVal vstrShutdownTypeSuffixComment As String = "") As String
    
    Dim strLog As String, strPath As String
    
    With GetCurrentOfficeFileObject()
    
        strPath = .FullName
    End With
    
    With New Scripting.FileSystemObject
    
        If vstrShutdownTypeSuffixComment <> "" Then
    
            strLog = "Windows Shutdown" & vstrShutdownTypeSuffixComment & "," & .GetFileName(strPath) & "," & Format(Now(), "yyyy/m/d hh:mm:ss")
        Else
            strLog = "Windows Shutdown, " & .GetFileName(strPath) & "," & Format(Now(), "yyyy/m/d hh:mm:ss")
        End If
    End With

    With New CurrentUserDomain
    
        strLog = strLog & "," & CStr(.CurrentUserName) & "," & CStr(.CurrentNodePCName) & "," & CStr(.MACAddress)
    End With

    mfstrGetLogForShutdown = strLog
End Function


Private Function mfstrGetLogFieldTitlesForShutdown() As String
    
    Dim strFieldTitles As String
    
    strFieldTitles = "OSOperationType,OfficeFileName,DateTime,CurrentUser,ThisComputerName,ThisComputerMACAddress"
 
    mfstrGetLogFieldTitlesForShutdown = strFieldTitles
End Function


'''
'''
'''
Private Function mfstrGetLoggingFilePath() As String

    Dim strBookPath As String, strDir As String, strLogPath As String

    Const strLogFileName As String = "ShutdownLogging.csv"
    
    Const strLogDir As String = "VBALog"


    With GetCurrentOfficeFileObject()
    
        strBookPath = .FullName
    End With
    
    With New Scripting.FileSystemObject
    
        strDir = .GetParentFolderName(strBookPath) & "\" & strLogDir
        
        ForceToCreateDirectory strDir
        
        strLogPath = strDir & "\" & strLogFileName
    End With

    mfstrGetLoggingFilePath = strLogPath
End Function


'''
'''
'''
Private Sub msubWriteShutdownLog(Optional ByVal vstrShutdownTypeSuffixComment As String = "")

    Dim strLogPath As String, objTS As Scripting.TextStream
    
    On Error Resume Next

    With New Scripting.FileSystemObject
        
        strLogPath = mfstrGetLoggingFilePath()
        
        If .FileExists(strLogPath) Then
        
            Set objTS = .OpenTextFile(strLogPath, ForAppending, False)
        Else
            Set objTS = .CreateTextFile(strLogPath, False)
            
            With objTS
            
                .WriteLine mfstrGetLogFieldTitlesForShutdown()
            End With
        End If
    
        If Not objTS Is Nothing Then
            
            With objTS
            
                .WriteLine mfstrGetLogForShutdown(vstrShutdownTypeSuffixComment)
            
                .Close
            End With
        End If
    End With

    On Error GoTo 0

End Sub


'''
''' using Shutdown.exe
'''
Private Sub msubShutdownWithSaveThisBook(ByVal vintSleepMiliSecond As Long, Optional ByVal vstrShutdownOptionsString As String = " /s /f")
    
    Dim strCommand As String

    With GetCurrentOfficeFileObject()   ' this is supported for Excel, Word, and PowerPoint
    
        If Not .Saved Then
        
            .Save
        End If
    End With

    Sleep vintSleepMiliSecond

    strCommand = "shutdown" & vstrShutdownOptionsString

    ShellExectionWithoutWatingByFirstWshShellRunNextRetryVBAShell strCommand

    
    With GetCurrentOfficeFileObject()
    
        .Close
    
        With .Application
        
            .Quit
        End With
    End With
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Auto this PC shutdown tests
'**---------------------------------------------
'''
''' Testing to shut down in the account lock.
'''
''' <Test Procedure>
'''     Start up this VB editor of the VB project, and then run this macro.
'''     As soon as started, start the control-system window, which has some options of security and task manager and trouble shootings, by pressing Ctrl+Alt+Del keys
'''     By pressing Ctrl+k cause the Window system account lock.
'''     Next you must wait, then the Windows system should have shutted down.
''' </Test Procedure>
Public Sub SanityTestOfShutdownWithSaveThisBook01()

    msubShutdownWithSaveThisBook 20000

End Sub

'''
''' Testing to shut down in the account lock.
'''
Public Sub SanityTestOfShutdownWithSaveThisBook02()

    msubShutdownWithSaveThisBook 25000, " /s /f /t 20"
End Sub


'''
''' Test option pattern third
'''
Public Sub SanityTestOfShutdownWithSaveThisBook03()

    msubShutdownWithSaveThisBook 25000, " /s /f /t 10"
End Sub

'''
'''
'''
Private Sub msubSanityTestOfWriteShutdownLog()

    Debug.Print mfstrGetLogForShutdown()

    ' msubWriteShutdownLog
End Sub

