Attribute VB_Name = "UTfSortGeneral"
'
'   Sanity tests of traditional sort implementation
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 11/Jun/2022    Kalmclaeyd Tarclanus    Started refactoring
'

Option Explicit

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
''' random sequence type for unit test
'''
Public Enum SortTestingRandomSequenceType

    RandomNaturalIntegerForTest
    
    RandomCharacterForTest
End Enum

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
'**---------------------------------------------
'** local logging keys
'**---------------------------------------------
Private Const mstrElapsedSortingTimeBySecond As String = "ElapsedSortingTime"

Private Const mstrElapsedCreatingRandomShequenceTimeBySecond As String = "ElapsedCreatingRandomSequenceTime"

Private Const mstrElapsedOutputtingExcelSheetSecondTimeBySecond As String = "ElapsedOutputtingExcelSheetTime"


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToUTfSortGeneral()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "SortGeneral,CreateRandomValue,CreateRandomSequence,UTfCreateDataTable,CreateRandomDataTable"
End Sub

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetDTColForComprehensiveQuickSortPerformanceTests()

    Dim objDTCol As Collection: Set objDTCol = New Collection

    GetDTColForComprehensiveQuickSortPerformanceTests objDTCol, 10

    DebugCol objDTCol
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Traditional sort comprehensive test without outputting result to Excel sheet
'**---------------------------------------------
'''
'''
'''
Public Sub GetDTColForComprehensiveQuickSortPerformanceTests(ByRef robjDTCol As Collection, ByVal vintCountOfArray As Long)
    
    Dim objLogs As Collection
    Dim varTestingRandomSequenceType As Variant, varSortOrder As Variant
    Dim varInputArray() As Variant, varOutputArray() As Variant

    ' About QuickSortArrayType, QuickSort02ArrayType, MergeSortArrayType, QuickSort02ArrayUsingFunctionCallType

    For Each varTestingRandomSequenceType In GetSortTestingRandomSequenceTypesCol()
    
        For Each varSortOrder In GetSortOrderTypesCol()
    
            Set objLogs = Nothing
    
            GetLogsAndIOArrayAfterSortTestingRandomArray objLogs, varInputArray, varOutputArray, vintCountOfArray, varSortOrder, varTestingRandomSequenceType
            
            robjDTCol.Add mfobjGetLogColFromSortedResult(objLogs, vintCountOfArray, varSortOrder, varTestingRandomSequenceType)
        Next
    Next
End Sub


'''
''' test plural sorting alogorithms
'''
Public Sub GetLogsAndIOArrayAfterSortTestingRandomArray(ByRef robjLogs As Collection, _
        ByRef rvarInputArray() As Variant, _
        ByRef rvarOutputArray() As Variant, _
        ByVal vintCountOfArray As Long, _
        Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending, _
        Optional ByVal venmTestingRandomSequenceType As SortTestingRandomSequenceType = SortTestingRandomSequenceType.RandomNaturalIntegerForTest)
    
    
    Dim objSheet As Excel.Worksheet
    
    With New DoubleStopWatch
    
        .MeasureStart
    
        GetRandomSomeTypeSequenceForTest rvarInputArray, vintCountOfArray, venmTestingRandomSequenceType

        .MeasureInterval

        If robjLogs Is Nothing Then Set robjLogs = New Collection
        
        robjLogs.Add .ElapsedSecondTime, key:=mstrElapsedCreatingRandomShequenceTimeBySecond
    End With

    ' keep the varInputArray sequence
    CopyArray rvarOutputArray, rvarInputArray
    
    ' execute sort
    msubSortArrayAndGetLog robjLogs, rvarOutputArray, venmSortOrder
End Sub


'''
'''
'''
Private Sub msubSortArrayAndGetLog(ByRef robjLogs As Collection, ByRef rvarArray() As Variant, ByVal venmSortOrder As SortOrder)

    With New DoubleStopWatch
        
        .MeasureStart

        QuickSortArray rvarArray, 1, UBound(rvarArray), venmSortOrder

        .MeasureInterval
        
        If robjLogs Is Nothing Then Set robjLogs = New Collection
        
        robjLogs.Add .ElapsedSecondTime, key:=mstrElapsedSortingTimeBySecond
    End With
End Sub


'**---------------------------------------------
'** About testing conditions
'**---------------------------------------------
'''
'''
'''
Public Function GetSortOrderTypesCol() As Collection

    Dim objSortOrders As Collection

    Set objSortOrders = New Collection
    
    With objSortOrders
        
        .Add SortOrder.Ascending
        
        .Add SortOrder.Decending
    End With

    Set GetSortOrderTypesCol = objSortOrders
End Function

'''
'''
'''
Public Function GetSortTestingRandomSequenceTypesCol() As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add SortTestingRandomSequenceType.RandomNaturalIntegerForTest
        
        .Add SortTestingRandomSequenceType.RandomCharacterForTest
    End With
    
    Set GetSortTestingRandomSequenceTypesCol = objCol
End Function


'**---------------------------------------------
'** About getting an initial random sequence
'**---------------------------------------------
'''
'''
'''
Public Sub GetRandomSomeTypeSequenceForTest(ByRef rvarOutputRandomArray As Variant, ByVal vintCountOfArray As Long, Optional ByVal venmTestingRandomSequenceType As SortTestingRandomSequenceType = SortTestingRandomSequenceType.RandomNaturalIntegerForTest)

    Select Case venmTestingRandomSequenceType
    
        Case SortTestingRandomSequenceType.RandomNaturalIntegerForTest
        
            msubGetRandomNaturalNumberSequenceForTest rvarOutputRandomArray, vintCountOfArray
        
        Case SortTestingRandomSequenceType.RandomCharacterForTest
        
            msubGetRandomUpperCharacterSequenceForTest rvarOutputRandomArray, vintCountOfArray, 15
    End Select

End Sub

'''
'''
'''
Public Function GetRandomSequenceTypeName(ByVal venmTestingRandomSequenceType As SortTestingRandomSequenceType) As String
    
    Dim strName As String
    
    strName = ""
    
    Select Case venmTestingRandomSequenceType
    
        Case SortTestingRandomSequenceType.RandomNaturalIntegerForTest
        
            strName = "Random natural integer"
        
        Case SortTestingRandomSequenceType.RandomCharacterForTest
        
            strName = "Randam characters string"
    End Select

    GetRandomSequenceTypeName = strName
End Function

'''
'''
'''
Public Function GetTestingRandomSequenceDataTypeString(ByVal venmTestingRandomSequenceType As SortTestingRandomSequenceType) As String
    
    Dim strDataType As String
    
    Select Case venmTestingRandomSequenceType
    
        Case SortTestingRandomSequenceType.RandomNaturalIntegerForTest
        
            strDataType = "Natual-number"
            
        Case SortTestingRandomSequenceType.RandomCharacterForTest
        
            strDataType = "Random-alphabet-sequence"
    End Select
    
    GetTestingRandomSequenceDataTypeString = strDataType
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** Traditional sort comprehensive test without outputting result to Excel sheet
'**---------------------------------------------
'''
''' get log Collection
'''
Private Function mfobjGetLogColFromSortedResult(ByVal vobjLogs As Collection, ByVal vintCountOfArray As Long, ByVal venmSortOrder As SortOrder, ByVal venmTestingRandomSequenceType As SortTestingRandomSequenceType) As Collection

    Dim objRowCol As Collection
    
    Set objRowCol = New Collection
    
    With objRowCol
    
        .Add vintCountOfArray
        
        .Add vobjLogs.Item(mstrElapsedSortingTimeBySecond)
        
        .Add vobjLogs.Item(mstrElapsedCreatingRandomShequenceTimeBySecond)
    
        .Add GetRandomSequenceTypeName(venmTestingRandomSequenceType)
        
        .Add GetSortOrderString(venmSortOrder)
    End With

    Set mfobjGetLogColFromSortedResult = objRowCol
End Function


'**---------------------------------------------
'** About getting an initial random sequence
'**---------------------------------------------
'''
'''
'''
Private Sub msubGetRandomNaturalNumberSequenceForTest(ByRef rvarOutputRandomArray As Variant, ByVal vintCountOfArray As Long)
    
    Dim i As Long
    
    ReDim rvarOutputRandomArray(1 To vintCountOfArray)
    
    VBA.Math.Randomize
    
    For i = 1 To vintCountOfArray
        
        rvarOutputRandomArray(i) = Int((vintCountOfArray * 10) * VBA.Math.Rnd() + 1)
    Next
End Sub


'''
'''
'''
Private Sub msubGetRandomUpperCharacterSequenceForTest(ByRef rvarOutputRandomArray As Variant, ByVal vintCountOfArray As Long, ByVal vintStringLength As Long)
    
    Dim i As Long
    
    ReDim rvarOutputRandomArray(1 To vintCountOfArray)
    
    VBA.Math.Randomize
    
    For i = 1 To vintCountOfArray
        
        rvarOutputRandomArray(i) = GetUpperCaseAlphabetRandomString(vintStringLength, "A", "Z")
    Next
End Sub


