Attribute VB_Name = "SortGeneral"
'
'   Traditional sort implementation by VBA Variant type array
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Some algorithms should have looked like either uncompleted or naive, if you are a IT architect, a computer scientist, or a mathematician.
'       It is not a open-source project purpose that the completed algorithms are adopted in all individual processes.
'       After I have confirmed both the users' needs and my sufficient funds, I will investigate to improve the concerned algorithms.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 19/Apr/2019    Mr. Oomura              A basic part has been published and updated at https://gihyo.jp/book/2016/978-4-7741-8177-6/support
'       Sat, 11/Jun/2022    Kalmclaeyd Tarclanus    Started refactoring and added new functions
'

Option Explicit

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
'''
'''
Public Enum SortOrder

    Ascending = 0
    
    Decending = 1
End Enum


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToSortGeneral()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UTfSortGeneral,PTfSortGeneral,CreateRandomValue,CreateRandomSequence"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** a special sort, which is a little hard to explain by SQL
'**---------------------------------------------
'''
'''
'''
Public Function GetSortedDTColBySeparatedIsNullFirstColumnWithinGroupedSecondColumnByFieldTitles(ByRef robjInputDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        ByVal vstrValueIsNullOrNotColumnTitle As String, _
        ByVal vstrSameValueGroupColumnTitlesDelimitedByComma As String) As Collection


    Dim intValueIsNullOrNotColumnIndex As Long, varTitle As Variant, intIndex As Long, strSameValueGroupColumnIndexesDelimitedByComma As String


    intValueIsNullOrNotColumnIndex = GetFieldTitleIndexFromCol(vstrValueIsNullOrNotColumnTitle, robjFieldTitlesCol)

    strSameValueGroupColumnIndexesDelimitedByComma = ""

    For Each varTitle In Split(vstrSameValueGroupColumnTitlesDelimitedByComma, ",")
    
        intIndex = GetFieldTitleIndexFromCol(varTitle, robjFieldTitlesCol)
        
        If strSameValueGroupColumnIndexesDelimitedByComma <> "" Then
        
            strSameValueGroupColumnIndexesDelimitedByComma = strSameValueGroupColumnIndexesDelimitedByComma & ","
        End If
        
        strSameValueGroupColumnIndexesDelimitedByComma = strSameValueGroupColumnIndexesDelimitedByComma & CStr(intIndex)
    Next
    
    Set GetSortedDTColBySeparatedIsNullFirstColumnWithinGroupedSecondColumnByFieldTitles = GetSortedDTColBySeparatedIsNullFirstColumnWithinGroupedSecondColumn(robjInputDTCol, _
            intValueIsNullOrNotColumnIndex, _
            strSameValueGroupColumnIndexesDelimitedByComma)
End Function


'''
''' It is a little hard to describe SQL.
'''
''' <Argument>robjInputDTCol: Input</Argument>
''' <Argument>vintValueIsNullOrNotColumnIndex: Input</Argument>
''' <Argument>vstrSameValueGroupColumnIndexesDelimitedByComma: Input</Argument>
''' <Return>Collection</Return>
Public Function GetSortedDTColBySeparatedIsNullFirstColumnWithinGroupedSecondColumn(ByRef robjInputDTCol As Collection, _
        ByVal vintValueIsNullOrNotColumnIndex As Long, _
        ByVal vstrSameValueGroupColumnIndexesDelimitedByComma As String) As Collection


    Dim objDTCol As Collection, varRowCol As Variant, objRowCol As Collection
    Dim varIsNullOrNot As Variant, varIndex As Variant, varGroupValue As Variant
    Dim objSameValueGroupColumnIndexesCol As Collection
    
    Dim objGroupToNullOrNotToRowColDic As Scripting.Dictionary  ' Dictionary(Of GroupValue, Dictionary(Of NullOrNot, Of Collection[RowCol]))
    Dim objNullOrNotToRowColDic As Scripting.Dictionary, objPartDTCol As Collection
    
    
    Set objDTCol = New Collection
    
    Set objSameValueGroupColumnIndexesCol = GetIntegerColFromLineDelimitedChar(vstrSameValueGroupColumnIndexesDelimitedByComma)
    
    
    For Each varRowCol In robjInputDTCol
    
        Set objRowCol = varRowCol

        With objRowCol
        
            varIsNullOrNot = .Item(vintValueIsNullOrNotColumnIndex)
    
            varGroupValue = ""
            
            For Each varIndex In objSameValueGroupColumnIndexesCol
            
                If varGroupValue <> "" Then
                
                    varGroupValue = varGroupValue & "_"
                End If
            
                varGroupValue = varGroupValue & CStr(objRowCol.Item(varIndex))
            Next
        End With

        If objGroupToNullOrNotToRowColDic Is Nothing Then Set objGroupToNullOrNotToRowColDic = New Scripting.Dictionary

        With objGroupToNullOrNotToRowColDic
        
            If Not .Exists(varGroupValue) Then
            
                Set objNullOrNotToRowColDic = New Scripting.Dictionary
                
                .Add varGroupValue, objNullOrNotToRowColDic
            Else
                Set objNullOrNotToRowColDic = .Item(varGroupValue)
            End If
        
            With objNullOrNotToRowColDic
            
                Select Case True
                
                    Case IsEmpty(varIsNullOrNot), IsNull(varIsNullOrNot)
            
                        If Not .Exists(False) Then
                            
                            Set objPartDTCol = New Collection
                        
                            .Add False, objPartDTCol
                        Else
                            Set objPartDTCol = .Item(False)
                        End If
                    
                        objPartDTCol.Add objRowCol
                        
                    Case Else
                    
                        If Not .Exists(True) Then
                        
                            Set objPartDTCol = New Collection
                        
                            .Add True, objPartDTCol
                        Else
                            Set objPartDTCol = .Item(True)
                        End If
                    
                        objPartDTCol.Add objRowCol
                End Select
            End With
        End With
    Next

    If Not objGroupToNullOrNotToRowColDic Is Nothing Then
    
        With objGroupToNullOrNotToRowColDic
        
            For Each varGroupValue In .Keys
            
                Set objNullOrNotToRowColDic = .Item(varGroupValue)
        
                With objNullOrNotToRowColDic
                
                    If .Exists(True) Then
                    
                        Set objPartDTCol = .Item(True)
                        
                        UnionDoubleCollectionsToSingle objDTCol, objPartDTCol
                    End If
                
                    If .Exists(False) Then
                        
                        Set objPartDTCol = .Item(False)
                        
                        UnionDoubleCollectionsToSingle objDTCol, objPartDTCol
                    End If
                End With
            Next
        End With
    End If

    Set GetSortedDTColBySeparatedIsNullFirstColumnWithinGroupedSecondColumn = objDTCol
End Function

'**---------------------------------------------
'** Tools for SortOrder enumeration
'**---------------------------------------------
'''
'''
'''
Private Function GetSortOrderEnumValueFromText(ByVal vstrSortOrderText As String) As SortOrder

    Dim enmSortOrder As SortOrder

    Select Case vstrSortOrderText
    
        Case "Ascending"
    
            enmSortOrder = SortOrder.Ascending
    
        Case "Decending"
    
            enmSortOrder = SortOrder.Decending
    End Select

    GetSortOrderEnumValueFromText = enmSortOrder
End Function



'''
''' from continuous text delimiting some character, get Dictionary(Of String, SortOrder)
'''
Public Function GetTextToSortOrderDicFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, i As Long, strArray() As String
    
    Set objDic = New Scripting.Dictionary
    
    If vstrLineDelimited <> "" Then
    
        strArray = Split(vstrLineDelimited, vstrDelimiter)
    
        For i = LBound(strArray) To UBound(strArray) Step 2
        
            If i + 1 <= UBound(strArray) And Not IsEmpty(Trim(strArray(i + 1))) Then
            
                objDic.Add Trim(strArray(i)), GetSortOrderEnumValueFromText(Trim(strArray(i + 1)))
            End If
        Next
    End If
     
    Set GetTextToSortOrderDicFromLineDelimitedChar = objDic
End Function


'**---------------------------------------------
'** About Collection and Scripting.Dictionary
'**---------------------------------------------
'''
'''
'''
''' <Argument>robjCol: 2-dimentional col, Collection(Of Collection[Row Collection]) this is changed</Argument>
''' <Argument>venmSortOrder: Input</Argument>
Public Sub SortDataTableCollection(ByRef robjDataTableCol As Collection, Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending)

    Dim objCol As Collection, objDictionaryOfSortKeyToRowCol As Scripting.Dictionary
    
    
    Set objDictionaryOfSortKeyToRowCol = mfobjGetDicFromDataTableCol(robjDataTableCol)
    
    SortDictionaryAboutKeys objDictionaryOfSortKeyToRowCol, venmSortOrder
    
    Set objCol = GetEnumeratorValuesColFromDic(objDictionaryOfSortKeyToRowCol, True)
    
    Set robjDataTableCol = objCol
End Sub


'''
'''
'''
Public Sub SortDataTableCollectionBySpecifiedOneColumn(ByRef robjDataTableCol As Collection, _
        ByVal vintColumnIndexAsKey As Long, _
        Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending)

    Dim objCol As Collection, objDictionaryOfSortKeyToRowCol As Scripting.Dictionary
    
    
    Set objDictionaryOfSortKeyToRowCol = mfobjGetDicFromDataTableColWithSpecifyingOneColumnIndex(robjDataTableCol, vintColumnIndexAsKey)
    
    SortDictionaryAboutKeys objDictionaryOfSortKeyToRowCol, venmSortOrder
    
    Set objCol = GetEnumeratorValuesColFromDic(objDictionaryOfSortKeyToRowCol, True)
    
    Set robjDataTableCol = objCol
End Sub

'''
'''
'''
''' <Argument>robjCol: 2-dimentional col, Collection(Of Collection[Row Collection]) this is changed</Argument>
''' <Argument>venmSortOrder: Input</Argument>
Public Function GetSortDataTableCollection(ByVal vobjInputDataTableCol As Collection, _
        Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending) As Collection
    
    Dim objCol As Collection, objDictionaryOfSortKeyToRowCol As Scripting.Dictionary
    
    
    Set objDictionaryOfSortKeyToRowCol = mfobjGetDicFromDataTableCol(vobjInputDataTableCol)
    
    SortDictionaryAboutKeys objDictionaryOfSortKeyToRowCol, venmSortOrder
    
    Set objCol = GetEnumeratorValuesColFromDic(objDictionaryOfSortKeyToRowCol, True)
    
    Set GetSortDataTableCollection = objCol
End Function


'''
''' use the QuickSort as defined below
'''
''' <Argument>robjCol: this is changed</Argument>
''' <Argument>venmSortOrder: Input</Argument>
Public Sub SortCollection(ByRef robjCol As Collection, _
        Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending)

    Dim varArray() As Variant

    ConvertColToArray varArray, robjCol, True

    QuickSortArray varArray, 1, robjCol.Count, venmSortOrder
    
    Set robjCol = GetColFromArray(varArray)
End Sub


'''
'''
''' <Argument>vobjInputCol: this is not changed</Argument>
''' <Argument>venmSortOrder: Input</Argument>
Public Function GetSortedCollection(ByVal vobjInputCol As Collection, _
        Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending) As Collection

    Dim varArray() As Variant

    ConvertColToArray varArray, vobjInputCol, True

    QuickSortArray varArray, 1, vobjInputCol.Count, venmSortOrder
    
    Set GetSortedCollection = GetColFromArray(varArray)
End Function


'''
''' use the QuickSort as defined below
'''
''' <Argument>robjDic: this is changed</Argument>
''' <Argument>venmSortOrder: Input</Argument>
Public Sub SortDictionaryAboutKeys(ByRef robjDic As Scripting.Dictionary, _
        Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending)

    Dim varArray() As Variant

    If Not robjDic Is Nothing Then
        
        If robjDic.Count > 1 Then
            
            ConvertEnumeratorKeysToArray varArray, robjDic, True
        
            QuickSortArray varArray, 1, robjDic.Count, venmSortOrder
        
            Set robjDic = mfobjGetDicFromNewSequenceKeys(robjDic, varArray)
        End If
    End If
End Sub

'''
''' use the QuickSort as defined below
'''
''' <Argument>robjDic: Dictionary(Of Variant[String], Of Dictionary(Of Variant[String : child dictionary key], Of Variant(Something, for example, Collection))), this is changed</Argument>
''' <Argument>venmSortOrder: Input</Argument>
Public Sub SortDictionaryAboutValuesChildDictionaryKeys(ByRef robjDic As Scripting.Dictionary, _
        Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending)

    Dim varKey As Variant
    
    If Not robjDic Is Nothing Then
    
        With robjDic
            
            For Each varKey In .Keys

                ' sort each child dictionary
                Set .Item(varKey) = GetSortedDictionaryAboutKeys(.Item(varKey), venmSortOrder)
            Next
        End With
    End If
End Sub

'''
''' use the QuickSort as defined below
'''
''' <Argument>robjDic: Dictionary(Of Variant[String], Of Collection(Of Variant)), this is changed</Argument>
''' <Argument>venmSortOrder: Input</Argument>
Public Sub SortDictionaryAboutValuesCollection(ByRef robjDic As Scripting.Dictionary, _
        Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending)

    Dim varKey As Variant
    
    If Not robjDic Is Nothing Then
    
        With robjDic
            
            For Each varKey In .Keys

                ' sort each child collection
                Set .Item(varKey) = GetSortedCollection(.Item(varKey), venmSortOrder)
            Next
        End With
    End If
End Sub


'''
''' use the QuickSort as defined below
'''
''' <Argument>vobjInputDic: this is not changed</Argument>
''' <Argument>venmSortOrder: Input</Argument>
Public Function GetSortedDictionaryAboutKeys(ByVal vobjInputDic As Scripting.Dictionary, _
        Optional ByVal venmSortOrder As SortOrder = SortOrder.Ascending) As Scripting.Dictionary

    Dim varArray() As Variant, objDic As Scripting.Dictionary

    If Not vobjInputDic Is Nothing Then
        
        If vobjInputDic.Count > 1 Then
        
            ConvertEnumeratorKeysToArray varArray, vobjInputDic, True
        
            QuickSortArray varArray, 1, vobjInputDic.Count, venmSortOrder
        
            Set objDic = mfobjGetDicFromNewSequenceKeys(vobjInputDic, varArray)
        
        ElseIf vobjInputDic.Count = 1 Then
        
            Set objDic = vobjInputDic
        End If
    End If
    
    Set GetSortedDictionaryAboutKeys = objDic
End Function


'''
'''
'''
Private Function mfobjGetDicFromDataTableCol(ByVal vobjDataTableCol As Collection) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, varRowCol As Variant, objRowCol As Collection, strKey As String
    
    If Not vobjDataTableCol Is Nothing Then
        
        If vobjDataTableCol.Count > 0 Then
    
            Set objDic = New Scripting.Dictionary
            
            For Each varRowCol In vobjDataTableCol
                
                Set objRowCol = varRowCol
            
                strKey = GetLineTextWithLineFeedFromCol(objRowCol, vblnIncludeSpaceAfterDelimitedChar:=False)
            
                objDic.Add strKey, objRowCol
            Next
        End If
    End If
    
    Set mfobjGetDicFromDataTableCol = objDic
End Function

'''
'''
'''
Private Function mfobjGetDicFromDataTableColWithSpecifyingOneColumnIndex(ByVal vobjDataTableCol As Collection, _
        ByVal vintColumnIndexAsKey As Long) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, varRowCol As Variant, objRowCol As Collection, varKey As Variant, i As Long
    
    Dim strSuffixFormatString As String, strNumberFormatString As String
    
    Dim intDigits As Long, blnNoDuplicatedValueExists As Boolean, blnIsSpecifiedColumnStringType As Boolean
    
    Dim intMaxLongLongStringLength As Long
    
    Dim intBaseSize As Long
    
#If VBA7 Then

    Dim intBaseSizeOfLongLong As LongLong
#Else
    Dim intBaseSizeOfLongLong As Long
#End If
    
    
    If Not vobjDataTableCol Is Nothing Then
        
        If vobjDataTableCol.Count > 0 Then
    
            Set objDic = New Scripting.Dictionary
            
            blnNoDuplicatedValueExists = mfblnNoSpecifiedColumnDuplicatedValueExists(vobjDataTableCol, vintColumnIndexAsKey, intMaxLongLongStringLength)
            
            If blnNoDuplicatedValueExists And (intMaxLongLongStringLength < 0) Then
            
                 For Each varRowCol In vobjDataTableCol
                    
                    Set objRowCol = varRowCol
                
                    varKey = objRowCol.Item(vintColumnIndexAsKey)
                
                    objDic.Add varKey, objRowCol
                Next
                
            ElseIf blnNoDuplicatedValueExists And (intMaxLongLongStringLength >= 0) Then
            
                strNumberFormatString = String(intMaxLongLongStringLength, "0")
            
                For Each varRowCol In vobjDataTableCol
                    
                    Set objRowCol = varRowCol
                
                    varKey = Format(objRowCol.Item(vintColumnIndexAsKey), strNumberFormatString)
                
                    objDic.Add varKey, objRowCol
                Next
            Else
            
                intDigits = mintGetNecessaryDigitOfInteger(vobjDataTableCol.Count)
        
                i = 1
            
                Select Case TypeName(vobjDataTableCol.Item(1).Item(vintColumnIndexAsKey))
                
                    Case "String"
                
                        strSuffixFormatString = String(intDigits, "0")
                        
                        For Each varRowCol In vobjDataTableCol
                            
                            Set objRowCol = varRowCol
                        
                            varKey = objRowCol.Item(vintColumnIndexAsKey) & Format(i, strSuffixFormatString)
                        
                            objDic.Add varKey, objRowCol
                            
                            i = i + 1
                        Next
                        
                    Case "Long", "Integer"
                
                        intBaseSize = 10 ^ intDigits
                
                        For Each varRowCol In vobjDataTableCol
                            
                            Set objRowCol = varRowCol
                        
                            varKey = objRowCol.Item(vintColumnIndexAsKey) * intBaseSize + i
                        
                            objDic.Add varKey, objRowCol
                            
                            i = i + 1
                        Next
                        
                    Case "LongLong"
                
                        ' LongLong (64-bit) can be used in the Scripting.Dictionary key-items
                
                
                        strNumberFormatString = String(intMaxLongLongStringLength, "0")
                
                        strSuffixFormatString = String(intDigits, "0")
                
                        For Each varRowCol In vobjDataTableCol
                            
                            Set objRowCol = varRowCol
                        
                            varKey = Format(objRowCol.Item(vintColumnIndexAsKey), strNumberFormatString) & Format(i, strSuffixFormatString)
                        
                            objDic.Add varKey, objRowCol
                            
                            i = i + 1
                        Next
                        
                    Case "Double", "Float", "Date"
                
                        intBaseSize = 10 ^ intDigits
                
                        For Each varRowCol In vobjDataTableCol
                            
                            Set objRowCol = varRowCol
                        
                            varKey = CDbl(objRowCol.Item(vintColumnIndexAsKey) * intBaseSize) + CDbl(i)
                        
                            objDic.Add varKey, objRowCol
                            
                            i = i + 1
                        Next
                End Select
            End If
        End If
    End If
    
    Set mfobjGetDicFromDataTableColWithSpecifyingOneColumnIndex = objDic
End Function

'''
'''
'''
Private Function mfblnNoSpecifiedColumnDuplicatedValueExists(ByRef robjDTCol As Collection, _
        ByVal vintColumnIndexAsKey As Long, _
        ByRef rintStringLengthOfLongLong As Long) As Boolean

    Dim varRowCol As Variant, objRowCol As Collection, varItem As Variant
    
    Dim blnDuplicatedValueExists As Boolean, blnIsColumnDataTypeLongLong As Boolean
    
    Dim objDic As Scripting.Dictionary: Set objDic = New Scripting.Dictionary
    
    Dim intLengthOfLongLongText As Long, intCurrentLength As Long, strValue As String
    
    rintStringLengthOfLongLong = -1
    
    
    blnIsColumnDataTypeLongLong = False
    
    If TypeName(robjDTCol.Item(1).Item(vintColumnIndexAsKey)) = "LongLong" Then
    
        blnIsColumnDataTypeLongLong = True
        
        intLengthOfLongLongText = 0
    End If
    
    
    blnDuplicatedValueExists = False
    
    For Each varRowCol In robjDTCol
    
        Set objRowCol = varRowCol
        
        varItem = objRowCol.Item(vintColumnIndexAsKey)
    
        If blnIsColumnDataTypeLongLong Then
        
            strValue = CStr(varItem)
        
            intCurrentLength = Len(strValue)
        
            If intLengthOfLongLongText < intCurrentLength Then
            
                intLengthOfLongLongText = intCurrentLength
            End If
        
            varItem = strValue
        End If
    
        With objDic
        
            If Not .Exists(varItem) Then
            
                .Add varItem, 0
            Else
                blnDuplicatedValueExists = True
                
                Exit For
            End If
        End With
    Next
    
    If intLengthOfLongLongText > 0 Then
    
        rintStringLengthOfLongLong = intLengthOfLongLongText
    End If
    
    mfblnNoSpecifiedColumnDuplicatedValueExists = Not blnDuplicatedValueExists
End Function


'''
'''
'''
Private Function mintGetNecessaryDigitOfInteger(ByVal vintNumber As Long) As Long

    Dim i As Long, intMax As Long, intDigits As Long
    
    'intMax = 2147483647 ' Power(2, 32) / 2 - 1
    
    intMax = 1000000000
    
    i = 1

    intDigits = 1

    Do
        intDigits = intDigits * 10
    
        If (CDbl(vintNumber) / CDbl(intDigits)) <= 1# Then
        
            Exit Do
        End If
    
        i = i + 1
    
    Loop While intDigits < intMax

    mintGetNecessaryDigitOfInteger = i
End Function

'''
'''
'''
Private Function mfobjGetDicFromNewSequenceKeys(ByVal vobjInputDic As Scripting.Dictionary, ByVal varNewSequenceKeyArray As Variant) As Scripting.Dictionary

    Dim varKey As Variant, objDic As Scripting.Dictionary
    
    If Not vobjInputDic Is Nothing Then
    
        If vobjInputDic.Count > 1 Then
        
            Set objDic = New Scripting.Dictionary
            
            With vobjInputDic
            
                For Each varKey In varNewSequenceKeyArray
                
                    objDic.Add varKey, .Item(varKey)
                Next
            End With
        Else
            Set objDic = vobjInputDic
        End If
    End If

    Set mfobjGetDicFromNewSequenceKeys = objDic
End Function

'**---------------------------------------------
'** get SortOrder enumeration string
'**---------------------------------------------
Public Function GetSortOrderString(ByVal venmSortOrder As SortOrder) As String

    Dim strSortOrder As String
    
    strSortOrder = ""
    
    Select Case venmSortOrder
    
        Case SortOrder.Ascending
    
            strSortOrder = "Ascending"
            
        Case SortOrder.Decending
        
            strSortOrder = "Decending"
    End Select

    GetSortOrderString = strSortOrder
End Function

'**---------------------------------------------
'** Basic functions from internet published code samples
'**---------------------------------------------
'''
''' Traditional quick sort, this should have been fastest in some published sample algorithms.
'''
Public Sub QuickSortArray(ByRef rvarInputArray() As Variant, ByVal vintStartIndex As Long, ByVal vintEndIndex As Long, ByVal venmSortOrder As SortOrder)

    Dim varTmp  As Variant, varThresholdValue As Variant
    Dim i As Long, j As Long
    
    varThresholdValue = rvarInputArray((vintEndIndex + vintStartIndex) \ 2)
    
    i = vintStartIndex - 1
    
    j = vintEndIndex + 1
    
    Select Case venmSortOrder
    
        Case SortOrder.Ascending
        
            Do
                Do
                    i = i + 1
                Loop While rvarInputArray(i) < varThresholdValue
                
                Do
                    j = j - 1
                Loop While rvarInputArray(j) > varThresholdValue
                
                If i >= j Then Exit Do
                
                varTmp = rvarInputArray(j): rvarInputArray(j) = rvarInputArray(i): rvarInputArray(i) = varTmp
            Loop
            
        Case SortOrder.Decending
        
            Do
                Do
                    i = i + 1
                Loop While rvarInputArray(i) > varThresholdValue
                
                Do
                    j = j - 1
                Loop While rvarInputArray(j) < varThresholdValue
                
                If i >= j Then Exit Do
                
                varTmp = rvarInputArray(j): rvarInputArray(j) = rvarInputArray(i): rvarInputArray(i) = varTmp
            Loop
    End Select
    
    If i - vintStartIndex > 1 Then
    
        ' recursive call
        QuickSortArray rvarInputArray, vintStartIndex, i - 1, venmSortOrder
    End If
    
    If vintEndIndex - j > 1 Then
    
        ' recursive call
        QuickSortArray rvarInputArray, j + 1, vintEndIndex, venmSortOrder
    End If
End Sub





