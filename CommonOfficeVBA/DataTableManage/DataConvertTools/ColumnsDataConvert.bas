Attribute VB_Name = "ColumnsDataConvert"
'
'   Convert some type data from one type to another type
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 13/Jun/2022    Kalmclaeyd Tarclanus    Improved
'

Option Explicit


'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
'''
'''
Public Enum ColumnDataConvertType

    Digits8IntegersToDate       ' Long (8 digits) to Date
    
    Digits14StringToDateTime    ' String (14 digit characters) to Date
    
    DateTo8DigitsInteger        ' Date to Long (8 digits)
    
    DateTo14DigitsString        ' Date to String (14 digit characters)
    
    Digits8StringToDate         ' String (continuous 8 digits) to Date
    
    DeletePrefixSuffixSpacesAndLineFeed ' Delete ' ' and vbLf
End Enum


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** RegExp cache
'**---------------------------------------------
Private mobjDateFromContinuous8Digits As VBScript_RegExp_55.RegExp

Private mobjDateFromContinuous14Digits As VBScript_RegExp_55.RegExp


Private mobjHalfWidthKanaRegExp As VBScript_RegExp_55.RegExp

Private mobjFullWidthKanaRegExp As VBScript_RegExp_55.RegExp


Private mobjWideWidthAlphaNumRegExp As VBScript_RegExp_55.RegExp

Private mobjNarrowWidthNumbersRegExp As VBScript_RegExp_55.RegExp

Private mobjWideWidthParenRegExp As VBScript_RegExp_55.RegExp


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Columns data convert main
'**---------------------------------------------
'''
'''
'''
Public Function ConvertValueFromOneTypeToAnotherTypeBy(ByRef rvarValue As Variant, _
        ByVal venmColumnDataConvertType As ColumnDataConvertType) As Boolean
    
    Dim blnIsChanged As Boolean, strConverted As String
    
    blnIsChanged = False
    
    If Not IsEmpty(rvarValue) Then
    
        Select Case venmColumnDataConvertType
        
            Case ColumnDataConvertType.Digits8IntegersToDate
            
                If IsNumeric(rvarValue) Then
                
                    rvarValue = Convert8DigitsIntegersToDate(rvarValue)
                
                    blnIsChanged = True
                End If
            
            Case ColumnDataConvertType.Digits8StringToDate
            
                If Len(rvarValue) = 8 Then
                
                    rvarValue = Convert8DigitsStringToDate(rvarValue)
                    
                    blnIsChanged = True
                Else
                    rvarValue = ""
                    
                    blnIsChanged = True
                End If
            Case ColumnDataConvertType.Digits14StringToDateTime
                
                rvarValue = Convert14DigitsStringToDateTime(rvarValue)
            
                blnIsChanged = True
                
            Case ColumnDataConvertType.DateTo8DigitsInteger
            
                If IsDate(rvarValue) Then
                
                    rvarValue = ConvertDateTo8DigitsDateLong(rvarValue)
                
                    blnIsChanged = True
                End If
                
            Case ColumnDataConvertType.DateTo14DigitsString
            
                If IsDate(rvarValue) Then
                
                    rvarValue = ConvertDateTo14DigitsDateString(rvarValue)
                
                    blnIsChanged = True
                End If
                
            Case ColumnDataConvertType.DeletePrefixSuffixSpacesAndLineFeed
            
                strConverted = TrimAndDeleteLineFeed(rvarValue)
            
                If strConverted <> CStr(rvarValue) Then
                
                    rvarValue = strConverted
                    
                    blnIsChanged = True
                End If
            
        End Select
    
    End If
    
    ConvertValueFromOneTypeToAnotherTypeBy = blnIsChanged
End Function


'''
'''
'''
Public Function GetEnumLiteralOfColumnDataConvertType(ByVal venmColumnDataConvertType As ColumnDataConvertType) As String
    
    Dim strLiteral As String
    
    strLiteral = ""
    
    Select Case venmColumnDataConvertType
    
        Case ColumnDataConvertType.Digits8IntegersToDate
        
            strLiteral = "Digits8IntegersToDate"
        
        Case ColumnDataConvertType.Digits8StringToDate
        
            strLiteral = "Digits8StringToDate"
        
        Case ColumnDataConvertType.Digits14StringToDateTime
        
            strLiteral = "Digits14StringToDateTime"
        
        Case ColumnDataConvertType.DateTo8DigitsInteger
        
            strLiteral = "DateTo8DigitsInteger"
        
        Case ColumnDataConvertType.DateTo14DigitsString
        
            strLiteral = "DateTo14DigitsString"
    
        Case ColumnDataConvertType.DeletePrefixSuffixSpacesAndLineFeed
        
            strLiteral = "DeletePrefixSuffixSpacesAndLineFeed"
    End Select

    GetEnumLiteralOfColumnDataConvertType = strLiteral
End Function

'''
'''
'''
Public Function GetColumnDataConvertTypeFromString(ByVal vstrConvertType As String) As ColumnDataConvertType
    
    Dim enmValue As ColumnDataConvertType
    
    enmValue = 0
    
    Select Case Trim(vstrConvertType)
    
        Case "Digits8IntegersToDate"
        
            enmValue = ColumnDataConvertType.Digits8IntegersToDate
    
        Case "Digits8StringToDate"
        
            enmValue = ColumnDataConvertType.Digits8StringToDate
            
        Case "Digits14StringToDateTime"
        
            enmValue = ColumnDataConvertType.Digits14StringToDateTime
        
        Case "DateTo8DigitsInteger"
        
            enmValue = ColumnDataConvertType.DateTo8DigitsInteger
        
        Case "DateTo14DigitsString"
        
            enmValue = ColumnDataConvertType.DateTo14DigitsString
    
        Case "DeletePrefixSuffixSpacesAndLineFeed"
        
            enmValue = ColumnDataConvertType.DeletePrefixSuffixSpacesAndLineFeed
            
    End Select

    GetColumnDataConvertTypeFromString = enmValue
End Function


'**---------------------------------------------
'** Date data conversion functions
'**---------------------------------------------

Public Function ConvertDateTo8DigitsDateLong(ByVal vobjDate As Date) As Long
    
    Dim intValue As Long
    
    intValue = Year(vobjDate) * 10000 + Month(vobjDate) * 100 + Day(vobjDate)

    ConvertDateTo8DigitsDateLong = intValue
End Function

Public Function ConvertDateTo8DigitsDateString(ByVal vobjDate As Date) As String
    
    ConvertDateTo8DigitsDateString = Format(vobjDate, "yyyymmdd")
End Function

Public Function ConvertDateTo14DigitsDateString(ByVal vobjDate As Date) As String
    
    ConvertDateTo14DigitsDateString = Format(vobjDate, "yyyymmddhhmmss")
End Function


Public Function Convert8DigitsIntegersToDate(ByVal vintDateValue As Long) As Date
    
    Dim intYear As Long, intMonth As Long, intDay As Long
    
    intYear = Int(vintDateValue / 10000)
    
    intMonth = Int((vintDateValue - intYear * 10000) / 100)
    
    intDay = vintDateValue - intYear * 10000 - intMonth * 100

    Convert8DigitsIntegersToDate = DateSerial(intYear, intMonth, intDay)
End Function

'''
'''
'''
Public Function Convert8DigitsStringToDate(ByVal vstrDateValue As String) As Date

    Dim intYear As Long, intMonth As Long, intDay As Long
    
    Dim objMatch As VBScript_RegExp_55.Match
    
    Dim udtDate As Date
    
    
    With GetCacheDateFromContinuous8Digits()
    
        If .Test(vstrDateValue) Then
        
            With .Execute(vstrDateValue)
            
                Set objMatch = .Item(0)
                
                With objMatch.SubMatches
                
                    intYear = CLng(.Item(0)): intMonth = CLng(.Item(1)): intDay = CLng(.Item(2))
                End With
            
                udtDate = DateSerial(intYear, intMonth, intDay)
            End With
        End If
    End With
    
    Convert8DigitsStringToDate = udtDate
End Function


Public Function Convert14DigitsStringToDateTime(ByVal vstrDateTimeValue As String) As Date

    Dim intYear As Long, intMonth As Long, intDay As Long
    Dim intHour As Long, intMinute As Long, intSecond As Long
    Dim objMatch As VBScript_RegExp_55.Match
    Dim udtDate As Date
    
    With GetCacheDateFromContinuous14Digits()
    
        If .Test(vstrDateTimeValue) Then
        
            With .Execute(vstrDateTimeValue)
                
                Set objMatch = .Item(0)
                
                With objMatch.SubMatches
                
                    intYear = CLng(.Item(0)): intMonth = CLng(.Item(1)): intDay = CLng(.Item(2))
                    
                    intHour = CLng(.Item(3)): intMinute = CLng(.Item(4)): intSecond = CLng(.Item(5))
                End With
            
                udtDate = DateSerial(intYear, intMonth, intDay) + TimeSerial(intHour, intMinute, intSecond)
            
                'udtDate = CDate(CStr(intYear) & "/" & CStr(intMonth) & "/" & CStr(intDay) & " " & Format(intHour, "00") & ":" & Format(intMinute, "00") & ":" & Format(intSecond, "00"))
            End With
        End If
    End With

    Convert14DigitsStringToDateTime = udtDate
End Function


'''
'''
'''
Public Function TrimAndDeleteLineFeed(ByVal vstrText As String) As String

    TrimAndDeleteLineFeed = Replace(Trim(vstrText), vbLf, "")
End Function


'**---------------------------------------------
'** Character conversion functions
'**---------------------------------------------
'''
''' Japanese string conversion from a half-width Kana to a full-width Kana
'''
Public Function ConvertKanaFromHalfWidthToFullWidth(ByVal vstrText As String) As String
    
    Dim objRegExp As VBScript_RegExp_55.RegExp, objMatch As VBScript_RegExp_55.Match, strReplacedText As String
    
    strReplacedText = vstrText
    
    If vstrText <> "" Then
    
        With GetCacheHalfWidthKanaRegExp()
        
            If .Test(vstrText) Then
            
                For Each objMatch In .Execute(strReplacedText)
                
                    strReplacedText = Replace(strReplacedText, objMatch.Value, StrConv(objMatch.Value, vbWide))
                Next
            End If
        End With
    End If

    ConvertKanaFromHalfWidthToFullWidth = strReplacedText
End Function

'''
''' convert half-wide Japanese Kana into full-wide Japanese Kana
'''
Public Function IsSomeHalfWideKanaConvertedToOrdinaryFullWideKanaForVariant(ByRef rvarValue As Variant) As Boolean
    
    Dim strText As String, blnIsConverted As Boolean
    
    blnIsConverted = False
    
    If Not IsNumeric(rvarValue) Then
    
        strText = Trim(rvarValue)
    
        blnIsConverted = IsSomeHalfWideKanaConvertedToOrdinaryFullWideKana(strText)
        
        If blnIsConverted Then
        
            rvarValue = strText
        End If
    End If

    IsSomeHalfWideKanaConvertedToOrdinaryFullWideKanaForVariant = blnIsConverted
End Function

'''
''' convert half-wide Japanese Kana into full-wide Japanese Kana
'''
Public Function IsSomeHalfWideKanaConvertedToOrdinaryFullWideKana(ByRef rstrText As String) As Boolean
    
    Dim objMatch As VBScript_RegExp_55.Match, blnIsConverted As Boolean, blnNoExceptionKeyword As Boolean
    
    
    blnIsConverted = False
    
    If rstrText <> "" Then
    
        With GetCacheHalfWidthKanaRegExp()
        
            If .Test(rstrText) Then
            
                blnIsConverted = True
            
                For Each objMatch In .Execute(rstrText)
                
                    rstrText = Replace(rstrText, objMatch.Value, StrConv(objMatch.Value, vbWide))
                Next
            End If
        End With
    End If

    IsSomeHalfWideKanaConvertedToOrdinaryFullWideKana = blnIsConverted
End Function

'''
''' Japanese string conversion from a full width Kana to a half width Kana
'''
Public Function ConvertKanaFromFullWidthToHalfWidth(ByVal vstrText As String) As String
    
    Dim objRegExp As VBScript_RegExp_55.RegExp, objMatch As VBScript_RegExp_55.Match, strReplacedText As String
    
    
    strReplacedText = vstrText
    
    If vstrText <> "" Then
    
        With GetCacheFullWidthKanaRegExp()
        
            If .Test(vstrText) Then
            
                For Each objMatch In .Execute(strReplacedText)
                
                    strReplacedText = Replace(strReplacedText, objMatch.Value, StrConv(objMatch.Value, vbNarrow))
                Next
            End If
        End With
    End If

    ConvertKanaFromFullWidthToHalfWidth = strReplacedText
End Function


'''
''' convert wide-characters, which are alphabets, numbers and dot, into narrow-characters
'''
Public Function ConvertWideAlphaNumToOrdinaryAlphaNum(ByVal vstrText As String) As String
    
    Dim objMatch As VBScript_RegExp_55.Match, strReplacedText As String
    
    strReplacedText = vstrText
    
    If vstrText <> "" Then
    
        With GetWideWidthAlphaNumRegExp()
        
            If .Test(vstrText) Then
            
                For Each objMatch In .Execute(strReplacedText)
                
                    strReplacedText = Replace(strReplacedText, objMatch.Value, StrConv(objMatch.Value, vbNarrow))
                Next
            End If
        End With
    End If

    ConvertWideAlphaNumToOrdinaryAlphaNum = strReplacedText
End Function

'''
''' convert narrow number characters into wide characters
'''
Public Function ConvertNarrowNumbersToWideNumbers(ByVal vstrText As String) As String
    
    Dim strReplacedText As String
    
    strReplacedText = vstrText
    
    ConvertNarrowNumbersOfArgToWideNumbers strReplacedText
    
    ConvertNarrowNumbersToWideNumbers = strReplacedText
End Function

'''
''' convert narrow number characters into wide characters
'''
Public Sub ConvertNarrowNumbersOfArgToWideNumbers(ByRef rstrText As String)
    
    Dim objMatch As VBScript_RegExp_55.Match
    
    If rstrText <> "" Then
    
        With GetNarrowWidthNumberRegExp()
        
            If .Test(rstrText) Then
            
                For Each objMatch In .Execute(rstrText)
                
                    rstrText = Replace(rstrText, objMatch.Value, StrConv(objMatch.Value, vbWide))
                Next
            End If
        End With
    End If
End Sub


'''
''' convert wide-characters into narrow-characters
'''
Public Function IsSomeWideAlphaNumConvertedToOrdinaryAlphaNumForVariant(ByRef rvarValue As Variant, _
        Optional ByVal vobjExceptionKeywords As Collection = Nothing) As Boolean
    
    Dim strText As String, blnIsConverted As Boolean
    
    blnIsConverted = False
    
    If Not IsNumeric(rvarValue) Then
    
        strText = Trim(rvarValue)
    
        blnIsConverted = IsSomeWideAlphaNumConvertedToOrdinaryAlphaNum(strText, vobjExceptionKeywords)
        
        If blnIsConverted Then
        
            rvarValue = strText
        End If
    End If

    IsSomeWideAlphaNumConvertedToOrdinaryAlphaNumForVariant = blnIsConverted
End Function

'''
''' convert wide-characters into narrow-characters
'''
Public Function IsSomeWideAlphaNumConvertedToOrdinaryAlphaNum(ByRef rstrText As String, _
        Optional ByVal vobjExceptionKeywords As Collection = Nothing) As Boolean
    
    Dim objMatch As VBScript_RegExp_55.Match, blnIsConverted As Boolean, blnNoExceptionKeyword As Boolean
    
    
    blnIsConverted = False: blnNoExceptionKeyword = False
    
    If rstrText <> "" Then
    
        If Not vobjExceptionKeywords Is Nothing Then
        
            msubIsExceptionKeywordFound blnNoExceptionKeyword, rstrText, vobjExceptionKeywords
        End If
    
        If Not blnNoExceptionKeyword Then
    
            With GetWideWidthAlphaNumRegExp()
            
                If .Test(rstrText) Then
                
                    blnIsConverted = True
                
                    For Each objMatch In .Execute(rstrText)
                    
                        rstrText = Replace(rstrText, objMatch.Value, StrConv(objMatch.Value, vbNarrow))
                    Next
                End If
            End With
        End If
    End If

    IsSomeWideAlphaNumConvertedToOrdinaryAlphaNum = blnIsConverted
End Function

'''
''' convert wide-parens into narrow-characters
'''
Public Function IsSomeWideParenConvertedToOrdinaryParenForVariant(ByRef rvarValue As Variant) As Boolean
    
    Dim strText As String, blnIsConverted As Boolean
    
    blnIsConverted = False
    
    If Not IsNumeric(rvarValue) Then
    
        strText = Trim(rvarValue)
    
        blnIsConverted = IsSomeWideParenConvertedToOrdinaryParen(strText)
        
        If blnIsConverted Then
        
            rvarValue = strText
        End If
    End If

    IsSomeWideParenConvertedToOrdinaryParenForVariant = blnIsConverted
End Function

'''
''' convert wide-parens into narrow-characters
'''
Public Function IsSomeWideParenConvertedToOrdinaryParen(ByRef rstrText As String) As Boolean
    
    Dim objMatch As VBScript_RegExp_55.Match, blnIsConverted As Boolean
    
    
    blnIsConverted = False
    
    If rstrText <> "" Then
    
        With GetWideWidthParenRegExp()
        
            If .Test(rstrText) Then
            
                blnIsConverted = True
            
                For Each objMatch In .Execute(rstrText)
                
                    rstrText = Replace(rstrText, objMatch.Value, StrConv(objMatch.Value, vbNarrow))
                Next
            End If
        End With
    End If

    IsSomeWideParenConvertedToOrdinaryParen = blnIsConverted
End Function


'''
'''
'''
Private Sub msubIsExceptionKeywordFound(ByRef rblnNoExceptionKeyword As Boolean, _
        ByRef rstrText As String, _
        ByRef robjExceptionKeywords As Collection)

    Dim varKeyword As Variant

    For Each varKeyword In robjExceptionKeywords
    
        If InStr(1, rstrText, CStr(varKeyword)) > 0 Then
        
            rblnNoExceptionKeyword = True
            
            Exit For
        End If
    Next
End Sub

'''
'''
'''
Public Function IsAtLeastOneReplacedByExactMatchOfKeyForVariant(ByRef rvarValue As Variant, _
        ByRef robjReplaceWordDic As Scripting.Dictionary) As Boolean

    Dim blnIsAtLeastOneReplaced As Boolean, strText As String

    blnIsAtLeastOneReplaced = False
    
    If Not IsNumeric(rvarValue) Then
    
        strText = Trim(rvarValue)
        
        blnIsAtLeastOneReplaced = IsAtLeastOneReplacedByExactMatchOfKey(strText, robjReplaceWordDic)
        
        If blnIsAtLeastOneReplaced Then
        
            rvarValue = strText
        End If
    End If

    IsAtLeastOneReplacedByExactMatchOfKeyForVariant = blnIsAtLeastOneReplaced
End Function


'''
'''
'''
Public Function IsAtLeastOneReplacedByExactMatchOfKey(ByRef rstrText As String, _
        ByRef robjReplaceWordDic As Scripting.Dictionary) As Boolean

    Dim blnIsAtLeastOneReplaced As Boolean
    
    blnIsAtLeastOneReplaced = False
    
    If rstrText <> "" Then
    
        With robjReplaceWordDic
        
            If .Exists(rstrText) Then
            
                rstrText = .Item(rstrText)
                
                blnIsAtLeastOneReplaced = True
            End If
        End With
    End If

    IsAtLeastOneReplacedByExactMatchOfKey = blnIsAtLeastOneReplaced
End Function


Public Function GetCacheDateFromContinuous8Digits() As VBScript_RegExp_55.RegExp

    If mobjDateFromContinuous8Digits Is Nothing Then
        
        Set mobjDateFromContinuous8Digits = New VBScript_RegExp_55.RegExp
    
        With mobjDateFromContinuous8Digits
        
            .Pattern = "([0-9]{4})([0-9]{2})([0-9]{2})" ' yyyymmdd
            
            .Global = True
        End With
    End If

    Set GetCacheDateFromContinuous8Digits = mobjDateFromContinuous8Digits
End Function



Public Function GetCacheDateFromContinuous14Digits() As VBScript_RegExp_55.RegExp

    If mobjDateFromContinuous14Digits Is Nothing Then
        
        Set mobjDateFromContinuous14Digits = New VBScript_RegExp_55.RegExp
    
        With mobjDateFromContinuous14Digits
            
            .Pattern = "([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})"   ' yyyymmddhhmnss
            
            .Global = True
        End With
    End If

    Set GetCacheDateFromContinuous14Digits = mobjDateFromContinuous14Digits
End Function


'''
'''
'''
Public Function GetCacheHalfWidthKanaRegExp() As VBScript_RegExp_55.RegExp
    
    If mobjHalfWidthKanaRegExp Is Nothing Then
        
        Set mobjHalfWidthKanaRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjHalfWidthKanaRegExp
        
            ' Detect ΌpΆΐΆΕ
            
            .Pattern = "[\uFF61-\uFF9F]+"
            
            .Global = True
        End With
    End If

    Set GetCacheHalfWidthKanaRegExp = mobjHalfWidthKanaRegExp
End Function


Public Function GetCacheFullWidthKanaRegExp() As VBScript_RegExp_55.RegExp
    
    If mobjFullWidthKanaRegExp Is Nothing Then
        
        Set mobjFullWidthKanaRegExp = GetFullWidthKanaRegExp()
    End If

    Set GetCacheFullWidthKanaRegExp = mobjFullWidthKanaRegExp
End Function


Private Function GetFullWidthKanaRegExp() As VBScript_RegExp_55.RegExp
    
    Dim objRegExp As VBScript_RegExp_55.RegExp: Set objRegExp = New VBScript_RegExp_55.RegExp
    
    With objRegExp
    
        ' Detect SpJ^Ji
        
        .Pattern = "[@-[]+"
        .Global = True
    End With

    Set GetFullWidthKanaRegExp = objRegExp
End Function

'''
'''
'''
Public Function GetWideWidthAlphaNumRegExp() As VBScript_RegExp_55.RegExp
    
    If mobjWideWidthAlphaNumRegExp Is Nothing Then
        
        Set mobjWideWidthAlphaNumRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjWideWidthAlphaNumRegExp
        
            .Pattern = "[`-y-O-X\|\D]+"
            
            .Global = True
        End With
    End If
    
    Set GetWideWidthAlphaNumRegExp = mobjWideWidthAlphaNumRegExp
End Function

'''
'''
'''
Public Function GetNarrowWidthNumberRegExp() As VBScript_RegExp_55.RegExp
    
    If mobjNarrowWidthNumbersRegExp Is Nothing Then
        
        Set mobjNarrowWidthNumbersRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjNarrowWidthNumbersRegExp
        
            .Pattern = "[0-9]+"
            
            .Global = True
        End With
    End If
    
    Set GetNarrowWidthNumberRegExp = mobjNarrowWidthNumbersRegExp
End Function


'''
''' About parenthesis
'''
Public Function GetWideWidthParenRegExp() As VBScript_RegExp_55.RegExp
    
    If mobjWideWidthParenRegExp Is Nothing Then
        
        Set mobjWideWidthParenRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjWideWidthParenRegExp
        
            .Pattern = "[ij]{1,}"
            
            .Global = True
        End With
    End If
    
    Set GetWideWidthParenRegExp = mobjWideWidthParenRegExp
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfConvertWideAlphaNumToOrdinaryAlphaNum()
    
    Dim strText As String
    
    strText = "OPQ345WUVtux"
    
    Debug.Print "Before: " & strText
    
    Debug.Print "After: " & ConvertWideAlphaNumToOrdinaryAlphaNum(strText)
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetWideWidthParenRegExp()

    With GetWideWidthParenRegExp()
    
        Debug.Print .Test("(")  ' ordinary Half-width
    
        Debug.Print .Test("i") ' Wide-width
    End With
End Sub


Private Sub msubSanityTestOfConvertKanaFromFullWidthToHalfWidth()
    
    Dim strText As String
    
    strText = "eXgWc"
    
    Debug.Print "Before: " & strText
    
    Debug.Print "After: " & ConvertKanaFromFullWidthToHalfWidth(strText)
End Sub

Private Sub msubSanityTestOfConvertKanaFromHalfWidthToFullWidth()
    Dim strText As String
    
    strText = "Γ½ΔΣΌήΪΒ"
    
    Debug.Print "Before: " & strText
    
    Debug.Print "After: " & ConvertKanaFromHalfWidthToFullWidth(strText)
End Sub

'''
'''
'''
Private Sub msubSanityTestOfGetHalfWidthKanaRegExp()

    Dim strText As String

    With GetCacheHalfWidthKanaRegExp()
        
        strText = "eXgWc"
        
        If .Test(strText) Then
        
            Debug.Print "Hit: " & strText
        Else
            Debug.Print "NoHit: " & strText
        End If
        
        strText = "Γ½ΔΣΌήΪΒ"
        
        If .Test(strText) Then
        
            Debug.Print "Hit: " & strText
        Else
            Debug.Print "NoHit: " & strText
        End If
    End With
End Sub

