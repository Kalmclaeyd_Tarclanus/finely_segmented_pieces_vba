Attribute VB_Name = "DataTableMerge"
'
'   merge either Collection or Scripting.Dictionary as a data-table
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 26/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function ConvertColFromNullToZeroLengthString(ByVal vobjCol As Collection) As Collection
    
    Dim varItem As Variant, objNewCol As Collection
    
    If Not vobjCol Is Nothing Then
    
        Set objNewCol = New Collection
        
        For Each varItem In vobjCol
    
            If IsNull(varItem) Or IsEmpty(varItem) Then
            
                objNewCol.Add ""
            Else
                objNewCol.Add varItem
            End If
        Next
    End If
    
    Set ConvertColFromNullToZeroLengthString = objNewCol
End Function

'**---------------------------------------------
'** merge two collections or dictionaries
'**---------------------------------------------
'''
''' union two collections int one collection
'''
Public Sub UnionDoubleCollectionsToSingle(ByRef robjGatheredCol As Collection, ByRef robjChildCol As Collection)
    
    Dim varItem As Variant
    
    If Not robjChildCol Is Nothing Then
    
        If robjChildCol.Count > 0 Then
        
            For Each varItem In robjChildCol
            
                robjGatheredCol.Add varItem
            Next
        End If
    End If
End Sub

'''
''' union two dictionaries int one dictionary
'''
Public Sub UnionDoubleDictionariesToSingle(ByRef robjGatheredDic As Scripting.Dictionary, ByRef robjChildDic As Scripting.Dictionary)
    
    Dim varKey As Variant
    
    If Not robjChildDic Is Nothing Then
    
        With robjChildDic
        
            If .Count > 0 Then
            
                For Each varKey In .Keys
                
                    robjGatheredDic.Add varKey, .Item(varKey)
                Next
            End If
        End With
    End If
End Sub

'''
'''
'''
Public Function GetUnionedColFromOnsStringAndStrCollection(ByVal vstrFirstString As String, _
        ByVal vobjSecondCollection As Collection, _
        Optional ByVal vblnWithDistinctValues As Boolean = True)

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    objCol.Add vstrFirstString
    
    If vblnWithDistinctValues Then
    
        UnionDoubleCollectionsToSingleWithoutDuplicatedValues objCol, vobjSecondCollection
    Else
        UnionDoubleCollectionsToSingle objCol, vobjSecondCollection
    End If

    Set GetUnionedColFromOnsStringAndStrCollection = objCol
End Function


'''
'''
'''
Public Sub UnionDoubleCollectionsToSingleWithoutDuplicatedValues(ByRef robjGatheredCol As Collection, _
        ByRef robjChildCol As Collection)
    
    Dim varItem As Variant
    
    Dim objDic As Scripting.Dictionary, blnIncreased As Boolean, intCountOfBefore As Long, intCountOfAfter As Long
    
    
    blnIncreased = False
    
    If Not robjChildCol Is Nothing Then
    
        Set objDic = GetDicFromColButValuesAreAllNothing(robjGatheredCol)
        
        intCountOfBefore = objDic.Count
    
        If robjChildCol.Count > 0 Then
        
            For Each varItem In robjChildCol
            
                With objDic
                
                    If Not .Exists(varItem) Then
                    
                        .Add varItem, Null
                        
                        blnIncreased = True
                    End If
                End With
            
            Next
        
            If objDic.Count > 0 And blnIncreased Then
            
                intCountOfAfter = objDic.Count
                
                Set robjGatheredCol = GetEnumeratorKeysColFromDic(objDic)
            
                Debug.Print "Count of elements before merge: " & CStr(intCountOfBefore) & " ,Count of elements after merge: " & CStr(intCountOfAfter)
            End If
        End If
    End If
End Sub



'**---------------------------------------------
'** Divide a collection into plural collections
'**---------------------------------------------
'''
'''
'''
''' <Argument>robjFirstHalfCol: Output</Argument>
''' <Argument>robjLatterHalfCol: Output</Argument>
''' <Argument>vobjInputCol: Input - only support one-dimensional Collection</Argument>
''' <Argument>vintLastElementIndexOfFirstHalfCol: Input</Argument>
Public Sub DivideTwoPartColsFromOneDimensionalCollection(ByRef robjFirstHalfCol As Collection, _
        ByRef robjLatterHalfCol As Collection, _
        ByVal vobjInputCol As Collection, _
        ByVal vintLastElementIndexOfFirstHalfCol As Long)
    
    
    If robjFirstHalfCol Is Nothing Then Set robjFirstHalfCol = New Collection
    
    If robjLatterHalfCol Is Nothing Then Set robjLatterHalfCol = New Collection
    
    Dim varItem As Variant, i As Long
    
    i = 1
    
    For Each varItem In vobjInputCol
        
        If i <= vintLastElementIndexOfFirstHalfCol Then
            
            robjFirstHalfCol.Add varItem
        Else
            robjLatterHalfCol.Add varItem
        End If
    
        i = i + 1
    Next
End Sub

'''
'''
'''
''' <Return>Collection(Of Collection(Of Variant))</Return>
Public Function DivideColsFromOneDimensionalCollectionBySpecifiedNumber(ByRef robjInputCol As Collection, _
        Optional ByVal vintDividingUnit As Long = 100) As Collection

    Dim objCol As Collection, objChildCol As Collection
    Dim varItem As Variant, i As Long, intCounter
    
    Set objCol = New Collection
    
    i = 1
    
    Set objChildCol = New Collection
    
    For Each varItem In robjInputCol
     
        objChildCol.Add varItem
        
        If (i Mod vintDividingUnit) = 0 Then
        
            If objChildCol.Count > 0 Then
            
                objCol.Add objChildCol
                
                Set objChildCol = New Collection
            End If
        End If
        
        i = i + 1
    Next


    If objChildCol.Count > 0 Then
    
        objCol.Add objChildCol
    End If

    Set DivideColsFromOneDimensionalCollectionBySpecifiedNumber = objCol
End Function



'''
'''
'''
''' <Argument>robjLatterHalfCol: Output</Argument>
''' <Argument>vobjInputCol: Input - only support one-dimensional Collection</Argument>
''' <Argument>vintLastElementIndexOfFirstHalfCol: Input</Argument>
Public Sub ExtractLatterHalfColFromOneDimensionalCollection(ByRef robjLatterHalfCol As Collection, _
        ByRef robjInputCol As Collection, _
        Optional ByVal vintLastElementIndexOfFirstHalfCol As Long = 1)

    If robjLatterHalfCol Is Nothing Then Set robjLatterHalfCol = New Collection
    
    Dim varItem As Variant, i As Long
    
    i = 1
    
    For Each varItem In robjInputCol
    
        If i > vintLastElementIndexOfFirstHalfCol Then
    
            robjLatterHalfCol.Add varItem
        End If
        
        i = i + 1
    Next
End Sub



'''
''' convert Collection values to Long or Double (Date type is also converted...)
'''
Public Function GetNumericColFromCol(ByVal vobjCol As Collection) As Collection
    
    Dim blnIsNestedCol As Boolean, objOutCol As Collection, varItem As Variant, objChildCol As Collection
    
    Dim varRowCol As Variant, objRowCol As Collection, objNewRowCol As Collection
    
    
    Set objOutCol = vobjCol
    
    If Not vobjCol Is Nothing Then
        
        If vobjCol.Count > 0 Then
            
            blnIsNestedCol = False
            
            If IsObject(vobjCol.Item(1)) Then
            
                Set objChildCol = Nothing
                        
                On Error Resume Next
                
                Set objChildCol = vobjCol
                
                On Error GoTo 0
            
                If Not objChildCol Is Nothing Then
                
                    blnIsNestedCol = True
                
                    For Each varRowCol In vobjCol
                        
                        Set objRowCol = varRowCol
                    
                        Set objNewRowCol = New Collection
                        
                        For Each varItem In objRowCol
                        
                            If IsNumeric(varItem) Then
                                
                                If (CDbl(CLng(varItem)) - Int(varItem)) > 0# Then
                                
                                    objNewRowCol.Add CDbl(varItem)
                                Else
                                
                                    objNewRowCol.Add CLng(varItem)
                                End If
                            Else
                                objNewRowCol.Add varItem
                            End If
                        Next
                        
                        objOutCol.Add objNewRowCol
                    Next
                End If
            Else
                Set objOutCol = New Collection
                
                For Each varItem In vobjCol
                
                    If IsNumeric(varItem) Then
                        
                        If (CDbl(CLng(varItem)) - Int(varItem)) > 0# Then
                        
                            objOutCol.Add CDbl(varItem)
                        Else
                        
                            objOutCol.Add CLng(varItem)
                        End If
                    Else
                        objOutCol.Add varItem
                    End If
                Next
            End If
        End If
    End If

    Set GetNumericColFromCol = objOutCol
End Function


'''
''' remain dates only after vudtAfterDate
'''
Public Function GetFilteredDateColFromColByAfterDateCondition(ByVal vobjCol As Collection, _
        ByVal vudtAfterDate As Date) As Collection
    
    Dim objFilteredCol As Collection, varDate As Variant, udtDate As Date

    Set objFilteredCol = New Collection
    
    If vudtAfterDate <> 0 Then
    
        For Each varDate In vobjCol
        
            If IsDate(varDate) Then
            
                udtDate = varDate
                
                If udtDate >= vudtAfterDate Then
                
                    objFilteredCol.Add udtDate
                End If
            End If
        Next
    End If
    
    Set GetFilteredDateColFromColByAfterDateCondition = objFilteredCol
End Function

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfGetFilteredDateColFromColByAfterDateCondition()

    Dim objCol As Collection, udtAfterDate As Date
    
    
    udtAfterDate = CDate("2021/06/01")
    
    
    Set objCol = GetColAsDateFromLineDelimitedChar("2021/5/30,2021/6/1")
    
    Debug.Print "Condition 1:"
    
    DebugDumpColAsString GetFilteredDateColFromColByAfterDateCondition(objCol, udtAfterDate)
    
    
    Set objCol = GetColAsDateFromLineDelimitedChar("2021/6/1, 2021/6/12")
    
    Debug.Print "Condition 2:"
    
    DebugDumpColAsString GetFilteredDateColFromColByAfterDateCondition(objCol, udtAfterDate)

    Set objCol = GetColAsDateFromLineDelimitedChar("2021/5/5, 2021/5/24")

    Debug.Print "Condition 3:"
    
    DebugDumpColAsString GetFilteredDateColFromColByAfterDateCondition(objCol, udtAfterDate)
End Sub


