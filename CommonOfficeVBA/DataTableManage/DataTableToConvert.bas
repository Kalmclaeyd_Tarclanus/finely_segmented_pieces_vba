Attribute VB_Name = "DataTableToConvert"
'
'   convert data-table Collection or Scripting.Dictionary
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon,  6/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub JoinTwoArrays(ByRef rvarOutputArray As Variant, ByRef rvarInput1stArray As Variant, ByRef rvarInput2ndArray As Variant)

    Dim i As Long, j As Long
    
    ReDim rvarOutputArray(LBound(rvarInput1stArray) To UBound(rvarInput1stArray), 1 To 2)

    For i = LBound(rvarInput1stArray) To UBound(rvarInput1stArray)
                
        rvarOutputArray(i, 1) = rvarInput1stArray(i)
    Next
    
    For i = LBound(rvarInput2ndArray) To UBound(rvarInput2ndArray)
                
        rvarOutputArray(i, 2) = rvarInput2ndArray(i)
    Next
End Sub


'''
'''
'''
Public Sub CopyArray(ByRef rvarOutputArray As Variant, ByRef rvarInputArray As Variant)
    
    Dim i As Long, j As Long, intDimension As Long
    
    intDimension = GetDimensionOfArray(rvarInputArray)
    
    If intDimension > 0 Then
    
        Select Case intDimension
        
            Case 1
        
                ReDim rvarOutputArray(LBound(rvarInputArray) To UBound(rvarInputArray))
    
                For i = LBound(rvarInputArray) To UBound(rvarInputArray)
                
                    rvarOutputArray(i) = rvarInputArray(i)
                Next
                
            Case 2
            
                ReDim rvarOutputArray(LBound(rvarInputArray, 1) To UBound(rvarInputArray, 1), LBound(rvarInputArray, 2) To UBound(rvarInputArray, 2))
                
                For i = LBound(rvarInputArray, 1) To UBound(rvarInputArray, 1)
                
                    For j = LBound(rvarInputArray, 2) To UBound(rvarInputArray, 2)
                    
                        rvarOutputArray(i, j) = rvarInputArray(i, j)
                    Next
                Next
        End Select
    End If
End Sub

'''
'''
'''
Public Function GetDimensionOfArray(ByRef rvarInputArray As Variant) As Long
    
    Dim i As Long, intTmp As Long
    
    i = 0
    
    If IsArrayInitialized(rvarInputArray) Then
    
        On Error Resume Next
        
        Do While Err.Number = 0
        
            i = i + 1
        
            intTmp = UBound(rvarInputArray, i)
        Loop
    
        i = i - 1
        
        On Error GoTo 0
    End If

    GetDimensionOfArray = i
End Function

'''
'''
''' Refactoring from https://zukucode.com/2019/08/vba-array-init.html
Public Function IsArrayInitialized(ByRef rvarInputArray As Variant) As Boolean
    
    Dim blnIsInitialized As Boolean, intRet As Long
    
On Error GoTo ErrHandler

    If IsArray(rvarInputArray) Then
        
        'intRet = IIf(UBound(rvarInputArray) >= 0, 1, 0)
        If UBound(rvarInputArray) >= 0 Then ' this is faster than using IIF
        
            intRet = 1
        Else
            intRet = 0
        End If
    Else
        intRet = -1
    End If

    GoTo LastProcess

    Exit Function
    
ErrHandler:

    If Err.Number = 9 Then
        intRet = 0
    Else
        
    End If
    
LastProcess:
    If intRet = -1 Then
    
        blnIsInitialized = False
    Else
        blnIsInitialized = True
    End If
    
    IsArrayInitialized = blnIsInitialized
End Function


'''
'''
'''
Public Function GetArrayFromCol(ByVal vobjCol As Collection, Optional ByVal vblnIsStartIndexOne As Boolean = False) As Variant()
    
    Dim varArray() As Variant, i As Long, varItem As Variant
    
    If vblnIsStartIndexOne Then
    
        ReDim varArray(0 To (vobjCol.Count - 1))
        
        i = 0
    Else
        ReDim varArray(1 To vobjCol.Count)
        
        i = 1
    End If
    
    For Each varItem In vobjCol
    
        varArray(i) = varItem
    
        i = i + 1
    Next
    
    GetArrayFromCol = varArray
End Function


'''
''' copy 1-diemnstional collection to array
'''
Public Sub ConvertColToArray(ByRef rvarArray As Variant, ByVal vobjInputCol As Collection, Optional ByVal vblnIsStartIndexOne As Boolean = False)

    Dim i As Long, varItem As Variant
    
    If vblnIsStartIndexOne Then
    
        ReDim rvarArray(1 To vobjInputCol.Count)
        
        i = 1
    Else
        ReDim rvarArray(0 To (vobjInputCol.Count - 1))
        
        i = 0
    End If
    
    For Each varItem In vobjInputCol
    
        rvarArray(i) = varItem
    
        i = i + 1
    Next
End Sub

'''
'''
'''
Public Sub ConvertEnumeratorKeysToArray(ByRef rvarArray As Variant, ByVal vobjInputDic As Scripting.Dictionary, Optional ByVal vblnIsStartIndexOne As Boolean = False)

    Dim i As Long, varKey As Variant
    
    If vblnIsStartIndexOne Then
    
        ReDim rvarArray(1 To vobjInputDic.Count)
        
        i = 1
    Else
        ReDim rvarArray(0 To (vobjInputDic.Count - 1))
        
        i = 0
    End If
    
    For Each varKey In vobjInputDic.Keys
        
        rvarArray(i) = varKey
    
        i = i + 1
    Next
End Sub


Public Function GetColFromArray(ByRef rvarArray() As Variant) As Collection
    
    Dim objCol As Collection, i As Long
    
    Set objCol = New Collection
    
    For i = LBound(rvarArray) To UBound(rvarArray)
    
        objCol.Add rvarArray(i)
    Next

    Set GetColFromArray = objCol
End Function

Public Function GetColFromStringArray(ByRef rstrArray() As String) As Collection
    
    Dim objCol As Collection, i As Long
    
    Set objCol = New Collection
    
    For i = LBound(rstrArray) To UBound(rstrArray)
    
        objCol.Add rstrArray(i)
    Next

    Set GetColFromStringArray = objCol
End Function

'''
''' convert a String data delimited some character into Collection object
'''
Public Function GetColFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Collection
    
    Dim objCol As Collection, i As Long, strArray() As String

    
    Set objCol = New Collection
    
    If vstrLineDelimited <> "" Then
        
        strArray = Split(vstrLineDelimited, vstrDelimiter)
        
        For i = LBound(strArray) To UBound(strArray)
        
            objCol.Add Trim(strArray(i))
        Next
    End If
    
    Set GetColFromLineDelimitedChar = objCol
End Function

'''
''' convert a Long-type integer data delimited some character into Collection object
'''
Public Function GetIntegerColFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Collection
    
    Dim objCol As Collection, i As Long, strArray() As String

    Set objCol = New Collection
    
    If vstrLineDelimited <> "" Then
        
        strArray = Split(vstrLineDelimited, vstrDelimiter)
        
        For i = LBound(strArray) To UBound(strArray)
        
            If IsNumeric(strArray(i)) Then
        
                objCol.Add CLng(Trim(strArray(i)))
            End If
        Next
    End If
    
    Set GetIntegerColFromLineDelimitedChar = objCol
End Function

'''
''' convert a boolean data delimited some character into Collection object
'''
Public Function GetBooleanColFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Collection
    
    Dim objCol As Collection, i As Long, strArray() As String

    Set objCol = New Collection
    
    If vstrLineDelimited <> "" Then
        
        strArray = Split(vstrLineDelimited, vstrDelimiter)
        
        For i = LBound(strArray) To UBound(strArray)
        
            objCol.Add CBool(Trim(strArray(i)))
        Next
    End If
    
    Set GetBooleanColFromLineDelimitedChar = objCol
End Function


Public Function GetColFromLineDelimitedCharWithSetKey(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Collection
    
    Dim objCol As Collection, i As Long, strArray() As String

    Set objCol = New Collection
    
    If vstrLineDelimited <> "" Then
    
        strArray = Split(vstrLineDelimited, vstrDelimiter)
        
        For i = LBound(strArray) To UBound(strArray)
        
            objCol.Add Trim(strArray(i)), key:=Trim(strArray(i))
        Next
    End If
    
    Set GetColFromLineDelimitedCharWithSetKey = objCol
End Function

'''
'''
'''
Public Function GetColFromLineDelimitedCharWithRemainingSpaceOrTabOrLineFeed(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Collection
    
    Dim objCol As Collection, i As Long, strArray() As String

    Set objCol = New Collection
    
    If vstrLineDelimited <> "" Then
    
        strArray = Split(vstrLineDelimited, vstrDelimiter)
        
        For i = LBound(strArray) To UBound(strArray)
        
            objCol.Add strArray(i)
        Next
    End If
    
    Set GetColFromLineDelimitedCharWithRemainingSpaceOrTabOrLineFeed = objCol
End Function

'''
'''
'''
Public Function GetColAsDateFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Collection
    
    Dim objCol As Collection, i As Long, strArray() As String

    Set objCol = New Collection
    
    If vstrLineDelimited <> "" Then
        
        strArray = Split(vstrLineDelimited, vstrDelimiter)
        
        For i = LBound(strArray) To UBound(strArray)
        
            If IsDate(strArray(i)) Then
            
                objCol.Add CDate(Trim(strArray(i)))
            Else

                objCol.Add Trim(strArray(i))
            End If
        Next
    End If
    
    Set GetColAsDateFromLineDelimitedChar = objCol
End Function

'''
''' convert a String data delimited some character into Scripting.Dictionary object only
'''
Public Function GetDicOnlyKeysFromLineDelimitedChar(ByVal vstrLineDelimited As String, _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vvarEmptyValue As Variant = 0) As Scripting.Dictionary
    
    
    Dim objDic As Scripting.Dictionary, strArray() As String, strValue As String, i As Long
    
    
    Set objDic = New Scripting.Dictionary
    
    If vstrLineDelimited <> "" Then
        
        strArray = Split(vstrLineDelimited, vstrDelimiter)
    
        For i = LBound(strArray) To UBound(strArray)
            
            With objDic
            
                strValue = Trim(strArray(i))
            
                If Not .Exists(strValue) Then
                
                    .Add strValue, vvarEmptyValue
                End If
            End With
        Next
    End If
    
    Set GetDicOnlyKeysFromLineDelimitedChar = objDic
End Function


'''
'''
'''
Public Function GetEnumeratorKeysColFromDic(ByVal vobjDic As Scripting.Dictionary) As Collection
    
    Dim objCol As Collection, i As Long, varKey As Variant
    
    If Not vobjDic Is Nothing Then
    
        If vobjDic.Count > 0 Then
        
            Set objCol = New Collection
            
            For Each varKey In vobjDic.Keys
            
                objCol.Add varKey
            Next
        End If
    End If

    Set GetEnumeratorKeysColFromDic = objCol
End Function

'''
'''
'''
Public Function GetEnumeratorValuesColFromDic(ByVal vobjDic As Scripting.Dictionary, Optional ByVal vblnUseKeysProperty As Boolean = False) As Collection
    
    Dim objCol As Collection, i As Long, varItem As Variant, varKey As Variant
    
    If Not vobjDic Is Nothing Then
    
        If vobjDic.Count > 0 Then
        
            Set objCol = New Collection
            
            If vblnUseKeysProperty Then
            
                With vobjDic
                
                    For Each varKey In .Keys
                    
                        objCol.Add .Item(varKey)
                    Next
                End With
            Else
                For Each varItem In vobjDic.Items
                
                    objCol.Add varItem
                Next
            End If
        End If
    End If

    Set GetEnumeratorValuesColFromDic = objCol
End Function

'''
'''
'''
Public Function GetDicFromColButValuesAreAllNothing(ByVal vobjCol As Collection) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, varItem As Variant
    
    If Not vobjCol Is Nothing Then
    
        If vobjCol.Count > 0 Then
            
            Set objDic = New Scripting.Dictionary
            
            For Each varItem In vobjCol
            
                objDic.Add varItem, Nothing
            Next
        End If
    End If

    Set GetDicFromColButValuesAreAllNothing = objDic
End Function


'''
'''
'''
Public Function GetFirstItemFromDic(ByVal vobjDic As Scripting.Dictionary) As Variant
    
    Dim varValue As Variant, varKey As Variant
    
    For Each varKey In vobjDic.Keys
    
        If vobjDic.Item(varKey) Is Nothing Then
        
            Set varValue = Nothing
            
            Set GetFirstItemFromDic = varValue
        
        ElseIf IsObject(vobjDic.Item(varKey)) Then
            
            Set varValue = vobjDic.Item(varKey)
            
            Set GetFirstItemFromDic = varValue
        
        ElseIf IsEmpty(vobjDic.Item(varKey)) Then
        
            varValue = Empty
            
            GetFirstItemFromDic = varValue
        Else
        
            varValue = vobjDic.Item(varKey)
            
            GetFirstItemFromDic = varValue
        End If
    Next
End Function

'''
'''
'''
Public Function GetDTColFromKeyToDelimitedCharValuesString(ByRef robjKeyToDelimitedCharValuesDic As Scripting.Dictionary, Optional vstrDelimiter As String = ",") As Collection

    Dim objDTCol As Collection, objRowCol As Collection, varKey As Variant
    
    Set objDTCol = New Collection
    
    With robjKeyToDelimitedCharValuesDic
    
        For Each varKey In .Keys
    
            Set objRowCol = New Collection
            
            objRowCol.Add varKey
            
            UnionDoubleCollectionsToSingle objRowCol, GetColFromLineDelimitedChar(.Item(varKey), vstrDelimiter)
    
            objDTCol.Add objRowCol
        Next
    End With

    Set GetDTColFromKeyToDelimitedCharValuesString = objDTCol
End Function


'''
''' from continuous text delimiting some character, get Dictionary(Of String, Double)
'''
Public Function GetTextToRealNumberDicFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, i As Long, strArray() As String
    
    Set objDic = New Scripting.Dictionary
    
    If vstrLineDelimited <> "" Then
    
        strArray = Split(vstrLineDelimited, vstrDelimiter)
    
        For i = LBound(strArray) To UBound(strArray) Step 2
        
            If i + 1 <= UBound(strArray) And IsNumeric(Trim(strArray(i + 1))) Then
            
                objDic.Add Trim(strArray(i)), CDbl(Trim(strArray(i + 1)))
            End If
        Next
    End If
     
    Set GetTextToRealNumberDicFromLineDelimitedChar = objDic
End Function

'''
''' from continuous text delimiting some character, get Dictionary(Of Double, Double)
'''
Public Function GetRealNumberToRealNumberDicFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, i As Long, strArray() As String
    
    Set objDic = New Scripting.Dictionary
    
    If vstrLineDelimited <> "" Then
    
        strArray = Split(vstrLineDelimited, vstrDelimiter)
    
        For i = LBound(strArray) To UBound(strArray) Step 2
        
            If i + 1 <= UBound(strArray) And IsNumeric(Trim(strArray(i + 1))) Then
            
                objDic.Add CDbl(Trim(strArray(i))), CDbl(Trim(strArray(i + 1)))
            End If
        Next
    End If
     
    Set GetRealNumberToRealNumberDicFromLineDelimitedChar = objDic
End Function

'''
''' from continuous text delimiting some character, get Dictionary(Of Long, Long)
'''
Public Function GetIntegerToIntegerDicFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, i As Long, strArray() As String
    
    Set objDic = New Scripting.Dictionary
    
    If vstrLineDelimited <> "" Then
    
        strArray = Split(vstrLineDelimited, vstrDelimiter)
    
        For i = LBound(strArray) To UBound(strArray) Step 2
        
            If i + 1 <= UBound(strArray) And IsNumeric(Trim(strArray(i + 1))) Then
            
                objDic.Add CLng(Trim(strArray(i))), CLng(Trim(strArray(i + 1)))
            End If
        Next
    End If
     
    Set GetIntegerToIntegerDicFromLineDelimitedChar = objDic
End Function

'''
''' from continuous text delimiting some character
'''
Public Function GetIntegerToZeroDicFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, i As Long, strArray() As String
    
    Set objDic = New Scripting.Dictionary
    
    If vstrLineDelimited <> "" Then

        strArray = Split(vstrLineDelimited, vstrDelimiter)
        
        For i = LBound(strArray) To UBound(strArray)
        
            objDic.Add CLng(Trim(strArray(i))), 0
        Next
    End If
     
    Set GetIntegerToZeroDicFromLineDelimitedChar = objDic
End Function


'''
''' from continuous text delimiting some character, get Dictionary(Of String, String)
'''
Public Function GetTextToTextDicFromLineDelimitedChar(ByVal vstrLineDelimited As String, _
        Optional ByVal vstrDelimiter As String = ",") As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, i As Long, strArray() As String
    
    Set objDic = New Scripting.Dictionary
    
    If vstrLineDelimited <> "" Then
        
        strArray = Split(vstrLineDelimited, vstrDelimiter)
    
        For i = LBound(strArray) To UBound(strArray) Step 2
            
            If i + 1 <= UBound(strArray) Then
                
                objDic.Add Trim(strArray(i)), Trim(strArray(i + 1))
            End If
        Next
    End If
     
    Set GetTextToTextDicFromLineDelimitedChar = objDic
End Function

'''
''' from continuous text delimiting some character, get Dictionary(Of String, String)
'''
Public Function GetTextToTextDicFromTwoTypeLineDelimitedChars(ByVal vstrLineDelimited As String, _
        Optional ByVal vstr1stDelimiter As String = ",", _
        Optional ByVal vstr2ndDelimiter As String = ";") As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, i As Long, str1stArray() As String, str2ndArray() As String
    
    Set objDic = New Scripting.Dictionary
    
    If vstrLineDelimited <> "" Then
        
        str1stArray = Split(vstrLineDelimited, vstr1stDelimiter)
    
        For i = LBound(str1stArray) To UBound(str1stArray)
            
            str2ndArray = Split(str1stArray(i), vstr2ndDelimiter)
            
            If 0 = UBound(str2ndArray) Then
                
                objDic.Add Trim(str2ndArray(0)), Empty
            Else
                objDic.Add Trim(str2ndArray(0)), Trim(str2ndArray(1))
            End If
        Next
    End If
     
    Set GetTextToTextDicFromTwoTypeLineDelimitedChars = objDic
End Function

'''
''' from one text-string delimiting some character, get Dictionary(Of String, ColumnDataConvertType)
'''
Public Function GetTextToColumnDataConvertTypeDicFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, i As Long, strArray() As String
    
    Set objDic = New Scripting.Dictionary
    
    If vstrLineDelimited <> "" Then
    
        strArray = Split(vstrLineDelimited, vstrDelimiter)
    
        For i = LBound(strArray) To UBound(strArray) Step 2
            
            If i + 1 <= UBound(strArray) Then
                
                objDic.Add Trim(strArray(i)), GetColumnDataConvertTypeFromString(strArray(i + 1))
            End If
        Next
    End If
     
    Set GetTextToColumnDataConvertTypeDicFromLineDelimitedChar = objDic
End Function

'''
'''
'''
''' <Argument>robjKeyToValuesDelimitedCommaDic: Input - Dictionary(Of Key[Some Value Type], Of Value[String delimited by comma]</Argument>
''' <Return>Dictionary(Of Key[Some Value Type], Dictionary(Of Key[Each Value], Value[0]))</Return>
Public Function GetNestedDicFromKeyToValuesDelimitedCommaDic(ByRef robjKeyToValuesDelimitedCommaDic As Scripting.Dictionary) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, objChildDic As Scripting.Dictionary, varItem As Variant, varKey As Variant
    
    If Not robjKeyToValuesDelimitedCommaDic Is Nothing Then
    
        If robjKeyToValuesDelimitedCommaDic.Count > 0 Then
    
            Set objDic = New Scripting.Dictionary
    
            With robjKeyToValuesDelimitedCommaDic
            
                For Each varKey In .Keys
                
                    Set objChildDic = New Scripting.Dictionary
                    
                    For Each varItem In Split(.Item(varKey), ",")
                    
                        objChildDic.Add varItem, 0
                    Next
                
                    objDic.Add varKey, objChildDic
                Next
            End With
        End If
    End If
    
    Set GetNestedDicFromKeyToValuesDelimitedCommaDic = objDic
End Function

'''
'''
'''
''' <Argument>robjDTCol: Input - more than one column</Argument>
''' <Return>Dictionary(Of Key[1st column of robjDTCol], Of Collection(Of Value[2nd later columns of robjDTCol]))</Return>
Public Function GetNestedDicFromGeneralDTCol(ByRef robjDTCol As Collection) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, varRowCol As Variant, objRowCol As Collection
    Dim objLatterRowCol As Collection, varKey As Variant, objPartialDTCol As Collection
    
    Set objDic = New Scripting.Dictionary
    
    If Not robjDTCol Is Nothing Then
    
        If robjDTCol.Count > 0 Then
        
            If TypeOf robjDTCol.Item(1) Is Collection Then
        
                For Each varRowCol In robjDTCol
        
                    Set objRowCol = varRowCol
                
                    Set objLatterRowCol = New Collection
                
                    ExtractLatterHalfColFromOneDimensionalCollection objLatterRowCol, objRowCol, 1
                
                    varKey = objRowCol.Item(1)
                    
                    With objDic
                    
                        If Not .Exists(varKey) Then
                        
                            Set objPartialDTCol = New Collection
                            
                            .Add varKey, objPartialDTCol
                        Else
                            Set objPartialDTCol = .Item(varKey)
                        End If
                    
                        objPartialDTCol.Add objLatterRowCol
                    End With
                Next
            End If
        End If
    End If
    
    Set GetNestedDicFromGeneralDTCol = objDic
End Function

