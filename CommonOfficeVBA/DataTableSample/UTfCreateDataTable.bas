Attribute VB_Name = "UTfCreateDataTable"
'
'   create fictional data-tables for unit-tests in this VBProject
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue,  7/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
        Optional ByVal vintCountOfRows As Long = 100) As String

    Dim strCsvPath As String
    
    strCsvPath = GetPreparedSampleDataTableCSVPathForTest(venmUnitTestPreparedSampleDataTable)
    
    With New Scripting.FileSystemObject
    
        If Not .FileExists(strCsvPath) Then
        
            ' create
        
            GetCSVPathAfterCreateSampleDataTable venmUnitTestPreparedSampleDataTable, vintCountOfRows
        End If
    End With

    GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist = strCsvPath
End Function


'''
'''
'''
Public Function GetCSVPathAfterCreateSampleDataTable(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
        Optional ByVal vintCountOfRows As Long = 1000, _
        Optional ByVal vblnOpenCSVParentDirectoryByExplorer As Boolean = False) As String

    Dim varDataTable As Variant, objFieldTitles As Collection, objDoubleStopWatch As DoubleStopWatch
    Dim strCsvPath As String

    Set objDoubleStopWatch = New DoubleStopWatch
    
    With objDoubleStopWatch
    
        .MeasureStart
    
        CreateSimpleRandomDataTable varDataTable, objFieldTitles, vintCountOfRows, venmUnitTestPreparedSampleDataTable
    
        .MeasureInterval
    End With
    
    
    strCsvPath = GetPreparedSampleDataTableCSVPathForTest(venmUnitTestPreparedSampleDataTable)
    
    msubOutputCSVFileAndExplorer strCsvPath, varDataTable, objFieldTitles, vblnOpenCSVParentDirectoryByExplorer
    
    GetCSVPathAfterCreateSampleDataTable = strCsvPath
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** craete CSV files
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCreateSimpleData01Table()

    Dim varDataTable As Variant, objFieldTitles As Collection

    CreateSimpleRandomDataTable varDataTable, objFieldTitles, 100, BasicSampleDT
End Sub


Private Sub msubSanityTestToCreateSimpleData02Table()

    Dim varDataTable As Variant, objFieldTitles As Collection

    CreateSimpleRandomDataTable varDataTable, objFieldTitles, 100, IncludeFictionalProductionNumberSampleDT
End Sub


'**---------------------------------------------
'** craete a CSV file and open it
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToOpenCSVAfterCreateSampleDataTable01()

    mfstrGetCSVPathAfterCreateSampleDataTable01 vblnOpenCSVParentDirectoryByExplorer:=True
End Sub

'''
'''
'''
Private Sub msubSanityTestToOpenCSVAfterCreateSampleDataTable02()

    mfstrGetCSVPathAfterCreateSampleDataTable02 vblnOpenCSVParentDirectoryByExplorer:=True
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** About CSV files
'**---------------------------------------------
'''
'''
'''
Private Function mfstrGetCSVPathAfterCreateSampleDataTable01(Optional ByVal vintCountOfRows As Long = 100, _
        Optional ByVal vblnOpenCSVParentDirectoryByExplorer As Boolean = False) As String

    mfstrGetCSVPathAfterCreateSampleDataTable01 = GetCSVPathAfterCreateSampleDataTable(BasicSampleDT, vintCountOfRows, vblnOpenCSVParentDirectoryByExplorer)
End Function


'''
'''
'''
Private Function mfstrGetCSVPathAfterCreateSampleDataTable02(Optional ByVal vintCountOfRows As Long = 1000, _
        Optional ByVal vblnOpenCSVParentDirectoryByExplorer As Boolean = False) As String

    mfstrGetCSVPathAfterCreateSampleDataTable02 = GetCSVPathAfterCreateSampleDataTable(IncludeFictionalProductionNumberSampleDT, vintCountOfRows, vblnOpenCSVParentDirectoryByExplorer)
End Function



'''
'''
'''
Private Sub msubOutputCSVFileAndExplorer(ByRef rstrCSVPath As String, _
        ByRef rvarDataTable As Variant, _
        ByRef robjFieldTitles As Collection, _
        Optional ByVal vblnOpenCSVParentDirectoryByExplorer As Boolean = False)

    With New Scripting.FileSystemObject
    
        ForceToCreateDirectory .GetParentFolderName(rstrCSVPath)
    
        OutputVarDataTableToCSVFile rvarDataTable, robjFieldTitles, rstrCSVPath
    
        If vblnOpenCSVParentDirectoryByExplorer Then
        
            ExploreParentFolderWhenOpenedThatIsNothing .GetParentFolderName(rstrCSVPath)
            
            OpenTextBySomeSelectedTextEditorFromWshShell rstrCSVPath, True, True
        End If
    End With
End Sub



