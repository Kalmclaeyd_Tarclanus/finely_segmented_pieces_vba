Attribute VB_Name = "LocalInstalledMsOfficeSofts"
'
'   Check installed Microsoft-office softwares such as Excel, Word, PowerPoint using CreateObject() interfaces
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       temporary VBA source code output directory in order to output other office with a part of this VBA project
'       Both Microsoft Access and Outlook have no permission to access VBIDE.VBProject object-model
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 13/Jun/2023    Kalmclaeyd Tarclanus    Separated from CodeExternalOfficeCommonOut.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjIsTheSpecifiedOfficeApplicationInstalledInThisComputerCacheDic As Scripting.Dictionary


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** check installed softwares in this computer
'**---------------------------------------------
'''
'''
'''
Public Function IsTheSpecifiedOfficeApplicationInstalledInThisComputer(ByVal vstrOfficeApplicationName As String) As Boolean

    Dim blnIsInstalled As Boolean
    
    blnIsInstalled = False
    
    Select Case vstrOfficeApplicationName

        Case "Excel", "Word", "PowerPoint", "Access", "Outlook"
        
            If mobjIsTheSpecifiedOfficeApplicationInstalledInThisComputerCacheDic Is Nothing Then Set mobjIsTheSpecifiedOfficeApplicationInstalledInThisComputerCacheDic = New Scripting.Dictionary
            
            With mobjIsTheSpecifiedOfficeApplicationInstalledInThisComputerCacheDic
            
                If .Exists(vstrOfficeApplicationName) Then
                
                    blnIsInstalled = mobjIsTheSpecifiedOfficeApplicationInstalledInThisComputerCacheDic.Item(vstrOfficeApplicationName)
                Else
                    blnIsInstalled = mfblnIsTheSpecifiedOfficeApplicationInstalledInThisComputer(vstrOfficeApplicationName)
                
                    .Add vstrOfficeApplicationName, blnIsInstalled
                End If
            End With
    End Select

    IsTheSpecifiedOfficeApplicationInstalledInThisComputer = blnIsInstalled
End Function


'''
'''
'''
Public Sub CheckCreateOfficeFileAndTrustingVBAState(Optional ByVal vstrOfficeApplicationName As String = "Excel")

    Dim strMessage As String
    Dim strDBDir As String, strDbPath As String, blnIsAlreadyExistedInThisComputerWindowsProcesses As Boolean, intErrorCodeAtCreateObject As Long
    
    Dim objApplication As Object, objOfficeFile As Object
    Dim enmApplicationVisible As Long


    Set objApplication = GetOfficeApplicationObjectFromNameInThisComputer(vstrOfficeApplicationName, _
            intErrorCodeAtCreateObject, _
            blnIsAlreadyExistedInThisComputerWindowsProcesses)


    If objApplication Is Nothing Then
    
        If intErrorCodeAtCreateObject <> 0 Then
    
            strMessage = "Microsoft " & vstrOfficeApplicationName & " hasn't been installed in this PC"
        
            MsgBox strMessage, vbExclamation Or vbOKOnly, vstrOfficeApplicationName & " VBA"
            
            Debug.Print strMessage
        End If
    Else
        Debug.Print objApplication.Name
        
        Debug.Print objApplication.Visible
        
        
        Select Case vstrOfficeApplicationName
        
            Case "Excel", "Word", "PowerPoint"
        
                Set objOfficeFile = GetNewMsOfficeFileObjectByInterface(objApplication, vstrOfficeApplicationName)
                
            Case "Access"
        
                Set objOfficeFile = GetTestingAccessCurrentProjectObject(objApplication)
                
            Case "Outlook"
                
                MsgBox "This '" & vstrOfficeApplicationName & "' application is not supported.", vbInformation Or vbOKOnly, "VBIDE.VBProject test"
                
                Exit Sub
        End Select
        
        On Error Resume Next
        
        ' Try to access VBIDE.VBProject
        
        Select Case vstrOfficeApplicationName
        
            Case "Excel", "Word", "PowerPoint"
            
                Debug.Print "VBProject name: " & objOfficeFile.VBProject.Name
            
            Case "Access"
        
                Debug.Print "VBProject name: " & objOfficeFile.Application.VBE.ActiveVBProject.Name
        
            Case "Outlook"
        
                Debug.Print "The Microsoft Outlook doesn't support the VBProject of Outlook.Application"
                
                Debug.Assert False
                
        End Select
        
        If Err.Number <> 0 Then
        
            strMessage = "It is not checked that 'Trust access to the VBA project object model' check-box in the " & vstrOfficeApplicationName & " Application of this computer" & vbNewLine & "The accessing VBA object model is not trusted at " & vstrOfficeApplicationName
        
            Debug.Print strMessage
            
            MsgBox strMessage, vbCritical Or vbOKOnly, vstrOfficeApplicationName & " VBA"
        Else
            ' No problems
            
            Debug.Print "Probably, 'Trust access to the VBA project object model' has been checked in the " & vstrOfficeApplicationName & " Application of this computer"
        End If
        
        On Error GoTo 0
        
        'objOfficeFile
        
        'objApplication.Visible = enmApplicationVisible
        
        Select Case vstrOfficeApplicationName
        
            Case "Excel", "Word", "PowerPoint"
            
                objOfficeFile.Close
        End Select
        
        If Not blnIsAlreadyExistedInThisComputerWindowsProcesses Then
        
            objApplication.Quit
            
            Set objApplication = Nothing
        End If
    End If
End Sub

'''
'''
'''
''' <Argument>vstrOfficeApplicationName: Input</Argument>
''' <Argument>rintErrorCodeAtCreateObject: Output</Argument>
''' <Argument>rblnIsAlreadyExistedInWindowsProcesses: Output</Argument>
''' <Return>Object: Either Excel.Application, Word.Application, or PowerPoint.Application</Return>
Public Function GetOfficeApplicationObjectFromNameInThisComputer(ByVal vstrOfficeApplicationName As String, _
        ByRef rintErrorCodeAtCreateObject As Long, _
        ByRef rblnIsAlreadyExistedInWindowsProcesses As Boolean, _
        Optional ByVal vblnAllowToSuppressControlApplicationVisible As Boolean = True) As Object


    Dim objApplication As Object    ' Excel.Application, Word.Application, PowerPoint.Application
    
    Dim intErrorCodeAtCreateObject As Long, blnIsAlreadyExistedInThisComputerWindowsProcesses As Boolean

    Dim blnVisible As Boolean, blnOmitVisiblePropertyControl As Boolean


    intErrorCodeAtCreateObject = 0
    
    On Error Resume Next
    
    Set objApplication = Nothing

    Set objApplication = GetObject(, vstrOfficeApplicationName & ".Application")

    On Error GoTo 0

    If objApplication Is Nothing Then
    
        On Error Resume Next
    
        Set objApplication = CreateObject(vstrOfficeApplicationName & ".Application")
        
        intErrorCodeAtCreateObject = Err.Number
        
        On Error GoTo 0
        
        If Not vblnAllowToSuppressControlApplicationVisible Then
        
            blnOmitVisiblePropertyControl = False
            
            On Error Resume Next
            
            blnVisible = objApplication.Visible
            
            If Err.Number <> 0 Then
            
                blnOmitVisiblePropertyControl = True
            End If
            
            On Error GoTo 0
            
            If Not blnOmitVisiblePropertyControl Then
            
                If Not objApplication.Visible Then
                
                    objApplication.Visible = True
                End If
            End If
        End If
        
        blnIsAlreadyExistedInThisComputerWindowsProcesses = False
    Else
        blnIsAlreadyExistedInThisComputerWindowsProcesses = True
    End If
    
    ' outputs
    rintErrorCodeAtCreateObject = intErrorCodeAtCreateObject
    
    rblnIsAlreadyExistedInWindowsProcesses = blnIsAlreadyExistedInThisComputerWindowsProcesses
    
    Set GetOfficeApplicationObjectFromNameInThisComputer = objApplication
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfblnIsTheSpecifiedOfficeApplicationInstalledInThisComputer(ByVal vstrOfficeApplicationName As String) As Boolean
    
    Dim blnIsInstalled As Boolean
    Dim objApplication As Object, blnIsAlreadyExistedInThisComputerWindowsProcesses As Boolean, intErrorCodeAtCreateObject As Long
    
    blnIsInstalled = False
    
    Select Case vstrOfficeApplicationName
        
        Case "Excel", "Word", "PowerPoint", "Access", "Outlook"
        
            Set objApplication = GetOfficeApplicationObjectFromNameInThisComputer(vstrOfficeApplicationName, _
                    intErrorCodeAtCreateObject, _
                    blnIsAlreadyExistedInThisComputerWindowsProcesses)
        
            If intErrorCodeAtCreateObject = 0 Then
            
                blnIsInstalled = True
            
                If Not blnIsAlreadyExistedInThisComputerWindowsProcesses Then
            
                    objApplication.Quit
                    
                    Set objApplication = Nothing
                End If
            Else
            
                blnIsInstalled = False
            End If
    End Select
    
    mfblnIsTheSpecifiedOfficeApplicationInstalledInThisComputer = blnIsInstalled
End Function

