Attribute VB_Name = "LocalInstalledSoftsForVBS"
'
'   Check this computer local installed softwares
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on WMI
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 13/Jun/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit


'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = True

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub GetInstalledApplicationInfo(ByRef robjDTDic As Scripting.Dictionary, _
        ByRef robjFieldTitlesDic As Scripting.Dictionary, _
        ByRef rstrHostName As String, _
        ByRef rstrUserName As String, _
        ByRef rstrPassword As String, _
        Optional ByVal vstrDelimiterChar As String = ",")

#If HAS_REF Then

    Dim objSet As WbemScripting.SWbemObjectSet, objEx As SWbemObjectEx, objLocator As WbemScripting.SWbemLocator, objService As WbemScripting.SWbemServices
    
    Set objLocator = New WbemScripting.SWbemLocator
#Else
    Dim objSet As Object, objEx As Object, objLocator As Object, objService As Object
    
    Set objLocator = CreateObject("WbemScripting.SWbemLocator")
#End If
    
    Dim strDelimiter As String, i As Long, strItemValuesByDelimited As String, strName As String
    
    strDelimiter = vbTab
    
    If Not IsEmpty(vstrDelimiterChar) Then
        
        If vstrDelimiterChar <> "" Then
        
            strDelimiter = vstrDelimiterChar
        End If
    End If
    
    
    Set objService = objLocator.ConnectServer(rstrHostName, "", rstrUserName, rstrPassword)
    
    Set objSet = objService.ExecQuery("Select * from Win32_Product")
    
    
    Set robjFieldTitlesDic = GetDicOnlyKeysFromLineDelimitedChar("Name,Vendor,HOST,InstallDate,InstallLocation,Version,IdentifyingNumber")
    
    Set robjDTDic = New Scripting.Dictionary
    
    i = 1
    
    For Each objEx In objSet
        
        With objEx
        
            If Not IsNull(.Name) Then
        
                strName = .Name
            
                strItemValuesByDelimited = .Vendor & strDelimiter & rstrHostName & strDelimiter & .InstallDate & strDelimiter & .InstallLocation & strDelimiter & .Version & strDelimiter & .IdentifyingNumber
            
                robjDTDic.Add strName, strItemValuesByDelimited
            End If
        End With
        
        i = i + 1
    Next
    
    SortDictionaryAboutKeys robjDTDic
End Sub



'''
''' This needs the Administrator authority
'''
Public Sub GetInstalledWin32ProgramInformationInThisComputerByOriginalTypes(ByRef robjDTDic As Scripting.Dictionary, _
        ByRef robjFieldTitlesDic As Scripting.Dictionary, _
        Optional ByVal vstrDelimiterChar As String = ";")

#If HAS_REF Then

    Dim objSet As WbemScripting.SWbemObjectSet, objEx As SWbemObjectEx
    Dim objLocator As WbemScripting.SWbemLocator, objService As WbemScripting.SWbemServices
    
    Set objLocator = New WbemScripting.SWbemLocator
#Else
    Dim objSet As Object, objEx As Object
    Dim objLocator As Object, objService As Object
    
    Set objLocator = CreateObject("WbemScripting.SWbemLocator")
#End If

    Dim strName As String, intCount As Long, i As Long
    Dim strInfo As String
    
    Set objService = objLocator.ConnectServer
    
    Set objSet = objService.ExecQuery("Select * From Win32_InstalledWin32Program")
    
    On Error Resume Next
    
    intCount = objSet.Count
    
    If Err.Number <> 0 Then
    
        MsgBox "Need for Administrator authority user", vbCritical Or vbOKOnly, "Error for WMI select"
        
        Exit Sub
    End If
    
    On Error GoTo 0
    
    
    If robjDTDic Is Nothing Then Set robjDTDic = New Scripting.Dictionary
    
    Set robjFieldTitlesDic = GetDicOnlyKeysFromLineDelimitedChar("Name,Vendor,Version,ProgramID")
    
    i = 1
    
    For Each objEx In objSet
    
        With objEx
        
            If Not IsNull(.Name) Then
        
                strName = .Name
            
                strInfo = .Vendor & vstrDelimiterChar & .Version & vstrDelimiterChar & .ProgramID
                
                With robjDTDic
                
                    If Not .Exists(strName) Then
                
                        .Add strName, strInfo
                    End If
                End With
            End If
        End With
    
        i = i + 1
    Next
    
    SortDictionaryAboutKeys robjDTDic
End Sub



'''
''' Using Shell32.Shell, confirm whether the specified application is installed or not.
'''
''' <Argument>rstrReadApplicationName: Output</Argument>
''' <Argument>vstrApplicationName: Input</Argument>
''' <Return>Boolean</Return>
Public Function IsTheApplicationInstalledFromSpecialDirectory(ByRef rstrReadApplicationName As String, _
        ByVal vstrApplicationName As String) As Boolean
    
    Const strFolderName As String = "shell:::{7b81be6a-ce2b-4676-a29e-eb907a5126c5}"
    
    Dim strName As String, blnIsFound As Boolean
    
#If HAS_REF Then

    Dim objFolderItem As Shell32.FolderItem, objShell As Shell32.Shell
    
    Set objShell = New Shell32.Shell
#Else
    Dim objFolderItem As Object, objShell As Object
    
    Set objShell = CreateObject("Shell.Application")
#End If
    
    blnIsFound = False
    
    strName = ""
    
    With objShell.Namespace(strFolderName)
        
        For Each objFolderItem In .Items
        
            If InStr(LCase(objFolderItem.Name), LCase(vstrApplicationName)) > 0 Then
                
                rstrReadApplicationName = objFolderItem.Name
                
                blnIsFound = True
                
                Exit For
            End If
        Next
    End With
    
    IsTheApplicationInstalledFromSpecialDirectory = blnIsFound
End Function

'''
''' Return True if this computer is a X64 Windows
'''
Public Function IsThisComputerX64() As Boolean
    
#If HAS_REF Then

    Dim objSet As WbemScripting.SWbemObjectSet, objEx As WbemScripting.SWbemObjectEx
#Else
    Dim objSet As Object, objEx As Object
#End If
    
    Dim blnIsThisX64 As Boolean
      
    blnIsThisX64 = False
    
    Set objSet = CreateObject("WbemScripting.SWbemLocator").ConnectServer.ExecQuery("Select * From Win32_OperatingSystem")
    
    For Each objEx In objSet
        
        If InStr(objEx.OSArchitecture, "64") > 0 Then
            
            blnIsThisX64 = True
            
            Exit For
        End If
    Next
    
    IsThisComputerX64 = blnIsThisX64
End Function

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfIsThisComputerX64()

    Debug.Print IsThisComputerX64()
End Sub

