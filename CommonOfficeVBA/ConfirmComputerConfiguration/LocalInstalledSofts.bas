Attribute VB_Name = "LocalInstalledSofts"
'
'   Check this computer local installed softwares
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on WMI
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 13/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** get data-table of installed software information
'**---------------------------------------------
'''
'''
'''
Public Sub GetInstalledApplicationInfoForWmiConnectedComputer(ByRef robjDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        Optional ByVal vstrHostName As String = "localhost", _
        Optional ByVal vstrUserName As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vstrDelimitedChar As String = ",")

    Dim objDTDic As Scripting.Dictionary, objFieldTitlesDic As Scripting.Dictionary

    GetInstalledApplicationInfo objDTDic, objFieldTitlesDic, vstrHostName, vstrUserName, vstrPassword, vstrDelimitedChar
    
    Set robjDTCol = GetDTColFromKeyToDelimitedCharValuesString(objDTDic, vstrDelimitedChar)
    
    Set robjFieldTitlesCol = GetEnumeratorKeysColFromDic(objFieldTitlesDic)
End Sub

'''
'''
'''
Public Sub GetInstalledApplicationInfoForWmiConnectedComputerByOriginalTypes(ByRef robjDTDic As Scripting.Dictionary, _
        ByRef robjFieldTitlesDic As Scripting.Dictionary, _
        Optional ByVal vstrHostName As String = "localhost", _
        Optional ByVal vstrUserName As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vstrDelimitedChar As String = ",")

    GetInstalledApplicationInfo robjDTDic, robjFieldTitlesDic, vstrHostName, vstrUserName, vstrPassword, vstrDelimitedChar
End Sub


'''
''' This needs the Administrator authority, which WMI use Win32_InstalledWin32Program
'''
Public Sub GetInstalledWin32ProgramInformationInThisComputer(ByRef robjDTCol As Collection, _
        ByRef robjFieldTitlesCol As Collection, _
        Optional ByVal vstrDelimiterChar As String = ";")
    
    Dim objDTDic As Scripting.Dictionary, objFieldTitlesDic As Scripting.Dictionary
    
    GetInstalledWin32ProgramInformationInThisComputerByOriginalTypes objDTDic, objFieldTitlesDic, vstrDelimiterChar
    
    Set robjDTCol = GetDTColFromKeyToDelimitedCharValuesString(objDTDic, vstrDelimiterChar)
    
    Set robjFieldTitlesCol = GetEnumeratorKeysColFromDic(objFieldTitlesDic)
End Sub


'**---------------------------------------------
'** get installed program list from Shell
'**---------------------------------------------
'''
'''
'''
Public Function GetInstalledProgramNameListByShell() As Collection

    Const strFolderName As String = "shell:::{7b81be6a-ce2b-4676-a29e-eb907a5126c5}"

    Dim objCol As Collection

#If HAS_REF Then

    Dim objFolderItem As Shell32.FolderItem, objShell As Shell32.Shell
    
    Set objShell = New Shell32.Shell
    
    With objShell.Namespace(strFolderName)
#Else
    Dim objFolderItem As Object
    
    With CreateObject("Shell.Application").Namespace(strFolderName)
#End If
    
        Set objCol = New Collection
    
        For Each objFolderItem In .Items
        
            objCol.Add objFolderItem.Name
            
            'Debug.Print objFolderItem.Name & "," & objFolderItem.Path
        Next
    End With

    SortCollection objCol

    Set GetInstalledProgramNameListByShell = objCol
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestOfGetInstalledApplicationInfoOfThisComputer()

    Dim objDTCol As Collection, objFieldTitlesCol As Collection

    GetInstalledApplicationInfoForWmiConnectedComputer objDTCol, objFieldTitlesCol
    
    DebugCol objDTCol
End Sub

'''
'''
'''
Private Sub msubSanityTestOfGetInstalledApplicationInfoOfThisComputerByOriginalTypes()

    Dim objDTDic As Scripting.Dictionary, objFieldTitlesDic As Scripting.Dictionary

    GetInstalledApplicationInfoForWmiConnectedComputerByOriginalTypes objDTDic, objFieldTitlesDic
    
    DebugDic objDTDic
End Sub



'''
''' This needs the administrator authority
'''
Private Sub msubSanityTestOfGetInstalledWin32ProgramInformationInThisComputerByOriginalTypes()

    Dim objDTDic As Scripting.Dictionary, objFieldTitlesDic As Scripting.Dictionary

    ' The following needs the administrator authority
    GetInstalledWin32ProgramInformationInThisComputerByOriginalTypes objDTDic, objFieldTitlesDic
    
    DebugDic objDTDic
End Sub
