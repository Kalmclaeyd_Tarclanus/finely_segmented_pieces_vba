Attribute VB_Name = "SqlAdoConnectingUTfCsv"
'
'   Sanity tests to connecting a CSV files directory in a directory using ADO OLE DB
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
'       which should have been the installed when the Microsoft Office Excel software has been installed.
'       Dependent on UTfCreateDataTable.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri,  4/Aug/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** CsvAdoConnector
'**---------------------------------------------
'''
'''
'''
Public Sub msubSanityTestToConnectCsvAndOutputRecordsetToImmediateWiondowByASampleTable()

    Dim strInputCSVPath As String, strSQL As String
    
    strInputCSVPath = GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(BasicSampleDT, 15)

    With New CsvAdoConnector
    
        .SetInputCsvFileConnection strInputCSVPath
    
        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strInputCSVPath)
        
        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub


'''
''' Create an in-line table from the only SELECT sql inside
'''
Private Sub msubSanityTestToConnectCsvAndOutputRecordsetToImmediateWiondowByVirtualTable()
    
    Dim strSQL As String, strInputCSVPath As String

    strInputCSVPath = GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(BasicSampleDT, 15)

    With New CsvAdoConnector

        .SetInputCsvFileConnection strInputCSVPath

        .AllowToRecordSQLLog = True

        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"

        ' Confirm the Recordset of the virtual-table in the SQL without reading CSV file.

        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub

