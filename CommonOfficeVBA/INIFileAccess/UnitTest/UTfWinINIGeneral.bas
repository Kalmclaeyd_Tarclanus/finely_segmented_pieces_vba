Attribute VB_Name = "UTfWinINIGeneral"
'
'   Sanity tests for editing Windows INI file by Windows API
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       Dependent on InterfaceCall.bas and WinAPIMessageWithTimeOut
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  1/Sep/2016    Atsushi Oomura          A part of the idea has been disclosed at https://www.shuwasystem.co.jp/support/7980html/4734.html
'       Thu, 16/Jun/2022    Kalmclaeyd Tarclanus    Refactoring
'

Option Explicit

'///////////////////////////////////////////////
'/// Private WinAPI Definitions
'///////////////////////////////////////////////
#If VBA7 Then
    
    ' Retrieves an integer associated with a key in the specified section of an initialization file.
    Private Declare PtrSafe Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String) As LongPtr
    
    ' Retrieves all the keys and values for the specified section of an initialization file.
    Private Declare PtrSafe Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As LongPtr
    
    ' Retrieves a string from the specified section in an initialization file.
    Private Declare PtrSafe Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As LongPtr
    
    ' Replaces the keys and values for the specified section in an initialization file.
    Private Declare PtrSafe Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String) As LongPtr
    
    ' Copies a string into the specified section of an initialization file.
    Private Declare PtrSafe Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As LongPtr
#Else
    
    Private Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String) As Long
    Private Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
    Private Declare Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String) As Long
    Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
#End If


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Conversion test among INI file, Dictionary object, and Excel file
'**---------------------------------------------
'''
'''
'''
Public Function GetIniPathAfterCreateIniFileFromDic(ByVal vstrSection As String, _
        ByVal vstrKey As String, _
        ByVal vstrValue As String, _
        Optional ByVal vstrIniFileBaseName As String = "CharacterLimitationINITest") As String


    Dim strIniPath As String, objSectionToKeyValueDic As Scripting.Dictionary, objDic As Scripting.Dictionary
    
    
    strIniPath = GetTmporaryIniDirectoryPath() & "\" & vstrIniFileBaseName & ".ini"

    msubAddIniInfoToSectionToKeyValueDic objSectionToKeyValueDic, vstrSection, vstrKey, vstrValue

    WriteIniFromSectionToKeyValueDic strIniPath, objSectionToKeyValueDic

    GetIniPathAfterCreateIniFileFromDic = strIniPath
End Function

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubAddIniInfoToSectionToKeyValueDic(ByRef robjSectionToKeyValue As Scripting.Dictionary, ByVal vstrSection As String, ByVal vstrKey As String, ByVal vstrValue As String)

    Dim objChildDic As Scripting.Dictionary

    If robjSectionToKeyValue Is Nothing Then Set robjSectionToKeyValue = New Scripting.Dictionary
    
    With robjSectionToKeyValue
    
        If .Exists(vstrSection) Then
        
            Set objChildDic = .Item(vstrSection)
        Else
        
            Set objChildDic = New Scripting.Dictionary
        
            .Add vstrSection, objChildDic
        End If
    
        With objChildDic
        
            If .Exists(vstrKey) Then
            
                .Item(vstrKey) = vstrValue
            Else
            
                .Add vstrKey, vstrValue
            End If
        End With
    End With
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** About read INI with RegExp
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToIniSectionRegExp()

    Dim strText As String

    ClearCacheRegExpsForWinINIGeneral

    With GetIniSectionRegExp()
    
        strText = "[Section]": Debug.Print strText & " -> " & CStr(.Test(strText))
        
        strText = "[A to B]": Debug.Print strText & " -> " & CStr(.Test(strText))
        
        strText = "[A;B:C]": Debug.Print strText & " -> " & CStr(.Test(strText))
        
        strText = "[E - + C]": Debug.Print strText & " -> " & CStr(.Test(strText))
        
        strText = "[F # 1G]": Debug.Print strText & " -> " & CStr(.Test(strText))
        
        strText = "[C:\tmp\A_File.txt]": Debug.Print strText & " -> " & CStr(.Test(strText))
        
        strText = "[<https://www.Alpha;Beta-Gamma.com>]": Debug.Print strText & " -> " & CStr(.Test(strText))
        
        strText = "[<https://www.AlphaBeta02.com/SomeTest.php?>]": Debug.Print strText & " -> " & CStr(.Test(strText))
        
        strText = "[Subject-AndOp-LCase;amazon;緊急;お支払い方法]": Debug.Print strText & " -> " & CStr(.Test(strText))
        
        strText = "[プド]": Debug.Print strText & " -> " & CStr(.Test(strText))
        
        strText = "[ー]": Debug.Print strText & " -> " & CStr(.Test(strText))
        
        strText = "[プラスアップグレード]": Debug.Print strText & " -> " & CStr(.Test(strText))
    End With
End Sub
'''
'''
'''
Private Sub msubSanityTestToNullStringRegExp()

    ClearCacheRegExpsForWinINIGeneral
    
    With GetNullStringRegExp()
    
        Debug.Print "This has to be FALSE -> " & .Test("abc")
        
        Debug.Print "This has to be TRUE -> " & .Test("ab" & vbNullChar & "c")
        
        Debug.Print .Replace("ab" & vbNullChar & "c" & vbNullChar & "d", "^")
        
        Debug.Print .Replace("ab" & vbNullChar & vbNullChar & vbNullChar & vbNullChar & vbNullChar & vbNullChar & "c" & vbNullChar & vbNullChar & vbNullChar & "d", "^")
        
    End With
End Sub

'**---------------------------------------------
'** About conversion between INI and dictionary
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCreateINIFileFromDic()

    Dim objDic As Scripting.Dictionary, strIniPath As String
    
    strIniPath = GetTmporaryIniDirectoryPath() & "\Sample02.ini"

    Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add "Key2", 13
        
        .Add "FruitKey", "Grape"
    End With

    WriteIniFromKeyValueDicAboutASpecifiedSection strIniPath, "Section2", objDic
End Sub


'''
'''
'''
Private Sub msubSanityTestToLoadINIFile()

    Dim objDic As Scripting.Dictionary, strIniPath As String

    strIniPath = GetTmporaryIniDirectoryPath() & "\Sample01.ini"

    Set objDic = GetKeyValueDicFromIniASection(strIniPath, "Section1")

    DebugDic objDic
End Sub



'''
'''
'''
Private Sub msubSanityTestToCreateINIFile()

    Dim strIniPath As String
    
    strIniPath = GetTmporaryIniDirectoryPath() & "\Sample01.ini"

#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    ' About Section1

    intRet = WritePrivateProfileString("Section1", "Key1", "12", strIniPath)
    
    intRet = WritePrivateProfileString("Section1", "FruitKey", "Orange", strIniPath)

End Sub


'''
'''
'''
Private Sub msubSanityTestToGetSectionToKeyValueDicFromIni()

    Dim strIniPath As String, objDic As Scripting.Dictionary
    
    strIniPath = GetTmporaryIniDirectoryPath() & "\SamplePluralSections01.ini"
    
    Set objDic = GetSectionToKeyValueDicFromIni(strIniPath)

    DebugDic objDic
End Sub


'''
'''
'''
Private Sub msubSanityTestToWriteIniFromSectionToKeyValueDic()

    Dim strInputIniPath As String, objDic As Scripting.Dictionary
    Dim strOutputIniPath As String
    
    strInputIniPath = GetTmporaryIniDirectoryPath() & "\SamplePluralSections01.ini"
    
    Set objDic = GetSectionToKeyValueDicFromIni(strInputIniPath)


    strOutputIniPath = GetTmporaryIniDirectoryPath() & "\SampleOutputTestOfPluralSections01.ini"

    WriteIniFromSectionToKeyValueDic strOutputIniPath, objDic

End Sub


'**---------------------------------------------
'** Use Windows API directly
'**---------------------------------------------
'''
''' get a integer value from .ini file
'''
Private Sub msubSanityTestOfGetPrivateProfileInt()

    Dim strIniPath As String

#If VBA7 Then
    Dim intValue As LongPtr
#Else
    Dim intValue As Long
#End If
    
    strIniPath = mfstrGetFilePathAfterWriteASamplePrivateProfileString()
  
    intValue = GetPrivateProfileInt("Section1", "Key1", 999, strIniPath)

    Debug.Print "[Section1]Key1: " & intValue
End Sub


'''
''' get all of both key and value from a specified section of a .INI file
'''
Private Sub msubSanityTestOfGetPrivateProfileSection()
    
    Dim strIniPath As String, strReturnedString As String * 32767
    Dim strKey As String, i As Long, blnIsNullCharFound As Boolean
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If
    
    strIniPath = mfstrGetFilePathAfterWriteASamplePrivateProfileString()
    
    intRet = GetPrivateProfileSection("Section2", strReturnedString, Len(strReturnedString), strIniPath)

    blnIsNullCharFound = False
    
    For i = 1 To Len(strReturnedString)
    
        If Mid(strReturnedString, i, 1) = vbNullChar Then
        
            If blnIsNullCharFound Then
            
                Exit For
            Else
                strKey = strKey & Mid(strReturnedString, i, 1)
                
                blnIsNullCharFound = True
            End If
        Else
            strKey = strKey & Mid(strReturnedString, i, 1)
            
            blnIsNullCharFound = False
        End If
    Next
    
    strKey = Left(strKey, Len(strKey) - 1)
    
    Debug.Print strKey
End Sub


'''
''' get a string from an .INI file
'''
Private Sub msubSanityTestOfGetPrivateProfileString()
    Dim strIniPath As String, strReturnedString As String * 32767
    Dim strKey As String, i As Long, blnIsNullCharFound As Boolean
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If
       
    strIniPath = mfstrGetFilePathAfterWriteASamplePrivateProfileString()
    
    intRet = GetPrivateProfileString("Section2", "Key4", vbNullString, strReturnedString, Len(strReturnedString), strIniPath)

    blnIsNullCharFound = False
    
    For i = 1 To Len(strReturnedString)
    
        If Mid(strReturnedString, i, 1) = vbNullChar Then
        
            If blnIsNullCharFound Then
            
                Exit For
            Else
                strKey = strKey & Mid(strReturnedString, i, 1)
                
                blnIsNullCharFound = True
            End If
        Else
            strKey = strKey & Mid(strReturnedString, i, 1)
            
            blnIsNullCharFound = False
        End If
    Next i
    
    strKey = Left(strKey, Len(strKey) - 1)
        
    Debug.Print strKey
End Sub


'''
''' modify all of both key and value in a specified section of .INI file
'''
Private Sub msubSanityTestOfWritePrivateProfileSection()
    
    Dim strIniPath As String, strString As String

#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If
    
    strIniPath = GetTmporaryIniDirectoryPath() & "\Test.ini"
    
    strString = "New Key1=Net-phoenix" & vbNullChar & "New Key2=Representative" & vbNullChar & "New Key3=Tim Baker" & vbNullChar & vbNullChar

    intRet = WritePrivateProfileSection("Section1", strString, strIniPath)

    MessageBoxWithTimeoutBySecondUnit "check Test.ini out", 2, "INI file operation test", vbOKOnly Or vbInformation
End Sub


'''
''' change string values in a INI file
'''
Private Sub msubSanityTestOfWritePrivateProfileString()

    mfstrGetFilePathAfterWriteASamplePrivateProfileString True, True
End Sub

'''
'''
'''
Private Function mfstrGetFilePathAfterWriteASamplePrivateProfileString(Optional ByVal vblnAllowToForceToCreate As Boolean = False, _
        Optional ByVal vblnAllowToShowCreationResult As Boolean = False) As String
    
    Dim strIniPath As String, blnContinueToWriteINI As Boolean
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    blnContinueToWriteINI = True

    strIniPath = GetTmporaryIniDirectoryPath() & "\Test.ini"
    
    If Not vblnAllowToForceToCreate Then
    
        With New Scripting.FileSystemObject
        
            If .FileExists(strIniPath) Then
            
                blnContinueToWriteINI = False
            End If
        End With
    End If
    
    
    If blnContinueToWriteINI Then
    
        intRet = WritePrivateProfileString("Section1", "Key1", "12", strIniPath)
        
        ' Modify Key3
        intRet = WritePrivateProfileString("Section1", "New Key3", "Therressa Mineva", strIniPath)
    
        ' Add Key31
        intRet = WritePrivateProfileString("Section1", "New Key31", "API for VBA", strIniPath)
        
        ' Delete keys of the Section2
        intRet = WritePrivateProfileString("Section2", vbNullString, vbNullString, strIniPath)
    
        intRet = WritePrivateProfileString("Section2", "Section2Key01", "Value2", strIniPath)
        
        intRet = WritePrivateProfileString("Section2", "Key1", "TestingValue1Section2", strIniPath)
        
        intRet = WritePrivateProfileString("Section2", "Key4", "TestKey4String", strIniPath)

        If vblnAllowToShowCreationResult Then
        
            MessageBoxWithTimeoutBySecondUnit "check Test.ini out", 2, "INI file operation test", vbOKOnly Or vbInformation
        End If
    End If
    
    mfstrGetFilePathAfterWriteASamplePrivateProfileString = strIniPath
End Function



'''
'''
'''
Private Function GetTmporaryIniDirectoryPath() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\SampleINI"
    
    ForceToCreateDirectory strDir

    GetTmporaryIniDirectoryPath = strDir
End Function

