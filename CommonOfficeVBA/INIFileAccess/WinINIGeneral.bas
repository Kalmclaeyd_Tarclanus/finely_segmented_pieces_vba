Attribute VB_Name = "WinINIGeneral"
'
'   read and write Windows INI file by Windows API
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  1/Sep/2016    Atsushi Oomura          A part of the idea has been disclosed at https://www.shuwasystem.co.jp/support/7980html/4734.html
'       Thu, 16/Jun/2022    Kalmclaeyd Tarclanus    Refactoring
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
'**---------------------------------------------
'** Regular expression static variable declarations
'**---------------------------------------------

Private Const mstrJapaneseCharPattern As String = "-ñ@-ħ-ŬŜßê-êE["

'///////////////////////////////////////////////
'/// Private WinAPI Definitions
'///////////////////////////////////////////////
#If VBA7 Then

    ' Retrieves an integer associated with a key in the specified section of an initialization file.
    Private Declare PtrSafe Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String) As LongPtr
    
    ' Retrieves all the keys and values for the specified section of an initialization file.
    Private Declare PtrSafe Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As LongPtr
    
    ' Retrieves a string from the specified section in an initialization file.
    Private Declare PtrSafe Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As LongPtr
    
    
    ' Replaces the keys and values for the specified section in an initialization file.
    Private Declare PtrSafe Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String) As LongPtr
    
    ' Copies a string into the specified section of an initialization file.
    Private Declare PtrSafe Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As LongPtr
#Else

    Private Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String) As Long
    Private Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
    Private Declare Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String) As Long
    Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
#End If

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjNullStringRegExp As VBScript_RegExp_55.RegExp

Private mobjIniSectionRegExp As VBScript_RegExp_55.RegExp

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToReadFromShapeOnXlSheet()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "WinINIGeneralForXl,UTfWinINIGeneral,UTfWinINIGeneralForXl,ReadFromShapeOnXlSheet"
End Sub


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Null-char RegExp
'**---------------------------------------------
'''
'''
'''
Public Function GetNullStringRegExp() As VBScript_RegExp_55.RegExp

    If mobjNullStringRegExp Is Nothing Then
    
        Set mobjNullStringRegExp = New VBScript_RegExp_55.RegExp
    
        With mobjNullStringRegExp
        
            .Pattern = "[" & vbNullChar & "]{1,}"
        
            .Global = True
        End With
    End If

    Set GetNullStringRegExp = mobjNullStringRegExp
End Function

'''
'''
'''
Public Function GetIniSectionRegExp() As VBScript_RegExp_55.RegExp

    If mobjIniSectionRegExp Is Nothing Then
    
        Set mobjIniSectionRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjIniSectionRegExp
        
            .Pattern = "\[([\w _;:\?\.\-\+\#\\\/\<\>" & mstrJapaneseCharPattern & "]{1,})\]"
        End With
    End If

    Set GetIniSectionRegExp = mobjIniSectionRegExp
End Function

'''
'''
'''
Public Sub ClearCacheRegExpsForWinINIGeneral()

    Set mobjNullStringRegExp = Nothing
    
    Set mobjIniSectionRegExp = Nothing
End Sub


'**---------------------------------------------
'** conversion between Scripting.Dictionary and INI file
'**---------------------------------------------
'''
'''
'''
Public Function GetKeyValueDicFromIniASection(ByVal vstrIniPath As String, ByVal vstrSection As String) As Scripting.Dictionary

    Dim strReturnedString As String * 32767, strInfo As String
    Dim varString As Variant, strSourceText As String
    Dim objDic As Scripting.Dictionary, strKey As String, strValue As String, intP1 As Long

#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If
    
    intRet = GetPrivateProfileSection(vstrSection, strReturnedString, Len(strReturnedString), vstrIniPath)
    
    
    With GetNullStringRegExp()
    
        strInfo = Trim(.Replace(strReturnedString, vbTab))
    End With
    
    Set objDic = New Scripting.Dictionary
    
    For Each varString In Split(strInfo, vbTab)
    
        'Debug.Print varString
        
        strSourceText = varString
        
        If IsDelimiterCharFoundAndThenGetLeftPartAndRightPartByDelimiting(strKey, strValue, strSourceText, "=") Then
        
            objDic.Add strKey, strValue
        End If
    Next

    Set GetKeyValueDicFromIniASection = objDic
End Function

'''
'''
'''
Public Sub WriteIniFromKeyValueDicAboutASpecifiedSection(ByVal vstrIniPath As String, ByVal vstrSection As String, ByVal vobjDic As Scripting.Dictionary)

    Dim varKey As Variant, strKey As String, strValue As String
    
#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    If Not vobjDic Is Nothing Then
        
        If vobjDic.Count > 0 Then
        
            With vobjDic
            
                For Each varKey In .Keys
        
                    strKey = varKey
                    
                    strValue = CStr(.Item(varKey))
        
                    intRet = WritePrivateProfileString(vstrSection, strKey, strValue, vstrIniPath)
                Next
            End With
        End If
    End If
End Sub


'''
'''
'''
Public Function GetSectionToKeyValueDicFromIni(ByVal vstrIniPath As String) As Scripting.Dictionary

    Dim strLine As String, varDataLines As Variant, i As Long, intRowMax As Long
    
    Dim strSection As String, objChildSectionDic As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary, strKey As String, strValue As String, intP1 As Long
    Dim objIniSectionRegExp As VBScript_RegExp_55.RegExp, objMatch As VBScript_RegExp_55.Match
    

#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If
    
    
    Set objIniSectionRegExp = GetIniSectionRegExp()
    
    LoadAllTextLinesArrayWithHighSpeed varDataLines, vstrIniPath
    
    Set objDic = New Scripting.Dictionary
    
    Set objChildSectionDic = Nothing
    
    strSection = ""
    
    For i = LBound(varDataLines) To UBound(varDataLines)
    
        strLine = varDataLines(i)
        
        If InStr(1, Trim(strLine), ";") <> 1 Then   ' detect a INI file comment line
        
            With objIniSectionRegExp
            
                If .Test(strLine) Then
                
                    ' The line is a Section string
                
                    With .Execute(strLine)
                    
                        Set objMatch = .Item(0)
                    
                        If strSection <> "" And Not objChildSectionDic Is Nothing Then
                            
                            If objChildSectionDic.Count > 0 Then
                            
                                objDic.Add strSection, objChildSectionDic
                            End If
                        End If
                    
                        strSection = objMatch.SubMatches.Item(0)
                        
                        Set objChildSectionDic = New Scripting.Dictionary
                    End With
                Else
                    ' The line is a key-value
                
                    intP1 = InStr(1, strLine, "=")
                    
                    If intP1 > 0 Then
                    
                        strKey = Left(strLine, intP1 - 1)
                        
                        strValue = Right(strLine, Len(strLine) - intP1)
                        
                        objChildSectionDic.Add strKey, strValue
                    End If
                End If
            End With
        End If
    Next
    
    ' final process
    If strSection <> "" And Not objChildSectionDic Is Nothing Then
        
        If objChildSectionDic.Count > 0 Then
        
            objDic.Add strSection, objChildSectionDic
        End If
    End If

    Set GetSectionToKeyValueDicFromIni = objDic
End Function

'''
'''
'''
Public Sub WriteIniFromSectionToKeyValueDic(ByVal vstrIniPath As String, ByVal vobjSectionToKeyValueDic As Scripting.Dictionary)

    Dim varSection As Variant, objChildSectionDic As Scripting.Dictionary
    Dim strSection As String, varKey As Variant, strKey As String, strValue As String

#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    With vobjSectionToKeyValueDic
    
        For Each varSection In .Keys
        
            strSection = varSection
        
            Set objChildSectionDic = .Item(varSection)
        
            With objChildSectionDic
            
                For Each varKey In .Keys
                
                    strKey = varKey
                    
                    strValue = .Item(varKey)
                    
                    intRet = WritePrivateProfileString(strSection, strKey, strValue, vstrIniPath)
                Next
            End With
        Next
    End With
End Sub


'''
'''
'''
Public Function GetIniTestFilePathAfterCheckExistenceOrCreateWhenItDoesntExist() As String

    Dim strIniFilePath As String
    
    strIniFilePath = GetTmporaryIniDirectoryPath() & "\SamplePluralSections01.ini"

    With New Scripting.FileSystemObject
    
        If Not .FileExists(strIniFilePath) Then
        
            msubSanityTestToCreateINIFileWithPluralSections
        End If
    End With

    GetIniTestFilePathAfterCheckExistenceOrCreateWhenItDoesntExist = strIniFilePath
End Function

'''
'''
'''
Private Sub msubSanityTestToCreateINIFileWithPluralSections()

    Dim strIniPath As String
    
    strIniPath = GetTmporaryIniDirectoryPath() & "\SamplePluralSections01.ini"

#If VBA7 Then
    Dim intRet As LongPtr
#Else
    Dim intRet As Long
#End If

    ' About Section1

    intRet = WritePrivateProfileString("Section1", "Key1", "12", strIniPath)
    
    intRet = WritePrivateProfileString("Section1", "FruitKey", "Orange", strIniPath)


    ' About Section2

    intRet = WritePrivateProfileString("Section2", "Key2", "35", strIniPath)
    
    intRet = WritePrivateProfileString("Section2", "FruitKey", "GrapeFruits", strIniPath)

    ' About Section3

    intRet = WritePrivateProfileString("Section3", "Key3", "3021", strIniPath)
    
    intRet = WritePrivateProfileString("Section3", "FruitKey", "Pineapple", strIniPath)
End Sub


'**---------------------------------------------
'** About temporary logging directory path
'**---------------------------------------------
'''
'''
'''
Public Function GetTmporaryIniDirectoryPath() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\SampleINI"
    
    ForceToCreateDirectory strDir

    GetTmporaryIniDirectoryPath = strDir
End Function





