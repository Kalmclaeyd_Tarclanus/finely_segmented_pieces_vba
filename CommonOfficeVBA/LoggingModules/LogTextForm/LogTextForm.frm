VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} LogTextForm 
   Caption         =   "LogTextForm"
   ClientHeight    =   7260
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   10220
   OleObjectBlob   =   "LogTextForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "LogTextForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   Simple displaying log text form
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  8/Feb/2023    Kalmclaeyd Tarclanus    Improved
'

Option Explicit

'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function GetInputState Lib "user32" () As Boolean
#Else
    Private Declare Function GetInputState Lib "user32" () As Boolean
#End If


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjFormTopMostEnabledCtlHdr As FormTopMostEnabledCtlHdr


Private mintInitializing As Long


'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub UserForm_Initialize()

    mintInitializing = 0

    InitializeFormTopMostEnabledControlHandler mobjFormTopMostEnabledCtlHdr, Me

    With mobjFormTopMostEnabledCtlHdr
        
        Set .WindowTopMostPlacingEnablingButton = chkTopMostEnabled
        
        .RefreshWindowTopMostPlacingEnablingToggleButton
    End With

    lblShortMessage.Caption = ""
    
    txtLogText.Text = ""
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
''' Let/Get this form window TopMost state
'''
Public Property Get IsWindowTopMostPlacingEnabled() As Boolean

    IsWindowTopMostPlacingEnabled = mobjFormTopMostEnabledCtlHdr.IsWindowTopMostPlacingEnabled
End Property
Public Property Let IsWindowTopMostPlacingEnabled(ByVal vblnIsWindowTopMostPlacingEnabled As Boolean)

    mobjFormTopMostEnabledCtlHdr.IsWindowTopMostPlacingEnabled = vblnIsWindowTopMostPlacingEnabled
End Property


'''
''' log-title
'''
Public Property Get ShortMessage() As String

    ShortMessage = lblShortMessage.Caption
End Property
Public Property Let ShortMessage(ByVal vstrShortMessage As String)

    lblShortMessage.Caption = vstrShortMessage
End Property

'''
''' main log
'''
Public Property Get Text() As String

    Text = txtLogText.Text
End Property
Public Property Let Text(ByVal vstrLogText As String)

    With txtLogText
        
        On Error Resume Next
    
        .SetFocus   ' display the latest text line
        
        On Error GoTo 0
        
        .Text = vstrLogText
    End With
    
    If GetInputState() Then VBA.Interaction.DoEvents
End Property


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
Public Sub ShowByModelessWithWindowTopMostPosition()

    With Me
        
        .Show vbModeless
        
        .SetFormToTopMost
    End With
End Sub


Public Sub SetFormToTopMost()
    
    If Not mobjFormTopMostEnabledCtlHdr Is Nothing Then
        
        With mobjFormTopMostEnabledCtlHdr
        
            .SetFormToTopMost
            
            .RefreshWindowTopMostPlacingEnablingToggleButton
        End With
    End If
End Sub
