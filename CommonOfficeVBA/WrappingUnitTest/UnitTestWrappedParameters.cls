VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "UnitTestWrappedParameters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Output results to Excel sheet for the formal wrapped unit tests
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Visualize the results of Unit-tests
'
'   Dependency Abstract:
'       Dependent on Excel for sheet-output
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 25/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjUnitTestSheetFormatting As UnitTestSheetFormatSetting

Private mstrModuleNameOfTestingFunction As String

Private mobjTestFunctionNameToRightIOGettingFunctionNameDic As Scripting.Dictionary

Private mobjTestFunctionNameToOriginalFunctionNameBeforeWrappingDic As Scripting.Dictionary


Private mintCountOfInputs As Long

Private mintCountOfOutputs As Long


Private mstrUnitTestResultBookBaseName As String

Private mstrAutoShapeUnitTestTheme As String

'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjUnitTestSheetFormatting = New UnitTestSheetFormatSetting
    
    Set mobjTestFunctionNameToRightIOGettingFunctionNameDic = New Scripting.Dictionary
    
    Set mobjTestFunctionNameToOriginalFunctionNameBeforeWrappingDic = New Scripting.Dictionary
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Get ModuleNameOfTestingFunction() As String

    ModuleNameOfTestingFunction = mstrModuleNameOfTestingFunction
End Property
Public Property Let ModuleNameOfTestingFunction(ByVal vstrModuleNameOfTestingFunction As String)

    mstrModuleNameOfTestingFunction = vstrModuleNameOfTestingFunction
End Property


Public Property Get SheetFormatSetting() As UnitTestSheetFormatSetting

    Set SheetFormatSetting = mobjUnitTestSheetFormatting
End Property


Public Property Get TestFunctionNameToRightIOGettingFunctionNameDic() As Scripting.Dictionary

    Set TestFunctionNameToRightIOGettingFunctionNameDic = mobjTestFunctionNameToRightIOGettingFunctionNameDic
End Property

Public Property Get TestFunctionNameToOriginalFunctionNameBeforeWrappingDic() As Scripting.Dictionary

    Set TestFunctionNameToOriginalFunctionNameBeforeWrappingDic = mobjTestFunctionNameToOriginalFunctionNameBeforeWrappingDic
End Property


Public Property Get CountOfInputs() As Long

    CountOfInputs = mintCountOfInputs
End Property

Public Property Get CountOfOutputs() As Long

    CountOfOutputs = mintCountOfOutputs
End Property


Public Property Get UnitTestResultBookBaseName() As String

    UnitTestResultBookBaseName = mstrUnitTestResultBookBaseName
End Property
Public Property Let UnitTestResultBookBaseName(ByVal vstrUnitTestResultBookBaseName As String)

    mstrUnitTestResultBookBaseName = vstrUnitTestResultBookBaseName
End Property

Public Property Get AutoShapeUnitTestTheme() As String

    AutoShapeUnitTestTheme = mstrAutoShapeUnitTestTheme
End Property
Public Property Let AutoShapeUnitTestTheme(ByVal vstrAutoShapeUnitTestTheme As String)

    mstrAutoShapeUnitTestTheme = vstrAutoShapeUnitTestTheme
End Property

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub SetUpTargetFunctionParameters(ByVal vstrTargetFunctionName As String, _
        ByVal vstrRightIOGettingFunctionName As String, _
        Optional ByVal vstrOriginalFunctionNameBeforeWrapping As String = "", _
        Optional ByVal vintCountOfOutputs As Long = 1, Optional ByVal vintCountOfInputs As Long = 1)


    With mobjTestFunctionNameToRightIOGettingFunctionNameDic
    
        .Add vstrTargetFunctionName, vstrRightIOGettingFunctionName
    End With

    If vstrOriginalFunctionNameBeforeWrapping <> "" Then
    
        With mobjTestFunctionNameToOriginalFunctionNameBeforeWrappingDic
        
            .Add vstrTargetFunctionName, vstrOriginalFunctionNameBeforeWrapping
        End With
    End If

    mintCountOfOutputs = vintCountOfOutputs

    mintCountOfInputs = vintCountOfInputs
End Sub

