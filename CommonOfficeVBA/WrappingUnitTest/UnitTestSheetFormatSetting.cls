VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "UnitTestSheetFormatSetting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Output results to Excel sheet for the formal wrapped unit tests
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Visualize the results of Unit-tests
'
'   Dependency Abstract:
'       Dependent on Excel for sheet-output
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 25/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjInputFieldTitlesCol As Collection

Private mobjInputFieldTitleToColumnWidthDic As Scripting.Dictionary
    
Private mobjOutputFieldTitlesCol As Collection

Private mobjOutputFieldTitleToColumnWidthDic As Scripting.Dictionary

'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub Class_Terminate()

    Set mobjInputFieldTitlesCol = Nothing
    
    If Not mobjInputFieldTitleToColumnWidthDic Is Nothing Then
    
        mobjInputFieldTitleToColumnWidthDic.RemoveAll
    End If
    
    Set mobjInputFieldTitleToColumnWidthDic = Nothing
    
    Set mobjOutputFieldTitlesCol = Nothing
    
    If Not mobjOutputFieldTitleToColumnWidthDic Is Nothing Then
    
        mobjOutputFieldTitleToColumnWidthDic.RemoveAll
    End If

    Set mobjOutputFieldTitleToColumnWidthDic = Nothing
End Sub


'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Get InputFieldTitlesCol() As Collection

    Set InputFieldTitlesCol = mobjInputFieldTitlesCol
End Property

Public Property Get InputFieldTitleToColumnWidthDic() As Scripting.Dictionary

    Set InputFieldTitleToColumnWidthDic = mobjInputFieldTitleToColumnWidthDic
End Property

Public Property Get OutputFieldTitlesCol() As Collection

    Set OutputFieldTitlesCol = mobjOutputFieldTitlesCol
End Property

Public Property Get OutputFieldTitleToColumnWidthDic() As Scripting.Dictionary

    Set OutputFieldTitleToColumnWidthDic = mobjOutputFieldTitleToColumnWidthDic
End Property

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vstrInputColumnNameAndWidthDelimitedByComma: Input</Argument>
''' <Argument>vstrOutputColumnNameAndWidthDelimitedByComma: Input</Argument>
Public Sub SetUpIOColumnNameAndColumnWidth(ByVal vstrInputColumnNameAndWidthDelimitedByComma As String, _
        ByVal vstrOutputColumnNameAndWidthDelimitedByComma As String)

    
    Set mobjInputFieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar(vstrInputColumnNameAndWidthDelimitedByComma)

    Set mobjInputFieldTitlesCol = GetEnumeratorKeysColFromDic(mobjInputFieldTitleToColumnWidthDic)

    Set mobjOutputFieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar(vstrOutputColumnNameAndWidthDelimitedByComma)

    Set mobjOutputFieldTitlesCol = GetEnumeratorKeysColFromDic(mobjOutputFieldTitleToColumnWidthDic)
End Sub

