Attribute VB_Name = "UnitTestWrap"
'
'   Formal wrapped unit tests for between input-output testing function and intput-right-output predetermined data
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Refactoring tools for variable names
'
'   Dependency Abstract:
'       Dependent on InterfaceCall.bas
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 17/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const PRIOR_TO_EXCEL = True

'///////////////////////////////////////////////
'/// User defined type
'///////////////////////////////////////////////
Public Type UnitTestSheetFormatSettingStruct

    InputFieldTitlesCol As Collection

    InputFieldTitleToColumnWidthDic As Scripting.Dictionary
    
    OutputFieldTitlesCol As Collection

    OutputFieldTitleToColumnWidthDic As Scripting.Dictionary
End Type

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
#If PRIOR_TO_EXCEL Then

Private mobjCacheApplication As Excel.Application
#Else
Private mobjCacheApplication As Object
#End If


'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToUnitTestWrap()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UnitTestWrapForXl,UnitTestSheetFormatSetting,UnitTestWrappedParameters,UTfMetaMetaStruct,LTfMetaMetaStruct,UTfMetaMetaComment,MetaMetaStructForXl"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Unit test wrapper tools
'**---------------------------------------------
'''
'''
'''
Public Function IsPassedCompletelyByInterfaceOfOneInputAndPluralOutputs(ByRef robjDTCol As Collection, _
        ByVal vstrFunctionName As String, _
        ByVal vstrInputToPluralRightOutputsDicName As String, _
        ByVal vintCountOfOutputs As Long, _
        Optional ByVal vblnNoLogging As Boolean = False, _
        Optional ByVal vstrModuleName As String = "MetaMetaVarNaming", _
        Optional ByVal vstrTargetFunctionNameBeforeWrapping As String = "") As Boolean


    Dim varKey As Variant, strVariableName As String, objRowCol As Collection
    
    Dim objInputToPluralRightOutputsDic As Scripting.Dictionary, blnIsOutputValid As Boolean, blnAreAllValid As Boolean
    
    Dim strInput As String, objRightOutputs As Collection, varRightOutput As Variant, strRightOutput As String, i As Long, objInterfaceOutputCol As Collection
    
    Dim strOutputs() As String, blnIsValid() As Boolean, strTargetFunctionNameBeforeWrapping As String, strOutput As String
    
    
    ReDim strOutputs(vintCountOfOutputs - 1)
    
    ReDim blnIsValid(vintCountOfOutputs - 1)
    
    
    Set objInputToPluralRightOutputsDic = GetDictionaryFromCallBackInterfaceWithCache(vstrInputToPluralRightOutputsDicName)
    
    blnAreAllValid = True
    
    If vstrTargetFunctionNameBeforeWrapping <> "" Then
    
        strTargetFunctionNameBeforeWrapping = vstrTargetFunctionNameBeforeWrapping
    Else
        strTargetFunctionNameBeforeWrapping = vstrFunctionName
    End If
    
    With objInputToPluralRightOutputsDic
    
        If Not vblnNoLogging Then
        
            For Each varKey In .Keys
            
                strInput = varKey
                
                If vintCountOfOutputs = 1 Then
                
                    strRightOutput = .Item(varKey)
                
                    Set objRightOutputs = New Collection: objRightOutputs.Add strRightOutput
                
                    strOutput = GetStringFromCallBackInterfaceAndOneStringArgumentWithCache(vstrFunctionName, strInput)
                    
                    Set objInterfaceOutputCol = New Collection: objInterfaceOutputCol.Add strOutput
                Else
                    Set objRightOutputs = .Item(varKey)
                
                    Set objInterfaceOutputCol = GetColByCallBackInterfaceBySingleArgumentWithCache(vstrFunctionName, strInput)
                End If

                Set objRowCol = New Collection

                With objRowCol

                    .Add vstrModuleName
                    
                    .Add strTargetFunctionNameBeforeWrapping

                    
                    .Add strInput
                End With
            
                i = 0
                For Each varRightOutput In objRightOutputs
                
                    strRightOutput = varRightOutput
                
                    objRowCol.Add strRightOutput
                
                    strOutputs(i) = objInterfaceOutputCol.Item(i + 1)
                
                    blnIsValid(i) = (StrComp(strOutputs(i), strRightOutput) = 0)
                
                    If Not blnIsValid(i) Then
                    
                        blnAreAllValid = False
                    End If
                
                    i = i + 1
                Next
                
                For i = 0 To (vintCountOfOutputs - 1)
                
                    objRowCol.Add blnIsValid(i)
                Next
                
                robjDTCol.Add objRowCol
            Next
        Else
            For Each varKey In .Keys
            
                strInput = varKey
                
                If vintCountOfOutputs = 1 Then
                
                    strRightOutput = .Item(varKey)
                
                    Set objRightOutputs = New Collection: objRightOutputs.Add strRightOutput
                
                    strOutput = GetStringFromCallBackInterfaceAndOneStringArgumentWithCache(vstrFunctionName, strInput)
                    
                    Set objInterfaceOutputCol = New Collection: objInterfaceOutputCol.Add strOutput
                Else
                    Set objRightOutputs = .Item(varKey)
                
                    Set objInterfaceOutputCol = GetColByCallBackInterfaceBySingleArgumentWithCache(vstrFunctionName, strInput)
                End If
                
                i = 0
                
                For Each varRightOutput In objRightOutputs
                
                    strRightOutput = varRightOutput
                
                    strOutputs(i) = objInterfaceOutputCol.Item(i + 1)
                
                    blnIsValid(i) = (StrComp(strOutputs(i), strRightOutput) = 0)
                
                    If Not blnIsValid(i) Then
                    
                        blnAreAllValid = False
                    End If
                
                    i = i + 1
                Next
            Next
        End If
    End With

    IsPassedCompletelyByInterfaceOfOneInputAndPluralOutputs = blnAreAllValid
End Function




'''
'''
'''
Public Function IsPassedCompletelyByInterfaceOfOneInputAndThreeOutputs(ByRef robjDTCol As Collection, _
        ByVal vstrFunctionName As String, _
        ByVal vstrInputToPluralRightOutputsDicName As String, _
        Optional ByVal vblnNoLogging As Boolean = False, _
        Optional ByVal vstrModuleName As String = "MetaMetaVarNaming", _
        Optional ByVal vstrTargetFunctionNameBeforeWrapping As String = "") As Boolean


    Dim varKey As Variant, strVariableName As String, objRowCol As Collection
    
    Dim objInputToPluralRightOutputsDic As Scripting.Dictionary, blnIsOutputValid As Boolean, blnAreAllValid As Boolean
    
    Dim strInput As String, objRightOutputs As Collection, varRightOutput As Variant, strRightOutput As String, i As Long, objInterfaceOutputCol As Collection
    
    Dim strOutputs() As String, blnIsValid() As Boolean, strTargetFunctionNameBeforeWrapping As String
    
    ReDim strOutputs(2)
    
    ReDim blnIsValid(2)
    
    
    Set objInputToPluralRightOutputsDic = GetDictionaryFromCallBackInterfaceWithCache(vstrInputToPluralRightOutputsDicName)
    
    blnAreAllValid = True
    
    If vstrTargetFunctionNameBeforeWrapping <> "" Then
    
        strTargetFunctionNameBeforeWrapping = vstrTargetFunctionNameBeforeWrapping
    Else
        strTargetFunctionNameBeforeWrapping = vstrFunctionName
    End If
    
    With objInputToPluralRightOutputsDic
    
        If Not vblnNoLogging Then
        
            For Each varKey In .Keys
            
                strInput = varKey
                
                Set objRightOutputs = .Item(varKey)
                
                Set objInterfaceOutputCol = GetColByCallBackInterfaceByFourArgumentsWithCache(vstrFunctionName, strInput, strOutputs(0), strOutputs(1), strOutputs(2))
            

                Set objRowCol = New Collection

                With objRowCol

                    .Add vstrModuleName
                    
                    .Add strTargetFunctionNameBeforeWrapping

                    
                    .Add strInput
                End With
            
                i = 0
                For Each varRightOutput In objRightOutputs
                
                    strRightOutput = varRightOutput
                
                    objRowCol.Add strRightOutput
                
                    strOutputs(i) = objInterfaceOutputCol.Item(i + 1)
                
                    blnIsValid(i) = (StrComp(strOutputs(i), strRightOutput) = 0)
                
                    If Not blnIsValid(i) Then
                    
                        blnAreAllValid = False
                    End If
                
                    i = i + 1
                Next
                
                For i = 0 To 2
                
                    objRowCol.Add blnIsValid(i)
                Next
                
                robjDTCol.Add objRowCol
            Next
        Else
            For Each varKey In .Keys
            
                strInput = varKey
                
                Set objRightOutputs = .Item(varKey)
                
                Set objInterfaceOutputCol = GetColByCallBackInterfaceByFourArgumentsWithCache(vstrFunctionName, strInput, strOutputs(0), strOutputs(1), strOutputs(2))

                i = 0
                For Each varRightOutput In objRightOutputs
                
                    strRightOutput = varRightOutput
                
                    strOutputs(i) = objInterfaceOutputCol.Item(i + 1)
                
                    blnIsValid(i) = (StrComp(strOutputs(i), strRightOutput) = 0)
                
                    If Not blnIsValid(i) Then
                    
                        blnAreAllValid = False
                    End If
                
                    i = i + 1
                Next
                
            Next
        End If
    End With

    IsPassedCompletelyByInterfaceOfOneInputAndThreeOutputs = blnAreAllValid
End Function

'''
'''
'''
Public Function IsPassedCompletelyByInterfaceOfBothFunctionNameAndIO(ByRef robjDTCol As Collection, _
        ByVal vstrFunctionName As String, _
        ByVal vstrInputToRightOutputDicName As String, _
        Optional ByVal vblnNoLogging As Boolean = False, _
        Optional ByVal vstrModuleName As String = "MetaMetaVarNaming") As Boolean


    Dim varKey As Variant, strVariableName As String, strRightOutput As String, strConvertedName As String, objRowCol As Collection
    
    Dim objInputToRightOutputDic As Scripting.Dictionary, blnIsOutputValid As Boolean, blnAreAllValid As Boolean
    
    
    Set objInputToRightOutputDic = GetDictionaryFromCallBackInterfaceWithCache(vstrInputToRightOutputDicName)
    
    blnAreAllValid = True
    
    With objInputToRightOutputDic
    
        If Not vblnNoLogging Then
    
            For Each varKey In .Keys
            
                strVariableName = varKey
                
                strRightOutput = .Item(varKey)
                
                strConvertedName = GetStringFromCallBackInterfaceAndOneStringArgumentWithCache(vstrFunctionName, strVariableName)
            
                If StrComp(strConvertedName, strRightOutput) = 0 Then
                
                    blnIsOutputValid = True
                Else
                    blnIsOutputValid = False
                    
                    blnAreAllValid = False
                End If
                
                Set objRowCol = New Collection
                
                With objRowCol
                
                    .Add vstrModuleName
                    
                    .Add vstrFunctionName
                    
                    
                    .Add strVariableName
                    
                    .Add strRightOutput
                    
                    .Add blnIsOutputValid
                End With
                
                robjDTCol.Add objRowCol
            Next
        Else
            For Each varKey In .Keys
            
                strVariableName = varKey
                
                strRightOutput = .Item(varKey)
                
                strConvertedName = GetStringFromCallBackInterfaceAndOneArgument(vstrFunctionName, strVariableName)
            
                If StrComp(strConvertedName, strRightOutput) = 0 Then
                
                    blnIsOutputValid = True
                Else
                    blnIsOutputValid = False
                    
                    blnAreAllValid = False
                End If
            Next
        End If
    End With

    IsPassedCompletelyByInterfaceOfBothFunctionNameAndIO = blnAreAllValid
End Function


'**---------------------------------------------
'** Call back interface by cache Application object
'**---------------------------------------------
'''
''' call back for Scripting.Dictionary
'''
Private Function GetDictionaryFromCallBackInterfaceWithCache(ByVal vstrOperationInterface As String) As Scripting.Dictionary
    
    Dim objDic As Scripting.Dictionary

#If PRIOR_TO_EXCEL Then

    Dim objApplication As Excel.Application
#Else
    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''
#End If

    Set objDic = Nothing
    
    If mobjCacheApplication Is Nothing Then
    
        Set mobjCacheApplication = GetOfficeApplicationObjectByTwoMethods()
    End If
    
    Set objApplication = mobjCacheApplication
    
    On Error Resume Next

    Set objDic = objApplication.Run(vstrOperationInterface)

    On Error GoTo 0

    Set GetDictionaryFromCallBackInterfaceWithCache = objDic
End Function

'''
'''
'''
Private Function GetStringFromCallBackInterfaceAndOneStringArgumentWithCache(ByVal vstrOperationInterface As String, _
        ByRef rvarArgument As Variant) As String
    
#If PRIOR_TO_EXCEL Then

    Dim objApplication As Excel.Application
#Else
    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''
#End If
    
    Dim strValue As String
    
    If mobjCacheApplication Is Nothing Then
    
        Set mobjCacheApplication = GetOfficeApplicationObjectByTwoMethods()
    End If
    
    Set objApplication = mobjCacheApplication
    
    strValue = ""
    
    On Error Resume Next

    strValue = objApplication.Run(vstrOperationInterface, rvarArgument)

    On Error GoTo 0

    GetStringFromCallBackInterfaceAndOneStringArgumentWithCache = strValue
End Function

'''
''' for single, testing interface
'''
''' If more than one Excel application started by multi-processes on Windows, The single 'Application' keyword may often causes some unexpected errors.
'''
''' <Argument>vstrOperationInterface: Input</Argument>
''' <Argument>rvarArgument1: Input</Argument>
''' <Return>Collection</Return>
Private Function GetColByCallBackInterfaceBySingleArgumentWithCache(ByVal vstrOperationInterface As String, _
        ByRef rvarArgument1 As Variant) As Collection

    Dim objCol As Collection

#If PRIOR_TO_EXCEL Then

    Dim objApplication As Excel.Application
#Else
    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''
#End If

    If mobjCacheApplication Is Nothing Then
    
        Set mobjCacheApplication = GetOfficeApplicationObjectByTwoMethods()
    End If
    
    Set objApplication = mobjCacheApplication

    On Error Resume Next
    
    If Not objApplication Is Nothing Then
    
        Set objCol = objApplication.Run(vstrOperationInterface, rvarArgument1)
    End If

    On Error GoTo 0
    
    Set GetColByCallBackInterfaceBySingleArgumentWithCache = objCol
End Function

'''
''' for Three, testing interface
'''
''' If more than one Excel application started by multi-processes on Windows, The single 'Application' keyword may often causes some unexpected errors.
'''
''' <Argument>vstrOperationInterface: Input</Argument>
''' <Argument>rvarArgument1: Input</Argument>
''' <Argument>rvarArgument2: Input</Argument>
''' <Argument>rvarArgument3: Input</Argument>
''' <Return>Collection</Return>
Private Function GetColByCallBackInterfaceByThreeArgumentsWithCache(ByVal vstrOperationInterface As String, _
        ByRef rvarArgument1 As Variant, _
        ByRef rvarArgument2 As Variant, _
        ByRef rvarArgument3 As Variant) As Collection

    Dim objCol As Collection

#If PRIOR_TO_EXCEL Then

    Dim objApplication As Excel.Application
#Else
    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''
#End If

    If mobjCacheApplication Is Nothing Then
    
        Set mobjCacheApplication = GetOfficeApplicationObjectByTwoMethods()
    End If
    
    Set objApplication = mobjCacheApplication

    On Error Resume Next
    
    If Not objApplication Is Nothing Then
    
        Set objCol = objApplication.Run(vstrOperationInterface, rvarArgument1, rvarArgument2, rvarArgument3)
    End If

    On Error GoTo 0
    
    Set GetColByCallBackInterfaceByThreeArgumentsWithCache = objCol
End Function


'''
''' for four, testing interface
'''
''' If more than one Excel application started by multi-processes on Windows, The single 'Application' keyword may often causes some unexpected errors.
'''
''' <Argument>vstrOperationInterface: Input</Argument>
''' <Argument>rvarArgument1: Input</Argument>
''' <Argument>rvarArgument2: Input</Argument>
''' <Argument>rvarArgument3: Input</Argument>
''' <Argument>rvarArgument4: Input</Argument>
''' <Return>Collection</Return>
Private Function GetColByCallBackInterfaceByFourArgumentsWithCache(ByVal vstrOperationInterface As String, _
        ByRef rvarArgument1 As Variant, _
        ByRef rvarArgument2 As Variant, _
        ByRef rvarArgument3 As Variant, _
        ByRef rvarArgument4 As Variant) As Collection

    Dim objCol As Collection

#If PRIOR_TO_EXCEL Then

    Dim objApplication As Excel.Application
#Else
    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''
#End If

    If mobjCacheApplication Is Nothing Then
    
        Set mobjCacheApplication = GetOfficeApplicationObjectByTwoMethods()
    End If
    
    Set objApplication = mobjCacheApplication

    On Error Resume Next
    
    If Not objApplication Is Nothing Then
    
        Set objCol = objApplication.Run(vstrOperationInterface, rvarArgument1, rvarArgument2, rvarArgument3, rvarArgument4)
    End If

    On Error GoTo 0
    
    Set GetColByCallBackInterfaceByFourArgumentsWithCache = objCol
End Function


'''
''' for five, testing interface.
'''
''' If more than one Excel application started by multi-processes on Windows, The single 'Application' keyword may often causes some unexpected errors.
'''
''' <Argument>vstrOperationInterface: Input</Argument>
''' <Argument>rvarArgument1: Input</Argument>
''' <Argument>rvarArgument2: Input</Argument>
''' <Argument>rvarArgument3: Input</Argument>
''' <Argument>rvarArgument4: Input</Argument>
''' <Argument>rvarArgument5: Input</Argument>
''' <Return>Collection</Return>
Private Function GetColByCallBackInterfaceByFiveArgumentsWithCache(ByVal vstrOperationInterface As String, _
        ByRef rvarArgument1 As Variant, _
        ByRef rvarArgument2 As Variant, _
        ByRef rvarArgument3 As Variant, _
        ByRef rvarArgument4 As Variant, _
        ByRef rvarArgument5 As Variant) As Collection

    Dim objCol As Collection
    
#If PRIOR_TO_EXCEL Then

    Dim objApplication As Excel.Application
#Else
    Dim objApplication As Object    ' 'Excel.Application', 'Word.Application', 'PowerPoint.Application', 'Outlook.Application', ''
#End If

    If mobjCacheApplication Is Nothing Then
    
        Set mobjCacheApplication = GetOfficeApplicationObjectByTwoMethods()
    End If
    
    Set objApplication = mobjCacheApplication

    On Error Resume Next

    If Not objApplication Is Nothing Then
    
        Set objCol = objApplication.Run(vstrOperationInterface, rvarArgument1, rvarArgument2, rvarArgument3, rvarArgument4, rvarArgument5)
    End If

    On Error GoTo 0
    
    Set GetColByCallBackInterfaceByFiveArgumentsWithCache = objCol
End Function


