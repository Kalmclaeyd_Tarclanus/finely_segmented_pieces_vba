Attribute VB_Name = "DataTableListView"
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both MSComctlLib and MSForms
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri,  3/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mintLsvDefaultColumnWidth As Long = 60


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vobjListViews: Input</Argument>
''' <Return>Collection</Return>
Public Function GetAllSelectedTextOfListViews(ByVal vobjListViews As Collection) As Collection

    Dim objCol As Collection, varListView As Variant, objListView As MSComctlLib.ListView
    Dim objListItem As MSComctlLib.ListItem
    
    Set objCol = New Collection
    
    For Each varListView In vobjListViews
    
        Set objListView = varListView
        
        With objListView
    
            For Each objListItem In .ListItems
            
                With objListItem
                
                    If .Selected Then
                    
                        objCol.Add .Text
                    End If
                End With
            Next
        End With
    Next

    Set GetAllSelectedTextOfListViews = objCol
End Function


'''
'''
'''
Public Sub RemoveAllSelectionsOfListViews(ByVal vobjListViews As Collection)

    Dim varListView As Variant, objListView As MSComctlLib.ListView
    Dim objListItem As MSComctlLib.ListItem
    
    For Each varListView In vobjListViews
    
        Set objListView = varListView
        
        With objListView
    
            For Each objListItem In .ListItems
            
                With objListItem
                
                    If .Selected Then
                    
                        .Selected = False
                    End If
                End With
            Next
        End With
    Next
End Sub


'**---------------------------------------------
'** Initialize a ListView for DataTable
'**---------------------------------------------
'''
'''
'''
Public Sub InitalizeListViewForDataTableView(ByRef robjListView As MSComctlLib.ListView)

    With robjListView
    
        .View = lvwReport
        
        .LabelEdit = lvwManual
        
        .Gridlines = True
        .FullRowSelect = True
        .AllowColumnReorder = True
    
        .HideSelection = False
        
        .MultiSelect = True
    End With
End Sub



'**---------------------------------------------
'** Reflesh a single ListView from DataTable collection
'**---------------------------------------------
'''
''' This is only refered on the CodePaneSwitchSingleLsvForm
'''
Public Function GetAllSelectedModuleNameAndFileNameOfListViews(ByVal vobjListView As MSComctlLib.ListView) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    Dim objListItem As MSComctlLib.ListItem
    
    Set objDic = New Scripting.Dictionary
    
    With vobjListView

        For Each objListItem In .ListItems
        
            With objListItem
            
                If .Selected Then
                
                    objDic.Add .Text, .SubItems(3)
                End If
            End With
        Next
    End With

    Set GetAllSelectedModuleNameAndFileNameOfListViews = objDic
End Function


'''
'''
'''
Public Sub RefreshListViewFromDataTableCol(ByRef robjListView As MSComctlLib.ListView, ByVal vobjFieldTitlesCol As Collection, ByVal vobjDTCol As Collection, Optional ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary = Nothing)

    RefreshListViewFieldsForDataTableCol robjListView, vobjFieldTitlesCol, vobjFieldTitleToColumnWidthDic
    
    RefreshListViewRecordsForDataTableCol robjListView, vobjDTCol
End Sub

'''
'''
'''
Public Sub RefreshListViewFieldsForDataTableCol(ByRef robjListView As MSComctlLib.ListView, ByVal vobjFieldTitlesCol As Collection, Optional ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary = Nothing)

    Dim varTitle As Variant, strTitle As String, intColumnWidth As Long

    With robjListView
    
        With .ColumnHeaders
            
            .Clear
        
            For Each varTitle In vobjFieldTitlesCol
            
                strTitle = varTitle
                
                Select Case True
                
                    Case vobjFieldTitleToColumnWidthDic Is Nothing, Not vobjFieldTitleToColumnWidthDic.Exists(strTitle)
                    
                        intColumnWidth = mintLsvDefaultColumnWidth
                    Case Else
                    
                        intColumnWidth = vobjFieldTitleToColumnWidthDic.Item(strTitle)
                End Select
                
                .Add , strTitle, strTitle, intColumnWidth
            Next
        End With
    End With
End Sub

'''
'''
'''
Public Sub RefreshListViewRecordsForDataTableCol(ByRef robjListView As MSComctlLib.ListView, ByVal vobjDTCol As Collection)

    Dim varRowCol As Variant, objRowCol As Collection, varItem As Variant, i As Long

    With robjListView
    
        .ListItems.Clear
    
        For Each varRowCol In vobjDTCol
        
            Set objRowCol = varRowCol
        
            With .ListItems.Add()
            
                i = 0
                For Each varItem In objRowCol
                    
                    If i = 0 Then
                    
                        .Text = CStr(varItem)
                    Else
                    
                        .SubItems(i) = CStr(varItem)
                    End If
                    
                    i = i + 1
                Next
            End With
        Next
    End With
End Sub


'**---------------------------------------------
'** Get selected item information from a ListView
'**---------------------------------------------
'''
'''
'''
Public Function GetSingleDataTableColFromCurrentSelectedListView(ByRef robjListView As MSComctlLib.ListView) As Collection

    Dim objDTCol As Collection, objRowCol As Collection
    Dim intColumnMax As Long, j As Long
   
   
    Set objDTCol = New Collection

    With robjListView
    
        intColumnMax = .ColumnHeaders.Count
    
        With .SelectedItem
        
            Set objRowCol = New Collection
        
            objRowCol.Add .Text
            
            If intColumnMax > 1 Then
            
                For j = 1 To intColumnMax - 1
            
                    objRowCol.Add CStr(.SubItems(j))
                Next
            End If
            
            objDTCol.Add objRowCol
        End With
    End With

    Set GetSingleDataTableColFromCurrentSelectedListView = objDTCol
End Function
'''
'''
'''
Public Function GetDataTableColFromSelectedListView(ByRef robjListView As MSComctlLib.ListView) As Collection

    Dim objDTCol As Collection, objRowCol As Collection
    Dim objListItem As MSComctlLib.ListItem
    Dim intColumnMax As Long, j As Long
   
   
    Set objDTCol = New Collection

    With robjListView
    
        intColumnMax = .ColumnHeaders.Count
    
        For Each objListItem In .ListItems
    
            With objListItem
            
                If .Selected Then
                
                    Set objRowCol = New Collection
                
                    objRowCol.Add .Text
                    
                    If intColumnMax > 1 Then
                    
                        For j = 1 To intColumnMax - 1
                    
                            objRowCol.Add CStr(.SubItems(j))
                        Next
                    End If
                    
                    objDTCol.Add objRowCol
                End If
            End With
        Next
    End With

    Set GetDataTableColFromSelectedListView = objDTCol
End Function

