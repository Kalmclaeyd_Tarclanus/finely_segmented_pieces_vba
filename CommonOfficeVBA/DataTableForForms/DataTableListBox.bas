Attribute VB_Name = "DataTableListBox"
'
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on MSForms
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri,  3/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetAllSelectedTextOfListBoxes(ByVal vobjListBoxes As Collection) As Collection

    Dim varListBox As Variant, objListBox As MSForms.ListBox, objCol As Collection
    Dim i As Long
    
    Set objCol = New Collection
    
    For Each varListBox In vobjListBoxes

        Set objListBox = varListBox
        
        With objListBox
        
            For i = 0 To .ListCount - 1
            
                If .Selected(i) Then
        
                    objCol.Add .List(i)
                End If
            Next
        End With
    Next

    Set GetAllSelectedTextOfListBoxes = objCol
End Function


'''
'''
'''
Public Sub RemoveAllSelectionsOfListBoxes(ByVal vobjListBoxes As Collection)

    Dim varListBox As Variant, objListBox As MSForms.ListBox
    Dim i As Long
    
    For Each varListBox In vobjListBoxes

        Set objListBox = varListBox
        
        With objListBox
        
            For i = 0 To .ListCount - 1
            
                If .Selected(i) Then
        
                    .Selected(i) = False
                End If
            Next
        End With
    Next
End Sub


'**---------------------------------------------
'** Reflesh a ListBox from DataTable collection
'**---------------------------------------------
'''
'''
'''
Public Sub RefreshListBoxFromDataTableCol(ByRef robjListBox As MSForms.ListBox, ByVal vobjDTCol As Collection)

    Dim varRowCol As Variant, objRowCol As Collection, varItem As Variant, i As Long
    
    With robjListBox
    
        .Clear
    
        For Each varRowCol In vobjDTCol
        
            Set objRowCol = varRowCol
            
            .AddItem GetListBoxItemTextFromRowCol(objRowCol)
        Next
    End With
End Sub

'**---------------------------------------------
'** Get selected item information from a ListBox
'**---------------------------------------------
'''
'''
'''
Public Function GetDataTableColFromSelectedListBox(ByRef robjListBox As MSForms.ListBox) As Collection

    Dim objDTCol As Collection, objRowCol As Collection
    Dim i As Long
   
   
    Set objDTCol = New Collection

    With robjListBox
    
        For i = 0 To .ListCount - 1
        
            If .Selected(i) Then
            
                Set objRowCol = GetRowColFromListBoxItemText(.List(i))
            
                objDTCol.Add objRowCol
            End If
        Next
    
    End With

    Set GetDataTableColFromSelectedListBox = objDTCol
End Function




'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function GetListBoxItemTextFromRowCol(ByVal vobjRowCol As Collection) As String

    Dim strText As String, i As Long, varItem As Variant, strItem As String
    
    strText = ""
    
    i = 1
    For Each varItem In vobjRowCol
    
        strItem = varItem
        
        If i = 1 Then
        
            strText = strItem
        Else
        
            If i = 2 Then
            
                strText = strText & " ("
            End If
            
            strText = strText & strItem
            
            If i < vobjRowCol.Count Then
            
                strText = strText & ", "
            Else
                strText = strText & ")"
            End If
        End If
    
        i = i + 1
    Next
    
    GetListBoxItemTextFromRowCol = strText
End Function

'''
'''
'''
Private Function GetRowColFromListBoxItemText(ByVal vstrText As String) As Collection

    Dim objRowCol As Collection, intLeftParen As Long, intRightParen As Long, strLeftText As String, strRightText As String
    Dim varItem As Variant
    
    Set objRowCol = New Collection
    
    intLeftParen = InStr(1, vstrText, "(")
    
    If intLeftParen > 0 Then
    
        intRightParen = InStrRev(vstrText, ")")
        
        strLeftText = Left(vstrText, intLeftParen - 1)
        
        strRightText = Mid(vstrText, intLeftParen + 1, intRightParen - intLeftParen - 1)
        
        objRowCol.Add Trim(strLeftText)
        
        For Each varItem In Split(strRightText, ",")
        
            objRowCol.Add Trim(varItem)
        Next
    Else
    
        objRowCol.Add vstrText
    End If

    Set GetRowColFromListBoxItemText = objRowCol
End Function


