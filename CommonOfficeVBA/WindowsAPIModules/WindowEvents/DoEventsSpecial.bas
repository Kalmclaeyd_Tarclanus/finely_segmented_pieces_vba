Attribute VB_Name = "DoEventsSpecial"
'
'   Try to replicate VBA.Interaction.DoEvents() using the Windows API
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 31/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Windows API Declarations
'///////////////////////////////////////////////
Private Type POINTAPI
    x As Long
    y As Long
End Type

#If VBA7 Then
    Private Declare PtrSafe Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As LongPtr
    
    Private Declare PtrSafe Function PeekMessage Lib "user32" Alias "PeekMessageA" (ByRef lpMsg As Msg, ByVal hwnd As LongPtr, ByVal wMsgFilterMin As Long, ByVal wMsgFilterMax As Long, ByVal wRemoveMsg As Long) As Long
    Private Declare PtrSafe Function TranslateMessage Lib "user32" (ByRef lpMsg As Msg) As LongPtr
    Private Declare PtrSafe Function DispatchMessage Lib "user32" Alias "DispatchMessageA" (ByRef lpMsg As Msg) As LongPtr
    
    Private Declare PtrSafe Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)
    
    Private Declare PtrSafe Function GetInputState Lib "user32" () As Boolean
    
    Private Type Msg
    
        hwnd As LongPtr
        message As Long
        wParam As LongPtr
        lParam As LongPtr
        time As Long
        pt As POINTAPI
    End Type
    
#Else
    Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
    
    Private Declare Function PeekMessage Lib "user32" Alias "PeekMessageA" (ByRef lpMsg As Msg, ByVal hWnd As Long, ByVal wMsgFilterMin As Long, ByVal wMsgFilterMax As Long, ByVal wRemoveMsg As Long) As Long
    Private Declare Function TranslateMessage Lib "user32" (ByRef lpMsg As Msg) As Long
    Private Declare Function DispatchMessage Lib "user32" Alias "DispatchMessageA" (ByRef lpMsg As Msg) As Long
    
    Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)
    
    Private Declare Function GetInputState Lib "user32" () As Boolean
    
    Private Type Msg
    
        hwnd As Long
        message As Long
        wParam As Long
        lParam As Long
        time As Long
        pt As POINTAPI
    End Type
#End If


Private Enum PeekMsgOption

    PM_NOREMOVE = &H0   ' Messages are not removed from the queue after processing by PeekMessage.
    
    PM_REMOVE = &H1     ' Messages are removed from the queue after processing by PeekMessage.
    
    PM_NOYIELD = &H2    ' Prevents the system from releasing any thread that is waiting for the caller to go idle
End Enum

Private Const WM_KEYFIRST = &H100

Private Const WM_KEYLAST = &H108

Private Const WM_PAINT = &HF

Private Const WM_QUIT = &H12


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' Force to paint the specified UserForm object by the PeekMessage and the DispatchMessage of Windows API
'''
Public Sub DoWindowPaintEvents(ByRef robjUserDefinedForm As Object)

    Dim udtMsg As Msg
    
#If VBA7 Then
    Dim intWindowHandle As LongPtr
#Else
    Dim intWindowHandle As Long
#End If

    On Error Resume Next
    
    intWindowHandle = FindWindow(vbNullString, robjUserDefinedForm.Caption)

    On Error GoTo 0

    If intWindowHandle <> 0 Then
    
        While PeekMessage(udtMsg, intWindowHandle, WM_PAINT, WM_PAINT, PeekMsgOption.PM_REMOVE)
            
            TranslateMessage udtMsg
            
            DispatchMessage udtMsg
            
            Debug.Print "Executed DispatchMessage by the general Object reference"
        Wend
    End If
End Sub


Public Sub DoWindowPaintEventsOld(ByRef robjUserDefinedForm As Object)

    Dim udtMsg As Msg
    
#If VBA7 Then
    Dim intWindowHandle As LongPtr
#Else
    Dim intWindowHandle As Long
#End If

    On Error Resume Next
    
    intWindowHandle = FindWindow(vbNullString, robjUserDefinedForm.Caption)

    On Error GoTo 0

    If intWindowHandle <> 0 Then
    
        If PeekMessage(udtMsg, intWindowHandle, WM_PAINT, WM_PAINT, PeekMsgOption.PM_REMOVE) Then
            
            TranslateMessage udtMsg
            
            DispatchMessage udtMsg
            
            Debug.Print "Executed DispatchMessage by the general Object reference"
        End If
    End If
End Sub

'''
''' Replicate the VBA.Interaction.DoEvents using Windows API from https://www.activebasic.com/forum/viewtopic.php?t=426
'''
''' testing ... not-reccomended
'''
Private Function DoEventsByWindowsAPI() As Long
    
    Dim udtMsg As Msg
    
    While PeekMessage(udtMsg, Null, 0, 0, PM_REMOVE)
        
        Select Case udtMsg.message
            
            Case WM_QUIT
                
                ' ExitProcess 0
            
            Case Else
                
                TranslateMessage udtMsg
                
                DispatchMessage udtMsg
        End Select
        
        Sleep 0  ' This may be a waste code in order to affect other threads.
    Wend
End Function

'''
'''
'''
#If VBA7 Then
Private Function GetWindowHandleFromSampleForm(ByRef robjForm As LogTextForm) As LongPtr

    Dim intWindowHandle As LongPtr
#Else
Private Function GetWindowHandleFromSampleForm(ByRef robjForm As LogTextForm) As Long
    
    Dim intWindowHandle As Long
#End If

    intWindowHandle = FindWindow(vbNullString, robjForm.Caption)

    GetWindowHandleFromSampleForm = intWindowHandle
End Function

'''
'''
'''
#If VBA7 Then
Private Function GetWindowHandleFromUserForm(ByRef robjForm As UserForm) As LongPtr
    
    Dim intWindowHandle As LongPtr
#Else
Private Function GetWindowHandleFromUserForm(ByRef robjForm As UserForm) As Long
    
    Dim intWindowHandle As Long
#End If

    intWindowHandle = FindWindow(vbNullString, robjForm.Caption)

    GetWindowHandleFromUserForm = intWindowHandle
End Function

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' second test
'''
Private Sub msubSanityTestToPeekMessageBySomeMethod()

    Dim objForm As LogTextForm, blnUseGetInputStateAPI As Boolean
    
    Set objForm = New LogTextForm
    
    objForm.Caption = "Sample 01"
    
    objForm.Text = ""

    objForm.Show vbModeless


    blnUseGetInputStateAPI = False

    With objForm
    
        ' Prevent user from pressing the button by disabling the parent form
        .Enabled = False
        
        ' something processes
        Dim i As Integer
        
        For i = 0 To 12
            
            '-- Start: something processes
            objForm.Caption = "Sample form - Current time is " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
            
            With objForm.txtLogText
            
                .Text = .Text & vbNewLine & "Count : " & CStr(i)
            End With
            '-- End: something processes
            
            Sleep 100
            
            ' Using Windows 32 API, execute only WM_PAINT message
            ' If you comment the next line out, the opened text-box will be not updated
            
            If blnUseGetInputStateAPI Then
            
                If GetInputState() Then VBA.Interaction.DoEvents
            Else
                DoWindowPaintEvents objForm
            End If
        Next
        
        ' change the form state into enable
        .Enabled = True
    End With

    Unload objForm

End Sub



'''
''' test of getting Window handle of SampleForm user-form
'''
Private Sub msubSanityTestToFindWindowHandle()

    Dim objForm As LogTextForm
    
#If VBA7 Then
    Dim intWindowHandle As LongPtr
#Else
    Dim intWindowHandle As Long
#End If
    
    Set objForm = New LogTextForm
    
    objForm.Caption = "Sample 01"

    objForm.Show vbModeless

    'intWindowHandle = FindWindow(vbNullString, objForm.Caption)
    intWindowHandle = GetWindowHandleFromSampleForm(objForm)
    
    Debug.Print "Sample Form WindowHandle: " & intWindowHandle
    
    intWindowHandle = GetWindowHandleFromSampleForm(objForm)
    
    Debug.Print "Sample Form WindowHandle Second: " & intWindowHandle
    
    intWindowHandle = GetWindowHandleFromUserForm(objForm)

    Debug.Print "Sample Form WindowHandle As UserForm: " & intWindowHandle

    Unload objForm
End Sub
