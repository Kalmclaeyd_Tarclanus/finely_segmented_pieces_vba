Attribute VB_Name = "TextFileCodePagesConversion"
'
'   Tools for coverting code-texts
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADODB.Stream in ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 30/Nov/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = True


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub WriteStringToTextFileWithUTF8(ByRef rstrText As String, ByRef rstrTextFilePath As String)


    WriteStringToTextFileWithSpecifyingCharSet rstrText, rstrTextFilePath, "UTF-8"
End Sub

'''
'''
'''
Public Sub WriteStringToTextFileWithSpecifyingCharSet(ByRef rstrText As String, _
        ByRef rstrTextFilePath As String, _
        Optional ByVal vstrTextFileCharactersSet As String = "UTF-8")

#If HAS_REF Then

    With New ADODB.Stream
#Else
    With CreateObject("ADODB.Stream")
#End If
        .Type = adTypeText
        
        .Charset = vstrTextFileCharactersSet
    
        .Open
    
        .WriteText rstrText, adWriteLine
    
        .SaveToFile rstrTextFilePath, adSaveCreateOverWrite
    
        .Close
    End With
End Sub

'''
'''
'''
Public Sub GetTextToArgumentFromSpecifiedCharSetTextFile(ByRef rstrOutputText As String, _
        ByRef rstrTextFilePath As String, _
        Optional ByVal vstrTextFileCharactersSet As String = "UTF-8")

    
#If HAS_REF Then

    With New ADODB.Stream
#Else
    With CreateObject("ADODB.Stream")
#End If
    
        .Type = adTypeText
        
        .Charset = vstrTextFileCharactersSet
    
        .Open
        
        .LoadFromFile rstrTextFilePath
        
        rstrOutputText = .ReadText()
    
        .Close
    End With
End Sub

'''
'''
'''
Public Function GetTextFromSpecifiedCharSetTextFile(ByRef rstrTextFilePath As String, _
        Optional ByVal vstrTextFileCharactersSet As String = "UTF-8") As String
        
    GetTextToArgumentFromSpecifiedCharSetTextFile GetTextFromSpecifiedCharSetTextFile, rstrTextFilePath, vstrTextFileCharactersSet
End Function


'''
'''
'''
Public Sub ConvertUTF8TextFileToShiftJisTextFile(ByRef rstrInputUTF8TextFilePath As String, _
        ByRef rstrOutputShiftJisTextFilePath As String)


    ConvertACharSetTextFileToAnotherCharSetTextFile rstrInputUTF8TextFilePath, rstrOutputShiftJisTextFilePath, "UTF-8", "Shift-JIS"
End Sub



'''
'''
'''
Public Sub ConvertACharSetTextFileToAnotherCharSetTextFile(ByRef rstrInputACharSetTextFilePath As String, _
        ByRef rstrOutputAnotherCharSetTextFilePath As String, _
        Optional ByVal vstrInputTextCharset As String = "UTF-8", _
        Optional ByVal vstrOutputTextCharset As String = "Shift-JIS")

    Dim strReadText As String

#If HAS_REF Then

    With New ADODB.Stream
#Else
    With CreateObject("ADODB.Stream")
#End If
    
        .Type = adTypeText
    
        .Charset = vstrInputTextCharset
    
        .Open
    
        .LoadFromFile rstrInputACharSetTextFilePath
        
        strReadText = .ReadText()
    
#If HAS_REF Then

        With New ADODB.Stream
#Else
        With CreateObject("ADODB.Stream")
#End If
        
            .Type = adTypeText
        
            .Charset = vstrOutputTextCharset
        
            .Open
        
            .WriteText strReadText, adWriteLine
        
            .SaveToFile rstrOutputAnotherCharSetTextFilePath, adSaveCreateOverWrite
            
            .Close
        End With
    
        .Close
    End With
End Sub



