Attribute VB_Name = "ExternalTextEditors"
'
'   Open text the local default editor
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 27/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleKey As String = "ExternalTextEditors"   ' 1st part of registry sub-key


Private Const mstrSubjectSakuraEditorPathKey As String = "SakuraEditorPath"   ' 2nd part of registry sub-key

Private Const mstrSubjectVSCodePathKey As String = "VSCodeEditorPath"   ' 2nd part of registry sub-key for the Microsoft Visual Studio Code

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Public Enum SelectedTextEditorType

    WindowsNotepad = 0  ' default Notepad.exe
    
    SakuraEditor = 1    ' Sakura editor
    
    VSCodeEditor = 2    ' Visual Studio Code
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open a text-editor after the opened same name file is closed whenever it has been already opened
'**---------------------------------------------
'''
'''
'''
''' <Argument>vstrTextFilePath: Input</Argument>
''' <Argument>vblnReadOnly: Input</Argument>
''' <Argument>vblnAllowToCloseEditorWhenTheFileOpened: Input</Argument>
Public Sub OpenTextBySomeSelectedTextEditorFromWshShell(ByVal vstrTextFilePath As String, _
        Optional ByVal vblnReadOnly As Boolean = False, _
        Optional ByVal vblnAllowToCloseEditorWhenTheFileOpened As Boolean = False)
    
    Dim objFS As Scripting.FileSystemObject
    
    If vblnAllowToCloseEditorWhenTheFileOpened Then
        
        CloseTextEditorWhenTheFileOpenedInAutoSelectedEditorType vstrTextFilePath
    End If
    
    With New WshShell
    
        .Run GetCommandStringByOpeningSakuraEditorOrOtherEditor(vstrTextFilePath, vblnReadOnly), 1
    End With
End Sub

'''
'''
'''
Public Sub CloseTextEditorWhenTheFileOpenedInAutoSelectedEditorType(ByVal vstrTextFilePath As String)

    Select Case GetEditorTypeWhenTryToOpenSakuraEditor()
    
        Case SelectedTextEditorType.SakuraEditor
    
            CloseSakuraEditorWhenTheFileIsOpened GetParentDirectoryPathByVbaDir(vstrTextFilePath)
            
        Case SelectedTextEditorType.WindowsNotepad
            
            CloseWindowsNotepadWhenTheFileIsOpened GetParentDirectoryPathByVbaDir(vstrTextFilePath)
    End Select
End Sub


'**---------------------------------------------
'** Get shell command string
'**---------------------------------------------
'''
'''
'''
Public Function GetCommandStringByOpeningSakuraEditorOrOtherEditor(ByVal vstrFilePath As String, Optional ByVal vblnReadOnly As Boolean = False) As String
    
    Dim strEditorPath As String, strCmd As String
    
    strEditorPath = GetSakuraEditorPathOrSomeEditorPath()
    
    strCmd = """" & strEditorPath & """ """ & vstrFilePath & """"

    If vblnReadOnly Then
    
        If InStr(1, LCase(strEditorPath), "sakura") > 0 Then
            
            strCmd = strCmd & " -R"
        End If
    End If

    GetCommandStringByOpeningSakuraEditorOrOtherEditor = strCmd
End Function


'**---------------------------------------------
'** About outside editor paths
'**---------------------------------------------
'''
'''
'''
Public Function GetEditorTypeWhenTryToOpenSakuraEditor() As SelectedTextEditorType
    
    Dim strEditorPath As String, enmTextEditorType As SelectedTextEditorType

    
    strEditorPath = mfstrGetSakuraEditorPath()
    
    If strEditorPath = "" Then
        ' try to search other editor...
        
        strEditorPath = mfstrGetVisualStudioCodeEditorPath()
        
        If strEditorPath <> "" Then
        
            enmTextEditorType = VSCodeEditor
        Else
            ' decide to open text-file by notepad
            strEditorPath = GetLocalPCNotepadPath()
            
            enmTextEditorType = WindowsNotepad
        End If
    Else
        enmTextEditorType = SakuraEditor
    End If
    
    GetEditorTypeWhenTryToOpenSakuraEditor = enmTextEditorType
End Function

'''
'''
'''
Public Function GetLocalPCNotepadPath() As String

    GetLocalPCNotepadPath = "%SystemRoot%\System32\notepad.exe"
End Function

'''
'''
'''
Public Function GetSakuraEditorPathOrSomeEditorPath() As String
    
    Dim strEditorPath As String
    
    ' try to find the path of the 'sakura.exe'
    strEditorPath = mfstrGetSakuraEditorPath()
    
    If strEditorPath = "" Then
    
        ' try to find other editor...
        
        strEditorPath = mfstrGetVisualStudioCodeEditorPath()
        
        If strEditorPath = "" Then
    
            ' open by notepad
            strEditorPath = GetLocalPCNotepadPath()
        End If
    End If

    GetSakuraEditorPathOrSomeEditorPath = strEditorPath
End Function

'''
'''
'''
Public Function GetVisualStudioCodePath() As String
    
    Dim strEditorPath As String
    
    strEditorPath = mfstrGetVisualStudioCodeEditorPath()

    GetVisualStudioCodePath = strEditorPath
End Function

'**---------------------------------------------
'** Close a text-editor by the opened text file name using Windows API
'**---------------------------------------------
'''
''' close sakura.exe
'''
Public Sub CloseSakuraEditorWhenTheFileIsOpened(ByVal vstrFileName As String)

    If vstrFileName <> "" Then
    
        CloseSakuraEditorIfItIsStarted vstrFileName
    End If
End Sub


'''
'''
'''
Public Sub CloseAllSakuraEditorsIfItIsStarted()

    CloseSakuraEditorIfItIsStarted
End Sub


'''
''' close the sakura.exe when the specified file is opended
'''
Private Sub CloseSakuraEditorIfItIsStarted(Optional ByVal vstrFileName As String = "")
    
    Dim strPattern As String
    
    Const strSakuraEditorSuffix As String = " - sakura 2"

    If vstrFileName <> "" Then
    
        strPattern = vstrFileName & strSakuraEditorSuffix
    Else
        strPattern = strSakuraEditorSuffix
    End If

    CloseMatchedWindowTitleStringProcessIfItExists strPattern
End Sub


'''
''' close notepad.exe
'''
Public Sub CloseWindowsNotepadWhenTheFileIsOpened(ByVal vstrFileName As String)

    If vstrFileName <> "" Then
    
        CloseWindowsNotepadIfItIsStarted vstrFileName
    End If
End Sub


'''
'''
'''
Public Sub CloseWindowsNotepadIfItIsStarted(Optional ByVal vstrFileName As String = "")

    msubCloseWindowsNotepadIfItExists vstrFileName
End Sub

'''
'''
'''
Private Sub msubCloseWindowsNotepadIfItExists(Optional ByVal vstrMatchString As String = "")

    Dim varHWnd As Variant, strWindowText As String

    With GetNotepadHwndToWindowCaptionDic()
    
        For Each varHWnd In .Keys
        
            strWindowText = .Item(varHWnd)
            
            If vstrMatchString <> "" Then
            
                If InStr(1, strWindowText, vstrMatchString) > 0 Then
                
                    CloseApplicationProcessFromWindowHandle varHWnd
                End If
            Else
                CloseApplicationProcessFromWindowHandle varHWnd
            End If
        Next
    End With
End Sub

'///////////////////////////////////////////////
'/// Operations - a UNC path portability solution
'///////////////////////////////////////////////
'**---------------------------------------------
'** set user-specified path
'**---------------------------------------------
'''
''' Individual-Set-Registy for the Sakura editor
'''
Public Sub ISRInExternalTextEditorsOfSakuraEditorPath(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectSakuraEditorPathKey, vstrTargetPath
End Sub
Public Sub IDelRInExternalTextEditorsOfSakuraEditorPath()

    DeleteUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectSakuraEditorPathKey
End Sub

'''
''' Individual-Set-Registy for the Microsoft Visual Studio Code editor
'''
Public Sub ISRInExternalTextEditorsOfVSCodeEditorPath(ByVal vstrTargetPath As String)

    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectVSCodePathKey, vstrTargetPath
End Sub
Public Sub IDelRInExternalTextEditorsOfVSCodeEditorPath()

    DeleteUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectVSCodePathKey
End Sub


'**---------------------------------------------
'** solute path generaly
'**---------------------------------------------
'''
''' solute path from registry CURRENT_USER
'''
Public Function mfstrGetSakuraEditorPath() As String

    Dim strTargetPath As String, strModuleNameAndSubjectKey As String, intStopSearchingPath As Long, objSubPaths As Collection
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectSakuraEditorPathKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)
    
    intStopSearchingPath = GetInt32StopSearchingPathToCurUserReg(strModuleNameAndSubjectKey)
    
    If strTargetPath = "" And intStopSearchingPath <= 0 Then
    
        ' start to search for typical paths if it has been installed
        
        Set objSubPaths = New Collection
        
        With objSubPaths
        
            .Add ":\Program Files (x86)\sakura\sakura.exe"
            
            .Add ":\Program Files\sakura\sakura.exe"
        End With
        
        strTargetPath = SearchSpecifiedFileFromSpecifiedSubPathsAndSetRegistryIfFound(strModuleNameAndSubjectKey, objSubPaths)
        
        If strTargetPath = "" Then
        
            ' stop search
            
            SetInt32StopSearchingPathToCurUserReg strModuleNameAndSubjectKey, 1
        End If
    End If

    If FeatureOfSakuraEditorDisabling Then
    
        strTargetPath = ""
    End If
    
    mfstrGetSakuraEditorPath = strTargetPath
End Function


'''
''' solute path from registry CURRENT_USER
'''
Public Function mfstrGetVisualStudioCodeEditorPath() As String

    ' Const mstrVSCodePath As String = """D:\Program Files\Microsoft VS Code\Code.exe"""

    Dim strTargetPath As String, strModuleNameAndSubjectKey As String, intStopSearchingPath As Long, strPath As String
    Dim objSubPaths As Collection
    
    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectVSCodePathKey
    
    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)
    
    intStopSearchingPath = GetInt32StopSearchingPathToCurUserReg(strModuleNameAndSubjectKey)
    
    If strTargetPath = "" And intStopSearchingPath <= 0 Then
        
        ' start to search
        
        On Error Resume Next
        
        ' Search the User install path
        
        strPath = GetWindowsLocalUserAppDataDirectoryPath() & "\Local\Programs\Microsoft VS Code\Code.exe"
        
        On Error GoTo 0
        
        With CreateObject("Scripting.FileSystemObject")
        
            If .FileExists(strPath) Then
            
                strTargetPath = strPath
            End If
        End With
        
        
        If strTargetPath = "" Then
        
            ' Search the System install path
            
            Set objSubPaths = New Collection
            
            With objSubPaths
            
                .Add ":\Program Files (x86)\Microsoft VS Code\Code.exe"
                
                .Add ":\Program Files\Microsoft VS Code\Code.exe"
            End With
            
            strTargetPath = SearchSpecifiedFileFromSpecifiedSubPathsAndSetRegistryIfFound(strModuleNameAndSubjectKey, objSubPaths)
            
            If strTargetPath = "" Then
            
                ' stop search
                SetInt32StopSearchingPathToCurUserReg strModuleNameAndSubjectKey, 1
            End If
        Else
            SetUNCPathToCurUserReg strModuleNameAndSubjectKey, strTargetPath
        End If
    End If
    
    If FeatureOfVSCodeDisabling Then
    
        strTargetPath = ""
    End If
    
    mfstrGetVisualStudioCodeEditorPath = strTargetPath
End Function


'''
'''
'''
Private Function GetWindowsLocalUserAppDataDirectoryPath() As String
    
    With CreateObject("WScript.Shell") ' Type is 'IWshRuntimeLibrary.WshShell'
    
        GetWindowsLocalUserAppDataDirectoryPath = Replace(.SpecialFolders("AppData"), "\Roaming", "")
    End With
End Function

'''
'''
'''
Public Sub OpenNewSakuraEditor()

    Dim intProcessID As Long

    intProcessID = CLng(VBA.Shell(mfstrGetSakuraEditorPath(), vbNormalFocus))
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Sanity test to solute path
'**---------------------------------------------
'''
''' test of mfstrGetSakuraEditorPath()
'''
Private Sub msubSanityTestToGetSakuraEditorPath()

    Dim strPath As String
    
    strPath = mfstrGetSakuraEditorPath()
End Sub


Private Sub msubSanityTestToGetVisualStudioCodeEditorPath()

    Dim strPath As String
    
    strPath = mfstrGetVisualStudioCodeEditorPath()

    Debug.Print strPath
End Sub


'**---------------------------------------------
'** Close external text-editor tests
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCloseWindowsNotepadIfItIsStarted()

    CloseWindowsNotepadIfItIsStarted ""
End Sub


