Attribute VB_Name = "DataTableStringIn"
'
'   get numbers data-table Collection simply
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu, 25/Jul/2024    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit



'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** About two-dimensional Collection
'**---------------------------------------------
'''
'''
'''
Public Function GetNumberDTColByParsingSimply(ByVal vstrParensNumbersDelimitedComma As String) As Collection

    Dim strTwoNumbersArray() As String, i As Long, j As Long
    
    Dim objDTCol As Collection, objRowCol As Collection, strTmp As String, strNumbers() As String, varANumber As Variant
    
    
    Set objDTCol = New Collection
    
    strTwoNumbersArray = Split(vstrParensNumbersDelimitedComma, ")")
    
    For i = LBound(strTwoNumbersArray) To UBound(strTwoNumbersArray)
    
        strTmp = Right(strTwoNumbersArray(i), Len(strTwoNumbersArray(i)) - InStr(1, strTwoNumbersArray(i), "("))
    
        If strTmp <> "" Then
        
            strNumbers = Split(strTmp, ",")
            
            Set objRowCol = New Collection
            
            For j = LBound(strNumbers) To UBound(strNumbers)
            
                varANumber = Trim(strNumbers(j))
            
                If IsNumeric(varANumber) Then
                
                    If InStr(1, varANumber, ".") > 0 Then
                    
                        varANumber = CDbl(varANumber)
                    Else
                        varANumber = CLng(varANumber)
                    End If
                Else
                    Debug.Print "!!! This [" & varANumber & "] is not number string."
                    
                    varANumber = -1
                End If
                
                objRowCol.Add varANumber
            Next
        End If
        
        objDTCol.Add objRowCol
    Next
    
    Set GetNumberDTColByParsingSimply = objDTCol
End Function


'**---------------------------------------------
'** About one-dimensional Array
'**---------------------------------------------
'''
'''
'''
''' <Argument>rintArray: Output</Argument>
''' <Argument>vstrIntegerValuesDelimitedComma: Input</Argument>
Public Sub GetIntegerArrayFromDelimitedCommaString(ByRef rintArray() As Long, _
        ByVal vstrIntegerValuesDelimitedComma As String)

    Dim strIntegersArray() As String, i As Long
    
    strIntegersArray = Split(vstrIntegerValuesDelimitedComma, ",")
    
    ReDim rintArray(LBound(strIntegersArray) To UBound(strIntegersArray))
    
    For i = LBound(strIntegersArray) To UBound(strIntegersArray)
    
        rintArray(i) = CLng(strIntegersArray(i))
    Next
End Sub

'**---------------------------------------------
'** About one-dimensional Collection
'**---------------------------------------------
'''
''' it is useful when the field-titles are generated with more than one pattern
'''
Public Function GetFieldTitleIndexFromCol(ByVal vstrTargetFieldTitle As String, _
        ByVal vobjFieldTitles As Collection) As Long
    
    Dim varTitle As Variant, strTitle As String, i As Long
    
    i = 1
    
    For Each varTitle In vobjFieldTitles
    
        strTitle = varTitle
        
        If StrComp(strTitle, vstrTargetFieldTitle) = 0 Then
        
            Exit For
        End If
        
        i = i + 1
    Next
    
    GetFieldTitleIndexFromCol = i
End Function

