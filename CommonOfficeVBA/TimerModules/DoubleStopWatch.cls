VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DoubleStopWatch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Stop-watch both Date type and mili-time integer value
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 27/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mdblThresholdOfSecond As Double = 60#

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
#If VBA7 Then
    ' For 64 bit version windows, declare timeGetTime
    Private Declare PtrSafe Function timeGetTime Lib "winmm.dll" () As Long
#Else
    ' For 32 bit version windows, declare timeGetTime
    Private Declare Function timeGetTime Lib "winmm.dll" () As Long
#End If


Private mintMilitime1 As Long   ' mili-second
Private mintMilitime2 As Long   ' mili-second

Private mudtDate1 As Date: Private mudtDate2 As Date

Private mblnIsUsedMilisecondMeasuredTime As Boolean

Private mblnIsMeasured As Boolean

'///////////////////////////////////////////////
'/// Event Handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    mblnIsMeasured = False
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
Public Property Get IsUsedMilisecondMeasuredTime() As Boolean

    IsUsedMilisecondMeasuredTime = mblnIsUsedMilisecondMeasuredTime
End Property

Public Property Get DateOfMeasureStart() As Date

    DateOfMeasureStart = mudtDate1
End Property

Public Property Get ElapsedSecondTime() As Double

    Dim dblElapsedTime As Double
    
    dblElapsedTime = -1
    
    If mblnIsMeasured Then
        
        If mblnIsUsedMilisecondMeasuredTime Then
            
            dblElapsedTime = CDbl((mintMilitime2 - mintMilitime1) / 1000#)
        Else
            dblElapsedTime = CDbl(DateDiff("s", mudtDate1, mudtDate2))
        End If
    End If

    ElapsedSecondTime = dblElapsedTime
End Property


Public Property Get ElapsedTimeByString() As String

    Dim strElapsedTime As String
    
    strElapsedTime = "Not measured yet"
    
    If mblnIsMeasured Then

        strElapsedTime = mfstrGetElapsedTimeByString()
    End If
    
    ElapsedTimeByString = strElapsedTime
End Property


'///////////////////////////////////////////////
'/// Operation
'///////////////////////////////////////////////
Public Sub MeasureStart()

    mintMilitime1 = timeGetTime
    
    mudtDate1 = Now()
End Sub

Public Sub MeasureInterval()

    mintMilitime2 = timeGetTime
    
    mudtDate2 = Now()

    Dim intDiffSecond As Long
    
    intDiffSecond = DateDiff("s", mudtDate1, mudtDate2)

    mblnIsUsedMilisecondMeasuredTime = CBool(intDiffSecond <= mdblThresholdOfSecond)

    mblnIsMeasured = True
End Sub

'''
'''
'''
Private Function mfstrGetElapsedTimeByString() As String
    
    Dim strElapsedTime As String
    Dim intSecond As Long, intMinute As Long, intHour As Long
    
    strElapsedTime = ""
    
    If mblnIsMeasured Then
    
        If mblnIsUsedMilisecondMeasuredTime Then
        
            strElapsedTime = Format(CDbl((mintMilitime2 - mintMilitime1) / 1000#), "0.000") & " [s]"
        Else
            intSecond = DateDiff("s", mudtDate1, mudtDate2)
            
            strElapsedTime = mfstrConvertDateTimeFormat(intSecond)
        End If
    End If

    mfstrGetElapsedTimeByString = strElapsedTime
End Function





