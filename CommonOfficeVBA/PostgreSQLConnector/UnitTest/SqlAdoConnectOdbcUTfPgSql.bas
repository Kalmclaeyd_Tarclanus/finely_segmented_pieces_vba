Attribute VB_Name = "SqlAdoConnectOdbcUTfPgSql"
'
'   Sanity test to connect a PostgreSQL database using ADO ODBC
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on both PsqlOdbc driver at this Windows and an PostgreSQL database server in either a network somewhere or the local-host
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri,  4/Aug/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
#If Win64 Then

    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
#Else
    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
#End If

Private Const mintPostgreSQLDefaultPortNumber As Long = 5432

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** PgSqlOdbcConnector
'**---------------------------------------------
'''
''' Create an in-line table from the only SELECT sql inside
'''
Private Sub msubSanityTestToConnectByVirtualTablePgSqlOdbcConnector()
    
    Dim strSQL As String

    With New PgSqlOdbcConnector

        ' for default connection setting soon after the PostgreSQL typcal configuration is installed in this local computer.

        .SetODBCParametersWithoutDSN mstrDriverName, "LocalHost", "postgres", "postgres", "postgres"

        .AllowToRecordSQLLog = True

        strSQL = "select 'ABC' as A_Column, 'DEF' as B_Column"

        ' Confirm the Recordset of the virtual-table in the SQL without reading CSV file.

        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
    End With
End Sub

'**---------------------------------------------
'** Test dependent on outside VB module
'**---------------------------------------------
'''
''' table list
'''
Private Sub msubSanityTestToConnectToPgSqlByTableInfo()

    Dim strSQL As String, objConnector As PgSqlOdbcConnector
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
    
    Set objConnector = mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo()
    
    If Not objConnector Is Nothing Then
    
        With objConnector
        
            strSQL = "select schemaname, tablename, tableowner from pg_tables"
        
            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
        
            .CloseConnection
        End With
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If
End Sub


'**---------------------------------------------
'** Simple SQL command tests
'**---------------------------------------------
'''
''' SQL Test for create table
'''
Private Sub msubSanityTestToConnectToPgSqlBySqlCreateTable()

    Dim strSQL As String, objConnector As PgSqlOdbcConnector
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
    
    Set objConnector = mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo()
    
    If Not objConnector Is Nothing Then
    
        With objConnector
    
            strSQL = "create table Test_Table (Col1 integer, Col2 integer, Col3 integer, ColText varchar)"
        
            On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
        
            .CloseConnection
        End With
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If
End Sub

'''
''' SQL Test for drop table
'''
Private Sub msubSanityTestToConnectToPgSqlBySqlDropTable()

    Dim strSQL As String, objConnector As PgSqlOdbcConnector
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
    
    Set objConnector = mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo()
    
    If Not objConnector Is Nothing Then
    
        With objConnector
    
            strSQL = "drop table Test_Table"
        
            On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
        
            .CloseConnection
        End With
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If
End Sub

'''
''' SQL Test for insert
'''
Private Sub msubSanityTestToConnectToPgSqlBySqlInsert()

    Dim strSQL As String, objConnector As PgSqlOdbcConnector
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
    
    Set objConnector = mfobjGetPgSqlOdbcConnectorAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
    
    If Not objConnector Is Nothing Then
    
        With objConnector
    
            strSQL = "insert into Test_Table values (13, 15, 16, 'Type3')"
        
            .ExecuteSQL strSQL
        
            .CloseConnection
            
            strSQL = "select * from Test_Table"
            
            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
        End With
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If
End Sub

'''
''' SQL Test for delete
'''
Private Sub msubSanityTestToConnectToPgSqlBySqlDelete()

    Dim strSQL As String, objConnector As PgSqlOdbcConnector
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
    
    Set objConnector = mfobjGetPgSqlOdbcConnectorAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
    
    If Not objConnector Is Nothing Then
    
        With objConnector
    
            strSQL = "delete from Test_Table where ColText = 'Type2'"
        
            .ExecuteSQL strSQL
        
            .CloseConnection
            
            strSQL = "select * from Test_Table"
            
            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
        End With
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If
End Sub

'''
''' SQL Test for update
'''
Private Sub msubSanityTestToConnectToPgSqlBySqlUpdate()

    Dim strSQL As String, objConnector As PgSqlOdbcConnector
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
    
    Set objConnector = mfobjGetPgSqlOdbcConnectorAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
    
    If Not objConnector Is Nothing Then
    
        With objConnector
    
            strSQL = "update Test_Table set Col1 = 51, Col2 = 52 where Col3 = 9 and ColText = 'Type2'"
        
            .ExecuteSQL strSQL
        
            .CloseConnection
            
            strSQL = "select * from Test_Table"
            
            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
        End With
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetPgSqlOdbcConnectorAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests() As PgSqlOdbcConnector

    Dim strSQL As String, objConnector As PgSqlOdbcConnector
    
    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
    
    Set objConnector = mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo()
    
    If Not objConnector Is Nothing Then
    
        PrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests objConnector
    Else
        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
    End If

    Set mfobjGetPgSqlOdbcConnectorAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests = objConnector
End Function



'''
''' You must prepare the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' public function outside this VB code module before execute this module sanity tests
'''
Private Function mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo() As PgSqlOdbcConnector

    Dim objPgSqlOdbcConnector As PgSqlOdbcConnector
    
    On Error Resume Next
    
    Set objPgSqlOdbcConnector = Application.Run("GetCurrentThisSytemTestingPgSqlOdbcConnector")

    On Error GoTo 0

    Set mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo = objPgSqlOdbcConnector
End Function

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub PrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests(ByRef ritfPgSqlConnector As IADOConnector)

    Dim strSQL As String

    With ritfPgSqlConnector
    
        strSQL = "create table Test_Table (Col1 integer, Col2 integer, Col3 integer, ColText varchar)"
            
        On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
        
        strSQL = "truncate table Test_Table"
    
        .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorByExcelSheetFlag
        
        strSQL = "insert into Test_Table values (1, 2, 3, 'Type1');"
        
        .ExecuteSQL strSQL
        
        strSQL = "insert into Test_Table values (4, 5, 6, 'Type1');"
        
        .ExecuteSQL strSQL
        
        strSQL = "insert into Test_Table values (7, 8, 9, 'Type2');"
        
        .ExecuteSQL strSQL
    
        strSQL = "insert into Test_Table values (10, 11, 12, 'Type2');"
    
        .ExecuteSQL strSQL
        
        .CommitTransaction
    End With
End Sub



