Attribute VB_Name = "ADOConStrToolsForPgSql"
'
'   Generate ADODB connection string to a PostgreSQL data base
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
#If Win64 Then

    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
#Else
    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
#End If

Private Const mintPostgreSQLDefaultPortNumber As Long = 5432

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function GetAdoDbConnectionOdbcDSNLessStringToPostgreSQL(ByVal vstrDriverName As String, _
        ByVal vstrServerHostName As String, _
        ByVal vstrDatabaseName As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String, _
        Optional ByVal vintPortNo As Long = mintPostgreSQLDefaultPortNumber) As String
        
    Dim strConnectionString As String
    
    strConnectionString = "DRIVER=" & vstrDriverName & ";" _
                        & "SERVER=" & vstrServerHostName & ";" _
                        & "DATABASE=" & vstrDatabaseName & ";" _
                        & "UID=" & vstrUID & ";" _
                        & "PWD=" & vstrPWD & ";" _
                        & "Port=" & CStr(vintPortNo) & ";"
                        
    GetAdoDbConnectionOdbcDSNLessStringToPostgreSQL = strConnectionString
End Function
'''
'''
'''
Public Function GetAdoDbConnectionDSNLessStringToPostgreSQLWithFixedDriverName(ByVal vstrServerHostName As String, _
        ByVal vstrDatabaseName As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String, _
        Optional ByVal vintPortNo As Long = mintPostgreSQLDefaultPortNumber) As String
        
                        
    GetAdoDbConnectionDSNLessStringToPostgreSQLWithFixedDriverName = GetAdoDbConnectionOdbcDSNLessStringToPostgreSQL(mstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUID, vstrPWD, vintPortNo)
End Function


'**---------------------------------------------
'** About connecting to PostgreSQL
'**---------------------------------------------
'''
''' DSN less ODBC Oracle DB connection test
'''
''' <Argument>UID: User ID</Argument>
''' <Argument>PWD: Password</Argument>
Public Function DoesODBCPostgreSQLDSNLessSettingExists(ByVal vstrServerHostName As String, _
        ByVal vstrDatabaseName As String, _
        ByVal vstrUID As String, _
        ByVal vstrPWD As String) As Boolean
    
    Dim strConnectionString As String, blnIsConnected As Boolean, strLog As String
    
    strConnectionString = GetAdoDbConnectionDSNLessStringToPostgreSQLWithFixedDriverName(vstrServerHostName, vstrDatabaseName, vstrUID, vstrPWD)
    
    blnIsConnected = IsADOConnectionStringEnabled(strConnectionString)
    
    If blnIsConnected Then
    
        strLog = "Confirmed that the PostgreSQL [" & vstrDatabaseName & "] had been connected." & vbNewLine
        
        strLog = strLog & vbTab & "Server-host: " & vstrServerHostName & ", user name: " & vstrUID
    
        Debug.Print strLog
    Else
        strLog = "Failed to connect the PostgreSQL [" & vstrDatabaseName & "]" & vbNewLine
        
        strLog = strLog & vbTab & "Server-host: " & vstrServerHostName & ", user name: " & vstrUID
        
        Debug.Print strLog
    End If
    
    
    DoesODBCPostgreSQLDSNLessSettingExists = blnIsConnected
End Function

'**---------------------------------------------
'** for control USetAdoOdbcConStrForPgSql user-form
'**---------------------------------------------
'''
'''
'''
Public Function GetInitialUserInputParametersDicForAdoDSNlessConnectionOfPostgreSQL(Optional ByVal vstrDriverName As String = mstrDriverName, _
        Optional ByVal vstrServerHostName As String = "", _
        Optional ByVal vstrDatabaseName As String = "", _
        Optional ByVal vstrUserid As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary
    
    Set objDic = New Scripting.Dictionary

    With objDic
    
        .Add "UseDSN", False
    
    
        .Add "DriverName", vstrDriverName
    
        .Add "ServerHostName", vstrServerHostName
    
        .Add "DataBaseName", vstrDatabaseName
    
        .Add "PortNumber", vintPortNumber
        
    
        .Add "UserName", vstrUserid
    
        .Add mstrTemporaryPdKey, vstrPassword
        
        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOdbcConnectionDestinationRDBType(OdbcConnectionDestinationToPostgreSQL)
    End With

    Set GetInitialUserInputParametersDicForAdoDSNlessConnectionOfPostgreSQL = objDic
End Function

'''
'''
'''
Public Function GetADOConStrOfOdbcPgSqlFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfOdbcPgSql

    Dim objADOConStrOfOdbcPgSql As ADOConStrOfOdbcPgSql
    
    Set objADOConStrOfOdbcPgSql = New ADOConStrOfOdbcPgSql
    
    UpdateADOConStrOfOdbcPgSqlFromInputDic objADOConStrOfOdbcPgSql, robjUserInputParametersDic
    
    Set GetADOConStrOfOdbcPgSqlFromInputDic = objADOConStrOfOdbcPgSql
End Function


'''
'''
'''
Public Sub UpdateADOConStrOfOdbcPgSqlFromInputDic(ByRef robjADOConStrOfOdbcPgSql As ADOConStrOfOdbcPgSql, ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim blnUseDSN As Boolean

    blnUseDSN = robjUserInputParametersDic.Item("UseDSN")
    
    robjADOConStrOfOdbcPgSql.UseDSN = blnUseDSN
    
    If blnUseDSN Then
    
        UpdateADOConStrOfDSNFromInputDic robjADOConStrOfOdbcPgSql.DsnAdoConStrGenerator, robjUserInputParametersDic
    Else
        UpdateADOConStrOfDsnlessPgSqlFromInputDic robjADOConStrOfOdbcPgSql.DsnlessAdoConStrGenerator, robjUserInputParametersDic
    End If
End Sub

'''
'''
'''
Public Sub UpdateADOConStrOfDsnlessPgSqlFromInputDic(ByRef robjADOConStrOfDsnlessPgSql As ADOConStrOfDsnlessPgSql, _
        ByRef robjUserInputParametersDic As Scripting.Dictionary)

    Dim strKey As String, intPortNo As Long
    
    With robjUserInputParametersDic
    
        strKey = "PortNumber"
        
        intPortNo = 0
        
        If .Exists(strKey) Then
        
            intPortNo = .Item(strKey)
        End If
    
        If intPortNo > 0 Then
        
            robjADOConStrOfDsnlessPgSql.SetPostgreSqlOdbcConnectionWithoutDSN .Item("DriverName"), .Item("ServerHostName"), .Item("DataBaseName"), .Item("UserName"), .Item(mstrTemporaryPdKey), intPortNo
        Else
            robjADOConStrOfDsnlessPgSql.SetPostgreSqlOdbcConnectionWithoutDSN .Item("DriverName"), .Item("ServerHostName"), .Item("DataBaseName"), .Item("UserName"), .Item(mstrTemporaryPdKey)
        End If
    End With
End Sub


'''
'''
'''
Public Function GetADOConStrOfDsnlessPgSqlFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfDsnlessPgSql

    Dim objADOConStrOfDsnlessPgSql As ADOConStrOfDsnlessPgSql
    
    Set objADOConStrOfDsnlessPgSql = New ADOConStrOfDsnlessPgSql
    
    UpdateADOConStrOfDsnlessPgSqlFromInputDic objADOConStrOfDsnlessPgSql, robjUserInputParametersDic
    
    Set GetADOConStrOfDsnlessPgSqlFromInputDic = objADOConStrOfDsnlessPgSql
End Function

'''
'''
'''
Public Function GetADOConnectionOdbcPgSqlStringFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As String

    Dim strConnection As String, blnUseDSN As Boolean

    With robjUserInputParametersDic
    
        blnUseDSN = .Item("UseDSN")
        
        If blnUseDSN Then
        
            strConnection = GetADOConStrOfDSNFromInputDic(robjUserInputParametersDic).GetConnectionString()
        Else
            strConnection = GetADOConStrOfDsnlessPgSqlFromInputDic(robjUserInputParametersDic).GetConnectionString()
        End If
    End With

    GetADOConnectionOdbcPgSqlStringFromInputDic = strConnection
End Function


'**---------------------------------------------
'** ADO connection string parameters solution interfaces by each special form
'**---------------------------------------------
'''
'''
'''
''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
''' <Argument>robjADOConStrOfOdbcPgSql: Output</Argument>
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrDatabaseName: Input</Argument>
''' <Argument>vstrUserid: Input</Argument>
''' <Argument>vstrPassword: Input</Argument>
''' <Argument>vintPortNumber: Input</Argument>
''' <Argument>vstrDSN: Input</Argument>
Public Sub SetOdbcConnectionParametersByPostgreSqlForm(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
        ByRef robjADOConStrOfOdbcPgSql As ADOConStrOfOdbcPgSql, _
        ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDriverName As String = mstrDriverName, _
        Optional ByVal vstrServerHostName As String = "", _
        Optional ByVal vstrDatabaseName As String = "", _
        Optional ByVal vstrUserid As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber, _
        Optional ByVal vstrDSN As String = "")

    Dim objUserInputParametersDic As Scripting.Dictionary, strPassword As String, blnIsOpeningFormNeeded As Boolean
        
    rblnIsRequestedToCancelAdoConnection = False
        
    blnIsOpeningFormNeeded = IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfPostgreSql(objUserInputParametersDic, vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber, vstrDSN)

    If blnIsOpeningFormNeeded Then
    
        msubSetUserInputParametersDicOfPostgreSqlByOpeningFormWithFixedSettingKeyName rblnIsRequestedToCancelAdoConnection, objUserInputParametersDic, vstrSettingKeyName
    End If

    If Not rblnIsRequestedToCancelAdoConnection Then
    
        UpdateADOConStrOfOdbcPgSqlFromInputDic robjADOConStrOfOdbcPgSql, objUserInputParametersDic
    End If
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
''' <Argument>robjUserInputParametersDic: Input - Output</Argument>
''' <Argument>rstrSettingKeyName: Input</Argument>
Private Sub msubSetUserInputParametersDicOfPostgreSqlByOpeningFormWithFixedSettingKeyName(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        ByRef rstrSettingKeyName As String)


    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr, objUSetAdoOdbcConStrForPgSql As USetAdoOdbcConStrForPgSql
   
    Set objFormStateToSetAdoConStr = GetFormStateToSetAdoConStrByDefaultParams(robjUserInputParametersDic, rstrSettingKeyName)
   
    Set objUSetAdoOdbcConStrForPgSql = New USetAdoOdbcConStrForPgSql
    
    With objUSetAdoOdbcConStrForPgSql
    
        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
    
        .Show vbModal
    End With

    AfterProcessAdoConStrFormSetting rblnIsRequestedToCancelAdoConnection, robjUserInputParametersDic, objFormStateToSetAdoConStr
End Sub


'''
'''
'''
''' <Argument>robjUserInputParametersDic: Output</Argument>
''' <Argument>vstrSettingKeyName: Input</Argument>
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrDataBaseName: Input</Argument>
''' <Argument>vstrUserid: Input</Argument>
''' <Argument>vstrPassword: Input</Argument>
''' <Argument>vintPortNumber: Input</Argument>
''' <Argument>vstrDSN: Input</Argument>
Private Function IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfPostgreSql(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        ByVal vstrSettingKeyName As String, _
        Optional ByVal vstrDriverName As String = mstrDriverName, _
        Optional ByVal vstrServerHostName As String = "", _
        Optional ByVal vstrDatabaseName As String = "", _
        Optional ByVal vstrUserid As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber, _
        Optional ByVal vstrDSN As String = "") As Boolean

    Dim blnIsOpeningFormNeeded As Boolean
    
    blnIsOpeningFormNeeded = True

    ' Try to get parameters from Win-registry cache
    
    Set robjUserInputParametersDic = GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(vstrSettingKeyName)

    Select Case True
    
        Case robjUserInputParametersDic Is Nothing, robjUserInputParametersDic.Count = 0
    
            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfPostgreSql(robjUserInputParametersDic, vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber, vstrDSN)
        Case Else
            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache(robjUserInputParametersDic)
    End Select

    IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfPostgreSql = blnIsOpeningFormNeeded
End Function


'''
'''
'''
''' <Argument>robjUserInputParametersDic: Output</Argument>
''' <Argument>vstrDriverName: Input</Argument>
''' <Argument>vstrServerHostName: Input</Argument>
''' <Argument>vstrDataBaseName: Input</Argument>
''' <Argument>vstrUserid: Input</Argument>
''' <Argument>vstrPassword: Input</Argument>
''' <Argument>vintPortNumber: Input</Argument>
''' <Argument>vstrDSN: Input</Argument>
Private Function IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfPostgreSql(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
        Optional ByVal vstrDriverName As String = mstrDriverName, _
        Optional ByVal vstrServerHostName As String = "", _
        Optional ByVal vstrDatabaseName As String = "", _
        Optional ByVal vstrUserid As String = "", _
        Optional ByVal vstrPassword As String = "", _
        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber, _
        Optional ByVal vstrDSN As String = "") As Boolean


    Dim blnIsOpeningFormNeeded As Boolean
    
    blnIsOpeningFormNeeded = True
    
    If vstrDSN <> "" Then
    
        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToPostgreSQL, vstrDSN, vstrUserid, vstrPassword)
    Else
        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfPostgreSQL(vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber)
    End If
    
    If vstrPassword <> "" Then
    
        If IsADOConnectionStringEnabled(GetADOConnectionOdbcPgSqlStringFromInputDic(robjUserInputParametersDic)) Then
        
            blnIsOpeningFormNeeded = False
        End If
    End If

    IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfPostgreSql = blnIsOpeningFormNeeded
End Function



'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Object creation tests
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToCreateADOConStrOfOdbcPgSql()

    Dim objADOConStrOfOdbcPgSql As ADOConStrOfOdbcPgSql, objADOConStrOfDsnlessPgSql As ADOConStrOfDsnlessPgSql
    
    Set objADOConStrOfOdbcPgSql = New ADOConStrOfOdbcPgSql
    
    Set objADOConStrOfDsnlessPgSql = New ADOConStrOfDsnlessPgSql
End Sub


'**---------------------------------------------
'** USetAdoOdbcConStrForPgSql form tests
'**---------------------------------------------
'''
''' delete testing SettingKeyName from the registory
'''
Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToPgSql()

    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestPgSqlDSNlessKey"
    
    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestPgSqlDSNKey"
End Sub


'''
'''
'''
Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForPgSqlForDSNless()

    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
 
    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfPostgreSQL()

    msubSetUserInputParametersDicOfPostgreSqlByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestPgSqlDSNlessKey"

    If Not blnIsRequestedToCancelAdoConnection Then
    
        If IsADOConnectionStringEnabled(GetADOConStrOfOdbcPgSqlFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
    
            Debug.Print "Connected to PostgreSQL by ADO"
        End If
    End If
End Sub


'''
''' DSNless PostgreSQL connection
'''
Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForPgSqlForDSNlessWithLongNotations()


    Dim objUSetAdoOdbcConStrForPgSql As USetAdoOdbcConStrForPgSql
    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr

    Set objFormStateToSetAdoConStr = New FormStateToSetAdoConStr
    
    With objFormStateToSetAdoConStr
    
        Set .UserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfPostgreSQL()       ' A document object of a properties form
    
        .SettingKeyName = "TestPgSqlDSNlessKey"
    End With
    
    Set objUSetAdoOdbcConStrForPgSql = New USetAdoOdbcConStrForPgSql
    
    With objUSetAdoOdbcConStrForPgSql
    
        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
    
        .Show vbModal
    End With

    If Not objFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection Then

        msubConnectionTestForOdbcPgSql objFormStateToSetAdoConStr, True
    End If
End Sub


'''
''' DSN connection to PostgreSQL
'''
Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForPgSqlForDSN()

    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
 
    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToPostgreSQL)

    msubSetUserInputParametersDicOfPostgreSqlByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestPgSqlDSNKey"

    If Not blnIsRequestedToCancelAdoConnection Then
    
        If IsADOConnectionStringEnabled(GetADOConStrOfOdbcPgSqlFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
    
            Debug.Print "Connected to PostgreSQL by ADO"
        End If
    End If
End Sub

'''
''' Connection to PostgreSQL with Form
'''
Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForPgSqlFromSettingCache()


    Dim objUSetAdoOdbcConStrForPgSql As USetAdoOdbcConStrForPgSql, objFormStateToSetAdoConStr As FormStateToSetAdoConStr
    

    Set objFormStateToSetAdoConStr = New FormStateToSetAdoConStr
    
    With objFormStateToSetAdoConStr
    
        Set .UserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToPostgreSQL)       ' A document object of a properties form
    
        .SettingKeyName = ""
    End With

    Set objUSetAdoOdbcConStrForPgSql = New USetAdoOdbcConStrForPgSql
    
    With objUSetAdoOdbcConStrForPgSql
    
        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, False
    
        .Show vbModal
    End With

    If Not objFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection Then
    
        msubConnectionTestForOdbcPgSql objFormStateToSetAdoConStr, True
    End If
End Sub



'///////////////////////////////////////////////
'/// Internal functions for sanity tests
'///////////////////////////////////////////////

'''
'''
'''
Private Sub msubConnectionTestForOdbcPgSql(ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr, _
        Optional ByVal vblnAllowToWriteAdoConStrInfoSettingIntoWinRegistry As Boolean = False)

    If IsADOConnectionStringEnabled(GetADOConStrOfOdbcPgSqlFromInputDic(robjFormStateToSetAdoConStr.UserInputParametersDic).GetConnectionString()) Then
    
        Debug.Print "Connected to PostgreSQL by ADO"
    
        If vblnAllowToWriteAdoConStrInfoSettingIntoWinRegistry Then
    
            ' Write setting to Registory
            
            If GetAllowToSaveAdoConStrPasswordInTemporaryCache() Then
            
                UpdateWinRegKeyValuesOfAdoConnectionStringInfoFromAdoConStrFormStateDic robjFormStateToSetAdoConStr
            End If
        End If
    End If
End Sub

