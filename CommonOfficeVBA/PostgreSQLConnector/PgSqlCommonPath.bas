Attribute VB_Name = "PgSqlCommonPath"
'
'   utilities to solute relative ADO PostgreSQL connection
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on PgSqlOdbcConnector.cls
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Redesigned
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleKey As String = "PgSqlCommonPath"   ' 1st part of registry sub-key

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Actual implementation of IOutputBookPath
'**---------------------------------------------
'''
''' solute directory path from registry CURRENT_USER
'''
Public Function GetPgSqlConnectingPrearrangedLogBookPath() As String
    
    GetPgSqlConnectingPrearrangedLogBookPath = GetCurrentBookOutputDir() & "\SQLTests\PgSqlAdoConnectingUnittests.xlsx"
End Function


'**---------------------------------------------
'** PostgreSQL exclusive SQL
'**---------------------------------------------
'''
''' get sheet name and SQL for both the table info and the table columns
'''
''' <Argument>rstrTableInfoSheetName: Input</Argument>
''' <Argument>rstrTableInfoSQL: Input</Argument>
''' <Argument>rstrTableColumnsInfoSheetName: Input</Argument>
''' <Argument>rstrColumnsInfoSQL: Input</Argument>
''' <Argument>vstrDatabaseName: Input</Argument>
''' <Argument>vstrTableName: Input</Argument>
''' <Argument>vstrSchemaName: Input</Argument>
Public Sub GetPgSqlTableInfoSQLsAndSheetNames(ByRef rstrTableInfoSheetName As String, _
        ByRef rstrTableInfoSQL As String, _
        ByRef rstrTableColumnsInfoSheetName As String, _
        ByRef rstrColumnsInfoSQL As String, _
        ByVal vstrDatabaseName As String, _
        ByVal vstrTableName As String, _
        Optional ByVal vstrSchemaName As String = "")

    rstrTableInfoSheetName = vstrTableName & "_Info"

    rstrTableInfoSQL = "select relid AS id, schemaname AS schema_name, relname AS table_name from pg_stat_user_tables where relname = '" & vstrTableName & "' ORDER BY relname"

    rstrTableColumnsInfoSheetName = vstrTableName & "_Columns"

    rstrColumnsInfoSQL = GetSQLForPgSqlColumnsInformation(vstrDatabaseName, vstrTableName, vstrSchemaName)
End Sub


'''
''' get column information getting SQL for PostgreSQL
'''
''' <Argument>vstrDatabaseName: Input</Argument>
''' <Argument>vstrTableName: Input</Argument>
''' <Argument>vstrSchemaName: Input</Argument>
''' <Return>String: SQL</Return>
Public Function GetSQLForPgSqlColumnsInformation(ByVal vstrDatabaseName As String, _
        ByVal vstrTableName As String, _
        Optional ByVal vstrSchemaName As String = "") As String
    
    Dim strSQL As String, strSQLSelect As String
    
    
    strSQLSelect = "ordinal_position as Cln_ID, column_name AS ��, data_type as Data_type, character_maximum_length as size"
    
    If vstrSchemaName = "" Then
        
        strSQL = "select " & strSQLSelect & " from information_schema.columns where table_catalog = '" & vstrDatabaseName & "' and table_name = '" & vstrTableName & "'"
    Else
        strSQL = "select " & strSQLSelect & " from information_schema.columns where table_catalog = '" & vstrDatabaseName & "' and table_schema = '" & vstrSchemaName & "' and table_name = '" & vstrTableName & "'"
    End If

    GetSQLForPgSqlColumnsInformation = strSQL
End Function

