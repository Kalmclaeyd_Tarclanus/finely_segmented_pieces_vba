VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ADOConStrOfDsnlessPgSql"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ADO ODBC connection string generator for PostgreSQL RDB without DSN
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on ADO
'       Dependent on both an installed PostgreSQL and PsqlOdbc driver at this Windows
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator
'       Tue, 28/Nov/2023    Kalmclaeyd Tarclanus    Introduced DbConPgSql.cls
'

Option Explicit

'///////////////////////////////////////////////
'/// Interface implementations
'///////////////////////////////////////////////
Implements IADOConnectStrGenerator

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjDbConPgSql As DbConPgSql

'**---------------------------------------------
'** ODBC DSNless connection
'**---------------------------------------------
Private mstrDriverName As String    ' ODBC driver name string

'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    Set mobjDbConPgSql = New DbConPgSql
End Sub

'///////////////////////////////////////////////
'/// Implemented operations
'///////////////////////////////////////////////
Private Function IADOConnectStrGenerator_GetConnectionString() As String

    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForPostgreSqlODBCWithoutDSN()
End Function
'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
''' read-only User Name - ODBC
'''
Public Property Get UserName() As String

    UserName = mobjDbConPgSql.UserName
End Property

'**---------------------------------------------
'** ODBC DSNless connection
'**---------------------------------------------
'''
''' read-only Driver Name for ODBC
'''
Public Property Get DriverName() As String

    DriverName = mstrDriverName
End Property

'''
''' read-only RDB server host name or IP address
'''
Public Property Get ServerHostName() As String

    ServerHostName = mobjDbConPgSql.ServerHostName
End Property

'''
''' read-only PostgreSQL database name
'''
Public Property Get DataBaseName() As String

    DataBaseName = mobjDbConPgSql.DataBaseName
End Property

'''
''' read-only Port number
'''
Public Property Get PortNo() As Long

    PortNo = mobjDbConPgSql.PortNo
End Property

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' using PostgreSQL ODBC Driver connection string
'''
Public Sub SetPostgreSqlOdbcConnectionWithoutDSN(ByVal vstrDriverName As String, _
        ByVal vstrServerHostName As String, _
        ByVal vstrDatabaseName As String, _
        ByVal vstrUserid As String, _
        ByVal vstrPassword As String, _
        Optional ByVal vintPortNumber As Long = 5432)
    
    mstrDriverName = vstrDriverName ' for PostgreSQL
    
    mobjDbConPgSql.SetPostgreSqlConnectionParameters vstrServerHostName, _
            vstrDatabaseName, _
            vstrUserid, _
            vstrPassword, _
            vintPortNumber
End Sub

'''
'''
'''
Public Function GetConnectionString() As String

    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
End Function

'''
''' For PostgreSQL, DSNless connection, which ConnectionString must request PortNo
'''
Public Function GetConnectionStringForPostgreSqlODBCWithoutDSN() As String

    GetConnectionStringForPostgreSqlODBCWithoutDSN = GetAdoDbConnectionOdbcDSNLessStringToPostgreSQL(mstrDriverName, _
            Me.ServerHostName, _
            Me.DataBaseName, _
            Me.UserName, _
            mobjDbConPgSql.PWD, _
            Me.PortNo)
End Function



