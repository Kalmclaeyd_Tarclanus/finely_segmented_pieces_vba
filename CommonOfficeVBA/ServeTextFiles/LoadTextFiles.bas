Attribute VB_Name = "LoadTextFiles"
'
'   load text file and convert it to some data types
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon,  3/Jul/2023    Kalmclaeyd Tarclanus    Separated from LoadAndModifyBook.bas
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' The text data loading speed is to be prior than the used memory costs
'''
''' <Argument>rstrAllLines: Output all test string</Argument>
''' <Argument>vstrTextFilePath: input file path</Argument>
Public Sub LoadAllTextWithHighSpeed(ByRef rstrAllLines As String, ByVal vstrTextFilePath As String, Optional ByVal vblnNoLockReadOnly As Boolean = False)
    
    Dim intFileNumber As Integer, bytBuf() As Byte
    
    intFileNumber = FreeFile
    
    If vblnNoLockReadOnly Then
        
        Open vstrTextFilePath For Binary Access Read Shared As #intFileNumber
    Else
        Open vstrTextFilePath For Binary As #intFileNumber
    End If
    
    ReDim bytBuf(1 To LOF(intFileNumber))
    
    Get #intFileNumber, , bytBuf
    
    Close #intFileNumber
    
    rstrAllLines = StrConv(bytBuf, vbUnicode)
End Sub

'''
''' The text data loading speed is to be prior than the used memory costs
'''
''' <Argument>rvarDataLines: Output variant array</Argument>
''' <Argument>vstrTextFilePath: input file path</Argument>
Public Sub LoadAllTextLinesArrayWithHighSpeed(ByRef rvarDataLines As Variant, ByVal vstrTextFilePath As String)
    
    Dim strAllLines As String, strOneLine(0) As String
    
    LoadAllTextWithHighSpeed strAllLines, vstrTextFilePath

    GetLinesArrayFromOneStringText rvarDataLines, strAllLines
End Sub

'''
'''
'''
''' <Argument>rvarDataLines: Output variant array</Argument>
''' <Argument>vrstrAllLines: input text string which should has include CrLf</Argument>
Public Sub GetLinesArrayFromOneStringText(ByRef rvarDataLines As Variant, ByRef rstrAllLines As String)

    Dim strOneLine(0) As String

    If InStr(1, rstrAllLines, vbCrLf) > 0 Then
    
        rvarDataLines = Split(rstrAllLines, vbCrLf)
        
    ElseIf InStr(1, rstrAllLines, vbLf) > 0 Then
    
        rvarDataLines = Split(rstrAllLines, vbLf)
        
    ElseIf InStr(1, rstrAllLines, vbCr) > 0 Then
    
        rvarDataLines = Split(rstrAllLines, vbCr)
    Else
        ' seems to be only one line
        strOneLine(0) = rstrAllLines
        
        rvarDataLines = strOneLine
    End If
End Sub

'''
'''
'''
Public Function GetMaximumLineNumberFromRawTextString(ByRef rstrText As String) As Long

    Dim varLinesForReading As Variant

    GetLinesArrayFromOneStringText varLinesForReading, rstrText
    
    GetMaximumLineNumberFromRawTextString = UBound(varLinesForReading) + 1
End Function

'''
''' When you needs remove some condition matched line, the following may be convenient
'''
Public Function GetLinesDicFromOneStringText(ByRef rstrAllLines As String) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary, varDataLines As Variant, i As Long, varLine As Variant
    
    Set objDic = New Scripting.Dictionary
    
    GetLinesArrayFromOneStringText varDataLines, rstrAllLines

    i = 1
    
    For Each varLine In varDataLines
    
        objDic.Add i, varLine
        
        i = i + 1
    Next
    
    Set GetLinesDicFromOneStringText = objDic
End Function


