Attribute VB_Name = "ConvertTextFile"
'
'   simple text file convert
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 15/May/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** simple string modification functions
'**---------------------------------------------
'''
'''
'''
Public Function PadLeft(ByRef rstrValue As String, ByVal vintLength As Long, Optional ByVal vstrSpacingChar As String = " ") As String

    Dim strLine As String
    
    PadLeftToArg strLine, rstrValue, vintLength, vstrSpacingChar

    PadLeft = strLine
End Function

'''
'''
'''
Public Function PadRight(ByRef rstrValue As String, ByVal vintLength As Long, Optional ByVal vstrSpacingChar As String = " ") As String

    Dim strLine As String
    
    PadRightToArg strLine, rstrValue, vintLength, vstrSpacingChar

    PadRight = strLine
End Function

'''
'''
'''
Public Sub PadLeftToArg(ByRef rstrLine As String, ByRef rstrValue As String, ByVal vintLength As Long, Optional ByVal vstrSpacingChar As String = " ")

    Dim intValueLength As String
    
    intValueLength = Len(rstrValue)

    If intValueLength >= vintLength Then
    
        rstrLine = rstrValue
    Else
        If Len(vstrSpacingChar) = 1 Then
        
            rstrLine = VBA.String(vintLength - intValueLength, vstrSpacingChar) & rstrValue
        Else
            rstrLine = rstrValue
            
            Do While Len(rstrLine) < vintLength
            
                rstrLine = vstrSpacingChar & rstrLine
            Loop
        
            If Len(rstrLine) > vintLength Then
            
                rstrLine = Right(rstrLine, vintLength)
            End If
        End If
    End If
End Sub

'''
'''
'''
Public Sub PadRightToArg(ByRef rstrLine As String, ByRef rstrValue As String, ByVal vintLength As Long, Optional ByVal vstrSpacingChar As String = " ")

    Dim intValueLength As String
    
    intValueLength = Len(rstrValue)

    If intValueLength >= vintLength Then
    
        rstrLine = rstrValue
    Else
        If Len(vstrSpacingChar) = 1 Then
        
            rstrLine = rstrValue & VBA.String(vintLength - intValueLength, vstrSpacingChar)
        Else
            rstrLine = rstrValue
            
            Do While Len(rstrLine) < vintLength
            
                rstrLine = rstrLine & vstrSpacingChar
            Loop
        
            If Len(rstrLine) > vintLength Then
            
                rstrLine = Left(rstrLine, vintLength)
            End If
        End If
    End If
End Sub

'**---------------------------------------------
'** simple path-string modification utilities
'**---------------------------------------------
'''
'''
'''
Public Function CutLineHeadSpecifiedLengthCharactersWithFixedSuffix(ByVal vstrInputTextFilePath As String, ByVal vintCutHeadLengthOfChar As Long)

    Dim strSuffix As String, strOutputPath As String
    
    strSuffix = "_Cut" & CStr(vintCutHeadLengthOfChar)
    
    strOutputPath = GetPathWithAddingSuffix(vstrInputTextFilePath, strSuffix)

    CutLineHeadSpecifiedLengthCharacters vstrInputTextFilePath, strOutputPath, vintCutHeadLengthOfChar
End Function


'''
'''
'''
Public Function CutLineHeadSpecifiedLengthCharacters(ByVal vstrInputTextFilePath As String, ByVal vstrOutputTextFilePath As String, ByVal vintCutHeadLengthOfChar As Long)

    Dim varLines As Variant, strOutputText As String, strLine As String
    Dim i As Long

    LoadAllTextLinesArrayWithHighSpeed varLines, vstrInputTextFilePath

    msubGetTextOfLineHeadSpecifiedLengthCharactersFromSourceLineArray strOutputText, varLines, vintCutHeadLengthOfChar

    
    With New Scripting.FileSystemObject
    
        With .CreateTextFile(vstrOutputTextFilePath, True)
    
            .Write strOutputText
            
            .Close
        End With
    End With
End Function


'''
'''
'''
Public Function GetPathWithAddingSuffix(ByVal vstrPath As String, ByVal vstrSuffix As String) As String

    Dim strPath As String

    With New Scripting.FileSystemObject
    
        strPath = .GetParentFolderName(vstrPath) & "\" & .GetBaseName(vstrPath) & vstrSuffix & "." & .GetExtensionName(vstrPath)
    End With
    
    GetPathWithAddingSuffix = strPath
End Function


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubGetTextOfLineHeadSpecifiedLengthCharactersFromSourceLineArray(ByRef rstrOutputText As String, ByRef rvarLines As Variant, ByVal vintCutHeadLengthOfChar As Long)

    Dim strLine As String, i As Long

    rstrOutputText = ""

    For i = LBound(rvarLines) To UBound(rvarLines)
    
        strLine = rvarLines(i)
        
        If strLine <> "" And Len(strLine) > 1 Then
    
            strLine = Right(strLine, Len(strLine) - vintCutHeadLengthOfChar)
        End If
        
        If rstrOutputText <> "" Then
        
            rstrOutputText = rstrOutputText & vbNewLine
        End If
        
        rstrOutputText = rstrOutputText & strLine
    Next
End Sub

