Attribute VB_Name = "UsingClipboard"
'
'   Utilities for Windows Clip-board,
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = True

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
#Else
    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
#End If

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrCLIP = "%windir%\System32\clip.exe"

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
''' clip-board copy by InternetExplorer 11 object
'''
Public Sub PutInClipboardText(ByVal vstrText As String)

    Dim objElement As Object    ' HTMLTextAreaElement

#If HAS_REF Then

    With New SHDocVw.InternetExplorer
#Else
    With CreateObject("InternetExplorer.Application")
#End If
        .Visible = False
        
        .Navigate "about:blank"
        
        While .Busy Or .ReadyState <> 4
        
            'Application.Wait Now + TimeValue("0:00:01")
            Sleep 10
        Wend
        
        Set objElement = .Document.createElement("textarea")
        
        objElement.Value = vstrText
        
        .Document.Body.appendChild objElement
        
        objElement.Focus
        
        Set objElement = Nothing
        
        .ExecWB 17, 0
        
        .ExecWB 12, 0
        
        .Quit
    End With
End Sub


'''
''' After soon, avoid using the .Exec method, should use the Run with no-display mode
'''
Public Sub PutInClipboardTextByWshShell(ByVal vstrText As String)

#If HAS_REF Then

    With New IWshRuntimeLibrary.WshShell
#Else
    With CreateObject("WScript.Shell")
#End If
        With .Exec(mstrCLIP)
        
            .StdIn.Write vstrText
        End With
    End With
End Sub


'''
'''
'''
Public Function GetWindowsClipboardTextFromMSFormsDataObject() As String

    On Error Resume Next

#If HAS_REF Then

    With New MSForms.DataObject
#Else
    With CreateObject("new:{1C3B4210-F441-11CE-B9EA-00AA006B1A69}")
#End If
        .GetFromClipboard
        
        GetWindowsClipboardTextFromMSFormsDataObject = .GetText
    End With
End Function

'''
'''
'''
Public Function GetWindowsClipboardTextFromMSFormsTextBox() As String

    With CreateObject("Forms.TextBox.1")
    
        .MultiLine = True
        
        If .CanPaste Then .Paste
        
        GetWindowsClipboardTextFromMSFormsTextBox = .Text
    End With
End Function
