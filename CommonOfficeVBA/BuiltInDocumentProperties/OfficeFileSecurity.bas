Attribute VB_Name = "OfficeFileSecurity"
'
'   Serve each office file built-in property security
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat,  1/Sep/2023    Kalmclaeyd Tarclanus    Separated from InterfaceCall.bas
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Change BuiltinDocumentProperties
'**---------------------------------------------
'''
'''
'''
Public Sub ChangeBuiltInProperties(ByRef robjOfficeFile As Object, _
        ByVal vstrAuthorToOverwrite As String, _
        ByVal vstrLastAuthorToOverwrite As String, _
        Optional ByVal vstrCompanyToOverwrite As String = "")


    Dim objProperty As Office.DocumentProperty, blnIsChanged As Boolean, objApplication As Object, varOldDisplayAlerts As Variant
    
    Dim mintNoneAlert As Long


    blnIsChanged = False

    Select Case TypeName(robjOfficeFile)
    
        Case "Workbook" ' Excel.Workbook
    
            mintNoneAlert = CLng(False)
            
        Case "Document" ' Word.Document

            mintNoneAlert = 0   ' Word.WdAlertLevel.wdAlertsNone
            
        Case "Presentation" ' PowerPoint.Presentation

            mintNoneAlert = 1   ' PpAlertLevel.ppAlertsNone
    End Select


    With robjOfficeFile

        If vstrAuthorToOverwrite <> "" Then
        
            If mfblnIsDocumentPropertyUpdatedAfterTryToChange(.BuiltinDocumentProperties, "Author", vstrAuthorToOverwrite) Then
            
                blnIsChanged = True
            End If
        End If
        
        If vstrLastAuthorToOverwrite <> "" Then
        
            If mfblnIsDocumentPropertyUpdatedAfterTryToChange(.BuiltinDocumentProperties, "Last Author", vstrLastAuthorToOverwrite) Then
            
                blnIsChanged = True
            End If
        End If

        If vstrCompanyToOverwrite <> "" Then
        
            If mfblnIsDocumentPropertyUpdatedAfterTryToChange(.BuiltinDocumentProperties, "Company", vstrCompanyToOverwrite) Then
            
                blnIsChanged = True
            End If
        End If
    End With

    If blnIsChanged Then
    
        Set objApplication = robjOfficeFile.Application
    
        varOldDisplayAlerts = objApplication.DisplayAlerts
    
        Select Case TypeName(robjOfficeFile)
        
            Case "Workbook" ' Excel.Workbook
        
                objApplication.DisplayAlerts = False
        
            Case Else
            
                objApplication.DisplayAlerts = mintNoneAlert
        End Select
    
        msubSaveOfficeFileOnlyWhenItIsNotNewCreatedFile robjOfficeFile
        
        objApplication.DisplayAlerts = varOldDisplayAlerts
    End If
End Sub

'''
'''
'''
Public Function GetOfficeFileBuiltInPropertiesKeyValuesDic(ByRef robjOfficeFile As Object) As Scripting.Dictionary

    Dim varProperty As Variant, objDic As Scripting.Dictionary

    Set objDic = New Scripting.Dictionary

    For Each varProperty In robjOfficeFile.BuiltinDocumentProperties
    
        'Debug.Print TypeName(varProperty)
    
        'Debug.Print "DocumentProperty name: " & varProperty.Name
        
        With varProperty
        
            objDic.Add .Name, .Value
        End With
    Next

    Set GetOfficeFileBuiltInPropertiesKeyValuesDic = objDic
End Function


'''
'''
'''
Private Function mfblnIsDocumentPropertyUpdatedAfterTryToChange(ByRef robjBuiltinDocumentProperties As Object, _
        ByVal vstrPropertyName As String, _
        ByRef rstrNewValue As String) As Boolean

    Dim objProperty As Office.DocumentProperty, varProperty As Variant, blnIsChanged As Boolean
    
    blnIsChanged = False

    With robjBuiltinDocumentProperties
    
        On Error Resume Next
        
        Set objProperty = .Item(vstrPropertyName)
        
        On Error GoTo 0
    
        If Not objProperty Is Nothing Then
        
            If objProperty.Value <> rstrNewValue Then
            
                objProperty.Value = rstrNewValue
                
                blnIsChanged = True
            End If
        Else
            ' Retry As Variant
            
            On Error Resume Next
            
            Set varProperty = Nothing
            
            Set varProperty = .Item(vstrPropertyName)
            
            On Error GoTo 0
            
            If Not varProperty Is Nothing Then
            
                If varProperty.Value <> rstrNewValue Then
            
                    ' Probably, the changing document property from external the office file application process will be failed because of the Microsoft Office software securities
            
                    varProperty.Value = rstrNewValue
                    
                    blnIsChanged = True
                End If
            End If
        End If
    End With

    mfblnIsDocumentPropertyUpdatedAfterTryToChange = blnIsChanged
End Function


'**---------------------------------------------
'** About BuiltinDocumentProperties
'**---------------------------------------------
'''
'''
'''
''' <Argument>vobjOfficeFile: Type is either Excel.Workbook, Word.Document, or PowerPoint.Presentation</Argument>
Public Sub ModifyOfficeFilePropertiesInfo(ByVal vobjOfficeFile As Object, _
        Optional vblnDeletePersonalInfoFromOfficeFile As Boolean = True, _
        Optional ByVal vstrFileGenerationComment As String = "", _
        Optional ByVal vblnAddCurrentUserName As Boolean = True)


    If vblnDeletePersonalInfoFromOfficeFile Then
    
        DeleteOfficeFilePropertiesInfo vobjOfficeFile
    Else
        WriteGenerationOfficeFilePropertyInfoIntoNewGenerated vobjOfficeFile, vstrFileGenerationComment
    End If
End Sub

'''
'''
'''
''' <Argument>vobjOfficeFile: Type is either Excel.Workbook, Word.Document, or PowerPoint.Presentation</Argument>
Public Function DeleteOfficeFilePropertiesInfo(ByVal vobjOfficeFile As Object) As Boolean
    
    DeleteOfficeFilePropertiesInfo = False
    
    Dim strUserName As String
    
    On Error GoTo ErrHandler:
    
    strUserName = vobjOfficeFile.Application.UserName
    
    vobjOfficeFile.Application.UserName = " "
    
    With vobjOfficeFile
    
        With .BuiltinDocumentProperties
        
            .Item("Title").Value = ""
            
            .Item("Subject").Value = ""
            
            .Item("Category").Value = ""
            
            .Item("Comments").Value = ""
            
            .Item("Author").Value = Empty   ' Author
            
            .Item("Company").Value = Empty  ' Company
            
            .Item("Manager").Value = Empty  ' Manager
        End With
    End With
    
    msubSaveOfficeFileOnlyWhenItIsNotNewCreatedFile vobjOfficeFile
    
    vobjOfficeFile.Application.UserName = strUserName
    
    DeleteOfficeFilePropertiesInfo = True
    
ErrHandler:
    Debug.Print Err.Description & ", " & Err.Number
End Function


'''
'''
'''
''' <Argument>vobjOfficeFile: Type is either Excel.Workbook, Word.Document, or PowerPoint.Presentation</Argument>
Public Sub WriteGenerationOfficeFilePropertyInfoIntoNewGenerated(ByVal vobjOfficeFile As Object, _
        ByVal vstrComment As String, _
        Optional ByVal vblnAddCurrentUserName As Boolean = True)
    
    Dim objProperty As Office.DocumentProperty, objNetwork As WshNetwork

    With vobjOfficeFile
    
        With .BuiltinDocumentProperties
        
            .Item("Subject").Value = "Distributed by an auto-generator"
            
            If vblnAddCurrentUserName Then
            
                Set objNetwork = New WshNetwork
            
                .Item("Author").Value = "Auto-generator with " & objNetwork.UserName
            Else
                .Item("Author").Value = "Auto-generator"
            End If
            
            If vstrComment <> "" Then
            
                .Item("Comments").Value = vstrComment
            End If
        End With
    End With
    
    msubSaveOfficeFileOnlyWhenItIsNotNewCreatedFile vobjOfficeFile
End Sub


'''
'''
'''
Public Sub AddOfficeFilePropertyInfo(ByVal vobjExistedOfficeFile As Object, _
        ByVal vstrComment As String, Optional ByVal vblnTryToSave As Boolean = True)

    Dim objProperty As Office.DocumentProperty, strPreviousComment As String
    
    With vobjExistedOfficeFile
    
        With .BuiltinDocumentProperties
            
            If .Item("Subject").Value = "" Then
            
                .Item("Subject").Value = "Distributed by an auto-generator"
            End If
            
            
            If vstrComment <> "" Then
            
                Set objProperty = .Item("Comments")
            
                strPreviousComment = objProperty.Value
            
                If strPreviousComment <> "" Then
                
                    objProperty.Value = strPreviousComment & vbNewLine & vstrComment
                Else
                    objProperty.Value = vstrComment
                End If
            End If
        End With
        
        If vblnTryToSave Then
        
            .Save
        End If
    End With
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vobjOfficeFile: Type is either Excel.Workbook, Word.Document, or PowerPoint.Presentation</Argument>
Private Sub msubSaveOfficeFileOnlyWhenItIsNotNewCreatedFile(ByRef robjOfficeFile As Object)

    Dim strFullPath As String
    
    strFullPath = ""
    
    On Error Resume Next
    
    strFullPath = VBA.CallByName(robjOfficeFile, "FullName", VbGet)
    
    If FileExistsByVbaDir(strFullPath) Then
    
        robjOfficeFile.Save
    End If

    On Error GoTo 0
End Sub


