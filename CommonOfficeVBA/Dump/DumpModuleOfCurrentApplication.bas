Attribute VB_Name = "DumpModuleOfCurrentApplication"
'
'   dump current Ms Office application relative object types
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////

#Const HAS_REF = False

'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then '64bit

    Private Declare PtrSafe Function GetCurrentProcessId Lib "kernel32" () As Long
#Else
    Private Declare Function GetCurrentProcessId Lib "kernel32" () As Long
#End If

'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
'''
'''
Private Enum CurrentMsOfficeApplicationType

    CurrentVBEIsInAnExcel
    
    CurrentVBEIsInAnWord
    
    CurrentVBEIsInAnPowerPoint
    
End Enum


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub CurrentOfficeApplicationProcessInfo()
    
    CurrentProcessInfoFromThisApplication mfobjGetCurrentOfficeFile()
End Sub

'''
'''
'''
Public Sub CurrentProcessInfoFromThisApplication(ByVal vobjCurrentOfficeFile As Object, _
        Optional ByVal vblnCreateLogTextFile As Boolean = False, _
        Optional ByVal vstrLogFilePrefix As String = "", _
        Optional ByVal vstrLoggingDir As String = "")
    
    Dim strLog As String, varOfficeFileObject As Object, strLoggingDir As String
    
    strLog = mfstrGetLogStringOfCurrentApplicationProcess(vobjCurrentOfficeFile)
    
    With New Scripting.FileSystemObject
        
        If vstrLoggingDir <> "" Then
            
            strLoggingDir = vstrLoggingDir
        Else
            strLoggingDir = .GetParentFolderName(vobjCurrentOfficeFile.FullName)
        End If
        
        If vblnCreateLogTextFile Then
            
            With .CreateTextFile(strLoggingDir & "\LogProcessID" & CStr(GetCurrentProcessId()) & "_" & vstrLogFilePrefix & .GetBaseName(vobjCurrentOfficeFile.FullName) & "_" & Format(Now(), "yyyymmdd_hhmmss") & ".txt", True)
                
                .Write strLog
                
                .Close
            End With
        Else
            Debug.Print strLog
        End If
    End With
End Sub

'''
'''
'''
Private Function mfstrGetLogStringOfCurrentApplicationProcess(ByRef robjCurrentOfficeFile As Object) As String

    Dim strLog As String, enmCurrentMsOfficeApplicationType As CurrentMsOfficeApplicationType, varOfficeFileObject As Variant

    strLog = ""

    Select Case Trim(Replace(LCase(Application.Name), "microsoft", ""))
    
        Case "excel"
        
            enmCurrentMsOfficeApplicationType = CurrentVBEIsInAnExcel
            
        Case "word"
        
            enmCurrentMsOfficeApplicationType = CurrentVBEIsInAnWord
            
        Case "powerpoint"
    
            enmCurrentMsOfficeApplicationType = CurrentVBEIsInAnPowerPoint
    End Select
    
    With New Scripting.FileSystemObject
    

    
        strLog = "Current ProcessID: " & GetCurrentProcessId & vbNewLine
        
        Select Case enmCurrentMsOfficeApplicationType
        
            Case CurrentMsOfficeApplicationType.CurrentVBEIsInAnExcel
        
                strLog = strLog & vbTab & "Macro Workbook name: " & robjCurrentOfficeFile.Name & vbNewLine
        
            Case CurrentMsOfficeApplicationType.CurrentVBEIsInAnWord
            
                strLog = strLog & vbTab & "Macro Document name: " & robjCurrentOfficeFile.Name & vbNewLine
            
            Case CurrentMsOfficeApplicationType.CurrentVBEIsInAnPowerPoint
        
                strLog = strLog & vbTab & "Macro PowerPoint name: " & robjCurrentOfficeFile.Name & vbNewLine
        End Select
        
        strLog = strLog & vbTab & "Memory usage: " & GetCurrentOfficeApplicationProcessMemoryUsage() & " [MB]" & vbNewLine & vbNewLine
        
        For Each varOfficeFileObject In GetCurrentApplicationOpenedObjects()
        
            Select Case enmCurrentMsOfficeApplicationType
        
                Case CurrentMsOfficeApplicationType.CurrentVBEIsInAnExcel
                    
                    strLog = strLog & "Opened Workbook path: " & varOfficeFileObject.FullName & vbNewLine
                
                Case CurrentMsOfficeApplicationType.CurrentVBEIsInAnWord
                
                    strLog = strLog & "Opened Document path: " & varOfficeFileObject.FullName & vbNewLine
                    
                Case CurrentMsOfficeApplicationType.CurrentVBEIsInAnPowerPoint
                
                    strLog = strLog & "Opened PowerPoint path: " & varOfficeFileObject.FullName & vbNewLine
            End Select
        Next
    End With

    mfstrGetLogStringOfCurrentApplicationProcess = strLog
End Function


'''
''' get all process IDs for each EXCEL.EXE by WMI service
'''
Public Function DebugExcelProcessHandle()

    Dim strWQL As String
    
#If HAS_REF Then
    Dim objItems As WbemScripting.SWbemObjectSet, objXlsProc As WbemScripting.SWbemObjectEx
#Else
    Dim objItems As Object, objXlsProc As Object
#End If
    
    strWQL = "SELECT Handle FROM Win32_Process WHERE Name = 'EXCEL.EXE'"
    
    Set objItems = GetObject("winmgmts:").ExecQuery(strWQL)
    
    For Each objXlsProc In objItems
    
        Debug.Print objXlsProc.Handle
    Next
End Function

'''
'''
'''
Public Sub ConfirmOpenedBooks()

    Dim objApplication As Excel.Application, objBook As Excel.Workbook
    
    Set objApplication = ThisWorkbook.Application
    
    For Each objBook In objApplication.Workbooks
    
        Debug.Print objBook.FullName
    Next
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function GetCurrentApplicationOpenedObjects() As Collection

    Dim objCol As Collection, varOfficeFile As Variant
    
    Set objCol = New Collection
    
    Select Case Trim(Replace(LCase(Application.Name), "microsoft", ""))
    
        Case "excel"
    
            For Each varOfficeFile In mfobjGetCurrentOfficeFile().Application.Workbooks
    
                objCol.Add varOfficeFile
            Next
            
        Case "word"
        
            For Each varOfficeFile In mfobjGetCurrentOfficeFile().Application.Documents
    
                objCol.Add varOfficeFile
            Next
            
        Case "powerpoint"
        
            For Each varOfficeFile In mfobjGetCurrentOfficeFile().Application.Presentations
    
                objCol.Add varOfficeFile
            Next
    End Select

    Set GetCurrentApplicationOpenedObjects = objCol
End Function

'''
''' Attention! use duplicated codes for keeping this module independency
'''
Private Function mfobjGetCurrentOfficeFile() As Object

    Dim objOfficeObject As Object
    
    Set objOfficeObject = Nothing

    Select Case Trim(Replace(LCase(Application.Name), "microsoft", ""))
    
        Case "excel"
        
            On Error Resume Next
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisWorkbook", VbGet) ' Application.ThisWorkbook
            
            On Error GoTo 0
            
            If objOfficeObject Is Nothing Then
            
                Set objOfficeObject = VBA.CallByName(Application, "ActiveWorkbook", VbGet)  ' Excel.Workbook
            End If
            
        Case "word"
        
            On Error Resume Next
            
            Set objOfficeObject = VBA.CallByName(Application, "ThisDocument", VbGet)
            
            On Error GoTo 0
            
            If objOfficeObject Is Nothing Then
            
                Set objOfficeObject = VBA.CallByName(Application, "ActiveDocument", VbGet)  ' Word.Document
            End If
            
        Case "powerpoint"
    
            Set objOfficeObject = VBA.CallByName(Application, "ActivePresentation", VbGet)  ' PowerPoint.Presentation
    End Select

    Set mfobjGetCurrentOfficeFile = objOfficeObject
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetCurrentApplicationOpenedObjects()

    Dim varOfficeFileObject As Variant

    For Each varOfficeFileObject In GetCurrentApplicationOpenedObjects()
    
        Debug.Print varOfficeFileObject.FullName
    Next
End Sub

'''
'''
'''
Private Sub msubSanityTestToDebugExcelProcessHandle()

    DebugExcelProcessHandle
End Sub

