Attribute VB_Name = "DumpModule"
'
'   dump various data type objects
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'       Dependent on Scripting.Dictionary
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then '64bit
    Private Declare PtrSafe Function GetCurrentProcessId Lib "kernel32" () As Long
    
    Private Declare PtrSafe Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As LongPtr, ByVal dwMilliseconds As Long) As Long
    
    Private Declare PtrSafe Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As LongPtr
    
    Private Declare PtrSafe Function CloseHandle Lib "kernel32" (ByVal hObject As LongPtr) As Long
#Else
    Private Declare Function GetCurrentProcessId Lib "kernel32" () As Long
    
    Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
    
    Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
    
    Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
#End If

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const PROCESS_ALL_ACCESS As Long = &H1F0FFF ' (STANDARD_RIGHTS_REQUIRED (0x000F0000L) |SYNCHRONIZE (0x00100000L) |0xFFFF)

Private Const PROCESS_QUERY_INFORMATION As Long = &H400

Private Const PROCESS_QUERY_LIMITED_INFORMATION As Long = &H1000

Private Const INFINITE As Long = &HFFFF


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** print debug strings into VBE immediate window
'**---------------------------------------------
'''
''' Returns the current Excel.Application
''' memory usage in MB
'''
Public Function GetCurrentOfficeApplicationProcessMemoryUsage()
    
    With GetObject("winmgmts:")
        
        GetCurrentOfficeApplicationProcessMemoryUsage = .Get("Win32_Process.Handle='" & GetCurrentProcessId & "'").WorkingSetSize / 1024
    End With
End Function

'''
'''
'''
Public Function GetThisApplicationProcessID() As Variant

    GetThisApplicationProcessID = GetCurrentProcessId()
End Function

'''
''' Run
'''
Public Sub RunWithWating(ByVal vstrExecutableProgramFilePath As String)

    Dim intTaskID As Long
    
#If VBA7 Then '64bit
    Dim intProcessHandle As LongPtr
#Else
    Dim intProcessHandle As Long
#End If

    intTaskID = VBA.Shell(vstrExecutableProgramFilePath, vbHide)
    
    intProcessHandle = OpenProcess(PROCESS_ALL_ACCESS, 1, intTaskID)

    If intProcessHandle <> 0 Then
    
        WaitForSingleObject intProcessHandle, INFINITE
        
        CloseHandle intProcessHandle
    End If
End Sub

'''
''' Get the process handle of the application which had been started and release the process handle
'''
Private Sub msubSampleForCalc()
    
    Dim intProcessID As Long

#If VBA7 Then
    Dim intProcessHandle As LongPtr
#Else
    Dim intProcessHandle As Long
#End If

    ' Start to the calculator application and get Task ID
    intProcessID = CLng(Shell("calc", vbNormalFocus))
    
    ' get the process handle
    intProcessHandle = OpenProcess(PROCESS_QUERY_INFORMATION, 1, intProcessID)
    
    MsgBox "Calc ProcessHandle: " & intProcessHandle
    
    ' Release the object handle
    CloseHandle intProcessHandle
End Sub

'''
'''
'''
Public Sub DebugCol(ByVal vobjCol As Collection)

    Dim blnIsNestedCollection As Boolean

    blnIsNestedCollection = False

    If Not vobjCol Is Nothing Then
    
        If vobjCol.Count > 0 Then
        
            If IsObject(vobjCol.Item(1)) Then
            
                If StrComp(TypeName(vobjCol.Item(1)), "Collection") = 0 Then
                
                    blnIsNestedCollection = True
                End If
            End If
            
            If blnIsNestedCollection Then
            
                DebugDumpNestedColAsString vobjCol
            Else
                DebugDumpColAsString vobjCol
            End If
        End If
    End If
End Sub

'''
'''
'''
Public Sub DebugColAboutRowColumnCounts(ByVal vobjCol As Collection)

    Dim blnIsNestedCollection As Boolean, strLog As String, i As Long
    
    Dim varChildCol As Variant, objChildCol As Collection
    Dim intColumnsCount As Long
    Dim intFirstColumnsCount As Long, blnIncludesValueButNotObject As Boolean
    
    Dim blnIncludesDifferentColumnsCollection As Boolean
    
    
    blnIncludesValueButNotObject = False

    blnIncludesDifferentColumnsCollection = False
    

    blnIsNestedCollection = False

    If Not vobjCol Is Nothing Then
    
        Debug.Print "Row counts: " & CStr(vobjCol.Count)
    
        If vobjCol.Count > 0 Then
        
            If IsObject(vobjCol.Item(1)) Then
            
                If StrComp(TypeName(vobjCol.Item(1)), "Collection") = 0 Then
                
                    blnIsNestedCollection = True
                End If
            End If
            
            If blnIsNestedCollection Then
            
                i = 1
            
                strLog = ""
            
                For Each varChildCol In vobjCol
                
                    If IsObject(varChildCol) Then
                
                        Set objChildCol = varChildCol
                    
                        intColumnsCount = objChildCol.Count
                    
                        If i = 1 Then
                        
                            intFirstColumnsCount = intColumnsCount
                        End If
                        
                        If intColumnsCount <> intFirstColumnsCount Then
                        
                            blnIncludesDifferentColumnsCollection = True
                        End If
                        
                        strLog = strLog & vbTab & "Row index: " & CStr(i) & ", Columns count: " & CStr(intColumnsCount) & vbNewLine
                    Else
                        blnIncludesValueButNotObject = True
                    End If
                
                    i = i + 1
                Next
                
                If blnIncludesDifferentColumnsCollection Then
                 
                    Debug.Print strLog
                Else
                    Debug.Print "Columns count: " & CStr(intFirstColumnsCount) & " - [Constant]"
                End If
            Else
                Debug.Print "Column counts: 1"
            End If
        End If
    End If
End Sub


'''
'''
'''
Public Sub DebugDic(ByVal vobjDic As Scripting.Dictionary, _
        Optional ByVal vblnAllowToInterpretSingleDimensionColAsRowData As Boolean = True)

    Dim varKey As Variant, strDumpedKey As String, objCol As Collection, objDTCol As Collection, objRowCol As Collection, varRowCol As Variant
    
    Dim objChildDic As Scripting.Dictionary
    Dim varChildKey As Variant, varChildItem As Variant
    
    Dim varGrandChildItem As Variant, objGrandChildCol As Collection
    
    For Each varKey In vobjDic.Keys
    
        If IsObject(varKey) Then
        
            If TypeOf varKey Is Collection Then
            
                ' It doesn't seems recommended that the Key is a Colection object
            
                strDumpedKey = "[" & GetLineTextFromCol(varKey) & "]"
                
            ElseIf TypeOf varKey Is Scripting.Dictionary Then
            
                ' It doesn't seems recommended that the Key is a Colection object
            
                strDumpedKey = "[" & GetLineTextFromDic(varKey) & "]"
            End If
        Else
            strDumpedKey = varKey
        End If
    
        If IsObject(vobjDic.Item(varKey)) Then
        
            If TypeOf vobjDic.Item(varKey) Is Collection Then
                
                Set objCol = vobjDic.Item(varKey)
                
                If objCol.Count > 0 Then
                
                    If TypeOf objCol.Item(1) Is Collection Then
                    
                        ' The .Item(varKey) is expected as data-table-collection
                        
                        Set objDTCol = objCol
                    
                        For Each varRowCol In objDTCol
                        
                            Set objRowCol = varRowCol
                            
                            Debug.Print strDumpedKey & ", " & GetLineTextFromCol(objRowCol)
                        Next
                    Else
                        ' The .Item(varKey) is expected as Row-collection
                        
                        Set objRowCol = objCol
                        
                        Debug.Print strDumpedKey & ", " & GetLineTextFromCol(objRowCol)
                    End If
                End If
                
            ElseIf TypeOf vobjDic.Item(varKey) Is Scripting.Dictionary Then
                
                If vblnAllowToInterpretSingleDimensionColAsRowData Then
                
                    DebugDicByEachLineFromDic vobjDic.Item(varKey), strDumpedKey & ", ", "", ",", True
                Else
                
                    ' child-col is as a column-data-col
                
                    Set objChildDic = vobjDic.Item(varKey)
                    
                    With objChildDic
                    
                        For Each varChildKey In .Keys
                        
                            If IsObject(.Item(varChildKey)) Then
                            
                                If TypeOf .Item(varChildKey) Is Collection Then
                                
                                    Set objGrandChildCol = .Item(varChildKey)
                                    
                                    For Each varGrandChildItem In objGrandChildCol
                                    
                                        Debug.Print strDumpedKey & ", " & CStr(varChildKey) & ", " & CStr(varGrandChildItem)
                                    Next
                                    
                                ElseIf TypeOf .Item(varChildKey) Is Scripting.Dictionary Then
                                
                                    DebugDicByEachLineFromDic .Item(varChildKey), strDumpedKey & ", " & CStr(varChildKey) & ", ", "", ",", True
                                End If
                            Else
                                Debug.Print strDumpedKey & ", " & CStr(varChildKey) & ", " & CStr(.Item(varChildKey))
                            End If
                        Next
                    End With
                End If
            End If
        Else
            Debug.Print strDumpedKey & ", " & vobjDic.Item(varKey)
        End If
    Next
End Sub

'''
'''
'''
Public Sub DebugTypeNameOfDic(ByVal vobjDic As Scripting.Dictionary)

    Debug.Print GetTypeNameOfDicStructure(vobjDic)
End Sub


'''
'''
'''
Public Sub DebugDicByEachLineFromDic(ByVal vobjDic As Scripting.Dictionary, _
        Optional ByVal vstrPrefixLine As String = "", _
        Optional ByVal vstrSurroundChar As String = "", _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True)

    Dim strLine As String
    Dim varKey As Variant, varItem As Variant, objRowCol As Collection
    
    strLine = ""
    
    With vobjDic
    
        For Each varKey In .Keys
        
            strLine = ""
        
            strLine = strLine & vstrSurroundChar & CStr(varKey) & vstrSurroundChar
        
            If IsObject(.Item(varKey)) Then
            
                If StrComp(TypeName(.Item(varKey)), "Collection") = 0 Then
                
                    If vblnIncludeSpaceAfterDelimitedChar Then
                        
                        strLine = strLine & vstrDelimiter & " "
                    Else
                    
                        strLine = strLine & vstrDelimiter
                    End If
                    
                    strLine = strLine & GetLineTextFromCol(.Item(varKey), vstrSurroundChar, vstrDelimiter, vblnIncludeSpaceAfterDelimitedChar)
                    
                ElseIf StrComp(TypeName(.Item(varKey)), "Dictionary") = 0 Then
                
                    If vblnIncludeSpaceAfterDelimitedChar Then
                        
                        strLine = strLine & vstrDelimiter & " "
                    Else
                        strLine = strLine & vstrDelimiter
                    End If
                    
                    ' recursive call
                    strLine = strLine & GetLineTextFromDic(.Item(varKey), vstrSurroundChar, vstrDelimiter, vblnIncludeSpaceAfterDelimitedChar)
                End If
            Else
                Let varItem = .Item(varKey)
                
                If vblnIncludeSpaceAfterDelimitedChar Then
                        
                    strLine = strLine & vstrDelimiter & " "
                Else
                
                    strLine = strLine & vstrDelimiter
                End If
                
                If Not IsNull(varItem) Then
                
                    strLine = strLine & vstrSurroundChar & CStr(varItem) & vstrSurroundChar
                End If
            End If
    
            Debug.Print vstrPrefixLine & strLine
        Next
    End With
End Sub



'''
'''
'''
Public Sub DebugDumpColAsString(ByVal vobjCol As Collection)

    Dim varItem As Variant
    
    For Each varItem In vobjCol
    
        Debug.Print varItem
    Next
End Sub

'''
'''
'''
Public Sub DebugVar(ByRef rvarValues As Variant, _
        Optional ByVal vblnAllowToTransposeWhenTheOneDimensionalArray As Boolean = False, _
        Optional ByVal vstrDelimitedChar As String = ",")

 
    Debug.Print GetTextFromVar(rvarValues, vblnAllowToTransposeWhenTheOneDimensionalArray, vstrDelimitedChar)
End Sub


'''
''' dump one-dimentional array to immediate window
'''
Public Sub DebugDumpArrayWithDelimited(ByRef rvarValues As Variant, Optional ByVal vstrDelimitedChar As String = ",")

    Dim varValue As Variant, intCountOfArray As Long, i As Long
    Dim strLine As String
    
    If VBA.IsArray(rvarValues) Then
    
        intCountOfArray = UBound(rvarValues) - LBound(rvarValues) + 1
        
        strLine = ""
        
        i = 1
        
        For Each varValue In rvarValues
        
            strLine = strLine & CStr(varValue)
        
            If i < intCountOfArray Then
            
                strLine = strLine & vstrDelimitedChar & " "
            End If
            
            i = i + 1
        Next

        Debug.Print strLine
    End If
End Sub

'''
'''
'''
Public Sub DebugDumpNestedColAsString(ByVal vobjNestedCol As Collection)

    Dim varItem As Variant, objInnerCol As Collection
    Dim strLine As String, i As Long
    
    For Each objInnerCol In vobjNestedCol
        
        strLine = ""
        
        i = 1
        
        For Each varItem In objInnerCol
        
            If Not IsNull(varItem) Then
        
                strLine = strLine & CStr(varItem)
            End If
            
            If i < objInnerCol.Count Then
            
                strLine = strLine & ", "
            End If
        
            i = i + 1
        Next
        
        Debug.Print strLine
    Next
End Sub



'''
''' load stub-data for unit-test
'''
Public Function LoadUnitTestStubDataFromFile(ByVal vstrPath As String) As Collection
    
    Dim objCol As Collection, strLines() As String, strAllText As String
    Dim strValue As String, i As Long
    
    With New Scripting.FileSystemObject
    
        If .FileExists(vstrPath) Then
            
            Set objCol = New Collection
            
            With .OpenTextFile(vstrPath)
            
                strAllText = .ReadAll
            
                .Close
            End With
            
            
            strLines = Split(strAllText, vbNewLine)
            
            For i = LBound(strLines) To UBound(strLines)
            
                strValue = Trim(strLines(i))
                
                If strValue <> "" Then
                    
                    objCol.Add strValue
                End If
            Next
            
'            While Not objTS.AtEndOfStream
'
'            Wend
        End If
    End With

    Set LoadUnitTestStubDataFromFile = objCol
End Function

'''
'''
'''
Public Sub DebugDumpOneDimensionalArray(ByRef rvarArray As Variant)

    Dim varValue As Variant
    
    For Each varValue In rvarArray
    
        Debug.Print varValue
    Next
End Sub

'''
'''
'''
Public Sub DumpStringToCodeInteger(ByRef rstrText As String)

    Dim i As Long
    
    For i = 1 To Len(rstrText)
        
        Debug.Print i & " " & Mid(rstrText, i, 1) & " " & Hex(Asc(Mid(rstrText, i, 1)))
    Next
End Sub

'**---------------------------------------------
'** Dump RegExp hit results
'**---------------------------------------------
'''
''' output to immediate window for VBScript regular expression match results
'''
Public Sub DebugOutputOfRegExpMatches(ByVal vobjRegExp As VBScript_RegExp_55.RegExp, ByVal vobjLines As Collection)

    Dim strLine As String, varLine As Variant, objMatch As VBScript_RegExp_55.Match, i As Long, j As Long
    Dim varSubMatch As Variant

    With vobjRegExp
    
        For Each varLine In vobjLines
        
            strLine = varLine
            
            If .Test(strLine) Then
            
                i = 1
                
                For Each objMatch In .Execute(strLine)
                
                    Debug.Print "Hit <" & CStr(i) & "> : [" & CStr(objMatch.Value) & "], Original string : [" & strLine & "]"
                
                    j = 1
                    
                    For Each varSubMatch In objMatch.SubMatches
                    
                        Debug.Print "Sub matches <" & CStr(i) & "> : [" & CStr(varSubMatch) & "]"
                        
                        j = j + 1
                    Next
                
                    i = i + 1
                Next
            Else
                Debug.Print "No-hit, original line : " & strLine
            End If
        Next
    End With
End Sub

'''
'''
'''
Public Sub DebugOutputOfMatchedConnectedString(ByVal vobjRegExp As VBScript_RegExp_55.RegExp, _
        ByVal vobjLines As Collection)

    Debug.Print GetAllMatchedConnectedStringAndCompareLog(vobjRegExp, vobjLines)
End Sub

'''
'''
'''
''' <Argument>vobjRegExp: Input</Argument>
''' <Argument>vobjLines: Input</Argument>
''' <Return>String: log-text</Return>
Public Function GetAllMatchedConnectedStringAndCompareLog(ByVal vobjRegExp As VBScript_RegExp_55.RegExp, _
        ByVal vobjLines As Collection) As String

    Dim varLine As Variant, strLine As String, strLog As String
    
    Dim strMatchedConnectedLine As String
    
    strLog = ""
    
    With vobjRegExp
    
        For Each varLine In vobjLines
        
            strLine = varLine
            
            If .Test(strLine) Then
            
                strMatchedConnectedLine = GetConnectedMatchedString(.Execute(strLine))
            
                If strLine = strMatchedConnectedLine Then
                
                    strLog = strLog & "Perfect hits: " & strMatchedConnectedLine & vbNewLine
                Else
                    strLog = strLog & "Partially hits, original string : [" & strLine & "], conntected matched string :[" & strMatchedConnectedLine & "]" & vbNewLine
                End If
            Else
                strLog = strLog & "No-hit, original string : [" & strLine & "]" & vbNewLine
            End If
        Next
    End With

    GetAllMatchedConnectedStringAndCompareLog = strLog
End Function


'**---------------------------------------------
'** Creating testing Collection object and Dictionary object
'**---------------------------------------------
'''
'''
'''
Public Function GetExtraOrdinaryTestingCollection01(Optional ByVal vblnAllowToIncludeNothingValue As Boolean = False) As Collection

    Dim objCol As Collection: Set objCol = New Collection
    
    With objCol
    
        .Add 1: .Add Null: .Add "abc": .Add Empty: .Add 1.2
    
        If vblnAllowToIncludeNothingValue Then .Add Nothing
    End With
    
    Set GetExtraOrdinaryTestingCollection01 = objCol
End Function

'''
'''
'''
Public Function GetExtraOrdinaryTestingCollection02(Optional ByVal vblnAllowToIncludeNothingValue As Boolean = False) As Collection

    Dim objCol As Collection: Set objCol = New Collection
    
    With objCol
    
        .Add 2: .Add Null: .Add "def": .Add Empty: .Add 2.4
    
        If vblnAllowToIncludeNothingValue Then .Add Nothing
    End With
    
    Set GetExtraOrdinaryTestingCollection02 = objCol
End Function

'''
''' Collection(Of Collection(Of Variant))
'''
Public Function GetExtraOrdinaryTestingNestedCollection01(Optional ByVal vblnAllowToIncludeNothingValue As Boolean = False) As Collection

    Dim objCol As Collection
    
    Set objCol = New Collection
    
    With objCol
    
        .Add GetExtraOrdinaryTestingCollection01()
        
        .Add GetExtraOrdinaryTestingCollection02()
    End With

    Set GetExtraOrdinaryTestingNestedCollection01 = objCol
End Function

'''
''' Dictionary(Of Variant, Collection(Of Variant))
'''
Public Function GetExtraOrdinaryTestingNestedDictionary01(Optional ByVal vblnAllowToIncludeNothingValue As Boolean = False) As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary: Set objDic = New Scripting.Dictionary
    
    With objDic
    
        .Add 1, GetExtraOrdinaryTestingCollection01()
    
        .Add 2, GetExtraOrdinaryTestingCollection02()
    End With

    Set GetExtraOrdinaryTestingNestedDictionary01 = objDic
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToGetExtraOrdinaryTestingCollection()

    DebugCol GetExtraOrdinaryTestingCollection01
    
    'DebugCol GetExtraOrdinaryTestingCollection01(True)
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetExtraOrdinaryTestingNestedCollection01()

    DebugCol GetExtraOrdinaryTestingNestedCollection01
    
    'DebugCol GetExtraOrdinaryTestingCollection01(True)
End Sub

'''
'''
'''
Private Sub msubSanityTestToGetExtraOrdinaryTestingNestedDictionary01()

    DebugDic GetExtraOrdinaryTestingNestedDictionary01()
End Sub


