Attribute VB_Name = "DumpModuleToTextFile"
'
'   I recommend to use this defined operation procedures in the VBIDE immediate-window
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Development Policy:
'       Dump module both with a logging text file and some pre-installed text-editor (ExternalTextEditors.bas and OperateWindowsProcesses.bas)
'       This module also works in the Word appication and the PowerPoint application
'
'   Dependency Abstract:
'       Dependent on Windows OS
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 10/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrDebugTextSubDirectoryName As String = "DebugPrintOutByVBA"

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Write debug strings into test file in the local hardware place
'**---------------------------------------------
'''
'''
'''
Public Sub DebugDicToTmpText(ByRef robjDic As Scripting.Dictionary)

    DebugPrintToTextFile GetTextFromDic(robjDic), "DebugDicWithPrintingFromVBA.txt"
End Sub

'''
'''
'''
Public Sub DebugColToTmpText(ByRef robjCol As VBA.Collection)

    DebugPrintToTextFile GetTextFromCol(robjCol), "DebugColWithPrintingFromVBA.txt"
End Sub

'''
'''
'''
Public Sub DebugVarToTmpText(ByRef rvarTwoDimensionalArray As Variant)

    DebugPrintToTextFile GetTextFromVar(rvarTwoDimensionalArray), "DebugVarWithPrintingFromVBA.txt"
End Sub


'''
'''
'''
Public Sub DebugPrintToTextFile(ByRef rstrDebugText As String, Optional ByVal vstrDebugTextFileName As String = "DebugPrintFromVBA.txt", Optional ByVal vstrAdditionalHeaderComment As String = "")

    Dim strPath As String
    
    strPath = mfstrGetDebugPrintoutPath(vstrDebugTextFileName)
    
    msubCreateLogTextFile strPath, rstrDebugText, vstrAdditionalHeaderComment
    
    OpenTextBySomeSelectedTextEditorFromWshShell strPath, True, True
End Sub

'''
'''
'''
Public Sub DebugPrintToFullPathTextFile(ByRef rstrDebugText As String, ByVal vstrLogPath As String)

    msubCreateLogTextFile vstrLogPath, rstrDebugText
    
    OpenTextBySomeSelectedTextEditorFromWshShell vstrLogPath, True, True
End Sub

'**---------------------------------------------
'** Log texts generation
'**---------------------------------------------
'''
''' get Debug texts string for saving file as the text file about RegExp results
'''
Public Function GetLogOfRegExpAndPluralLines(ByVal vobjRegExp As VBScript_RegExp_55.RegExp, ByVal vobjLines As Collection) As String

    Dim strLog As String, varLine As Variant, strLine As String, i As Long, intMatchIndex As Long
    Dim objMatch As VBScript_RegExp_55.Match, varSubMatch As Variant
    
    strLog = "" & vbNewLine
    
    With vobjRegExp
    
        For Each varLine In vobjLines
        
            strLine = varLine
    
            strLog = strLog & "Original code: [" & strLine & "]" & vbNewLine
    
            If .Test(strLine) Then
                
                intMatchIndex = 0
                
                For Each objMatch In .Execute(strLine)
                
                    'Debug.Print "Match [" & CStr(intMatchIndex) & "] - all value : " & objMatch.Value
                    strLog = strLog & "Match [" & CStr(intMatchIndex) & "] - all value : " & objMatch.Value & vbNewLine
                
                    i = 0
                    For Each varSubMatch In objMatch.SubMatches
                    
                        'Debug.Print "Match [" & CStr(intMatchIndex) & "] - Submatch " & CStr(i) & " : " & varSubMatch
                        strLog = strLog & "Match [" & CStr(intMatchIndex) & "] - Submatch " & CStr(i) & " : " & varSubMatch & vbNewLine
                        
                        i = i + 1
                    Next
                    
                    intMatchIndex = intMatchIndex + 1
                Next
            Else
                'Debug.Print "No-hit, original line : " & strLine
                
                strLog = strLog & "No-hit, original line : " & vbNewLine
            End If
            
            strLog = strLog & vbNewLine
        Next
    End With

    GetLogOfRegExpAndPluralLines = strLog
End Function


'''
'''
'''
''' <Argument>vobjRegExp: Input</Argument>
''' <Argument>vobjLines: Input</Argument>
''' <Return>String: log-text</Return>
Public Function GetLogBothOfRegExpTestAndAllMatchedConnectedString(ByVal vobjRegExp As VBScript_RegExp_55.RegExp, _
        ByVal vobjLines As Collection) As String

    Dim strLog As String

    strLog = ""
    
    strLog = strLog & GetLogOfRegExpAndPluralLines(vobjRegExp, vobjLines)
    
    strLog = strLog & vbNewLine & "MatchedValuesAllConnected:" & vbNewLine & vbNewLine
    
    strLog = strLog & GetAllMatchedConnectedStringAndCompareLog(vobjRegExp, vobjLines)

    GetLogBothOfRegExpTestAndAllMatchedConnectedString = strLog
End Function

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfstrGetDebugPrintoutPath(ByVal vstrDebugTextFileName As String)

    Dim strDir As String, strPath As String
    
    strDir = GetCurrentOfficeFileObject().Path & "\" & mstrDebugTextSubDirectoryName
    
    With CreateObject("Scripting.FileSystemObject") ' New Scripting.FileSystemObject
    
        If Not .FolderExists(strDir) Then
        
            .CreateFolder strDir
        End If
    End With
    
    strPath = strDir & "\" & vstrDebugTextFileName
    
    mfstrGetDebugPrintoutPath = strPath
End Function


'''
'''
'''
Private Sub msubCreateLogTextFile(ByVal vstrPath As String, ByVal vstrLogText As String, Optional ByVal vstrAdditionalHeaderComment As String = "")

    Dim varText As Variant, strText As String, strLines As String

    With CreateObject("Scripting.FileSystemObject")  ' New Scripting.FileSystemObject

        With .CreateTextFile(vstrPath, True)
        
            .WriteLine "Logged time : " & FormatDateTimeWithWeekdayByEnglish(Now())
        
            If vstrAdditionalHeaderComment <> "" Then
            
                strLines = ""
            
                For Each varText In Split(vstrAdditionalHeaderComment, vbNewLine)
                
                    strText = varText
                
                    If strLines <> "" Then
                    
                        strLines = strLines & vbNewLine
                    End If
                
                    strLines = strLines & strText
                Next
                
                .WriteLine vbNewLine & strLines & vbNewLine
                
            End If
        
            .WriteLine vstrLogText
            
            .Close
        End With
    End With
End Sub
