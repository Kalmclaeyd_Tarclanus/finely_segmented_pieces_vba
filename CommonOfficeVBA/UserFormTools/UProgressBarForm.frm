VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UProgressBarForm 
   Caption         =   "UProgressBarForm"
   ClientHeight    =   1540
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   6240
   OleObjectBlob   =   "UProgressBarForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "UProgressBarForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   Simple MSComctlLib.ProgressBar form with modeless
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both MSForms and MSComctlLib
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 12/Jun/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Windows API declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function GetInputState Lib "user32" () As Boolean
#Else
    Private Declare Function GetInputState Lib "user32" () As Boolean
#End If

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjFormTopMostEnabledCtlHdr As FormTopMostEnabledCtlHdr


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub UserForm_Initialize()

    ' When the modal mode, don't use to SetFormToTopMost. In the only modeless mode, should use to SetFormToTopMost
    
    InitializeFormTopMostEnabledControlHandler mobjFormTopMostEnabledCtlHdr, Me
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get Min() As Single

    Min = pgrProgressBar.Min
End Property
Public Property Let Min(ByVal vsngMin As Single)

    pgrProgressBar.Min = vsngMin
End Property

'''
'''
'''
Public Property Get Max() As Single

    Max = pgrProgressBar.Max
End Property
Public Property Let Max(ByVal vsngMax As Single)

    pgrProgressBar.Max = vsngMax
End Property

'''
'''
'''
Public Property Get Value() As Single

    Value = pgrProgressBar.Value
End Property
Public Property Let Value(ByVal vsngValue As Single)

    If pgrProgressBar.Value <> vsngValue Then
    
        pgrProgressBar.Value = vsngValue
    
        If GetInputState() Then DoEvents
    End If
End Property

'''
'''
'''
Public Property Get ShortMessage() As String

    ShortMessage = lblShortMessage.Caption
End Property
Public Property Let ShortMessage(ByVal vstrShortMessage As String)

    If lblShortMessage.Caption <> vstrShortMessage Then

        lblShortMessage.Caption = vstrShortMessage
        
        DoEvents
    End If
End Property

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Sub CloseForm()

    Unload Me
End Sub


