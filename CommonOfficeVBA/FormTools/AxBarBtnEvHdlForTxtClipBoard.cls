VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AxBarBtnEvHdlForTxtClipBoard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   ActiveX CommandBarButton control Event handlers for ClipBoard
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
'       Dependent on MSForms
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  8/Sun/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private WithEvents mobjTextBox As MSForms.TextBox
Attribute mobjTextBox.VB_VarHelpID = -1

Private WithEvents mobjCommandBarCopyButton As Office.CommandBarButton
Attribute mobjCommandBarCopyButton.VB_VarHelpID = -1
Private WithEvents mobjCommandBarCutButton As Office.CommandBarButton
Attribute mobjCommandBarCutButton.VB_VarHelpID = -1
Private WithEvents mobjCommandBarPasteButton As Office.CommandBarButton
Attribute mobjCommandBarPasteButton.VB_VarHelpID = -1

Private menmMenuClipBoardEnabledFlag As MenuClipBoardEnabledFlag

Private mobjApplication As Object ' Excel.Application


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub Class_Initialize()

    menmMenuClipBoardEnabledFlag = ClipBoardMenuAllEnabled
End Sub

Private Sub Class_Terminate()

    Set mobjTextBox = Nothing
    
    Set mobjCommandBarCopyButton = Nothing
    Set mobjCommandBarCutButton = Nothing
    Set mobjCommandBarPasteButton = Nothing
End Sub

'''
''' Set up Right click menu
'''
Private Sub mobjTextBox_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal x As Single, ByVal y As Single)

    
    If Button <> 2 Then Exit Sub
    
    
    With mobjApplication.CommandBars.Add(Position:=msoBarPopup)
    
        If (menmMenuClipBoardEnabledFlag And ClipBoardMenuCopyEnabled) > 0 Then
    
            Set mobjCommandBarCopyButton = .Controls.Add(msoControlButton)
            
            With mobjCommandBarCopyButton
            
                .Caption = GetTextOfStrKeyRclickOnAxCtlTextClipboardCopy() & "(&C)"
                .Enabled = mobjTextBox.SelLength > 0
            End With
        End If
    
        If (menmMenuClipBoardEnabledFlag And ClipBoardMenuCutEnabled) > 0 Then
        
            Set mobjCommandBarCutButton = .Controls.Add(msoControlButton)
        
            With mobjCommandBarCutButton
            
                .Caption = GetTextOfStrKeyRclickOnAxCtlTextClipboardCut() & "(&X)"
                .Enabled = mobjTextBox.SelLength > 0
            End With
        
        End If
    
        If (menmMenuClipBoardEnabledFlag And ClipBoardMenuPasteEnabled) > 0 Then
        
            Set mobjCommandBarPasteButton = .Controls.Add(msoControlButton)
            
            With mobjCommandBarPasteButton
            
                .Caption = GetTextOfStrKeyRclickOnAxCtlTextClipboardPaste() & "(&V)"
                .Enabled = mobjTextBox.CanPaste
            End With
        
        End If
    
        .ShowPopup
        
        .Delete
    End With

End Sub

'''
''' Copy text into ClipBoard
'''
Private Sub mobjCommandBarCopyButton_Click(ByVal Ctrl As Office.CommandBarButton, CancelDefault As Boolean)

    mobjTextBox.Copy
End Sub

'''
''' Cut text into ClipBoard
'''
Private Sub mobjCommandBarCutButton_Click(ByVal Ctrl As Office.CommandBarButton, CancelDefault As Boolean)

    mobjTextBox.Cut
End Sub

'''
''' Paste from ClipBoard
'''
Private Sub mobjCommandBarPasteButton_Click(ByVal Ctrl As Office.CommandBarButton, CancelDefault As Boolean)

    mobjTextBox.Paste
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
Public Sub Initialize(ByRef robjTextBox As MSForms.TextBox, ByRef robjApplication As Object, Optional ByVal venmMenuClipBoardEnabledFlag As MenuClipBoardEnabledFlag = MenuClipBoardEnabledFlag.ClipBoardMenuAllEnabled)

    Set mobjTextBox = robjTextBox
    
    Set mobjApplication = robjApplication

    menmMenuClipBoardEnabledFlag = venmMenuClipBoardEnabledFlag
End Sub



