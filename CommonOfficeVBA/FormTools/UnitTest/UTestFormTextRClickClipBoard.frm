VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UTestFormTextRClickClipBoard 
   Caption         =   "UTestFormTextClipBoard"
   ClientHeight    =   4400
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   3900
   OleObjectBlob   =   "UTestFormTextRClickClipBoard.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "UTestFormTextRClickClipBoard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   Sanity test for right-click Clip board operation menu test form
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'       Dependent on both the MSForms and the Office
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  8/Feb/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Declrations
'///////////////////////////////////////////////
Private mobjIndexToAxBarBtnEvHdlForTxtClipBoardDic As Scripting.Dictionary

Private mblnInitialized As Boolean


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
Private Sub UserForm_Initialize()

    Set mobjIndexToAxBarBtnEvHdlForTxtClipBoardDic = New Scripting.Dictionary
    

    
    txtClipBoardAll.Text = "All texts"
    
    txtClipBoardCopyOnly.Text = "Clip board copy only"

End Sub


Private Sub UserForm_Terminate()

    mobjIndexToAxBarBtnEvHdlForTxtClipBoardDic.RemoveAll
    
    Set mobjIndexToAxBarBtnEvHdlForTxtClipBoardDic = Nothing
End Sub


Private Sub UserForm_Layout()

    Dim objAxBarBtnEvHdlForTxtClipBoard As AxBarBtnEvHdlForTxtClipBoard

    If Not mblnInitialized Then
    
        With mobjIndexToAxBarBtnEvHdlForTxtClipBoardDic
        
            Set objAxBarBtnEvHdlForTxtClipBoard = New AxBarBtnEvHdlForTxtClipBoard
            
            objAxBarBtnEvHdlForTxtClipBoard.Initialize txtClipBoardAll, GetCurrentOfficeFileObject().Application, ClipBoardMenuAllEnabled
        
            .Add 0, objAxBarBtnEvHdlForTxtClipBoard
        
        
            Set objAxBarBtnEvHdlForTxtClipBoard = New AxBarBtnEvHdlForTxtClipBoard
            
            objAxBarBtnEvHdlForTxtClipBoard.Initialize txtClipBoardCopyOnly, GetCurrentOfficeFileObject().Application, ClipBoardMenuCopyEnabled
        
            .Add 1, objAxBarBtnEvHdlForTxtClipBoard
        End With
    
        mblnInitialized = True
    End If


End Sub


