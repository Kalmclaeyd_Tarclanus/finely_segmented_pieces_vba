Attribute VB_Name = "InputPasswordBox"
'
'   Methods for simply inputting password characters form
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on MSForms
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 27/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "InputPasswordBox"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mobjInputPasswordBoxForm As UInputPasswordBox

'**---------------------------------------------
'** Key-Value cache preparation for InputPasswordBox
'**---------------------------------------------
Private mobjStrKeyValueInputPasswordBoxDic As Object ' Scripting.Dictionary

Private mblnIsStrKeyValueInputPasswordBoxDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'''
'''
'''
Public Function InputPasswordSimpleBox(ByVal vstrPrompt As String, _
        Optional ByVal vstrTitle As String = "", _
        Optional ByVal vblnPasswordVisibleCheckBoxEnabled As Boolean = False, _
        Optional ByVal vblnFormTopRightCancelButtonDisabled As Boolean = False) As String

    Dim strPassword As String, strTitle As String, objResultCol As Collection
    
    
    If vstrTitle <> "" Then
    
        strTitle = vstrTitle
    Else
        strTitle = Application.Name
    End If
    
    strPassword = ""
    
    Set mobjInputPasswordBoxForm = New UInputPasswordBox
    
    Set objResultCol = New Collection
    
    With mobjInputPasswordBoxForm
    
        .Prompt = vstrPrompt
        
        .Title = strTitle
    
        .PasswordVisibleCheckBoxEnabled = vblnPasswordVisibleCheckBoxEnabled
    
        .FormTopRightCancelButtonDisabled = vblnFormTopRightCancelButtonDisabled
    
        Set .ResultCol = objResultCol
    
        .Show VBA.FormShowConstants.vbModal
        
        strPassword = objResultCol.Item(1)
    End With
    
    InputPasswordSimpleBox = strPassword
End Function

'''
''' general password authentication
'''
Public Function InputPasswordSimpleBoxWithGeneralMessages() As String

    InputPasswordSimpleBoxWithGeneralMessages = InputPasswordSimpleBox(GetTextOfStrKeyInputPasswordBoxGeneralPromptInputPassword(), GetTextOfStrKeyInputPasswordBoxGeneralTitleAuthenticationByPassword(), True, True)
End Function

'''
''' request a password to open Excel book
'''
Public Function InputPasswordSimpleBoxToOpenPasswordLockedExcelBook(ByVal vstrBookFileName As String, _
        Optional ByVal vblnPasswordVisibleCheckBoxEnabled As Boolean = False) As String

    Dim strPrompt As String
    
    strPrompt = GetTextOfStrKeyInputPasswordBoxGeneralPromptInputPassword() & vbNewLine & vstrBookFileName
    
    InputPasswordSimpleBoxToOpenPasswordLockedExcelBook = InputPasswordSimpleBox(strPrompt, GetTextOfStrKeyInputPasswordBoxTitleExcelBookPasswordAuthenticationToOpen(), vblnPasswordVisibleCheckBoxEnabled, True)
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToInputPasswordSimpleBox()

    Dim strPassword As String
    
    strPassword = InputPasswordSimpleBox("Test prompt", "Test title", False, True)
    
    'strPassword = InputPasswordSimpleBox("Test prompt", "")
    If strPassword <> "" Then
    
        Debug.Print strPassword
    End If
End Sub

'''
'''
'''
Private Sub msubSanityTestToInputPasswordSimpleBoxWithLocalizedCaption()

    Dim strPassword As String
    
    strPassword = InputPasswordSimpleBox(GetTextOfStrKeyInputPasswordBoxGeneralPromptInputPassword(), GetTextOfStrKeyInputPasswordBoxGeneralTitleAuthenticationByPassword(), True, True)
    
    'strPassword = InputPasswordSimpleBox("Test prompt", "")
    If strPassword <> "" Then
    
        Debug.Print strPassword
    End If
End Sub

'''
'''
'''
Private Sub msubSanityTestToInputPasswordSimpleBoxForExcelBook()

    Dim strPassword As String
    
    strPassword = InputPasswordSimpleBox("Input a password to open Book.", "Remove a password lock of the Book", True)
    
    Debug.Print strPassword
End Sub

'''
'''
'''
Private Sub msubSanityTestToInputPasswordSimpleBoxToOpenPasswordLockedExcelBook()

    Debug.Print InputPasswordSimpleBoxToOpenPasswordLockedExcelBook("SampleBook.xlsx")
End Sub


'**---------------------------------------------
'** Key-Value cache preparation for InputPasswordBox
'**---------------------------------------------
'''
''' get string Value from STR_KEY_INPUT_PASSWORD_BOX_GENERAL_TITLE_REMOVING_A_PASSWORD_LOCK
'''
''' automatically-added for InputPasswordBox string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInputPasswordBoxGeneralTitleRemovingAPasswordLock() As String

    If Not mblnIsStrKeyValueInputPasswordBoxDicInitialized Then

        msubInitializeTextForInputPasswordBox
    End If

    GetTextOfStrKeyInputPasswordBoxGeneralTitleRemovingAPasswordLock = mobjStrKeyValueInputPasswordBoxDic.Item("STR_KEY_INPUT_PASSWORD_BOX_GENERAL_TITLE_REMOVING_A_PASSWORD_LOCK")
End Function

'''
''' get string Value from STR_KEY_INPUT_PASSWORD_BOX_GENERAL_TITLE_AUTHENTICATION_BY_PASSWORD
'''
''' automatically-added for InputPasswordBox string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInputPasswordBoxGeneralTitleAuthenticationByPassword() As String

    If Not mblnIsStrKeyValueInputPasswordBoxDicInitialized Then

        msubInitializeTextForInputPasswordBox
    End If

    GetTextOfStrKeyInputPasswordBoxGeneralTitleAuthenticationByPassword = mobjStrKeyValueInputPasswordBoxDic.Item("STR_KEY_INPUT_PASSWORD_BOX_GENERAL_TITLE_AUTHENTICATION_BY_PASSWORD")
End Function

'''
''' get string Value from STR_KEY_INPUT_PASSWORD_BOX_GENERAL_PROMPT_INPUT_PASSWORD
'''
''' automatically-added for InputPasswordBox string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInputPasswordBoxGeneralPromptInputPassword() As String

    If Not mblnIsStrKeyValueInputPasswordBoxDicInitialized Then

        msubInitializeTextForInputPasswordBox
    End If

    GetTextOfStrKeyInputPasswordBoxGeneralPromptInputPassword = mobjStrKeyValueInputPasswordBoxDic.Item("STR_KEY_INPUT_PASSWORD_BOX_GENERAL_PROMPT_INPUT_PASSWORD")
End Function

'''
''' get string Value from STR_KEY_INPUT_PASSWORD_BOX_TITLE_EXCEL_BOOK_PASSWORD_AUTHENTICATION_TO_OPEN
'''
''' automatically-added for InputPasswordBox string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInputPasswordBoxTitleExcelBookPasswordAuthenticationToOpen() As String

    If Not mblnIsStrKeyValueInputPasswordBoxDicInitialized Then

        msubInitializeTextForInputPasswordBox
    End If

    GetTextOfStrKeyInputPasswordBoxTitleExcelBookPasswordAuthenticationToOpen = mobjStrKeyValueInputPasswordBoxDic.Item("STR_KEY_INPUT_PASSWORD_BOX_TITLE_EXCEL_BOOK_PASSWORD_AUTHENTICATION_TO_OPEN")
End Function

'''
''' get string Value from STR_KEY_INPUT_PASSWORD_BOX_CHK_VISIBLE_PASSWORD
'''
''' automatically-added for InputPasswordBox string-key-value management for standard module and class module
Public Function GetTextOfStrKeyInputPasswordBoxChkVisiblePassword() As String

    If Not mblnIsStrKeyValueInputPasswordBoxDicInitialized Then

        msubInitializeTextForInputPasswordBox
    End If

    GetTextOfStrKeyInputPasswordBoxChkVisiblePassword = mobjStrKeyValueInputPasswordBoxDic.Item("STR_KEY_INPUT_PASSWORD_BOX_CHK_VISIBLE_PASSWORD")
End Function

'''
''' Initialize Key-Values which values. Avoiding to use LocalizationADOConnector instance.
'''
''' automatically-added for InputPasswordBox string-key-value management for standard module and class module
Private Sub msubInitializeTextForInputPasswordBox()

    Dim objKeyToTextDic As Object ' Scripting.Dictionary

    If Not mblnIsStrKeyValueInputPasswordBoxDicInitialized Then

        Set objKeyToTextDic = CreateObject("Scripting.Dictionary") ' Scripting.Dictionary

        Select Case Application.LanguageSettings.LanguageID(msoLanguageIDUI)

            Case 1041 ' Japanese environment

                AddStringKeyValueForInputPasswordBoxByJapaneseValues objKeyToTextDic
            Case Else

                AddStringKeyValueForInputPasswordBoxByEnglishValues objKeyToTextDic
        End Select
        mblnIsStrKeyValueInputPasswordBoxDicInitialized = True
    End If

    Set mobjStrKeyValueInputPasswordBoxDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for InputPasswordBox key-values cache
'''
''' automatically-added for InputPasswordBox string-key-value management for standard module and class module
Private Sub AddStringKeyValueForInputPasswordBoxByEnglishValues(ByRef robjDic As Object)

    With robjDic

        .Add "STR_KEY_INPUT_PASSWORD_BOX_GENERAL_TITLE_REMOVING_A_PASSWORD_LOCK", "Removing a password lock"
        .Add "STR_KEY_INPUT_PASSWORD_BOX_GENERAL_TITLE_AUTHENTICATION_BY_PASSWORD", "Password authentication"
        .Add "STR_KEY_INPUT_PASSWORD_BOX_GENERAL_PROMPT_INPUT_PASSWORD", "Input the password"
        .Add "STR_KEY_INPUT_PASSWORD_BOX_TITLE_EXCEL_BOOK_PASSWORD_AUTHENTICATION_TO_OPEN", "Password authentication to open Excel book"
        .Add "STR_KEY_INPUT_PASSWORD_BOX_CHK_VISIBLE_PASSWORD", "Visible the password"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for InputPasswordBox key-values cache
'''
''' automatically-added for InputPasswordBox string-key-value management for standard module and class module
Private Sub AddStringKeyValueForInputPasswordBoxByJapaneseValues(ByRef robjDic As Object)

    With robjDic

        .Add "STR_KEY_INPUT_PASSWORD_BOX_GENERAL_TITLE_REMOVING_A_PASSWORD_LOCK", "パスワードロックの解除"
        .Add "STR_KEY_INPUT_PASSWORD_BOX_GENERAL_TITLE_AUTHENTICATION_BY_PASSWORD", "パスワード認証"
        .Add "STR_KEY_INPUT_PASSWORD_BOX_GENERAL_PROMPT_INPUT_PASSWORD", "パスワードを入力してください"
        .Add "STR_KEY_INPUT_PASSWORD_BOX_TITLE_EXCEL_BOOK_PASSWORD_AUTHENTICATION_TO_OPEN", "Excelブックを開くためにパスワードを入力"
        .Add "STR_KEY_INPUT_PASSWORD_BOX_CHK_VISIBLE_PASSWORD", "パスワードを表示"
    End With
End Sub

'''
''' Remove Keys for InputPasswordBox key-values cache
'''
''' automatically-added for InputPasswordBox string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueInputPasswordBox(ByRef robjDic As Object)

    With robjDic

        If .Exists("STR_KEY_INPUT_PASSWORD_BOX_GENERAL_TITLE_REMOVING_A_PASSWORD_LOCK") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_INPUT_PASSWORD_BOX_GENERAL_TITLE_REMOVING_A_PASSWORD_LOCK"
            .Remove "STR_KEY_INPUT_PASSWORD_BOX_GENERAL_TITLE_AUTHENTICATION_BY_PASSWORD"
            .Remove "STR_KEY_INPUT_PASSWORD_BOX_GENERAL_PROMPT_INPUT_PASSWORD"
            .Remove "STR_KEY_INPUT_PASSWORD_BOX_TITLE_EXCEL_BOOK_PASSWORD_AUTHENTICATION_TO_OPEN"
            .Remove "STR_KEY_INPUT_PASSWORD_BOX_CHK_VISIBLE_PASSWORD"
        End If
    End With
End Sub


