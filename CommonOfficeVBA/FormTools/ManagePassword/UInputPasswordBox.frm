VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UInputPasswordBox 
   Caption         =   "UInputPasswordBox"
   ClientHeight    =   2100
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   5010
   OleObjectBlob   =   "UInputPasswordBox.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "UInputPasswordBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'   Simple input password characters form
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on MSForms
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Tue, 27/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mblnPasswordVisibleCheckBoxEnabled As Boolean

Private mblnFormTopRightCancelButtonDisabled As Boolean


Private mblnInitialized As Boolean

Private mintInitializing As Long

' cache
Private mobjOutsideResultCol As Collection


'///////////////////////////////////////////////
'/// Event handlers
'///////////////////////////////////////////////
'''
'''
'''
Private Sub UserForm_Initialize()

    mblnInitialized = False
    
    mblnPasswordVisibleCheckBoxEnabled = False
    
    mblnFormTopRightCancelButtonDisabled = False
End Sub
'''
'''
'''
Private Sub UserForm_Layout()

    If Not mblnInitialized Then
    
        Localize
    
        If mblnPasswordVisibleCheckBoxEnabled Then
        
            chkVisiblePassword.Visible = True
        Else
            chkVisiblePassword.Visible = False
        End If
        
        With txtPrompt
        
            .Locked = True
        End With
        
        If mblnFormTopRightCancelButtonDisabled Then
        
            msubDisableFormTopRightCancelButton
        End If
        
        With txtInputPassword
        
            .IMEMode = fmIMEModeAlpha
        
            .SetFocus
        End With
    
        mblnInitialized = True
    End If
End Sub

'''
'''
'''
Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)

    If Not mobjOutsideResultCol Is Nothing Then
    
        If CloseMode Then
        
            mobjOutsideResultCol.Add txtInputPassword.Text
        Else
            mobjOutsideResultCol.Add ""
        End If
    End If
End Sub

'''
'''
'''
Private Sub chkVisiblePassword_Change()

    mintInitializing = mintInitializing + 1
    
    If chkVisiblePassword.Value Then
    
        txtInputPassword.PasswordChar = ""
    Else
        txtInputPassword.PasswordChar = "*"
    End If
    
    mintInitializing = mintInitializing - 1
End Sub

'''
'''
'''
Private Sub txtInputPassword_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)

    If mintInitializing = 0 Then
    
        If KeyCode = VBA.KeyCodeConstants.vbKeyReturn Then
        
            cmdOK_Click
        End If
    End If
End Sub

'''
'''
'''
Private Sub cmdOK_Click()

    Unload Me
End Sub

'///////////////////////////////////////////////
'/// Properties
'///////////////////////////////////////////////
'''
'''
'''
Public Property Get Prompt() As String

    Prompt = txtPrompt.Text
End Property
Public Property Let Prompt(ByVal vstrPrompt As String)

    txtPrompt.Text = vstrPrompt
End Property

'''
'''
'''
Public Property Let Title(ByVal vstrTitle As String)

    Me.Caption = vstrTitle
End Property

'''
'''
'''
Public Property Get PasswordVisibleCheckBoxEnabled() As Boolean

    PasswordVisibleCheckBoxEnabled = mblnPasswordVisibleCheckBoxEnabled
End Property
Public Property Let PasswordVisibleCheckBoxEnabled(ByVal vblnPasswordVisibleCheckBoxEnabled As Boolean)

    mblnPasswordVisibleCheckBoxEnabled = vblnPasswordVisibleCheckBoxEnabled
End Property

'''
'''
'''
Public Property Get FormTopRightCancelButtonDisabled() As Boolean

    FormTopRightCancelButtonDisabled = mblnFormTopRightCancelButtonDisabled
End Property
Public Property Let FormTopRightCancelButtonDisabled(ByVal vblnFormTopRightCancelButtonDisabled As Boolean)

    mblnFormTopRightCancelButtonDisabled = vblnFormTopRightCancelButtonDisabled
End Property

'''
'''
'''
Public Property Set ResultCol(ByRef robjResultCol As Collection)

    Set mobjOutsideResultCol = robjResultCol
End Property

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Sub Localize()

    If chkVisiblePassword.Visible Then

        chkVisiblePassword.Caption = GetTextOfStrKeyInputPasswordBoxChkVisiblePassword()
    End If
End Sub

'''
'''
'''
Private Sub msubDisableFormTopRightCancelButton()

    On Error Resume Next
    
    ' If this VB project doesn't include 'DisableFormTopRightCancelButton' (in ModifyFormsByWinAPI.bas), then the following is nothing to do without VBA syntax error.
    
    Application.Run "DisableFormTopRightCancelButton", Me
    
    On Error GoTo 0
End Sub
