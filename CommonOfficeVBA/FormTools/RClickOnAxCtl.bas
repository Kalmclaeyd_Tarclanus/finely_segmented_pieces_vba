Attribute VB_Name = "RClickOnAxCtl"
'
'   Richt-click context menu tools for User-forms
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'       Dependent on both the MSForms and the Office
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed,  8/Feb/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private Const mstrModuleName As String = "RClickOnAxCtl"

'**---------------------------------------------
'** Key-Value cache preparation for RClickOnAxCtl
'**---------------------------------------------
Private mobjStrKeyValueRClickOnAxCtlDic As Scripting.Dictionary

Private mblnIsStrKeyValueRClickOnAxCtlDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.



'///////////////////////////////////////////////
'/// Enumerations
'///////////////////////////////////////////////
'''
'''
'''
Public Enum MenuClipBoardEnabledFlag

    ClipBoardMenuCopyEnabled = &H1
    
    ClipBoardMenuCutEnabled = &H2
    
    ClipBoardMenuPasteEnabled = &H4
    
    
    ClipBoardMenuAllEnabled = ClipBoardMenuCopyEnabled Or ClipBoardMenuCutEnabled Or ClipBoardMenuPasteEnabled
End Enum



'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToRClickOnAxCtl()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next

    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UTfRClickOnAxCtl,AxBarBtnEvHdlForTxtClipBoard,UTestFormTextClipBoard"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Key-Value cache preparation for RClickOnAxCtl
'**---------------------------------------------
'''
''' get string Value from STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_COPY
'''
''' automatically-added for RClickOnAxCtl string-key-value management for standard module and class module
Public Function GetTextOfStrKeyRclickOnAxCtlTextClipboardCopy() As String

    If Not mblnIsStrKeyValueRClickOnAxCtlDicInitialized Then

        msubInitializeTextForRClickOnAxCtl
    End If

    GetTextOfStrKeyRclickOnAxCtlTextClipboardCopy = mobjStrKeyValueRClickOnAxCtlDic.Item("STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_COPY")
End Function

'''
''' get string Value from STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_CUT
'''
''' automatically-added for RClickOnAxCtl string-key-value management for standard module and class module
Public Function GetTextOfStrKeyRclickOnAxCtlTextClipboardCut() As String

    If Not mblnIsStrKeyValueRClickOnAxCtlDicInitialized Then

        msubInitializeTextForRClickOnAxCtl
    End If

    GetTextOfStrKeyRclickOnAxCtlTextClipboardCut = mobjStrKeyValueRClickOnAxCtlDic.Item("STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_CUT")
End Function

'''
''' get string Value from STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_PASTE
'''
''' automatically-added for RClickOnAxCtl string-key-value management for standard module and class module
Public Function GetTextOfStrKeyRclickOnAxCtlTextClipboardPaste() As String

    If Not mblnIsStrKeyValueRClickOnAxCtlDicInitialized Then

        msubInitializeTextForRClickOnAxCtl
    End If

    GetTextOfStrKeyRclickOnAxCtlTextClipboardPaste = mobjStrKeyValueRClickOnAxCtlDic.Item("STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_PASTE")
End Function

'''
''' Initialize Key-Values which values
'''
''' automatically-added for RClickOnAxCtl string-key-value management for standard module and class module
Private Sub msubInitializeTextForRClickOnAxCtl()

    Dim objKeyToTextDic As Scripting.Dictionary

    If Not mblnIsStrKeyValueRClickOnAxCtlDicInitialized Then

        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)

        If objKeyToTextDic Is Nothing Then

            ' If the Key-Value table file is lost, then the default setting is used

            Set objKeyToTextDic = New Scripting.Dictionary

            Select Case GetCurrentUICaptionLanguageType()

                Case CaptionLanguageType.UIJapaneseCaptions

                    AddStringKeyValueForRClickOnAxCtlByJapaneseValues objKeyToTextDic
                Case Else

                    AddStringKeyValueForRClickOnAxCtlByEnglishValues objKeyToTextDic
            End Select
        End If

        mblnIsStrKeyValueRClickOnAxCtlDicInitialized = True
    End If

    Set mobjStrKeyValueRClickOnAxCtlDic = objKeyToTextDic
End Sub

'''
''' Add Key-Values which values are in English for RClickOnAxCtl key-values cache
'''
''' automatically-added for RClickOnAxCtl string-key-value management for standard module and class module
Private Sub AddStringKeyValueForRClickOnAxCtlByEnglishValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_COPY", "Copy"
        .Add "STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_CUT", "Cut"
        .Add "STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_PASTE", "Paste"
    End With
End Sub

'''
''' Add Key-Values which values are in Japanese for RClickOnAxCtl key-values cache
'''
''' automatically-added for RClickOnAxCtl string-key-value management for standard module and class module
Private Sub AddStringKeyValueForRClickOnAxCtlByJapaneseValues(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        .Add "STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_COPY", "コピー"
        .Add "STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_CUT", "切り取り"
        .Add "STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_PASTE", "貼り付け"
    End With
End Sub

'''
''' Remove Keys for RClickOnAxCtl key-values cache
'''
''' automatically-added for RClickOnAxCtl string-key-value management for standard module and class module
Private Sub RemoveKeysOfKeyValueRClickOnAxCtl(ByRef robjDic As Scripting.Dictionary)

    With robjDic

        If .Exists("STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_COPY") Then   ' Check only the first key, otherwise omit items

            .Remove "STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_COPY"
            .Remove "STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_CUT"
            .Remove "STR_KEY_RCLICK_ON_AX_CTL_TEXT_CLIPBOARD_PASTE"
        End If
    End With
End Sub

