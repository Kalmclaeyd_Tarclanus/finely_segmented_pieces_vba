Attribute VB_Name = "ModifyFormsByWinAPI"
'
'   modify Window-style by Windows API
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is dependent on Windows API, which means this support only Windows OS
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 -2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 22/Mar/2017    Mr. vbabeginner         A part of the idea has been disclosed at https://vbabeginner.net/change-form-size-minimize-and-maximize/
'       Fri,  7/Jul/2017    Mr. keimado             A part of the idea has been disclosed at http://www2.aqua-r.tepm.jp/~kmado/ke13u003.html
'       Thu,  2/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////
Private Const mintFixedMininumOfItemsOfAdjustingHeightRange As Long = 2 ' 3


'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Windows API declarations
'**---------------------------------------------
Private Const GWL_STYLE = (-16)

Private Const WS_SYSMENU = &H80000

Private Const WS_MAXIMIZEBOX = &H10000

Private Const WS_MINIMIZEBOX = &H20000

Private Const WS_THICKFRAME = &H40000   ' flag to change Window-style allowed to resize MSForms.UserForm


#If VBA7 Then
    ' For 64 bit version windows, declare timeGetTime
    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
    
    
    Private Declare PtrSafe Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As LongPtr, ByVal nIndex As Long) As Long
    
    Private Declare PtrSafe Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As LongPtr, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
    
    Private Declare PtrSafe Function GetActiveWindow Lib "user32" () As LongPtr
    
    Private Declare PtrSafe Function WindowFromObject Lib "oleacc" Alias "WindowFromAccessibleObject" (ByVal pacc As Object, phwnd As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function DrawMenuBar Lib "user32" (ByVal hwnd As LongPtr) As Long
    
    ' For DisableFormTopRightCancelButton
    Private Declare PtrSafe Function GetWindowLongPtr Lib "user32" Alias "GetWindowLongPtrA" (ByVal hwnd As LongPtr, ByVal nIndex As Long) As LongPtr
    
    Private Declare PtrSafe Function SetWindowLongPtr Lib "user32" Alias "SetWindowLongPtrA" (ByVal hwnd As LongPtr, ByVal nIndex As Long, ByVal dwNewLong As LongPtr) As LongPtr
    
    Private Declare PtrSafe Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As LongPtr
#Else
    ' For 32 bit version windows, declare timeGetTime
    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
    
    
    Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
    
    Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
    
    Private Declare Function GetActiveWindow Lib "user32" () As Long
    
    Private Declare Function WindowFromObject Lib "oleacc" Alias "WindowFromAccessibleObject" (ByVal pacc As Object, phwnd As Long) As Long
    
    Private Declare Function DrawMenuBar Lib "user32" (ByVal hwnd As Long) As Long
    
    ' For DisableFormTopRightCancelButton
    Private Declare Function GetWindowLongPtr Lib "user32" Alias "GetWindowLongPtrA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
    
    Private Declare Function SetWindowLongPtr Lib "user32" Alias "SetWindowLongPtrA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
    
    Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
#End If

'**---------------------------------------------
'** resizing control user defined-types
'**---------------------------------------------
Public Type ResizingAxControlSize

    DefaultWidth As Long
    
    DefaultHeight As Long
    
    DefaultRightDistance As Long
    
    DefaultBottomDistance As Long
End Type

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Delete form cancel button
'**---------------------------------------------
'''
''' delete the cancel (X) button at top-right of the UserForm
'''
''' <Argument>robjForm</Argument>
Public Sub DisableFormTopRightCancelButton(ByRef robjForm As Object)

#If VBA7 Then
    Dim intCurrentHWnd As LongPtr, intWindowStyle As LongPtr
#Else
    Dim intCurrentHWnd As Long, intWindowStyle As Long
#End If
    intCurrentHWnd = 0
    
    intCurrentHWnd = FindWindow("ThunderDFrame", robjForm.Caption)
    
    If intCurrentHWnd <> 0 Then
    
        intWindowStyle = GetWindowLongPtr(intCurrentHWnd, GWL_STYLE)
    
        SetWindowLongPtr intCurrentHWnd, GWL_STYLE, intWindowStyle And Not WS_SYSMENU
    
        DrawMenuBar intCurrentHWnd
    End If
End Sub


'**---------------------------------------------
'** Adjust a height of either a ListBox and a ListView
'**---------------------------------------------
'''
'''
'''
Public Sub AdjustHeightByCountOfItemsWithFixedMinimum(ByRef robjControl As Object, _
        ByVal vintCurrentCountOfItems As Long, _
        ByVal vintMaximumOfItemsOfAdjustingHeightRange As Long, _
        Optional ByVal vblnIsListViewControl As Boolean = True)


    AdjustHeightByCountOfItems robjControl, _
            vintCurrentCountOfItems, _
            mintFixedMininumOfItemsOfAdjustingHeightRange, _
            vintMaximumOfItemsOfAdjustingHeightRange, _
            vblnIsListViewControl
End Sub


'''
''' Adjust a height by the count of the listed items
'''
Public Sub AdjustHeightByCountOfItems(ByRef robjControl As Object, _
        ByVal vintCurrentCountOfItems As Long, _
        ByVal vintMininumOfItemsOfAdjustingHeightRange As Long, _
        ByVal vintMaximumOfItemsOfAdjustingHeightRange As Long, _
        Optional ByVal vblnIsListViewControl As Boolean = True)


    Dim intCountToDecideHeight As Long, intHeight As Long, intHeightUnit As Long
    
    intHeightUnit = 11  ' 12

    intCountToDecideHeight = GetCountToDecideHeight(vintCurrentCountOfItems, _
            vintMininumOfItemsOfAdjustingHeightRange, _
            vintMaximumOfItemsOfAdjustingHeightRange)

    If vblnIsListViewControl Then
    
        intHeight = (intCountToDecideHeight + 1) * intHeightUnit
    Else
        intHeight = intCountToDecideHeight * intHeightUnit
    End If

    robjControl.Height = intHeight
End Sub


'''
'''
'''
Private Function GetCountToDecideHeight(ByVal vintCurrentCountOfItems As Long, _
        ByVal vintMinimumOfItemsOfAdjustingHeightRange As Long, _
        ByVal vintMaximumOfItemsOfAdjustingHeightRange As Long) As Long

    Dim intCountToDecideHeight As Long
    
    Select Case True
    
        Case vintMinimumOfItemsOfAdjustingHeightRange > vintCurrentCountOfItems
    
            intCountToDecideHeight = vintMinimumOfItemsOfAdjustingHeightRange
            
        Case vintMaximumOfItemsOfAdjustingHeightRange < vintCurrentCountOfItems
        
            intCountToDecideHeight = vintMaximumOfItemsOfAdjustingHeightRange
        Case Else
        
            intCountToDecideHeight = vintCurrentCountOfItems
    End Select
    
    GetCountToDecideHeight = intCountToDecideHeight
End Function


'**---------------------------------------------
'** Change the user form Window-style by Windows API
'**---------------------------------------------
'''
'''
'''
Public Sub ModifyFormStyle(ByRef robjUserForm As MSForms.UserForm)

#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    WindowFromObject robjUserForm, intHWnd
    
    SetWindowLong intHWnd, GWL_STYLE, GetWindowLong(intHWnd, GWL_STYLE) Or WS_MAXIMIZEBOX Or WS_MINIMIZEBOX Or WS_THICKFRAME
    
    DrawMenuBar intHWnd
End Sub


'**---------------------------------------------
'** Adjust Axtive-X control size and position
'**---------------------------------------------
'''
'''
'''
Public Sub AdjustControlSizeAlongWithFormBottomRight(ByRef rudtResizingAxControlSize As ResizingAxControlSize, _
        ByRef robjForm As Object, _
        ByRef robjControl As Object)

    AdjustControlWidthAlongWithFormBottomRight rudtResizingAxControlSize, robjForm, robjControl
    
    AdjustControlHeightAlongWithFormBottomRight rudtResizingAxControlSize, robjForm, robjControl
End Sub


'''
'''
'''
Public Sub AdjustControlWidthAlongWithFormBottomRight(ByRef rudtResizingAxControlSize As ResizingAxControlSize, _
        ByRef robjForm As Object, _
        ByRef robjControl As Object)

    Dim intControlWidth As Long
    
    With robjControl
    
        intControlWidth = robjForm.Width - rudtResizingAxControlSize.DefaultRightDistance - .Left
    
        If intControlWidth > rudtResizingAxControlSize.DefaultWidth Then
        
            .Width = intControlWidth
        Else
            .Width = rudtResizingAxControlSize.DefaultWidth
        End If
    End With
End Sub

'''
'''
'''
Public Sub AdjustControlHeightAlongWithFormBottomRight(ByRef rudtResizingAxControlSize As ResizingAxControlSize, _
        ByRef robjForm As Object, _
        ByRef robjControl As Object)

    Dim intControlHeight As Long
    
    With robjControl
    
        intControlHeight = robjForm.Height - rudtResizingAxControlSize.DefaultBottomDistance - .Top
        
        If intControlHeight > rudtResizingAxControlSize.DefaultHeight Then
        
            .Height = intControlHeight
        Else
            .Height = rudtResizingAxControlSize.DefaultHeight
        End If
    End With
End Sub

'''
'''
'''
Public Sub AdjustControlTopLeftPositionAlongWithFormBottomRight(ByRef rudtResizingAxControlSize As ResizingAxControlSize, _
        ByRef robjForm As Object, _
        ByRef robjControl As Object)

    Dim intControlLeft As Long, intControlTop As Long
    
    With robjControl
    
        intControlLeft = robjForm.Width - rudtResizingAxControlSize.DefaultRightDistance - rudtResizingAxControlSize.DefaultWidth
    
        intControlTop = robjForm.Height - rudtResizingAxControlSize.DefaultBottomDistance - rudtResizingAxControlSize.DefaultHeight
        
        .Left = intControlLeft
        
        .Top = intControlTop
    End With
End Sub


'''
''' solute by interface
'''
Public Sub GetDefaultResizingAxControlSize(ByRef rudtResizingAxControlSize As ResizingAxControlSize, _
        ByRef robjForm As Object, _
        ByRef robjControl As Object)

    With robjControl
    
        rudtResizingAxControlSize.DefaultWidth = .Width
        
        rudtResizingAxControlSize.DefaultHeight = .Height
        
        rudtResizingAxControlSize.DefaultRightDistance = robjForm.Width - .Left - .Width
        
        rudtResizingAxControlSize.DefaultBottomDistance = robjForm.Height - .Top - .Height
    End With
End Sub

