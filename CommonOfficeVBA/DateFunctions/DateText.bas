Attribute VB_Name = "DateText"
'
'   date text processing functions
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon,  6/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////

Private mobjSimpleNumberRegExp As VBScript_RegExp_55.RegExp

Private mobjDateContinuousNumberRegExp As VBScript_RegExp_55.RegExp


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Set up RegExp
'**---------------------------------------------
'''
'''
'''
Public Function GetSimpleNumberRegExp() As VBScript_RegExp_55.RegExp

    If mobjSimpleNumberRegExp Is Nothing Then
    
        Set mobjSimpleNumberRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjSimpleNumberRegExp
        
            .Pattern = "[0-9]{1,}"
        End With
    End If

    Set GetSimpleNumberRegExp = mobjSimpleNumberRegExp
End Function

'''
'''
'''
Public Function GetDateContinuousNumberRegExp() As VBScript_RegExp_55.RegExp

    If mobjDateContinuousNumberRegExp Is Nothing Then
    
        Set mobjDateContinuousNumberRegExp = New VBScript_RegExp_55.RegExp
        
        With mobjDateContinuousNumberRegExp
    
            .Pattern = "([0-9]{4})([0-9]{2})([0-9]{2})[_\-]{1}([0-9]{4})([0-9]{2})([0-9]{2})"
            
            .Global = True
        End With
    End If

    Set GetDateContinuousNumberRegExp = mobjDateContinuousNumberRegExp
End Function


'**---------------------------------------------
'** Japanese era, such as �ߘa... conversion
'**---------------------------------------------
'''
'''
'''
Public Function GetJapaneseEraCalenderYear(ByVal vintYear As Long, ByVal vintMonth As Long) As Long

    Dim udtDate As Date, intJapaneseEraYear As Long, objMatches As VBScript_RegExp_55.MatchCollection, objMatch As VBScript_RegExp_55.Match
    
    udtDate = DateSerial(vintYear, vintMonth, 1)
    
    With GetSimpleNumberRegExp
    
        Set objMatches = .Execute(Format(udtDate, "ggge"))
        
        Set objMatch = objMatches.Item(0)
        
        intJapaneseEraYear = CLng(objMatch.Value)
    End With
    
    GetJapaneseEraCalenderYear = intJapaneseEraYear
End Function


'''
''' set padding two digits
'''
Public Function PadStringLeftZero2Digits(ByVal vintNum As Integer, Optional ByVal vstrPadChar As String = "0") As String
    
    Dim strNumber As String
    
    If vintNum < 10 Then
        
        strNumber = vstrPadChar & CStr(vintNum)
    Else
        strNumber = CStr(vintNum)
    End If
    
    PadStringLeftZero2Digits = strNumber
End Function


Public Function GetAFormattedDateString(ByVal vobjDateTime As Date) As String
    
    GetAFormattedDateString = Year(vobjDateTime) & PadStringLeftZero2Digits(Month(vobjDateTime)) & PadStringLeftZero2Digits(Day(vobjDateTime))
End Function

Public Function GetAFormattedTimeString(ByVal vobjDateTime As Date) As String
    
    GetAFormattedTimeString = PadStringLeftZero2Digits(Hour(vobjDateTime)) & PadStringLeftZero2Digits(Minute(vobjDateTime)) & PadStringLeftZero2Digits(Second(vobjDateTime))
End Function


'''
'''
'''
Public Sub GetPeriodFromFormatedTestByContinuos8Digits(ByVal vstrText As String, ByRef rstdFrom As Date, ByRef rstdTo As Date)

    If IsEmpty(vstrText) Then
        
        Err.Raise vbObjectError + 2, "LoadAndModifyModule.GetPeriodFromFormatedTestByContinuos8Digit...", "text is nothing"
    End If
    
    If vstrText = "" Then
        
        Err.Raise vbObjectError + 3, "LoadAndModifyModule.GetPeriodFromFormatedTestByContinuos8Digit...", "text is void string"
    End If

    Dim objMatch As VBScript_RegExp_55.Match, objSubMatches As VBScript_RegExp_55.SubMatches
    
    Dim intFromYear As Long, intFromMonth As Long, intFromDay As Long
    Dim intToYear As Long, intToMonth As Long, intToDay As Long
    
    Dim udtFrom As Date, udtTo As Date, udtTmp As Date
    
    
    With GetDateContinuousNumberRegExp()
        
        If .Test(vstrText) Then
            
            For Each objMatch In .Execute(vstrText)
            
                Set objSubMatches = objMatch.SubMatches
                
                If objSubMatches.Count > 0 Then
                
                    With objSubMatches
                        
                        intFromYear = CLng(.Item(0)): intFromMonth = CLng(.Item(1)): intFromDay = CLng(.Item(2))
                    
                        intToYear = CLng(.Item(3)): intToMonth = CLng(.Item(4)): intToDay = CLng(.Item(5))
                    
                        udtFrom = DateSerial(intFromYear, intFromMonth, intFromDay)
                        
                        udtTo = DateSerial(intToYear, intToMonth, intToDay)
                        
                        If DateDiff("d", udtFrom, udtTo) < 0 Then
                            
                            udtTmp = udtFrom
                            
                            udtFrom = udtTo
                            
                            udtTo = udtTmp
                        End If
                        
                        rstdFrom = udtFrom
                        
                        rstdTo = udtTo
                    End With
                End If
            Next
        Else
            Err.Raise vbObjectError + 1, "DateText.GetPeriodFromFormatedTestByContinuos8Digit...", "Not match the regular-expression. " & vbNewLine & "Source string: " & vstrText & vbNewLine & "Pattern: " & .Pattern
        End If
    End With
End Sub


Public Function mfstrConvertDateTimeFormat(ByVal vintSecond As Long) As String
    
    Dim strElapsedTime As String

    If Fix(vintSecond / 3600) > 0 Then
            
        strElapsedTime = CStr(Fix(vintSecond / 3600)) & " [h] " & CStr(Fix((vintSecond Mod 3600) / 60)) & " [min] " & CStr((vintSecond Mod 3600) Mod 60) & " [s]"
    
    ElseIf Fix(vintSecond / 60) > 0 Then
    
        strElapsedTime = CStr(Fix(vintSecond / 60)) & " [min] " & CStr(vintSecond Mod 60) & " [s]"
    Else
        strElapsedTime = CStr(vintSecond) & " [s]"
    End If

    mfstrConvertDateTimeFormat = strElapsedTime
End Function


Public Function FormatDateAndWeekdayJapanese(ByVal vudtDate As Date) As String

    FormatDateAndWeekdayJapanese = FormatDateTime(vudtDate, vbLongDate) & "(" & WeekdayJapanese(vudtDate) & ")"
End Function

'''
''' Such as formatted [Tue, 17/Jan/2023 16:02:31]
'''
Public Function FormatDateTimeWithWeekdayByEnglish(ByVal vudtDate As Date) As String

    FormatDateTimeWithWeekdayByEnglish = WeekdayEnglishShort3Chars(vudtDate) & ", " & PadStringLeftZero2Digits(Day(vudtDate)) & "/" & MonthEnglishShort3Chars(vudtDate) & "/" & CStr(Year(vudtDate)) & " " & FormatDateTime(vudtDate, vbLongTime)
End Function


Public Function FormatPluralDatesWithWeekdayJapanese(ByVal vobjDateCol As Collection) As String

    Dim varItem As Variant, udtDate As Date, i As Long, strDates As String
    
    i = 1
    
    strDates = ""
    
    For Each varItem In vobjDateCol
    
        udtDate = varItem
    
        strDates = strDates & FormatDateAndWeekdayJapanese(udtDate)
        
        If i < vobjDateCol.Count Then
        
            strDates = strDates & ", "
        End If
    
        i = i + 1
    Next
    
    FormatPluralDatesWithWeekdayJapanese = strDates
End Function



'''
''' get Japanese weekday character, but you can also use Format(vudtDate, "aaa")
'''
Public Function WeekdayJapanese(ByVal vudtDate As Date) As String
    
    Dim strWeekday As String
    
    strWeekday = ""
    
    Select Case Weekday(vudtDate)
        
        Case VbDayOfWeek.vbSunday
        
            strWeekday = "��"
        
        Case VbDayOfWeek.vbMonday
        
            strWeekday = "��"
            
        Case VbDayOfWeek.vbTuesday
        
            strWeekday = "��"
            
        Case VbDayOfWeek.vbWednesday
        
            strWeekday = "��"
            
        Case VbDayOfWeek.vbThursday
        
            strWeekday = "��"
            
        Case VbDayOfWeek.vbFriday
        
            strWeekday = "��"
            
        Case VbDayOfWeek.vbSaturday
        
            strWeekday = "�y"
    End Select

    WeekdayJapanese = strWeekday
End Function

'''
''' get English weekday character
'''
Public Function WeekdayEnglishFull(ByVal vudtDate As Date) As String
    
    Dim strWeekday As String
    
    strWeekday = ""
    
    Select Case Weekday(vudtDate)
        
        Case VbDayOfWeek.vbSunday
        
            strWeekday = "Sunday"
            
        Case VbDayOfWeek.vbMonday
        
            strWeekday = "Monday"
            
        Case VbDayOfWeek.vbTuesday
        
            strWeekday = "Tuesday"
            
        Case VbDayOfWeek.vbWednesday
        
            strWeekday = "Wednesday"
            
        Case VbDayOfWeek.vbThursday
        
            strWeekday = "Thursday"
            
        Case VbDayOfWeek.vbFriday
        
            strWeekday = "Friday"
            
        Case VbDayOfWeek.vbSaturday
        
            strWeekday = "Saturday"
    End Select

    WeekdayEnglishFull = strWeekday
End Function

'''
''' get English weekday character, but you can also use Format(vudtDate, "ddd")
'''
Public Function WeekdayEnglishShort3Chars(ByVal vudtDate As Date) As String
    
    Dim strWeekday As String
    
    strWeekday = ""
    
    Select Case Weekday(vudtDate)
        
        Case VbDayOfWeek.vbSunday
        
            strWeekday = "Sun"
            
        Case VbDayOfWeek.vbMonday
        
            strWeekday = "Mon"
            
        Case VbDayOfWeek.vbTuesday
        
            strWeekday = "Tue"
            
        Case VbDayOfWeek.vbWednesday
        
            strWeekday = "Wed"
            
        Case VbDayOfWeek.vbThursday
        
            strWeekday = "Thu"
            
        Case VbDayOfWeek.vbFriday
        
            strWeekday = "Fri"
            
        Case VbDayOfWeek.vbSaturday
        
            strWeekday = "Sat"
    End Select

    WeekdayEnglishShort3Chars = strWeekday
End Function

'''
''' get English month string by short three characters
'''
Public Function MonthEnglishShort3Chars(ByVal vudtDate As Date) As String
    
    Dim str3CharsMonth As String
    
    str3CharsMonth = ""
    
    Select Case Month(vudtDate)
        Case 1
        
            str3CharsMonth = "Jan"
        Case 2
        
            str3CharsMonth = "Feb"
        Case 3
        
            str3CharsMonth = "Mar"
        Case 4
        
            str3CharsMonth = "Apr"
        Case 5
        
            str3CharsMonth = "May"
        Case 6
        
            str3CharsMonth = "Jun"
        Case 7
        
            str3CharsMonth = "Jul"
        Case 8
        
            str3CharsMonth = "Aug"
        Case 9
        
            str3CharsMonth = "Sep"
        Case 10
        
            str3CharsMonth = "Oct"
        Case 11
        
            str3CharsMonth = "Nov"
        Case 12
        
            str3CharsMonth = "Dec"
    End Select

    MonthEnglishShort3Chars = str3CharsMonth
End Function

'''
''' get month value from English short three characters
'''
Public Function GetMonthValueFromEnglish3CharsMonth(ByRef rstrMonth As String) As Long

    Dim intMonth As Long
    
    intMonth = 0

    Select Case LCase(rstrMonth)
    
        Case "jan"
    
            intMonth = 1
            
        Case "feb"
    
            intMonth = 2
            
        Case "mar"
    
            intMonth = 3
        Case "apr"
    
            intMonth = 4
            
        Case "may"
        
            intMonth = 5
            
        Case "jun"
    
            intMonth = 6
            
        Case "jul"
    
            intMonth = 7
    
        Case "aug"
    
            intMonth = 8
    
        Case "sep"
    
            intMonth = 9
    
        Case "oct"
        
            intMonth = 10
        
        Case "nov"
        
            intMonth = 11
            
        Case "dec"
        
            intMonth = 12
    End Select
    
    GetMonthValueFromEnglish3CharsMonth = intMonth
End Function


'''
''' get natural number of the order of weeks on each month. When the first day of the month is between Thursday and Sunday, the getting value is zero (0).
'''
Public Function GetWeekSequenceNumber(ByVal vudtDate As Date, Optional ByVal venmCountingThreshold As VbDayOfWeek = VbDayOfWeek.vbThursday, Optional ByVal venmFirstDayOfWeek As VbDayOfWeek = VbDayOfWeek.vbMonday) As Long
    
    Dim udtFirstDay As Date, intCountOfWeek As Long
    
    intCountOfWeek = Int(Day(vudtDate) / 7#)

    udtFirstDay = DateSerial(Year(vudtDate), Month(vudtDate), 1)
    
    If Weekday(udtFirstDay, venmFirstDayOfWeek) < Weekday(udtFirstDay, venmCountingThreshold) Then
    
        intCountOfWeek = intCountOfWeek + 1
    End If

    GetWeekSequenceNumber = intCountOfWeek
End Function


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' Testing Application.Wait
'''
Private Sub msubSanityTestOfTimeDiff()

    Dim udtTime1 As Date, udtTime2 As Date, blnNoErrorOccured As Boolean

    blnNoErrorOccured = True

    udtTime1 = Now()
    
    ' Application.Wait is only Excel.Appliation object member function
    
    On Error Resume Next
    
    VBA.CallByName Application, "Wait", VbMethod, Now + TimeValue("0:00:02")

    If Err.Number <> 0 Then
    
        ' When Word.Application or PowerPoint.Application should have been failed...
    
        blnNoErrorOccured = False
    End If

    On Error GoTo 0
    
    udtTime2 = Now()
    
    Debug.Print CStr(DateDiff("s", udtTime1, udtTime2)) ' Plus value
    
    If blnNoErrorOccured Then Debug.Assert DateDiff("s", udtTime1, udtTime2) > 0
    
    Debug.Print CStr(DateDiff("s", udtTime2, udtTime1)) ' minus value
    
    If blnNoErrorOccured Then Debug.Assert DateDiff("s", udtTime2, udtTime1) < 0
End Sub

