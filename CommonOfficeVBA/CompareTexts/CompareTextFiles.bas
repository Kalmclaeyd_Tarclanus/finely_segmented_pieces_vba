Attribute VB_Name = "CompareTextFiles"
'
'   compare two text-files, such as source files
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri,  2/Jun/2023    Kalmclaeyd Tarclanus    Separated from CodeAnalysisVBA.bas
'

Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = True

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** FileSystemObject dependency
'**---------------------------------------------
'''
''' simple diff, simple practical code
'''
''' <Argument>vstrTextFile01Path: Input</Argument>
''' <Argument>vstrTextFile02Path: Input</Argument>
''' <Return>Boolean</Return>
Public Function IsDifferentBetweenTextFilesByFileSystemObject(ByVal vstrTextFile01Path As String, ByVal vstrTextFile02Path As String) As Boolean
    
    Dim strAllText01 As String, strAllText02 As String

    msubLoadTwoStringsFromEachTextFiles strAllText01, strAllText02, vstrTextFile01Path, vstrTextFile02Path

    IsDifferentBetweenTextFilesByFileSystemObject = IsDifferentBetweenTwoTextsByFileSystemObject(strAllText01, strAllText02)
End Function

'''
'''
'''
Public Function IsDifferentBetweenTwoTextsByFileSystemObject(ByRef rstrText01 As String, ByRef rstrText02 As String) As Boolean

    If rstrText01 = "" And rstrText02 = "" Then

        IsDifferentBetweenTwoTextsByFileSystemObject = False
    Else
        If InStr(rstrText01, rstrText02) = 0 Or InStr(rstrText02, rstrText01) = 0 Then
        
            IsDifferentBetweenTwoTextsByFileSystemObject = True
        Else
            IsDifferentBetweenTwoTextsByFileSystemObject = False
        End If
    End If
End Function



'''
''' simple diff, which Mr. gontarosuke suggest, and re-design it
'''
''' <Argument>rstrDiffLines: Output</Argument>
''' <Argument>vstrTextFile01Path: Input</Argument>
''' <Argument>vstrTextFile02Path: Input</Argument>
''' <Return>Boolean</Return>
Public Function DoesDiffentLinesBetweenTwoTextFilesExistUsingFSO(ByRef rstrDiffLines As String, ByVal vstrTextFile01Path As String, ByVal vstrTextFile02Path As String) As Boolean

    Dim strAllText01 As String, strAllText02 As String
    
    msubLoadTwoStringsFromEachTextFiles strAllText01, strAllText02, vstrTextFile01Path, vstrTextFile02Path
    
    DoesDiffentLinesBetweenTwoTextFilesExistUsingFSO = DoesDiffentLinesBetweenTwoTextsExistUsingFSO(rstrDiffLines, strAllText01, strAllText02)
End Function


'''
''' simple diff between two texts
'''
''' <Argument>rstrDiffLines: Output</Argument>
''' <Argument>rstrText01: Input</Argument>
''' <Argument>rstrText02: Input</Argument>
''' <Return>Boolean</Return>
Public Function DoesDiffentLinesBetweenTwoTextsExistUsingFSO(ByRef rstrDiffLines As String, ByRef rstrText01 As String, ByRef rstrText02 As String) As Boolean

    Dim intLine01IndexMax As Long, intLine02IndexMax As Long
    Dim intCurrentLine01Index As Long, intCurrentLine02Index As Long
    Dim intNextLine01Idx As Long, intNextLine02Idx As Long
    
    Dim strText01Array() As String, strText02Array() As String
    Dim strCurrentLine01 As String, strCurrentLine02 As String
    Dim blnIsFinishedScanningSrc01 As Boolean, blnIsFinishedScanningSrc02 As Boolean
    
    Dim strDiff As String, strRemainedDiff As String, blnDiffExists As Boolean
    Dim i As Long
    
    
    blnDiffExists = False
    
    strText01Array = Split(rstrText01, vbCrLf): strText02Array = Split(rstrText02, vbCrLf)
    
    intCurrentLine01Index = 0: intCurrentLine02Index = 0


    
    intLine01IndexMax = UBound(strText01Array): intLine02IndexMax = UBound(strText02Array)
    
    blnIsFinishedScanningSrc01 = False: blnIsFinishedScanningSrc02 = False
    
    strDiff = ""
    
    strRemainedDiff = ""
    
    Do Until blnIsFinishedScanningSrc01 And blnIsFinishedScanningSrc02
    
        If intCurrentLine01Index <= intLine01IndexMax Then
            
            strCurrentLine01 = strText01Array(intCurrentLine01Index)
        End If
        
        If intCurrentLine02Index <= intLine02IndexMax Then
            
            strCurrentLine02 = strText02Array(intCurrentLine02Index)
        End If
        
        If intCurrentLine01Index > intLine01IndexMax And intCurrentLine02Index <= intLine02IndexMax Then
            
            For i = intCurrentLine02Index To intLine02IndexMax
                
                If strDiff <> "" Then
                
                    strDiff = strDiff & vbNewLine
                End If
                
                strDiff = strDiff & "> " & strText02Array(i)
            Next
            
            intCurrentLine02Index = intLine02IndexMax + 1
        End If
        
        If intCurrentLine01Index <= intLine01IndexMax And intCurrentLine02Index > intLine02IndexMax Then
            
            For i = intCurrentLine01Index To intLine01IndexMax
                
                If strDiff <> "" Then
                
                    strDiff = strDiff & vbNewLine
                End If
                
                strDiff = strDiff & "< " & strText01Array(i)
            Next
            
            intCurrentLine01Index = intLine01IndexMax + 1
        End If
        
        If intCurrentLine01Index <= intLine01IndexMax And intCurrentLine02Index <= intLine02IndexMax Then
            
            If StrComp(strCurrentLine01, strCurrentLine02) <> 0 Then
                
                If intCurrentLine02Index <= intLine02IndexMax Then
                    
                    intNextLine02Idx = -1
                
                    For i = intCurrentLine02Index + 1 To intLine02IndexMax
                    
                        If StrComp(strCurrentLine01, strText02Array(i)) = 0 Then
                        
                            intNextLine02Idx = i
                            
                            Exit For
                        End If
                    Next
                    
                    If intNextLine02Idx > 0 Then
                    
                        intCurrentLine01Index = intCurrentLine01Index + 1
                        
                        For i = intCurrentLine02Index To intNextLine02Idx - 1
                        
                            If strDiff <> "" Then
                
                                strDiff = strDiff & vbNewLine
                            End If
                        
                            strDiff = strDiff & "> " & strText02Array(i)
                        Next
                    
                        intCurrentLine02Index = intNextLine02Idx + 1
                    Else
                    
                        If intCurrentLine01Index + 1 <= intLine01IndexMax Then
                        
                            If strDiff <> "" Then
                
                                strDiff = strDiff & vbNewLine
                            End If
                        
                            strDiff = strDiff & "< " & strCurrentLine01
                            
                            intCurrentLine01Index = intCurrentLine01Index + 1
                        End If
                        
                        If strDiff <> "" Then
                
                            strDiff = strDiff & vbNewLine
                        End If
                        
                        strDiff = strDiff & "> " & strCurrentLine02
                        
                        intCurrentLine02Index = intCurrentLine02Index + 1
                    End If
                End If
            
                If StrComp(strDiff, strRemainedDiff) = 0 Then
                    
                    If intCurrentLine01Index + 1 <= intLine01IndexMax Then
                        
                        intNextLine01Idx = -1
                        
                        For i = intCurrentLine01Index + 1 To intLine01IndexMax
                            
                            If StrComp(strCurrentLine02, strText01Array(i)) = 0 Then
                            
                                intNextLine01Idx = i
                                
                                Exit For
                            End If
                        Next
                            
                        If intNextLine01Idx > 0 Then
                            
                            intCurrentLine02Index = intCurrentLine02Index + 1
                            
                            For i = intCurrentLine01Index To intNextLine01Idx - 1
                            
                                If strDiff <> "" Then
                
                                    strDiff = strDiff & vbNewLine
                                End If
                            
                                strDiff = strDiff & "< " & strText01Array(i)
                            Next
                            
                            intCurrentLine01Index = intNextLine01Idx + 1
                        Else
                            
                            If strDiff <> "" Then
                
                                strDiff = strDiff & vbNewLine
                            End If
                            
                            strDiff = strDiff & "< " & strCurrentLine01
                            
                            intCurrentLine01Index = intCurrentLine01Index + 1
                            
                            If intCurrentLine02Index + 1 <= intLine02IndexMax Then
                            
                                If strDiff <> "" Then
                
                                    strDiff = strDiff & vbNewLine
                                End If
                            
                                strDiff = strDiff & "> " & strCurrentLine02
                                
                                intCurrentLine02Index = intCurrentLine02Index + 1
                            End If
                            
                            
                            If strDiff <> "" Then
                
                                strDiff = strDiff & vbNewLine
                            End If
                            
                            strDiff = strDiff & "> " & strCurrentLine02
                            
                            intCurrentLine02Index = intCurrentLine02Index + 1
                        End If
                    End If
                End If
            Else
                intCurrentLine01Index = intCurrentLine01Index + 1: intCurrentLine02Index = intCurrentLine02Index + 1
            End If
        End If
        
        
        If intCurrentLine01Index > intLine01IndexMax Then
        
            blnIsFinishedScanningSrc01 = True
        End If
        
        If intCurrentLine02Index > intLine02IndexMax Then
        
            blnIsFinishedScanningSrc02 = True
        End If
        
        strRemainedDiff = strDiff
    Loop

    rstrDiffLines = strDiff

    If strDiff <> "" Then
    
        blnDiffExists = True
    End If

    DoesDiffentLinesBetweenTwoTextsExistUsingFSO = blnDiffExists
End Function

'**---------------------------------------------
'** FileSystemObject and FindKeywords.bas dependency
'**---------------------------------------------
'''
'''
'''
''' <Argument>vstrTextFile01Path: Input</Argument>
''' <Argument>vstrTextFile02Path: Input</Argument>
''' <Argument>vobjIgnoreRegExpPatternsDic: Input - Dictionary(Of Key[1st pattern], Value[2nd pattern]</Argument>
''' <Return>Boolean</Return>
Public Function IsDifferentBetweenTextFilesWithSpecifiedIgnoreRegExpPatternLinesByFileSystemObject(ByVal vstrTextFile01Path As String, _
        ByVal vstrTextFile02Path As String, _
        ByVal vobjIgnoreRegExpPatternsDic As Scripting.Dictionary) As Boolean
    
    Dim strAllText01 As String, strAllText02 As String
    
    msubLoadTwoStringsFromEachTextFiles strAllText01, _
            strAllText02, _
            vstrTextFile01Path, _
            vstrTextFile02Path

    IsDifferentBetweenTextFilesWithSpecifiedIgnoreRegExpPatternLinesByFileSystemObject = IsDifferentBetweenTwoTextsWithSpecifiedIgnoreRegExpPatternLinesByFileSystemObject(strAllText01, _
            strAllText02, _
            vobjIgnoreRegExpPatternsDic)
End Function


'''
'''
'''
''' <Argument>vstrTextFile01Path: Input</Argument>
''' <Argument>vstrTextFile02Path: Input</Argument>
''' <Argument>vobjIgnoreRegExpPatternsDic: Input - Dictionary(Of Key[1st pattern], Value[2nd pattern]</Argument>
''' <Return>Boolean</Return>
Public Function IsDifferentBetweenTwoTextsWithSpecifiedIgnoreRegExpPatternLinesByFileSystemObject(ByRef rstrText01 As String, _
        ByRef rstrText02 As String, _
        ByVal vobjIgnoreRegExpPatternsDic As Scripting.Dictionary) As Boolean

    Dim objRegExp01 As VBScript_RegExp_55.RegExp, objRegExp02 As VBScript_RegExp_55.RegExp, blnIsDiffExisted As Boolean
    Dim varPattern01 As Variant, strPattern01 As String, strPattern02 As String
    Dim objDiffCodesDic As Scripting.Dictionary, varLineNumber As Variant, strLine As String
    Dim intFound01LineNumber As Long, intFound02LineNumber As Long
    Dim strDiffLines As String
    
    
    blnIsDiffExisted = False
    
    If IsDifferentBetweenTwoTextsByFileSystemObject(rstrText01, rstrText02) Then
    
        DoesDiffentLinesBetweenTwoTextsExistUsingFSO strDiffLines, rstrText01, rstrText02
        
        Set objDiffCodesDic = GetLinesDicFromOneStringText(strDiffLines)
        
        With vobjIgnoreRegExpPatternsDic
        
            For Each varPattern01 In .Keys
        
                strPattern01 = varPattern01
                
                strPattern02 = .Item(varPattern01)
                
                Set objRegExp01 = GetRegExpObjectFromGeneralCacheAfterSetting(strPattern01)
                
                Set objRegExp02 = GetRegExpObjectFromGeneralCacheAfterSetting(strPattern02)
                
                intFound01LineNumber = 0: intFound02LineNumber = 0
                
                With objDiffCodesDic
                
                    For Each varLineNumber In .Keys
                    
                        strLine = .Item(varLineNumber)
                    
                        If intFound01LineNumber = 0 Then
                        
                            If objRegExp01.Test(strLine) Then
                            
                                intFound01LineNumber = varLineNumber
                            End If
                        End If
                    
                        If intFound02LineNumber = 0 Then
                        
                            If objRegExp02.Test(strLine) Then
                            
                                intFound02LineNumber = varLineNumber
                            End If
                        End If
                    
                        If intFound01LineNumber > 0 And intFound02LineNumber > 0 Then
                        
                            Exit For
                        End If
                    Next
                End With
                
                If intFound01LineNumber > 0 Then
                
                    objDiffCodesDic.Remove intFound01LineNumber
                End If
                
                If intFound02LineNumber > 0 Then
                
                    objDiffCodesDic.Remove intFound02LineNumber
                End If
                
                If objDiffCodesDic.Count = 0 Then
                
                    Exit For
                End If
            Next
            
            If objDiffCodesDic.Count > 0 Then
            
                blnIsDiffExisted = True
            End If
        End With
    End If

    IsDifferentBetweenTwoTextsWithSpecifiedIgnoreRegExpPatternLinesByFileSystemObject = blnIsDiffExisted
End Function



'///////////////////////////////////////////////
'/// Internal function
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubLoadTwoStringsFromEachTextFiles(ByRef rstrAllText01 As String, ByRef rstrAllText02 As String, ByRef rstrTextFile01Path As String, ByRef rstrTextFile02Path As String)

#If HAS_REF Then

    With New Scripting.FileSystemObject
#Else
    With CreateObject("Scripting.FileSystemObject")
#End If

        With .OpenTextFile(rstrTextFile01Path, Scripting.IOMode.ForReading)
        
            If Not .AtEndOfStream Then
        
                rstrAllText01 = .ReadAll
            Else
                rstrAllText01 = ""
            End If
        
            .Close
        End With
    
        With .OpenTextFile(rstrTextFile02Path, Scripting.IOMode.ForReading)
        
            If Not .AtEndOfStream Then
        
                rstrAllText02 = .ReadAll
            Else
                rstrAllText02 = ""
            End If
            
            .Close
        End With
    End With
End Sub


