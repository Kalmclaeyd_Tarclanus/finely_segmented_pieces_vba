Attribute VB_Name = "UTfCompareTextFiles"
'
'   Some tests for compare two text-files, such as source files
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri,  2/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Control VBE windows
'///////////////////////////////////////////////
'**---------------------------------------------
'** Open relative VBE code-panes
'**---------------------------------------------
Private Sub OpenVBECodePaneRelativeToUTfCompareTextFiles()

    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.

    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
    
    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "CompareTextFiles,CodeFilesManagementVBA"
End Sub

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Output diff-texts to text-file and open it by some text-editor
'**---------------------------------------------
'''
'''
'''
''' <Argument>rstrSrc01Path: Input</Argument>
''' <Argument>rstrSrc02Path: Input</Argument>
Public Sub ConfirmDiffBySomeTextEditors(ByRef rstrSrc01Path As String, _
        ByRef rstrSrc02Path As String)

    Dim strDiff As String

    With New Scripting.FileSystemObject

        If DoesDiffentLinesBetweenTwoTextFilesExistUsingFSO(strDiff, rstrSrc01Path, rstrSrc02Path) Then
        
            Debug.Print "The " & .GetFileName(rstrSrc01Path) & " include some differences"
            
            msubOpenDiffResultAfterOutputLogText rstrSrc01Path, rstrSrc02Path, strDiff
        Else
            Debug.Print "The " & .GetFileName(rstrSrc01Path) & " include no difference"
        End If
    End With
End Sub

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'**---------------------------------------------
'** Use sample texts
'**---------------------------------------------
'''
'''
'''
Private Sub msubSanityTestToSampleTwoTextFilesUsingFSO_Same01()

    Dim strSrc01Path As String, strSrc02Path As String
    
    msubGetTwoSampleFilePaths strSrc01Path, strSrc02Path


    msubWriteSample01 strSrc01Path
    
    msubWriteSample01 strSrc02Path


    msubDebugPrintDiff strSrc01Path, strSrc02Path
End Sub

'''
'''
'''
Private Sub msubSanityTestToSampleTwoTextFilesUsingFSO_Diff02()

    Dim strSrc01Path As String, strSrc02Path As String
    
    msubGetTwoSampleFilePaths strSrc01Path, strSrc02Path


    msubWriteByFSO strSrc01Path, "Sample_ABCD"
    
    msubWriteByFSO strSrc02Path, "Sample_EFGH"


    msubDebugPrintDiff strSrc01Path, strSrc02Path
End Sub

'''
'''
'''
Private Sub msubSanityTestToSampleTwoTextFilesUsingFSO_Diff03()

    Dim strSrc01Path As String, strSrc02Path As String
    
    msubGetTwoSampleFilePaths strSrc01Path, strSrc02Path


    msubWriteByFSO strSrc01Path, "Sample_ABCD" & vbNewLine & "Sample_ABCD"
    
    msubWriteByFSO strSrc02Path, "Sample_ABCD" & vbNewLine & "Sample_EFGH"


    msubDebugPrintDiff strSrc01Path, strSrc02Path
End Sub

'''
'''
'''
Private Sub msubSanityTestToSampleTwoTextFilesUsingFSO_Diff04()

    Dim strSrc01Path As String, strSrc02Path As String
    
    msubGetTwoSampleFilePaths strSrc01Path, strSrc02Path


    msubWriteByFSO strSrc01Path, "Sample_ABCD" & vbNewLine & "Sample_ABCD" & vbNewLine & "Sample_ABCD"
    
    msubWriteByFSO strSrc02Path, "Sample_ABCD" & vbNewLine & "Sample_EFGH" & vbNewLine & "Sample_ABCD"


    msubDebugPrintDiff strSrc01Path, strSrc02Path
End Sub

'''
'''
'''
Private Sub msubSanityTestToSampleTwoTextFilesUsingFSO_Diff05()

    Dim strSrc01Path As String, strSrc02Path As String
    
    msubGetTwoSampleFilePaths strSrc01Path, strSrc02Path


    msubWriteByFSO strSrc01Path, "Sample_ABCD" & vbNewLine & "Sample_ABCD" & vbNewLine & "Sample_ABCD" & vbNewLine & "Sample_ABCD"
    
    msubWriteByFSO strSrc02Path, "Sample_ABCD" & vbNewLine & "Sample_EFGH" & vbNewLine & "Sample_ABCD" & vbNewLine & "Sample_IJKL"


    msubDebugPrintDiff strSrc01Path, strSrc02Path
End Sub


'''
'''
'''
Private Sub msubGetTwoSampleFilePaths(ByRef rstrSrc01Path As String, ByRef rstrSrc02Path As String)

    rstrSrc01Path = mfstrGetTmpSampleTextFilesDir() & "\Src01Text.txt"

    rstrSrc02Path = mfstrGetTmpSampleTextFilesDir() & "\Src02Text.txt"
End Sub


'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'**---------------------------------------------
'** Solute path
'**---------------------------------------------
'''
'''
'''
Private Function mfstrGetTmpSampleTextFilesDir() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\SampleTextFiles"

    ForceToCreateDirectory strDir
    
    mfstrGetTmpSampleTextFilesDir = strDir
End Function

'''
'''
'''
Private Function mfstrGetTmpSampleDiffResultDir() As String

    Dim strDir As String
    
    strDir = GetTemporaryDevelopmentVBARootDir() & "\SampleTextFiles\DiffResults"

    ForceToCreateDirectory strDir
    
    mfstrGetTmpSampleDiffResultDir = strDir
End Function

'''
'''
'''
Private Sub msubOpenDiffResultAfterOutputLogText(ByRef rstrSrc01Path As String, ByRef rstrSrc02Path As String, ByRef rstrDiff As String)

    Dim strLogPath As String
    
    strLogPath = mfstrGetLogPathAfterCreateLogTextFile(rstrSrc01Path, rstrSrc02Path, rstrDiff)

    OpenTextBySomeSelectedTextEditorFromWshShell strLogPath, True, True
End Sub

'''
'''
'''
Private Function mfstrGetLogPathAfterCreateLogTextFile(ByRef rstrSrc01Path As String, ByRef rstrSrc02Path As String, ByRef rstrDiff As String) As String

    Dim strLogDir As String, strLogPath As String
    
    strLogDir = mfstrGetTmpSampleDiffResultDir()

    With New Scripting.FileSystemObject

        strLogPath = mfstrGetTmpSampleDiffResultDir() & "\SimpleTextDiffOf" & .GetBaseName(rstrSrc01Path) & "." & .GetExtensionName(rstrSrc01Path)

        With .CreateTextFile(strLogPath, True)
        
            .WriteLine "' Difference logged date: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
            
            .WriteLine "' Source path 1st: " & rstrSrc01Path
            
            .WriteLine "' Source path 2nd: " & rstrSrc02Path
        
            .WriteLine vbNewLine
            
            .Write rstrDiff
        
            .Close
        End With
    End With

    mfstrGetLogPathAfterCreateLogTextFile = strLogPath
End Function


'**---------------------------------------------
'** Small testing procedures
'**---------------------------------------------
'''
'''
'''
Private Sub msubDebugPrintDiff(ByRef rstrSrc01Path As String, ByRef rstrSrc02Path As String)

    Dim strDiff As String

    With New Scripting.FileSystemObject

        If DoesDiffentLinesBetweenTwoTextFilesExistUsingFSO(strDiff, rstrSrc01Path, rstrSrc02Path) Then
        
            Debug.Print "The " & .GetFileName(rstrSrc01Path) & " include some differences as:"
            
            Debug.Print strDiff
        Else
            Debug.Print "The " & .GetFileName(rstrSrc01Path) & " include no difference"
        End If
    End With
End Sub

'''
'''
'''
Private Sub msubWriteSample01(ByRef rstrTxtPath As String)

    Dim strText As String
    
    strText = "Sample_ABCD"

    msubWriteByFSO rstrTxtPath, strText
End Sub

'''
'''
'''
Private Sub msubWriteSample02(ByRef rstrTxtPath As String)

    Dim strText As String
    
    strText = "Sample_EFGH"

    msubWriteByFSO rstrTxtPath, strText
End Sub

'''
'''
'''
Private Sub msubWriteByFSO(ByRef rstrTxtPath As String, ByRef rstrBodyText As String)

    With New Scripting.FileSystemObject
    
        With .CreateTextFile(rstrTxtPath, True)
        
            .Write rstrBodyText
        
            .Close
        End With
    End With
End Sub

