Attribute VB_Name = "UTfDataTableTextOut"
'
'   Testing for output text tools
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 25/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
Private Sub msubSanityTestOfGetLineTextWithLineFeedFromCol()

    Dim strText As String, objCol As Collection
    
    Set objCol = mfobjGetColFromNaturalNumber(20)

    strText = GetLineTextWithLineFeedFromCol(objCol)

    Debug.Print strText

    strText = GetLineTextWithLineFeedFromCol(objCol, 1)

    Debug.Print strText
    
    strText = GetLineTextWithLineFeedFromCol(objCol, 5)

    Debug.Print strText
    
    
    strText = GetLineTextWithLineFeedFromCol(objCol, 3)

    Debug.Print strText
    
    strText = GetLineTextWithLineFeedFromCol(objCol, 3, """", vbTab, False)

    Debug.Print strText
    
    strText = GetLineTextWithLineFeedFromCol(mfobjGetColFromNaturalNumber(3), 5, "", vbTab, False)

    Debug.Print strText
End Sub


Private Sub msubSanityTestOfGetLineTextFromCol()

    Dim strText As String, objCol As Collection
    
    Set objCol = mfobjGetColFromNaturalNumber(20)

    strText = GetLineTextFromCol(objCol)

    Debug.Print strText
    
    strText = GetLineTextFromCol(objCol, """")

    Debug.Print strText
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
Private Function mfobjGetColFromNaturalNumber(ByVal vintCountOfElements As Long) As Collection

    Dim objCol As Collection, i As Long
    
    Set objCol = New Collection
    
    For i = 1 To vintCountOfElements
    
        objCol.Add i
    Next
    
    Set mfobjGetColFromNaturalNumber = objCol
End Function
