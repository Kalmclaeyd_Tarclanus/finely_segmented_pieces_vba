Attribute VB_Name = "DataTableTextOut"
'
'   output text, such as CSV
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 25/May/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Parse simple parameter text
'**---------------------------------------------
'''
'''
'''
Public Function IsDelimiterCharFoundAndThenGetLeftPartAndRightPartByDelimiting(ByRef rstrLeftPart As String, ByRef rstrRightPart As String, ByRef rstrSourceText As String, Optional ByVal vstrDelimiterChar As String = "=") As Boolean

    Dim intP1 As Long, blnIsFound As Boolean

    blnIsFound = False

    intP1 = InStr(1, rstrSourceText, vstrDelimiterChar)
    
    If intP1 > 0 Then
    
        rstrLeftPart = Left(rstrSourceText, intP1 - 1)
        
        rstrRightPart = Right(rstrSourceText, Len(rstrSourceText) - intP1)
        
        blnIsFound = True
    Else
        rstrLeftPart = ""
    
        rstrRightPart = ""
    End If
    
    IsDelimiterCharFoundAndThenGetLeftPartAndRightPartByDelimiting = blnIsFound
End Function

'**---------------------------------------------
'** About output CSV file text
'**---------------------------------------------
'''
'''
'''
Public Sub OutputColToCSVFile(ByVal vobjCol As Collection, ByVal vobjFieldTitlesCol As Collection, ByVal vstrCSVFilePath As String)

    With New FileSystemObject
    
        ForceToCreateDirectory .FolderExists(.GetParentFolderName(vstrCSVFilePath))
    
        If .FolderExists(.GetParentFolderName(vstrCSVFilePath)) Then
        
            With .CreateTextFile(vstrCSVFilePath, True)
                
                .Write GetCSVTextFromCol(vobjCol, vobjFieldTitlesCol)
                
                .Close
            End With
        End If
    End With
End Sub


'''
'''
'''
Public Sub OutputDicToCSVFile(ByVal vobjDic As Scripting.Dictionary, ByVal vobjFieldTitlesCol As Collection, ByVal vstrCSVFilePath As String)

    With New FileSystemObject
    
        ForceToCreateDirectory .FolderExists(.GetParentFolderName(vstrCSVFilePath))
    
        If .FolderExists(.GetParentFolderName(vstrCSVFilePath)) Then
        
            With .CreateTextFile(vstrCSVFilePath, True)
                
                .Write GetCSVTextFromDic(vobjDic, vobjFieldTitlesCol)
                
                .Close
            End With
        End If
    End With
End Sub

'''
'''
'''
''' <Argument>vvarDataTable; Two dimensional variant array</Argument>
Public Sub OutputVarDataTableToCSVFile(ByVal vvarDataTable As Variant, ByVal vobjFieldTitlesCol As Collection, ByVal vstrCSVFilePath As String)

    With New FileSystemObject
        
        ForceToCreateDirectory .FolderExists(.GetParentFolderName(vstrCSVFilePath))
    
        If .FolderExists(.GetParentFolderName(vstrCSVFilePath)) Then
            
            With .CreateTextFile(vstrCSVFilePath, True)
                
                .Write GetCSVTextFromVarDataTable(vvarDataTable, vobjFieldTitlesCol)
                
                .Close
            End With
        End If
    End With
End Sub

'''
''' get CSV text string from Collection
'''
Public Function GetCSVTextFromCol(ByVal vobjCol As Collection, _
        ByVal vobjFieldTitlesCol As Collection, _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vstrSurroundChar As String = "", _
        Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True) As String
    
    Dim strText As String
    
    strText = ""
    
    Select Case True
    
        Case vobjFieldTitlesCol Is Nothing, vobjFieldTitlesCol.Count = 0
        
            ' nothing to do
        Case Else
            msubWriteFieldTitlesTextToString strText, vobjFieldTitlesCol, vstrDelimiter
    End Select
    
    msubWriteCSVTextFromColToString strText, vobjCol, vstrDelimiter, vstrSurroundChar, vblnIncludeSpaceAfterDelimitedChar

    GetCSVTextFromCol = strText
End Function

'''
'''
'''
Public Function GetTextFromCol(ByVal vobjCol As Collection) As String

    GetTextFromCol = GetCSVTextFromCol(vobjCol, Nothing)
End Function


'''
'''
'''
Public Function GetTwoDimentionalTextFromCol(ByVal vobjCol As Collection, Optional ByVal vstrSurroundChar As String = "", Optional ByVal vstrDelimiter As String = ",", Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True) As String

    Dim strText As String: strText = ""

    msubWriteCSVTextFromColToString strText, vobjCol, vstrDelimiter, vstrSurroundChar, vblnIncludeSpaceAfterDelimitedChar

    GetTwoDimentionalTextFromCol = strText
End Function

'''
'''
'''
Public Function GetTwoDimentionalTextFromDic(ByVal vobjDic As Scripting.Dictionary, _
        Optional ByVal vstrSurroundChar As String = "", _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True, _
        Optional ByVal vblnAllowToUseColonDelimiterAtOnlyFirstDelimiter As Boolean = False) As String

    Dim strText As String: strText = ""

    msubWriteCSVTextFromDicToString strText, vobjDic, vstrDelimiter, vstrSurroundChar, vblnIncludeSpaceAfterDelimitedChar, vblnAllowToUseColonDelimiterAtOnlyFirstDelimiter

    GetTwoDimentionalTextFromDic = strText
End Function



'''
''' get CSV text string
'''
Public Function GetCSVTextFromDic(ByVal vobjDic As Scripting.Dictionary, _
        ByVal vobjFieldTitlesCol As Collection, _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vstrSurroundChar As String = "", _
        Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True, _
        Optional ByVal vblnAllowToUseColonDelimiterAtOnlyFirstDelimiter As Boolean = False) As String
    
    Dim strText As String
    
    strText = ""
    
    Select Case True
    
        Case vobjFieldTitlesCol Is Nothing, vobjFieldTitlesCol.Count = 0
        
            ' nothing to do
        Case Else
    
            msubWriteFieldTitlesTextToString strText, vobjFieldTitlesCol, vstrDelimiter
    End Select
    
    msubWriteCSVTextFromDicToString strText, vobjDic, vstrDelimiter, vstrSurroundChar, vblnIncludeSpaceAfterDelimitedChar, vblnAllowToUseColonDelimiterAtOnlyFirstDelimiter

    GetCSVTextFromDic = strText
End Function

'''
'''
'''
Public Function GetTextFromDic(ByVal vobjDic As Scripting.Dictionary) As String

    GetTextFromDic = GetCSVTextFromDic(vobjDic, Nothing)
End Function

'''
'''
'''
''' <Argument>rstrText: Output</Argument>
''' <Argument>vobjFieldTitlesCol: Input</Argument>
''' <Argument>vstrDelimiter: Input</Argument>
Private Sub msubWriteFieldTitlesTextToString(ByRef rstrText As String, ByVal vobjFieldTitlesCol As Collection, Optional ByVal vstrDelimiter As String = ",")

    Dim j As Long, varFieldTitle As Variant, strFieldTitle As String
    
    If Not vobjFieldTitlesCol Is Nothing Then
    
        j = 1
        
        For Each varFieldTitle In vobjFieldTitlesCol
            
            If rstrText <> "" Then
            
                rstrText = rstrText & vbNewLine
            End If
            
            strFieldTitle = varFieldTitle
            
            rstrText = rstrText & strFieldTitle
            
            If j < vobjFieldTitlesCol.Count Then
                
                rstrText = rstrText & vstrDelimiter
            End If
            
            j = j + 1
        Next
    End If
End Sub

'''
'''
'''
Public Function GetMultiLineTextForGridCellFromCol(ByRef robjCol As Collection, Optional ByVal vstrSurroundLeftChar As String = "", Optional ByVal vstrSurroundRightChar As String = "", Optional ByVal vstrNewLineChar As String = vbLf)

    Dim strText As String, varItem As Variant
    
    strText = ""
    
    For Each varItem In robjCol
    
        If strText <> "" Then
        
            strText = strText & vstrNewLineChar
        End If
    
        strText = strText & vstrSurroundLeftChar & CStr(varItem) & vstrSurroundRightChar
    Next
    
    GetMultiLineTextForGridCellFromCol = strText
End Function


'''
'''
'''
''' <Argument>rstrText: Output</Argument>
''' <Argument>vobjCol: Input</Argument>
''' <Argument>vstrDelimiter: Input</Argument>
''' <Argument>vstrSurroundChar: Input</Argument>
''' <Argument>vblnIncludeSpaceAfterDelimitedChar: Input</Argument>
Private Sub msubWriteCSVTextFromColToString(ByRef rstrText As String, _
        ByVal vobjCol As Collection, _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vstrSurroundChar As String = "", _
        Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True)

    Dim i As Long, j As Long
    Dim varFieldTitle As Variant, strFieldTitle As String
    Dim varRowCol As Variant, objRowCol As Collection
    Dim varItem As Variant, blnIsOneDimensionalCol As Boolean

    If Not vobjCol Is Nothing Then
    
        If rstrText <> "" Then
        
            rstrText = rstrText & vbNewLine
        End If
    
        If vobjCol.Count > 0 Then
        
            blnIsOneDimensionalCol = (Not IsObject(vobjCol.Item(1)))
            
            If blnIsOneDimensionalCol Then
            
                i = 1
                
                For Each varItem In vobjCol
                
                    If IsObject(varItem) Then
                        
                        If varItem Is Nothing Then
                            
                            rstrText = rstrText & " "
                        Else
                            Debug.Print "Error: an object is included, " & TypeName(varItem)
                            
                            Debug.Assert False
                            
                            rstrText = rstrText & TypeName(varItem)
                        End If
                        
                    ElseIf IsEmpty(varItem) Or IsNull(varItem) Then
                    
                        rstrText = rstrText & " "
                    Else
                        rstrText = rstrText & vstrSurroundChar & CStr(varItem) & vstrSurroundChar
                    End If
                    
                    If i < vobjCol.Count Then
                    
                        rstrText = rstrText & vbNewLine
                    End If
                    
                    i = i + 1
                Next
            Else
                i = 1
                
                For Each varRowCol In vobjCol
                    
                    Set objRowCol = varRowCol
                    
                    j = 1
                    
                    For Each varItem In objRowCol
                    
                        If IsObject(varItem) Then
                            
                            If varItem Is Nothing Then
                                
                                rstrText = rstrText & " "
                            Else
                                Debug.Assert False
                                
                                Debug.Print "Error: an object is included, " & TypeName(varItem)
                                
                                rstrText = rstrText & TypeName(varItem)
                            End If
                        ElseIf IsEmpty(varItem) Or IsNull(varItem) Then
                            
                            rstrText = rstrText & " "
                        Else
                            
                            rstrText = rstrText & vstrSurroundChar & CStr(varItem) & vstrSurroundChar
                        End If
                        
                        If j < objRowCol.Count Then
                            
                            If vblnIncludeSpaceAfterDelimitedChar Then
                            
                                rstrText = rstrText & vstrDelimiter & " "
                            Else
                                rstrText = rstrText & vstrDelimiter
                            End If
                        End If
                        
                        j = j + 1
                    Next
                
                    If i < vobjCol.Count Then
                    
                        rstrText = rstrText & vbNewLine
                    End If
                    
                    i = i + 1
                Next
            End If
        End If
    End If
End Sub


'''
'''
'''
''' <Argument>rstrText: Output</Argument>
''' <Argument>vobjDic: Input</Argument>
''' <Argument>vstrDelimiter: Input</Argument>
''' <Argument>vstrSurroundChar: Input</Argument>
''' <Argument>vblnIncludeSpaceAfterDelimitedChar: Input</Argument>
''' <Argument>vblnAllowToUseColonDelimiterAtOnlyFirstDelimiter: Input</Argument>
Private Sub msubWriteCSVTextFromDicToString(ByRef rstrText As String, _
        ByVal vobjDic As Scripting.Dictionary, _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vstrSurroundChar As String = "", _
        Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True, _
        Optional ByVal vblnAllowToUseColonDelimiterAtOnlyFirstDelimiter As Boolean = False)
        

    Dim i As Long, j As Long, varFieldTitle As Variant, strFieldTitle As String
    Dim varRowCol As Variant, objRowCol As Collection, varItem As Variant
    Dim blnIsOneDimensionalDic As Boolean
    Dim varKey As Variant, objKeyCol As Collection

    If Not vobjDic Is Nothing Then
    
        If vobjDic.Count > 0 Then
            
            If rstrText <> "" Then
            
                rstrText = rstrText & vbNewLine
            End If
            
            With vobjDic
            
                For Each varKey In .Keys
                
                    If IsObject(.Item(varKey)) Then
                        
                        Set varItem = .Item(varKey)
                    Else
                        varItem = .Item(varKey)
                    End If
                    
                    Exit For
                Next
            
                blnIsOneDimensionalDic = (Not IsObject(varItem))
                
                If blnIsOneDimensionalDic Then
                
                    i = 1
                    
                    For Each varKey In .Keys
                    
                        varItem = .Item(varKey)
                        
                        ' about Key
                        If IsObject(varKey) Then
                        
                            If varKey Is Nothing Then
                            
                                rstrText = rstrText & " "
                                
                            ElseIf TypeName(varKey) = "Collection" Then
                            
                                Set objKeyCol = varKey
                            
                                rstrText = rstrText & vstrSurroundChar & "[" & GetLineTextFromCol(objKeyCol) & "]" & vstrSurroundChar
                            
                            Else
                                Debug.Print "Error: an object is included, " & TypeName(varItem)
                                
                                Debug.Assert False
                                
                                rstrText = rstrText & TypeName(varItem)
                            End If
                        ElseIf IsEmpty(varKey) Or IsNull(varKey) Then
                        
                            rstrText = rstrText & " "
                        Else
                            rstrText = rstrText & vstrSurroundChar & CStr(varKey) & vstrSurroundChar
                        End If
                        
                        ' 1st delimiter
                        
                        If vblnAllowToUseColonDelimiterAtOnlyFirstDelimiter Then
                        
                            rstrText = rstrText & ": "
                        Else
                            If vblnIncludeSpaceAfterDelimitedChar Then
                            
                                rstrText = rstrText & vstrDelimiter & " "
                            Else
                                rstrText = rstrText & vstrDelimiter
                            End If
                        End If
                        
                        
                        ' about Item
                        If IsObject(varItem) Then
                        
                            If varItem Is Nothing Then
                            
                                rstrText = rstrText & " "
                            Else
                                Debug.Print "Error: an object is included, " & TypeName(varItem)
                                
                                Debug.Assert False
                                
                                rstrText = rstrText & vstrSurroundChar & TypeName(varItem) & vstrSurroundChar
                            End If
                            
                        ElseIf IsEmpty(varItem) Or IsNull(varItem) Then
                        
                            rstrText = rstrText & " "
                        Else
                            rstrText = rstrText & vstrSurroundChar & CStr(varItem) & vstrSurroundChar
                        End If
                    
                        If i < vobjDic.Count Then
                        
                            rstrText = rstrText & vbNewLine
                        End If
                        
                        i = i + 1
                    Next
                Else    ' Not blnIsOneDimensionalDic
                
                    i = 1
                    
                    For Each varKey In .Keys
                    
                        Set objRowCol = .Item(varKey)
                        
                        ' about Key
                        If IsObject(varKey) Then
                        
                            If varKey Is Nothing Then
                            
                                rstrText = rstrText & " "
                            
                            ElseIf TypeName(varKey) = "Collection" Then
                        
                                Set objKeyCol = varKey
                            
                                rstrText = rstrText & vstrSurroundChar & "[" & GetLineTextFromCol(objKeyCol) & "]" & vstrSurroundChar
                            Else
                                Debug.Print "Error: an object is included, " & TypeName(varKey)
                                
                                Debug.Assert False
                                
                                rstrText = rstrText & TypeName(varKey)
                            End If
                            
                        ElseIf IsEmpty(varKey) Or IsNull(varKey) Then
                        
                            rstrText = rstrText & " "
                        Else
                            rstrText = rstrText & vstrSurroundChar & CStr(varKey) & vstrSurroundChar
                        End If
                        
                        ' 1st delimiter
                        If vblnAllowToUseColonDelimiterAtOnlyFirstDelimiter Then
                        
                            rstrText = rstrText & ": "
                        Else
                            If vblnIncludeSpaceAfterDelimitedChar Then
                            
                                rstrText = rstrText & vstrDelimiter & " "
                            Else
                                rstrText = rstrText & vstrDelimiter
                            End If
                        End If
                        
                        ' about Collection
                        j = 1
                        
                        For Each varItem In objRowCol
                        
                            If IsObject(varItem) Then
                            
                                If varItem Is Nothing Then
                                
                                    rstrText = rstrText & " "
                                Else
                                    Debug.Assert False
                                    
                                    Debug.Print "Error: an object is included, " & TypeName(varItem)
                                    
                                    rstrText = rstrText & TypeName(varItem)
                                End If
                                
                            ElseIf IsEmpty(varItem) Or IsNull(varItem) Then
                            
                                rstrText = rstrText & " "
                            Else
                                rstrText = rstrText & vstrSurroundChar & CStr(varItem) & vstrSurroundChar
                            End If
                            
                        
                            If j < objRowCol.Count Then
                            
                                If vblnIncludeSpaceAfterDelimitedChar Then
                                
                                    rstrText = rstrText & vstrDelimiter & " "
                                Else
                                    rstrText = rstrText & vstrDelimiter
                                End If
                            End If
                            
                            j = j + 1
                        Next
                    
                        If i < .Count Then
                        
                            rstrText = rstrText & vbNewLine
                        End If
                        
                        i = i + 1
                    Next
                End If
            End With
        End If
    End If
End Sub


'''
''' get CSV text string from two-dimentional data table array
'''
Public Function GetCSVTextFromVarDataTable(ByRef rvarDataTable As Variant, _
        ByVal vobjFieldTitlesCol As Collection, _
        Optional ByVal vstrDelimiter As String = ",") As String
    
    Dim strText As String
    Dim i As Long, j As Long
    Dim varFieldTitle As Variant, strFieldTitle As String
    Dim varRowCol As Variant, objRowCol As Collection
    Dim varItem As Variant
    Dim blnIsOneDimensionalCol As Boolean
    
    strText = ""
    
    msubWriteFieldTitlesTextToString strText, vobjFieldTitlesCol
    
    GetCsvTableTextFromVariantArray strText, vstrDelimiter
    
    GetCSVTextFromVarDataTable = strText
End Function

'''
'''
'''
Public Function GetTextFromVar(ByRef rvarValues As Variant, _
        Optional ByVal vblnAllowToTransposeWhenTheOneDimensionalArray As Boolean = False, _
        Optional ByVal vstrDelimitedChar As String = ",") As String

    Dim strLine As String: strLine = ""

    If Not VBA.IsArray(rvarValues) Then
    
        Select Case True
        
            Case IsEmpty(rvarValues), IsNull(rvarValues)
            
            
            Case Else
            
                strLine = CStr(rvarValues)
        End Select
    Else
        GetCsvTableTextFromVariantArray strLine, rvarValues, vstrDelimitedChar, , , vblnAllowToTransposeWhenTheOneDimensionalArray
    End If

    GetTextFromVar = strLine
End Function


'''
'''
'''
''' <Argument>rstrText: Output</Argument>
''' <Argument>rvarValues: Input</Argument>
''' <Argument>vstrDelimiter: Input</Argument>
''' <Argument>vstrSurroundChar: Input</Argument>
''' <Argument>vblnAllowToTransposeWhenTheOneDimensionalArray: Input</Argument>
Public Sub GetCsvTableTextFromVariantArray(ByRef rstrText As String, _
        ByRef rvarValues As Variant, _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vstrSurroundChar As String = "", _
        Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True, _
        Optional ByVal vblnAllowToTransposeWhenTheOneDimensionalArray As Boolean = False)

    Dim intDimension As Long, intRowMax As Long, intColumnMax As Long, int3rdDimensionMax As Long, int4thDimensionMax As Long
    
    Dim strSpaceChar As String, objItem As Object
    
    Dim i As Long, j As Long
    
    
    If vblnIncludeSpaceAfterDelimitedChar Then
    
        strSpaceChar = " "
    Else
        strSpaceChar = ""
    End If
    

    GetVariantArrayDimensions intDimension, intRowMax, intColumnMax, int3rdDimensionMax, int4thDimensionMax, rvarValues

    Select Case intDimension
    
        Case 1  ' one-dimensional array
        
            For i = LBound(rvarValues, 1) To intRowMax
            
                If rstrText <> "" Then
                
                    If vblnAllowToTransposeWhenTheOneDimensionalArray Then
                    
                        rstrText = rstrText & vstrDelimiter & strSpaceChar
                    Else
                        rstrText = rstrText & vbNewLine
                    End If
                End If
            
                If IsObject(rvarValues(i)) Then
                
                    ' an illegal process
                
                    Set objItem = rvarValues(i)
                    
                    Debug.Print "Error: an object is included - " & TypeName(objItem)
                
                    Debug.Assert False
                
                    rstrText = rstrText & vstrSurroundChar & TypeName(objItem) & vstrSurroundChar
                Else
                    Select Case True
                    
                        Case IsEmpty(rvarValues(i)), IsNull(rvarValues(i))
                        
                            rstrText = rstrText & vstrSurroundChar & vstrSurroundChar
                        Case Else
                            
                            rstrText = rstrText & vstrSurroundChar & CStr(rvarValues(i)) & vstrSurroundChar
                    End Select
                End If
            Next
        
        Case 2  ' two-dimensional array, for example, as the Excel.Worksheet ranges Value
        
            For i = LBound(rvarValues, 1) To intRowMax
            
                If rstrText <> "" Then
                
                    Select Case True
                    
                        Case Not vblnAllowToTransposeWhenTheOneDimensionalArray, intColumnMax <> 1
                
                            rstrText = rstrText & vbNewLine
                        Case Else
                        
                            rstrText = rstrText & vstrDelimiter & strSpaceChar
                    End Select
                End If
            
                For j = LBound(rvarValues, 2) To intColumnMax
                
                    If IsObject(rvarValues(i, j)) Then
                    
                        ' an illegal process
                        
                        Set objItem = rvarValues(i, j)
                        
                        Debug.Print "Error: an object is included - " & TypeName(objItem)
                    
                        Debug.Assert False
                    
                        rstrText = rstrText & vstrSurroundChar & TypeName(objItem) & vstrSurroundChar
                    Else
                        Select Case True
                        
                            Case IsEmpty(rvarValues(i, j)), IsNull(rvarValues(i, j))
                            
                                rstrText = rstrText & vstrSurroundChar & vstrSurroundChar
                            Case Else
                                
                                rstrText = rstrText & vstrSurroundChar & CStr(rvarValues(i, j)) & vstrSurroundChar
                        End Select
                    End If
                
                    If j < intColumnMax Then
                    
                        rstrText = rstrText & vstrDelimiter & strSpaceChar
                    End If
                Next
            Next
        
        Case 3
            
            ' Not supported for the 3rd dimensional array
        
        Case 4

            ' Not supported for the 4th dimensional array
    End Select
End Sub

'''
'''
'''
''' <Argument>rintDimension: Output</Argument>
''' <Argument>rintRowMax: Output</Argument>
''' <Argument>rintColumnMax: Output</Argument>
''' <Argument>rint3rdDimensionMax: Output</Argument>
''' <Argument>rint4thDimensionMax: Output</Argument>
''' <Argument>rvarValues: Input</Argument>
Public Sub GetVariantArrayDimensions(ByRef rintDimension As Long, _
        ByRef rintRowMax As Long, _
        ByRef rintColumnMax As Long, _
        ByRef rint3rdDimensionMax As Long, _
        ByRef rint4thDimensionMax As Long, _
        ByRef rvarValues As Variant)

    Dim blnContinueToInvestigate As Boolean
    
    blnContinueToInvestigate = True

    rintDimension = 0
    
    If VBA.IsArray(rvarValues) Then
    
        rintDimension = 1
    
        blnContinueToInvestigate = True
    
        On Error Resume Next
        
        rintRowMax = UBound(rvarValues, 1)
        
        If Err.Number <> 0 Then
        
            rintDimension = 0
        
            blnContinueToInvestigate = False
        End If
        
        If blnContinueToInvestigate Then
        
            rintColumnMax = UBound(rvarValues, 2)
        
            If Err.Number <> 0 Then
            
                rintDimension = 1
                
                blnContinueToInvestigate = False
            Else
                rintDimension = 2
            End If
            
            If blnContinueToInvestigate Then
            
                rint3rdDimensionMax = UBound(rvarValues, 3)
            
                If Err.Number <> 0 Then
                
                    rintDimension = 2
                    
                    blnContinueToInvestigate = False
                Else
                    rintDimension = 3
                    
                    Debug.Print "This Variant array has 3rd dimension !"
                End If
            
                If blnContinueToInvestigate Then
            
                    rint4thDimensionMax = UBound(rvarValues, 4)
            
                    If Err.Number <> 0 Then
                    
                        rintDimension = 3
                        
                        blnContinueToInvestigate = False
                    Else
                        rintDimension = 4
                        
                        Debug.Print "This Variant array has 4th dimension !!"
                    End If
                End If
            End If
        End If
        
        On Error GoTo 0
    End If
End Sub

'''
'''
'''
Public Function GetLineTextFromDic(ByVal vobjDic As Scripting.Dictionary, _
        Optional ByVal vstrSurroundChar As String = "", _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True) As String

    Dim strLine As String
    Dim varKey As Variant, varItem As Variant, objRowCol As Collection
    
    strLine = ""
    
    With vobjDic
    
        For Each varKey In .Keys
        
            If strLine <> "" Then
            
                If vblnIncludeSpaceAfterDelimitedChar Then
                    
                    strLine = strLine & vstrDelimiter & " "
                Else
                    strLine = strLine & vstrDelimiter
                End If
            End If
        
            strLine = strLine & vstrSurroundChar & CStr(varKey) & vstrSurroundChar
        
            If vblnIncludeSpaceAfterDelimitedChar Then
                
                strLine = strLine & vstrDelimiter & " "
            Else
                strLine = strLine & vstrDelimiter
            End If
        
            If IsObject(.Item(varKey)) Then
            
                If StrComp(TypeName(.Item(varKey)), "Collection") = 0 Then
                
                    strLine = strLine & GetLineTextFromCol(.Item(varKey), vstrSurroundChar, vblnIncludeSpaceAfterDelimitedChar)
                    
                ElseIf StrComp(TypeName(.Item(varKey)), "Dictionary") = 0 Then
                    
                    ' recursive call
                    strLine = strLine & GetLineTextFromDic(.Item(varKey), vstrSurroundChar, vblnIncludeSpaceAfterDelimitedChar)
                End If
            Else
                Let varItem = .Item(varKey)
                
                Select Case True
                
                    Case IsEmpty(varItem), IsNull(varItem)
                    
                        strLine = strLine & vstrSurroundChar & vstrSurroundChar
                    Case Else
                        strLine = strLine & vstrSurroundChar & CStr(varItem) & vstrSurroundChar
                End Select
            End If
        Next
    End With

    GetLineTextFromDic = strLine
End Function


'''
'''
'''
Public Function GetTypeNameOfDicStructure(ByVal vobjDic As Scripting.Dictionary) As String

    Dim strEstimatedInternalType As String
    
    msubGetTypeStructureInfoOfDic strEstimatedInternalType, vobjDic

    GetTypeNameOfDicStructure = strEstimatedInternalType
End Function

'''
'''
'''
Private Sub msubGetTypeStructureInfoOfDic(ByRef rstrInfo As String, ByRef robjDic As Scripting.Dictionary)

    Dim strLeftInfo As String, strRightInfo As String


    msubGetTypeStructureInfoOfDicForEachPart strLeftInfo, strRightInfo, robjDic

    If strRightInfo <> "" Then
     
        rstrInfo = strLeftInfo & ", " & strRightInfo
    Else
        rstrInfo = strLeftInfo
    End If
End Sub


'''
'''
'''
Private Sub msubGetTypeStructureInfoOfDicForEachPart(ByRef rstrLeftInfo As String, ByRef rstrRightInfo As String, ByRef robjDic As Scripting.Dictionary)

    Dim varKey As Variant, varItem As Variant
    
    Dim objKey As Object, objItem As Object, objKeyCol As VBA.Collection, objKeyDic As Scripting.Dictionary, objItemCol As VBA.Collection, objItemDic As Scripting.Dictionary
    
    Dim strTypeStructureColInfo As String, strTypeStructureDicInfo As String
    
    Select Case True
    
        Case robjDic Is Nothing, robjDic.Count = 0
        
            rstrLeftInfo = "Dictionary"
        
            rstrRightInfo = ""
                
            Exit Sub
    End Select
    
    
    With robjDic
    
        For Each varKey In .Keys
        
            If IsObject(varKey) Then
            
                Set objKey = varKey
                
                If TypeOf objKey Is VBA.Collection Then
                
                    Set objKeyCol = objKey
                
                    msubGetTypeStructureInfoOfCol strTypeStructureColInfo, objKeyCol
                    
                    rstrLeftInfo = "Dictionary(Of " & strTypeStructureColInfo
                    
                ElseIf TypeOf objKey Is Scripting.Dictionary Then
                
                    Set objKeyDic = objKey
                
                    ' recursive call
                    
                    msubGetTypeStructureInfoOfDic strTypeStructureDicInfo, objKeyDic
                    
                    rstrLeftInfo = "Dictionary(Of " & strTypeStructureDicInfo
                Else
                
                    rstrLeftInfo = "Dictionary(Of " & TypeName(objKey)
                End If
            Else
                rstrLeftInfo = "Dictionary(Of " & TypeName(varKey)
            End If
        
            If IsObject(.Item(varKey)) Then
            
                Set objItem = .Item(varKey)
                
                If TypeOf objItem Is VBA.Collection Then
                
                    Set objItemCol = objItem
                
                    msubGetTypeStructureInfoOfCol strTypeStructureColInfo, objItemCol
                    
                    rstrRightInfo = strTypeStructureColInfo & ")"
                    
                ElseIf TypeOf objItem Is Scripting.Dictionary Then
                
                    Set objItemDic = objItem
                    
                    ' recursive call
                    
                    msubGetTypeStructureInfoOfDic strTypeStructureDicInfo, objItemDic
                    
                    rstrRightInfo = strTypeStructureDicInfo & ")"
                Else
                
                    rstrRightInfo = TypeName(objItem) & ")"
                End If
            Else
            
                rstrRightInfo = TypeName(.Item(varKey)) & ")"
            End If
        Next
    End With
End Sub

'''
'''
'''
Private Sub msubGetTypeStructureInfoOfCol(ByRef rstrInfo As String, ByRef robjCol As VBA.Collection)

    Dim objChild As Object, strChildColInfo As String, strChildDicInfoLeft As String, strChildDicInfoRight As String, objChildDic As Scripting.Dictionary

    Dim objChildCol As VBA.Collection, strChildDicInfo As String
    

    Select Case True
    
        Case robjCol Is Nothing, robjCol.Count = 0
        
            rstrInfo = "Collection"
        
        Case Else
    
            If IsObject(robjCol.Item(1)) Then
            
                Set objChild = robjCol.Item(1)
                
                If TypeOf objChild Is VBA.Collection Then
                
                    ' recursive call
                    
                    Set objChildCol = objChild
                    
                    msubGetTypeStructureInfoOfCol strChildColInfo, objChild
                
                    rstrInfo = "Collection(Of " & strChildColInfo & ")"
                
                ElseIf TypeOf objChild Is Scripting.Dictionary Then
                
                    Set objChildDic = objChild
                
                    msubGetTypeStructureInfoOfDic strChildDicInfo, objChildDic
                    
                    rstrInfo = "Collection(Of " & strChildDicInfo & ")"
                Else
                    rstrInfo = "Collection(Of " & TypeName(robjCol.Item(1)) & ")"
                End If
            Else
                rstrInfo = "Collection(Of " & TypeName(robjCol.Item(1)) & ")"
            End If
    End Select
End Sub



'''
'''
'''
Public Function GetLineTextFromOnlyKeysOfDic(ByVal vobjDic As Scripting.Dictionary, _
        Optional ByVal vstrSurroundChar As String = "", _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True) As String

    Dim strLine As String
    Dim varKey As Variant, varItem As Variant, objRowCol As Collection
    
    strLine = ""
    
    With vobjDic
    
        For Each varKey In .Keys
        
            If strLine <> "" Then
        
                If vblnIncludeSpaceAfterDelimitedChar Then
                    
                    strLine = strLine & vstrDelimiter & " "
                Else
                    strLine = strLine & vstrDelimiter
                End If
            End If
        
            strLine = strLine & vstrSurroundChar & CStr(varKey) & vstrSurroundChar
        Next
    End With

    GetLineTextFromOnlyKeysOfDic = strLine
End Function

'''
'''
'''
Public Function GetLineTextFromOnlyItemsOfDic(ByVal vobjDic As Scripting.Dictionary, _
        Optional ByVal vstrSurroundChar As String = "", _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True) As String

    Dim strLine As String
    Dim varKey As Variant, varItem As Variant, objRowCol As Collection
    
    strLine = ""
    
    With vobjDic
    
        For Each varKey In .Keys
        
            If strLine <> "" Then
        
                If vblnIncludeSpaceAfterDelimitedChar Then
                    
                    strLine = strLine & vstrDelimiter & " "
                Else
                    strLine = strLine & vstrDelimiter
                End If
            End If
        
            If IsObject(.Item(varKey)) Then
            
                If StrComp(TypeName(.Item(varKey)), "Collection") = 0 Then
                
                    strLine = strLine & GetLineTextFromCol(.Item(varKey), vstrSurroundChar, vblnIncludeSpaceAfterDelimitedChar)
                    
                ElseIf StrComp(TypeName(.Item(varKey)), "Dictionary") = 0 Then
                    
                    ' recursive call
                    strLine = strLine & GetLineTextFromDic(.Item(varKey), vstrSurroundChar, vblnIncludeSpaceAfterDelimitedChar)
                End If
            Else
                Let varItem = .Item(varKey)
                
                Select Case True
                
                    Case IsEmpty(varItem), IsNull(varItem)
                    
                        strLine = strLine & vstrSurroundChar & vstrSurroundChar
                    Case Else
                        strLine = strLine & vstrSurroundChar & CStr(varItem) & vstrSurroundChar
                End Select
            End If
        Next
    End With

    GetLineTextFromOnlyItemsOfDic = strLine
End Function


'**---------------------------------------------
'** About line text from either Collection or Dictionary
'**---------------------------------------------
'''
'''
'''
Public Function GetLineTextFromCol(ByVal vobjCol As Collection, Optional ByVal vstrSurroundChar As String = "", Optional ByVal vstrDelimiter As String = ",", Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True) As String
    
    GetLineTextFromCol = GetLineTextWithLineFeedFromCol(vobjCol, 0, vstrSurroundChar, vstrDelimiter, vblnIncludeSpaceAfterDelimitedChar)
End Function

'''
'''
'''
Public Function GetLineTextWithLineFeedFromCol(ByVal vobjCol As Collection, Optional _
        ByVal vintCountOfOneLineElementsBeforeFeedingLine As Long = 0, _
        Optional ByVal vstrSurroundChar As String = "", _
        Optional ByVal vstrDelimiter As String = ",", _
        Optional ByVal vblnIncludeSpaceAfterDelimitedChar As Boolean = True) As String
    
    
    Dim strLine As String, varItem As Variant, strItem As String, i As Long, intCountOfElements As Long
    Dim intCountOfOneLineElementsBeforeFeedingLine As Long

    intCountOfElements = vobjCol.Count
    
    intCountOfOneLineElementsBeforeFeedingLine = 0
    
    If vintCountOfOneLineElementsBeforeFeedingLine > 0 Then
    
        intCountOfOneLineElementsBeforeFeedingLine = vintCountOfOneLineElementsBeforeFeedingLine
    End If
    
    strLine = ""
    
    If vobjCol.Count > 0 Then
    
        i = 1
        
        For Each varItem In vobjCol
            
            If IsEmpty(varItem) Or IsNull(varItem) Then
                
                strLine = strLine & vstrSurroundChar & vstrSurroundChar
                
            ElseIf IsObject(varItem) Then
            
                If varItem Is Nothing Then
                
                    strLine = strLine & vstrSurroundChar & vstrSurroundChar
                Else
                    strLine = strLine & vstrSurroundChar & CStr(varItem) & vstrSurroundChar
                End If
            Else
                strItem = CStr(varItem)
                
                strLine = strLine & vstrSurroundChar & strItem & vstrSurroundChar
            End If
            
            If intCountOfOneLineElementsBeforeFeedingLine = 0 Then
            
                If i < intCountOfElements Then
                
                    If vblnIncludeSpaceAfterDelimitedChar Then
                        
                        strLine = strLine & vstrDelimiter & " "
                    Else
                    
                        strLine = strLine & vstrDelimiter
                    End If
                End If
            ElseIf intCountOfOneLineElementsBeforeFeedingLine = 1 Then
            
                If i < intCountOfElements Then
                
                    strLine = strLine & vbNewLine
                End If
            Else
                If i < intCountOfElements Then
                
                    If (i Mod intCountOfOneLineElementsBeforeFeedingLine) = 0 Then
                    
                        strLine = strLine & vbNewLine
                    Else
                        If vblnIncludeSpaceAfterDelimitedChar Then
                        
                            strLine = strLine & vstrDelimiter & " "
                        Else
                            strLine = strLine & vstrDelimiter
                        End If
                    End If
                End If
            End If
            
            i = i + 1
        Next
    End If
    
    GetLineTextWithLineFeedFromCol = strLine
End Function


'''
''' get text with line-feeds
'''
Public Function GetTextWithOneColumnByLineFeedFromCol(ByVal vobjCol As Collection, Optional ByVal vintCountOfTabChar As Long = 0) As String

    Dim strText As String, varItem As Variant, strItem As String
    Dim i As Long, intTabCounter As Long
    
    i = 1
    
    strText = ""
    
    For Each varItem In vobjCol
    
        If Not IsObject(varItem) Then
        
            If Not IsEmpty(varItem) And Not IsNull(varItem) Then
                
                If vintCountOfTabChar > 0 Then
                
                    For intTabCounter = 1 To vintCountOfTabChar
                    
                        strText = strText & vbTab
                    Next
                End If
            
                strText = strText & CStr(varItem)
                
                If i < vobjCol.Count Then
                
                    strText = strText & vbNewLine
                End If
            End If
        End If
        
        i = i + 1
    Next

    GetTextWithOneColumnByLineFeedFromCol = strText
End Function
