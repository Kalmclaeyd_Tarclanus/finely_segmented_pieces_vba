Attribute VB_Name = "CompareDictionaries"
'
'   compare plural Scripting.Dictionary from various view points
'
'   Implementation Note:
'       These equivalent implementation can be some M language of Power-Query or proper SQL query of general relational-DB,
'       Whose command statement is very shorter than VBA, after the key-values are saved as data-base table or Excel-sheet query-table.
'       But these are independent of either Excel or ADO or some data-base,
'       therefore these can be used Word VBA, PowerPoint VBA, Outlook VBA without neither Excel, or database
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on Scripting.Dictionary
'       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Fri, 24/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** clear dictionary cache
'**---------------------------------------------
'''
'''
'''
Public Sub ClearDictionaryCache(ByRef robjDic As Scripting.Dictionary)

    If Not robjDic Is Nothing Then
    
        robjDic.RemoveAll
        
        Set robjDic = Nothing
    End If
End Sub

'**---------------------------------------------
'** check internal Dictionary
'**---------------------------------------------
'''
'''
'''
''' <Argument>vobjDic: Input</Argument>
''' <Return>Boolean</Return>
Public Function IsDictionaryValuesNestedCollection(ByVal vobjDic As Scripting.Dictionary) As Boolean

    Dim varKey As Variant, blnIsNestedCollection As Boolean
    
    blnIsNestedCollection = False

    If Not vobjDic Is Nothing Then
        
        If vobjDic.Count > 0 Then
        
            With vobjDic
            
                For Each varKey In .Keys
                    
                    If IsObject(.Item(varKey)) Then
                        
                        If StrComp(TypeName(.Item(varKey)), "Collection") = 0 Then
                            
                            blnIsNestedCollection = True
                        End If
                    End If
                    
                    Exit For
                Next
            End With
        End If
    End If

    IsDictionaryValuesNestedCollection = blnIsNestedCollection
End Function

'**---------------------------------------------
'** extract some characteristics from Dictionary
'**---------------------------------------------
'''
'''
'''
''' <Argument>rvarMax: Output</Argument>
''' <Argument>rvarMin: Output</Argument>
''' <Argument>robjDic: Input</Argument>
Public Sub GetMaximumAndMinimumFromDictionaryItems(ByRef rvarMax As Variant, _
        ByRef rvarMin As Variant, _
        ByRef robjDic As Scripting.Dictionary)

    Dim varKey As Variant, varItem As Variant, i As Long
    
    With robjDic
    
        i = 1
    
        For Each varKey In .Keys
        
            Let varItem = .Item(varKey)
        
            If i = 1 Then
            
                rvarMax = varItem
                
                rvarMin = varItem
            Else
                If rvarMin > varItem Then
                
                    rvarMin = varItem
                
                ElseIf rvarMax < varItem Then
                
                    rvarMax = varItem
                End If
            End If
        
            i = i + 1
        Next
    End With
End Sub


'**---------------------------------------------
'** compare dictionaries about both key and value
'**---------------------------------------------
'''
'''
'''
''' <Argument>robjDic01: Input</Argument>
''' <Argument>robjDic02: Input</Argument>
''' <Return>Boolean</Return>
Public Function IsAnyDifferencesBetweenTwoDictionariesAboutBothKeyAndValue(ByRef robjDic01 As Scripting.Dictionary, _
        ByRef robjDic02 As Scripting.Dictionary) As Boolean


    Dim blnHasSomeDifferences As Boolean, varKey01 As Variant, varValue01 As Variant, varValue02 As Variant
    
    blnHasSomeDifferences = False

    If Not robjDic01 Is Nothing And Not robjDic02 Is Nothing Then
    
        If robjDic01.Count = robjDic02.Count Then
        
            With robjDic01
            
                For Each varKey01 In .Keys
                
                    If Not robjDic02.Exists(varKey01) Then
                    
                        blnHasSomeDifferences = True
                    
                        Exit For
                    End If
                
                    If Not IsObject(.Item(varKey01)) Then
                
                        varValue01 = .Item(varKey01)
                    
                        varValue02 = robjDic02.Item(varKey01)
                        
                        If varValue01 <> varValue02 Then
                        
                            blnHasSomeDifferences = True
                    
                            Exit For
                        End If
                    Else
                        If TypeName(.Item(varKey01)) <> TypeName(robjDic02.Item(varKey01)) Then
                        
                            blnHasSomeDifferences = True
                    
                            Exit For
                        Else
                            Select Case TypeName(.Item(varKey01))
                            
                                Case "Dictionary"
                            
                                    ' recursive-call
                                    
                                    If IsAnyDifferencesBetweenTwoDictionariesAboutBothKeyAndValue(.Item(varKey01), robjDic02.Item(varKey01)) Then
                                    
                                        blnHasSomeDifferences = True
                    
                                        Exit For
                                    End If
                            
                                Case "Collection"
                            
                                    If Not AreTwoColsExactlyMatched(.Item(varKey01), robjDic02.Item(varKey01)) Then
                                    
                                        blnHasSomeDifferences = True
                    
                                        Exit For
                                    End If
                                    
                                Case Else
                                
                                    ' No implementation yet
                        
                            End Select
                        End If
                    
                        ' No implementation yet
                    
                    End If
                Next
            End With
        Else
            blnHasSomeDifferences = True
        End If
    Else
        If robjDic01 Is Nothing And robjDic02 Is Nothing Then
        
        Else
            blnHasSomeDifferences = True
        End If
    End If

    IsAnyDifferencesBetweenTwoDictionariesAboutBothKeyAndValue = blnHasSomeDifferences
End Function


'**---------------------------------------------
'** compare dictionaries about only keys
'**---------------------------------------------
'''
'''
'''
''' <Argument>robjCommonKeysDictionaries: Output</Argument>
''' <Argument>robjPartialCommonKeysDictionaries: Output</Argument>
''' <Argument>robjIndependentKeysDictionaries: Output</Argument>
''' <Argument>vobjInputDictionaries: Input</Argument>
''' <Argument>vobjKeysOfInputDictionaries: Input</Argument>
Public Sub CompareKeysOfEachDictionaries(ByRef robjCommonKeysDictionaries As Scripting.Dictionary, _
        ByRef robjPartialCommonKeysDictionaries As Scripting.Dictionary, _
        ByRef robjIndependentKeysDictionaries As Scripting.Dictionary, _
        ByVal vobjInputDictionaries As Collection, _
        Optional ByVal vobjKeysOfInputDictionaries As Collection = Nothing)

    msubCompareKeysOfEachDictionaries robjCommonKeysDictionaries, _
            robjPartialCommonKeysDictionaries, _
            robjIndependentKeysDictionaries, _
            vobjInputDictionaries, _
            vobjKeysOfInputDictionaries
End Sub


'''
'''
'''
''' <Argument>robjCommonKeysDictionaries: Output</Argument>
''' <Argument>robjPartialCommonKeysDictionaries: Output</Argument>
''' <Argument>robjIndependentKeysDictionaries: Output</Argument>
''' <Argument>vobjInputNestedDictionaries: Input</Argument>
Public Sub CompareKeysOfEachNestedDictionaries(ByRef robjCommonKeysDictionaries As Scripting.Dictionary, _
        ByRef robjPartialCommonKeysDictionaries As Scripting.Dictionary, _
        ByRef robjIndependentKeysDictionaries As Scripting.Dictionary, _
        ByVal vobjInputNestedDictionaries As Scripting.Dictionary)

    msubCompareKeysOfEachNestedDictionaries robjCommonKeysDictionaries, _
            robjPartialCommonKeysDictionaries, _
            robjIndependentKeysDictionaries, _
            vobjInputNestedDictionaries
End Sub



Public Function GetCommonKeyToAlignedValuesDic(ByVal vobjIndexToCommonKeyAndValueDic As Scripting.Dictionary, ByVal vintMaxOfInputDictionaries As Long, Optional ByVal vobjKeysOfInputDictionaries As Collection = Nothing, Optional ByVal vstrMergeColumnIndexesDelimitedComma As String = "1", Optional ByVal vvarNullValue As Variant = Empty) As Scripting.Dictionary

    Set GetCommonKeyToAlignedValuesDic = mfobjGetCommonKeyToAlignedValuesDic(vobjIndexToCommonKeyAndValueDic, vintMaxOfInputDictionaries, vobjKeysOfInputDictionaries, vstrMergeColumnIndexesDelimitedComma)
End Function


'''
'''
'''
''' <Argument>robjCommonKeyDic01: Output</Argument>
''' <Argument>robjCommonKeyDic02: Output</Argument>
''' <Argument>robjIndependentKeyDic01: Output</Argument>
''' <Argument>robjIndependentKeyDic02: Output</Argument>
''' <Argument>vobjInputDic01: Input</Argument>
''' <Argument>vobjInputDic02: Input</Argument>
''' <Argument>vvarKeyOfInputDic01: Input</Argument>
''' <Argument>vvarKeyOfInputDic02: Input</Argument>
Public Sub CompareKeysOfTwoDictionaries(ByRef robjCommonKeyDic01 As Scripting.Dictionary, _
        ByRef robjCommonKeyDic02 As Scripting.Dictionary, _
        ByRef robjIndependentKeyDic01 As Scripting.Dictionary, _
        ByRef robjIndependentKeyDic02 As Scripting.Dictionary, _
        ByVal vobjInputDic01 As Scripting.Dictionary, _
        ByVal vobjInputDic02 As Scripting.Dictionary, _
        Optional ByVal vvarKeyOfInputDic01 As Variant = 1, _
        Optional ByVal vvarKeyOfInputDic02 As Variant = 2)

    msubCompareKeysOfTwoDictionaries robjCommonKeyDic01, _
            robjCommonKeyDic02, _
            robjIndependentKeyDic01, _
            robjIndependentKeyDic02, _
            vobjInputDic01, _
            vobjInputDic02, _
            vvarKeyOfInputDic01, _
            vvarKeyOfInputDic02
End Sub


'''
''' This expects the values of input-dictionary is not object type.
'''
''' <Argument>vobjInputDic: Input</Argument>
''' <Return>Dictionary(Of Key, Of Value)</Return>
Public Function GetInverseDictionaryAsDictionaryFromValueToKeyWithoutDuplicatedValues(ByVal vobjInputDic As Scripting.Dictionary) As Scripting.Dictionary

    Dim varKey As Variant, varItem As Variant
    
    Dim objOutputDic As Scripting.Dictionary
    
    
    Set objOutputDic = New Scripting.Dictionary
    
    With vobjInputDic
        
        For Each varKey In .Keys
        
            Let varItem = .Item(varKey)
    
            With objOutputDic
            
                If Not .Exists(varItem) Then
                
                    .Add varItem, varKey
                End If
            End With
        Next
    End With

    Set GetInverseDictionaryAsDictionaryFromValueToKeyWithoutDuplicatedValues = objOutputDic
End Function

'''
''' This expects the values of input-dictionary is not object type.
'''
''' <Argument>vobjInputDic: input</Argument>
''' <Return>Dictionary(Of Key, Of Collection(Of Value))</Return>
Public Function GetInverseDictionaryAsCollectionFromValueToKey(ByVal vobjInputDic As Scripting.Dictionary) As Scripting.Dictionary

    Dim varKey As Variant, varItem As Variant
    Dim objOutputDic As Scripting.Dictionary, objChildCol As Collection
    
    Set objOutputDic = New Scripting.Dictionary
    
    With vobjInputDic
        
        For Each varKey In .Keys
        
            Let varItem = .Item(varKey)
    
            With objOutputDic
            
                If Not .Exists(varItem) Then
                
                    Set objChildCol = New Collection
                    
                    objChildCol.Add varKey
                
                    .Add varItem, objChildCol
                Else
                    Set objChildCol = .Item(varItem)
                
                    objChildCol.Add varKey
                    
                    Set .Item(varItem) = objChildCol
                End If
            End With
        Next
    End With

    Set GetInverseDictionaryAsCollectionFromValueToKey = objOutputDic
End Function


'''
''' get inverse Dictionary(Of Variant[Key], Collection(Of Variant[Value]))
'''
''' <Argument>vobjDataTableCol: Input - Collection(Of Collection(Of Variant))</Argument>
''' <Return>Dictionary: Output - Dictionary(Of Key, Of Collection(Of Variant))</Return>
Public Function GetInverseDictionaryFrom2ColumnDataTableCollection(ByVal vobjDataTableCol As Collection) As Scripting.Dictionary

    Dim varRowCol As Variant, objRowCol As Collection
    
    Dim objDic As Scripting.Dictionary, varKey As Variant, varItem As Variant, objItems As Collection
    
    
    Set objDic = New Scripting.Dictionary
    
    If Not vobjDataTableCol Is Nothing Then
        
        If vobjDataTableCol.Count > 0 Then
            
            For Each varRowCol In vobjDataTableCol
        
                Set objRowCol = varRowCol
        
                varKey = objRowCol.Item(2)  ' second item
                
                varItem = objRowCol.Item(1) ' first item
        
                With objDic
                
                    If Not .Exists(varKey) Then
                    
                        Set objItems = New Collection
                        
                        objItems.Add varItem
                        
                        .Add varKey, objItems
                    Else
                        Set objItems = .Item(varKey)
                    
                        objItems.Add varItem
                        
                        Set .Item(varKey) = objItems
                    End If
                End With
            Next
        End If
    End If

    Set GetInverseDictionaryFrom2ColumnDataTableCollection = objDic
End Function



'''
'''
'''
''' <Argument>robjKeys: Output</Argument>
''' <Argument>robjValues: Output</Argument>
''' <Argument>vobjDic: Input</Argument>
Public Sub GetDividedTwoCollectionsFromDictionary(ByRef robjKeys As Collection, ByRef robjValues As Collection, ByVal vobjDic As Scripting.Dictionary)

    Dim varKey As Variant
    
    Select Case True
    
        Case vobjDic Is Nothing, vobjDic.Count = 0
        
        Case Else
    
            If robjKeys Is Nothing Then Set robjKeys = New Collection
            
            If robjValues Is Nothing Then Set robjValues = New Collection
            
            With vobjDic
            
                For Each varKey In .Keys
                
                    robjKeys.Add varKey
                    
                    robjValues.Add .Item(varKey)
                Next
            End With
    End Select
End Sub

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
'''
'''
''' <Argument>vobjParentKeyToCommonKeyAndValueDic: Input</Argument>
''' <Argument>vintMaxOfInputDictionaries: Input</Argument>
''' <Argument>vobjKeysOfInputDictionaries: Input</Argument>
''' <Argument>vstrMergeColumnIndexesDelimitedComma: Input</Argument>
''' <Argument>vvarNullValue: Input</Argument>
Private Function mfobjGetCommonKeyToAlignedValuesDic(ByVal vobjParentKeyToCommonKeyAndValueDic As Scripting.Dictionary, _
        ByVal vintMaxOfInputDictionaries As Long, _
        Optional ByVal vobjKeysOfInputDictionaries As Collection = Nothing, _
        Optional ByVal vstrMergeColumnIndexesDelimitedComma As String = "1", _
        Optional ByVal vvarNullValue As Variant = Empty) As Scripting.Dictionary
    
    
    
    Dim objOutputDic As Scripting.Dictionary, objInputDic As Scripting.Dictionary, i As Long, intColumnIndexesArray() As Long
    
    Dim objInputRowCol As Collection, varValue As Variant, objOutputRowCol As Collection
    
    Dim varChildKey As Variant, varIndex As Variant, intIndex As Long
    
    Dim intColumnIndex As Long, blnIsNestedCollection As Boolean
    
    Dim objKeysOfInputDictionaries As Collection, varParentKey As Variant
    
    
    If Not vobjKeysOfInputDictionaries Is Nothing Then
    
        Set objKeysOfInputDictionaries = vobjKeysOfInputDictionaries
    Else
        Set objKeysOfInputDictionaries = New Collection
        
        With objKeysOfInputDictionaries
        
            For i = 1 To vintMaxOfInputDictionaries
            
                .Add i
            Next
        End With
    End If
    
    If Not vobjParentKeyToCommonKeyAndValueDic Is Nothing Then
    
        If vobjParentKeyToCommonKeyAndValueDic.Count > 0 Then
        
            GetIntegerArrayFromDelimitedCommaString intColumnIndexesArray, vstrMergeColumnIndexesDelimitedComma
        
            ' check values type
            blnIsNestedCollection = IsDictionaryValuesNestedCollection(vobjParentKeyToCommonKeyAndValueDic)
            
            With vobjParentKeyToCommonKeyAndValueDic
                
            
                For Each varChildKey In mfobjGetOnlyValuesChildDictionaryKeysFromDoubleNestedDictionary(vobjParentKeyToCommonKeyAndValueDic).Keys
                
                    Set objOutputRowCol = New Collection
                    
                    For Each varParentKey In objKeysOfInputDictionaries
            
                        If .Exists(varParentKey) Then
                        
                            Set objInputDic = .Item(varParentKey)
                            
                            With objInputDic
                            
                                If .Exists(varChildKey) Then
                            
                                    If blnIsNestedCollection Then
                                    
                                        Set objInputRowCol = objInputDic.Item(varChildKey)
                                    Else
                                        Let varValue = objInputDic.Item(varChildKey)
                                    End If
                                
                                    For intColumnIndex = LBound(intColumnIndexesArray) To UBound(intColumnIndexesArray)
                            
                                        If blnIsNestedCollection Then
                                        
                                            objOutputRowCol.Add objInputRowCol.Item(intColumnIndex)
                                        Else
                                            objOutputRowCol.Add varValue
                                        End If
                                    Next
                                End If
                            End With
                        Else
                        
                            ' if vobjParentKeyToCommonKeyAndValueDic has some partially common parts, the following code will be executed.
                            
                            For intColumnIndex = LBound(intColumnIndexesArray) To UBound(intColumnIndexesArray)
                            
                                objOutputRowCol.Add vvarNullValue
                            Next
                        End If
                    Next
                    
                    If objOutputDic Is Nothing Then Set objOutputDic = New Scripting.Dictionary
                    
                    objOutputDic.Add varChildKey, objOutputRowCol
                Next
            End With
        End If
    End If

    Set mfobjGetCommonKeyToAlignedValuesDic = objOutputDic
End Function

'''
'''
'''
''' <Argument>vobjDoubleNestedDictionary: Input</Argument>
''' <Argument>vvarEmptyValue: Input</Argument>
''' <Return>Scripting.Dictionary</Return>
Private Function mfobjGetOnlyValuesChildDictionaryKeysFromDoubleNestedDictionary(ByVal vobjDoubleNestedDictionary As Scripting.Dictionary, _
        Optional ByVal vvarEmptyValue As Variant = 0) As Scripting.Dictionary

    Dim objOutputDic As Scripting.Dictionary
    Dim varParentKey As Variant, varNestedDictionaryKey As Variant
    Dim objChildDic As Scripting.Dictionary
    
    
    Set objOutputDic = New Scripting.Dictionary

    If Not vobjDoubleNestedDictionary Is Nothing Then
    
        If vobjDoubleNestedDictionary.Count > 0 Then
        
            With vobjDoubleNestedDictionary
            
                For Each varParentKey In .Keys
                
                    Set objChildDic = .Item(varParentKey)
                    
                    With objChildDic
                    
                        For Each varNestedDictionaryKey In .Keys
                    
                            With objOutputDic
                            
                                If Not .Exists(varNestedDictionaryKey) Then
                                
                                    .Add varNestedDictionaryKey, vvarEmptyValue
                                End If
                            End With
                        Next
                    End With
                Next
            End With
        End If
    End If

    Set mfobjGetOnlyValuesChildDictionaryKeysFromDoubleNestedDictionary = objOutputDic
End Function



'''
''' Either any objects or not-objects of Values are allowed.
''' If values are not objects, this expects the Collection type
'''
''' <Argument>robjCommonKeysDictionaries Dictionary(Of KeyIndex, Of Collection(Of Dictionary(Of Key, Of Value)))): Output, common key in all input dictionaries</Argument>
''' <Argument>robjPartialCommonKeysDictionaries: Output, partial common key, the 'partial common' stands for common in a part of dictionaries, not common in all input dictionaries</Argument>
''' <Argument>robjIndependentKeysDictionaries: Output, Dictionary(Of KeyIndex, Of Dictionary(Of Key, Of Value)) </Argument>
''' <Argument>vobjInputDictionaries: Input, Collection(Of Dictinary(Of Key, Of Value))</Argument>
''' <Argument>vobjKeysOfInputDictionaries: Input, Collection(Of Key[Variant])</Argument>
Private Sub msubCompareKeysOfEachDictionaries(ByRef robjCommonKeysDictionaries As Scripting.Dictionary, _
        ByRef robjPartialCommonKeysDictionaries As Scripting.Dictionary, _
        ByRef robjIndependentKeysDictionaries As Scripting.Dictionary, _
        ByVal vobjInputDictionaries As Collection, _
        Optional ByVal vobjKeysOfInputDictionaries As Collection = Nothing)


    Dim objKeyToCountDic As Scripting.Dictionary
    
    ' scan input-dictionaries
    msubGetKeyToIncludedCountDic objKeyToCountDic, vobjInputDictionaries
    
    ' get reverse dic of objKeyToCountDic
    
    msubGetClassifiedDictionariesAboutCommonKeys robjCommonKeysDictionaries, _
            robjPartialCommonKeysDictionaries, _
            robjIndependentKeysDictionaries, _
            objKeyToCountDic, _
            vobjInputDictionaries, _
            vobjKeysOfInputDictionaries
End Sub


'''
'''
'''
''' <Argument>robjCommonKeysDictionaries: Output - Dictionary(Of KeyIndex, Of Collection(Of Dictionary(Of Key, Of Value)))), common key in all input dictionaries</Argument>
''' <Argument>robjPartialCommonKeysDictionaries: Output, partial common key, the 'partial common' stands for common in a part of dictionaries, not common in all input dictionaries</Argument>
''' <Argument>robjIndependentKeysDictionaries: Output, Dictionary(Of KeyIndex, Of Dictionary(Of Key, Of Value)) </Argument>
''' <Argument>vobjInputNestedDictionaries: Input, Dictionary(Of Key, Of Dictinary(Of Key, Of Value))</Argument>
Private Sub msubCompareKeysOfEachNestedDictionaries(ByRef robjCommonKeysDictionaries As Scripting.Dictionary, _
        ByRef robjPartialCommonKeysDictionaries As Scripting.Dictionary, _
        ByRef robjIndependentKeysDictionaries As Scripting.Dictionary, _
        ByVal vobjInputNestedDictionaries As Scripting.Dictionary)

    Dim objInputDictionaries As Collection, objKeysOfInputDictionaries As Collection

    GetDividedTwoCollectionsFromDictionary objKeysOfInputDictionaries, objInputDictionaries, vobjInputNestedDictionaries

    msubCompareKeysOfEachDictionaries robjCommonKeysDictionaries, _
            robjPartialCommonKeysDictionaries, _
            robjIndependentKeysDictionaries, _
            objInputDictionaries, _
            objKeysOfInputDictionaries
End Sub

'''
'''
'''
''' <Argument>robjIndexToCommonKeyAndValueDic: Output</Argument>
''' <Argument>robjIndexToPartialCommonKeyAndValueDic: Output</Argument>
''' <Argument>robjIndexToIndependentKeyAndValueDic: Output</Argument>
''' <Argument>vobjKeyToCountDic: Input</Argument>
''' <Argument>vobjInputDictionaries: Input</Argument>
''' <Argument>vobjKeysOfInputDictionaries: Input</Argument>
Private Sub msubGetClassifiedDictionariesAboutCommonKeys(ByRef robjIndexToCommonKeyAndValueDic As Scripting.Dictionary, _
        ByRef robjIndexToPartialCommonKeyAndValueDic As Scripting.Dictionary, _
        ByRef robjIndexToIndependentKeyAndValueDic As Scripting.Dictionary, _
        ByVal vobjKeyToCountDic As Scripting.Dictionary, _
        ByVal vobjInputDictionaries As Collection, _
        Optional ByVal vobjKeysOfInputDictionaries As Collection = Nothing)


    Dim intMaxOfInputDictionaries As Long, intIncludedCountOfKey As Long
    
    Dim varKey As Variant
    

    intMaxOfInputDictionaries = vobjInputDictionaries.Count
    
    For Each varKey In vobjKeyToCountDic.Keys
    
        intIncludedCountOfKey = vobjKeyToCountDic.Item(varKey)
    
        Select Case intIncludedCountOfKey
        
            Case 1
                
                ' independent key
                msubAddIndexToSomeTypeKeyAndValueDicFromInputDictionaries robjIndexToIndependentKeyAndValueDic, varKey, vobjInputDictionaries, vobjKeysOfInputDictionaries

            Case intMaxOfInputDictionaries
                
                ' common in all input-dictionaries
                msubAddIndexToSomeTypeKeyAndValueDicFromInputDictionaries robjIndexToCommonKeyAndValueDic, varKey, vobjInputDictionaries, vobjKeysOfInputDictionaries
                
            Case Else
                
                ' partial common, (common in a part of input-dictionaries)
                msubAddIndexToSomeTypeKeyAndValueDicFromInputDictionaries robjIndexToPartialCommonKeyAndValueDic, varKey, vobjInputDictionaries, vobjKeysOfInputDictionaries
        End Select
    Next
End Sub

'''
'''
'''
''' <Argument>robjKeyToCountDic: Output</Argument>
''' <Argument>vobjInputDictionaries: Input</Argument>
Private Sub msubGetKeyToIncludedCountDic(ByRef robjKeyToCountDic As Scripting.Dictionary, _
        ByVal vobjInputDictionaries As Collection)

    Dim objInputDic As Scripting.Dictionary, varInputDic As Variant
    
    Dim varKey As Variant, i As Long


    Select Case True
    
        Case vobjInputDictionaries Is Nothing, vobjInputDictionaries.Count = 0
        
            ' irregular case
            
        Case Else
        
            Set robjKeyToCountDic = New Scripting.Dictionary
            
            i = 1
            
            For Each varInputDic In vobjInputDictionaries
            
                Set objInputDic = varInputDic
                
                For Each varKey In objInputDic.Keys
                
                    With robjKeyToCountDic
                    
                        If Not .Exists(varKey) Then
                        
                            .Add varKey, 1
                        Else
                            .Item(varKey) = CLng(.Item(varKey)) + 1
                        End If
                    End With
                Next
                
                i = i + 1
            Next
    End Select
End Sub


'''
'''
'''
''' <Argument>robjIndexToSomeTypeKeyAndValueDic: Output</Argument>
''' <Argument>vvarKey: Input</Argument>
''' <Argument>vobjInputDictionaries: Input - Collection(Of Dictionary(Of Key, Value))</Argument>
''' <Argument>vobjKeysOfInputDictionaries: Input</Argument>
Private Sub msubAddIndexToSomeTypeKeyAndValueDicFromInputDictionaries(ByRef robjIndexToSomeTypeKeyAndValueDic As Scripting.Dictionary, _
        ByVal vvarKey As Variant, _
        ByVal vobjInputDictionaries As Collection, _
        Optional ByVal vobjKeysOfInputDictionaries As Collection = Nothing)


    Dim i As Long, varInputDic As Variant, objInputDic As Scripting.Dictionary
    Dim objPartDic As Scripting.Dictionary, blnIsParentKeyOfEachInputDicObjectExisted As Boolean
    Dim varChildKey As Variant


    If robjIndexToSomeTypeKeyAndValueDic Is Nothing Then Set robjIndexToSomeTypeKeyAndValueDic = New Scripting.Dictionary

    blnIsParentKeyOfEachInputDicObjectExisted = False
    
    If Not vobjKeysOfInputDictionaries Is Nothing Then
    
        blnIsParentKeyOfEachInputDicObjectExisted = True
    End If
    
    
    i = 1
    
    For Each varInputDic In vobjInputDictionaries
    
        Set objInputDic = varInputDic
        
        If objInputDic.Exists(vvarKey) Then
        
            If blnIsParentKeyOfEachInputDicObjectExisted Then
                
                varChildKey = vobjKeysOfInputDictionaries.Item(i)
            Else
                varChildKey = i
            End If
        
            With robjIndexToSomeTypeKeyAndValueDic
            
                If Not .Exists(varChildKey) Then
                
                    Set objPartDic = New Scripting.Dictionary
                
                    objPartDic.Add vvarKey, objInputDic.Item(vvarKey)
                    
                    .Add varChildKey, objPartDic
                Else
                    Set objPartDic = .Item(varChildKey)
                    
                    objPartDic.Add vvarKey, objInputDic.Item(vvarKey)
                
                    Set .Item(varChildKey) = objPartDic
                End If
            End With
        End If
        
        i = i + 1
    Next
End Sub


'''
''' reduction version of msubCompareKeysOfEachDictionaries
'''
''' <Argument>robjCommonKeyDic01: Output</Argument>
''' <Argument>robjCommonKeyDic02: Output</Argument>
''' <Argument>robjIndependentKeyDic01: Output</Argument>
''' <Argument>robjIndependentKeyDic02: Output</Argument>
''' <Argument>robjInputDic01: Input</Argument>
''' <Argument>robjInputDic02: Input</Argument>
''' <Argument>vvarKeyOfInputDic01: Input</Argument>
''' <Argument>vvarKeyOfInputDic02: Input</Argument>
Private Sub msubCompareKeysOfTwoDictionaries(ByRef robjCommonKeyDic01 As Scripting.Dictionary, _
        ByRef robjCommonKeyDic02 As Scripting.Dictionary, _
        ByRef robjIndependentKeyDic01 As Scripting.Dictionary, _
        ByRef robjIndependentKeyDic02 As Scripting.Dictionary, _
        ByRef robjInputDic01 As Scripting.Dictionary, _
        ByRef robjInputDic02 As Scripting.Dictionary, _
        Optional ByVal vvarKeyOfInputDic01 As Variant = 1, _
        Optional ByVal vvarKeyOfInputDic02 As Variant = 2)


    Dim objInputCol As Collection, objCommonKeysDictionaries As Scripting.Dictionary, objIndependentKeysDictionaries As Scripting.Dictionary
    
    Dim objKeysOfInputDictionaries As Collection
    
    
    If vvarKeyOfInputDic01 <> 1 And vvarKeyOfInputDic02 <> 2 Then
    
        Set objKeysOfInputDictionaries = New Collection
        
        With objKeysOfInputDictionaries
        
            .Add vvarKeyOfInputDic01: .Add vvarKeyOfInputDic02
        End With
    End If
    
    Set objInputCol = New Collection
    
    With objInputCol
    
        If Not robjInputDic01 Is Nothing Then
        
            .Add robjInputDic01
        Else
            .Add New Scripting.Dictionary
        End If
        
        If Not robjInputDic02 Is Nothing Then
        
            .Add robjInputDic02
        Else
            .Add New Scripting.Dictionary
        End If
    End With

    msubCompareKeysOfEachDictionaries objCommonKeysDictionaries, Nothing, objIndependentKeysDictionaries, objInputCol, objKeysOfInputDictionaries


    Select Case True
    
        Case objCommonKeysDictionaries Is Nothing, objCommonKeysDictionaries.Count = 0
    
        Case Else
        
            With objCommonKeysDictionaries
                
                If .Exists(1) Then Set robjCommonKeyDic01 = .Item(1)
                
                If .Exists(2) Then Set robjCommonKeyDic02 = .Item(2)
            End With
    End Select

    If robjCommonKeyDic01 Is Nothing Then Set robjCommonKeyDic01 = New Scripting.Dictionary
    
    If robjCommonKeyDic02 Is Nothing Then Set robjCommonKeyDic02 = New Scripting.Dictionary

    Select Case True
    
        Case objIndependentKeysDictionaries Is Nothing, objIndependentKeysDictionaries.Count = 0
        
        Case Else
    
            With objIndependentKeysDictionaries
            
                If .Exists(1) Then Set robjIndependentKeyDic01 = .Item(1)
                
                If .Exists(2) Then Set robjIndependentKeyDic02 = .Item(2)
            End With
    End Select

    If robjIndependentKeyDic01 Is Nothing Then Set robjIndependentKeyDic01 = New Scripting.Dictionary
    
    If robjIndependentKeyDic02 Is Nothing Then Set robjIndependentKeyDic02 = New Scripting.Dictionary
End Sub
