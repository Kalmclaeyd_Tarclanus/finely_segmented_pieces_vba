'
'   About [PowerPointCommonCore], loading specified VBA source files set and necessary VB project COM components references group names string
'
'   Coding Conventions Note:
'       This source code includes a system-Hungarian notations.
'       For compatibility from the relative VBA codes, this source may look like having some types.
'       However, the VBScript has no type system, and all variables can be interpreted as the Variant type on VBA.
'
'   Dependency Abstract:
'       This is referred from LoadVBAToOfficeFile.wsf
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 28/Aug/2024    Tarclanus-generator     Generated at 08:25 AM.
'

Option Explicit

'''
''' About "PowerPointCommonCore", get importing VB component file names and COM references group names
'''
Public Sub GetLoadingVBComponentFileNamesAndRefGroupsInfoPowerPointCommonCore(ByRef robjFileNameKeysDic, ByRef rstrRefGroupsDelimitedByComma)

    ' Count of setting VB project reference group names: 7

    rstrRefGroupsDelimitedByComma = "ADO,ComFewest,MSComctlLib,MSExcel,MSForms,MSPowerPoint,msWMI"

    Set robjFileNameKeysDic = CreateObject("Scripting.Dictionary")

    ' Count of loading VBA source files: 115

    With robjFileNameKeysDic

        .Add "ADOConStrToolsForExcelBookBase.bas", 0

        .Add "ADOParameters.bas", 0

        .Add "AxBarBtnEvHdlForTxtClipBoard.cls", 0

        .Add "ColorDefinedByUser.bas", 0

        .Add "ColorToConvert.bas", 0

        .Add "ColumnsDataConvert.bas", 0

        .Add "CompareDictionaries.bas", 0

        .Add "CompareTextFiles.bas", 0

        .Add "ConvertDicToWinReg.bas", 0

        .Add "ConvertTextFile.bas", 0

        .Add "CoreFunctionFeatures.bas", 0

        .Add "CreateRandomDataTable.bas", 0

        .Add "CreateRandomSequence.bas", 0

        .Add "CreateRandomValue.bas", 0

        .Add "CurrentLocaleUtility.bas", 0

        .Add "CurrentLocaleUtilityForWin.bas", 0

        .Add "CurrentLocalize.bas", 0

        .Add "CurrentUserDomain.cls", 0

        .Add "CurrentUserDomainUtility.bas", 0

        .Add "CurrentUserDrivesUtility.bas", 0

        .Add "DataTableCompressing.bas", 0

        .Add "DataTableListBox.bas", 0

        .Add "DataTableMerge.bas", 0

        .Add "DataTableStringIn.bas", 0

        .Add "DataTableTextOut.bas", 0

        .Add "DataTableToConvert.bas", 0

        .Add "DateText.bas", 0

        .Add "DecorationSetterToShape.bas", 0

        .Add "DecorationSetterToShapeForPP.bas", 0

        .Add "DoEventsSpecial.bas", 0

        .Add "DoubleStopWatch.cls", 0

        .Add "DumpModule.bas", 0

        .Add "DumpModuleOfCurrentApplication.bas", 0

        .Add "DumpModuleToTextFile.bas", 0

        .Add "EnumerateAppWindows.bas", 0

        .Add "EnumerateAppWindowsBasic.bas", 0

        .Add "EnumerateChildWindows.bas", 0

        .Add "ExternalTextEditors.bas", 0

        .Add "FileSysBackup.bas", 0

        .Add "FileSysCompare.bas", 0

        .Add "FileSysCopyProgressBar.bas", 0

        .Add "FileSysFilesInfo.bas", 0

        .Add "FileSysListFilesByFSO.bas", 0

        .Add "FileSysListFilesByVBADir.bas", 0

        .Add "FileSysSearchCondUtil.bas", 0

        .Add "FileSysSearchCondition.cls", 0

        .Add "FileSysSearchResult.cls", 0

        .Add "FileSysSyncCopy.bas", 0

        .Add "FileSysSyncCopyForTextFiles.bas", 0

        .Add "FindKeywords.bas", 0

        .Add "FindURLAndEmailAddresses.bas", 0

        .Add "FormTopMostEnabledCtlHdr.cls", 0

        .Add "FormTopMostEnabledUtilities.bas", 0

        .Add "InitRegParamOfStorageUNCPath.bas", 0

        .Add "InputPasswordBox.bas", 0

        .Add "InterfaceCall.bas", 0

        .Add "InterfaceCallForUI.bas", 0

        .Add "LoadAndModifyPresentation.bas", 0

        .Add "LoadTextFiles.bas", 0

        .Add "LocalInstalledMsOfficeSofts.bas", 0

        .Add "LocalInstalledSofts.bas", 0

        .Add "LocalInstalledSoftsForVBS.bas", 0

        .Add "LocalizationADOConnector.cls", 0

        .Add "LogTextForm.frm", 0

        .Add "ModifyFormsByWinAPI.bas", 0

        .Add "OfficeFileSecurity.bas", 0

        .Add "OfficePresentationSecurity.bas", 0

        .Add "OperateWinProcessByAPI.bas", 0

        .Add "OperateWinShell.bas", 0

        .Add "OperateWinShutDown.bas", 0

        .Add "OperateWinWindowPos.bas", 0

        .Add "RClickOnAxCtl.bas", 0

        .Add "RegExpGeneral.bas", 0

        .Add "RegParamOfStorageUNCPath.cls", 0

        .Add "SimplePdAuthentication.bas", 0

        .Add "SolveSavePathGeneral.bas", 0

        .Add "SortGeneral.bas", 0

        .Add "TerminateProcessByVBA.bas", 0

        .Add "UInputPasswordBox.frm", 0

        .Add "UProgressBarForm.frm", 0

        .Add "UTestFormKeepStateReg.frm", 0

        .Add "UTestFormNoFormTop.frm", 0

        .Add "UTestFormTextRClickClipBoard.frm", 0

        .Add "UTestFormWithFormTop.frm", 0

        .Add "UTfColorToConvert.bas", 0

        .Add "UTfCompareTextFiles.bas", 0

        .Add "UTfCreateDataTable.bas", 0

        .Add "UTfDataTableTextOut.bas", 0

        .Add "UTfExternalTextEditors.bas", 0

        .Add "UTfFileSysFilesInfo.bas", 0

        .Add "UTfFileSysListFilesByFSO.bas", 0

        .Add "UTfFileSysListFilesByVBADir.bas", 0

        .Add "UTfFileSysSyncCopy.bas", 0

        .Add "UTfLogTextForm.bas", 0

        .Add "UTfOfficePresentationSecurity.bas", 0

        .Add "UTfRClickOnAxCtl.bas", 0

        .Add "UTfSortGeneral.bas", 0

        .Add "UTfWinAPIRegEnum.bas", 0

        .Add "UTfWinAPIRegistryRW.bas", 0

        .Add "UTfWinINIGeneral.bas", 0

        .Add "UnitTestSheetFormatSetting.cls", 0

        .Add "UnitTestWrap.bas", 0

        .Add "UnitTestWrappedParameters.cls", 0

        .Add "UserFormControlCommon.bas", 0

        .Add "VariantTypeConversion.bas", 0

        .Add "WinAPIErrorMessageGeneral.bas", 0

        .Add "WinAPIMessageWithTimeOut.bas", 0

        .Add "WinExplorer.bas", 0

        .Add "WinINIGeneral.bas", 0

        .Add "WinRegDelete.bas", 0

        .Add "WinRegGeneral.bas", 0

        .Add "WinRegKeepingFormState.bas", 0

        .Add "WinRegManagementDeclarations.bas", 0

        .Add "WinRegStorageUNCPath.bas", 0

        .Add "WinRegView.bas", 0
    End With
End Sub