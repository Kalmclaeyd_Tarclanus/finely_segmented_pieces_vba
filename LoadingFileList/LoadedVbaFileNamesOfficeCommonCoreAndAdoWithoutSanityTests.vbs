'
'   About [OfficeCommonCoreAndAdoWithoutSanityTests], loading specified VBA source files set and necessary VB project COM components references group names string
'
'   Coding Conventions Note:
'       This source code includes a system-Hungarian notations.
'       For compatibility from the relative VBA codes, this source may look like having some types.
'       However, the VBScript has no type system, and all variables can be interpreted as the Variant type on VBA.
'
'   Dependency Abstract:
'       This is referred from LoadVBAToOfficeFile.wsf
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 28/Aug/2024    Tarclanus-generator     Generated at 08:25 AM.
'

Option Explicit

'''
''' About "OfficeCommonCoreAndAdoWithoutSanityTests", get importing VB component file names and COM references group names
'''
Public Sub GetLoadingVBComponentFileNamesAndRefGroupsInfoOfficeCommonCoreAndAdoWithoutSanityTests(ByRef robjFileNameKeysDic, ByRef rstrRefGroupsDelimitedByComma)

    ' Count of setting VB project reference group names: 7

    rstrRefGroupsDelimitedByComma = "ADO,ComFewest,MSComctlLib,MSExcel,MSForms,VBIDE,msWMI"

    Set robjFileNameKeysDic = CreateObject("Scripting.Dictionary")

    ' Count of loading VBA source files: 162

    With robjFileNameKeysDic

        .Add "ADOConStrOfAceOleDb.cls", 0

        .Add "ADOConStrOfAceOleDbCsv.cls", 0

        .Add "ADOConStrOfAceOleDbXl.cls", 0

        .Add "ADOConStrOfDsn.cls", 0

        .Add "ADOConStrOfDsnlessOracle.cls", 0

        .Add "ADOConStrOfDsnlessPgSql.cls", 0

        .Add "ADOConStrOfDsnlessSqLite.cls", 0

        .Add "ADOConStrOfOdbcOracle.cls", 0

        .Add "ADOConStrOfOdbcPgSql.cls", 0

        .Add "ADOConStrOfOdbcSqLite.cls", 0

        .Add "ADOConStrOfOleDbMsAccess.cls", 0

        .Add "ADOConStrOfOleDbOracle.cls", 0

        .Add "ADOConStrToolsForAccDb.bas", 0

        .Add "ADOConStrToolsForCSV.bas", 0

        .Add "ADOConStrToolsForExcelBook.bas", 0

        .Add "ADOConStrToolsForExcelBookBase.bas", 0

        .Add "ADOConStrToolsForOracle.bas", 0

        .Add "ADOConStrToolsForPgSql.bas", 0

        .Add "ADOConStrToolsForSqLite.bas", 0

        .Add "ADOConnectingInClasses.bas", 0

        .Add "ADOConnectionSetting.cls", 0

        .Add "ADOConnectionUtilities.bas", 0

        .Add "ADOConnector.cls", 0

        .Add "ADOFailedInformation.bas", 0

        .Add "ADOParameters.bas", 0

        .Add "AccDbAdoConnector.cls", 0

        .Add "AxBarBtnEvHdlForSwitchLstPane.cls", 0

        .Add "AxBarBtnEvHdlForTxtClipBoard.cls", 0

        .Add "AxCtlLstCodeModulePaneHdl.cls", 0

        .Add "AxCtlLstRClickCodePaneMenuHdl.cls", 0

        .Add "CodePaneSwitchLstUtilities.bas", 0

        .Add "CodePaneSwitchMultiLstForm.frm", 0

        .Add "CodeVBIDECommon.bas", 0

        .Add "ColorDefinedByUser.bas", 0

        .Add "ColorToConvert.bas", 0

        .Add "ColumnsDataConvert.bas", 0

        .Add "CompareDictionaries.bas", 0

        .Add "CompareOnlySheetsByAdo.bas", 0

        .Add "CompareTextFiles.bas", 0

        .Add "ConvertDicToWinReg.bas", 0

        .Add "ConvertTextFile.bas", 0

        .Add "CoreFunctionFeatures.bas", 0

        .Add "CreateRandomDataTable.bas", 0

        .Add "CreateRandomSequence.bas", 0

        .Add "CreateRandomValue.bas", 0

        .Add "CsvAdoConnector.cls", 0

        .Add "CsvConnectingCommonPath.bas", 0

        .Add "CurrentLocaleUtility.bas", 0

        .Add "CurrentLocaleUtilityForWin.bas", 0

        .Add "CurrentLocalize.bas", 0

        .Add "CurrentUserDomain.cls", 0

        .Add "CurrentUserDomainForAdo.bas", 0

        .Add "CurrentUserDomainUtility.bas", 0

        .Add "CurrentUserDrivesUtility.bas", 0

        .Add "DataTableADOIn.bas", 0

        .Add "DataTableCompressing.bas", 0

        .Add "DataTableListBox.bas", 0

        .Add "DataTableMerge.bas", 0

        .Add "DataTableStringIn.bas", 0

        .Add "DataTableTextOut.bas", 0

        .Add "DataTableToConvert.bas", 0

        .Add "DateText.bas", 0

        .Add "DbConOracle.cls", 0

        .Add "DbConPgSql.cls", 0

        .Add "DecorationSetterToShape.bas", 0

        .Add "DoubleStopWatch.cls", 0

        .Add "DumpModule.bas", 0

        .Add "EnumerateAppWindows.bas", 0

        .Add "EnumerateAppWindowsBasic.bas", 0

        .Add "EnumerateChildWindows.bas", 0

        .Add "ErrorADOSQL.cls", 0

        .Add "ErrorContentKeeper.cls", 0

        .Add "ExternalTextEditors.bas", 0

        .Add "FileSysBackup.bas", 0

        .Add "FileSysCompare.bas", 0

        .Add "FileSysConvertCodePages.bas", 0

        .Add "FileSysCopyProgressBar.bas", 0

        .Add "FileSysFilesInfo.bas", 0

        .Add "FileSysListFilesByFSO.bas", 0

        .Add "FileSysListFilesByVBADir.bas", 0

        .Add "FileSysSearchCondUtil.bas", 0

        .Add "FileSysSearchCondition.cls", 0

        .Add "FileSysSearchResult.cls", 0

        .Add "FileSysSyncCopy.bas", 0

        .Add "FileSysSyncCopyForTextFiles.bas", 0

        .Add "FindKeywords.bas", 0

        .Add "FindURLAndEmailAddresses.bas", 0

        .Add "FormStateToSetAdoConStr.cls", 0

        .Add "FormTopMostEnabledCtlHdr.cls", 0

        .Add "FormTopMostEnabledUtilities.bas", 0

        .Add "IADOConnectStrGenerator.cls", 0

        .Add "IADOConnector.cls", 0

        .Add "IVBComponentIdentifier.cls", 0

        .Add "InitRegParamOfStorageUNCPath.bas", 0

        .Add "InterfaceCall.bas", 0

        .Add "InterfaceCallForUI.bas", 0

        .Add "InterfaceCallForVBIDE.bas", 0

        .Add "LoadTextFiles.bas", 0

        .Add "LoadedADOConnectionSetting.cls", 0

        .Add "LocalInstalledMsOfficeSofts.bas", 0

        .Add "LocalInstalledSofts.bas", 0

        .Add "LocalInstalledSoftsForVBS.bas", 0

        .Add "LocalizationADOConnector.cls", 0

        .Add "ModifyFormsByWinAPI.bas", 0

        .Add "OfficeFileSecurity.bas", 0

        .Add "OpenCodePaneSwitchForm.bas", 0

        .Add "OperateSqLite3.bas", 0

        .Add "OperateVBIDECodePanes.bas", 0

        .Add "OperateVBIDEWindowCommon.bas", 0

        .Add "OperateWinProcessByAPI.bas", 0

        .Add "OperateWinShell.bas", 0

        .Add "OperateWinShutDown.bas", 0

        .Add "OperateWinWindowPos.bas", 0

        .Add "OracleCommonPath.bas", 0

        .Add "OracleOdbcConnector.cls", 0

        .Add "OracleOleDbConnector.cls", 0

        .Add "PgSqlCommonPath.bas", 0

        .Add "PgSqlDSNlessTest.bas", 0

        .Add "PgSqlOdbcConnector.cls", 0

        .Add "PluralCondDivider.bas", 0

        .Add "PluralCondTools.bas", 0

        .Add "PluralCondition.cls", 0

        .Add "RClickOnAxCtl.bas", 0

        .Add "RegExpForOraclePlSql.bas", 0

        .Add "RegExpGeneral.bas", 0

        .Add "RegParamOfStorageUNCPath.cls", 0

        .Add "SQLGeneral.bas", 0

        .Add "SQLResult.cls", 0

        .Add "SimpleAdoConStrAuthentication.bas", 0

        .Add "SimplePdAuthentication.bas", 0

        .Add "SimplePdAuthenticationAdo.bas", 0

        .Add "SolveSavePathGeneral.bas", 0

        .Add "SortGeneral.bas", 0

        .Add "SqLiteAdoConnector.cls", 0

        .Add "TerminateProcessByVBA.bas", 0

        .Add "TextFileCodePagesConversion.bas", 0

        .Add "UErrorADOSQLForm.frm", 0

        .Add "UProgressBarForm.frm", 0

        .Add "USetAdoOdbcConStrForOracle.frm", 0

        .Add "USetAdoOdbcConStrForPgSql.frm", 0

        .Add "USetAdoOdbcConStrForSqLite.frm", 0

        .Add "USetAdoOleDbConStrForAccdb.frm", 0

        .Add "USetAdoOleDbConStrForXlBook.frm", 0

        .Add "UnitTestSheetFormatSetting.cls", 0

        .Add "UnitTestWrap.bas", 0

        .Add "UnitTestWrappedParameters.cls", 0

        .Add "UserFormControlCommon.bas", 0

        .Add "UserFormToolsForAdoConStr.bas", 0

        .Add "UsingClipboard.bas", 0

        .Add "VBECodePaneGetSelectionUtil.bas", 0

        .Add "VariantTypeConversion.bas", 0

        .Add "WinAPIErrorMessageGeneral.bas", 0

        .Add "WinAPIMessageWithTimeOut.bas", 0

        .Add "WinExplorer.bas", 0

        .Add "WinINIGeneral.bas", 0

        .Add "WinRegDelete.bas", 0

        .Add "WinRegGeneral.bas", 0

        .Add "WinRegKeepingFormState.bas", 0

        .Add "WinRegManagementDeclarations.bas", 0

        .Add "WinRegStorageUNCPath.bas", 0

        .Add "WinRegView.bas", 0

        .Add "XlAdoConnector.cls", 0
    End With
End Sub