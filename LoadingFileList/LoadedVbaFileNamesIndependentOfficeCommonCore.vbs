'
'   About [IndependentOfficeCommonCore], loading specified VBA source files set and necessary VB project COM components references group names string
'
'   Coding Conventions Note:
'       This source code includes a system-Hungarian notations.
'       For compatibility from the relative VBA codes, this source may look like having some types.
'       However, the VBScript has no type system, and all variables can be interpreted as the Variant type on VBA.
'
'   Dependency Abstract:
'       This is referred from LoadVBAToOfficeFile.wsf
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 28/Aug/2024    Tarclanus-generator     Generated at 08:25 AM.
'

Option Explicit

'''
''' About "IndependentOfficeCommonCore", get importing VB component file names and COM references group names
'''
Public Sub GetLoadingVBComponentFileNamesAndRefGroupsInfoIndependentOfficeCommonCore(ByRef robjFileNameKeysDic, ByRef rstrRefGroupsDelimitedByComma)

    ' Count of setting VB project reference group names: 2

    rstrRefGroupsDelimitedByComma = "ComFewest,VBIDE"

    Set robjFileNameKeysDic = CreateObject("Scripting.Dictionary")

    ' Count of loading VBA source files: 3

    With robjFileNameKeysDic

        .Add "ExportAllVBACodes.bas", 0

        .Add "ImportAllVBACodes.bas", 0

        .Add "RemoveAllVBACodes.bas", 0
    End With
End Sub