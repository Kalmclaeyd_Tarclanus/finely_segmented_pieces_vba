'
'   About [PostgreSqlAdoExcelTools], loading specified VBA source files set and necessary VB project COM components references group names string
'
'   Coding Conventions Note:
'       This source code includes a system-Hungarian notations.
'       For compatibility from the relative VBA codes, this source may look like having some types.
'       However, the VBScript has no type system, and all variables can be interpreted as the Variant type on VBA.
'
'   Dependency Abstract:
'       This is referred from LoadVBAToOfficeFile.wsf
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 28/Aug/2024    Tarclanus-generator     Generated at 08:25 AM.
'

Option Explicit

'''
''' About "PostgreSqlAdoExcelTools", get importing VB component file names and COM references group names
'''
Public Sub GetLoadingVBComponentFileNamesAndRefGroupsInfoPostgreSqlAdoExcelTools(ByRef robjFileNameKeysDic, ByRef rstrRefGroupsDelimitedByComma)

    ' Count of setting VB project reference group names: 8

    rstrRefGroupsDelimitedByComma = "ADO,ADOX,ComFewest,MSComctlLib,MSExcel,MSForms,VBIDE,msWMI"

    Set robjFileNameKeysDic = CreateObject("Scripting.Dictionary")

    ' Count of loading VBA source files: 301

    With robjFileNameKeysDic

        .Add "ADOConStrOfAceOleDb.cls", 0

        .Add "ADOConStrOfAceOleDbCsv.cls", 0

        .Add "ADOConStrOfAceOleDbXl.cls", 0

        .Add "ADOConStrOfDsn.cls", 0

        .Add "ADOConStrOfDsnlessPgSql.cls", 0

        .Add "ADOConStrOfDsnlessSqLite.cls", 0

        .Add "ADOConStrOfOdbcPgSql.cls", 0

        .Add "ADOConStrOfOdbcSqLite.cls", 0

        .Add "ADOConStrToolsForCSV.bas", 0

        .Add "ADOConStrToolsForExcelBook.bas", 0

        .Add "ADOConStrToolsForExcelBookBase.bas", 0

        .Add "ADOConStrToolsForPgSql.bas", 0

        .Add "ADOConStrToolsForSqLite.bas", 0

        .Add "ADOConnectingInClasses.bas", 0

        .Add "ADOConnectionSetting.cls", 0

        .Add "ADOConnectionUtilities.bas", 0

        .Add "ADOConnector.cls", 0

        .Add "ADOExSheetOut.bas", 0

        .Add "ADOFailedInformation.bas", 0

        .Add "ADOFailedLogSheetOut.bas", 0

        .Add "ADOLogTextSheetOut.bas", 0

        .Add "ADOParameters.bas", 0

        .Add "ADOSheetFormatter.cls", 0

        .Add "ADOSheetIn.bas", 0

        .Add "ADOSheetOut.bas", 0

        .Add "ASWindowPositionAdjuster.cls", 0

        .Add "AddAutoLogShapes.bas", 0

        .Add "AddAutoLogShapesForAdo.bas", 0

        .Add "AdoRSetSheetExpander.cls", 0

        .Add "AppendAdoRsetOnSheet.bas", 0

        .Add "AxBarBtnEvHdlForSwitchLstPane.cls", 0

        .Add "AxBarBtnEvHdlForTxtClipBoard.cls", 0

        .Add "AxCtlLstCodeModulePaneHdl.cls", 0

        .Add "AxCtlLstRClickCodePaneMenuHdl.cls", 0

        .Add "CellFontInteriorPartialWrapper.cls", 0

        .Add "CodePaneSwitchLstUtilities.bas", 0

        .Add "CodePaneSwitchMultiLstForm.frm", 0

        .Add "CodeVBIDECommon.bas", 0

        .Add "ColorDefinedByUser.bas", 0

        .Add "ColorDefinedByUserForXl.bas", 0

        .Add "ColorToConvert.bas", 0

        .Add "ColumnsDataConvert.bas", 0

        .Add "ColumnsDataConvertForXl.bas", 0

        .Add "ColumnsFormatConditionParam.cls", 0

        .Add "ColumnsNumberFormatLocalParam.cls", 0

        .Add "CommonDataTableSheetFormatter.cls", 0

        .Add "CompareDictionaries.bas", 0

        .Add "CompareOnlySheetsByAdo.bas", 0

        .Add "CompareTextFiles.bas", 0

        .Add "ConvertDicToWinReg.bas", 0

        .Add "ConvertTextFile.bas", 0

        .Add "CoreFunctionFeatures.bas", 0

        .Add "CreateRandomDataTable.bas", 0

        .Add "CreateRandomSequence.bas", 0

        .Add "CreateRandomValue.bas", 0

        .Add "CsvAdoConnector.cls", 0

        .Add "CsvAdoSheetExpander.cls", 0

        .Add "CsvConnectingCommonPath.bas", 0

        .Add "CurrentLocaleUtility.bas", 0

        .Add "CurrentLocaleUtilityForWin.bas", 0

        .Add "CurrentLocalize.bas", 0

        .Add "CurrentSelectedXlCells.bas", 0

        .Add "CurrentSelectedXlCellsWithForm.bas", 0

        .Add "CurrentUserDomain.cls", 0

        .Add "CurrentUserDomainForAdo.bas", 0

        .Add "CurrentUserDomainUtility.bas", 0

        .Add "CurrentUserDrivesUtility.bas", 0

        .Add "DataTableADOIn.bas", 0

        .Add "DataTableCompressing.bas", 0

        .Add "DataTableCompressingToXlSheet.bas", 0

        .Add "DataTableListBox.bas", 0

        .Add "DataTableLogTextSheetOut.bas", 0

        .Add "DataTableMerge.bas", 0

        .Add "DataTableSecurity.bas", 0

        .Add "DataTableSecurityToReport.bas", 0

        .Add "DataTableSheetAutoChange.bas", 0

        .Add "DataTableSheetExOut.bas", 0

        .Add "DataTableSheetFormatter.cls", 0

        .Add "DataTableSheetIn.bas", 0

        .Add "DataTableSheetOut.bas", 0

        .Add "DataTableSheetRangeAddresses.cls", 0

        .Add "DataTableStringIn.bas", 0

        .Add "DataTableTextOut.bas", 0

        .Add "DataTableToConvert.bas", 0

        .Add "DataTableXlAutoShapeOut.bas", 0

        .Add "DataVectorSheetIn.bas", 0

        .Add "DateText.bas", 0

        .Add "DbConOracle.cls", 0

        .Add "DbConPgSql.cls", 0

        .Add "DecorateXlCellFontAndInterior.bas", 0

        .Add "DecorateXlShapeFontAndInterior.bas", 0

        .Add "DecorationGetterFromXlSheet.bas", 0

        .Add "DecorationSetterToShape.bas", 0

        .Add "DecorationSetterToShapeForXl.bas", 0

        .Add "DecorationSetterToXlSheet.bas", 0

        .Add "DistinctedNotTable.cls", 0

        .Add "DoEventsSpecial.bas", 0

        .Add "DoubleStopWatch.cls", 0

        .Add "DumpModule.bas", 0

        .Add "DumpModuleOfCurrentApplication.bas", 0

        .Add "DumpModuleToTextFile.bas", 0

        .Add "EnumerateAppWindows.bas", 0

        .Add "EnumerateAppWindowsBasic.bas", 0

        .Add "EnumerateAppWindowsForXl.bas", 0

        .Add "EnumerateChildWindows.bas", 0

        .Add "EnumerateChildWindowsToXlSheet.bas", 0

        .Add "EnumerateMsExcels.bas", 0

        .Add "EnumerateMsExcelsAndClose.bas", 0

        .Add "ErrorADOSQL.cls", 0

        .Add "ErrorContentKeeper.cls", 0

        .Add "ErrorLogSheetLocator.cls", 0

        .Add "ExpandAdoRsetOnSheet.bas", 0

        .Add "ExternalTextEditors.bas", 0

        .Add "FileSysBackup.bas", 0

        .Add "FileSysBackupBook.bas", 0

        .Add "FileSysCompare.bas", 0

        .Add "FileSysCompareForXl.bas", 0

        .Add "FileSysConvertCodePages.bas", 0

        .Add "FileSysCopyProgressBar.bas", 0

        .Add "FileSysFilesInfo.bas", 0

        .Add "FileSysFilesInfoForXl.bas", 0

        .Add "FileSysListFilesByFSO.bas", 0

        .Add "FileSysListFilesByVBADir.bas", 0

        .Add "FileSysSearchCondUtil.bas", 0

        .Add "FileSysSearchCondUtilForXl.bas", 0

        .Add "FileSysSearchCondition.cls", 0

        .Add "FileSysSearchResult.cls", 0

        .Add "FileSysSyncCopy.bas", 0

        .Add "FileSysSyncCopyForTextFiles.bas", 0

        .Add "FileSysSyncCopyForXl.bas", 0

        .Add "FindDiffFromXlSheet.bas", 0

        .Add "FindDistinctedTablesOnSheet.bas", 0

        .Add "FindForNotTablesOnSheet.bas", 0

        .Add "FindKeywords.bas", 0

        .Add "FindURLAndEmailAddresses.bas", 0

        .Add "FormStateToSetAdoConStr.cls", 0

        .Add "FormTopMostEnabledCtlHdr.cls", 0

        .Add "FormTopMostEnabledUtilities.bas", 0

        .Add "FormatConditionExpander.cls", 0

        .Add "FormatFormulasCondition.cls", 0

        .Add "FormatTextContainCondition.cls", 0

        .Add "IADOConnectStrGenerator.cls", 0

        .Add "IADOConnector.cls", 0

        .Add "ICommonDataTableSheetFormatter.cls", 0

        .Add "ICtlTextBoxesOfCurrentPrintArea.cls", 0

        .Add "ICtlTextBoxesOfCurrentRange.cls", 0

        .Add "IDataTableSheetFormatter.cls", 0

        .Add "IExpandAdoRecordsetOnSheet.cls", 0

        .Add "IOutputBookPath.cls", 0

        .Add "ISqlRsetSheetExpander.cls", 0

        .Add "ITrDataTableSheetFormatter.cls", 0

        .Add "IVBComponentIdentifier.cls", 0

        .Add "InitRegParamOfStorageUNCPath.bas", 0

        .Add "InputPasswordBox.bas", 0

        .Add "InterfaceCall.bas", 0

        .Add "InterfaceCallForUI.bas", 0

        .Add "InterfaceCallForVBIDE.bas", 0

        .Add "InterfaceCallForXl.bas", 0

        .Add "LoadAndModifyBook.bas", 0

        .Add "LoadTextFiles.bas", 0

        .Add "LoadedADOConnectionSetting.cls", 0

        .Add "LocalInstalledMsOfficeSofts.bas", 0

        .Add "LocalInstalledSofts.bas", 0

        .Add "LocalInstalledSoftsForVBS.bas", 0

        .Add "LocalInstalledSoftsForXl.bas", 0

        .Add "LocalizationADOConnector.cls", 0

        .Add "LogTextForm.frm", 0

        .Add "MakeHyperLinksModule.bas", 0

        .Add "ModifyFormsByWinAPI.bas", 0

        .Add "NotTableDistinctCondition.cls", 0

        .Add "OfficeBookSecurity.bas", 0

        .Add "OfficeBookSecurityForXl.bas", 0

        .Add "OfficeFileSecurity.bas", 0

        .Add "OpenCodePaneSwitchForm.bas", 0

        .Add "OperateSqLite3.bas", 0

        .Add "OperateVBIDECodePanes.bas", 0

        .Add "OperateVBIDEWindowCommon.bas", 0

        .Add "OperateWinProcessByAPI.bas", 0

        .Add "OperateWinShell.bas", 0

        .Add "OperateWinShutDown.bas", 0

        .Add "OperateWinWindowPos.bas", 0

        .Add "PgSqlCommonPath.bas", 0

        .Add "PgSqlDSNlessTest.bas", 0

        .Add "PgSqlOdbcConnector.cls", 0

        .Add "PgSqlOdbcSheetExpander.cls", 0

        .Add "PluralCondDivider.bas", 0

        .Add "PluralCondTools.bas", 0

        .Add "PluralCondition.cls", 0

        .Add "RClickOnAxCtl.bas", 0

        .Add "ReadFromShapeOnXlSheet.bas", 0

        .Add "RegExpForOraclePlSql.bas", 0

        .Add "RegExpGeneral.bas", 0

        .Add "RegExpSheetIn.bas", 0

        .Add "RegExpSheetOut.bas", 0

        .Add "RegParamOfStorageUNCPath.cls", 0

        .Add "SQLGeneral.bas", 0

        .Add "SQLGeneralFromXlSheet.bas", 0

        .Add "SQLResult.cls", 0

        .Add "SimpleAdoConStrAuthToXlSheet.bas", 0

        .Add "SimpleAdoConStrAuthentication.bas", 0

        .Add "SimplePdAuthentication.bas", 0

        .Add "SimplePdAuthenticationAdo.bas", 0

        .Add "SolveSavePathForXl.bas", 0

        .Add "SolveSavePathGeneral.bas", 0

        .Add "SortExcelSheet.bas", 0

        .Add "SortGeneral.bas", 0

        .Add "SqLiteAdoConnector.cls", 0

        .Add "SqLiteAdoSheetExpander.cls", 0

        .Add "SqlAdoConnectOdbcUTfPgSql.bas", 0

        .Add "SqlAdoConnectOdbcUTfSqLite.bas", 0

        .Add "SqlAdoConnectingUTfCsv.bas", 0

        .Add "SqlAdoConnectingUTfXl.bas", 0

        .Add "SqlUTfCsvAdoSheetExpander.bas", 0

        .Add "SqlUTfPgSqlOdbcSheetExpander.bas", 0

        .Add "SqlUTfSqLiteOdbcSheetExpander.bas", 0

        .Add "SqlUTfXlSheetExpander.bas", 0

        .Add "SqlUTfXlSheetExpanderByPassword.bas", 0

        .Add "TerminateProcessByVBA.bas", 0

        .Add "TextAutoShapeFontInterior.cls", 0

        .Add "TextAutoShapePositionSize.cls", 0

        .Add "TextFileCodePagesConversion.bas", 0

        .Add "TrDataTableSheetFormatter.cls", 0

        .Add "UErrorADOSQLForm.frm", 0

        .Add "UInputPasswordBox.frm", 0

        .Add "UProgressBarForm.frm", 0

        .Add "USetAdoOdbcConStrForPgSql.frm", 0

        .Add "USetAdoOdbcConStrForSqLite.frm", 0

        .Add "USetAdoOleDbConStrForXlBook.frm", 0

        .Add "UTestFormKeepStateReg.frm", 0

        .Add "UTestFormNoFormTop.frm", 0

        .Add "UTestFormTextRClickClipBoard.frm", 0

        .Add "UTestFormWithFormTop.frm", 0

        .Add "UTfADOExSheetOut.bas", 0

        .Add "UTfADOSheetExpander.bas", 0

        .Add "UTfAddAutoShapes.bas", 0

        .Add "UTfAdoConnectExcelSheet.bas", 0

        .Add "UTfAdoConnectSqLiteDb.bas", 0

        .Add "UTfColorToConvert.bas", 0

        .Add "UTfCompareOnlySheetsByAdo.bas", 0

        .Add "UTfCompareTextFiles.bas", 0

        .Add "UTfConvertDicToWinRegOnXl.bas", 0

        .Add "UTfCreateDataTable.bas", 0

        .Add "UTfCreateDataTableForXl.bas", 0

        .Add "UTfCurrentLocalize.bas", 0

        .Add "UTfDataTableSecurity.bas", 0

        .Add "UTfDataTableSheetExOut.bas", 0

        .Add "UTfDataTableSheetIn.bas", 0

        .Add "UTfDataTableSheetOut.bas", 0

        .Add "UTfDataTableTextOut.bas", 0

        .Add "UTfDataVectorSheetIn.bas", 0

        .Add "UTfDecorateXlShapeFontInterior.bas", 0

        .Add "UTfDecorationGetterFromXlSheet.bas", 0

        .Add "UTfDecorationSetterToXlSheet.bas", 0

        .Add "UTfExternalTextEditors.bas", 0

        .Add "UTfFileSysFilesInfo.bas", 0

        .Add "UTfFileSysFilesInfoForXl.bas", 0

        .Add "UTfFileSysListFilesByFSO.bas", 0

        .Add "UTfFileSysListFilesByVBADir.bas", 0

        .Add "UTfFileSysSyncCopy.bas", 0

        .Add "UTfFindDiffFromXlSheet.bas", 0

        .Add "UTfLoadAndModifyModule.bas", 0

        .Add "UTfLogTextForm.bas", 0

        .Add "UTfMakeHyperLinksModule.bas", 0

        .Add "UTfOfficeBookSecurityForXl.bas", 0

        .Add "UTfRClickOnAxCtl.bas", 0

        .Add "UTfReadFromShapeByAdoCsv.bas", 0

        .Add "UTfReadFromShapeByAdoXl.bas", 0

        .Add "UTfReadFromShapeOnXlSheet.bas", 0

        .Add "UTfSortGeneral.bas", 0

        .Add "UTfSortGeneralForXl.bas", 0

        .Add "UTfWinAPICreateKeyAndValues.bas", 0

        .Add "UTfWinAPIRegEnum.bas", 0

        .Add "UTfWinAPIRegEnumForXl.bas", 0

        .Add "UTfWinAPIRegistryRW.bas", 0

        .Add "UTfWinINIGeneral.bas", 0

        .Add "UTfWinINIGeneralByAdoXl.bas", 0

        .Add "UTfWinINIGeneralForXl.bas", 0

        .Add "UTfWinRegStorageUNCPath.bas", 0

        .Add "UnitTestSheetFormatSetting.cls", 0

        .Add "UnitTestWrap.bas", 0

        .Add "UnitTestWrapForXl.bas", 0

        .Add "UnitTestWrappedParameters.cls", 0

        .Add "UserFormControlCommon.bas", 0

        .Add "UserFormToolsForAdoConStr.bas", 0

        .Add "VBECodePaneGetSelectionUtil.bas", 0

        .Add "VariantTypeConversion.bas", 0

        .Add "WinAPIErrorMessageGeneral.bas", 0

        .Add "WinAPIMessageWithTimeOut.bas", 0

        .Add "WinExplorer.bas", 0

        .Add "WinINIGeneral.bas", 0

        .Add "WinINIGeneralByAdoXl.bas", 0

        .Add "WinINIGeneralForXl.bas", 0

        .Add "WinRegDelete.bas", 0

        .Add "WinRegGeneral.bas", 0

        .Add "WinRegKeepingFormState.bas", 0

        .Add "WinRegManagementDeclarations.bas", 0

        .Add "WinRegStorageUNCPath.bas", 0

        .Add "WinRegView.bas", 0

        .Add "WinRegViewForXl.bas", 0

        .Add "XlAdoConnector.cls", 0

        .Add "XlAdoSheetExpander.cls", 0
    End With
End Sub