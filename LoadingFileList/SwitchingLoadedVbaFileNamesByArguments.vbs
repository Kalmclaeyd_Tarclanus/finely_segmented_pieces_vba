'
'   Switching loading specified VBA source files set and necessary VB project COM components references group names by WScript.Arguments
'
'   Coding Conventions Note:
'       This source code includes a system-Hungarian notations.
'       For compatibility from the relative VBA codes, this source may look like having some types.
'       However, the VBScript has no type system, and all variables can be interpreted as the Variant type on VBA.
'
'   Dependency Abstract:
'       This is referred from LoadVBAToOfficeFile.wsf
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Wed, 28/Aug/2024    Tarclanus-generator     Generated at 08:25 AM.
'

Option Explicit

'''
''' Get importing VB component file names and COM references group names
'''
Public Sub LoadFileNameAndVBProjectReferencesInfoFromArguments(ByRef robjFileNameKeysDic, ByRef rstrRefGroupsDelimitedByComma, ByRef robjArguments)

    Dim varIdentifierKey

    If robjArguments.Count > 0 Then

        varIdentifierKey = robjArguments.Item(0)

        Select Case varIdentifierKey

            Case "OfficeCommonCore"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoOfficeCommonCore robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "OfficeCommonCoreWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoOfficeCommonCoreWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "IndependentOfficeCommonCore"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoIndependentOfficeCommonCore robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "OfficeCommonCoreAndAdo"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoOfficeCommonCoreAndAdo robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "OfficeCommonCoreAndAdoWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoOfficeCommonCoreAndAdoWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "CurrentAll"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoCurrentAll robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "CurrentAllWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoCurrentAllWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "RecommendedSmallSet"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoRecommendedSmallSet robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "RecommendedSmallSetWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoRecommendedSmallSetWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "ExcelCommonCore"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoExcelCommonCore robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "ExcelCommonCoreWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoExcelCommonCoreWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "SeparatedSwitchCodePanes"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoSeparatedSwitchCodePanes robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "SeparatedSwitchCodePanesOnlyListBox"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoSeparatedSwitchCodePanesOnlyListBox robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "ExcelCommonCoreAndAdoExpanding"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoExcelCommonCoreAndAdoExpanding robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "ExcelCommonCoreAndAdoExpandingWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoExcelCommonCoreAndAdoExpandingWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "PostgreSqlAdoExcelTools"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoPostgreSqlAdoExcelTools robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "AdjustPlottingPaperExcelTool"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoAdjustPlottingPaperExcelTool robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "OperateOutlookFromExcelTool"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoOperateOutlookFromExcelTool robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "OperateWinMergeAndGitForWindows"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoOperateWinMergeAndGitForWindows robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "WordCommonCore"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoWordCommonCore robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "WordCommonCoreWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoWordCommonCoreWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "WordCommonCoreAndXltools"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoWordCommonCoreAndXltools robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "WordCommonCoreAndXltoolsWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoWordCommonCoreAndXltoolsWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "WordCommonCoreAndAdoXltools"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoWordCommonCoreAndAdoXltools robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "WordCommonCoreAndAdoXltoolsWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoWordCommonCoreAndAdoXltoolsWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "WordCommonCoreAndVariousSystemAndAdoXltools"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoWordCommonCoreAndVariousSystemAndAdoXltools robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "PowerPointCommonCore"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoPowerPointCommonCore robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "PowerPointCommonCoreWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoPowerPointCommonCoreWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "PowerPointCommonCoreAndXltools"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoPowerPointCommonCoreAndXltools robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "PowerPointCommonCoreAndXltoolsWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoPowerPointCommonCoreAndXltoolsWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "PowerPointCommonCoreAndAdoXltools"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoPowerPointCommonCoreAndAdoXltools robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "PowerPointCommonCoreAndAdoXltoolsWithoutSanityTests"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoPowerPointCommonCoreAndAdoXltoolsWithoutSanityTests robjFileNameKeysDic, rstrRefGroupsDelimitedByComma

            Case "PowerPointCommonCoreAndVariousSystemAndAdoXltools"

                GetLoadingVBComponentFileNamesAndRefGroupsInfoPowerPointCommonCoreAndVariousSystemAndAdoXltools robjFileNameKeysDic, rstrRefGroupsDelimitedByComma
        End Select
    Else
        GetLoadingVBComponentFileNamesAndRefGroupsInfoCurrentAll robjFileNameKeysDic, rstrRefGroupsDelimitedByComma
    End If
End Sub