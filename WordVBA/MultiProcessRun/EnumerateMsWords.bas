Attribute VB_Name = "EnumerateMsWords"
'
'   Detect all Word.Document which includes the outside process of Word.Application
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Dependency Abstract:
'       Dependent on both Word and Windows OS
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sat, 11/Aug/2023    Kalmclaeyd Tarclanus    Initial creation
'

Option Explicit


'///////////////////////////////////////////////
'/// Windows API Declarations
'///////////////////////////////////////////////
#If VBA7 Then
    Private Declare PtrSafe Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As LongPtr, ByVal Msg As LongPtr, ByVal wParam As LongPtr, lParam As Any) As LongPtr
    
    Private Declare PtrSafe Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As LongPtr
    
    Private Declare PtrSafe Function ObjectFromLresult Lib "oleacc" (ByVal lResult As LongPtr, riid As Any, ByVal wParam As LongPtr, ppvObject As Any) As LongPtr
    
    Private Declare PtrSafe Function IsWindow Lib "user32" (ByVal hwnd As LongPtr) As LongPtr
    

    Private Declare PtrSafe Function SetForegroundWindow Lib "user32" (ByVal hwnd As LongPtr) As Long

    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
#Else
    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, lParam As Any) As Long
    
    Private Declare Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As Long
    
    Private Declare Function ObjectFromLresult Lib "oleacc" (ByVal lResult As Long, riid As Any, ByVal wParam As Long, ppvObject As Any) As Long
    
    Private Declare Function IsWindow Lib "user32" (ByVal hWnd As Long) As Long
    
    
    Private Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long

    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
#End If


Private Const OBJID_NATIVEOM = &HFFFFFFF0
Private Const OBJID_CLIENT = &HFFFFFFFC

' Interface IDentification
Private Const IID_IMdcList As String = "{8BD21D23-EC42-11CE-9E0D-00AA006002F3}"
Private Const IID_IUnknown As String = "{00000000-0000-0000-C000-000000000046}"
Private Const IID_IDispatch As String = "{00020400-0000-0000-C000-000000000046}"

Private Const WM_GETOBJECT = &H3D&

'-----
' class name of Windows programs
Private Const WINDOW_CLASS_EXCEL = "XLMAIN" ' Excel Application
Private Const WINDOW_CLASS_VBE = "wndclass_desked_gsk"  ' Visual Basic Editor
Private Const WINDOW_CALSS_EXPLORER = "CabinetWClass"

Private Const WINDOW_CLASS_MS_WORD = "OpusApp" ' Word Application

Private Const CHILD_CLASS_EXCELWINDOW = "EXCEL7"
Private Const CHILD_CLASS_VBE = "VbaWindow"

Private Const CHILD_CLASS_WORDWINDOW = "_WwG"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Filtering by each window title text
'**---------------------------------------------
Private mobjSearchingRegExp As VBScript_RegExp_55.RegExp

'///////////////////////////////////////////////
'/// Operations
'///////////////////////////////////////////////
'**---------------------------------------------
'** Word.Application state control for outside this VBIDE process
'**---------------------------------------------
'''
''' This can be also used from either Excel application or PowerPoint application
'''
Public Sub CloseAllProcessMsWordDocumentsExceptMySelfApplication()

    Dim varHWnd As Variant, objApplication As Word.Application, objDocument As Word.Document
    Dim strThisDocumentFullName As String
    
    Dim varWindow As Variant, objWindow As Word.Window, varOldDisplayAlerts As Variant
    
#If VBA7 Then

    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    strThisDocumentFullName = ""

    If Trim(Replace(LCase(Application.Name), "microsoft", "")) = "word" Then
    
        strThisDocumentFullName = GetCurrentOfficeFileObject().FullName
    End If
        
    With GetExecutedHWndToMsWordWindowsDic()
    
        For Each varHWnd In .Keys
            
            intHWnd = varHWnd
            
            Set objWindow = .Item(varHWnd)
            
            With objWindow.Application
            
                Set objDocument = objWindow.Document
                
                If objDocument.FullName <> strThisDocumentFullName Then
                
                    varOldDisplayAlerts = .DisplayAlerts
                
                    .DisplayAlerts = wdAlertsNone
                    
                    objDocument.Saved = True
                    
                    objDocument.Close
                
                    .DisplayAlerts = varOldDisplayAlerts
                End If
                
                If .Documents.Count = 0 Then
                
                    .Quit
                End If
            End With
        Next
    End With
    
    
    If strThisDocumentFullName = "" Then
    
        On Error Resume Next
        
        Set objApplication = GetObject(, "Word.Application")
        
        If Not objApplication Is Nothing Then
        
            objApplication.Quit
        End If
        
        On Error GoTo 0
    End If
End Sub


'''
''' This can be also used from either Excel application or PowerPoint application
'''
Public Sub ChangeVisibleAllProcessWordDocumentsExceptMySelfApplication()

    Dim varHWnd As Variant, objApplication As Word.Application, objDocument As Word.Document
    Dim strThisDocumentFullName As String
    
    Dim varWindow As Variant, objWindow As Word.Window, varOldDisplayAlerts As Variant
    
#If VBA7 Then

    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    strThisDocumentFullName = ""

    If Trim(Replace(LCase(Application.Name), "microsoft", "")) = "word" Then
    
        strThisDocumentFullName = GetCurrentOfficeFileObject().FullName
    End If
    
    With GetExecutedHWndToMsWordWindowsDic()
    
        For Each varHWnd In .Keys
            
            intHWnd = varHWnd
            
            Set objWindow = .Item(varHWnd)
            
            With objWindow.Application
            
                Set objDocument = objWindow.Document
            
                If objDocument.FullName <> strThisDocumentFullName Then
                
                    varOldDisplayAlerts = .DisplayAlerts
                
                    .DisplayAlerts = wdAlertsNone
                
                    If Not .Visible Then
                    
                        .Visible = True
                    End If
                    
                    .DisplayAlerts = varOldDisplayAlerts
                End If
            End With
        Next
    End With
End Sub

'**---------------------------------------------
'** Control specified window order
'**---------------------------------------------
'''
'''
'''
Public Function IsSpecifiedMicrosoftWordWindowSetForegroundByIncludingWindowCaptionPart(Optional ByVal vstrWindowCaption As String = "", _
        Optional ByVal vblnEnablingIgnoreCase As Boolean = False) As Boolean

    Dim varWindowCaption As Variant, intRet As Long, blnIsTheWindowFound As Boolean

    Dim varHWnd As Variant

#If VBA7 Then
    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If

    blnIsTheWindowFound = False

    With GetExecutedHWndToMsWordWindowsDic(vstrWindowTextRegExpPattern:=vstrWindowCaption, _
            vblnIgnoreCase:=vblnEnablingIgnoreCase)
     
        If .Count > 0 Then
    
            For Each varHWnd In .Keys
            
                intHWnd = varHWnd
            
                intRet = SetForegroundWindow(intHWnd)
                        
                Interaction.DoEvents
                
                Sleep 50
                
                blnIsTheWindowFound = True
            Next
        End If
    End With
    
    IsSpecifiedMicrosoftWordWindowSetForegroundByIncludingWindowCaptionPart = blnIsTheWindowFound
End Function

'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////
'''
''' get all Word application window object for all Windows processes in execution
'''
''' <Return>Dictionary(Of LongPtr[HWnd], Word.Window): current the Word window objects</Return>
Private Function GetExecutedHWndToMsWordWindowsDic(Optional ByVal vblnAllowToExcludeProtectedViewWindowType As Boolean = False, _
        Optional ByVal vstrWindowTextRegExpPattern As String = "", _
        Optional ByVal vblnIgnoreCase As Boolean = False) As Scripting.Dictionary

    'get objects as if this enumerate WordProcess().excelWindow().hWnd
    Dim varHWndArray As Variant, varSafeWordWindowArray As Variant
    Dim objDic As Scripting.Dictionary, blnIsDisabledHWnd As Boolean, strText As String
    
#If VBA7 Then

    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If
     
    Set objDic = New Scripting.Dictionary
    
    If vstrWindowTextRegExpPattern <> "" Then
    
        msubSetupSearchingRegExp vstrWindowTextRegExpPattern, vblnIgnoreCase
    End If
    
    varHWndArray = EnumFindChildWindows(vstrParentClassName:=WINDOW_CLASS_MS_WORD, vstrChildClassName:=CHILD_CLASS_WORDWINDOW)
    
    If LBound(varHWndArray) <= UBound(varHWndArray) Then
    
        ReDim varSafeWordWindowArray(LBound(varHWndArray) To UBound(varHWndArray))
        
        Dim i As Long, j As Long: j = LBound(varHWndArray) - 1
        
        For i = LBound(varHWndArray) To UBound(varHWndArray)
            
            blnIsDisabledHWnd = False
            
            j = j + 1
            
            Set varSafeWordWindowArray(j) = GetWordWindowByHWnd(varHWndArray(i))
            
            ' To be failed to get it, because of the lack ot the authority
            If varSafeWordWindowArray(j) Is Nothing Then
                
                j = j - 1
                
                blnIsDisabledHWnd = True
            End If
            
            If vblnAllowToExcludeProtectedViewWindowType Then
            
                ' Disabled to operate this because of the protected view of the Word window. If you try to operate it, then the Word application process will be aborted.
                
                If TypeName(varSafeWordWindowArray(j)) = "ProtectedViewWindow" Then
    
                    j = j - 1
    
                    blnIsDisabledHWnd = True
                End If
            End If
            
'            ' Disabled to operate this because of the protected view of the Word window. If you try to operate it, then the Word application process will be aborted.
'            If varSafeWordWindowArray(j).Application.hwnd = 0 Then j = j - 1

            If Not blnIsDisabledHWnd Then
            
                Select Case True
                
                    Case mobjSearchingRegExp Is Nothing, mobjSearchingRegExp.Pattern = ""
            
                        objDic.Add "" & varHWndArray(i), varSafeWordWindowArray(j)
                    Case Else
                    
                        ' Need to filter the Word window caption text by the RegExp object pattern
                    
                        intHWnd = varHWndArray(i)
                    
                        strText = GetWindowTextVBAString(intHWnd)
                        
                        If strText <> "" Then
                        
                            If mobjSearchingRegExp.Test(strText) Then
                            
                                objDic.Add "" & varHWndArray(i), varSafeWordWindowArray(j)
                            End If
                        Else
                            objDic.Add "" & varHWndArray(i), varSafeWordWindowArray(j)
                        End If
                End Select
            End If
        Next
    End If
    
    Set GetExecutedHWndToMsWordWindowsDic = objDic
End Function


'''
''' get Word.Window instances from hWnd window-handle
'''
''' <Argument>hWnd : window handle</Argument>
''' <Return>Word.Window</Return>
Private Function GetWordWindowByHWnd(ByRef rvarHWnd As Variant) As Word.Window

#If VBA7 Then
    Dim intMessageResult As LongPtr, intReturn  As LongPtr
#Else
    Dim intMessageResult As Long, intReturn  As Long
#End If
    
    Dim intID(0 To 3) As Long, bytID() As Byte
    Dim objWordWindow As Word.Window
    

    If IsWindow(rvarHWnd) = 0 Then Exit Function
    
    intMessageResult = SendMessage(rvarHWnd, WM_GETOBJECT, 0, ByVal OBJID_NATIVEOM)
    
    If intMessageResult Then
    
        bytID = IID_IDispatch & vbNullChar
        
        IIDFromString bytID(0), intID(0)
        
        intReturn = ObjectFromLresult(intMessageResult, intID(0), 0, objWordWindow)
    End If
    
    Set GetWordWindowByHWnd = objWordWindow
End Function

'''
''' set up the standard-module private variable RegExp object
'''
Private Sub msubSetupSearchingRegExp(ByVal vstrNewPattern As String, Optional ByVal vblnIgnoreCase As Boolean = False)

    SetupRegExpForSearchingTopLevelHWnd mobjSearchingRegExp, vstrNewPattern, vblnIgnoreCase, False
End Sub


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
'''
'''
Private Sub msubSanityTestToIsSpecifiedMicrosoftWordWindowSetForegroundByIncludingWindowCaptionPart()

    If IsSpecifiedMicrosoftWordWindowSetForegroundByIncludingWindowCaptionPart() Then
    
        Debug.Print "If the Word process exists, then it should have been in the foreground."
    End If
End Sub



'''
''' Enumerate all Word Window objects which includes other processes
'''
Private Sub msubSanityTestToCurrentExecutedWordWindows()

    Dim varWindow As Variant, objWindow As Word.Window, varHWnd As Variant
    
#If VBA7 Then

    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If
    
    Debug.Print "HWnd", "WindowTitleBarCaption", "FullPath"
    
    With GetExecutedHWndToMsWordWindowsDic()
    
        For Each varHWnd In .Keys
            
            intHWnd = varHWnd
            
            Set objWindow = .Item(varHWnd)
            
            Debug.Print CStr(intHWnd), objWindow.Caption, objWindow.Document.FullName
        Next
    End With
End Sub


'''
'''
'''
Private Sub msubSanityTestToCurrentExecutedWordWindows02()

    Dim varWindow As Variant, objWindow As Word.Window, varHWnd As Variant
    
#If VBA7 Then

    Dim intHWnd As LongPtr
#Else
    Dim intHWnd As Long
#End If
    
    Debug.Print "HWnd", "WindowTitleBarCaption", "FullPath"
    
    With GetExecutedHWndToMsWordWindowsDic(vstrWindowTextRegExpPattern:="����")
    
        For Each varHWnd In .Keys
            
            intHWnd = varHWnd
            
            Set objWindow = .Item(varHWnd)
            
            Debug.Print CStr(intHWnd), objWindow.Caption, objWindow.Document.FullName
        Next
    End With
End Sub

'''
''' Enumerate all Word Window objects which includes other processes
'''
Private Sub msubSanityTestToExecWordWindowsOldType()

    Dim varWindow As Variant, objWindow As Word.Window
    
    Debug.Print "WindowTitleBarCaption"
    
    For Each varWindow In GetExecutedHWndToMsWordWindowsDic().Items
        
        Set objWindow = varWindow
        
        Debug.Print objWindow.Caption
    Next
End Sub



